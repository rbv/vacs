#!/usr/bin/python
# Applicable to Embedding family problems
# Sum up some data from the logs printed by workers when they terminate

import sys
import os
import numpy as np
from os import environ as env

logs = env['VACS'] + '/fam/' + env['VACS_PROB'] + '/log'

#sum_exact, sum_prefixes, sum_total = 0, 0, 0
sums = np.zeros(3)
cache_times = np.zeros(2)
compute_times = np.zeros(2)
time = 0.

for fname in os.listdir(logs + '/worker'):
    if fname.startswith('cerr'):
        with open(logs + '/worker/' + fname, 'r') as log:
            for line in log.xreadlines():
                if 'system' in line:
                    pieces = line.split()
                    time += float(pieces[0][:-4]) + float(pieces[1][:-6])
                if 'EmbedState cache' in line:
                    cache_times += [float(x) for x in line.split()[-2:]]
                if 'EmbedState::compute' in line:
                    compute_times += [float(x) for x in line.split()[-2:]]
                if 'EmbeddableFamily cache' in line:
                    vals = [int(x) for x in line.split() if x.isdigit()]
                    sums += vals

print "EmbedState::compute time (cpu, wall): ", compute_times, " (%.1f%% of total cpu)" % (100 * compute_times[0] / time)
print "EmbedState cache time (cpu, wall): ", cache_times, " (%.1f%% of total cpu)" % (100 * cache_times[0] / time)
percent = int(100 * float(sums[0] + sums[1]) / sums[2] + 0.5)
print "cache hits: %d exact + %d prefix / %d (%d%%)" % (sums[0], sums[1], sums[2], percent)
