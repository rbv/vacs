
#include "fstream.h"
#include "memory.h"
#include "stdlib.h"

main( int argc, char** argv )
{
   if ( argc < 2 ) { cerr << "zero filename\n"; exit(1); }
   for ( int i=1; i<argc; i++ )
   {
     ofstream f( argv[i] );
     f.close();
   }
}
