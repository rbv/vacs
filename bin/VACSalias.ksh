echo VACSalias

alias   vc23='eval `Setenv.ksh vc 2 3`'

#alias   prob='echo "Current problem:" $VACS_PROB'
alias   ppr='cd $VACS/fam/$VACS_PROB'

# mjd's sets
#
export Vacs="$VACS"
export Src="$VACS/src"
export Inc="$VACS/inc"
export Obj="$VACS/obj"
export Bin="$VACS/bin"
export Tex="$VACS/tex"
export Exe="$VACS/exe"
export Fam="$VACS/fam"
#
# mjd's aliases
#
alias Vacs="cd $Vacs"
alias Src="cd $Src"
alias Inc="cd $Inc"
alias Obj="cd $Obj"
alias Bin="cd $Bin"
alias Tex="cd $Tex"
alias Exe="cd $Exe"
alias Fam="cd $Fam"
alias Drv="cd $Drv"

alias locker='$Exe/VACSman'
