#!/bin/csh
#
# Passed two args, problem number and path width.
#

# process the args
#
if ( $#argv == 2 ) then
   set prob = $1$2
else if ( $#argv == 3 ) then
   set prob = $1$2.$3
else
   echo "echo Error: 2 or 3 args required." 
   exit 1
endif

# check the dir exists
#
if ( ! -d $VACS/fam/$prob ) then
   echo "echo Error: no such problem"
   exit 1
endif

# All's ok -- return the commands
echo "export VACS_PROB=$prob"

exit 0
