echo "VACSsetup "

# should be in mfellows group on entry; now set file mask
#
#umask 007

#limit descriptors 128
#limit coredumpsize unlimited

# Update other environment variables
#
export VACS=/users/staff/mjd/Dirs/vacs
#setenv VACS /net/hannah/hannah/a/cdf/vacs

# still have current directory "." first in path
#
PATH=$PATH:$VACS/bin # environment var PATH is modified too.

#########export TEXINPUTS=$TEXINPUTS\:$VACS/tex/inputs
#export BIBINPUTS=.\:$VACS/tex/inputs/bibinputs
export VACS_PROB=vc2.3

# setup interactive aliases
#
. $VACS/bin/VACSalias.ksh
