BEGIN {
   print "Checking depth " depth
}

END {
   for (i in count)
      print pad( count[i], 8 ) i
}

{

   graph = $3
   if ( substr(graph,1,1) != "[" ) error( "no graph in col 3" );
   graph = substr( graph, 2, length(graph)-2 )

   pos = indexn( graph, ",", depth ) 

   if ( pos == 0 ) { count["none"]++; next }

   count[ "[" substr(graph,1,pos-1) "]" ]++
}

function indexn( s, t, n      ,i,j,k ) {
   i = 0
   for ( j=0; j<n; j++ )
   {
      k = index( substr(s,i+1), t )
      if ( k == 0 ) return 0
      i += k
   }
   return i
}

function error(s) {
   print "Error: " s " on line " NR 
}

function pad( s, len     ,t ) {
   t = s
   while( length(t) < len ) t = t " "
   return t
}

