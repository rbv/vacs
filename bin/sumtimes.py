#!/usr/bin/python

import sys
import os
import numpy as np
import cPickle
from os import environ as env

logs = env['VACS'] + '/fam/' + env['VACS_PROB'] + '/log'

#grep "^done" $VACS/fam/$VACS_PROB/log/worker/* | cut -d ' ' -f 3 | python -c "import sys; print sum([float(a.strip()) for a in sys.stdin.readlines()])"

workertimes = []

def sum_diagonals(array):
    """
    Returns the sums of the antidiagonals of a matrix:
    First entry is top left cell of input, second is sum of two adjacent cells...
    """
    ret = np.zeros(sum(array.shape) - 1, dtype=array.dtype)
    for index, x in np.ndenumerate(array):
        ret[sum(index)] += x
    return ret

class WorkerTask(object):
    def __init__(self, name):
        self.name = name
        self.counts = np.zeros((50, 50), dtype=np.int32)
        self.times = np.zeros((50, 50))

    def add(self, vertices, edges, time):
        if time >= 0.0:
            self.counts[vertices, edges] += 1
            self.times[vertices, edges] += time
        
    def print_stats(self, axis, name):
        if axis == -1:
            scounts = sum_diagonals(self.counts)
            stimes = sum_diagonals(self.times)
        else:
            scounts = self.counts.sum(axis=axis)
            stimes = self.times.sum(axis=axis)
        last = -1
        # Doesn't numpy has a better way to find the non-zero range of an array?
        for i, c in reversed(list(enumerate(scounts))):
            if c:
                last = i
                break
        start = False
        for i in range(last + 1):
            start = start or scounts[i]
            if not start:
                continue
            print "%2d %-9s %8d tasks \t%8.2fs \tavg %-8.3gs" % (i, name + ':', scounts[i], stimes[i], stimes[i]/scounts[i])

    def print_info(self):
        self.total_time = self.times.sum()
        if self.total_time == 0.0:
            return
        print
        print self.name + ': total time=%.2f avg time=%.3g' % (self.times.sum(), (self.times.sum() / self.counts.sum()))
        #self.print_stats(1, 'vertices')
        #self.print_stats(0, 'edges')
        self.print_stats(-1, 'ops')


tasks = [WorkerTask(n) for n in (
        "OOF testing",
        "Canonical t-parse testing",
        "Tilde-Minor testing",
        "Universal distinguisher testing",
        "DP Congruence testing",
        "Random distinguisher search",
        "Testset")]

for fname in os.listdir(logs + '/worker'):
    with open(logs + '/worker/' + fname, 'r') as log:
        print fname
        if fname.startswith('cerr'):
            for line in log.xreadlines():
                if line.startswith('done;'):
                    workertimes.append(float(line.split()[2]))
        else:
            for line in log.xreadlines():           
                if line.startswith('Size:'):
                    tokens = line.split()
                    vertices, edges = int(tokens[1]), int(tokens[2])
                    #index = int(tokens[1]), int(tokens[2])
                    for time, task in zip(tokens[4:], tasks):
                        task.add(vertices, edges, float(time))



rem = sum(workertimes)
for task in tasks:
    task.print_info()
    rem -= task.total_time

print "total worker time:", sum(workertimes)
print "unaccounted-for worker time:", rem
