/log/ { next }

BEGIN { max = 0 }

{
   count[ $2 ]++;
   if ( $2 > max ) max = $2
}

END {
   for (i=0; i<=max; i++)
      if ( count[i] )
         printf( "%4d %8d\n", i, count[i] );
}
