
/*tex

\file{vacslocker.c}
\path{src/process}
\title{The main server program for VACS filesystem locks.}
\classes{}
\makeheader

xet*/

#include <stdlib.h>
#include <iostream>
#include <fstream>

#include <signal.h>
#include <setjmp.h>

#include <sys/types.h>
#include "vacs/fs_lock.h"
#include "vacs/fs_server.h"

static sigjmp_buf sig_point;

#ifdef SUNPRO
void ctrlc(int,...)
#else
void ctrlc(int)
#endif
{
  cerr << "\nClosing locker\n";
  //siglongjmp(sig_point,1);
  FilesystemLock::closeServer();
  exit(0);
}

int main()
{
    signal( SIGINT, ctrlc );
    signal( SIGTERM, ctrlc );
    //if ( sigsetjmp(sig_point, 0) ) goto CtrlC_end;

    LockList  clients;
    const ClientNode  *pClientNode;

    int		listenfd, clifd, nread;
    char	buf[STRMAX];
    int		portNum = 0;
    fd_set	rset, allset;

    FD_ZERO(&allset);

    /* obtain fd to listen for client requests on */
    //
// gethostname( buf, STRMAX );

    aassert( getenv("VACS") );
    aassert( getenv("VACS_PROB") );

    aasserten( (listenfd = serverListen( buf, STRMAX, portNum)) >= 0 );

cerr << "Locker: setting server " << buf << ',' << portNum << nl;

    ofstream cmdLog;
    if ( ! FilesystemLock::setServerNames( buf, portNum, cmdLog ) )
    {
     cerr << "Lock server already open!\n";
     exit(0);
    }
    cmdLog.close();

    FD_SET(listenfd, &allset);

    while (true) 
    {
    	rset = allset;		/* rset gets modified each time around */

#if 0
cerr << "+ maxfd = " << clients.max_fd() << ' ';
clients.dump();
#endif
//cerr << "selecting with maxfd = " << clients.max_fd() << nl;

    	aasserten( select(max(clients.max_fd(),listenfd) + 1, &rset, 
			NULL, NULL, NULL) > 0 );

//cerr << "after select\n";

    	/* accept new client request */
        //
    	if (FD_ISSET(listenfd, &rset)) 
        { 	
    		aasserten( (clifd = serverAccept(listenfd)) >= 0 );

//cerr << "serverAccept " << clifd << nl;

    		clients.clientAdd(clifd);

    		FD_SET(clifd, &allset);

    		//cerr << "new connection: fd = " << clifd << nl;
    		continue;
    	}

        pClientNode = clients.head();
        //
    	while ( pClientNode ) 
        {
          clifd = pClientNode->object().fd;

//cerr << "checking action on " << clifd << nl;

          if (FD_ISSET(clifd, &rset)) 
          {
    	    /* 
             * read argument buffer from client 
             */
	    if ( (nread = read(clifd, buf, STRMAX)) < 0)
		aasserten(false); 
		// log_sys("read error on fd %d", clifd);

	    else if (nread == 0) 
    	    {
		//cerr << "closeing: fd " << clifd << nl;

		/* client has closed connection */
               
    		close(clifd);
    		FD_CLR(clifd, &allset);

    		clients.clientDel(clifd);

		//cerr << "closed: fd " << clifd << nl;

                break;  // our pClientNode is bad

    	    } else			

	    /* 
             * Process client's rquest 
	     */
	    clients.request(buf, nread, clifd);

    	 } // is selected for action

        pClientNode = pClientNode->next();

       } // while another client

   } // infinite loop

//CtrlC_end:

    FilesystemLock::closeServer();
    return 0;
}
