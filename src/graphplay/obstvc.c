
/* 
 * A fixed k-"vertex cover" obstruction tester.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/vertcover_ga.h"

static const int FixedK = 2;

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

bool membership( const Graph &G )
 {
     return vcLazy( G, FixedK );
 }

main()
{
   int num;

   cin >> num;

   Graph G;

   for (int i=0; i<num; i++)
    { 
     cin >> G;
  
     cerr << i+1 << nl;

     if (minorObstruction( G, membership )) cerr << G << nl;
    }

}
