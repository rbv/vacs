// precalc vert part for all graphs
// split loops and use streams

#include <iostream>
#include <stdlib.h>

#include "general/stream.h"
#include "set/iterset.h"
#include "isomorphism/iso_graph.h"
#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"
#include "graph/algorithm/distance_ga.h"

// MAX is one greater than number of possible triangles
// (or two greater than what k-FES obsts we want)
// 8
// 7
#define PASS2 1
#define MAX 8

//--------------------------------------------------------------------
//--------------------------------------------------------------------
#if 1
#define MAXMEM 50000000
static char* mem_base = 0;
static char* mem_pos;
 
void *operator new(size_t size, void*)
{
  cerr << "new " << (mem_pos - mem_base) << "\n";
  if ( mem_base == 0 )
  {
    mem_base = new char[MAXMEM];
    mem_pos = mem_base;
  }
  char* t = mem_pos;
  mem_pos += size;
  if ( mem_pos-mem_base > MAXMEM ) { cerr << "out of mem\n"; exit(1); }
  return t;
}
#endif

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Iso1
{
   vertNum vertices;
   vertNum edges;
   DegreeSequence degSeq;

public:
   void calc( const Graph& G );
   bool operator==( const Iso1& ) const;
};
void Iso1::calc( const Graph& G )
{
  vertices = G.order();
  edges = G.edges();
  degSeq.resize( vertices );
  G.degVec( degSeq, 1 );
}
bool Iso1::operator==( const Iso1& i ) const 
{
  if ( vertices != i.vertices ) return false;
  if ( edges != i.edges ) return false;
  if ( degSeq != i.degSeq ) return false;
  return true;
}

struct Iso2
{
   VertPartition vertPart;
   DistanceMatrix distMat;
public:
   void calc( const Graph& G );
};
void Iso2::calc( const Graph& G )
{
  distMat.setDim( G.order() );
  dist_mat( G, distMat );
  vertPart.init( G.order(), distMat );
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Info6
{
  Info6() {}
  Info6(int i) : G(i) {}
  Info6(const Graph& g) : G(g) {}
  Info6(const Graph& g, int i ) : G(g,i) {}
  Graph G;
  Iso1 iso1;
  Iso2 iso2;
  void calc();
  void clear();
};

void Info6::calc()
{
  iso1.calc(G);
  iso2.calc(G);
}

void Info6::clear()
{
  iso1.degSeq.resize(0);
  iso2.distMat.setDim(0);
  iso2.vertPart.class_num.resize(0);
  iso2.vertPart.class_cnt.resize(0);
  iso2.vertPart.class_rep.setDim(0);
}

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

static bool isoTest( const Info6& g1, const Info6& g2 )
{
   if ( ! ( g1.iso1 == g2.iso1 ) ) return false;
   return ::isomorphic( g1.iso2.distMat, g2.iso2.distMat, g1.iso2.vertPart, g2.iso2.vertPart, noBoundary );
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

void load( Array< PIterSet<Info6> >& obsts )
{
  obsts.resize( MAX );
  bistream is( "/tmp/obsts" );
  for ( int tri=2; tri<MAX; tri++ )
  {
    is >> ((PCollection<Info6>&) obsts[tri] );
    cerr << "  loaded level " << tri << " graphs " << obsts[tri].size() << nl;
  }
  is.close();
}


int main()
{
  Array< PIterSet<Info6> > obsts(MAX);

  // start with K3 
  {
  Info6 *G = new Info6(3);
  G->G.addEdge( 0, 1 );
  G->G.addEdge( 0, 2 );
  G->G.addEdge( 1, 2 );
  G->calc();
  obsts[1].add( G );
  }

  int tri;
#ifdef PASS0
  for ( tri=2; tri<MAX-1; tri++ )
#elif defined( PASS1 ) || defined( PASS2 ) || defined( PASS3 )
  load( obsts );
  for ( tri=MAX-1; tri<MAX; tri++ )
#elif defined( PASS9 )
  load( obsts );
  if ( 0 == 0 )
#endif
  {
    cerr << "building level " << tri;
    cerr << " from " << obsts[tri-1].size() << " graphs " << nl;
    cerr << "level size is initially " << obsts[tri].size() << nl;
    int num = 0;

    Info6* pG;
    obsts[tri-1].start();
    while ( pG = obsts[tri-1].next() )
    {
      cerr << ++num << nl;
      Graph& G = pG->G;
      Info6 *H;
      int n = G.order();
      int u, v, w;

#if defined(PASS0) || defined(PASS3)
      // add 0 vertices (1, 2 or 3 edges)
      for ( u=0; u<n-2; u++ )
        for ( v=u+1; v<n-1; v++ )
        {
          for ( w=v+1; w<n; w++ )
          {
            if ( G.isEdge(u,w) && G.isEdge(v,w) && G.isEdge(u,v) ) continue;

            H = new Info6(G);
            H->G.addEdge( u, v );
            H->G.addEdge( u, w );
            H->G.addEdge( v, w );
            H->calc();

            if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
          }
        }
#endif
 
#if defined(PASS0) || defined(PASS2)
      // add 1 vertex
      for ( u=0; u<n-1; u++ )
        for ( v=u+1; v<n; v++ )
        {
          H = new Info6(G,1);
          H->G.addEdge( u, v );
          H->G.addEdge( u, n );
          H->G.addEdge( v, n );
          H->calc();

          if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
        }
#endif
 
#if defined(PASS0) || defined(PASS1)
     // add 2 vertices
      for ( u=0; u<n; u++ )
      {
        H = new Info6(G,2);
        H->G.addEdge( u, n );
        H->G.addEdge( u, n+1 );
        H->G.addEdge( n, n+1 );
        H->calc();

        if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
      }
#endif

    }
    obsts[tri-1].stop();

    cerr << "level size = " << obsts[tri].size() << nl;

  }

#ifndef PASS9
  // save so far
  bostream os( "/tmp/obsts" );
  for ( tri=2; tri<MAX; tri++ )
    os << ((PCollection<Info6>&) obsts[tri] );
  os.close();

#endif

#ifdef PASS9

  for ( int fes = 1; fes <= MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    for ( int tri=1; tri<MAX; tri++ )
    {
      obsts[tri].start();
      Info6* pG;
      while ( pG = obsts[tri].next() )
      {
        if ( fes == fesBrute( pG->G ) )
           cerr << "triangles: " << tri << nl << pG->G << nl;
      }
      obsts[tri].stop();
    }
  }

  Array< PIterSet<Graph> > FES(MAX*2);  // over declare dim

  cerr << "sorting by FES: ";
  for ( tri=1; tri<MAX; tri++ )
  {
    cerr << tri << ' ';
    obsts[tri].start();
    Info6* pG;
    while ( pG = obsts[tri].next() )
    {
      FES[fesBrute(pG->G)].addUnique(&(pG->G));
    }
    obsts[tri].stop();
  }
  cerr << nl;

  for ( fes = 1; fes<MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    cerr << "num = " << FES[fes].size() << nl;
    Graph* pG;

    FES[fes].start();
    //
    while ( pG = FES[fes].next() )
    {
        cerr << *pG << nl;
    }
    //
    FES[fes].stop();
  }

#endif

} // main


//-----------------------------------------------------------------
//-----------------------------------------------------------------

bistream& operator>>( bistream& s, Iso1& p )
{
   s >> p.vertices >> p.edges >> p.degSeq;
   return s;
}  
   
bostream& operator<<( bostream& s, const Iso1& p )
{
   s << p.vertices << p.edges << p.degSeq;
   return s;
}  

bistream& operator>>( bistream& s, Iso2& p )
{
   s >> p.vertPart >> p.distMat;
   return s;
}  
   
bostream& operator<<( bostream& s, const Iso2& p )
{
   s << p.vertPart << p.distMat;
   return s;
}  

bistream& operator>>( bistream& s, Info6& p )
{
   s >> p.G >> p.iso1 >> p.iso2;
   return s;
}  
   
bostream& operator<<( bostream& s, const Info6& p )
{
   s << p.G << p.iso1 << p.iso2;
   return s;
}  

bostream& operator<<( bostream& o, const PCollection<Info6>& C )
{
   o << C.size();
   PCollectionIter<Info6> I( C );
   Info6* p;
   while ( (p = I()) != C.emptyFlag() )
      o << *p;
   return o;
}

bistream& operator>>( bistream& o, PCollection<Info6>& C )
{
   PCollectionIter<Info6> I( C );
   int size;
   o >> size;
   Info6* p;
   for (int i=0; i<size; i++ )
   {
     p = new Info6;
     o >> *p;
     C.add( p );
   } 
   return o;
}

