

/* 
 * A fixed within k "planar" obstruction finder.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/genus_ga.h"
#include "graph/i-o/tapes.h"

static const int FixedK = 1;

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

bool membership( const Graph &G )
{
//cerr << "membership on: " << G;

     //return planar( G, FixedK );
     //return planarEdge( G, FixedK );
     return outerplanar1( G );
}

#ifdef TRAP
#include <signal.h>
#include <setjmp.h>

jmp_buf entry_point; /* place to jump back */

ctrlc()
 {
  signal(SIGINT, ctrlc);
  cerr << "ontrol-c\n";
  longjmp(entry_point,1);
 }
#endif

static int another_file()
 {
  char y_n[40];
  printf("Want another file? ");
  fflush(stdout);
  scanf("%s", y_n);

  switch(y_n[0])
   {
    case 'y':
    case 'Y': return 1;
    default:  return 0;
   }
 }

//#define FROM_FILE

/*
 * Program to loop through Read's files.
 */
void main()
 { 

do {

#ifdef FROM_FILE

     Graph G;
     cin >> G;

     if (minorObstruction( G, membership )) cerr << "obstruction\n";

#else

  Tape_graph tape(NO_ISOLATED);

   /*
    * Trap interupt.
    */
#ifdef TRAP
  signal(SIGINT, ctrlc);
  if (setjmp(entry_point)) goto Break_point;
#endif

  while (tape.nextgraph())
   {
     Graph *G = tape.graph();

     if (minorObstruction( *G, membership )) cerr << *G << nl;

//   if (G->connected())  cerr << "graph is connected\n";
   }

#endif

  } while(another_file());

  return;

#ifdef TRAP
Break_point:
  cerr << "\n *** Control-C Trap ***\n";
#endif
 }
