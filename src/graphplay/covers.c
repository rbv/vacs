
/* 
 * A fixed k-"path/cycle cover" obstruction finder.
 */
#include <iostream>
#include <stdlib.h>
#include "graph/graph.h"
#include "graph/i-o/tapes.h"

static int FixedK = 3;

extern bool within1maxPath(const Graph &G, vertNum l);
extern bool within2maxPath(const Graph &G, vertNum l);
extern bool maxCycle(const Graph &G, vertNum l);
extern bool within1maxCycle(const Graph &G, vertNum l);
extern bool within2maxCycle(const Graph &G, vertNum l);

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

bool membership( const Graph &G )
{
     return within1maxCycle(G, FixedK );
}

#ifdef TRAP
#include <signal.h>
#include <setjmp.h>

jmp_buf entry_point; /* place to jump back */

ctrlc()
 {
  signal(SIGINT, ctrlc);
  cerr << "ontrol-c\n";
  longjmp(entry_point,1);
 }
#endif

static int another_file()
 {
  char y_n[40];
  printf("Want another file? ");
  fflush(stdout);
  scanf("%s", y_n);

  switch(y_n[0])
   {
    case 'y':
    case 'Y': return 1;
    default:  return 0;
   }
 }

bool NoDegreeZeroOrOneVertices( const Graph &G );

/*
 * Program to loop through Read's files.
 */
void main(int argc, char** argv)
{ 

   if (argc == 2) FixedK = atoi(argv[1]);

do {
  Tape_graph tape(NO_ISOLATED);

   /*
    * Trap interupt.
    */
#ifdef TRAP
  signal(SIGINT, ctrlc);
  if (setjmp(entry_point)) goto Break_point;
#endif

  while (tape.nextgraph())
   {
     Graph *G = tape.graph();

//     cerr << *G;
     if ( ! G->connected() ) continue;
     if ( ! NoDegreeZeroOrOneVertices(*G) ) continue;

     if (minorObstruction( *G, membership )) cerr << *G << nl;

//   if (G->connected())  cerr << "graph is connected\n";
   }

  } while(another_file());

  return;

#ifdef TRAP
Break_point:
  cerr << "\n *** Control-C Trap ***\n";
#endif
}
