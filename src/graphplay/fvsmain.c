
/* 
 * Test fixed fvs alg.
 */
#include <iostream>

#include "general/dtime.h"
#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"

main()
{
   int num;

   cin >> num;

   int ans1, ans2;

   Graph G;

   Dtime timer;
   float t1,t2, total=0.0;

   for (int i=0; i<num; i++)
    { 
     cin >> G;
  
     timer.start();
     //
     ans1 = fvsBrute( G, 4 );
     //
     t1 = timer();

     ans2 = fvsFixed( G, 4 );
     //
     t2 = timer();

     assert ( ans1 == ans2 );

     cerr << i+1 << ' ' << ans1 << " @ " << t1 << ' ' << t2 <<nl;

     total += t1+t2;
 
    }

  cerr << "total time: " << total <<nl;
}
