
/* 
 * Test minimum cycle.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/cycle_ga.h"

main()
{
   int num;

   cin >> num;

   int ans1, ans2;

   Graph G;

   for (int i=0; i<num; i++)
    { 
     cin >> G;
  
     ans1 = girth( G );

     Cycle C;
     //
     ans2 = girth( C, G );

     assert ( ans1 == ans2 );

     cerr << i+1 << ' ' << ans1 << nl;
 
     cerr << C << nl;

    }

}
