// precalc iso1 not iso2

#include <iostream>

#include "set/iterset.h"
#include "isomorphism/iso_graph.h"
#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"
#include "graph/algorithm/distance_ga.h"

// MAX is one greater than number of possible triangles
// (or two greater than what k-FES obsts we want)
// 8
#define MAX 8

//--------------------------------------------------------------------
//--------------------------------------------------------------------

#define MAXMEM 50000000
static char* mem_base = 0;
static char* mem_pos;

void *operator new(size_t size, void*)
{
  if ( mem_base == 0 )
  {
    mem_base = new [MAXMEM] char;
    mem_pos = mem_base;
  }
  char* t = mem_pos;
  mem_pos += size;
  if ( mem_pos > MAXMEM ) { cerr << "out of mem\n"; exit(1); }
  return t;
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Iso1
{
   vertNum vertices;
   vertNum edges;
   DegreeSequence degSeq;

public:
   void calc( const Graph& G );
   bool operator==( const Iso1& ) const;
};
void Iso1::calc( const Graph& G )
{
  vertices = G.order();
  edges = G.edges();
  degSeq.resize( vertices );
  G.degVec( degSeq, 1 );
}
bool Iso1::operator==( const Iso1& i ) const 
{
  if ( vertices != i.vertices ) return false;
  if ( edges != i.edges ) return false;
  if ( degSeq != i.degSeq ) return false;
  return true;
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Info5
{
  Info5(int i) : G(i) {}
  Info5(const Graph& g) : G(g) {}
  Info5(const Graph& g, int i ) : G(g,i) {}
  Graph G;
  Iso1 iso1;
  void calc();
  void clear();
};

void Info5::calc()
{
  iso1.calc(G);
}

void Info5::clear()
{
  iso1.degSeq.resize(0);
}

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

static bool isoTest( const Info5& gg1, const Info5& gg2 )
{
   Info5& g1 = (Info5&) gg1;
   Info5& g2 = (Info5&) gg2;
   if ( ! ( g1.iso1 == g2.iso1 ) ) return false;
   bool r = (g1.G==g2.G);
   return r;
}

int main()
{
  Array< PIterSet<Info5> > obsts(MAX);

  // start with K3 
  {
  Info5 *G = new Info5(3);
  G->G.addEdge( 0, 1 );
  G->G.addEdge( 0, 2 );
  G->G.addEdge( 1, 2 );
  G->calc();
  obsts[1].add( G );
  }

  for ( int tri=2; tri<MAX; tri++ )
  {
    cerr << "building level " << tri << nl;
    int num = 0;

    Info5* pG;
    obsts[tri-1].start();
    while ( pG = obsts[tri-1].next() )
    {
      cerr << ++num << nl;
      Graph& G = pG->G;
      Info5 *H;
      int n = G.order();
      int u, v, w;

      // add 0 vertices (1, 2 or 3 edges)
      for ( u=0; u<n-2; u++ )
        for ( v=u+1; v<n-1; v++ )
        {
          for ( w=v+1; w<n; w++ )
          {
            if ( G.isEdge(u,w) && G.isEdge(v,w) && G.isEdge(u,v) ) continue;

            H = new Info5(G);
            H->G.addEdge( u, v );
            H->G.addEdge( u, w );
            H->G.addEdge( v, w );
            H->calc();

            if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
          }
        }
  
      // add 1 vertex
      for ( u=0; u<n-1; u++ )
        for ( v=u+1; v<n; v++ )
        {
          H = new Info5(G,1);
          H->G.addEdge( u, v );
          H->G.addEdge( u, n );
          H->G.addEdge( v, n );
          H->calc();

          if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
        }

     // add 2 vertices
      for ( u=0; u<n; u++ )
      {
        H = new Info5(G,2);
        H->G.addEdge( u, n );
        H->G.addEdge( u, n+1 );
        H->G.addEdge( n, n+1 );
        H->calc();

        if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
      }

    }
    obsts[tri-1].stop();

    cerr << "level size = " << obsts[tri].size() << nl;

    obsts[tri-1].start();
    while ( pG = obsts[tri-1].next() ) pG->clear();
    obsts[tri-1].stop();
  }

  for ( int fes = 1; fes <= MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    for ( int tri=1; tri<MAX; tri++ )
    {
      obsts[tri].start();
      Info5* pG;
      while ( pG = obsts[tri].next() )
      {
        if ( fes == fesBrute( pG->G ) )
           cerr << "triangles: " << tri << nl << pG->G << nl;
      }
      obsts[tri].stop();
    }
  }

  Array< PIterSet<Graph> > FES(MAX*2);  // over declare dim

  cerr << "sorting by FES: ";
  for ( tri=1; tri<MAX; tri++ )
  {
    cerr << tri << ' ';
    obsts[tri].start();
    Info5* pG;
    while ( pG = obsts[tri].next() )
    {
      FES[fesBrute(pG->G)].addUnique(&(pG->G));
    }
    obsts[tri].stop();
  }
  cerr << nl;

  for ( fes = 1; fes<MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    cerr << "num = " << FES[fes].size() << nl;
    Graph* pG;

    FES[fes].start();
    //
    while ( pG = FES[fes].next() )
    {
        cerr << *pG << nl;
    }
    //
    FES[fes].stop();
  }

} // main
