#include "graph/tree.h"
#include "graph/psspec.h"
#include <stdlib.h>

static count = 0;

static void genCTcallback( Tree& freeTree )
{
  DegreeSequence deg;
  freeTree.degVec(deg, 1);

  if (deg[0] > 3) return;	// ignore if max degree 3

  count++;
  //cout << freeTree << nl;
  tree2ps(freeTree);
}

main(int argc, char** argv)
{
  int n = 9;
  if (argc == 2) n = atoi(argv[1]);
  
  Tree::freeTrees( n, genCTcallback );

  cerr << "total trees: " << count << nl;
}
