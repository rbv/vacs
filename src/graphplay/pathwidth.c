
/* 
 * User interactive pathwidth <= k tester.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/pathwidth_ga.h"

//#define FIXEDK

main()
{
#ifdef FIXEDK
   int k=4;
   //
   cerr << "Checking for pathwidth <= " << k << nl;
#endif

   int num;

   cin >> num;

   Graph G;

   for (int i=0; i<num; i++)
    { 
     cin >> G;
  
//     if (G.connected() == false) { cerr << "not connected\n"; continue; }

#ifdef FIXEDK
     bool is = pathwidth( G, k );
  
     cerr << i+1 << ' ';
     //
     if (is) cerr << "yes\n"; else cerr << "no\n";
#else
     cerr << i+1 << ' ' << pathwidth( G ) << nl;
#endif

    }

}
