
/* 
 * User interactive planar tester.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/genus_ga.h"

main()
{
   int num;

   cin >> num;

   Graph G;

   for (int i=0; i<num; i++)
    { 
     cin >> G;

     cerr << G << nl;
     // 
     cerr << i+1 << (planar(G) ? " Yes" : " No") << nl;
    }

}
