/* 
 * User interactive graph tester (iso setminus between two files).
 */
#include <iostream>
#include <fstream>

#include "assert.h"
#include "array/array.h"
#include "graph/graph.h"

main(int argc, char* argv[])
{
   Graph *G;

   aassert(argc == 3);

   ifstream in1(argv[1]);
   if ( in1.fail() ) { cerr << "Can't open " << argv[1] << nl; return -1; }
   //
   ifstream in2(argv[2]);
   if ( in2.fail() ) { cerr << "Can't open " << argv[2] << nl; return -1; }

   int num1;
   in1 >> num1;
   Array<Graph*> aGraph1(num1);

   for (int i=0; i<num1; i++)
   {
     G = new Graph();
     in1 >> *G;
     aGraph1[i] = G;
   }

   int num2;
   in2 >> num2;
   Array<Graph*> aGraph2(num2);

   for (i=0; i<num2; i++)
   {
     G = new Graph();
     in2 >> *G;
     aGraph2[i] = G;
   }

   int count = 0;
   for (i=0; i<num1; i++)
   {
     bool isoFlag = false;
     for (int j=0; j<num2; j++)  
     {
       if ( *(aGraph1[i]) == *(aGraph2[j]) )
       {
	 isoFlag = true;
         break;
       }
     }

     if ( isoFlag == false )
     {
       cout << *(aGraph1[i]);
       count++;
     }     

   }

   cout << "--- " << count << " differences ---\n";
}
