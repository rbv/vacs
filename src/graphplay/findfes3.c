// precalc vert part for all graphs

#include <iostream>

#include "set/iterset.h"
#include "isomorphism/iso_graph.h"
#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"
#include "graph/algorithm/distance_ga.h"

// MAX is one greater than number of possible triangles
// (or two greater than what k-FES obsts we want)
// 8
#define MAX 8

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Iso1
{
   vertNum vertices;
   vertNum edges;
   DegreeSequence degSeq;

public:
   void calc( const Graph& G );
   bool operator==( const Iso1& ) const;
};
void Iso1::calc( const Graph& G )
{
  vertices = G.order();
  edges = G.edges();
  degSeq.resize( vertices );
  G.degVec( degSeq, 1 );
}
bool Iso1::operator==( const Iso1& i ) const 
{
  if ( vertices != i.vertices ) return false;
  if ( edges != i.edges ) return false;
  if ( degSeq != i.degSeq ) return false;
  return true;
}

struct Iso2
{
   VertPartition vertPart;
   DistanceMatrix distMat;
public:
   void calc( const Graph& G );
};
void Iso2::calc( const Graph& G )
{
  distMat.setDim( G.order() );
  dist_mat( G, distMat );
  vertPart.init( G.order(), distMat );
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Info
{
  Info(int i) : G(i) {}
  Info(const Graph& g) : G(g) {}
  Info(const Graph& g, int i ) : G(g,i) {}
  Graph G;
  Iso1 iso1;
  Iso2 iso2;
  void calc();
  void clear();
};

void Info::calc()
{
  iso1.calc(G);
  iso2.calc(G);
}

void Info::clear()
{
  iso1.degSeq.resize(0);
  iso2.distMat.setDim(0);
  iso2.vertPart.class_num.resize(0);
  iso2.vertPart.class_cnt.resize(0);
  iso2.vertPart.class_rep.setDim(0);
}

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

static bool isoTest( const Info& g1, const Info& g2 )
{
   if ( ! ( g1.iso1 == g2.iso1 ) ) return false;
   return ::isomorphic( g1.iso2.distMat, g2.iso2.distMat, g1.iso2.vertPart, g2.iso2.vertPart, noBoundary );
}

int main()
{
  Array< PIterSet<Info> > obsts(MAX);

  // start with K3 
  {
  Info *G = new Info(3);
  G->G.addEdge( 0, 1 );
  G->G.addEdge( 0, 2 );
  G->G.addEdge( 1, 2 );
  G->calc();
  obsts[1].add( G );
  }

  for ( int tri=2; tri<MAX; tri++ )
  {
    cerr << "building level " << tri << nl;
    int num = 0;

    Info* pG;
    obsts[tri-1].start();
    while ( pG = obsts[tri-1].next() )
    {
      cerr << ++num << nl;
      Graph& G = pG->G;
      Info *H;
      int n = G.order();
      int u, v, w;

      // add 0 vertices (1, 2 or 3 edges)
      for ( u=0; u<n-2; u++ )
        for ( v=u+1; v<n-1; v++ )
        {
          for ( w=v+1; w<n; w++ )
          {
            if ( G.isEdge(u,w) && G.isEdge(v,w) && G.isEdge(u,v) ) continue;

            H = new Info(G);
            H->G.addEdge( u, v );
            H->G.addEdge( u, w );
            H->G.addEdge( v, w );
            H->calc();

            if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
          }
        }
  
      // add 1 vertex
      for ( u=0; u<n-1; u++ )
        for ( v=u+1; v<n; v++ )
        {
          H = new Info(G,1);
          H->G.addEdge( u, v );
          H->G.addEdge( u, n );
          H->G.addEdge( v, n );
          H->calc();

          if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
        }

     // add 2 vertices
      for ( u=0; u<n; u++ )
      {
        H = new Info(G,2);
        H->G.addEdge( u, n );
        H->G.addEdge( u, n+1 );
        H->G.addEdge( n, n+1 );
        H->calc();

        if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
      }

    }
    obsts[tri-1].stop();

    cerr << "level size = " << obsts[tri].size() << nl;

    obsts[tri-1].start();
    while ( pG = obsts[tri-1].next() ) pG->clear();
    obsts[tri-1].stop();
  }

  for ( int fes = 1; fes <= MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    for ( int tri=1; tri<MAX; tri++ )
    {
      obsts[tri].start();
      Info* pG;
      while ( pG = obsts[tri].next() )
      {
        if ( fes == fesBrute( pG->G ) )
           cerr << "triangles: " << tri << nl << pG->G << nl;
      }
      obsts[tri].stop();
    }
  }

  Array< PIterSet<Graph> > FES(MAX*2);  // over declare dim

  cerr << "sorting by FES: ";
  for ( tri=1; tri<MAX; tri++ )
  {
    cerr << tri << ' ';
    obsts[tri].start();
    Info* pG;
    while ( pG = obsts[tri].next() )
    {
      FES[fesBrute(pG->G)].addUnique(&(pG->G));
    }
    obsts[tri].stop();
  }
  cerr << nl;

  for ( fes = 1; fes<MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    cerr << "num = " << FES[fes].size() << nl;
    Graph* pG;

    FES[fes].start();
    //
    while ( pG = FES[fes].next() )
    {
        cerr << *pG << nl;
    }
    //
    FES[fes].stop();
  }

} // main
