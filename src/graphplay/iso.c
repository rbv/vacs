
/* 
 * User interactive graph isomorphism tester.
 */
#include <iostream>
#include "graph/graph.h"

main()
{
   Graph G;
   Graph H;

   cin >> G >> H;

   bool iso = ( G == H );

   if (iso) cout << "yes\n";
   else cout << "no\n";
}
