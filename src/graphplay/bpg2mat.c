#include "stdtypes.h"
#include "general/str.h"
#include "bpg/bpg.h"
#include "graph/graph.h"
#include "bpg/i-o/bpg2graph.h"

int main()
{
    int cnt = 0;
    Operator::initTables( 16 );
    for (;;)
    {
        char buf[400];
        cin.getline( buf, 400 );
        //cout << cin.fail() << " " << cin.bad() << " " << cin.eof() << nl;
        if ( !cin )
            break;
        Str line( buf );
        RBBPG p;
        BPG::fromString( line, p );
        //cerr << p << nl;

        Graph G;
        bpg2graph( p, G );
        //cerr << "size " << G.size() << " order " << G.order() << nl;
        cout << G;
        cnt++;
    }
    cerr << "converted " << cnt << nl;
}
