
/* 
 * Find/count all graphs that are triangle-free
 */
#include <iostream>

#include "graph/graph.h"
#include "array/array.h"
#include "graph/i-o/tapes.h"

static bool triangleFree(const Graph &G)
 {
  vertNum n = G.order();

  int i, j1, j2;

  for (i=0; i<n; i++)
   for (j1=i+1; j1<n-1; j1++)
    {
     if (G.isEdge(i,j1) == false) continue;
     for (j2=j1+1; j2<n; j2++)
      {
       if (G.isEdge(i,j2) == false) continue;
       if (G.isEdge(j1,j2) == true) return false;
      }
    }
  return true;
 }

#ifdef TRAP
#include <signal.h>
#include <setjmp.h>

jmp_buf entry_point; /* place to jump back */

ctrlc()
 {
  signal(SIGINT, ctrlc);
  cerr << "ontrol-c\n";
  longjmp(entry_point,1);
 }
#endif

static int another_file()
 {
  char y_n[40];
  printf("Want another file? ");
  fflush(stdout);
  scanf("%s", y_n);

  switch(y_n[0])
   {
    case 'y':
    case 'Y': return 1;
    default:  return 0;
   }
 }

/*
 * Program to loop through Read's files.
 */
void main()
 { 

  Array<long> cnt(11);
  //
  cnt.fill(0);

//cerr << cnt << nl;

do {
  Tape_graph tape(NO_ISOLATED);

   /*
    * Trap interupt.
    */
#ifdef TRAP
  signal(SIGINT, ctrlc);
  if (setjmp(entry_point)) goto Break_point;
#endif

  while (tape.nextgraph())
   {
     Graph *G = tape.graph();

     vertNum n = G->order();


     if (G->connected() && triangleFree(*G))
      {
//       if (G->size() == n-1) continue;

       cerr << *G;

       cnt[n] = cnt[n]+1;
      }

   }

  } while(another_file());


  cerr << "Counts " << cnt << nl;

  return;

#ifdef TRAP
Break_point:
  cerr << "\n *** Control-C Trap ***\n";
#endif
 }
