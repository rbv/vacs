
/*
 * count all free trees with 10 or fewer vertices.
 */
#include <iostream>
#include "graph/graph.h"
#include "graph/i-o/tapes.h"

#ifdef TRAP
#include <signal.h>
#include <setjmp.h>

jmp_buf entry_point; /* place to jump back */

ctrlc()
 {
  signal(SIGINT, ctrlc);
  cerr << "ontrol-c\n";
  longjmp(entry_point,1);
 }
#endif

static int another_file()
 {
  char y_n[40];
  printf("Want another file? ");
  fflush(stdout);
  scanf("%s", y_n);

  switch(y_n[0])
   {
    case 'y':
    case 'Y': return 1;
    default:  return 0;
   }
 }

int freeCounts[11] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

/*
 * Program
 */
void main()
 { 

  for (int i=1; i<11; i++) cout << freeCounts[i] << ' '; cout << '\n';

do {
  Tape_graph tape(NO_ISOLATED);

   /*
    * Trap interupt.
    */
#ifdef TRAP
  signal(SIGINT, ctrlc);
  if (setjmp(entry_point)) goto Break_point;
#endif


  while (tape.nextgraph())
   {
     Graph *G = tape.graph();

     //cerr << *G;

     vertNum n = G->order();
     vertNum size = G->size();

    if ( size == n-1 && G->connected() == true) freeCounts[n]++;
   }

  } while(another_file());


  for (i=1; i<11; i++) cout << freeCounts[i] << ' '; cout << '\n';
 
  return;

#ifdef TRAP
Break_point:
  cerr << "\n *** Control-C Trap ***\n";
#endif
 }
