
/* 
 * User interactive graph k-"vertex cover" tester.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/vertcover_ga.h"

main()
{
#ifdef FIXEDK
   int k=2;
   //
   cerr << "Checking for a " << k << "-vertex cover\n";
#endif

   int num;

   cin >> num;

   Graph G;

   for (int i=0; i<num; i++)
    { 
     cin >> G;
  
#ifdef FIXEDK
     bool is = vcLazy( G, k );
  
     cerr << i+1 << ' ';
     //
     if (is) cerr << "yes\n"; else cerr << "no\n";
#else
     cerr << i+1 << ' ' << vcLazy( G ) << nl;
#endif

    }

}
