/* 
 * Eliminates isomorphic graphs from a stream of graphs.
 */
#include <iostream>
#include <fstream>

#include "assert.h"
#include "array/array.h"
#include "graph/graph.h"

main(int argc, char* argv[])
{
   aassert(argc == 2);

   ifstream in1(argv[1]);
   if ( in1.fail() ) { cerr << "Can't open " << argv[1] << nl; return -1; }

   int num1;
   in1 >> num1;
   Array<Graph*> aGraph1(num1);

   Graph *G;
   for (int i=0; i<num1; i++)
   {
     G = new Graph();
     in1 >> *G;
     aGraph1[i] = G;
   }

   cerr << "read " << num1 << " graphs\n";

   int count = 0;
   for (i=0; i<num1; i++)
   {
     bool isoFlag = false;
     for (int j=i+1; j<num1; j++)  
     {
       if ( *(aGraph1[i]) == *(aGraph1[j]) )
       {
	 isoFlag = true;
         break;
       }
     }

     if ( isoFlag == false )
     {
       cout << *(aGraph1[i]);
       count++;
     }     

   }

   cerr << "found " << count << " unique graphs\n";

} // main

