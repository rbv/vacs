// precalc vert part for all graphs
// split loops and use streams

#include "findfes6.h"

// MAX is one greater than number of possible triangles
// (or two greater than what k-FES obsts we want)
// 8
// 7
#define PASS9
#define MAX 7

//--------------------------------------------------------------------
//--------------------------------------------------------------------
static bool mainStarted = 0;
#if 0
#define MAXMEM 50000000
static char* mem_base = 0;
static char* mem_pos;
 
void operator delete(void*)
{
}

void *operator new(size_t size)
{
  size |= 3;
  size++;
  //fprintf(stderr, "new %d, %d\n", mem_pos - mem_base, size );
  if ( mem_base == 0 )
  {
    mem_base = malloc( MAXMEM );
    mem_pos = mem_base;
  }
  char* t = mem_pos;
  mem_pos += size;
  if ( mainStarted && ( mem_pos-mem_base > MAXMEM ) )
    { cerr << "out of mem\n"; exit(1); }
  return t;
}
#endif

//--------------------------------------------------------------------
//--------------------------------------------------------------------

void load( Array< PIterSet<Info6> >& obsts )
{
  obsts.resize( MAX );
  bistream is( "/tmp/obsts" );
  for ( int tri=2; tri<MAX; tri++ )
  {
    is >> ((PCollection<Info6>&) obsts[tri] );
    cerr << "  loaded level " << tri << " graphs " << obsts[tri].size() << nl;
  }
  is.close();
}


int main()
{
  mainStarted = 1;
  Array< PIterSet<Info6> > obsts(MAX);

  // start with K3 
  {
  Info6 *G = new Info6(3);
  G->G.addEdge( 0, 1 );
  G->G.addEdge( 0, 2 );
  G->G.addEdge( 1, 2 );
  G->calc();
  obsts[1].add( G );
  }

  int tri;
#ifdef PASS0
  for ( tri=2; tri<MAX-1; tri++ )
#elif defined( PASS1 ) || defined( PASS2 ) || defined( PASS3 )
  load( obsts );
  for ( tri=MAX-1; tri<MAX; tri++ )
#elif defined( PASS9 )
  load( obsts );
  if ( 0 )
#endif
  {
    cerr << "building level " << tri;
    cerr << " from " << obsts[tri-1].size() << " graphs " << nl;
    cerr << "level size is initially " << obsts[tri].size() << nl;
    int num = 0;

    Info6* pG;
    obsts[tri-1].start();
    while ( pG = obsts[tri-1].next() )
    {
      cerr << ++num << nl;
      Graph& G = pG->G;
      Info6 *H;
      int n = G.order();
      int u, v, w;

#if defined(PASS0) || defined(PASS3)
      // add 0 vertices (1, 2 or 3 edges)
      for ( u=0; u<n-2; u++ )
        for ( v=u+1; v<n-1; v++ )
        {
          for ( w=v+1; w<n; w++ )
          {
            if ( G.isEdge(u,w) && G.isEdge(v,w) && G.isEdge(u,v) ) continue;

            H = new Info6(G);
            H->G.addEdge( u, v );
            H->G.addEdge( u, w );
            H->G.addEdge( v, w );
            H->calc();

            if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
          }
        }
#endif
 
#if defined(PASS0) || defined(PASS2)
      // add 1 vertex
      for ( u=0; u<n-1; u++ )
        for ( v=u+1; v<n; v++ )
        {
          H = new Info6(G,1);
          H->G.addEdge( u, v );
          H->G.addEdge( u, n );
          H->G.addEdge( v, n );
          H->calc();

          if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
        }
#endif
 
#if defined(PASS0) || defined(PASS1)
     // add 2 vertices
      for ( u=0; u<n; u++ )
      {
        H = new Info6(G,2);
        H->G.addEdge( u, n );
        H->G.addEdge( u, n+1 );
        H->G.addEdge( n, n+1 );
        H->calc();

        if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
      }
#endif

    }
    obsts[tri-1].stop();

    cerr << "level size = " << obsts[tri].size() << nl;

  }

#ifndef PASS9
  // save so far
  bostream os( "/tmp/obsts" );
  for ( tri=2; tri<MAX; tri++ )
    os << ((PCollection<Info6>&) obsts[tri] );
  os.close();

#endif

#ifdef PASS9

  for ( int fes = 1; fes <= MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    for ( int tri=1; tri<MAX; tri++ )
    {
      obsts[tri].start();
      Info6* pG;
      while ( pG = obsts[tri].next() )
      {
        if ( fes == fesBrute( pG->G ) )
           cerr << "triangles: " << tri << nl << pG->G << nl;
      }
      obsts[tri].stop();
    }
  }

  Array< PIterSet< Info6 > > FES(MAX*2);  // over declare dim

  cerr << "sorting by FES: ";
  for ( tri=1; tri<MAX; tri++ )
  {
    cerr << tri << ' ';
    obsts[tri].start();
    Info6* pG;
    int count = 0;
    while ( pG = obsts[tri].next() )
    {
      FES[fesBrute(pG->G)].addUnique(pG,isoTest);
      //cerr << ++count << nl;
    }
    obsts[tri].stop();
  }
  cerr << nl;

  for ( fes = 1; fes<MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    cerr << "num = " << FES[fes].size() << nl;
    Info6* pG;

    FES[fes].start();
    //
    while ( pG = FES[fes].next() )
    {
        cerr << (pG->G) << nl;
    }
    //
    FES[fes].stop();
  }

#endif

} // main


//-----------------------------------------------------------------
//-----------------------------------------------------------------
