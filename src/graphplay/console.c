/********** MODULE:  CONSOLE.C  which includes cursor.c and echo.c  **********/

/******************************************************************************
 *
 * F U N C T I O N   N A M E : CURSOR.C 
 *
 * Version: 01
 * Edit level: 02
 * Edit date: 7-Jan-92
 *
 ******************************************************************************
 *
 ******************************************************************************
 *
 * Developed by: Michael J. Dinneen
 *
 * Developed for: ANSI standard terminals. (Example: Digital VT100)
 *
 ******************************************************************************
 *
 ******************************************************************************
 *
 *
 * M O D I F I C A T I O N   H I S T O R Y
 *
 * Ver/Edit	  Date		Reason (Problem/Solution)
 * --------  	---------	-------------------------
 * V01/01	17-Nov-88	Released Version 1.
 *
 * V01/02	07-Jan-92	Changed to C++
 *
 ******************************************************************************
 *
 ******************************************************************************
 *
 * F U N C T I O N   D E S C R I P T I O N
 *
 *  Cursor is a function to make screen writing easier.
 *
 *  Function Parameters:
 *
 *	line_col = String containing screen line and column position.
 *		   Should be in the form of "YY;XX" wher YY is line number
 *		   and XX is the column place.  The semicolon is needed!
 *		   If an empty string "" is sent then the current cursor
 *		   position is used for the text.
 *
 *	what     = A integer containing bits that represent special screen
 *		   attributes.
 *                      0   = Nothing.
 *			1   = BOLD
 *			2   = UNDERLINE
 *			4   = BLINK
 *			8   = REVERSE VIDEO
 *                      16  = NORMAL HEIGHT & WIDTH
 *			32  = DOUBLE HEIGHT TOP
 *			64  = DOUBLE HEIGHT BOTTOM
 *			128 = DOUBLE WIDTH
 *                      256 = CLEAR LINE
 *                      512 = CLEAR SCREEN
 *                      1024= CLEAR EOL
 *                      2048= CLEAR EOS
 *		   Note: Any combination of the above are allowed.
 *
 *	text     = Character string that user wants displayed.  Should end with
 *		   an ASCII zero (0).
 *                      
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "console.h"

#define PREFIX putchar(27); putchar('[');
#define CHGLINE putchar(27); putchar('#');


/******************************************************************************
			   --- Function Defined ---
 ******************************************************************************/

void cursor(char *line_row, int what, char *text)
  {

	/* Move cursor to desired position. (if needed) */
	if (line_row[0]>'0') { PREFIX printf(line_row); putchar('H'); }
	
	/* See if screen needs to be erased anywhere. */
	if (what>=256) 
	 { 
	   if (what & CLRLINE) { PREFIX printf("2K"); } /* Entire line */
	   if (what & CLRSCRN) { PREFIX printf("2J"); } /* Entire screen */
	   if (what & CLREOL)  { PREFIX printf("0K"); }	/* End of line */
	   if (what & CLREOS)  { PREFIX printf("0J"); }	/* End of screen */

	   /* Trim off erase bits. */
	   what&=255;
	 }

	/* See if we need to change width or height of line. */
	if (what>=16)
	 {
	   if (what & NORMAL)    { CHGLINE putchar('5'); } /* Normal text */
	   if (what & TOPDHT)    { CHGLINE putchar('3'); } /* Top Double size */
	   if (what & BOTTOMDHT) { CHGLINE putchar('4'); } /* Bottom Double size */
	   if (what & WIDE)      { CHGLINE putchar('6'); } /* Double width only */
		
	   /* Trim off character size bits. */
	   what&=15;
	 }
	
	/* At last, See if we need to add special character attributes. */
	if (what) 
         {
	   PREFIX
	   if (what & BOLD)    { printf(";1"); }	/* Bolding */
	   if (what & UND)     { printf(";4"); }	/* Underlineing */
	   if (what & BLINK)   { printf(";5"); }	/* Blinking */
	   if (what & INVERSE) { printf(";7"); }	/* Reverse Video */
	   putchar('m');
	 }

	   printf(text);		/* Finally, print users text */
	
    	   if (what) { PREFIX putchar('m'); }	/* Unset attributes. */

	   /* Clean out the keyboard buffer. */

	   fflush(stdout);
  }

/******************************************************************************
 *
 * F U N C T I O N   N A M E : ECHO.C
 *
 ******************************************************************************
 *
 ******************************************************************************
 *
 * Developed by: Michael J. Dinneen
 *
 * Developed for: Screen entry (Echo control function).
 *
 ******************************************************************************
 *
 ******************************************************************************
 *
 *
 * M O D I F I C A T I O N   H I S T O R Y
 *
 * Ver/Edit	  Date		Reason (Problem/Solution)
 * --------  	---------	-------------------------
 * V01/01	17-Nov-88	Released Version 1.
 *
 ******************************************************************************
 *
 ******************************************************************************
 *
 * F U N C T I O N   D E S C R I P T I O N
 *
 *   ECHO.C is a terminal entry function.  This function is useful for error
 * free input from the user.  ECHO.C sets up input fields on the screen
 * determined by the parameters passed.  Escape sequences from the keyboard
 * are returned to notify delimiting keystrokes.
 *
 * Format of invocation:  echo(&position,length,&default,&answer,&escseq)
 *	
 * Parameters explained:
 *
 *	&position	= String entered as "RR;CC" that signifies starting
 *			  place for input field. (row,column).
 *
 *	length		= Length of field.  If length<0 then entry must equal
 *			  ABS(length).
 *
 *	&default_a	= Default answer if <PF1> or no entry is given.
 *
 *	&answer		= User's response string.
 *
 *	&escseq		= Integer representing an escape sequence which gets
 *			  set if the arrow keys or <PF1> to <PF4> are pressed.
 *				0 - Nothing.
 *				1 - <PF1>, <Do> keys pressed.
 *				2 - <PF2>, <Help> keys pressed.
 *				3 - <PF3> key pressed.
 *				4 - <PF4> key pressed.
 *				5 - <Left> arrow pressed.
 *				6 - <Down> arrow pressed.
 *				7 - <Right> arrow pressed.
 *				8 - <Up> arrow pressed.
 *
 *	
 ******************************************************************************/

/* #include <stdio.h> -FROM CURSOR */
/* #include "video.h" -FROM CURSOR */
/*
#define SAVECURSOR putchar(27); putchar('7'); 
#define RESTORECURSOR putchar(27); putchar('8');
*/
#define DELETE_CHAR putchar(8); putchar(32); putchar(8);

/******************************************************************************
			 --- Function(s) Defined ---
 ******************************************************************************/

int esclevel1()	/* We have received and <ESC> character. */
 {
   char key;
   int esc2_1(), esc2_2();

   key=getchar();
   switch (key)
    {
     case '[':	/* Prefix bracket found. */
     	{
     	 return esc2_1();
	}
     case 'O':	/* Prefix PF? key identifier found. */
        {
     	 return esc2_2();
        }
     case 'K':	/* IBM left arrow. */
        {
      	 return LEFT;
     	}
     case 'M':	/* IBM right arrow. */
        {
     	 return RIGHT;
        }
     case 'H':	/* IBM up arrow. */
     	{
     	 return UP;
     	}
     case 'P':	/* IBM down arrow. */
     	{
     	 return DOWN;
     	}
     case ';':	/* IBM F1 key. */
     	{
     	 return PF1;
     	}
     case '<':	/* IBM F2 key. */
     	{
     	 return PF2;
     	}
     case '=':	/* IBM F3 key. */
     	{
     	 return PF3;
     	}
     case '>':	/* IBM F4 key. */
     	{
     	 return PF4;
     	}
     default:	/* Unknown escape sequence. */
        {
     	 return 0;
        }
    }
 }

int esc2_1() /* We have received <ESC>[ characters. */
 {
   char key;
   key=getchar();
   switch (key)
    {
     case 'A':	/* Up arrow determined. */
     	{
     	 return UP;
	}
     case 'B':	/* Down arrow determined. */
        {
     	 return DOWN;
        }
     case 'C':	/* Right arrow determined. */
        {
     	 return RIGHT;
        }
     case 'D':	/* Left arrow determined. */
        {
     	 return LEFT;
        }
     default:	/* Unknown escape sequence. */
        {
     	 return 0;
        }
    }
 }

int esc2_2() /* We have received <ESC>O characters. */
 {
   char key;

   /* Key sequences: PF1=<ESC>OP, PF2=<ESC>OQ, PF3=<ESC>OR, and PF4=<ESC>OS */

   key=getchar()-'O';
   if (key>=1 && key<=4) return key; else return 0;
 }


/******************************************************************************
		      --- M A I N   F U N C T I O N ---
 ******************************************************************************/


void echo (char *position, int length, char *default_a, char *answer, int *escseq)
 {
//printf("in echo %s %d %s %s %d\n", 
// 	 position, length, default_a, answer, escseq);

  int i, count, true_len;
  char cstr1[80];


Start:	/* We will need to branch here and retry the input process if any 
           user input problems occur. (Example: too short of an response.) */


  /* Set up entry field parameters. */

  *escseq = 0;
  count = strlen(default_a);
  true_len = abs(length);
  for (i = 0; i < (true_len-count); cstr1[i++] = ' ');
  cstr1[i] = 0;

  /* Paint entry field in reverse video. */

  /* SAVECURSOR */
  cursor(position, INVERSE, default_a);
  cursor("", INVERSE, cstr1);
  /* RESTORECURSOR */
  cursor(position, 0, "");
  
  /* ---  Now to input user's response.  --- */

#if 0

  scanf("%s", cstr1);

  switch (cstr1[0])
   {
    case '<': *escseq = LEFT;
              cstr1[0] = 0;
              break;
    case '>': *escseq = RIGHT;
              cstr1[0] = 0;
              break;
    case '^': *escseq = UP;
              cstr1[0] = 0;
              break;
    case '&': *escseq = DOWN;
              cstr1[0] = 0;
              break;
   }

#else

  for (i = 0; i <= true_len; i++)
    {
	cstr1[i]=getchar();
	switch (cstr1[i])
	 {
	  case 0: ;	/* Start of an IBM extended sequence. */
	  case 27:	/* Start of an ANSI escape sequence found. */
	     	{
		 *escseq = esclevel1();
		 
		 /* See if we received a valid escape sequence. */

		 if (*escseq != 0)
		  {
		     cstr1[i] = 0;
		     i = true_len+1;
		  }
		 else	/* Unrecognizable sequence. */
		  {
		     putchar(7);
		     i--;
		  }
		 break;
		}
          case 10:	/* <LF> key pressed. */
	  case 13:	/* <Return> key pressed. */
		{
		 cstr1[i] = 0;
		 i = true_len + 1; /* Force an exit from our input loop. */
		 break;
		}
	  case 8: ;	/* Backspace key typed. */
	  case 127:	/* Delete character typed. */
		{
		 if (i > 0) 
		  {
		   DELETE_CHAR
		   i -= 2;
		  }
		 else
		  {
		   i--;
		  }
		 break;
		}
	  default:	/* Check for regular character. */
/*
  Comment out the following if System doesn't support single character
  input mode. 
 */
		{
/*		 if (cstr1[i] >= ' ')
		  {
		   putchar(cstr1[i]);
		  }
		 else
		  {
		   putchar(7);
		   i--;
		  }
 */		}
	 }

	/* Make sure we only accept <Return> or an escape sequence after 
	   the input field length has been exceeded. */

	if (i == true_len && cstr1[i] != 0) { DELETE_CHAR; putchar(7); i--; }
    }

#endif

  /* Check and see if we need an exact size of input. */

  count=strlen(cstr1);
  if (length < 0 && true_len > count && count > 0)
    {
      putchar(7);
      cursor("24;20", BOLD+BLINK, "Entry needs to fill up the input field!");
      goto Start ;
    }
  else
    {
      /* Use default answer if nothing entered by user. */

      if (count == 0)
       {
       	strcpy(cstr1, default_a);
        count = strlen(cstr1);
       }

      /* Move final input string into our return answer. */

      strcpy(answer, cstr1);
      answer[count] = 0;

      /* Clear (error) message line 24. */

      cursor("24;1", CLRLINE, "");

      /* Repaint input field in reverse video with new answer. */

      for (i = 0; i < (true_len-count); cstr1[i++] = ' ');
      cstr1[i] = 0;


      /* RESTORECURSOR */
      cursor(position, INVERSE, answer);
      cursor("", INVERSE, cstr1);
    }

 }
