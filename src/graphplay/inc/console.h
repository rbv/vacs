/******************************************************************************
 *
 * H E A D E R   N A M E : console.h
 *
 * Version: 01
 * Edit level: 01
 * Edit date: 12-SEP-88
 *
 ******************************************************************************
 *
 * Developed by: Michael J. Dinneen
 *
 * Developed for: ANSI screen sequences.
 *
 *****************************************************************************/

//
//  Will be replaced with modern techniques! (e.g. X-window stuff)
// 

void cursor(char*, int, char*);
void echo(char*, int, char*, char*, int*);

/* cursor.c attribute constants */
#define BOLD 1
#define UND 2
#define BLINK 4
#define INVERSE 8
#define NORMAL 16
#define TOPDHT 32
#define BOTTOMDHT 64
#define WIDE 128
#define CLRLINE 256
#define CLRSCRN 512
#define CLREOL 1024
#define CLREOS 2048

/* echo.c escape sequences */
#define PF1 1
#define PF2 2
#define PF3 3
#define PF4 4
#define LEFT 5
#define DOWN 6
#define RIGHT 7
#define UP 8
