/* 
 * Eliminates isomorphic graphs from a stream of graphs.
 */
#include <iostream>
#include <fstream>

#include "assert.h"
#include "array/array.h"
#include "graph/graph.h"

static bool filter( const Graph &G )
{
   vertNum n = G.order();

   DegreeSequence Deg(n);
   G.degVec(Deg);

   for (int i=0; i<n; i++)
   {
     if (Deg[i] <= 2) return true;
   }

   for (i = 0; i<n; i++) // biconnected test
   { 
     Graph H(G);
     H.rmNode(i);

     if (!H.connected()) return true;

#if 0
     for (int j=0; j<n-1; j++)  // triconnected
     {
       Graph K(H);
       K.rmNode(j);

       if (!K.connected()) return true;
     }
#endif
   }

   return false;
}

main(int argc, char* argv[])
{
   aassert(argc == 2);

   ifstream in1(argv[1]);
   if ( in1.fail() ) { cerr << "Can't open " << argv[1] << nl; return -1; }

   int num1, num2 = 0;
   in1 >> num1;

   Graph *G;
   for (int i=0; i<num1; i++)
   {
     G = new Graph();
     in1 >> *G;

     if ( filter(*G) ) num2++;
     else cout << *G;

     delete G;
   }

   cerr << num1 << " graphs inputed\n";
   cerr << num2 << " graphs filtered\n";
   cerr << num1 - num2 << " graphs outputed\n";

} // main

