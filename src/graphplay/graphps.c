/* 
 * User interactive graph tester.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/i-o/psspec.h"
#include "general/str.h"

#define DODVI

//Str labelGrph(vertNum i)
Str labelGrph(vertNum i, const void *Obj = 0)
 {
// cerr << "labelGrph returns:" << Str(i) << nl;
   return Str(i);
 }

main()
{
   Graph G;

   int num;

   cin >> num;

#ifdef DODVI

   PSSpec S(175,175,PSSpec::DviSpecial);
   S.setNodeStrFnc( labelGrph );

   cout << "\\documentstyle{article}" << nl
        << "\\special{header=/usr/snurp/mjd/vacs/lib/graph.ps}" << nl
        << "\\setlength{\\evensidemargin}{0in}" << nl
        << "\\setlength{\\oddsidemargin}{0in}" << nl
        << "\\setlength{\\topmargin}{-0.5in}" << nl
        << "\\setlength{\\textwidth}{6.5in}" << nl
        << "\\setlength{\\textheight}{9in}" << nl
        << "\\begin{document}" << nl;

#else

   PSSpec S(400,400);
   S.setNodeStrFnc( labelGrph );

#endif

   for (int i=1; i<=num; i++)
   {
     cin >> G;

#ifdef DODVI
     cout << "\\parbox{240pt}{";
     cout << nl << "Graph " << i 
          << ": (" << G.order() << ',' << G.size() << ")\\\\\n";
#else
     cout << "100 200 translate\n";
#endif

     graph2ps(G,&S);

#ifdef DODVI
     cout << "\\\\[2ex]}\n";  // parbox
     //cout << "\\vfill\n";
     //if (i%2==0) cout << "\\newpage\n";
#else
     cout << "showpage\n";
#endif
   }

#ifdef DODVI
  cout << "\\end{document}\n";
#endif
}
