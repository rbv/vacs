
#include <stdio.h>
#include "graph/graph.h"
#include "console.h"		/* Constants used in function cursor(). */


static void display_adj(vertNum n, bool **adj)
{
  cursor("5;1", 0, "");

/* Display a blank matrix before we ask for entry input. */

  for (int row = 0; row < n; row++)		/* Row loop. */
   {
    cursor("", CLRLINE, "  ");

    for (int column = 0; column < n; column++)	/* Column loop. */
     {
      if (row==column)           cursor("",0,"+"); 
      else if (adj[row][column]) cursor("", INVERSE, "1");
      else		         cursor("", INVERSE, "0");

      putchar(32); putchar(32);
     }
    putchar('\n');
   }      
}

static void get_adj(vertNum n, bool **adj)
 {
  vertNum row, column;		/* Matrix entry position. */
  int    byte_value,			/* Matrix entry value. */
         escseq;			/* Keyboard escape sequence# */

  char answer[4],			/* Response from user. */
       location[8],			/* Cursor position. */
       d_answer[4];			/* Default answer for questions. */

  
  cursor("1;1", CLRSCRN, "\n");

  // while (getchar()!='\n');

  printf("Please enter an adjacency matrix for graph of order %d.\n",n);

  display_adj(n, adj);

/* Now to get each element. */

  row = column = 0;	/* Start at matrix entry [1,1]. */

Row_loop:
  for (; row < n; row++)
   {
    column=row+1;

Column_loop:
    for (; column < n; column++)
     {
      cursor("3;1", 0, "Enter element: ");
      printf("(%d,%d)\n", row + 1, column + 1);

      sprintf(location, "%d;%d", row + 5, column * 3 + 3);

      /* Read in matrix entry while supplying default answer. */

      sprintf(d_answer, "%d", adj[row][column]);

      echo(location, 1, d_answer, answer, &escseq);

      /*
        Check for Restart input flag.
       */
      if (answer[0] == '^') { row = column = 0; goto Row_loop; }
      if (answer[0] == '>') 
       {
        if (column==n-1) row<n-2 ? row++ : 0; 
        column = n-1; goto Column_loop; 
       }
      if (answer[0] == '<') 
       {
        if (column==row+1) row ? row-- : 0; 
        column = row+1; goto Column_loop; 
       }
      if (answer[0] == 12 /* ^L */) { display_adj(n, adj); goto Column_loop; }

      /*
         Check for illegal input.
       */
      if (answer[0] > '1' || answer[0] < '0')
         {
 	   cursor("24;20", BOLD+BLINK, "\7Bad matrix element!  Please retry.");
	   column--;
           display_adj(n, adj);
	   continue;	/* Column loop. */
         }
	
      sscanf(answer, "%d", &byte_value);

      adj[row][column] = adj[column][row] = byte_value;

      /* Now just in case the user has used an arrow key. */

      switch (escseq)
       {
        case PF4:    {
			row = n;		/* Fast exit. */
			goto Row_loop;
		     }
	case DOWN:   {
		   	row = ++row % n;
			goto Row_loop;
		     }
	case UP:     {
		   	row = (--row + n) % n;
			goto Row_loop;
		     }
	case RIGHT:  {
		   	column = ++column % n;
			goto Column_loop;
		     }
	case LEFT:   {
		   	column = (--column + n) % n;
			goto Column_loop;
		     }
       } /* End Switch */

     } /* End Column */

     column = 0;	/* We want to wrap around to first column. */

     display_adj(n, adj);


   } /* End Row */

  cursor("1;1", CLRSCRN, "\n");

  return;
 }

/*************************************************************************/

void Graph::fromUser(vertNum n)
{
  while (n==0)
   {
    int v;
    cursor("1;1", CLRSCRN, "\n");
    printf("How many vertices for inputed graph adjacency matrix? ");
    scanf("%d", &v);
    while (getchar()!='\n');
    n = (vertNum) v;
   }

  if (_order) _deallocate(_adj);
  
  _order = n;
  _adj = _allocate(_order);

  /*
   * Start with no edges as default.
   */
  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++)
      {
       _adj[i][j] = false;
      }

  get_adj(_order, _adj);

}
