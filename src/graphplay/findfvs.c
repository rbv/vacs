

/* 
 * A fixed k-"feedback vertex set" obstruction finder.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"
#include "graph/i-o/tapes.h"

static const int FixedK = 2;

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

bool membership( const Graph &G )
{
     //return fvsFixed( G, FixedK );
     return allFVS_2(G);
}

#ifdef TRAP
#include <signal.h>
#include <setjmp.h>

jmp_buf entry_point; /* place to jump back */

ctrlc()
 {
  signal(SIGINT, ctrlc);
  cerr << "ontrol-c\n";
  longjmp(entry_point,1);
 }
#endif

static int another_file()
 {
  char y_n[40];
  printf("Want another file? ");
  fflush(stdout);
  scanf("%s", y_n);

  switch(y_n[0])
   {
    case 'y':
    case 'Y': return 1;
    default:  return 0;
   }
 }

/*
 * Program to loop through Read's files.
 */
void main()
 { 

do {
  Tape_graph tape(NO_ISOLATED);
  //Tape_graph tape(ALL);

   /*
    * Trap interupt.
    */
#ifdef TRAP
  signal(SIGINT, ctrlc);
  if (setjmp(entry_point)) goto Break_point;
#endif

  while (tape.nextgraph())
   {
     Graph *G = tape.graph();

//     cerr << *G;
//     if ( ! G->connected() ) continue;
//     if ( membership(*G) ) cerr << *G;

     if (minorObstruction( *G, membership )) 
     { 
       cerr << *G << nl;
       if (G->connected())  cerr << "graph is connected\n";
     }
   }

  } while(another_file());

  return;

#ifdef TRAP
Break_point:
  cerr << "\n *** Control-C Trap ***\n";
#endif
 }
