

/* 
 * A fixed k-"vertex cover" obstruction finder.
 */
#include <iostream>
#include <stdlib.h>

#include "graph/graph.h"
#include "graph/algorithm/clique_ga.h"
#include "graph/i-o/tapes.h"

//static const int FixedK = 5;
static int FixedK;

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

bool EBIScheck( const Graph &G )
{
   int sz = G.size();
   if (sz > FixedK) return false;
   Graph G1(G);
   G1.complement();
   int iset = clique(G1);
   return iset+sz <= FixedK;
}

#ifdef TRAP
#include <signal.h>
#include <setjmp.h>

jmp_buf entry_point; /* place to jump back */

ctrlc()
 {
  signal(SIGINT, ctrlc);
  cerr << "ontrol-c\n";
  longjmp(entry_point,1);
 }
#endif

static int another_file()
 {
  char y_n[40];
  printf("Want another file? ");
  fflush(stdout);
  scanf("%s", y_n);

  switch(y_n[0])
   {
    case 'y':
    case 'Y': return 1;
    default:  return 0;
   }
 }

/*
 * Program to loop through Read's files.
 */
void main(int argc, char **argv)
 { 

   FixedK=1;
   if (argc > 1) FixedK = atoi(argv[1]);

do {
  Tape_graph tape(NO_ISOLATED);
  //Tape_graph tape(ALL);

   /*
    * Trap interupt.
    */
#ifdef TRAP
  signal(SIGINT, ctrlc);
  if (setjmp(entry_point)) goto Break_point;
#endif

  while (tape.nextgraph())
   {
     Graph *G = tape.graph();

//     cerr << *G;

     if (G->connected() == false) continue;
     if (minorObstruction( *G, membership )) cerr << *G << nl;

   }

  } while(another_file());

  return;

#ifdef TRAP
Break_point:
  cerr << "\n *** Control-C Trap ***\n";
#endif
 }
