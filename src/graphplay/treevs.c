
/* 
 * Crunch through free trees and find vertex separation.
 */
#include <stdio.h>
#include <iostream>

#include "graph/graph.h"
#include "graph/tree.h"
#include "graph/rtree.h"
#include "graph/algorithm/pathwidth_ga.h"

//#define TESTGENERAL

typedef void (*Callback_FNC)(Tree &T);
//
extern void freeTrees( int, Callback_FNC = 0 );

static Array<unsigned int> counts(3);

void treeFNC(Tree &T)
 {
#if 0
  DegreeSequence deg(T.order());
  T.degVec(deg,1);
  cout << deg << nl;
#endif

  RootedTree R(T);
  //cout << pathwidth(R) << ' ';
  vertNum vs = pathwidth(R)-1;
  counts[vs] = counts[vs] + 1;
  if (vs == 2) cout << R;

#ifdef TESTGENERAL
//  cerr << T;
  Graph G(T);
//  cerr << G;
  vertNum pw = pathwidth(G)-1;
  if (pw!=vs)
   {
     cerr << T;
     cerr << G;
     cerr << "pw=" << pw+1 << ' ' << "vs=" << vs+1 << nl; 
   }
  assert(pw==vs);
#endif

 }

void main(int argc, char *argv[])
{
   int n;

   if (argc==2)
    {
     sscanf(argv[1],"%d", &n);
     //cout << "Processing " << n << "-trees\n";
    }
   else
    {
     cout << "What tree order? "; cout.flush();
     cin >> n; cout << nl;
    }

  counts.fill(0);
  //
  freeTrees(n,treeFNC);
  //
  cout << counts << nl;
}
