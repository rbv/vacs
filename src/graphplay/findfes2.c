#include <iostream>

#include "set/iterset.h"
#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"

// MAX is one greater than number of possible triangles
// (or two greater than what k-FES obsts we want)
#define MAX 7

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

static bool isoTest( const Graph& g1, const Graph& g2 )
{
   return g1 == g2;
}

int main()
{
  Array< PIterSet<Graph> > obsts(MAX);

  // start with K3 
  {
  Graph *G = new Graph(3);
  G->addEdge( 0, 1 );
  G->addEdge( 0, 2 );
  G->addEdge( 1, 2 );
  obsts[1].add( G );
  }

  for ( int tri=2; tri<MAX; tri++ )
  {
    cerr << "building level " << tri << nl;
    int num = 0;

    obsts[tri-1].start();
    Graph* pG;
    while ( pG = obsts[tri-1].next() )
    {
      cerr << ++num << nl;
      Graph& G = *pG;
      Graph *H;
      int n = G.order();
      int u, v, w;

      // add 0 vertices (1, 2 or 3 edges)
      for ( u=0; u<n-2; u++ )
        for ( v=u+1; v<n-1; v++ )
        {
          //if ( ! G.isEdge(u,v) ) continue;

          for ( w=v+1; w<n; w++ )
          {
            if ( G.isEdge(u,w) && G.isEdge(v,w) && G.isEdge(u,v) ) continue;

            H = new Graph(G);
            H->addEdge( u, v );
            H->addEdge( u, w );
            H->addEdge( v, w );

            if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
          }
        }
  
      // add 1 vertex
      for ( u=0; u<n-1; u++ )
        for ( v=u+1; v<n; v++ )
        {
          //if ( ! G.isEdge(u,v) ) continue;

          H = new Graph(G,1);
          H->addEdge( u, v );
          H->addEdge( u, n );
          H->addEdge( v, n );

          if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
        }

     // add 2 vertices
      for ( u=0; u<n; u++ )
      {
        H = new Graph(G,2);
        H->addEdge( u, n );
        H->addEdge( u, n+1 );
        H->addEdge( n, n+1 );

        if ( ! obsts[tri].addUnique(H,isoTest) ) delete H;
      }

    }
    obsts[tri-1].stop();

    cerr << "level size = " << obsts[tri].size() << nl;
  }

  for ( int fes = 1; fes <= MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    for ( int tri=1; tri<MAX; tri++ )
    {
      obsts[tri].start();
      Graph* pG;
      while ( pG = obsts[tri].next() )
      {
        if ( fes == fesBrute( *pG ) )
           cerr << "triangles: " << tri << nl << *pG << nl;
      }
      obsts[tri].stop();
    }
  }

  Array< PIterSet<Graph> > FES(MAX*2);  // over declare dim

  cerr << "sorting by FES: ";
  for ( tri=1; tri<MAX; tri++ )
  {
    cerr << tri << ' ';
    obsts[tri].start();
    Graph* pG;
    while ( pG = obsts[tri].next() )
    {
      FES[fesBrute(*pG)].addUnique(pG);
    }
    obsts[tri].stop();
  }
  cerr << nl;

  for ( fes = 1; fes<MAX; fes++ )
  {
    cerr << "obsts with FES = " << fes << nl;
    cerr << "num = " << FES[fes].size() << nl;
    Graph* pG;

    FES[fes].start();
    //
    while ( pG = FES[fes].next() )
    {
        cerr << *pG << nl;
    }
    //
    FES[fes].stop();
  }

} // main
