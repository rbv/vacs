/* 
 * User interactive graph tester.
 */
#include <iostream>
#include <fstream>

#include "graph/graph.h"
#include "graph/i-o/psspec.h"
#include "general/str.h"


//Str labelGrph(vertNum i)
Str labelGrph(vertNum i, const void *Obj = 0)
 {
   (void) Obj;
// cerr << "labelGrph returns:" << Str(i) << nl;
   return Str(i);
//   return Str(i+1);
 }

main(int argc, char* argv[])
{
   Graph G;

   int num;

   //FILE* file = fopen( "springer.in", "r" );
   //if ( !file ) fatalError( "Could not open springer.in file" );

   if (argc != 2)  argv[1] = "springer.in";
   ifstream in(argv[1]);
   if ( in.fail() ) { cerr << "Can't open " << argv[1] << nl; return -1; }


   in >> num;

   PSSpec S(400,400);
   S.setNodeStrFnc( labelGrph );

   //cout << "(/project/cdf/vacs/lib/graph.ps) run\n";

   for (int i=1; i<=num; i++)
   {
     in >> G;

     cerr << G;

     char okay[80]; okay[0]='n';
     //
     while(okay[0]!='y')
      {
       cout << "100 200 translate\n";
 
       graph2ps(G,&S);
 
       cout << "showpage\n";
 
       cerr << "done?\n"; cerr.flush();

       cin >> okay;

       cout << "%done = " << okay << nl;
      }
   }

}
