

/* 
 * User interactive graph tester.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/algorithm/Family_ga.h"

#define CLIQUE
//
main()
{
   Graph G;

   int num;

   cin >> num;

#ifdef CLIQUE
   cout << "clique\n";
#else
   cout << "chromatic\n";
#endif

for (int i=1; i<=num; i++)
 {
   cin >> G;
//   cout << G << nl;
#ifdef CLIQUE
//   cout << G << nl;
   int c = clique(G);
   cout << i << ' ' << c << nl;
#else
   int c = chromatic(G);
   cout << i << ' ' << c << nl;
#endif
 }
}
