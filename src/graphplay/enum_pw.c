/* Generate graphs using geng, and compute and tally their pathwidths.
 *
 * The pathwidth algorithm is a modification of the one in src/graph/algorithm/pw.c
 * to use NAUTY's graph format (only difference is that.
 *
 * The algorithm used for pathwidth is the one here:
 * http://www.sagemath.org/doc/reference/sage/graphs/graph_decompositions/vertex_separation.html
 * with some additional optimisations.
 */

#include "gtools.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <algorithm>

#include "../graph/algorithm/pw.c"

extern "C" void geng_main(int argc, const char *argv[]);

// [ order ][ pathwidth ][ edges ]
static unsigned long counts[MAXVERT+1][MAXVERT][MAXEDGES] = {};

extern "C"
{
   int prune( graph *g, int n, int maxn )
   {
      int edges = 0;
      for (int k = 0; k < n; k++)
      {
         edges += POPCOUNT( g[k] );
      }
      edges /= 2;

      PWGraph G( n, g );
      counts[ n ][ G.pathwidth() ][ edges ]++;
      return 0;
   }
}

int main( int argc, char *argv[] )
{
   int geng_argc;
   const char *geng_argv[10];

   geng_argv[0] = "enum_pw";
   geng_argv[1] = "-u";
   geng_argv[2] = NULL;
   geng_argc = 2;

/*
   if ( argc < 2 )
   {
      fprintf( stderr, "Usage:  enum_pw n [res/mod]\n" );
      return 1;
   }
*/

   int maxn = -1, temp = -1;
   argc--;
   argv++;
   while ( argc )
   {
      if ( maxn == -1 && sscanf( *argv, "%d", &temp ) == 1 )
         maxn = temp;

      geng_argv[ geng_argc++ ] = *argv;
      argv++;
      argc--;
   }
   geng_argv[geng_argc] = NULL;

   geng_main( geng_argc, geng_argv );

   for (int n = 1; n <= maxn; n++)
   {
      printf( "----------------------------------------------------------------------------------------\n", n );
      printf( "n = %d\n\t", n );
      for (int pw = 0; pw < maxn; pw++)
         printf( "pw=%d\t", pw );
      printf( "\n" );
      for (int m = 0; m <= n * (n - 1) / 2; m++)
      {
         printf( "m=%2d\t", m );
         for (int pw = 0; pw < maxn; pw++)
            printf( "%d\t", counts[n][pw][m] );
         printf( "\n" );
      }
      printf( "sums:\t" );
      for (int pw = 0; pw < maxn; pw++)
      {
         int sum = 0;
         for (int m = 0; m <= n * (n - 1) / 2; m++)
            sum += counts[n][pw][m];
         printf( "%d\t", sum );
      }
      printf( "\n" );

   }
   return 0;
}
