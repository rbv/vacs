
/* 
 * User interactive vertex separation of trees.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/stdgraph2.h"
#include "graph/tree.h"
#include "graph/rtree.h"
#include "graph/algorithm/pathwidth_ga.h"

//#define READTREE

main()
{
   int num;

   cin >> num;
//cerr << "to read " << num << " trees\n";

#ifdef READTREE
   AdjLists G;
#else
   Graph G;
#endif

   for (int i=0; i<num; i++)
    { 
     cin >> G;
  
     //cerr << G;

     Tree T(G);	// spanning tree at vertex 0 (or adjlist conversion)

     //cerr << T;

     RootedTree RT(T);

     //cerr << RT;

     cerr << i+1 << ' ' << pathwidth( RT ) << nl;

    }

}
