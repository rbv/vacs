// precalc vert part for all graphs
// split loops and use streams

#include <iostream>
#include <stdlib.h>

#include "general/stream.h"
#include "set/iterset.h"
#include "isomorphism/iso_graph.h"
#include "graph/graph.h"
#include "graph/algorithm/fvs_ga.h"
#include "graph/algorithm/distance_ga.h"

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Iso1
{
   vertNum vertices;
   vertNum edges;
   DegreeSequence degSeq;

public:
   void calc( const Graph& G );
   bool operator==( const Iso1& ) const;
};
inline void Iso1::calc( const Graph& G )
{
  vertices = G.order();
  edges = G.edges();
  degSeq.resize( vertices );
  G.degVec( degSeq, 1 );
}
inline bool Iso1::operator==( const Iso1& i ) const 
{
  bool r = true;
  if ( vertices != i.vertices ||
       edges != i.edges ||
       degSeq != i.degSeq ) r =  false;
  return r;
}

struct Iso2
{
   VertPartition vertPart;
   DistanceMatrix distMat;
public:
   void calc( const Graph& G );
};
inline void Iso2::calc( const Graph& G )
{
  distMat.setDim( G.order() );
  dist_mat( G, distMat );
  vertPart.init( G.order(), distMat );
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------

struct Info6
{
  Info6() {}
  Info6(int i) : G(i) {}
  Info6(const Graph& g) : G(g) {}
  Info6(const Graph& g, int i ) : G(g,i) {}
  Graph G;
  Iso1 iso1;
  Iso2 iso2;
  void calc();
  void clear();
};

inline void Info6::calc()
{
  iso1.calc(G);
  iso2.calc(G);
}

inline void Info6::clear()
{
  iso1.degSeq.resize(0);
  iso2.distMat.setDim(0);
  iso2.vertPart.class_num.resize(0);
  iso2.vertPart.class_cnt.resize(0);
  iso2.vertPart.class_rep.setDim(0);
}

extern bool 
       minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ));

inline static bool isoTest( const Info6& g1, const Info6& g2 )
{
   bool r= true;
   if ( ! ( g1.iso1 == g2.iso1 ) ) r =  false;
   else r =  ::isomorphic( g1.iso2.distMat, g2.iso2.distMat, g1.iso2.vertPart, g2.iso2.vertPart, noBoundary );
  return r;
}


//--------------------------------------------------------------------
//--------------------------------------------------------------------

inline bistream& operator>>( bistream& s, Iso1& p )
{
   s >> p.vertices >> p.edges >> p.degSeq;
   return s;
}  
   
inline bostream& operator<<( bostream& s, const Iso1& p )
{
   s << p.vertices << p.edges << p.degSeq;
   return s;
}  

inline bistream& operator>>( bistream& s, Iso2& p )
{
   s >> p.vertPart >> p.distMat;
   return s;
}  
   
inline bostream& operator<<( bostream& s, const Iso2& p )
{
   s << p.vertPart << p.distMat;
   return s;
}  

inline bistream& operator>>( bistream& s, Info6& p )
{
   s >> p.G >> p.iso1 >> p.iso2;
   return s;
}  
   
inline bostream& operator<<( bostream& s, const Info6& p )
{
   s << p.G << p.iso1 << p.iso2;
   return s;
}  

inline bostream& operator<<( bostream& o, const PCollection<Info6>& C )
{
   o << C.size();
   PCollectionIter<Info6> I( C );
   Info6* p;
   while ( (p = I()) != C.emptyFlag() )
      o << *p;
   return o;
}

inline bistream& operator>>( bistream& o, PCollection<Info6>& C )
{
   PCollectionIter<Info6> I( C );
   int size;
   o >> size;
   Info6* p;
   for (int i=0; i<size; i++ )
   {
     p = new Info6;
     o >> *p;
     C.add( p );
   } 
   return o;
}

