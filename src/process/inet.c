
/*tex

\file{inet.c}
\path{src/process}
\title{Library routines for connecting processes between hosts}
\classes{}
\makeheader

xet*/

//#define _SOCKADDR_LEN

#include <stdlib.h>
//#include <sysent.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#if 0 || defined(CRAY)
#include <iostream>
#include <string.h>
#endif
#include <strings.h>

#include "stdtypes.h"

// returns listen fd for server (and uses/modifies port number)
//
int serverListen( char *hostname, int hsize, int &portNum )
{
  //char hostname[STRMAX];
  struct sockaddr_in saddr;
  struct sockaddr *psaddr = (struct sockaddr *) &saddr;

//#if defined(SUNPRO) || defined(CRAY) || defined(__xlC__)
//  struct sockaddr *cpsaddr = (struct sockaddr *) &saddr;
//#else
  const struct sockaddr *cpsaddr = (const struct sockaddr *) &saddr;
//#endif

  struct hostent *hp;

//#if defined(CRAY) || defined(__xlC__) || defined(__DECCXX)
  bzero( (char*) &saddr, sizeof(saddr) );
//#else
//  bzero( &saddr, sizeof(saddr) );
//#endif
  saddr.sin_family = AF_INET;
	
  if ( gethostname( hostname, hsize ) ) {
    perror( "gethostname failed." );
    abort();
  }

  if ( !(hp = gethostbyname( hostname )) ) {
    cerr << "gethostname failed, h_errno = " << h_errno << "\n";
    abort();
  }
//#if defined(CRAY) || defined(__xlC__) || defined(__DECCXX)
  bcopy( hp->h_addr, (char*) &saddr.sin_addr, hp->h_length );
//#else
//  bcopy( hp->h_addr, &saddr.sin_addr, hp->h_length );
//#endif

  saddr.sin_port = htons(portNum);

  /*
   * Now try to create a socket.
   */
  int s = socket( AF_INET, SOCK_STREAM, 0 );
  aasserten( s != -1 );

  aasserten( bind(s, cpsaddr, sizeof(saddr)) == 0 ); 

  if ( portNum == 0 ) 
  {
    socklen_t slen = sizeof(saddr);
    aasserten( getsockname(s, psaddr, &slen) == 0 );
    portNum = ntohs( saddr.sin_port );
  }

  aasserten( listen(s, 5) == 0 );  // allow a backlog of 5 incoming requests

  return s;
}

// returns client's connection fd
//
int clientConnect( const char *hostname, int portNum )
{
  struct sockaddr_in saddr;
//#if defined(CRAY) || defined(__xlC__) || defined(__DECCXX)
//  struct sockaddr *cpsaddr = (struct sockaddr *) &saddr;
//#else
  const struct sockaddr *cpsaddr = (const struct sockaddr *) &saddr;
//#endif
  struct hostent *hp;
 
//#if defined(CRAY) || defined(__xlC__) || defined(__DECCXX)
  bzero( (char*) &saddr, sizeof(saddr) );
//#else
//  bzero( &saddr, sizeof(saddr) );
//#endif
  saddr.sin_family = AF_INET;
  
//#if defined(CRAY) || defined(__xlC__) || defined(__DECCXX)
  hp = gethostbyname( (char*) hostname );
  if ( !hp )
  {
      cerr << "Could not gethostbyname(" << hostname << ")! Errno = " << errno << nl;
      exit(18);
  }
  bcopy( hp->h_addr, (char*) &saddr.sin_addr, hp->h_length );
//#else
//  hp = gethostbyname( hostname );
//  if ( !hp ) clientConnectAbort();
//  bcopy( hp->h_addr, &saddr.sin_addr, hp->h_length );
//#endif
  saddr.sin_port = htons(portNum);

  // make the connection
  //
  int s = socket( AF_INET, SOCK_STREAM, 0 );
  aasserten( s != -1);

  if( connect(s, cpsaddr, sizeof(saddr)) != 0 )
  {
    cerr << "Could not connect! Errno = " << errno << nl;
    exit(1);
  }

  return s;
}

// new connection for server from a client
//
int serverAccept(int listenfd) // vanilla for now
{
  int fd = accept( listenfd, 0, 0);
  aasserten( fd >= 0 ); 
  return fd;
}

