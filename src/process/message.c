
/*tex

\file{message.c}
\path{src/process}
\title{Filesystem message client/server implementation}
\classes{Message}
\makeheader

xet*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <fstream>

#include "vacs/message.h"
#include "vacs/filesystem.h"

//-------------- Client and server stuff ----------------

bool Message::getServerNames( char* server, int &portNum ) 
{
   char path[STRMAX];
   strcpy( path, getenv("VACS") );
   strcat( path, "/fam/" );
   strcat( path, getenv("VACS_PROB") );
   strcat( path, "/_vacs" );

   ifstream in(path);
   if ( in.fail() ) return false;

   in >> server >> portNum;
   in.close();

//cerr << "Message::getServerNames: " << server << ' ' << portNum << nl; 

   return true;
}

bool Message::setServerNames( const char* server, int portNum, 
	    	     ofstream& out ) 
{
   char path[STRMAX];
   strcpy( path, getenv("VACS") );
   strcat( path, "/fam/" );
   strcat( path, getenv("VACS_PROB") );
   strcat( path, "/_vacs" );

   if ( fileExists(path) ) return false;
   out.open(path, ios::out | ios::trunc);

   if ( out.fail() ) return false;

   out << server << ' ' << portNum << nl;
   out.flush();

   return true;
}

void Message::closeServer() 
{
   char path[STRMAX];
   strcpy( path, getenv("VACS") );
   strcat( path, "/fam/" );
   strcat( path, getenv("VACS_PROB") );
   strcat( path, "/_vacs" );

   if ( unlink( path ) ) cerr << "_vacs server file already removed? errno=" << errno << nl;
}

//------------------ Just client stuff -----------------------

int Message::getDispatchFD()
{
    char server[STRMAX];
    int portNum;

    //getServerPathName( server );
    getServerNames( server, portNum );
    cerr << "getDispatchFD: server=" << server << " port=" << portNum << nl;

    int fd = clientConnect( server, portNum );
    if ( fd < 0 )
    {
        int e = errno;
	cerr << "Could not connect to dispatch server " << server << ":" << portNum << " errno=" << e << nl;
        exit(1);
    }
    cerr << "Connected dispatch server on fd=" << fd << nl;

    return fd;
}

