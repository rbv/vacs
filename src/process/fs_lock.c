
/*tex

\file{fs_server.c}
\path{src/process}
\title{Filesystem lock general client/server implementation}
\classes{FilesystemLock}
\makeheader

xet*/

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>

#include "vacs/fs_lock.h"
#include "vacs/filesystem.h"

//-------------- Client and server stuff ----------------

bool FilesystemLock::getServerNames( char* server, int &portNum ) 
{
   char path[STRMAX];
   strcpy( path, getenv("VACS") );
   strcat( path, "/fam/" );
   strcat( path, getenv("VACS_PROB") );
   strcat( path, "/_lock" );

   //ifstream in(path);
   ifstream in;
   if (in.fail()) aasserten(false);
   in.open(path,ios::in);
   if (in.fail())
   {
       cerr << "Couldn't open lock server file " << path << nl;
       in.close();
       return false;
   }

   in >> server >> portNum;
   in.close();
   return true;
}

bool FilesystemLock::setServerNames( const char* server, int portNum, 
	    	     ofstream& out ) 
{
   char path[STRMAX];
   strcpy( path, getenv("VACS") );
   strcat( path, "/fam/" );
   strcat( path, getenv("VACS_PROB") );
   strcat( path, "/_lock" );

   //out.open(path,ios::noreplace+ios::out);
   out.open(path,ios::out);

   if ( out.fail() ) return false;

   out << server << ' ' << portNum << nl;
   out.flush();

   return true;
}

void FilesystemLock::closeServer() 
{
   char path[STRMAX];
   strcpy( path, getenv("VACS") );
   strcat( path, "/fam/" );
   strcat( path, getenv("VACS_PROB") );
   strcat( path, "/_lock" );

   aasserten( !unlink( path ) );
}

void FilesystemLock::spawnServer()
{
   Filename exeDir = FileManager::executableDirectory();
   const char *locker = exeDir.name("VACSlocker");

   cout.flush();
   cerr.flush();

   int pid;
   if ( ( pid = fork() ) == 0 )
   {
      // close everything, backgrounding it
      for ( int tfd = maxFile() - 1; tfd >= 0; tfd-- ) 
         close( tfd );

      execl(locker, locker, NULL);

      cerr << "Couldn't run " << locker << nl;
      aasserten( false );
   }
   if ( pid == -1 )
      aasserten( false );
}

//------------------ Just client stuff -----------------------

FilesystemLock::FilesystemLock()
{
    char server[STRMAX];
    int portNum;

    //getServerPathName( server );
    if ( !getServerNames( server, portNum ) )
    {
        usleep(10000);
        log() << "Automatically opening locker\n";
        cerr << "Automatically running VACSlocker. You will have to kill it manually!\n";
        spawnServer();
        sleep(1);  // race condition
        if ( !getServerNames( server, portNum ) )
           asserten(false);
    }

    fd = clientConnect( server, portNum );
    if (fd < 0)
    {
        int e = errno;
        cerr << "Could not connect to lock server " << server << ':' << portNum << " errno=" << e << nl;
        exit(1);
    }
}

bool FilesystemLock::request( const FS_RequestType rType )
{
//cerr << "FilesystemLock::request for " << fd << "\n";

  char buf[1];  	// 1 byte protocal

  buf[0] = uchar(rType);

//cerr << fd << " writing " << rType << nl;
  aasserten( write(fd, buf, 1) == 1 );
//cerr << "after write / before read " << nl;

  aasserten( read(fd, buf, 1) == 1);	
//cerr << fd << " read in " << rType << nl;

  // Our request is confirmed if we get back the same thing we send.
  //
  return ( buf[0] == uchar(rType) );
}
