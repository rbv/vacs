
/*tex

\file{fs_server.c}
\path{src/process}
\title{Filesystem lock server and work server implementations.}
\classes{}
\makeheader

xet*/

//#include <math.h>
#ifndef _ABS
#define _ABS(x) ((x) < 0 ? -(x) : (x))
#endif
#include "vacs/fs_server.h"
#include "general/system.h"
#include "family/probinfo.h"

#ifdef CRAY
#include "general/stream.h"
#endif

// for inet_ntoa cerr thingy below
//
#include <netinet/in.h>
#include <arpa/inet.h>


ostream &operator<< (ostream &os, const Client &cl) {
 return os << cl.fd << '@' <<
     inet_ntoa( CAST(cl.peer, sockaddr_in).sin_addr );
}


//------------------ Just lock server stuff -----------------------

void LockList::clientAdd(int fd)
{
 if (fd>_maxfd) _maxfd = fd;

 Client tmp;
 tmp.fd = fd;

 socklen_t peerSize = sizeof(sockaddr);

 aasserten( getpeername( fd, &tmp.peer, &peerSize ) == 0 );
 //tmp.uid = uid;

 cerr << "Connected to host: " << 
	inet_ntoa( CAST(tmp.peer, sockaddr_in).sin_addr ) << nl;

 aassert( uniqueInsert(tmp) );
}

void LockList::clientDel(int fd)
{
  // for now we just delete from client list and hope that no
  // locks are still allocated to this client.
  //
  aassert( remove( fd ) );

  char ctmp[1];

  // Check for rude clients.
  //
  if ( _readers.getBit(fd) ) { ctmp[0]=USysR; request( ctmp, 1, -fd ); }
  //else
  if ( _writeLock == fd ) { ctmp[0]=USysW; request( ctmp, 1, -fd ); }
  //else
  //if ( _cmmdLock == fd ) { ctmp[0]=USysC; request( ctmp, 1, -fd ); }

  if (fd == _maxfd) _maxfd--; // not correct but update this way anyway. 
}


static void ackRequest( int fd, FS_RequestType req )
{
  char ctmp[1];
  ctmp[0] = req;
  
  aasserten( write( fd, ctmp, 1 ) );
  //cerr << "ackRequest to " << fd << " with " << req << nl;
}

#define ACK_REQ { aasserten( write( fd, buf, nread ) ); \
				 /* cerr << "sent back ack\n"; */}
#define NOACK_REQ { ackRequest(fd, NoAck); /* cerr << "NoAck sent back\n"; */}

// if fd < 0 then client -fd has died but needs to relequesh a lock.
//
void LockList::request(const char *buf, int nread, int fd)
{
aassert(nread==1); // single character commands for now.
//cerr << "ClientList::request( *buf=" << ((int) buf[0]) << ", fd = " << fd << " )\n";

 int ackfd;

 switch (buf[0])
 {
  case LSysR:
  {
    if ( _writeLock==-1 && _writeReqQueue.isEmpty() ) 	
    { 
      _readers.setBit(fd); 
      _readLocks++;
      ACK_REQ 
    }
    else 
    {
      if (_writeLock==fd) { _writeLock=-1; NOACK_REQ }	// error
      else _readReqQueue.enque(fd);
    }

    break;
  }

  case USysR:
  {
    aassert( _readers.getBit(_ABS(fd)) );

    _readers.clearBit(_ABS(fd));
    _readLocks--;

    if ( _readLocks == 0 )
    {
     if ( _writeReqQueue.deque( ackfd ) )
     { 
       _writeLock = ackfd;
       ackRequest( ackfd, LSysW );
     }
    } // end of readLocks == 0
    
    if (fd>0) ACK_REQ
    else cerr << "fd " << _ABS(fd) << " was SysR rude\n";
    break;
  }
 
  case LSysW:
  {
    if (_writeLock==fd) { _writeLock=0; NOACK_REQ break; } // error

    if ( _writeLock==-1 && _readLocks==0 ) 	
    { 
      _writeLock = fd;
      ACK_REQ 
    }
    else _writeReqQueue.enque(fd);

    break;
  }

  case USysW:
  {
    aassert( _writeLock == _ABS(fd) );
    aassert( _readLocks == 0 );

    if ( _writeReqQueue.deque( ackfd ) )
    { 
       _writeLock = ackfd;
       ackRequest( ackfd, LSysW );
    }
    else if ( _readReqQueue.deque( ackfd ) )
    {
     _writeLock = -1;

     do  // acknowledge all read requests
     {
       _readers.setBit(ackfd);
       ackRequest( ackfd, LSysR );
       _readLocks++;
     } while ( _readReqQueue.deque( ackfd ) );

    }
    else _writeLock = -1;
    
    if (fd>0) ACK_REQ
    else cerr << "fd " << _ABS(fd) << " was SysW rude\n";

    break;
  }

#if 0
  case LSysC:
  {
    if ( _cmmdLock == -1 ) 	
    { 
      _cmmdLock = fd;
      ACK_REQ 
    }
   else _cmmdReqQueue.enque(fd);

    break;
  }

  case USysC:
  {
    aassert( _cmmdLock == _ABS(fd) );

    if ( _cmmdReqQueue.deque( ackfd ) )
    { 
       _cmmdLock = ackfd;
       ackRequest( ackfd, LSysC );
    }
    else _cmmdLock = -1;
    
    if (fd>0) ACK_REQ
    else cerr << "fd " << _ABS(fd) << " was SysC rude\n";

    break;
  }
#endif

  default: aassert(false);
 }

}

void LockList::dump( ostream &out ) const
{
 out << "_readLocks = " << _readLocks << " _writeLock = " << _writeLock << nl;
//        " _cmmdLock = " << _cmmdLock << nl;

 //out << "_readReqQueue = " << _readReqQueue << nl;
 //out << "_cmmdReqQueue = " << _cmmdReqQueue << nl;
 //out << "_writeReqQueue = " << _writeReqQueue << nl;
}

//------------------ Just work server stuff -----------------------

// change to #if 0 when making the VACSman locker
//
#if 1
//#include "vacs/filesystem.h"
#include "search/graph_db.h"
#include "search/searchnode.h"
#include "search/proof_db.h"
#include "search/search.h"

#ifdef FST
FreeStoreDefn( GraphDictItem )
long long SkipListNode< NodeNumber, GraphDictItem >::newCount = 0;
long long SkipListNode< NodeNumber, GraphDictItem >::delCount = 0;
long long SkipListNode< int, Client >::newCount = 0;
long long SkipListNode< int, Client >::delCount = 0;
#endif

WorkList::WorkList()
   : _doing( FD_SETSIZE )
{ 
   _maxfd = -1;
   _workCount = 0;
   _numWorkers = 0;
   _assignedCount = 0;
   _jobsPerWorker = 5;
   //_doing.fill( Array<NodeNumber>() );
   _workersIdleTime = _idleStartTime = 0.0;
}

void WorkList::readConfig()
{
   _jobsPerWorker = ProbInfo::getInt(ProbInfo::WorkerQueue);
   cerr << "Dispatcher: jobs per worker = " << _jobsPerWorker << nl;
}

void WorkList::send2worker( int w_fd, NodeNumber n )
{
   bostream worker;
   worker.openOnFD( w_fd );

   GraphDictItem item;
   aassert( _graphDict.search( n, item ) );

   worker << n << item.G;

   //cerr << "send2worker(" << w_fd << ',' << n << ")\n";

   // update status
   //
   if ( _doing[w_fd].size() == 0 )
      _workCount++;
   _assignedCount++;
   _doing[w_fd].append( n );
   worker.flush();
   worker.detach();

   return;
}

void WorkList::doUnknownNode( NodeNumber node, const RBBPG& graph )
{
   GraphDictItem item;
   item.n = node;
   item.G = graph;

   aassert( _graphDict.uniqueInsert( item ) );
   _unknownQueue.enque( node );

   distributeWork();
}

void WorkList::clientFinishedJob( int fd )
{
   //cerr << "*** " << _doing[fd] << _graphDict << nl;
   aassert( _doing[fd].size() );

   NodeNumber node = _doing[fd][0];
   // pop front element
   _doing[fd].shiftAndResizeLeft( 0, 1 );
   aassert( _graphDict.remove( node ) );

   _assignedCount--;
   if ( ! _doing[fd].size() )
      _workCount--;
   _workQueue.enque( fd );

   distributeWork();
}

void WorkList::distributeWork()
{
   while ( _unknownQueue.size() && _workQueue.size() )
   {
      int fd;
      _workQueue.deque( fd );
      NodeNumber node;
      _unknownQueue.deque( node );
      send2worker( fd, node );
   }

   checkWhetherIdling();
}

void WorkList::checkWhetherIdling() const
{
   if ( _idleStartTime != 0.0 )
   {
      if ( ! workersWaiting() )
      {
         // idling ended
         _workersIdleTime += System::seconds() - _idleStartTime;
         _idleStartTime = 0.0;
      }
   }
   else
   {
      if ( workersWaiting() )
         // idling started
         _idleStartTime = System::seconds();
   }
}

double WorkList::workersIdleTime() const
{
   double ret = _workersIdleTime;
   if ( _idleStartTime != 0.0 )
      ret += System::seconds() - _idleStartTime;
   return ret;
}

void WorkList::clientAdd( int fd )
{
   if ( fd > _maxfd ) _maxfd = fd;
   _numWorkers++;

   Client tmp;
   tmp.fd = fd;

   socklen_t peerSize = sizeof(sockaddr);
   aasserten( getpeername( fd, &tmp.peer, &peerSize ) == 0 );

   cerr << "Adding client " << tmp << nl;

   aassert( uniqueInsert(tmp) );

   for (int i = 0; i < _jobsPerWorker; i++)
   {
      _workQueue.enque( fd );
   }

   distributeWork();
}

void WorkList::clientDel( int fd )
{
   // for now we just delete from client list and hope that no
   // locks are still allocated to this client.
   //
   aassert( remove( fd ) );
   _numWorkers--;

   // Unassign jobs sent to the worker
   //
   if ( _doing[fd].size() )
   {
      _workCount--;

      GraphDictItem item;
      for (int i = 0; i < _doing[fd].size(); i++)
      {
         NodeNumber node = _doing[fd][i];
         _assignedCount--;
         aassert( _graphDict.grab( node, item) );
         doUnknownNode( node, item.G );
      }

      cerr << "worker " << fd << " was rude!\n";

      _doing[fd].resize( 0 );
   }

   while ( _workQueue.remove( fd ) )
      ;
   checkWhetherIdling();

   if (fd == _maxfd) _maxfd--; // not correct but update this way anyway. 
}

void WorkList::dump( ostream &out ) const
{
   int assignCheck = 0;
   for (int i = 0; i < _doing.size(); i++)
      assignCheck += _doing[i].size();
   aassert( assignCheck == _assignedCount );

   out << "working count = " << _workCount
       << " assigned nodes = " << _assignedCount
       << " node queue = " << _unknownQueue.size()
       << " worker queue = " << _workQueue.size()
       << nl;
   //out << "node queue = " << _unknownQueue << nl;
   out.flush();
}

void WorkList::backDoorDump( bostream &out ) const
{
   // use ostream instead of bostream operator<< overloads
   ostream &aout = *out.ostreamCast();

   aout << "work count = " << _workCount
        << " assigned nodes = " << _assignedCount
        << " node queue = " << _unknownQueue.size()
        << " work queue = " << _workQueue
        << nl;

   const ClientNode  *pClientNode = head();
   //
   while ( pClientNode )
   {
      int clifd = pClientNode->object().fd;

      aout << "Client " << pClientNode->object() << ":" << nl;

      Array<NodeNumber> &jobs = _doing[clifd];
      for (int i = 0; i < jobs.size(); i++)
      {
         aout << " working on node " << jobs[i] << nl; 
      }

      pClientNode = pClientNode->next();

   } // while another client

   aout << '$'; // end of stream

   aout.flush();
}

#endif
