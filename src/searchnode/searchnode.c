 
// -------------------------------------------
// -------------- searchnode.c ---------------
// -------------------------------------------

/*tex
 
\file{searchnode.c}
\path{src/bpg}
\title{Definition of search node class}
\classes{SearchNode}
\makeheader
 
xet*/

#include <iostream>
#include "search/searchnode.h"

SearchNode::SearchNode()
{
#ifndef NDEBUG
    memset( this, 0, sizeof( SearchNode ) );
#endif
   _flags = 0;
   _depth = -1;
   _state._minimalProof = -1;
}

SearchNode::~SearchNode()
{
}

SearchNode::SearchNode( const SearchNode& s )
{
   memcpy( this, &s, sizeof( SearchNode ) );
}

void SearchNode::operator=( const SearchNode& s )
{
   memcpy( this, &s, sizeof( SearchNode ) );
}


SearchNode::SearchNode( NodeNumber parent, Operator extendByOp, short depth )
{
#ifndef NDEBUG
    memset( this, 0, sizeof( SearchNode ) );
#endif
   _parent = parent;
   _op = extendByOp;
   _depth = depth;
   status( Unknown );
   membership( NotComputed );
   growStatus( Growable );
   _state._minimalProof = -1;  // hack the field
}


SearchNode SearchNode::createRootNode()
{
   SearchNode s;
   s._parent = NodeNumber();
   s._op = Operator();
   s._depth = 0;
   s.status( Minimal );
   s.membership( InF );
   s.growStatus( Growable );
   return s;
}


const char* ProofRef::statusAsString() const
{
   const char* s;
   switch ( status() )
   {
      case Minimal:    s = "minimal"; break;
      case Nonminimal: s = "nonminimal"; break;
      case Unknown:    s = "unknown"; break;
      case Irrelevant: s = "irrelevant"; break;
      default: aassert(false);
   }
   return s;
}


const char* SearchNode::growStatusAsString() const
{
   const char* s;
   switch ( growStatus() )
   {
      case Grown:       s = "grown"; break;
      case Growable:    s = "growable"; break;
      case NotGrowable: s = "not growable"; break;
      default: aassert(false);
   }
   return s;
}


const char* SearchNode::membershipAsString() const
{
   const char* s;
   switch ( membership() )
   {
      case InF:            s = "in family"; break;
      case NotInF:         s = "not in family"; break;
      case NotComputed:    s = "not computed"; break;
      default: aassert(false);
   }
   return s;
}

char ProofRef::statusAsChar() const
{
   char s;
   switch ( status() )
   {
      case Minimal:    s = 'M'; break;
      case Nonminimal: s = 'N'; break;
      case Unknown:    s = 'U'; break;
      case Irrelevant: s = 'I'; break;
      default: aassert(false);
   }
   return s;
}

int SearchNode::proofReference() const
{
   return _state._minimalProof;
}
