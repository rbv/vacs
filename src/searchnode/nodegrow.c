
// -------------------------------------------
// --------------- nodegrow.c ----------------
// -------------------------------------------

/*tex
 
\file{nodegrow.c}
\path{src/search}
\title{Grow functions for search}
\classes{}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "search/searchutil.h"

#include "array/array.h"
#include "general/gtimers.h"
#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "search/search.h"
#include "search/proof_db.h"
#include "search/searchnode.h"
#include "search/searchutil2.h"
#include "vacs/runinfo.h"
#include "family/family.h"

#define DBG(s) { cerr << s; }
#define DBG1(s)
//define DBG(s)

#if 1
#define TIMER_START(i, n) Gtimers::start( i, n )
#define TIMER_STOP(i) Gtimers::stop( i )
#else
#define TIMER_START(i, n)
#define TIMER_STOP(i)
#endif

//------------------------------------------------------------------------

// Use only for a SearchNode not added to the DB yet!
static void irrelevant( SearchNode& sn, const IrrelevantInfo& info )
{
   int ref = search.irrelevantInfoDatabase().add( info );
   //cerr << "irr ref " << ref << nl;
   sn.status( SearchNode::Irrelevant );
   sn.irrelevantInfo( ref );
   sn.growStatus( SearchNode::NotGrowable );
}

// Use for existing nodes
static void makeIrrelevant( NodeNumber n, SearchNode& sn, const IrrelevantInfo& info )
{
   search.maybeDecrementGrowableCount( sn );  // nasty abstraction leak
   irrelevant( sn, info );
   search.changeNode( n, sn );
}

//------------------------------------------------------------------------

#if 0
static SearchNode::Status outOfFamilyMinorCheck(
   SearchNode& sn,
   RBBPG& graph,
   PartialMinorProof& pmp
)
{
   assert( sn.status() == SearchNode::Unknown );
   assert( sn.membership() == SearchNode::NotInF );

   const SearchMinorDictionary& sm = pmp.minors();
   MinorNumber i;

   ForAllDictionaryItems( sm, i )
   {
      RBBPG G = graph;

      const SearchMinor& m = sm.get(i);

      m.calcMinor( G );

      if ( ! Family::member( G ) )
      {
         return SearchNode::Nonminimal;
      }
   }

   // all minors are in f!!
   return SearchNode::Minimal;
}
#endif

//------------------------------------------------------------------------

#if 0
static MinorNumber congruenceCheck(
   const SearchNode& sn,
   const RBBPG& graph,
   PartialMinorProof& pmp
)
{
   assert( sn.status() == SearchNode::Unknown );
   assert( sn.membership() == SearchNode::InF );

   Congruence* C1 = Family::newCongruence();
   Congruence* C2 = Family::newCongruence();

   C1->compute( graph );

   const SearchMinorDictionary& sm = pmp.minors();
   MinorNumber i;

   MinorNumber congMin = -1;
   ForAllDictionaryItems( sm, i )
   {
      RBBPG G = graph;
      const SearchMinor& m = sm.get(i);
      m.calcMinor( G );

      //cerr << "Checking minor: " << m << nl;

      C2->compute( G );

      if ( *C1 == *C2 )
      {
         congMin = i;
         break;
      }
      else
      {
      }
   }

   delete C1;
   delete C2;

   return congMin;
}
#endif

//------------------------------------------------------------------------

static Array<Operator> growthOps;

void initGrowthOps()
{
   int regularVertices = ProbInfo::getInt(ProbInfo::PathWidth) + 1 - ProbInfo::getInt(ProbInfo::ApexVertices);
   int i;
   Operator op;
   for( op.firstOp(), i=0; !op.isNoOp(); op++, i++ )
   {
      if ( op.isVertexOp() && op.vertex() >= regularVertices )
         continue;
      growthOps.append( op );
   }
   cerr << "num ops " << Operator::numberOfOperators() << ' ' << growthOps.size() << nl;
}

//------------------------------------------------------------------------

void growNode(
  NodeNumber parentNumber, 
  Array<NodeNumber>* unknowns,
  bool assumeNoncanonic,
  const NBBPG* prefix
)	
{
   //DBG( "Growing from: " << parentNumber << nl )

   TIMER_START( 0, "build children" );

   SearchNode& parent = search.node( parentNumber );

   assert( runInfo.runType() == RunInfo::Manager );

   if ( parent.growStatus() != SearchNode::Growable )
      return;

   assert( parent.growStatus() == SearchNode::Growable );

   parent.growStatus( SearchNode::Grown );

   if ( ! growthOps.size() )
      initGrowthOps();

   int num = growthOps.size();

   // create a collection to keep track of the new nodes
   //
   Array<SearchNode> C( num );
   Array<RBBPG> G( num );

   RBBPG parentGraph;
   search.graph( parentNumber, parentGraph );

   //DBG( ' ' << parentGraph << nl )

   // Form all children of P, by appending each operator.
   // Add them into a collection.
   //
   Operator last = parentGraph[parentGraph.size() - 1];
   int vertices = parentGraph.vertices();
   int maxVertices = ProbInfo::getInt( ProbInfo::MaxVertices );

   int ind = 0;
   for( int i = 0; i < num; i++ )
   {
//      if ( ! QCanonicCheck( growthOps[i], last ) )
//           continue;
      if ( ! QCanonicCheck( growthOps[i], parentGraph ) )
          continue;

      if ( vertices == maxVertices && growthOps[i].isVertexOp() )
          continue;

      C[ind] = SearchNode( parentNumber, growthOps[i], parent.depth() + 1 );

      G[ind] = RBBPG( parentGraph, 1 );
      G[ind].setOp( G[ind].size() - 1, growthOps[i] );

      if ( hasMultipleEdge( G[ind] ) )
          continue;
      ind++;
   }

   num = ind;
   C.resize(num);
   G.resize(num);

   // Get search to tell us the nodenumbers that will be 
   // assigned by addNode later.
   // Note that we create all possible t-parses even if clearly bad (eg.
   // multiple edges) and assign them all NodeNumbers so that we can know
   // what a t-parse's NodeNumber will be before we finish processing them.
   //
   Array<NodeNumber>  numbers( num );
   search.predictNodeNumbers( numbers );

   TIMER_STOP( 0 );

   nodeCreation( C, G, numbers, unknowns, assumeNoncanonic, prefix );

   TIMER_START( 11, "addChildren" );

   // write parent's kids
   //
   search.addChildren( parentNumber, numbers );

   // write parent with new status (FIXME: what? where is it modified?)
   //
   // get a new copy of parent, cause our reference is no longer valid!
   //
   search.changeNode( parentNumber, search.node( parentNumber ) );

   TIMER_STOP( 11 );
}

//------------------------------------------------------------

void nodeCreation(
  const Array<SearchNode>& C,
  const Array<RBBPG>& G,
  const Array<NodeNumber>& numbers,
  Array<NodeNumber>* unknowns,
  bool assumeNoncanonic,
  const NBBPG* prefix
)
{
   bool sendMembership = ProbInfo::getBool( ProbInfo::WorkersDoMembership );

   int i;
   int num = C.size();

   assert( num == G.size() );

   TIMER_START( 1, "grow init" );

   if ( unknowns ) unknowns->resize(0);

   if ( num == 0 ) return;

   // Utility structures for iso tests
   //
   Array<bool> isNewGraph( num );
   Array<int>  isoResult( num );
   Array<IsoDBItem1>  isoReps1( num );
   Array<IsoDBItem2>  isoReps2( num );

   for ( i=0; i<num; i++ )
   {
      C[i].status( SearchNode::Unknown );
      C[i].membership( SearchNode::NotComputed );
      if ( sendMembership )
          // Won't know whether we should grow it until hearing back from a worker
//          C[i].growStatus( SearchNode::NotGrowable );
          C[i].growStatus( SearchNode::Growable );  // FIXME!!!!!
      else
          C[i].growStatus( SearchNode::Growable );
   }

   // check prefix
   //
   for ( i=0; i<num; i++ )
      if ( C[i].status() == SearchNode::Unknown )
         if ( prefix && ! BPG::equalUpToSmaller( *prefix, G[i] ) )
            irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::Prefix ) );

   TIMER_STOP( 1 );
   TIMER_START( 2, "multiedge+discon" );

#if 0
   // check multiedge
   //
   for ( i=0; i<num; i++ )
      if ( C[i].status() == SearchNode::Unknown )
         if ( hasMultipleEdge( G[i] ) )
            irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::MultiGraph ) );

#endif
   // check disconnected
   //
   if ( ProbInfo::getBool( ProbInfo::Connected ) )
      for ( i=0; i<num; i++ )
         if ( C[i].status() == SearchNode::Unknown )
            if ( isDisconnected( G[i] ) )
               irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::Disconnected ));

   // check biconnected
   //
   if ( ProbInfo::getBool( ProbInfo::Biconnected ) )
      for ( i=0; i<num; i++ )
         if ( C[i].status() == SearchNode::Unknown )
            if ( !isBiconnectedTest( G[i] ) )
               irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::Oneconnected ));

   TIMER_STOP( 2 );
   TIMER_START( 3, "qcanonic+tree" );

#if 0  // now handled in growNode
   // quick canonic
   //
   for ( i=0; i<num; i++ )
      if ( C[i].status() == SearchNode::Unknown )
         if ( ! QCanonicCheck( G[i] ) )
            irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::QNoncanonic ) );
#endif

#if 0 /* mjd -- here */

   // canonic
   //
   for ( i=0; i<num; i++ )
      if ( C[i].status() == SearchNode::Unknown )
         if ( ! isCanonic( G[i] ) )
            irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::Noncanonic ) );
#endif

   // check tree
   //
   if ( ProbInfo::getBool( ProbInfo::KeepTree ) )
      for ( i=0; i<num; i++ )
         if ( C[i].status() == SearchNode::Unknown )
            if ( ! isAcyclic( G[i] ) )
               irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::NonTree));

   DBG1( "   initial checks complete " << nl )

   TIMER_STOP( 3 );

   //Comment out pretests for allg family
   TIMER_START( 4, "pretests" );

   // pretests
   //
   for ( i=0; i<num; i++ )
      if ( C[i].status() == SearchNode::Unknown )
      {
         const NonminimalPretests& pretests = Family::nonminimalPretests();

         pretests.setGraph( G[i] );
         int pNum = pretests.applyAllTests();

         if ( pNum != -1 )
            irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::Pretest, pNum ) );

         pretests.releaseGraph();
      }


   TIMER_STOP( 4 );
   TIMER_START( 5, "compute iso" );

   DBG1( "   mem & pretests complete " << nl )

   // isomorphism and persistent database checks

   // compute the iso representitives
   //
   isNewGraph.fill( false );
   for ( i=0; i<num; i++ )
      if ( C[i].status() == SearchNode::Unknown )
         isNewGraph[i] = true;

   IsoDBItem__compute( G, isNewGraph, isoReps1, isoReps2 );

   TIMER_STOP( 5 );
   TIMER_START( 6, "remove isosiblings" );

   // check against each other
   //
   IsoDBItem__checkInternalIso( G, isoReps1, isoReps2, isNewGraph, isoResult, G[0].length() );

   for ( i=0; i<num; i++ )
      if ( isNewGraph[i] )
         if ( isoResult[i] != -1 )
         {
            irrelevant( C[i], 
                        IrrelevantInfo( IrrelevantInfo::IsoSibling, 
                                        numbers[ isoResult[i] ] ) );
            isNewGraph[i] = false;
         }
   
   DBG1( "   internal iso complete " << nl )

   TIMER_STOP( 6 );
   TIMER_START( 7, "db iso lookups" );

   search.isoDatabase().checkForIsomorphicEntry(
       isoReps1, isoReps2, isNewGraph, isoResult, G[0].length() );

   for ( i=0; i<num; i++ )
   {
      if ( isoResult[i] != -1 )
      {
         // already in db, possibly also tree
         //
         ProofRef proofRef = search.isoDatabase().proofRef( isoResult[i] );
         // Mark as not new even if iso to an existing Unknown node, and therefore remaining Unknown
         isNewGraph[i] = false;


         if (proofRef.status() == SearchNode::Minimal)
         {
            MinimalProof pf;
            search.minimalProofDatabase().getInfo( proofRef.minimalProof(), pf );

            bool addNode = true;
            if ( pf.isUsed() )
            {
               addNode = false;

               DBG1( "Min iso: " << isoResult[i] << '*' << pf << '*' 
                     << search.node(pf.usedBy() ) << '*' << isoG << nl );

               // If using lexicographic canonicity, then it is guaranteed that the
               // first representative of each isomorphism class that is encountered is
               // the canonic one, so no testing required. If using <_c, need to check
               // whether we've found a representative lower in <_c
               if ( !ProbInfo::getBool( ProbInfo::LexicographicCanonicity ) )
               {
                  RBBPG isoG;
                  search.graph( pf.usedBy(), isoG );

                  if ( G[i].canonicLess( isoG ) )
                  {
                     DBG( "MinimalProof change: old=" << pf.usedBy() 
                          << " new=" << numbers[i] << nl );
                     // Demote previous node
                     SearchNode& isoNode = search.node( pf.usedBy() );
                     makeIrrelevant( pf.usedBy(), isoNode, 
                        IrrelevantInfo( IrrelevantInfo::Isomorphic, numbers[i] ) );
                     DBG( "  new iso IInfo node: " << isoNode << nl )
                        search.changeNode( pf.usedBy(), isoNode );
                     search.incrementMinimalSwaps();
                     addNode = true;
                  }
               }
            }

            if ( addNode )
            {
               DBG1( cerr << "Min by iso: " << numbers[i] << ' ' << C[i] << nl );
               C[i].status( SearchNode::Minimal );
               C[i].minimalProof( proofRef.minimalProof() );
               // Redirect existing proof
               pf.usedBy( numbers[i] );
               search.minimalProofDatabase().update( proofRef.minimalProof(), pf );
            }
            else
            {
               irrelevant( C[i], 
                  IrrelevantInfo( IrrelevantInfo::Isomorphic, pf.usedBy() ) );
            }

         }
         else if (proofRef.status() == SearchNode::Nonminimal)
         {

            NonminimalProof pf;
            search.nonminimalProofDatabase().getInfo( proofRef.nonminimalProof(), pf );

            if ( pf.isUsed() )
            {
               // No real need to find the canonical representative for a nonminimal node

               irrelevant( C[i], 
                  IrrelevantInfo( IrrelevantInfo::Isomorphic, pf.usedBy() ) );
            }
            else
            {
               C[i].status( SearchNode::Nonminimal );
               C[i].nonminimalProof( proofRef.nonminimalProof() );
               pf.usedBy( numbers[i] );
               search.nonminimalProofDatabase().update( proofRef.nonminimalProof(), pf );
            }

         }
         else if (proofRef.status() == SearchNode::Unknown)
         {
   
            UnknownState pf;
            search.unknownStateDatabase().getInfo( proofRef.unknownState(), pf );
   
            if ( pf.isUsed() )
            {
               // See Minimal stuff above

               if ( ProbInfo::getBool( ProbInfo::LexicographicCanonicity ) )
               {
                  irrelevant( C[i], 
                     IrrelevantInfo( IrrelevantInfo::Isomorphic, pf.usedBy() ) );
               }
               else
               {

                  RBBPG isoG;
                  search.graph( pf.usedBy(), isoG );

                  if ( G[i].canonicLess( isoG ) )
                  {
                     // That node has probably already been sent to the Dispatcher, so we can't
                     // forget about it. (Well we could, and process it twice.)
                     // And this node doesn't need sending.
                     // When the result is returned, follow the isomorphic link
                     // back to this node (possibly chaining!) and update it.
                     DBG( "UnknownState change: old=" << pf.usedBy() 
                          << " new=" << numbers[i] << nl )
                     SearchNode& isoNode = search.node( pf.usedBy() );
                     search.unknownStateDatabase().del( isoNode.unknownState() );
                     makeIrrelevant( pf.usedBy(), isoNode, 
                        IrrelevantInfo( IrrelevantInfo::Isomorphic, numbers[i] ) );
                     search.incrementUnknownSwaps();
                  }
                  else
                  {
                     irrelevant( C[i], 
                        IrrelevantInfo( IrrelevantInfo::Isomorphic, pf.usedBy() ) );
                  }
               }
            }
            else
            {
               // Impossible because orphaned Unknown nodes shouldn't exist
               aassert( false ); // in unknown db, but no isomorph???
            }

         }
         else //if (proofRef.status() == SearchNode::Irrelevant)
         {
            // Isomorphic to an IrrelevantInfo::OOF node
            // iso ref isn't saved
            irrelevant( C[i], 
                        IrrelevantInfo( IrrelevantInfo::Isomorphic, -1 ) );
         }
      }
   }

   // end of iso tests

   DBG1( "   pretests and iso checks complete" << nl );

   TIMER_STOP( 7 );

   int newUnknowns = 0;

   // not in db...check for root
   //
   if ( G.size() == 1 && G[0].isEmpty() && isNewGraph[0] == true )
   {
     int isoref = search.isoDatabase().add( isoReps1[0], isoReps2[0] );
     MinimalProof proof( MinimalProof::Root );
     proof.usedBy( numbers[0] );
     proof.isoRef( isoref );
     int pfNum = search.minimalProofDatabase().add( proof );
     C[0].status( SearchNode::Minimal );
     C[0].membership( SearchNode::InF );
     C[0].growStatus( SearchNode::Growable );
     C[0].minimalProof( pfNum );
   }
   else if ( assumeNoncanonic )
   {
      // Turned on for nodes with depth below MaxEqualityIsoLevel.
      // Assume that we are resuming a previous run, and any
      // nodes that weren't selected last time were ignored because they are
      // irrelevant
      for ( i=0; i<num; i++ )
         if ( C[i].status() == SearchNode::Unknown )
         {
            irrelevant( C[i], IrrelevantInfo( IrrelevantInfo::ENoncanonic ) );
         }
   }

   if ( ! sendMembership )
   {
      TIMER_START( 8, "membership" );

      // compute membership
      //
      for ( i=0; i<num; i++ )
      {
         if ( C[i].status() == SearchNode::Unknown )
         {
            if ( Family::member( G[i] ) )
               C[i].membership( SearchNode::InF );
            else
            {
               C[i].membership( SearchNode::NotInF );
               C[i].growStatus( SearchNode::NotGrowable ); //mjd
            }
         }
      }
      TIMER_STOP( 8 );
   }
     
   TIMER_START( 9, "add unknown" );

   if ( ProbInfo::getBool( ProbInfo::PretestsComplete ) )
   {
      for ( i=0; i<num; i++ )
      {
         //if ( C[i].status() == SearchNode::Unknown )
         if ( isNewGraph[i] )
         {
            int isoref = search.isoDatabase().add( isoReps1[i], isoReps2[i] );
            MinimalProof proof( MinimalProof::External );
            proof.usedBy( numbers[i] );
            proof.isoRef( isoref );
            int pfNum = search.minimalProofDatabase().add( proof );

            // Now fix the iso entry
            ProofRef proofRef;
            proofRef.setProof( SearchNode::Minimal, pfNum );
            search.isoDatabase().proofRef( isoref, proofRef );

            assert( C[i].status() == SearchNode::Unknown );  // No Unknown nodes to be isom. to
            C[i].status( SearchNode::Minimal );
            C[i].minimalProof( pfNum );
            C[i].membership( SearchNode::InF );
            C[i].growStatus( SearchNode::Growable );
         }
      }
   }

   // OK, finally add nodes to unknownStateDatabase
   //
   for ( i=0; i<num; i++ )
   {
      if ( C[i].status() == SearchNode::Unknown )
      {
         int isoref;
         if ( isNewGraph[i] )
         {
            if ( unknowns ) unknowns->append( numbers[i] );
            newUnknowns++;
            isoref = search.isoDatabase().add( isoReps1[i], isoReps2[i] );
         }
         else
         {
            // Isomorphic to an existing Unknown node
            isoref = isoResult[i];
         }
         assert( isoref >= 0 );
         UnknownState us;
         us.usedBy( numbers[i] );
         us.isoRef( isoref );
         int ueNum = search.unknownStateDatabase().add( us );

         // Now fix the iso entry
         ProofRef proofRef;
         proofRef.setProof( SearchNode::Unknown, ueNum );
         search.isoDatabase().proofRef( isoref, proofRef );

         C[i].status( SearchNode::Unknown );
         C[i].unknownState( ueNum );
      }
   }

   TIMER_STOP( 9 );
   TIMER_START( 10, "search.addNode" );

   // add nodes to search
   //
/*   DBG( "writing: " << numbers[0] << " to " << numbers[num-1] <<
        ", " << newUnknowns << " new unknowns" << nl; )
*/

   //DBG( "writing new nodes:" )
   for ( i=0; i<num; i++ )
   {
      //DBG( " " << numbers[i]  )
      search.addNode( C[i] );
   }
   //DBG( nl )

   TIMER_STOP( 10 );
}

