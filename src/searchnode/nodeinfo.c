
// -------------------------------------------
// --------------- nodeinfo.c ----------------
// -------------------------------------------

/*tex
 
\file{nodeinfo.c}
\path{src/general}
\title{Definitions for node info classes}
\classes{}
\makeheader
 
xet*/

#include "general/system.h"
#include "search/nodeinfo.h"

//----------------------------------------------------

void TimeStamp::setCreation() const
{
   ((TimeStamp*) this)->_creation = ((TimeStamp*) this)->_update 
   = System::timeInSeconds();
}

void TimeStamp::setUpdate() const
{
   ((TimeStamp*)this)->_update = System::timeInSeconds();
}

char* TimeStamp::created() const
{
   return System::secondsToAscii( _creation );
}

char* TimeStamp::updated() const
{
   return System::secondsToAscii( _update );
}

//----------------------------------------------------

ProofUsage::ProofUsage()
{
   _user = NotUsed;
}

void ProofUsage::notUsed()
{
   _user = NotUsed;
}


void ProofUsage::usedBy( NodeNumber n )
{
   _user = n.asInt();
}


bool ProofUsage::isUsed() const
{
   return _user != NotUsed;
}

NodeNumber ProofUsage::usedBy() const
{
   assert( _user != NotUsed );

   return NodeNumber( _user );
}


//----------------------------------------------------

const char* MinimalProof::typeAsString() const
{
   const char* s;

   switch( type() )
   {
      case DistinguisherProof: s = "dist proof"; break;
      case OutOfFamily: s = "out of family"; break;
      case External: s = "external"; break;
      case TightCong: s = "tight cong"; break;
      case Testset: s = "testset"; break;
      case None: s = "none"; break;
      case Root: s = "root"; break;
      default: aassert(false);
   }
   return s;
}

//----------------------------------------------------

NonminimalProof::NonminimalProof( ProofType type )
{
   setZero;
   _type = type;
}

NonminimalProof::NonminimalProof( const SearchMinor& minor, ProofType type )
{
   setZero;
   _type = type; // CongruentMinor (default);
   _minor = minor;
}

const SearchMinor& NonminimalProof::congruentMinor() const
{
   return _minor;
}

const char* NonminimalProof::typeAsString() const
{
   const char* s;

   switch( type() )
   {
      case CongruentMinor: s = "congruent minor"; break;
      case Parent: s = "parent"; break;
      case External: s = "external"; break;
      case OutOfFamily: s = "out of family"; break;
      case NotSaved: s = "not saved"; break;
      case HNoncanonic: s = "hard noncanonic"; break;
      case Testset: s = "testset"; break;
      case GiveUp: s = "give up"; break;
      case None: s = "none"; break;
      default: aassert(false);
   }
   return s;
}

//----------------------------------------------------

const char* UnknownState::typeAsString() const
{
   const char* s = "no type";
   return s;
}

//----------------------------------------------------

const char* IrrelevantInfo::typeAsString() const
{
   const char* s;

   switch( type() )
   {
      case Isomorphic: s = "isomorphic"; break;
      case IsoSibling: s = "isosibling"; break;
      case MultiGraph: s = "multigraph"; break;
      case Disconnected: s = "disconnected"; break;
      case ENoncanonic: s = "easy noncanonic"; break;
      case QNoncanonic: s = "quick noncanonic"; break;
      case Pretest: s = "pretest"; break;
      case NonTree: s = "nontree"; break;
      case Prefix: s = "not prefix"; break;
      case OutOfFamily: s = "out of family"; break;
      default: aassert(false);
   }
   return s;
}


IrrelevantInfo::IrrelevantInfo()
{
   _type = InfoType( -1 );
}

IrrelevantInfo::IrrelevantInfo( InfoType type )
{
   assert( type != Isomorphic );
   assert( type != Pretest );
   _type = type;
   _data._pretest = 0;  // make it clean
}

IrrelevantInfo::IrrelevantInfo( InfoType type, int data )
{
   assert(type == Pretest );

   _type = type;
   _data._pretest = data;
}

IrrelevantInfo::IrrelevantInfo( InfoType type, NodeNumber n )
{
   assert(type == Isomorphic || type == IsoSibling );

   _type = type;
   _data._isomorphicNode = n.asInt();
}


