
// -------------------------------------------
// ------------ searchminor.c ----------------
// -------------------------------------------

/*tex
 
\file{searchminor.c}
\path{src/bpg}
\title{Definition of search minor class}
\classes{SearchMinor}
\makeheader
 
xet*/

#include <iostream>
//include "general/io_leda.h"
#include "search/searchminor.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"


//-----------------------------------------------------------------


SearchMinor::SearchMinor()
{
}

SearchMinor::SearchMinor( ReductionType type, int parsedPos1, int parsedPos2 )
{
   memset( this, 0, sizeof *this );
   _type = type;
   _parsedInfo1 = parsedPos1;
   _parsedInfo2 = parsedPos2;
}

SearchMinor::~SearchMinor()
{
}

void SearchMinor::zeroFill( SearchMinor& m )
{
   memset( &m, 0, sizeof m );
}

int SearchMinor::compare( const SearchMinor& sm ) const
{
   if ( _type < sm._type ) return -1;
   else if ( _type > sm._type ) return 1;
  
   if ( _parsedInfo1 < sm._parsedInfo1 ) return -1;
   else if ( _parsedInfo1 > sm._parsedInfo1 ) return 1;

   if ( _parsedInfo2 < sm._parsedInfo2 ) return -1;
   else if ( _parsedInfo2 > sm._parsedInfo2 ) return 1;

   return 0;
}

bool SearchMinor::isPrimitive( const BPG &B, const BGraph &G ) const
{
   if ( type() != ContractEdge )
      return true;

   Operator op = B.getOp( parsedPosition() );
   assert( op.isEdgeOp() );
   int v1, v2;
   B.getEdgeOpEndpoints( parsedPosition(), v1, v2 );

   for (int i = 0; i < G.order(); i++)
   {
      if ( v1 == i || v2 == i )
         continue;
      if ( G.isEdge( v1, i ) && G.isEdge( v2, i ) )
      {
         return false;
      }
   }
   return true;
}

void SearchMinor::calcMinor( BPG& G ) const
{
   int i = parsedPosition();

   switch( type() )
   {
      case DeleteVertex: G.deleteIsoVertex( i ); break;
      case DeleteEdge:   G.deleteEdge( i ); break;
      case ContractEdge: G.contractEdge( i ); break;
      default: aassert(false);
   }
}

PartialMinorProof::PartialMinorProof()
{
   nextFreeDistNumber = 0;
   nextFreeMinorNumber = 0;
}

PartialMinorProof::~PartialMinorProof()
{
}

int PartialMinorProof::numberOfMinors() const
{
   return _minors.size();
}

int PartialMinorProof::numberOfDistinguishers() const
{
   return _distinguishers.size();
}

int PartialMinorProof::numberOfMinorsDistinguished() const
{
   return _proofs.size();
}

bool PartialMinorProof::proofIsComplete() const
{
   return numberOfMinors() == numberOfMinorsDistinguished();
}

bool PartialMinorProof::proofIsOptimal() const
{
   return numberOfDistinguishers() == 1;
}

const DistinguisherDictionary& PartialMinorProof::distinguishers() const
{
   return _distinguishers;
}

const SearchMinorDictionary& PartialMinorProof::minors() const
{
   return _minors;
}

void PartialMinorProof::minorsDistinguishedByDistinguisher(
   DistinguisherNumber dist,
   Array<MinorNumber>& minors
) const
{
   minors.resize(0);
   MinorNumber m;
   ForAllDictionaryItems( _minors, m )
   {
      if ( dist != -1 && 
           minorStatus( m ) == Distinguished && 
           minorDistinguisher(m) == dist )
      {
         minors.append( m );
      }

      if ( dist == -1 && minorStatus( m ) == NotDistinguished )
      {
         minors.append( m );
      }
   }
}

DistinguisherNumber PartialMinorProof::checkForIsoDistinguisher(
    const NBBPG& extension
) const
{
   LBBPG G1;
   G1.appendToAxiom( extension );

   DistinguisherNumber distNum;
   ForAllDictionaryItems( _distinguishers, distNum )
   {
      LBBPG G2;
      G2.appendToAxiom( _distinguishers.get( distNum ) );
//cerr << "LBBG iso " << G1 << " and " << G2 << nl;
      if ( isomorphic( G1, G2, fixedBoundary ) )
         return distNum;
   }

   return -1;
}

MinorNumber PartialMinorProof::addMinor( const SearchMinor& m )
{
   MinorNumber i = nextFreeMinorNumber++;
   _minors.insert( i, m );
   return i;
}

void PartialMinorProof::removeMinor( MinorNumber m )
{
   _minors.del( m );
   if ( _proofs.defined( m ) ) aassert( false );
   // clean out proofs?
}


DistinguisherNumber PartialMinorProof::addDistinguisher( const Distinguisher& D )
{
   DistinguisherNumber i = nextFreeDistNumber++;
   _distinguishers.insert( i, D );
   return i;
}


void PartialMinorProof::addProof( MinorNumber m, DistinguisherNumber dist )
{
   _proofs.insert( m, dist );
}

/*
void PartialMinorProof::changeProof( MinorNumber, DistinguisherNumber )
{
   aassert(false);  // same as above but clean out proofs
}
*/

PartialMinorProof::Status PartialMinorProof::minorStatus( MinorNumber m ) const
{
   return _proofs.defined( m ) ? Distinguished : NotDistinguished;
}

DistinguisherNumber PartialMinorProof::minorDistinguisher( MinorNumber m ) const
{
   assert( minorStatus( m ) == Distinguished );
   return _proofs.get( m );
}

/*
void PartialMinorProof::clearAll()
{
   aassert(false);
}
*/

void PartialMinorProof::addAllReductions( const RBBPG& G )
{
   if ( ProbInfo::graphOrder() == ProbInfo::MinorOrder )
      addAllMinors( G );
   else if ( ProbInfo::graphOrder() == ProbInfo::SubgraphOrder )
      addAllSubgraphs( G );
   else if ( ProbInfo::graphOrder() == ProbInfo::EdgeDeletionOrder )
      addAllEdgeDeletions( G );
   else
      aassert( false );
}

void PartialMinorProof::addAllEdgeDeletions( const RBBPG& G )
{
   // cheat the const...nextMinor with "false" param does not modify graph
   RBBPG& H = (RBBPG&) G;

   BGraph B( G );

   SearchMinor::ReductionType type = SearchMinor::DeleteEdge;
   int pos = -1;

   while( true )
   {
      if ( ! ::nextMinor( H, type, pos, false ) ) break;

      SearchMinor m( type, pos );
      // Non-contractions always primitive
      if ( type == SearchMinor::DeleteEdge )
          addMinor( m );
   }
   // Checking minors from the end is likely to be faster
   _minors.reverse();
}

void PartialMinorProof::addAllSubgraphs( const RBBPG& G )
{
   // cheat the const...nextMinor with "false" param does not modify graph
   RBBPG& H = (RBBPG&) G;

   BGraph B( G );

   SearchMinor::ReductionType type = SearchMinor::DeleteVertex;
   int pos = -1;

   while( true )
   {
      if ( ! ::nextMinor( H, type, pos, false ) ) break;

      SearchMinor m( type, pos );
      // Non-contractions always primitive
      if ( type != SearchMinor::ContractEdge )
          addMinor( m );
   }
   // Checking minors from the end is likely to be faster
   _minors.reverse();
}

void PartialMinorProof::addAllMinors( const RBBPG& G )
{
   // cheat the const...nextMinor with "false" param does not modify graph
   RBBPG& H = (RBBPG&) G;

   BGraph B( G );

   SearchMinor::ReductionType type = SearchMinor::DeleteVertex;
   int pos = -1;

   while( true )
   {
      if ( ! ::nextMinor( H, type, pos, false ) ) break;

      SearchMinor m( type, pos );
      if ( !m.isPrimitive( G, B ) )
         continue;
      addMinor( m );

   }
   // Checking minors from the end is likely to be faster
   _minors.reverse();
}

//-----------------------------------------------------------

//void PartialMinorProof::optimizeProof(
