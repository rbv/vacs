
// -------------------------------------------
// --------------- searchext.c ---------------
// -------------------------------------------
 
/*tex
 
\file{searchext.c}
\path{src/searchnode}
\title{Routines to run extension searches on nodes}
\classes{}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "search/searchmain.h"
//#include "search/searchcontrol.h"
#include "search/searchnode.h"
#include "search/searchminor.h"
#include "search/nodeinfo.h"
#include "search/search.h"

#include "family/family.h"
#include "vacs/filesystem.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "error.h"

#define LOG if ( lg ) *lg

// Returns true if something good happend
//
void randomExtensionSearch(
   const RBBPG& G,
   PartialMinorProof& proof,
   const randomExtensionSearchInfo& info,
   // rest of params are optional (only used by extend_sm)
   int run,
   Array< NBBPG >* foundExtensions,
   Array< Array< MinorNumber > >* foundMinors,
   ostream* lg
)
{

//cerr << "randomExt\n";

   CBBPG K;

   if ( ! extendOutOfFamily( G, K, info, run ) )
   {
      LOG << "Could not extend out of family!" << nl;
      return;
   }

   LOG << "graph extended:  " << K << nl;

   minimizeRHS( K );

   LOG << "graph minimized: " << K << nl;

   // extract the extension
   //
   NBBPG E;
   K.getRhsAsExtension( E );
   E.compress();

   LOG << "extension: " << E << nl;

   if ( foundExtensions ) (*foundExtensions)[run] = E;

#if 0
   // check for invalid proof...this means that the database read
   // failed, which means that the node has since been proven min/nonmin
   //
   if ( proof.graph().isEmpty() )
   {
      LOG << "Could not load from DB." << nl;
      return; // good news?
   }
#endif

   const SearchMinorDictionary& minors = proof.minors();

   MinorNumber mNum;

   bool provedOne = false;

   ForAllDictionaryItems( minors, mNum )
   {
      const SearchMinor& minor = minors.get( mNum );

      LOG << "testing minor..." << int(mNum);
      if ( proof.minorStatus( mNum ) == PartialMinorProof::Distinguished )
      {
         LOG << " skipped" << nl;
         continue;
      }

      RBBPG mn( G );
      minor.calcMinor( mn );
      CBBPG C( mn, E );

      // Is this dangerous?
      //
      C.removeMultipleEdges();

      LOG << nl << "   glue: " << C << nl;
      
      if ( Family::member( C ) )
      {
         // cheat -- add proof without an extension
         proof.addProof( mNum, 0 );

         if (foundMinors) (*foundMinors)[run].append( mNum );

         LOG << " in F ... sent signal" << nl;
      }
      else
      {
         LOG << " not in F" << nl;
      }

   } // for all minors

   return;
}

//----------------------------------------------------------------

MinorNumber checkForCongruentMinor(
   const RBBPG& graph,
   PartialMinorProof& pmp
)
{

   Congruence* C1 = Family::newCongruence();
   Congruence* C2 = Family::newCongruence();

   C1->compute( graph );

   const SearchMinorDictionary& sm = pmp.minors();
   MinorNumber i;

   MinorNumber congMin = -1;
   ForAllDictionaryItems( sm, i )
   {
      RBBPG G = graph;
      const SearchMinor& m = sm.get(i);
      m.calcMinor( G );

      //cerr << "Checking minor: " << m << nl;

      C2->compute( G );

      if ( *C1 == *C2 )
      {
//         cerr << "*** Cong minor: " << m << nl;
         congMin = i;
         break;
      }
      else
      {
      }
   }

   delete C1;
   delete C2;

   return congMin;
}

//------------------------------------------------------------------------

bool outOfFamilyMinimalCheck(
   RBBPG& graph,
   PartialMinorProof& pmp
)
{
   const SearchMinorDictionary& sm = pmp.minors();
   MinorNumber i;

   ForAllDictionaryItems( sm, i )
   {
      RBBPG G = graph;

      const SearchMinor& m = sm.get(i);

      m.calcMinor( G );

      if ( ! Family::member( G ) )
      {
         return false;
      }
   }

   // all minors are in f!!
   return true;
}


//------------------------------------------------------------------------


class TMC
{
   bool CongThenExtn;   // order to check
   const randomExtensionSearchInfo& info;
   int statsMaxDepth;
   int statsRecEntries;
   int statsExtnHits;
   int statsExtnChecks;
   int statsCongChecks;

   ostream* lg;         // log output (optional)
   const RBBPG& G;      // input graph
   int numExtns;        // extns to build
   Congruence* stateG;  // input graph state
   Congruence* stateH;  // minor's state
   Array<NBBPG> extensions; // pre-built extensions

   bool recSearch( const RBBPG& G, int depth );

public:

   TMC( const RBBPG&, int, const randomExtensionSearchInfo&, ostream* = 0 );
   ~TMC();

   bool run();

};

TMC::TMC(
  const RBBPG& _G,
  int _extnRuns,
  const randomExtensionSearchInfo& _info,
  ostream* _lg
) : info( _info ), G( _G )
{
  // load as params later
  CongThenExtn = false;

  statsRecEntries = 0;
  statsMaxDepth = 0;
  statsExtnChecks = 0;
  statsCongChecks = 0;
  statsExtnHits = 0;

  lg = _lg;
  numExtns = _extnRuns;
  stateG = Family::newCongruence();
  stateH = Family::newCongruence();
  stateG->compute( G );

  // build some extensions
  for ( int i=0; i<numExtns; i++ )
  {
    CBBPG C;
    if ( extendOutOfFamily( G, C, info, i ) )
    {
      minimizeRHS( C );
      NBBPG E;
      C.getRhsAsExtension( E );
      E.compress();
      extensions.append( E );
    }
  }
}

TMC::~TMC()
{
  delete stateG;
  delete stateH;
  if (!lg) return;

  *lg << "Max dpth=" << statsMaxDepth
      << " rec calls=" << statsRecEntries
      << " c checks=" << statsCongChecks
      << " e checks=" << statsExtnChecks
      << " e hits=" << statsExtnHits
      << " extns=" << extensions.size()
      << nl;
}

// recursive minor search
// return true iff a cong minor has been found
//
bool TMC::recSearch( const RBBPG& A, int depth )
{

  statsRecEntries++;
  if ( depth > statsMaxDepth ) statsMaxDepth++;

  // iterate over minors
  //
  SearchMinor::ReductionType type = SearchMinor::DeleteVertex;
  int pos = -1;
  //
  while( true )
  {
    RBBPG H( A );

    if ( ! ::nextMinor( H, type, pos ) ) break;

    if ( CongThenExtn )
    {
      statsCongChecks++;
      stateH->compute( H );
      if ( *stateG == *stateH ) { return true; }
    }

    // try to prune with extn search
    statsExtnChecks++;
    bool foundDist = false;
    for ( int i=0; i<extensions.size(); i++ )
    {
      CBBPG C( H, extensions[i] );
      C.removeMultipleEdges();
      if ( Family::member( C ) )
      {
        statsExtnHits++;
        foundDist = true;
        break;
      }
    }
    if ( foundDist ) continue;

    if ( ! CongThenExtn )
    {
      statsCongChecks++;
      stateH->compute( H );
      if ( *stateG == *stateH ) return true;
    }
 
    // no luck either way - dive, dive!
    if ( recSearch( H, depth+1 ) ) return true;
  }

  return false;
}


bool TMC::run()
{
   bool result = recSearch( G, 1 );
   if (lg) *lg << "Result = " << int(result) << nl;
   return result;
}


// external interface

bool checkForTildeCongruentMinor( 
  const RBBPG& G,
  int extnRuns,
  const randomExtensionSearchInfo& info,
  ostream* lg
)
{
   TMC tmc( G, extnRuns, info, lg );
   return tmc.run();
}

//-----------------------------------------------------------------------

// Returns true if something good happend
//
bool universalDistinguisherSearch(
   const RBBPG& G,
   PartialMinorProof& proof,
   const randomExtensionSearchInfo& info,
   // rest of params are optional
   int run,
   ostream* lg
)
{

   CBBPG K;

   if ( ! extendOutOfFamily( G, K, info, run ) )
   {
      LOG << "Could not extend out of family!" << nl;
      return false;
   }

   LOG << "graph extended:  " << K << nl;

   minimizeRHS( K );

   LOG << "graph minimized: " << K << nl;

   // extract the extension
   //
   NBBPG E;
   K.getRhsAsExtension( E );
   E.compress();

   LOG << "extension: " << E << nl;

   const SearchMinorDictionary& minors = proof.minors();

   MinorNumber mNum;

   bool provedAll = true;

   ForAllDictionaryItems( minors, mNum )
   {
      const SearchMinor& minor = minors.get( mNum );

      LOG << "testing minor..." << int(mNum);
      if ( proof.minorStatus( mNum ) == PartialMinorProof::Distinguished )
      {
         LOG << " skipped" << nl;
         continue;
      }

      RBBPG mn( G );
      minor.calcMinor( mn );
      CBBPG C( mn, E );

      // Is this dangerous?
      //
      C.removeMultipleEdges();

      LOG << nl << "   glue: " << C << nl;
      
      if ( ! Family::member( C ) )
      {
         provedAll = false;
         break;
      }

   } // for all minors

   if ( provedAll ) return true;

   return false;
}
