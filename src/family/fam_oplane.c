/*
 * Outer Planar family/congruence.  
 */

/*tex
 
\file{inc/fam_oplane.c}
\path{src/family}
\title{Definition of outer planar family functions}
\classes{OuterPlanarFamily, OuterPlanarCongruence}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "bpg/bpgutil.h"	// for pretests
#include "bpg/bpgutil2.h"	// for "degree" pretest

#include "family/fam_oplane.h"
#include "graph/graph.h"
#include "graph/algorithm/genus_ga.h"
#include "bpg/i-o/bpg2graph.h"

#include <iostream>

// -- statics --

static int oplaneFixedK;
static NonminimalPretests oplanePretests;

// -- pretests --

static bool oplanenmpIsoV( const RBBPG& g )
   { return hasIsoNonboundaryVertex( g ); }

static bool oplanenmpDangle( const RBBPG& g )
   { return findAllDangles( g ); }

static bool op1nmpBridge( const RBBPG& g ) 
   { return hasBridge( g ); }

static bool op1consecDeg2( const BGraph& G )
{
   int n = G.order();

   DegreeSequence deg(n);
   //
   G.degVec(deg);
  
   for (int i=0; i<n-1; i++)
   {
     if (deg[i] != 2 || G.isBoundary(i) == true) continue;

     for (int j=i+1; j<n; j++)
     {
       if (deg[j] != 2 || G.isBoundary(j) || G.isEdge(i,j) == false) continue;
       return true;
     }
   }

   return false;
}

// ----------- Virtual Link mechanism -------------

OuterPlanarFamily::OuterPlanarFamily()
{
   oplaneFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

   //oplanePretests.addTest( "iso vertex", oplanenmpIsoV );
   oplanePretests.addTest( "dangle", oplanenmpDangle );
   oplanePretests.addTest( "bridge", op1nmpBridge );
   oplanePretests.addTest( "adj Deg2s", op1consecDeg2 );
}

bool OuterPlanarFamily::_member( const BPG& g ) const
{
   Graph G;
   //
   bpg2graph(g,G);
   //
   if      ( oplaneFixedK == 0 ) return outerplanar( G );
   else if ( oplaneFixedK == 1 ) return outerplanar1( G );
   else return false;
}


const NonminimalPretests& OuterPlanarFamily::_nonminimalPretests() const
{
   return oplanePretests;
}


// membership test, and records congruence class
//
void OuterPlanarCongruence::compute( const BPG& g )
{
   aassert(false);
}


// tests whether two congruence classes are equal
//
bool OuterPlanarCongruence::operator==( const Congruence& c ) const
{
   aassert(false);
   return false;
}

