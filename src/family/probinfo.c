

// ------------------------------------------
// --------------- problems.c ---------------
// ------------------------------------------

/*tex
 
\file{problems.c}
\path{src/family}
\title{Definitions of link to specific problems, and class ProblemInfo}
\classes{ProblemInfo}
\makeheader
 
xet*/

#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

#include "error.h"

#include "family/probinfo.h"
#include "family/family.h"
#include "vacs/runinfo.h"

// List all interested family include files here!
//
#include "family/fam_vc.h"
#include "family/fam_fvs.h"
#include "family/fam_fes.h"
#include "family/fam_allg.h"
#include "family/fam_mp.h"
#include "family/fam_plane.h"
#include "family/fam_oplane.h"
#include "family/fam_pw.h"
#include "family/fam_embedding.h"

// statics
//
bool  ProbInfo::_flags[ ProbInfo::ParamTableSize ];
//
void* ProbInfo::_params[ ProbInfo::ParamTableSize ];
//
int ProbInfo::lineNo;

ProbInfo::GraphOrderType ProbInfo::_graphOrder = ProbInfo::InvalidOrder;

// Associate keywords with enum type vacsProblem.
// Includes documentation of ProblemNumber meaning.
//
const ProbInfo::ProblemLinkage ProbInfo::problemLinkageTable[] =
{
   // Graphs with vertex cover at most k. ProblemNumber is k.
   { "vcover",	VertexCover },
   { "fvs",	FeedbackVertexSet },
   { "fes",	FeedbackEdgeSet },
   { "mpath",	MaximumPath },
   // Graphs embeddable in a surface possibly with crosscaps, handles, apices,
   // and max number of covering faces. ProblemNumber unused.
   { "embedding", EmbeddableTest },
   // Graphs within k vertices (apices) of planar. ProblemNumber is k.
   // No congruence implemented; membership only.
   // (Note: there's a membership test function for within k edges of planar,
   // but no keyword/vacsProblem defined to access that)
   { "plane",	PlanarTest },
   { "pw",	Pathwidth },
   // All graphs. ProblemNumber unused.
   { "allg",	AllGraphs },
   // ProblemNumber is number of apices: 0 or 1 supported.
   // No congruence implemented; membership only.
   { "apex-oplanar",  ApexOuterPlanarTest },
   { "",        NoProblem }   		// end of table
};


const ProbInfo::ParseTableEntry ProbInfo::parseTable[] =
{
   // Not used for anything except log header
   { ProblemName,               "problem name",         Required, String,  0  },
   // Family dependent parameter (eg. vertex cover size, genus). Not used by some families.
   // See problemLinkageTable for family-specific documentation.
   { ProblemNumber,             "problem number",       Required, Integer, 0  },
   // Not used for anything except log header
   { Directory,                 "directory",            Required, Subpath, 0  },
   // Specifies family, see problemLinkageTable
   { Keyword,                   "keyword",              Required, String,  0  },
   { PathWidth,                 "pathwidth",            Required, Integer, 0  },
   // Graph ordering to use. Options: 'minor', 'subgraph', 'edge deletion'
   { GraphOrder,                "graph order",          Optional, String, "minor"  },
   // If true, use t-parse canonicity defined by the lexicographic ordering
   // instead of <_c.
   // For compatibility with old runs. Using <_c requires more work while growing
   // nodes but fewer minimality tests.
   // WARNING: seems to be broken, leave true
   { LexicographicCanonicity,   "lex canonicity",       Optional, Boolean, "true"  },
   // Limit search to nodes with bounded order
   { MaxVertices,               "max vertices",         Optional, Integer, "999"  },
   // Limit nodes in search tree (stop early)
   { MaxNodes,                  "max nodes",            Optional, Integer, "0"  },
   // Search amongst k-apex t-parses.
   // Note: if using a bound on the family rather than the obstructions,
   // then if family is PW k, use pathwidth=k+1, apex vertices=1
   { ApexVertices,              "apex vertices",        Optional, Integer, "0"  },
   // Family membership-specific option (currently unused)
   { MembershipFlag,            "membership flag",      Optional, Integer, "0"  },
   // Embedding only: number of covering faces
   { EmbeddingFaces,            "embedding faces",      Optional, Integer, "0"  },
   // Embedding only: orientable genus
   { EmbeddingGenus,            "embedding genus",      Optional, Integer, "0"  },
   // Embedding only: non-orientable genus
   { EmbeddingCrosscaps,        "embedding crosscaps",  Optional, Integer, "0"  },
   // Embedding only: number of apex vertices
   { EmbeddingApices,           "embedding apices",     Optional, Integer, "0"  },
   // Causes tbrowse to dump congruence states
   { DumpCongruence,            "tbrowse dump congruence", Optional, Boolean, "false"  },
   // Family congruence-specific option
   { CongruenceFlag,            "congruence flag",      Optional, Integer, "0"  },
   // Ignore t-parses not connected to boundary.
   // May still find disconnected \delta obstructions, before Extract filtering
   { Connected,                 "connected",            Optional, Boolean, "true"  },
   // Ignore t-parses not biconnected to boundary.
   // May still find non-biconnected \delta obstructions, before Extract filtering
   { Biconnected,               "biconnected",          Optional, Boolean, "false"  },
   // Ignore all graphs that aren't trees
   { KeepTree,                  "tree",                 Optional, Boolean, "false"  },
   // Unused
   { ConservativeGrow,          "conservative grow",    Optional, Boolean, "true"  },
   // Max number of nodes to assign to each worker at once
   { WorkerQueue,               "worker queue size",    Optional, Integer, "6"  },
   // If true, manager does not compute membership, workers send it back
   { WorkersDoMembership,       "delegate membership",  Optional, Boolean, "true"  },
   // Used for "all graphs": pretests totally determine minimality; nothing sent to workers
   { PretestsComplete,          "pretests complete",    Optional, Boolean, "false"  },
   // Random distinguisher options
   // Percentage of operators which are edges
   { ExtnEdgeWeight,            "extn edge weight",     Optional, Integer, "80"  },
   // Number of extensions on which to compare with minors
   { ExtnRuns,                  "extn runs",            Optional, Integer, "1"  },
   // Number of times to try to find an extension which is OOF and doesn't exceed max length before giving up
   // (The extended base graph needs to be OOF. Note that this isn't done per-minor)
   { ExtnTries,                 "extn tries",           Optional, Integer, "10"  },
   { ExtnMaxLength,             "extn max length",      Optional, Integer, "50"  },
   // Check whether the extended graph is OOF every 'skip' many operators
   { ExtnSkip,                  "extn skip",            Optional, Integer, "0"  },
   // Choose random extensions deterministically
   { ExtnDeterministic,         "deterministic extn",   Optional, Boolean, "false"  },
   // (Obsolete) Caused nonminimal proofs to be created for nm-by-congruence nodes
   //{ SaveNMProofs,              "save nm proofs",       Optional, Boolean, "true"  },
   { CongIsTight,               "cong is tight",        Optional, Boolean, "false"  },
   // Workers print stats comparing congruence state comparison results
   // with other minimality determination methods
   // Overrides "cong is tight"
   { TestCongTightness,         "test cong tightness",  Optional, Boolean, "false"  },
   // Permill (0.1%) of nodes (randomly chosen) on which to test cong tightness
   { TestCongPermill,           "test cong permill",    Optional, Integer, "1000"  },
   { UseTestset,                "use testset",          Optional, Boolean, "true"  },
   // Works test only canonicity and minimality using OOF and TMC (?) in searchutil2.c...
   { TildeMinorSearch,          "tilde minor search",   Optional, Boolean, "false"  },
   // Strictly below this level, assume tree fully explored: 
   // assume any node not found in iso DB is irrelevant (IrrelevantInfo::ENoncanonic)
   { MaxEqualityIsoLevel,       "max eql iso level",    Optional, Integer, "-1"  },
   { LoadAllNonminimal,         "load all nonminimal",  Optional, Boolean, "false"  },
   // Restrict to subset of search tree (comma separated list of 1/2 char hexidecimal operators)
   { LimitToPrefix,             "limit to prefix",      Optional, String,  " "  },
   // Send PartialMinorProof (distinguisher for each minor) back to Manager
   { MessagesContainPMP,        "send PMP",             Optional, Boolean,  "true"  },
   { UnivDistSearch,            "univ dist search",     Optional, Boolean,  "false"  },
   { PretestExtns,              "pretest extensions",   Optional, Boolean,  "false"  },
   // If false nonminimal-by-OOF are made irrelevant; will probably fail an assert
   { NonMinOOFRelevant,         "nonmin OOF relevant",  Optional, Boolean,  "true"  },
   // Seconds for workers to spend testing t-parse for canonicity
   { CanonicTimeout,            "canonic timeout",      Optional, Integer,  "0"  },
   // When giving up, whether to mark as minimal or nonminimal
   // Obviously, setting to false finds a subset of the obstruction set
   { DefaultToMinimal,          "default to minimal",   Optional, Boolean,  "true"  },
   // Causes extra manager and worker logging
   { VerboseNodeLogging,        "verbose node logging", Optional, Boolean,  "true"  },
};

void ProbInfo::parseGraphOrder()
{
   if ( strcmp( getString( GraphOrder ), "minor" ) == 0 )
      _graphOrder = MinorOrder;
   else if ( strcmp( getString( GraphOrder ), "subgraph" ) == 0 )
      _graphOrder = SubgraphOrder;
   else if ( strcmp( getString( GraphOrder ), "edge deletion" ) == 0 )
      _graphOrder = EdgeDeletionOrder;
   else
   {
      cerr << "Unrecognised graph order " << getString( GraphOrder ) << nl;
      aassert( false );
   }
}

void ProbInfo::initializeFamily()
{

   VacsProblem p;
   bool found = false;
   for ( int i=0; strcmp(problemLinkageTable[i].keyword,""); i++ )
   {
      if ( ! strcmp( problemLinkageTable[i].keyword, getString(Keyword) ) )
      {
         p = problemLinkageTable[i].problem;
         found = true;
         break;
      }
   }

   if ( !found ) fatalError("ProbInfo::initializeFamily: Family (keyword) in _driver is missing or unrecognised");

   Family* c;

   switch( p )
   {
      case VertexCover: c = new VertexCoverFamily; break;
      case FeedbackVertexSet: c = new FvsFamily; break;
      case FeedbackEdgeSet: c = new FesFamily; break;
      case MaximumPath: c = new MaximumPathFamily; break;
      case EmbeddableTest: c = new EmbeddableFamily; break;
      case PlanarTest: c = new PlanarFamily; break;
      case ApexOuterPlanarTest: c = new OuterPlanarFamily; break;
      case Pathwidth:  c = new PathwidthFamily; break;
      case AllGraphs:  c = new AllGraphsFamily; break;
      default: aassert(false);
   }

   Family::setFamily( *c );
}


//-------------------------------------------------------------------------


void ProbInfo::parseSubpath( int i, const char* arg )
{
   if ( ! strcmp( arg, "" ) ) parseError( "missing arg" );

   const char *VACS = RunInfo::getVACSpath();
   int vLen = strlen(VACS);

   _params[i] = new char[ strlen(arg) + vLen + 3 ];
   char* dir = (char*) _params[i];

   strcpy( dir, VACS );

   dir[vLen]='/'; 
   dir += vLen+1;

   strcpy( dir, arg );

   // check for trailing "/"
   //
   int l = strlen( dir );
   if ( dir[ l-1 ] != '/' )
   {
      dir[ l ] = '/';
      dir[ l+1 ] = '\0';
   }
}


void ProbInfo::parseString( int i, const char* arg )
{
   if ( ! strcmp( arg, "" ) ) parseError( "missing arg" );
   _params[i] = new char[ strlen(arg) + 1 ];
   strcpy( (char*) _params[i], arg );
}

void ProbInfo::parseInteger( int i, const char* arg )
{
   int val;
   if ( sscanf( arg, "%d", &val ) != 1 )
         parseError( "bad value - expecting integer" );
   _params[i] = reinterpret_cast<void*>(val);
}

void ProbInfo::parseBoolean( int i, const char* arg )
{
   bool val;
   if ( strcmp( arg, "true" ) == 0 ) val = true;
   else if ( strcmp( arg, "false" ) == 0 ) val = false;
   else parseError( "bad value - expecting boolean" );
   _params[i] = (void*) val;
}


// Read successive lines from the file, and parse
//
void ProbInfo::readProbInfo( const char* filename )
{
   // init the param table
   //
   int i;
   for ( i=0; i<ParamTableSize; i++ ) assert( parseTable[i].num == i );
   for ( i=0; i<ParamTableSize; i++ ) _flags[i] = false;  // whether present
   for ( i=0; i<ParamTableSize; i++ ) _params[i] = 0;

   FILE* file = fopen( filename, "r" );

   if ( !file ) fatalError( "Could not open parameter file" );

   char inLine[ STRMAX ];
   char dest[ STRMAX ];
   char value[ STRMAX ];

   for ( lineNo=1; ; lineNo++ )
   {
      fgets( inLine, STRMAX, file );

      if ( feof( file ) ) break;

      // parse the line
      if ( parseInputLine( inLine, dest, value ) )
      {
         // parse was ok, and not a comment line
         // look for dest in parseTable	

         for ( i=0; i<ParamTableSize; i++ )
          {
            if ( ! strcmp( parseTable[i].name, dest ) ) break;
          }

         if (i == ParamTableSize) parseError( "unknown left hand side" );

         if ( _flags[i] ) parseError( "duplicated entry" );

         _flags[i] = true;

         // get the type...
         //
         switch ( parseTable[i].type )
         {
            case Integer: parseInteger( i, value ); break;
            case Boolean: parseBoolean( i, value ); break;
            case Subpath: parseSubpath( i, value ); break;
            case String:  parseString( i, value ); break;
         }
         
      }

   } // for

   for ( i=0; i<ParamTableSize; i++ )
      if ( !_flags[i] )
         if ( parseTable[i].req ==  Required )
         {
            paramError( parseTable[i].name );
         }
         else
         {
            const char* value = parseTable[i].dflt;
            assert( value );
            _flags[i] = true;
            switch ( parseTable[i].type )
            {
               case Integer: parseInteger( i, value ); break;
               case Boolean: parseBoolean( i, value ); break;
               case Subpath: parseSubpath( i, value ); break;
               case String:  parseString( i, value ); break;
            }
         }

}


int ProbInfo::getInt( Param i ) 
{
   assert( parseTable[i].type == Integer );
   return *(int*)&_params[i];
}

bool ProbInfo::getBool( Param i ) 
{
   assert( parseTable[i].type == Boolean );
   return _params[i] ? 1 : 0;
}

const char* ProbInfo::getString( Param i ) 
{
   assert( parseTable[i].type == String );
   return (const char*) _params[i];
}

const char* ProbInfo::getPath( Param i ) 
{
   assert( parseTable[i].type == Subpath );
   return (const char*) _params[i];
}


// report an error and exit
void ProbInfo::paramError( const char* s )
{
   cerr << "Parameter error: missing keyword: " << s << '\n';
   exit(-1);
}


// report an error and exit
void ProbInfo::parseError( const char* s )
{
   cerr << "Parse error on line: " << lineNo << " : " << s << '\n';
   exit(-1);
}


// Parse a line of the parameter file into destination and value.
// For example, "pathwidth: 2" is parsed into "pathwidth" and "2".
// Returns false if comment line, true is successful, fatal error on error.
//
bool ProbInfo::parseInputLine( char* inputLine, 
			       char* destination, char* value )
{

   if ( inputLine[0] == '#' ) return false;


   {
      int len = strlen( inputLine );
      if ( inputLine[len-1] != '\n' ) parseError("missing end-of-line");
      inputLine[len-1] = '\0';
   }

   // check for blank line
   if ( inputLine[ strspn(inputLine," ") ] == '\0' ) return false;

   // find the = sign
   char* p = index( inputLine, ':' );

   if ( !p ) parseError( "missing ':'" );

   int destLen = p - inputLine;
   strncpy( destination, inputLine, destLen );
   destination[ destLen ] = '\0';

   // strip trailing blanks from dest
   while ( destination[ --destLen ] == ' ' ) destination[destLen]='\0';

   // strip leading blanks from value and copy
   while( *(++p) == ' ' );
   strcpy( value, p );

   // strip trailing blanks from value
   int i = strlen( value );
   while( value[--i] == ' ' ) value[i] = '\0';

   return true;
}

