
/*
 * FVS testset routines
 */

/*tex

\file{fvs_testset.c}
\path{src/family/testset}
\title{Definition of feedback vertex set test set routines}
\classes{}
\makeheader

xet*/

#include "general/stream.h"
#include "family/fam_fvs.h"
#include "graph/lgraph.h"
#include "graph/bgraph.h"
#include "general/sets.h"

//#include "family/testset/testset.h"
#include "family/testset/fvs_testset.h"

// check family/testsets/fvs_testset.h
//
//typedef PIterSet< LForest > LForests;
//
//extern void generateForestTests(int t, LForests &FTests);

// global testset class. (now in FvsFamily class)
//
//FVS_TestSet FVS_Tests;

/*
 * Initialize testset with fixedK-membership and boundary size
 */
#define VEJ11 fixedK_eq_2
//
void FVS_TestSet::initTests(familyMembership membership, int fixedK, int t)  
 {
   //cerr << "entering initTests(" << fixedK << "," << t << ")\n";

   assert(fixedK >= 0);

   _membership = membership;
   _fixedK = fixedK;

   /*
    * See if we're luckey enough to have it stored away already.
    */
   bistream iTS( "_testset" );
   //
   if ( ! iTS.fail() )
   {
     iTS >> CAST( *this, TestSet );
     iTS.close();

     // do we have the implications?
     bistream iTSI( "_testsetI" );
     //
     if ( ! iTSI.fail() )
     {
       loadImplications( iTSI );
       iTSI.close();
     }

     return;
   }

   /*
    * Get all forest tests then we will add at most fixedK triangles
    * to all combinations of empty boundary vertices and other isolated
    * triangles.
    */
   LForests ForestTests;
   //
   generateForestTests(t, ForestTests);
 
   // Now add triangles!
   //
   LForest *pforest;

   ForestTests.start();
   //
   while (pforest = ForestTests.next())
   {

     /*
      * First calculate how many isolated boundary vertices
      * there are while building our initial BGraph.
      */
     LGraph noCycles(*pforest);
     //cerr << noCycles;

     BGraph B1(noCycles, t);
     //cerr << "First boundary graph\n" << B1;

     // added below... {t \choose 0} case
     // FVS_Tests.append(new BGraph(B1));  // tests for \FVSfam{0}

     // calculate number of isolated boundary
     //
     int isoBndry = 0;
     VertArray isoIndex(t);

     int i; for (i=0; i<t; i++)
      {
        if (B1.degree(B1.boundary(i)) == 0)
         {
          isoIndex[isoBndry++] = B1.boundary(i);
         }
      }

     // add boundary triangle loop
     //
     for (i=0; i <= min(isoBndry,fixedK); i++)
      {
#ifdef VEJ11
        if (i==2) continue;
#endif
        Subsets loop(isoBndry,i);
        //
        while (loop.nextset())
         {
#ifdef VEJ11
	   BGraph B2(B1,3*i);

           // do this combination of 4-cycle chorded loops
           //
           for (int j=0; j<i; j++)
            {
              B2.addEdge(isoIndex[loop.item(j)],B1.order()+3*j);
              B2.addEdge(isoIndex[loop.item(j)],B1.order()+3*j+1);
              B2.addEdge(isoIndex[loop.item(j)],B1.order()+3*j+2);
	      B2.addEdge(B1.order()+3*j,B1.order()+3*j+1);
	      B2.addEdge(B1.order()+3*j+1,B1.order()+3*j+2);
            }
#else
	   BGraph B2(B1,2*i);

           // do this combination of loops
           //
           for (int j=0; j<i; j++)
            {
              B2.addEdge(isoIndex[loop.item(j)],B1.order()+2*j);
              B2.addEdge(isoIndex[loop.item(j)],B1.order()+2*j+1);
	      B2.addEdge(B1.order()+2*j,B1.order()+2*j+1);
            }
#endif

//cerr << "add " << i << " triangles:\n" << B2;

	   append(new BGraph(B2));  

	   // add isolated triangle loop
	   //
#ifdef VEJ11
           switch (i)
           {
            case 0: // need iso 4-cycle with cord
            {
               BGraph B3(B2,4);

    	       B3.addEdge(B2.order(),B2.order()+1);
	       B3.addEdge(B2.order()+1,B2.order()+2);
	       B3.addEdge(B2.order()+2,B2.order()+3);
	       B3.addEdge(B2.order()+3,B2.order());

               append(new BGraph(B3));  // first just a ej-killing cycle

	       B3.addEdge(B2.order(),B2.order()+2);

	       append(new BGraph(B3));  

	       BGraph B4(B3,3);// also add one with 3-cycle

	       B4.addEdge(B3.order(),B3.order()+1);
	       B4.addEdge(B3.order()+1,B3.order()+2);
	       B4.addEdge(B3.order()+2,B3.order());

	       append(new BGraph(B4));  
               break;
             }
             case 1: // just add 3-cycle
             {
	       BGraph B3(B2,3);

	       B3.addEdge(B2.order(),B2.order()+1);
	       B3.addEdge(B2.order()+1,B2.order()+2);
	       B3.addEdge(B2.order()+2,B2.order());

	       append(new BGraph(B3));  
               break;
             }

             default: aassert(false);

	    }
#else
	   for (j=1; j <= fixedK-i; j++)
	    {
	       BGraph B3(B2,3*j);

	       // do this combination of loops
	       //
	       for (int k=0; k<j; k++)
		{
		  B3.addEdge(B2.order()+3*k,B2.order()+3*k+1);
		  B3.addEdge(B2.order()+3*k+1,B2.order()+3*k+2);
		  B3.addEdge(B2.order()+3*k+2,B2.order()+3*k);
		}

//cerr << "with " << j << " iso triangles:\n" << B3;

	       append(new BGraph(B3));  
	    }
#endif

         } // loop subsets

      } // boundary triangles
 
   } // next forests
   //
   ForestTests.stop();

//cerr << "Total # of tests = " << size() << nl;  
 }

// --------------------------- old stuff below --------------------------

#if 0 /* use base class's method in testset.c */

void FVS_TestSet::getState(const BPG &G, Array<bool> &_state)
{
  int sz = this->size();
 
  _state.resize(sz);

  for (int i=0; i<sz; i++)
   {
    BGraph concatG(G);

    concatG.concat(*get(i));	//  should use BGraph::getGluedGraph()

    //cerr << "Testing state i=" << i << " for\n" << concatG;

    _state.put( i, _membership(CAST(concatG,Graph), _fixedK) );

    //cerr << "membership = " << (_state.get(i)==false ? "false" : "true") << "\n";
   }

  //cerr << "state for "<< G << "is " << _state << nl;
}
#endif

#if 0

void FVS_TestSet::initTests(int fixedK, int b)  // with boundary size
 {
   int count=0; 
   //cerr << "entering initTests(" << fixedK << "," << b << ")\n";

   assert(fixedK >= 0);
 
   _fixedK = fixedK;
 
   // generate tests
   //
   int k = fixedK;
//   int b = Operator::boundarySize();
   int b_ejs = b*(b-1)/2;

   /*
    * Build look-up table for hashed edges.
    */
   int *b1 = new int[b_ejs];
   int *b2 = new int[b_ejs];
   
   for (int i=0, hash=0; i<b-1; i++)
    for (int j=i+1; j<b; j++)
     {
      b1[hash]=i;
      b2[hash]=j;
      hash++;
     }
 
   for (int t=0; t<=k; t++)		// Number of isolated triangles.
   for (int l=0; l<=k-t; l++)		// Number of loops on boundary.
   for (int a=0; a<=b_ejs; a++)		// Number of edges (arcs) on boundary.
   for (int m=0; m<=a; m++)		// Number of multi-edges on boundary.
    {
     BGraph *Gt = new BGraph( b + a + m + 2*l + 3*t, b );

     for (int j=0; j<t; j++)  // Add isolated triangles.
      {
       Gt -> addEdge ( b+3*j, b+3*j+1 );
       Gt -> addEdge ( b+3*j, b+3*j+2 );
       Gt -> addEdge ( b+3*j+1, b+3*j+2 );
      }

     int vt=3*t+b;  // The number of vertices that have been used.

     Subsets loop(b,l);  // add loops.
     //
     while (loop.nextset())
      {
       int vl = vt;
       BGraph *Gl = new BGraph(*Gt);

       for (int i=0; i<l; i++)
        {
         Gl -> addEdge ( loop.item(i), vl );
         Gl -> addEdge ( loop.item(i), vl+1 );
         Gl -> addEdge ( vl , vl+1 );
         vl+=2;
        }
          
      Subsets a_ejs(b_ejs,a); // add subdivided boundary edges.
      //
      while (a_ejs.nextset())
       {
        int va = vl;
        BGraph *Ga = new BGraph(*Gl);

        for (int i=0; i<a; i++)
         {
          Ga -> addEdge ( b1[a_ejs.item(i)], va );
          Ga -> addEdge ( b2[a_ejs.item(i)], va );
//          Ga -> addEdge ( b1[a_ejs.item(i)], b2[a_ejs.item(i)] );
          va++;
         }
 
        Subsets m_ejs(a,m); // add multi-edges
        //
        while (m_ejs.nextset())
         {
          int vm = va;
          BGraph *Gm = new BGraph(*Ga);
 
          for (int i=0; i<m; i++)
           {
            Gm -> addEdge ( b1[a_ejs.item(m_ejs.item(i))], vm );
            Gm -> addEdge ( b2[a_ejs.item(m_ejs.item(i))], vm );
            vm++;
           }
 
         if ( _add( Gm ))
          {
          count++;

          for (int f=3; f<b; f++)	// Fork size.
          for (vertNum fm=0; fm<=f; fm++)	// Number of multi-edges.
           {
	     Subsets f_ejs(b,f);
	     while (f_ejs.nextset())
	      {
		BGraph *Gf = new BGraph( *Gm , 1 + fm);  // new nodes

		for (int i=0; i<f; i++)
		 {
		  Gf -> addEdge ( f_ejs.item(i), vm );
		 }
	 
		Subsets fm_ejs(f,fm);
		while (fm_ejs.nextset())
		 {
                  int v = vm+1;
		  BGraph *Gfm = new BGraph(*Gf);
	 
		  for (int i=0; i<fm; i++)
		   {
		    Gfm -> addEdge ( f_ejs.item(fm_ejs.item(i)), v );
		    Gfm -> addEdge ( v, vm );
                    v++;
		   }

                  if (_add( Gfm )) count++; 
                 } 

                delete Gf;

               } // f_ejs
 
           } // end of forks

          } // test added

         } // m_ejs
         
        delete Ga;

       } // a_ejs
 
       delete Gl;

      } // loops

     delete Gt;

    } // end of for t,l,m,a

  delete [] b1;
  delete [] b2;

   cerr << "leaving initTests with " << count << " tests\n";
 }

#endif
