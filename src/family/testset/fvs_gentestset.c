
#include "family/fam_fvs.h"
#include "set/uocp.h"
#include "set/uociter.h"
#include "graph/ltree.h"
#include "general/sets.h"
#include "general/permutes.h"
#include "general/partitions.h"
#include "set/iterset.h"
//#include "graph/forest.h"

#define DEBUG 1

#if DEBUG >= 1
#define DEBUG1(s) cerr << s;
#else
#define DEBUG1(s)
#endif

#if DEBUG >= 2
#define DEBUG2(s) cerr << s;
#else
#define DEBUG2(s)
#endif

static bool validTreeTest( const LTree& T )
{
//cerr << "checking validity:\n" << T;

   for (int i=0; i<T.order(); i++)
    {
     if ( T.getLabel(i) != LTree::NoLabel ) continue;

     if (T.degree(i)==1)
      {
	return false;
      }
     else if(T.degree(i)==2)
      {
        if (T.getLabel(T.neighbor(i,0)) == LTree::NoLabel ||
            T.getLabel(T.neighbor(i,1)) == LTree::NoLabel ) return false;
      }
    }

//cerr << "is okay\n";

   return true;
}

//---------------------------------------------------------------------
//---------------------------------------------------------------------
// *** Connected Generation Section ***

// callbacks, callbacks.  when will it ever end??
// put all callback info together
//
static struct 
{
   PCollection<LTree>* testSet;
   int t;
   int n;
} cbrec;

// callback for connected-test generation
//
static void genCTcallback( Tree& freeTree )
{

   int n = cbrec.n;  // tree order
   int t = cbrec.t;  // boundary size

//cerr << "n==" << n << " t==" << t << nl; 
//cerr << freeTree << nl;

   // make a labelled tree that we can mangle
   //
   LTree T( freeTree, LTree::NoLabel );

   // apply all possible free boundaries, keep non-iso ones in a set

   PCollection<LTree> freeBndryTests;

   Subsets bndry( n, t );
   //
   while( bndry.nextset() )
   {
      // assign the free boundary (0=boundary)
      //
      T.setAllLabels( LTree::NoLabel );
     
      for ( int i=0; i<t; i++ ) T.setLabel( bndry.item(i), 0 );

      // is it a valid test?
      //
      if (n>t) if ( ! validTreeTest( T ) ) continue;

      // have we seen it?
      //
      if ( freeBndryTests.contains( &T ) ) continue;
      
      // it's a keeper :(
      //
      freeBndryTests.addDeep( &T );

      // put each possible fixed bndry on the tree, keep in a set

      PCollection<LTree> fixedBndryTests;

      PermEnum perm(t);
      do
      {
         for ( int i=0; i<t; i++ ) T.setLabel( bndry.item(i), perm[i] );

         // iso check
         if ( fixedBndryTests.contains( &T ) ) continue;

//cerr << "adding: \n" << T << nl;

         fixedBndryTests.add( new LTree(T) );

      } while (perm.next());

      // make sure this is a deep copy!
      //
      cbrec.testSet->addSetDeep( fixedBndryTests );

      // fixedBndryTests fall out of scope and get deleted here

   } // iterate over choice of bndry vertices

#if 0
   cerr << "*** Free bndry ***" << nl;
   cerr << freeBndryTests.size() << nl;
   cerr << freeBndryTests;
   cerr << nl << "******" << nl;
#endif

   // freeBndryTests fall out of scope and get deleted here

} // getCTcallback
   

// generate all connected tests with boundary labels 0..t-1
//
static void generateConnectedTests( int _t, PCollection<LTree> &_testSet )
{
   // set static for access by callback
   cbrec.testSet = &_testSet;
   cbrec.t = _t;

   assert( _t > 1 );
   assert( _testSet.isEmpty() );

//  DEBUG1( "connected gen for b.s. " << _t << nl )

   // get free trees of each order
   //
   for ( int n=_t; n < 2*_t; n++ )
   {
//      DEBUG1( "processing order " << n << nl )
      cbrec.n = n;
      Tree::freeTrees( n, genCTcallback );
      	
  cerr << "count (t=" << _t << ",n=" << n << ") is " << _testSet.size() << nl;
#if 0
      cerr << _testSet << nl;
      _testSet.purgeAll();
#endif
   }

}

// *** End of Connected Generation Section ***
//---------------------------------------------------------------------
//---------------------------------------------------------------------

// ******************************************************
// Get the following from Sir Kevin's contraption above.*
// ******************************************************
//
// Table of connected boundaried tree tests (1,...,t)
// The zero'th index is unused.
//
static Array< PIterSet< LTree > > _CTestTrees;

//---------------------------------------------------------------------
// static methods for forest test generation.
//---------------------------------------------------------------------

// Callback for PCollection's applyUnary() method.
//
static const DyArray< partType > *unaryMap;
//static const DyArray< Label > *unaryMap;
//
static void changeTreeLabels( LTree &T )
{
//cerr << "changingTreeLabels\n" << T;
 for (int i=0; i<T.order(); i++)
  {
   if (T.getLabel(i) != LTree::NoLabel)
    {
      T.setLabel( i, unaryMap->get(T.getLabel(i)) );
    }
  }
//cerr << "to\n" << T;
}

/*
 * How we keep our forest tests is typedef'ed below.  
 * (should be same definition as given in fvs_testset.c)
 */
// moved to fam_fvs.h
//typedef PIterSet< LForest > LForests;

// Build new test forest by taking cross product with new boundaried trees.
//
static void crossProduct(LForests &testSet, const PIterSet< LTree > &newLTrees)
{
 LForests newTestSet;
 
 LTree *ptree;
 //
 newLTrees.start();			// loop through new boundary trees
 //
 while ( ptree = newLTrees.next() )
 {
  LForest *pforest;
  
  testSet.start();			// add these to the previous forests
  //
  while ( pforest = testSet.next() ) 
  {
   LForest Tforest( *pforest );
   Tforest.addDeep(ptree);

//   DEBUG1("new forest test:\n" << Tforest)
   newTestSet.addDeep(&Tforest);
  }
  //
  testSet.stop();
 }
 //
 newLTrees.stop();

 testSet = newTestSet;	// Yikes!!!  (How do I avoid this copy?)
}

//---------------------------------------------------------------------

/*
 * fvs_testset.c calls this function.
 */
//void generateForestTests( int t, const VertSet &bndry, LForests &testSet )
void generateForestTests( int t, LForests &testSet )
{
 assert( testSet.isEmpty() );
 assert(t>1);

 // <--- put stuff to generate _CTestTrees table here

 _CTestTrees.resize(t+1);

 // do boundary size = 1
 //
 LTree LT(0);
 _CTestTrees[1].addDeep(&LT);

 int i; for ( i=2; i<=t; i++ )
 {
    generateConnectedTests( i, _CTestTrees[i] );
#if 0
    cerr << "------------------ " << i  << " -------------------" << nl;
    cerr << "count = " << _CTestTrees[i].size() << nl;
    cerr << "count = " << _CTestTrees[i] << nl;
//    LTree* T;
//    PCollectionIter<LTree> iter( _CTestTrees[i] );
//    while( (T = iter()) != 0 ) cerr << *T;
#endif
 }
 // just my part for now, mikey!
 //return;

 Partition B(t);
 DyArray< DyArray < partType > > BList;

 do { B.getPartition(BList);

//   DEBUG1("Starting boundary partition: " << B << nl)

   vertNum numParts = BList.size();

   /*
    * Set up first part of partition
    */
   PIterSet< LTree > newLTrees(_CTestTrees[BList[0].size()]);     

   unaryMap = &(BList[0]);

//   DEBUG1("Unary map is " << *unaryMap << nl)
   //
   newLTrees.applyUnary(changeTreeLabels);

   /*
    * Add these partial tests to the forest test set.
    * (Hold new tests with the current partition in tmpSet.)
    */
   LTree *ptree;
   //
   LForests tmpSet;

   newLTrees.start();
   //
   while ( ptree = newLTrees.next() )
   {
    //LForest TFL(ptree);
    LForest TFL(new LTree(*ptree));
//cerr << "adding first tree to forest:\n" << TFL;
    tmpSet.addDeep(&TFL);
   }
   //
   newLTrees.stop();

//cerr << "to numParts loop " << numParts << nl;

   /*
    * Now cross product the remaining parts of the partition
    */
    for (int i=1; i<numParts; i++)
    {
     PIterSet< LTree > newLTrees(_CTestTrees[BList[i].size()]);     
     
     unaryMap = &(BList[i]);
     //
     newLTrees.applyUnary(changeTreeLabels);

     crossProduct(tmpSet, newLTrees);	// Build new combinations.
    }

   testSet.addSetDeep(tmpSet);

 } while (B.getNextPart());

 // <--- put stuff to delete _CTestTrees table here

 for ( i=1; i<=t; i++ ) _CTestTrees[i].purgeAll();

 //DEBUG1("Forest testset (" << testSet.size() << "):\n" << testSet)
 DEBUG1("Forest testset (" << testSet.size() << "):\n")

 return;

}
