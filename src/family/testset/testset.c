
/*
 * Testset class for graph families.
 */

/*tex

\file{testset.c}
\path{src/family/testset}
\title{Definition of test set methods}
\classes{}
\makeheader

xet*/

#include "family/testset/testset.h"
#include "search/searchminor.h"
#include "general/dtime.h"
#undef minor

/*
 * destructor
 */
TestSet::~TestSet() 
{
  for (int i=0; i<size(); i++) delete (*this)[i];
}

/*
 * Test membership against all tests.
 */
void TestSet::getState(const BPG &G, Array<bool> &_state)
{
  int sz = this->size();
 
  _state.resize(sz);

  for (int i=0; i<sz; i++)
   {
    BGraph concatG(G);

    concatG.concat( *(*this)[i] );	//  should use BGraph::getGluedGraph()

    //cerr << "Testing state i=" << i << " for\n" << concatG;

    _state.put( i, _membership(CAST(concatG,Graph), _fixedK) );

    //cerr << "membership = " << (_state.get(i)==false ? "false" : "true") << "\n";
   }

  //cerr << "state for "<< G << "is " << _state << nl;
}


int TestSet::applyTestset( const RBBPG& graph, PartialMinorProof& pmp,
	                   Array<MinorNumber> &distMinors,
                           Array<int>& testNumbers )
{

//cerr << "in TestSet::applyTestset\n";
   Dtime timer;

   assert(distMinors.size() == 0);

   // convert whole mess to bgraphs
   //
   BGraph G( graph );

   const SearchMinorDictionary& sm = pmp.minors();

   PArray<BGraph> minors( sm.size() );
   Array<bool> dist( sm.size() );

   Array<MinorNumber> minorNumbers( sm.size() );
   dist.fill( false );
   int numDist = 0;

   {
      MinorNumber i;
      int j = 0;
      ForAllDictionaryItems( sm, i )
      {
         if ( pmp.minorStatus( i ) == PartialMinorProof::Distinguished )
         {
            dist[j] = true;
            numDist++;

            // don't need minor
            //
            minors[j] =  0;
            j++;
            continue;
         }

         RBBPG G( graph );
         const SearchMinor& m = sm.get(i);
         m.calcMinor( G );
         minors[j] = new BGraph(G);
         minorNumbers[j++] = i;
      }
      assert( j == sm.size() );
   }

  Graph glue, mglue; 		// Unboundaried graphs for membership tests.

  int sz = this->size();


 /*
  * Check for minors that are in F first, then check G
  */
   
  Array<int> mbr(sz);
  mbr.fill( 0 );        // 0=not checked, 1=if F, 2=not in F
  int agreesOnAll;      // loop exits with mNum if cong, -1 if dist found

  timer();

  for ( int j=0; j<minors.size(); j++ )
  {
    if ( dist[j] ) continue;

    agreesOnAll = j;
   
    int mChecks = 0;
    int gChecks = 0;

    for (int i=0; i<sz; i++)
    {

      // check minor first
      mChecks++;
      BGraph::getGluedGraph( *(minors[j]), *(*this)[i], mglue);
      if ( ! _membership( mglue, _fixedK ) ) continue;

      // need G membership
      gChecks++;
      BGraph::getGluedGraph( G, *(*this)[i], glue);
      if ( mbr[i] == 0 ) 
         mbr[i] = ( _membership( glue, _fixedK ) ) ? 1 : 2;

      // minor is in family...check G
      if ( mbr[i] == 1 ) continue; // G is in F too

#if 0
      for ( int jj=j; jj<minors.size(); jj++ )
      {
        if ( dist[jj] ) continue;

        // skip for j itself...we know it maps in
        if ( j != jj )
        {
          BGraph::getGluedGraph( *(minors[jj]), *(*this)[i], mglue);
          if ( ! _membership( mglue, _fixedK ) ) continue;
        }

        // minor is in, so we have a new result
        pmp.addProof( minorNumbers[jj], 1 );
        distMinors.append(minorNumbers[jj]);
        dist[jj] = true;
        numDist++;
        agreesOnAll = -1;
      }
#else
      // else we found a distinguisher
      pmp.addProof( minorNumbers[j], 1 );
      distMinors.append(minorNumbers[j]);
      dist[j] = true;
      numDist++;
      agreesOnAll = -1;
#endif
      break;
    }

    cerr << "Minor " << j << ", rslt=" << agreesOnAll 
         << ", mChecks=" << mChecks
         << ", gChecks=" << gChecks
         << ", time=" << timer()
         << nl;
       
    // did we luck out with a congruent minor?
    //
    if (agreesOnAll != -1) break;
  }

   minors.deleteAll();

   // all distinguished!!
   //
   if ( numDist == sm.size() ) return -1;

   // return the number of one that's not distinguished
   //
   return minorNumbers[agreesOnAll];
}

//----------------------------------------------------------------

// returns:
//
// -1: a2 <= a1
//  0: not related
//  1: a1 <= a2
//  2: a1 == a2
//
static int passSetLessOrEqual( const SquareMatrix< bool >& A, int r1, int r2 )
{
  int sz = A.dim();
  //assert( sz == a2.size() );

  bool a1lea2 = true;
  bool a2lea1 = true;
  for ( int i=0; i<sz; i++ )
  {
    if ( A.at(r1,i) > A.at(r2,i) ) a1lea2 = false;
    if ( A.at(r2,i) > A.at(r1,i) ) a2lea1 = false;
    if ( !a1lea2 && !a2lea1 ) break;
  }

  if ( a1lea2 && a2lea1 ) return 2;
  if ( a1lea2 ) return 1;
  if ( a2lea1 ) return -1;
  return 0;
}

//----------------------------------------------------------------

void TestSet::buildImplications()
{
  assert( ! impliesOOF );

  //resize( 50 );

  cerr << "Building testset implications..." << nl;
  cerr << "Number of tests: " << size() << nl;

  //impliesOOF = new Array< Array< int > >( size() );
  //impliesInF = new Array< Array< int > >( size() );
  impliesOOF = new SquareMatrix< bool >( size() );
  impliesInF = new SquareMatrix< bool >( size() );
  impliesOOF->fill(false);
  impliesInF->fill(false);

  //Array< Array< bool > > passSets( size() );
  SquareMatrix< bool > passSets( size() );

  Graph glue;

  // calc the pass set for each graph in the testset
  int i; for (i=0; i<size(); i++ )
  {
    cerr << i << ' ';
    for ( int j=i; j<size(); j++ )
    {
      BGraph::getGluedGraph( *(*this)[i], *(*this)[j], glue);
      if ( _membership( glue, _fixedK ) )
        { passSets.put(i,j,1); passSets.put(j,i,1); }
      else
        { passSets.put(i,j,0); passSets.put(j,i,0); }
    }
  }

  cerr << nl << "Pass sets completed" << nl;

  // compare the pass sets
  for ( i=0; i<size(); i++ )
  {
    cerr << i << ' ';
    for ( int j=i+1; j<size(); j++ )
    {
      int cmp = passSetLessOrEqual( passSets, i, j );
      if ( cmp == 1 )
        { impliesInF->put(i,j,1); impliesOOF->put(j,i,1); }
      else if ( cmp == -1 )
        { impliesOOF->put(i,j,1); impliesInF->put(j,i,1); }
      else if ( cmp == 2 )
        { impliesOOF->put(i,j,1); impliesInF->put(j,i,1);
          impliesOOF->put(j,i,1); impliesInF->put(i,j,1); }
    }
  }

  cerr << nl << "Implications complete" << nl;
}

void TestSet::dumpImplications( bostream& s )
{
   assert( impliesOOF );
   s << *impliesOOF;
   s << *impliesInF;
   cerr << "----------------------------------" << nl;
   //for ( int i=0; i<size(); i++ )
     //cerr << *(*this)[i] << nl << (*impliesOOF)[i] << nl << (*impliesInF)[i] << nl << nl;
   cerr << "----------------------------------" << nl;
}

void TestSet::loadImplications( bistream& s )
{
   assert( ! impliesOOF );
   impliesOOF = new SquareMatrix< bool >;
   impliesInF = new SquareMatrix< bool >;
   s >> *impliesOOF;
   s >> *impliesInF;
}

