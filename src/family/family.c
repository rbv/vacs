
// ------------------------------------------
// --------------- family.c -----------------
// ------------------------------------------

/*tex
 
\file{family.c}
\path{src/family}
\title{Definitions of classes in family.h}
\classes{NonminimalPretests}
\makeheader
 
xet*/

#include "error.h"
#include "family/family.h"
#include "graph/bgraph.h"

void Congruence::out( ostream& )
{
   // default output for congruence do nothing :)
}


NonminimalPretests::NonminimalPretests()
{
   _bpg = 0;
   _bgraph = 0;
}


void NonminimalPretests::setGraph( const RBBPG& g ) const
{
   assert( _bpg == 0 );
   assert( _bgraph == 0 );

   ((NonminimalPretests*) this)->_bpg = new RBBPG( g );
   ((NonminimalPretests*) this)->_bgraph = new BGraph( g );
}

void NonminimalPretests::releaseGraph() const
{
   delete ((NonminimalPretests*) this)->_bpg;
   delete ((NonminimalPretests*) this)->_bgraph;
   
   ((NonminimalPretests*) this)->_bpg = 0;
   ((NonminimalPretests*) this)->_bgraph = 0;
}


void NonminimalPretests::addTest( const char* name, NMPretestGraph test )
{
   tests.extend( 1 );
   int i = tests.size() - 1;
   tests[i].type = _BGRAPH;
   tests[i].name = name;
   tests[i].fnct = (void*) test;
}


void NonminimalPretests::addTest( const char* name, NMPretestBPG test )
{
   tests.extend( 1 );
   int i = tests.size() - 1;
   tests[i].type = _BPG;
   tests[i].name = name;
   tests[i].fnct = (void*) test;
}


int NonminimalPretests::applyAllTests() const
{
   int n = numberOfTests();
   for( int i=0; i<n; i++ )
      if ( applyTest( i ) ) return i;
   return -1;
}

int NonminimalPretests::applyBGraphTests(const BGraph& G) const
{
   for( int i=0; i<numberOfTests(); i++ )
   {
      if ( tests[i].type==_BGRAPH )
      {
         NMPretestGraph f = (NMPretestGraph) tests[i].fnct;
         if ( f(G) ) return i;
      }
   }
   return -1;
}

bool NonminimalPretests::applyTest( int i ) const
{
   assert( _bpg );
   assert( _bgraph );

   switch( tests[i].type )
   {
      case _BPG:
      {
         NMPretestBPG f = (NMPretestBPG) tests[i].fnct;
         return f( *_bpg );
      }

      case _BGRAPH:
      {
         NMPretestGraph f = (NMPretestGraph) tests[i].fnct;
         return f( *_bgraph );
      }

      default: aassert(false);
   }

   return false;
}

const char* NonminimalPretests::name( int i ) const
{
   return tests[i].name;
}

NonminimalPretests::~NonminimalPretests()
{
   // make sure releaseGraph was called
   assert( _bpg == 0 );
   assert( _bgraph == 0 );
}

// statics
//
Family* Family::rep = 0;

TestSet *Family::ts_rep = 0;

#include "general/stream.h"

void Family::buildTestset()
{
    assert( noTestSet() );

    bistream iTSet( "_testset" );
   
    if ( iTSet.fail() )  // then need to build one
    {
      rep->_setTestset();

      bostream oTSet( "_testset.tmp" );
      aassert( ! oTSet.fail() );
      oTSet << (*ts_rep);
      oTSet.close();
      system("mv _testset.tmp _testset");

#if 0
      ts_rep->buildImplications();
      bostream oTSetI( "_testsetI.tmp" );
      aassert( ! oTSetI.fail() );
      ts_rep->dumpImplications( oTSetI );
      oTSetI.close();
      system("mv _testsetI.tmp _testsetI");
#endif
    }  
    else
    {
      aassert( (iTSet.close(), false) );
    }
}

