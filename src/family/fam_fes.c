
/*
 * K-feedback edge set family/congruence.  
 */

/*tex
 
\file{fam_fes.c}
\path{src/family}
\title{Definition of feedback edge set family functions}
\classes{FesFamily,FesCongruence}
\makeheader
 
xet*/

// define VEJ12 yesDude

#include "general/stream.h"
#include "family/probinfo.h"
#include "family/fam_fes.h"
#include "family/fam_fvs.h"
#include "bpg/bpgutil.h"
#include "bpg/i-o/bpg2graph.h"
#include "graph/graph.h"
#include "graph/bgraph.h"
#include "bpg/algorithm/acyclic_b.h"

// -- statics for module --
static int fesFixedK;
static NonminimalPretests fesPretests;

// ----------- Virtual Link mechanism -------------

static bool fesnmpIsoV( const RBBPG& g ) { return hasIsoNonboundaryVertex( g );}
static bool fesnmpAnyDangle( const RBBPG& g ) { return findAllDangles( g ); }
static bool fesnmpBridge( const RBBPG& g ) { return hasBridge( g ); }
static bool fesnmpdeg2( const RBBPG& g ) { return hasNonboundaryDeg2( g ); }
static bool fesnmp2deg2c4( const BGraph &G )
{

   vertNum n = G.order();

   // fast "no" check
   //
   if ( n-G.boundarySize() < 2 ) return false;

   DegreeSequence Deg(n);
   AdjLists L(n);
   //
   G.getAdjLists(L,Deg);

   int num = 0;
   VertSet deg2s(n);
   //
   int i; for (i=0; i<n; i++)
    {
      if ( Deg[i] != 2 || G.isBoundary(i)==true ) continue;
      deg2s[num++]=i;
    }

   if (num<2) 
    {
      return false;
    }

   vertNum u1,u2,v1,v2;
   //
   for (i=0; i<num; i++)
    {
     u1=L[deg2s[i]][0];
     u2=L[deg2s[i]][1];

     for (int j=i+1; j<num; j++)
       {
        v1=L[deg2s[j]][0];
        v2=L[deg2s[j]][1];

        if ( v1!=u1 && v2!=u1 ) continue;
        if ( v1!=u2 && v2!=u2 ) continue;

        return true;
       }
    }

   return false;
 }

// temp test!
//
static bool fesnmpDeg2noK3( const BGraph &G )
 {
   vertNum n = G.order();

   DegreeSequence Deg(n);
   AdjLists L(n);
   //
   G.getAdjLists(L,Deg);

   int num = 0;
   VertSet deg2s(n);
   //
   vertNum n1,n2;
   //
   for (int i=0; i<n; i++)
    {
      if ( Deg[i] != 2 || G.isBoundary(i)==true ) continue;

      n1=L[i][0]; n2=L[i][1]; // neighbors

      if (G.isEdge(n1,n2)) continue;

      if (G.isBoundary(n1)==false || G.isBoundary(n2)==false) return true;
    }

   return false;
 }

static bool fesnmpDeg2onK3( const BGraph &G )
 {
   vertNum n = G.order();

   DegreeSequence Deg(n);
   AdjLists L(n);
   G.getAdjLists(L,Deg);

   int num = 0;
   VertSet deg2s(n);
   vertNum n1,n2;
   for (int i=0; i<n; i++)
    {
      if ( Deg[i] != 2 || G.isBoundary(i)==true ) continue;
      n1=L[i][0]; n2=L[i][1]; // neighbors
      if (G.isEdge(n1,n2)) return true;
    }
   return false;
 }

static bool anyCutVertices( const RBBPG& G )
{

   for (int i=0; i<G.size(); i++)
   {
     Operator op = G.getOp( i );
     if ( ! op.isVertexOp() ) continue;
     if ( G.isBoundaryVertex(i) ) continue;

     RBBPG graph(G);
     graph.deleteVertex( i );
 
     if ( isDisconnected(graph) ) return true;
   }

   return false;
}


//---------------------------------------------------------------------

static bool vej11BruteDummy(const Graph& G, int dumb)
{
  return vej11Brute(G);
}

static bool vej12BruteDummy(const Graph& G, int dumb)
{
  return vej12Brute(G);
}

void FesFamily::_setTestset( )
{
cerr << "in FesFamily::_setTestset\n";

    FVS_TestSet *ts = new FVS_TestSet;	// Yes, use FVS testset!

#if defined(VEJ11)
    ts->initTests( vej11BruteDummy, fesFixedK,
                   ProbInfo::getInt( ProbInfo::PathWidth )+1 );
#elif defined(VEJ12)
    ts->initTests( vej12BruteDummy, fesFixedK,
                   ProbInfo::getInt( ProbInfo::PathWidth )+1 );
#else
    ts->initTests( fesBrute, fesFixedK,
                   ProbInfo::getInt( ProbInfo::PathWidth )+1 );
#endif

    setTestset( (TestSet*) ts);
}

FesFamily::FesFamily()
{
   fesFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

   fesPretests.addTest( "dangle", fesnmpAnyDangle );
   fesPretests.addTest( "deg 2", fesnmpdeg2 );
   fesPretests.addTest( "bridge", fesnmpBridge );
   //fesPretests.addTest( "cut-vertex", anyCutVertices );
#if 0
   fesPretests.addTest( "deg 2 on k3", fesnmpDeg2onK3 );
#endif
//   fesPretests.addTest( "deg2noK3", fesnmpDeg2noK3 );
//   fesPretests.addTest( "2deg2c4", fesnmp2deg2c4 );

}

static int componentsHack( const BPG& g )
{
  BGraph B(g);
  int  c = CAST(B,Graph).components();
  //cerr << "g=" << g << " c=" << c;
  return c;
}

bool FesFamily::_member( const BPG& g ) const
{

#if defined(VEJ11)
   Graph G;
   bpg2graph(g,G);
   return vej11Brute(G);	
#elif defined(VEJ12)
   Graph G;
   bpg2graph(g,G);
   return vej12Brute(G);	
#else
//cerr << g.edges() << ' ' << g.vertices() << ' ' << fesFixedK << nl;
   if ( fesFixedK == 0 ) return acyclic(g);
   if ( g.edges() <= g.vertices() - componentsHack(g) + fesFixedK ) 
   return true;
// { cerr << " y\n";  assert(fes2Member(g)); return true;} 
   else 
   return false;
// { cerr << " n\n"; assert(!fes2Member(g)); return false;   }
#endif
#if 0
   if ( fesFixedK == 1 ) return fes1Member( g );
   if ( fesFixedK == 2 ) return fes2Member( g );
   if ( fesFixedK == 3 ) return fes3Member( g );
   assert( false );
#endif
}

 
const NonminimalPretests& FesFamily::_nonminimalPretests() const
{
   return fesPretests;
}

void FesCongruence::compute( const BPG& )
{ assert( false ); }

bool FesCongruence::operator==( const Congruence& ) const
{ assert( false ); return false; }

void FesCongruence::out( ostream& )
{ assert( false ); }

