/*
 * K-vertex cover family/congruence.  
 */

/*tex
 
\file{fam_vc.c}
\path{src/family}
\title{Definition of vertex cover family functions}
\classes{VertexCoverFamily, VertexCoverCongruence}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "family/fam_vc.h"
#include "bpg/bpgutil.h"

// -- statics --

static int vcFixedK;
static NonminimalPretests vcPretests;

// -- pretests --

static bool vcnmpIsoV( const RBBPG& g )
   { return hasIsoNonboundaryVertex( g ); }

static bool vcnmpDangle( const RBBPG& g )
   { return hasDangleWithOtherVertexDegreeGT1( g ); }

// Dinneen & Lai 2004, Thm 13:
// For every VC-k  obstruction,
// |G| <= 2*k - maxdeg(G) + 3
static bool vcnmpOrderBound( const BGraph& G )
{
   int maxdeg = 0;
   for (int i = 0; i < G.order(); i++)
   {
      if (maxdeg < G.degree(i))
         maxdeg = G.degree(i);
   }
   return G.order() > 2 * vcFixedK - maxdeg + 3;
}

// Has a very tiny effect, and excludes K_{k+2} obstruction.
static bool vcnmpRestrictDegree( const BGraph& G )
{
   for (int i = 0; i < G.order(); i++)
   {
      if (vcFixedK < G.degree(i))
         return true;
   }
   return false;
}

// Has a path of length 3
// Never triggers when using an (x,0)-apex-pathwidth search tree,
// because never have two adjacent non-boundary vertices
static bool vcnmpPathLength3( const BGraph& G )
{
   bool t = false;
   //if (G.order()>10){ t = true; cout << G << nl;}
   for (int i = 0; i < G.order(); i++)
   {
      if ( !G.isBoundary(i) && G.degree(i) == 2 )
      {
          //if (t)
          //   cout << " deg2 " << i << nl;
         for (int j = i + 1; j < G.order(); j++)
         {
             //if (t && G.isEdge(i,j))
             //  cout << "   edge " << j << nl;

            if ( G.isEdge( i, j ) && !G.isBoundary(j) && G.degree(j) == 2 )
               return true;
         }
      }
   }
   return false;
}

static NBBPG Suffix;


// ----------- Virtual Link mechanism -------------

VertexCoverFamily::VertexCoverFamily()
{
   vcFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

   // vcPretests.addTest( "iso vertex", vcnmpIsoV );
   vcPretests.addTest( "dangle", vcnmpDangle );
   vcPretests.addTest( "order-maxdeg bound", vcnmpOrderBound );
   // Not worth it
   //vcPretests.addTest( "restricted degree", vcnmpRestrictDegree );
   //vcPretests.addTest( "path len 3", vcnmpPathLength3 );
}

// AHH!  class Operator has not been inited when fam ctor called, so we'll have 
// to init the suffix later.  THIS SHOULD BE FIXED!!!
//
static void initSuffix()
{
   Suffix.resize(0);
   //Suffix.setOp( 0, Operator::edgeOperator(0,1) );
}


bool VertexCoverFamily::_member( const BPG& g ) const
{
   if ( ! Suffix.size() ) initSuffix();
   BPG *g2 = g.copy();
   g2->resize( g.size() + Suffix.size() );
   for ( int i=0, j=g.size(); i<Suffix.size(); i++, j++ )
      g2->setOp( j, Suffix.getOp(i) );
   g2->removeMultipleEdges();
   
   bool result = vertCover( *g2, vcFixedK );
   delete g2;
   return result;
}


const NonminimalPretests& VertexCoverFamily::_nonminimalPretests() const
{
   return vcPretests;
}


// membership test, and records congruence class
//
void VertexCoverCongruence::compute( const BPG& g )
{
   if ( ! Suffix.size() ) initSuffix();
   BPG *g2 = g.copy();
   g2->resize( g.size() + Suffix.size() );
   for ( int i=0, j=g.size(); i<Suffix.size(); i++, j++ )
      g2->setOp( j, Suffix.getOp(i) );
   g2->removeMultipleEdges();
   
   vertCover( *g2, vcFixedK, &_state );
   delete g2;
}


static bool checkForSuperiorCover(
   const vertexCoverState& s1,
   const vertexCoverState& s2,
   int index
)
{
//cerr << s1 << nl << s2 << nl;
   int num = 1 << Operator::boundarySize();
   for ( int i=1; i<num; i++ )
   {
       int j = index | i;
       if ( s1[j] == s2[j] )
          if ( s1[j] == min( s1[index], s2[index] ) )
          {
//cerr << "   hit: " << index << ' ' << j << nl;
             return true;
          }
   }
//cerr << "   diff" << nl;
   return false;
}

// tests whether two congruence classes are equal
//
bool VertexCoverCongruence::operator==( const Congruence& c ) const
{
   int flags = ProbInfo::getInt( ProbInfo::CongruenceFlag );

   const VertexCoverCongruence& C = (const VertexCoverCongruence&) c;

   return this->_state == C._state;

#if 0
   if ( ! ( flags & 1 ) )
   {
      // just do array comparison
      // the type cast is safe
      //
      return _state == C._state;
   }

   // do inferior cover check
   //
   bool eq = true;
   int l = _state.size();
   for ( int i=0; i<l; i++ )
      if ( _state.get(i) != C._state.get(i) )
         if ( ! checkForSuperiorCover( _state, C._state, i ) )
            { eq = false; break; }

   return eq;
#endif
}
