/*
 * Maximum path family/congruence.  
 */

/*tex
 
\file{fam_mp.c}
\path{src/family}
\title{Definition of maximum path family functions}
\classes{MaximumPathFamily, MaximumPathCongruence}
\makeheader
 
xet*/

#include "family/probinfo.h"

#include "bpg/bpgutil.h"	// for pretests
#include "bpg/bpggrph.h"	// for "degree" pretest

#include "family/fam_mp.h"
#include "bpg/algorithm/maxpath_b.h"
#include "graph/graph.h"
#include "bpg/i-o/bpg2graph.h"

#include <iostream>


// -- statics --

static int mpFixedK;
static NonminimalPretests mpPretests;

// --- pretests ---

static bool mpnmpIsoV( const RBBPG& g ) { return hasIsoNonboundaryVertex( g ); }

static bool mpnmpDegGE3( const RBBPG& g )
 {
  DegreeSequence deg;

  // Sorted up sequence while fixing boundary at front.
  //
  degreeSequence( g, deg, noBoundary );

  //cerr << "deg( " <<  g << " ) " <<  deg << nl;

  if ( deg[g.vertices()-1] >= 3 ) return true;
  else return false;
 }

// ----------- Virtual Link mechanism -------------

MaximumPathFamily::MaximumPathFamily()
{
   mpFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

      mpPretests.addTest( "iso vertex", mpnmpIsoV );
      mpPretests.addTest( "degree >= 3", mpnmpDegGE3 );
}

bool MaximumPathFamily::_member( const BPG& g ) const
{
#if 0
   Graph G;
   //
   bpg2graph(g,G);
   //
   return maxPath( G, mpFixedK );
#else
   return maxPath( g, mpFixedK );
#endif
}


const NonminimalPretests& MaximumPathFamily::_nonminimalPretests() const
{
   return mpPretests;
}


// membership test, and records congruence class
//
void MaximumPathCongruence::compute( const BPG& g )
{
   maxPath( g, mpFixedK, &_state );
}


// tests whether two congruence classes are equal
//
bool MaximumPathCongruence::operator==( const Congruence& c ) const
{
   // the type cast is safe to subclass' operator==
   //
   return _state == ( (const MaximumPathCongruence&) c)._state;
}
