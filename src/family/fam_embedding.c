/*
 * General surface embedding family/congruence.  
 */

/*tex
 
\file{inc/fam_embedding.c}
\path{src/family}
\title{Definition of surface embeddable family classes}
\classes{EmbeddableFamily, EmbeddingCongruence}
\makeheader
 
xet*/

#include "general/gtimers.h"
#include "general/lru_cache.h"
#include "general/memuse.h"
#include "family/probinfo.h"
#include "bpg/bpgutil.h"	// for pretests
#include "bpg/bpgutil2.h"	// for "degree" pretest

#include "family/fam_plane.h"
#include "family/fam_oplane.h"
#include "family/fam_embedding.h"

#include "graph/algorithm/genus_ga.h"
#include "bpg/i-o/bpg2graph.h"

#include <iostream>
#include <map>

//extern int total_ops_computed;

// Largest state to cache (in terms of number of embeddings)
#define MAX_STATE_MEM 200
#define MIN_STATE_COST 0
//#define MIN_COST_MULT10 30
#define CACHE_SIZE 100

// Disable the cache
//#define NOCACHE

// Always compute family membership using congruence, and check against other methods
//#define CHECK_MEMBERSHIP

struct BPGComparer
{
    bool operator()( const RBBPG &lhs, const RBBPG &rhs ) const { return lhs.compare( rhs ); }
};

// -- statics --

int EmbeddableFamily::genus;
int EmbeddableFamily::crosscaps;
int EmbeddableFamily::apices;
int EmbeddableFamily::covering_faces;

int EmbeddableFamily::alternativeMembership = 0;
int EmbeddableFamily::actualMembership = 0;
int EmbeddableFamily::totalMembershipTests = 0;
long long EmbeddableFamily::totalCost = 0;
long long EmbeddableFamily::totalSize = 0;
int EmbeddableFamily::totalCompute = 0;
int EmbeddableFamily::biggestState = 0;
int EmbeddableFamily::costliestState = 0;

int EmbeddableFamily::cacheLookups = 0;
int EmbeddableFamily::cacheHits = 0;
int EmbeddableFamily::cacheBackHits = 0;

#ifndef NOCACHE
static lru_cache< RBBPG, EmbeddingCongruence, std::map, BPGComparer > embedstate_cache( CACHE_SIZE );
#endif

// -- pretests --

// ----------- Virtual Link mechanism -------------

EmbeddableFamily::EmbeddableFamily()
{
   genus = ProbInfo::getInt( ProbInfo::EmbeddingGenus );
   crosscaps = ProbInfo::getInt( ProbInfo::EmbeddingCrosscaps );
   covering_faces = ProbInfo::getInt( ProbInfo::EmbeddingFaces );
   apices = ProbInfo::getInt( ProbInfo::EmbeddingApices );

   pretests.addTest( "iso vertex", planenmpIsoV );
   pretests.addTest( "dangle", planenmpDangle );
   pretests.addTest( "degree 2", planenmpContractDeg2 );
}

/*
EmbeddableFamily::~EmbeddableFamily()
{
   while ( embedstate_cache.size() )
      embedstate_cache.evict();
}
*/

void EmbeddableFamily::_printstats( ostream &os )
{
   int percent = 0;
   os << "Cache size: " << CACHE_SIZE << nl;
   if ( cacheLookups )
      percent = 100. * (cacheHits + cacheBackHits) / cacheLookups;
   os << "EmbeddableFamily stats: members: " << actualMembership << "/" << totalMembershipTests << "  successful alternativeMember tests: " << alternativeMembership << nl;
   os << "                        max size: " << biggestState << " max cost: " << costliestState
      << " avg size:" << ( (double)totalSize / totalCompute )
      << " avg cost:" << ( (double)totalCost / totalCompute ) << nl;
   os << "(The size of a state is the number of possible embeddings, the cost is the sum\n"
      << "over t-parse operators of number embeddings)" << nl;
   os << "EmbeddableFamily cache hits: " << cacheHits << " exact + " << cacheBackHits << " prefixes / " << cacheLookups
      << " (" << percent << "%)" << nl;
   //os << "total ops " << total_ops_computed << nl;
}

// Returns true if could determine membership
bool EmbeddableFamily::tryAlternativeMembership( const BPG& G, bool &result ) const
{
   // Even though we can't handle handles and crosscaps, try to recognise some of the family anyway
   // through planarity or other tests.
   // TODO: add a torodial testing routine.

   // Whether we can say that a graph is not in the family
   bool OOF_recognisable = true;
   if ( genus > 0 || crosscaps > 0 )
      OOF_recognisable = false;

   /*
     tests for non-toroidality:
     edges > vertices * 3
     f > m - n
   */

   if ( covering_faces == 0 )
   {
      Graph G2;
      bpg2graph( G, G2 );
      result = planar( G2, apices );
   }
   else  // covering_faces >= 1
   {
      Graph G2;
      bpg2graph( G, G2 );
      if ( apices == 0 )
      {
         result = outerplanar( G2 );
      }
      else
      {
         if ( apices > 1 )
            OOF_recognisable = false;
         result = outerplanar1( G2 );
      }
   }

   if ( result || OOF_recognisable )
      return true;
   return false;
}

bool EmbeddableFamily::_member( const BPG& G ) const
{
   bool correct;
   bool ret;

   Gtimers::start( 31, "non-Embed membership" );
   bool success = tryAlternativeMembership( G, correct );
   if ( success )
      alternativeMembership++;
   Gtimers::stop( 31 );
   ret = correct;

#ifndef CHECK_MEMBERSHIP
   if ( ! success )
#endif
   {
      EmbeddingCongruence state = _compute( G );
      ret = !state->isEmpty();
   }
   //cerr << "EmbeddableFamily::_member " << G << " result=" << ret << nl;

   totalMembershipTests++;
   if ( ret )
      actualMembership++;
#ifdef CHECK_MEMBERSHIP
   if ( success )
      aassert( correct == ret );
#endif

   return ret;
}

Congruence* EmbeddableFamily::_newCongruence() const
{
   EmbeddingCongruence *ret = new EmbeddingCongruence();
   return ret;
}

// Abstract cache. Returns a shared_ptr< EmbedStateWrapper >
EmbeddingCongruence EmbeddableFamily::_compute( const BPG &G )
{
#ifdef NOCACHE
   
   Gtimers::start( 33, "EmbedState::compute" );

   EmbeddingCongruence state = EmbeddingCongruence( new EmbedStateWrapper( Operator::boundarySize(), genus, crosscaps, apices, covering_faces ) );
   state->compute( G );

   Gtimers::stop( 33 );

#else

   Gtimers::start( 35, "EmbedState cache" );

   // First look for an exact match in the cache
   cacheLookups++;
   EmbeddingCongruence state = embedstate_cache( G );
   if ( state.get() )
   {
      Gtimers::stop( 35 );
      cacheHits++;
      return state;
   }

   // Otherwise look up 6 prefixes in the cache
   RBBPG G2( G );
   for (int i = 0; i < 6; i++)
   {
      G2.trim( 1 );
      if ( G2.length() <= Operator::boundarySize() )
         break;
      state = embedstate_cache( G2 );
      if ( state.get() )
      {
         state = EmbeddingCongruence( new EmbedStateWrapper( *state.get() ) );
         cacheBackHits++;
         break;
      }
   }
   // G2 should still contain real operators
   aassert( G2.length() );

   Gtimers::stop( 35 );
   Gtimers::start( 33, "EmbedState::compute" );

   if ( !state.get() )
   {
      // If we didn't find a prefix of the t-parse, then compute
      // the EmbedState for a prefix (4 operators trimmed)
      state = EmbeddingCongruence( new EmbedStateWrapper( Operator::boundarySize(), genus, crosscaps, apices, covering_faces ) );
      G2 = G;
      if ( G2.length() - 4 > Operator::boundarySize() )
         G2.trim( 4 );

      state->compute( G2 );
      biggestState = std::max( biggestState, state->size() );
      costliestState = std::max( costliestState, state->cost() );

      //if ( state->size() < MAX_STATE_MEM )
      // Don't bother caching states if we guess that they're cheaper to recompute
      if ( state->cost() >= MIN_STATE_COST )
      //if ( state->size() < 5 || 10 * state->cost() / state->size() >= MIN_COST_MULT10 )
      {
         Gtimers::stop( 33 );
         Gtimers::start( 35, "EmbedState cache" );
         embedstate_cache.insert( G2, EmbeddingCongruence( new EmbedStateWrapper( *state.get() ) ) );
         Gtimers::stop( 35 );
         Gtimers::start( 33, "EmbedState::compute" );
      }
   }

   // Now compute the EmbedState of the full t-parse
   int donelen = G2.size();
   G2 = G;
   for (int i = 0; i < donelen; i++)
   {
      G2.setOp( i, Operator::noOp() );
   }
   state->compute( G2 );

   Gtimers::stop( 33 );

   /*
   if ( donelen < G.size() )
   {
      Gtimers::start( 35, "EmbedState cache" );
      embedstate_cache.insert( G, state );
      //embedstate_cache.insert( G, EmbeddingCongruence( new EmbedStateWrapper( *state.get() ) ) );
      Gtimers::stop( 35 );
   }
   */

#endif

   biggestState = std::max( biggestState, state->size() );
   costliestState = std::max( costliestState, state->cost() );
   totalCompute++;
   totalCost += state->cost();
   totalSize += state->size();

   return state;
}

//----------------------------------------------------------------

void EmbeddingCongruence::compute( const class BPG &G )
{
   //_state = EmbeddableFamily::_compute( G );
   *this = EmbeddableFamily::_compute( G );
}

bool EmbeddingCongruence::operator==( const Congruence& c ) const
{
   // We assume that this function is used only to compare the congruence state of a graph
   // with one of its subgraphs/minors/immersions/Y \Delta transform etc, and that
   // the RHS argument is always the minor! (optimisation hack!)
   // See also EmbedState::leq()
   const EmbeddingCongruence &C = *static_cast<const EmbeddingCongruence *>( &c );
   //aassert( _state->leq( *C._state ) );
   Gtimers::start( 34, "EmbedState::leq" );
   bool ret = C._state->leq( *_state );
   Gtimers::stop( 34 );
   return ret;
}
