
/*
 * K-feedback vertex set family/congruence.  
 */

/*tex
 
\file{fam_fvs.c}
\path{src/family}
\title{Definition of feedback vertex set family functions}
\classes{FvsFamily,FvsCongruence}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "family/fam_fvs.h"
#include "family/testset/fvs_testset.h"
#include "bpg/bpgutil.h"
#include "bpg/i-o/bpg2graph.h"
#include "graph/graph.h"
#include "graph/bgraph.h"
#include "graph/algorithm/fvs_ga.h"

// -- statics for module --
static int fvsFixedK;
static NonminimalPretests fvsPretests;
//static Array<NBBPG> TestSet;

// ----------- Virtual Link mechanism -------------

static bool fvsnmpIsoV( const RBBPG& g ) { return hasIsoNonboundaryVertex( g );}

static bool fvsnmpAnyDangle( const RBBPG& g ) { return findAllDangles( g ); }

static bool fvsnmpBridge( const RBBPG& g ) { return hasBridge( g ); }

static bool fvsnmp2deg2c4( const BGraph &G )
 {
//   cerr << "nmp2deg2c4: " << G << nl;

   vertNum n = G.order();

   // fast "no" check
   //
   if ( n-G.boundarySize() < 2 ) return false;

   DegreeSequence Deg(n);
   AdjLists L(n);
   //
   G.getAdjLists(L,Deg);

   int num = 0;
   VertSet deg2s(n);
   //
   int i; for (i=0; i<n; i++)
    {
      if ( Deg[i] != 2 || G.isBoundary(i)==true ) continue;
      deg2s[num++]=i;
    }

   if (num<2) 
    {
//      cerr << "false\n";
      return false;
    }

   vertNum u1,u2,v1,v2;
   //
   for (i=0; i<num; i++)
    {
     u1=L[deg2s[i]][0];
     u2=L[deg2s[i]][1];

     for (int j=i+1; j<num; j++)
       {
        v1=L[deg2s[j]][0];
        v2=L[deg2s[j]][1];

        if ( v1!=u1 && v2!=u1 ) continue;
        if ( v1!=u2 && v2!=u2 ) continue;

//        cerr << "true\n";
        return true;
       }
    }

//   cerr << "false\n";
   return false;
 }

// temp test!
//
static bool fvsnmpDeg2noK3( const BGraph &G )
 {
   vertNum n = G.order();

   DegreeSequence Deg(n);
   AdjLists L(n);
   //
   G.getAdjLists(L,Deg);

   int num = 0;
   VertSet deg2s(n);
   //
   vertNum n1,n2;
   //
   for (int i=0; i<n; i++)
    {
      if ( Deg[i] != 2 || G.isBoundary(i)==true ) continue;

      n1=L[i][0]; n2=L[i][1]; // neighbors

      if (G.isEdge(n1,n2)) continue;

      if (G.isBoundary(n1)==false || G.isBoundary(n2)==false) return true;
    }

   return false;
 }

//---------------------------------------------------------------------

FvsFamily::FvsFamily()
{
   fvsFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

//   fvsPretests.addTest( "iso vertex", fvsnmpIsoV );
   fvsPretests.addTest( "dangle", fvsnmpAnyDangle );
   fvsPretests.addTest( "deg2noK3", fvsnmpDeg2noK3 );
   fvsPretests.addTest( "2deg2c4", fvsnmp2deg2c4 );
   fvsPretests.addTest( "bridge", fvsnmpBridge );

   // move init code to congruence (use a first-usage test)
   //LForests testSet; // temp for testing
   //generateForestTests( ProbInfo::getInt( ProbInfo::PathWidth )+1, testSet );

   // Global class of test set graphs (moved below because of design flaw)
   //
   //FVS_Tests.initTests(fvsFixedK, ProbInfo::getInt( ProbInfo::PathWidth )+1);
}

void FvsFamily::_setTestset( ) 
{
cerr << "in FvsFamily::_setTestset\n";

    FVS_TestSet *ts = new FVS_TestSet;

    //aassert( fvsFixedK == 2 );
    if (fvsFixedK == 2)
      ts->initTests( fvsBrute2I, fvsFixedK, 
		   ProbInfo::getInt( ProbInfo::PathWidth )+1 );
    else
      ts->initTests( fvsFixed, fvsFixedK, 
		   ProbInfo::getInt( ProbInfo::PathWidth )+1 );

    setTestset( (TestSet*) ts);
}

bool FvsFamily::_member( const BPG& g ) const
{
#if 0
   return fvs( g, fvsFixedK );
#else
   Graph G;
   //
   bpg2graph(g,G);
   //
   if ( fvsFixedK == 2 )
      return fvsBrute2( G );
   else
      return fvsFixed( G, fvsFixedK );
#endif
}

 
const NonminimalPretests& FvsFamily::_nonminimalPretests() const
{
   return fvsPretests;
}


#ifdef FVS_TESTSET_old
//
// pass set methods

void FvsCongruence::compute( const BPG& g )
{
   if ( FVS_Tests.size() == 0 )
    {
     FVS_Tests.initTests( fvsFixedK, ProbInfo::getInt( ProbInfo::PathWidth )+1 );
    }

   FVS_Tests.getState( g, _state );
}

bool FvsCongruence::operator==( const Congruence& c ) const
{
   return _state == ((const FvsCongruence&) c)._state;
}

#else
//
// d.p. methods

// membership test, and records congruence class
//
void FvsCongruence::compute( const BPG& g )
{
   fvs( g, fvsFixedK, &_state );
}

#if 0
// If state[i] >= state[k] where i \subseteq k then forget state[i]'s
// witnesses and update decrease state[i].
//
extern void tighten( fvsState& C );

// This routine modifies the state parameters!
//
static bool tightCongEQ( fvsState& C1, fvsState& C2)
 {
   tighten(C1);  // should be done by fvs_b.c
   tighten(C2);

   return C1==C2;
 }
#endif

// tests whether two congruence classes are equal
//
bool FvsCongruence::operator==( const Congruence& c ) const
 {
   fvsState &state2 = ((FvsCongruence&) c)._state;

// now done by fvs() congruence.
#if 0
   int flags = ProbInfo::getInt( ProbInfo::CongruenceFlag );
   if ( flags & 1 ) return tightCongEQ(_state, state2);
   else 	    
#endif
   //return _state == state2;
   bool ans = (_state == state2);
   //if (ans) cerr << "";
   return ans;
 }

#endif
