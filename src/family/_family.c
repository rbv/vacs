#include "../src/family/fam_allg.c"
#include "../src/family/fam_fes.c"
#include "../src/family/fam_mp.c"
#include "../src/family/fam_pw.c"
#include "../src/family/fam_vc.c"
#include "../src/family/fam_fvs.c"
#include "../src/family/family.c"
#include "../src/family/fix_fvs.c"
#include "../src/family/probinfo.c"
#include "../src/family/fam_embedding.c"
#include "../src/family/fam_plane.c"
#include "../src/family/fam_oplane.c"
