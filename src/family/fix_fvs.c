
/*
 * K-feedback vertex set congruence tightener.  
 */

/*tex
 
\file{fix_fvs.c}
\path{src/family}
\title{Tighten FVS congruence}
\makeheader
 
xet*/

#include "family/fam_fvs.h"

#ifdef DOTIGHTEN

#include "general/sets.h"
#include "family/probinfo.h"
#include "graph/algorithm/orders_ga.h"
#include "array/parray.h"
#include "graph/lgraph.h"
#include "graph/indgrph.h"

static int bs; // global boundary size

static bool parkContains( const Park &P1, const Park &P2 );

// Maybe should be a method of LTree to compute induced trees?
//
static void getInducedTrees( const LTree &T, Array<bool> &inducedVerts, 
			LTree &inducedTree, LForest &inducedForest )
{
//cerr << "getInducedTrees: " << T << "--- inducedVerts: " << inducedVerts <<nl;
 vertNum order1 = 0;

 assert(inducedForest.size()==0);

 int i; for (i=0; i<inducedVerts.size(); i++) 
    if (inducedVerts[i]) order1++;

 vertNum order2 = T.order()-order1;

 if (order2==0)  	// no forest to mess with
  {
    inducedTree = T;
//cerr << "inducedTree (no forest): " << inducedTree << nl;
    return;
  }

 LGraph indG(order1, LTree::NoLabel),		// tree
        indF(order2, LTree::NoLabel);		// forest

 VertArray map1(order1), map2(order2), invMap(T.order());

 vertNum offset1=0, offset2=0;

 for (i=0; i<T.order(); i++)
 {
  if (inducedVerts[i])
   {
       invMap[i] = offset1;
       indG.setLabel(offset1,T.getLabel(i));
       map1[offset1++] = i;
   }
   else
   {
       invMap[i] = offset2;
       indF.setLabel(offset2,T.getLabel(i));
       map2[offset2++] = i;
   }
 }

 assert(offset1==order1 && offset2==order2);

 // do tree
 //
 for (i=0; i<order1; i++)
 {
   for (int j=0; j<T.degree(map1[i]); j++)
   {
    vertNum neigh = T.neighbor(map1[i],j);

    if ( inducedVerts[neigh] ) indG.addEdge(i,invMap[neigh]);
   }
 }

 // do forest
 //
 for (i=0; i<order2; i++)
 {
   for (int j=0; j<T.degree(map2[i]); j++)
   {
    vertNum neigh = T.neighbor(map2[i],j);

    if ( ! inducedVerts[neigh] ) indF.addEdge(i,invMap[neigh]);
   }
 }

//cerr << "IndG: " << indG << nl << "IndF: " << indF << nl;

 inducedTree = LTree(indG);

 /*
  * All the components of the remaining part of T (within indF) are trees.
  */
 Components C;
 indF.components( C );

 InducedGraph *pIFgraph;

 C.start();
 //
 while ( pIFgraph = C.next() )
  {
    // For now we'll just regenerate labeled trees rooted at
    // the first vertex in each component.
    //
    inducedForest.add( new LTree( indF, pIFgraph->reference(0) ) );
  }
 //
 C.stop();

//cerr << "inducedTree: " << inducedTree;
//cerr << "inducedForest: " << inducedForest << nl;

 return;
}

// Determine if tree T contains all trees in F by breaking up
// T into subtrees with label set induced from F.
//
static bool labeledForestContains( const LTree &T, LForest &F )
{
  /*
   * First extract first tree in forest then build induced labeled
   * tree from T.  Left over parts of T will be called the induced
   * forest.
   */
//cerr << "labeledForestContains \n tree " << T << " forest " << F << nl;

  F.start(); 
  //
  LTree *firstTree = F.next(); 
  assert(firstTree);
  //
  F.removeIterItem();
  //
  F.stop();

//cerr << "removeIterItem forest " << F << nl;

  Array<bool> inducedVerts(T.order());
  inducedVerts.fill(false);

  vertNum indx1 = 0;
  vertNum n1 	= firstTree->order();
  vertNum root1 = n1;

  // find first labeled vertex of guest tree
  //
  while (firstTree->getLabel(indx1)==LTree::NoLabel) indx1++;

  vertNum Tindx1, Tindx2;

  if ( ! T.labelIndex(firstTree->getLabel(indx1), Tindx1) ) return false;

  inducedVerts[Tindx1] = true;  // might not be any paths so add vertex now.

  if ( firstTree->degree(indx1) <= 1 ) { root1 = indx1; }

  Path P;

  while (indx1<n1-1)
  {
    vertNum indx2=indx1+1;

    // find second labeled vertex of host tree (if exists)
    //
    while (indx2 < n1 && firstTree->getLabel(indx2)==LTree::NoLabel) indx2++;

    if (indx2==n1) break;	// no more paths to add.

    //Assert( T.labelIndex(firstTree->getLabel(indx2), Tindx2) );
    if ( ! T.labelIndex(firstTree->getLabel(indx2), Tindx2) ) return false;

    T.getPath(Tindx1,Tindx2,P);

    int i; for (i=1; P[i]!=Tindx2; i++) inducedVerts[P[i]] = true;
    //
    inducedVerts[P[i]] = true;		// mark second end point too (Tindx2)

    indx1 = indx2;
    Tindx1 = Tindx2;

    if ( root1 == n1 && firstTree->degree(indx1) <= 1 ) 
     { 
	root1 = indx1;
     }

  }

  assert(root1 < n1);

  /*
   * We now have indices of tree T to build the induced tree/forest.
   */

  LTree inducedTree(LTree::NoLabel);	// K_1 with garbage label
  LForest inducedForest;

  getInducedTrees( T, inducedVerts, inducedTree, inducedForest );

  vertNum root2; 
  //
#if 1
  Assert( inducedTree.labelIndex(firstTree->getLabel(root1), root2) );
#else
  if ( ! inducedTree.labelIndex(firstTree->getLabel(root1), root2) ) 
	return false;
#endif

//cerr << "anS= isRootedHomeo " << inducedTree << " and " << *firstTree;
//cerr << "  at roots " << root2 << " and " << root1 << nl;                 "
//cerr << "ans= ... parkContains " << inducedForest << " and " << F << nl;

  bool ans = ( isRootedHomeo( inducedTree, root2, *firstTree, root1 ) && 
		(F.size()==0 || parkContains( inducedForest, F )) );

#if 0
  F.add(firstTree);
#else
  delete firstTree;
#endif

//cerr << "labeledForestContains returns " << (ans ? "True" : "False") << nl;

  return ans;
}

// Does P1 contain P2 (topological order)?
//
static bool parkContains( const Park &P1, const Park &P2 )
{
//cerr << "parkContains: " << P1 << nl << P2 << nl;
  const PArray<LTree> &hostTrees = P1.asPArray();
  vertNum hostCount=hostTrees.size();

  VertArray hostBoundary(bs);	// index to host tree with boundary points
  hostBoundary.fill(bs);	// initially nothing known

//cerr << "hostCount = " << hostCount << nl;
  int i; for (i=0; i<hostCount; i++)
  {
//cerr << "hostTrees[" << i << "]=" << *hostTrees[i] << nl;
    for (int j=0; j < hostTrees[i]->order(); j++)
     {
       Label lbl = hostTrees[i]->getLabel(j);
       if (lbl < bs) hostBoundary[lbl] = i;
     }
  }

  const PArray<LTree> &guestTrees = P2.asPArray();
  vertNum guestCount=guestTrees.size();

  Array<VertSet> guests(hostCount); // list of trees for possible match

  // See which guests can go into which hosts.
  //
  for (i=0; i<guestCount; i++)
   {
    int prospectiveHost = hostCount;

    for (int j=0; j < guestTrees[i]->order(); j++)
     {
       Label lbl = guestTrees[i]->getLabel(j);
       if (lbl < bs) 
        {
          if (hostBoundary[lbl] == bs) return false; // boundary not in host

          if (prospectiveHost == hostCount)
           {
             prospectiveHost = hostBoundary[lbl];
           }
          else // make sure same host has this boundary vertex
           {
             if (prospectiveHost != hostBoundary[lbl]) return false;
           }
        }

     } // next guest tree vertex

    assert(prospectiveHost != hostCount);  // at least one guest boundary vert

    guests[prospectiveHost].append(i);

   } // next tree

  /*
   * Finally check this guest partition against the host.
   */
  for (i=0; i<hostCount; i++)
   {
//cerr << "checking host #" << i << nl;
     if (guests[i].size()==0) continue;

     LForest gForest;

     for (int j=0; j<guests[i].size(); j++)
      {
	gForest.addDeep(guestTrees[guests[i][j]]);
      }
      
     if ( ! labeledForestContains( *hostTrees[i], gForest ) ) return false;
   }

//cerr << "Park contains = true\n";

  return true;
}

// Topologically reduce a witness set.
//
static void topoReduce( const Parks &W, Parks &minW )
{
  //minW.purgeAll(); 	// start with empty collection
  assert(minW.size()==0);

  //Array<Park*> &arrayW = W.asArray();
  const PArray<Park> &arrayW = W.asPArray();

  int numParks = arrayW.size();

  Array<bool> stillMin(numParks);
  stillMin.fill(true);

  int i; for (i=0; i<numParks; i++)
   {
    if ( stillMin[i] == false ) continue;

    int internalFlag = 0;
    //
    for (int j=0; j<numParks; j++)
     { 
       if ( stillMin[j]==false || i==j ) continue;

       switch (internalFlag)
        {
          case 0:	// nothing known yet
         
	    if ( parkContains( *(arrayW[i]), *(arrayW[j]) ) )  // i > j
             {
               stillMin[i] = false;
               break;
             }
          
	  case 1: 	// Park i is known to be below something
        
	    if ( parkContains( *(arrayW[j]), *(arrayW[i]) ) ) // j > i
             {
               internalFlag = 1;
	       stillMin[j] = false;
             }
       
        } // switch action

       if ( stillMin[i] == false ) break;

     } // next j
   } // next i

  // Finally return the collection of minimal Parks. 
  //
  for (i=0; i<numParks; i++)
  {
    if (stillMin[i]) minW.addDeep(arrayW[i]);
  }
}

// ------------------------------------------------------------------


//void tighten( fvsState& C, unsigned int super )
static void tighten( Array<Witness> &C, unsigned int super )
{
  for (int i=0; i<bs; i++)
   {
     if ( super & (1<<i) )
      {

       int sub=super-(1<<i);
       //
       if ( C[sub].killed() > C[super].killed() )
        {
         //C[sub].setKilled(C[super].killed());
         //
         C[sub] = C[super];
        }
       else if ( C[sub].killed() == C[super].killed() )
        {
         C[sub].addParks(C[super]);
        }

      }
   }
}

// **********************************************************************

// If state[i] >= state[k] where i \subseteq k then forget state[i]'s
// witnesses and update decrease state[i].
//
//void tighten( fvsState& C )
void tighten( Array<Witness> &C, fvsState &State )
{
#ifdef DFVS
//cerr << "begin tighten\n" << C << nl;
#endif

  bs = ProbInfo::getInt(ProbInfo::PathWidth)+1;

  int i; for (i=bs; i>=1; i--)
   {
    Subsets S(bs,i);

    while ( S.nextset() )
     {
      tighten( C, S.intBitVec() );
     }
   }

  // first set kill counts and then set parks ...
  //
  State.reset(C.size());
  //
//cerr << "PreState=" << State << nl;
  for (i=0; i<C.size(); i++)
   { 
     State[i] = C[i].killed(); 
//if (C[i].killed()<2)
//cout << nl << "addParks(" << C[i].killed() << "):\n" << C[i] << nl;
     State.addParks(C[i].killed(),C[i]);  // should eliminate isomorphisms.
   }

  // [ attempt toplogical reductions ]
  //
  // Cheat and modify constant reference to FVSstate.
  //
  const Array<Parks> &witRef = State.getParks();
  //
  for (i=0; i<witRef.size(); i++)
  {  
    Parks minW;
    topoReduce( witRef[i], minW );
    CAST(witRef, Array<Parks>)[i] = minW;
  }

#ifdef DFVS
//cerr << "after tighten\n" << State << nl;
#endif
}

//do we want this code?
#endif
