/*
 * Test by finding all non-iso graphs up to some degree
 */

/*tex
 
\file{fam_allg.c}
\path{src/family}
\title{Definition of all-graphs family}
\classes{}
\makeheader
 
xet*/

//#define MJDsFVS
#ifdef MJDsFVS
//#include "graph/algorithm/fvs_ga.h"
//#include "bpg/algorithm/fvs_b.h"
#include "graph/graph.h"
#include "graph/stdgraph.h"
#include "graph/algorithm/clique_ga.h"
#include "bpg/i-o/bpg2graph.h"
#endif
//
#include "family/probinfo.h"
#include "family/fam_allg.h"

// -- statics --
static int allGFixedK;
static NonminimalPretests allGPretests;

//#define MJDsEBIS
#ifdef MJDsEBIS
#include "graph/graph.h"
#include "graph/algorithm/clique_ga.h"
#include "bpg/i-o/bpg2graph.h"

bool EBIScheck( const Graph &G )
{
   int sz = G.size();
   if (sz > allGFixedK) return false;
   Graph G1(G);
   G1.complement();
   int iset = clique(G1);
   return iset+sz <= allGFixedK;
}
#endif

#include "bpg/i-o/bpg2graph.h"
extern bool within1maxPath(const Graph &G, vertNum l);
extern bool within2maxPath(const Graph &G, vertNum l);
extern bool within1maxCycle(const Graph &G, vertNum l);


static bool restrictOrder( const RBBPG& g )
{
    return g.vertices() > allGFixedK;
}

// ----------- Virtual Link mechanism -------------

AllGraphsFamily::AllGraphsFamily()
{
   allGFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

   allGPretests.addTest( "order", restrictOrder );
}


bool AllGraphsFamily::_member( const BPG& g ) const
{
   return g.vertices() <= allGFixedK;

#if 0
   Graph G; bpg2graph(g,G);
   bool result = within1maxCycle(G, allGFixedK);
   return result;
#endif

#if 0
#ifdef MJDsEBIS
   Graph G; bpg2graph(g,G);
   bool result = EBIScheck(G);
   return result;
#endif

#ifndef MJDsFVS
   bool result = g.vertices() <= allGFixedK;
   return result;
//#else

   Graph G; bpg2graph(g,G);

   if (g.vertices() > allGFixedK) return false;
   bool ans1 = fvs( g, 2 );
   bool ans2 = fvsFixed( G, 2 );

   if (ans1 != ans2)
    {
      cerr << "FVS(2) ans1 = " << ans1 << " and ans2 = " << ans2 << nl;
      cerr << "for " << g << nl;
      assert(false);
    }

   return ans1;

   G.complement();
   return  G.order() - clique(G) <= allGFixedK;
#endif
   
#endif
}

const NonminimalPretests& AllGraphsFamily::_nonminimalPretests() const
{
   return allGPretests;
}


// membership test, and records congruence class
//
void AllGraphsCongruence::compute( const BPG& )
{
}


// tests whether two congruence classes are equal
//
bool AllGraphsCongruence::operator==( const Congruence& ) const
{
   return false;
}
