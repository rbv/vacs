/*
 * Planar family/congruence.
 * Can only be used with random distinguisher tests. See Embedding family for an actual implementation.
 */

/*tex
 
\file{inc/fam_plane.c}
\path{src/family}
\title{Definition of planar family functions}
\classes{PlanarFamily, PlanarCongruence}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "bpg/bpgutil.h"	// for pretests
#include "bpg/bpgutil2.h"	// for "degree" pretest

#include "family/fam_plane.h"
#include "graph/graph.h"
#include "graph/algorithm/genus_ga.h"
#include "bpg/i-o/bpg2graph.h"

#include <iostream>

// -- statics --

static int planeFixedK;
static NonminimalPretests planePretests;

// -- pretests --

bool planenmpIsoV( const RBBPG& g )
   { return hasIsoNonboundaryVertex( g ); }

bool planenmpDangle( const RBBPG& g )
{ return findAllDangles( g ); }
   //{ return hasDangleWithOtherVertexDegreeGT1( g ); }

bool planenmpContractDeg2( const BGraph &G )
{
   vertNum n = G.order();

   DegreeSequence Deg(n);
   //
   G.degVec(Deg);

   for (int i=0; i<n; i++) 
   {
     if ( Deg.get(i)==2 && G.isBoundary(i)==false) return true;
   } 

   return false;
} 



// ----------- Virtual Link mechanism -------------

PlanarFamily::PlanarFamily()
{
   planeFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

   planePretests.addTest( "iso vertex", planenmpIsoV );
   planePretests.addTest( "dangle", planenmpDangle );
   planePretests.addTest( "degree 2", planenmpContractDeg2 );
}

bool PlanarFamily::_member( const BPG& g ) const
{
   Graph G;
   //
   bpg2graph(g,G);
   //
   return planar( G, planeFixedK );
}


const NonminimalPretests& PlanarFamily::_nonminimalPretests() const
{
   return planePretests;
}


// membership test, and records congruence class
// Not implemented: use 'embedding' family instead
//
void PlanarCongruence::compute( const BPG& g )
{
   aassert( false );
}


// tests whether two congruence classes are equal
// Not implemented: use 'embedding' family instead
//
bool PlanarCongruence::operator==( const Congruence& c ) const
{
   aassert( false );
}

