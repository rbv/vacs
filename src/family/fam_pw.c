
/*tex
 
\file{fam_pw.c}
\path{src/family}
\title{Definition of pathwidth family functions}
\classes{PathwidthFamily,PathwidthCongruence}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "family/fam_pw.h"
#include "graph/graph.h"
#include "graph/bgraph.h"
#include "graph/algorithm/pathwidth_ga.h"
#include "bpg/i-o/bpg2graph.h"
#include "bpg/bpgutil.h"


// -- statics --

static int pwFixedK;
static NonminimalPretests pwPretests;

// -- pretests --

static bool nmpIsoV( const RBBPG& g )
  { return hasIsoNonboundaryVertex( g ); }

// lazy implementation -- O(n^2) -- should use adj lists.
//
static bool nmp2dangles( const BGraph &G )
  {
   //cerr << "nmp2dangles: " << G << nl;

   vertNum n = G.order();

   DegreeSequence Deg(n);
   //
   G.degVec(Deg);

   for (int i=0; i<n; i++) 
    {
     vertNum deg1count = 0;
     //
     for (int j=0; j<n; j++) 
       {  
        if ( i==j || G.isEdge(i,j)==false || G.isBoundary(j)==true ) continue;

        if (Deg.get(j)==1 && ++deg1count >= 2) { return true; }
       }
    }

   return false;
  } 

static bool nmp2deg2row( const BGraph &G )
  {
//   cerr << "nmp2dangles: " << G << nl;

   vertNum n = G.order();

   DegreeSequence Deg(n);
   //
   G.degVec(Deg);

   // Find first non-boundary degree two vertex.
   //
   for (int i=0; i<n-1; i++) 
    {
     if ( Deg.get(i)==2 && G.isBoundary(i)==false)
       { 
         // Look for adjacent degree 2 vertex (not on boundary).
         //
         for (int j=i+1; j<n; j++) 
          {
            if (G.isEdge(i,j)==true && Deg.get(j)==2 && G.isBoundary(j)==false)
             {
               //
               // Make sure vertices i & j are NOT part of a K3
               //
               bool pass = true;
               //
               for (int k=0; k<n; k++) 
                {
                 if (i==k || k==j) continue;
                 if (G.isEdge(i,k) && G.isEdge(j,k))
                  {
                    pass = false;
                    break;
                  }
                }

               if (pass) return true;
             }
          }
       } // if deg i == 2
    } 

   return false;
  } 


// ----------- Virtual Link mechanism -------------

PathwidthFamily::PathwidthFamily()
{
   pwFixedK = ProbInfo::getInt( ProbInfo::ProblemNumber );

   pwPretests.addTest( "node has two dangles", nmp2dangles );
   //pwPretests.addTest( "consec. deg 2 nodes", nmp2deg2row );
}

bool PathwidthFamily::_member( const BPG& g ) const
{
   Graph G;
   //
   bpg2graph(g,G);
   //
//cerr << "bool PathwidthFamily::_member( const BPG& g ) const\n";
//cerr << G;
   //
   return pathwidth( G, pwFixedK );
}

const NonminimalPretests& PathwidthFamily::_nonminimalPretests() const
{
   return pwPretests;
}


// membership test, and records congruence class
//
void PathwidthCongruence::compute( const BPG& g )
{
   pathwidth( g, pwFixedK, &_state );
}


// tests whether two congruence classes are equal
//
bool PathwidthCongruence::operator==( const Congruence& c ) const
{
   // just do array comparison
   // the type cast is safe
   //
   return _state == ( (const PathwidthCongruence&) c)._state;
}


