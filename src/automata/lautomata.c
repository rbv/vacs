
// -------------------------------------------
// ------------ lautomata.c -------------------
// -------------------------------------------

/*tex

\file{lautomata.c}
\path{src/automata}
\title{Defns for linear automata class}
\classes{LAutomata}
\makeheader

xet*/

LAutomata::LAutomata()
{
   _family = 0;
}


void LAutomata::setFamily( const Family& fam )
{
   _family = &fam;
}

void LAutomata::construct( const TestSet&, GrowOrder, GrowMethod, RepMethod )
{
}

void LAutomata::construct(
   const Congruence& cong,
   GrowOrder growOrder,
   GrowMethod growMethod,
   RepMethod repMethod
)
{
   assert( growOrder == DepthFirst );
   assert( growMethod == CanonicOnly );
   assert( repMethod == LexLeast );

   if ( growOrder == DepthFirst )
   {
      // make initial recursive call
   }
   else if ( growOrder == BreadthFirst )
   {
      // 
   }
   else
      aassert(false);

}


// -----------------------------------------------------------
// construction routines

void LAutomata::constructDFS( State state )
{
   constructState( state );

   // make call for each growable child
   for ( ;; )
   {
      constructDFS( 0 );
   }
}


void constructState( State state )
{
}


