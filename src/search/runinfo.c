
// ------------------------------------------
// --------------- runinfo.c ----------------
// ------------------------------------------

/*tex
 
\file{runinfo.c}
\path{src/general}
\title{Definitions of run information class}
\classes{RunInfo}
\makeheader
 
xet*/

#include <string.h>

#include "vacs/runinfo.h"
#include "family/probinfo.h"

#include "error.h"
#include "bpg/operator.h"
#include "vacs/filesystem.h"
#include "general/system.h"

//#include <new.h>            // for new handler
#include <signal.h>         // for signaling search


// The global variable runInfo is defined here
//
RunInfo runInfo;

// statics
//
RunInfo::RunType RunInfo::_runType;
//
//int RunInfo::_managerCommandPosition;
//
char RunInfo::_hostname[ MAXHOSTNAMELEN ];

// Constructor: init variables to invalid state

RunInfo::RunInfo()
{
}

// Destructor: nothing yet
RunInfo::~RunInfo(){}


// function to handle new failure
//
static void new_fail_handler()
{
   fatalError("New failed");
}



// Initialization
//
void RunInfo::initialize( const char* filename, RunType rType )
{
   _driverFilename = filename;
   _runType = rType;

   // install new handler
   //
   std::set_new_handler( new_fail_handler );

   // process the parameter file
   //
   ProbInfo::readProbInfo( filename );

   ProbInfo::parseGraphOrder();

   // set the boundary size
   //
   _boundarySize = ProbInfo::getInt( ProbInfo::PathWidth ) + 1;

   ProbInfo::initializeFamily();
   
   // print out some information
   //
   //printHeader( cout );

   // init the operator tables
   //
   Operator::initTables( boundarySize() );

};

void RunInfo::printHeader( ostream& o )
{

   if ( runType() == Manager || runType() == Dispatch )
   {
      o << "----- VACS -----" << nl;
      o
      << "Problem:            " 
      << ProbInfo::getString( ProbInfo::ProblemName ) << nl
      << "Prob num:           " 
      << ProbInfo::getInt( ProbInfo::ProblemNumber ) << nl
      << "Run dir:            " 
      << ProbInfo::getPath( ProbInfo::Directory ) << nl
      << "Pid:                " 
      << getpid() << nl
      << "Pathwidth:          " 
      << ProbInfo::getInt( ProbInfo::PathWidth ) << nl
      << "Graph order:          " 
      << ProbInfo::getString( ProbInfo::GraphOrder ) << nl
      << "Connected: " 
      << boolToString( ProbInfo::getBool( ProbInfo::Connected ) ) << nl
      << "Biconnected: " 
      << boolToString( ProbInfo::getBool( ProbInfo::Biconnected ) ) << nl
      << "Tree: " 
      << boolToString( ProbInfo::getBool( ProbInfo::KeepTree ) ) << nl
      << "Start:              " 
      << date << nl;
   }
   else
   {
      o << "----- VACS -----" << nl;
      o << "Pid:       " << getpid() << nl;
      o << "Start:     " << date << nl;
   }
}

// --------------------------------------------------------------

// --- Signals ---

// Catch incoming signal.
// Can't be a friend if it's static...hence extern.
//
void signalTrap(int)
{
   runInfo.setSignal();
}

#if 0
void RunInfo::installSignalTrap()
{
   assert( runType() == Manager || runType() == Dispatch );

   ::signal( SIGALRM, (SIG_PF) signalTrap );
}


bool RunInfo::sendSignalToPID( int pid )
{
   assert( runType() == Manager );

   if ( pid == 0 ) return false;

   int r = kill( pid, SIGALRM );

   if ( r == -1 ) return false;

   return true;
}


bool RunInfo::sendSignalToManager()
{
   int pid = getManagerPID();

   if ( pid == 0 ) return false;

   int r = kill( pid, SIGALRM );

   if ( r == -1 ) return false;

   return true;
}


// --------------------------------------------------------------
// --------------------------------------------------------------


// first run initialization
//
void RunInfo::initializeManagerStatus()
{
}


// Save manager status to disk.
// (NO LONGER USED)
//
void RunInfo::saveManagerStatus( bool shutdown )
{
   assert( runType() == Manager );

   int sd = shutdown ? 0 : 1;

   ofstream file;
   FileManager::openManagerStatusFileForWrite( file );

   assert( file );

   file << getPid() << ' ';                 // write the pid
   file << sd << ' ';                       // write the running status
   file << managerCommandPosition() << ' '; // write the command file position
   file << NodeNumber::nextNumber() << ' '; // write last used node num
   file << getHostname() << ' ';            // write the hostname
   file << nl;

   assert( file );

   FileManager::closeManagerStatusFile( file );
}


void RunInfo::loadManagerStatus()
{
   assert( runType() == Manager );

   ifstream file;

   FileManager::openManagerStatusFileForRead( file );

   assert( file );

   int i;
   file >> i;  // pid
   file >> i;  // running

   file >> i;  // command position
   managerCommandPosition( i );

   file >> i;
   //NodeNumber::lastNumber( i );

   assert( file );

   FileManager::closeManagerStatusFile( file );

}


int RunInfo::getManagerPID()
{
   assert( runType() != Manager );

   ifstream file;
   FileManager::openManagerStatusFileForRead( file );

   if (!file) return 0;

   int pid;
   file >> pid;

   assert( file );

   FileManager::closeManagerStatusFile( file );

   // check if valid?
   //
   return pid;
}

bool RunInfo::isManagerRunning()
{

   ifstream file;
   FileManager::openManagerStatusFileForRead( file );

   if (!file) return false;

   int pid;
   int flag;
   file >> pid >> flag;

   assert( file );

   FileManager::closeManagerStatusFile( file );

   //cerr << "running flag: " << int( flag ) << nl;

   return flag;
}

void RunInfo::managerCommandPosition( int pos )
{
   _managerCommandPosition = pos;
}

void RunInfo::managerShutdown()
{
   saveManagerStatus( true );
}
#endif
