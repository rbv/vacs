
// -------------------------------------------
// ------------------ search.c ---------------
// -------------------------------------------

/*tex
 
\file{search.c}
\path{src/general}
\title{Definitions for search class}
\classes{Search}
\makeheader
 
xet*/

#include "vacs/filesystem.h"
#include "general/database_v.h"
#include "general/database_f.h"
#include "general/stream.h"
#include "general/gtimers.h"
#include "vacs/runinfo.h"
#include "search/searchutil.h"
#include "search/searchnode.h"
#include "search/proof_db.h"
#include "family/probinfo.h"

#include "search/search.h"

// definition of global
//
Search search;

template<>
int Search::addToDB( MinimalProof &proof ) { return _mProofs->add( proof ); };
int Search::addToDB( MinimalProof &proof, PartialMinorProof& pmp ) { return _mProofs->add( proof, pmp ); };
template<>
int Search::addToDB( NonminimalProof &proof ) { return _nProofs->add( proof ); };
template<>
int Search::addToDB( UnknownState &proof ) { return _uState->add( proof ); };
template<>
int Search::addToDB( IrrelevantInfo &proof ) { return _iInfo->add( proof ); };

//--------------------------------------------------------------------

bool Search::isValid( NodeNumber n ) const
{
   return _snNodeDB->exists( n.asInt() );
}

SearchNode& Search::node( NodeNumber n )
{
   assert( isValid( n ) );

   if ( _snNodeDB->isCaching() )
   {
      //cerr << "---- " << n << ' ' << (int) _snNodeDB->getCache( n.asInt() ) << "----" << nl;
      return *( (SearchNode*) _snNodeDB->getCache( n.asInt() ) );
   }

   //cerr << "non cache node access" << nl;
   _snNodeDB->get( n.asInt(), (char*) _cacheNode );
   return *_cacheNode;
}

void Search::readNoCache( NodeNumber n, SearchNode& sn )
{
   _snNodeDB->get( n.asInt(), (char*) &sn, true );
}

void Search::graph( NodeNumber n, RBBPG& G )
{
   const SearchNode* sn = &node(n);
   G.setEmpty();
   G.resize( G.boundarySize() + sn->depth() );

   int i = G.size() - 1;
   while( sn->depth() != 0 )
   {
      G.setOp( i--, sn->op() );
      sn = &node( sn->parent() );
   }
   assert( i == G.boundarySize() - 1 );
}

//--------------------------------------------------------------------

void Search::minimalProof( NodeNumber n, MinimalProof& proof )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Minimal );
   _mProofs->getInfo( sn.minimalProof(), proof );
}

void Search::minimalProofPMP( NodeNumber n, PartialMinorProof& pmp )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Minimal );
   _mProofs->getPMP( sn.minimalProof(), pmp );
}

//-------------------------------

void Search::nonminimalProof( NodeNumber n, NonminimalProof& proof )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Nonminimal );
   _nProofs->getInfo( sn.nonminimalProof(), proof );
}

//-------------------------------

void Search::unknownState( NodeNumber n, UnknownState& state )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Unknown );
   _uState->getInfo( sn.unknownState(), state );
}

#if 0
void Search::unknownStatePMP( NodeNumber n, PartialMinorProof& pmp )
{
   SearchNode& sn = node( n );
   if ( sn.status() != SearchNode::Unknown ) return;
   assert( sn.status() == SearchNode::Unknown );
   //cerr << "US PMP get " << sn.unknownState() << nl;
   _uState->getPMP( sn.unknownState(), pmp );
}
#endif

void Search::updateUnknownState( NodeNumber n, const UnknownState& state )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Unknown );
   _uState->update( sn.unknownState(), state );
}

#if 0
void Search::updateUnknownStatePMP( NodeNumber n, const PartialMinorProof& pmp )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Unknown );
   _uState->update( sn.unknownState(), pmp );
}
#endif

void Search::delUnknownState( NodeNumber n )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Unknown );
   _uState->del( sn.unknownState() );
}

void Search::updateNodeAndIso( NodeNumber n, SearchNode& sn, SearchNode::Status stat, int pfNum )
{
   //SearchNode& sn = node( n );
   int isoref = isoRef( sn );
   ProofRef proofRef;
   proofRef.setProof( stat, pfNum );
   _isoDB->proofRef( isoref, proofRef );
   sn.setProof( stat, pfNum );
   changeNode( n, sn );
}

int Search::isoRef( SearchNode sn )
{
   switch ( sn.status() )
   {
   case SearchNode::Unknown:
   {
      UnknownState state;
      _uState->getInfo( sn.unknownState(), state );
      return state.isoRef();
   }
   case SearchNode::Nonminimal:
   {
      NonminimalProof state;
      _nProofs->getInfo( sn.nonminimalProof(), state );
      return state.isoRef();
   }
   case SearchNode::Minimal:
   {
      MinimalProof state;
      _mProofs->getInfo( sn.minimalProof(), state );
      return state.isoRef();
   }
   default:
      aassert( false );
   }
}

//-------------------------------

void Search::irrelevantInfo( NodeNumber n, IrrelevantInfo& info )
{
   SearchNode& sn = node( n );
   assert( sn.status() == SearchNode::Irrelevant );
   _iInfo->getInfo( sn.irrelevantInfo(), info );
}


//-------------------------------

/*
void Search::sync( NodeNumber n )
{
   // Unimplemented (unsafe)
   _snNodeDB->sync( n.asInt() );
}
*/

void Search::sync()
{
   _snNodeDB->sync();
   _mProofs->sync();
   _nProofs->sync();
   _uState->sync();
   _iInfo->sync();
   if ( _isoDB )
      _isoDB->sync();
}

void Search::statistics( ostream& o )
{
   o << "Database stats:" << nl;
   //_snNodeDB->statistics(o);
   //_snChildDB->statistics(o);
   _mProofs->statistics(o);
   _nProofs->statistics(o);
   _uState->statistics(o);
   _iInfo->statistics(o);
   o << "Global iso stats:" << nl;
   if ( _isoDB )
      _isoDB->statistics(o);
   IsoStats::statistics(o);
   o << "Times a more canonic t-parse was found," << nl;
   o << "For unknown nodes: " << _unknownSwaps << nl;
   o << "For minimal nodes: " << _minimalSwaps << nl;
}

void Search::flush()
{
   FileManager::lockRequest( LSysW );
   _snNodeDB->flush();
   _snChildDB->flush();
   _mProofs->flush();
   _nProofs->flush();
   _uState->flush();
   _iInfo->flush();
   if ( _isoDB )
      _isoDB->flush();
   FileManager::lockRequest( USysW );
}

//-------------------------------

// ctor
//
Search::Search()
{
   _cacheNode = (SearchNode*) new char[ sizeof(SearchNode) ];
   _numGrowable = 0;
   _unknownSwaps = 0;
   _minimalSwaps = 0;
   stopReceived = false;
}

// dtor
//
Search::~Search()
{
   delete [] (char*) _cacheNode;

   delete _mProofs;
   delete _nProofs;
   delete _isoDB;

   delete _uState;
   delete _iInfo;
   delete _snNodeDB;
   delete _snChildDB;
}

void Search::updateGrowableCount( const SearchNode& sn )
{
   if ( sn.status() == SearchNode::Minimal )
   {
       if ( sn.growStatus() == SearchNode::Growable )
           _numGrowable++;
       else if ( sn.growStatus() == SearchNode::Grown )
           _numGrowable--;
   }
}

NodeNumber Search::addNode( const SearchNode& sn )
{
   assert( ! sn.hasChildren() );
   assert( RunInfo::runType() == RunInfo::Manager );

   updateGrowableCount( sn );
   NodeNumber n = NodeNumber::newNumber();
   _snNodeDB->put( n.asInt(), (char*) &sn );
   return n;
}


void Search::children( NodeNumber parent, Array<NodeNumber>& C )
{
   assert( isValid( parent ) );
   assert( node(parent).hasChildren() );

   bistream* s = _snChildDB->get( parent.asInt() );
   *s >> (Array<int>&) C;
   delete s;
}


void Search::addChildren( NodeNumber parent, const Array<NodeNumber>& C )
{
   assert( RunInfo::runType() == RunInfo::Manager );
   assert( isValid( parent ) );
   assert( node(parent).hasChildren() );

#ifdef SUNPRO
   bostream s( bostream::MemFlag() );
#else
   bostream::MemFlag m;
   bostream s( m );
   //bostream s( CAST(this,bostream::MemFlag) );
#endif
   s << C;
   _snChildDB->put( parent.asInt(), s );
}

void Search::maybeDecrementGrowableCount( SearchNode &sn )
{
   if ( sn.status() == SearchNode::Minimal && sn.growStatus() == SearchNode::Growable )
      _numGrowable--;
}

void Search::changeNode( NodeNumber n, const SearchNode& sn )
{
   assert( isValid( n ) );
   // We assume that growable minimal nodes are only ever changed into grown minimal nodes,
   // otherwise you have to call decrementGrowableCount manually. I am ashamed
   updateGrowableCount( sn );
   _snNodeDB->put( n.asInt(), (char*) &sn );
}


void Search::createDatabases()
{
   if ( ! MinimalProofDatabase::dbExists( FileManager::MDBBasename() ) )
      MinimalProofDatabase::dbCreate( FileManager::MDBBasename() );
   if ( ! NonminimalProofDatabase::dbExists( FileManager::NDBBasename() ) )
      NonminimalProofDatabase::dbCreate( FileManager::NDBBasename() );
   if ( ! IsoDatabase::dbExists( FileManager::IsoDBBasename() ) )
      IsoDatabase::dbCreate( FileManager::IsoDBBasename() );

   UnknownStateDatabase::dbCreate( FileManager::SNUEBasename() );
   IrrelevantInfoDatabase::dbCreate( FileManager::SNIIBasename() );
   FRSDatabase::dbCreate( FileManager::SNBasename(), sizeof(SearchNode) );
   VRSDatabase::dbCreate( FileManager::SNCBasename() );
}


void Search::initialize()
{
   Gtimers::start( 22, "Search::init" );

   FileManager::lockRequest( LSysR );

   // get the open modes for the databases
   //
   DatabaseBase::AccessMode mode  = FileManager::standardAccessMode();
   DatabaseBase::AccessMode cache = FileManager::standardCacheMode();

   DatabaseBase::AccessMode iiMode;
   if( RunInfo::runType() == RunInfo::Browser )
      iiMode = FileManager::standardCacheMode();
   else
      iiMode = FileManager::standardAccessMode();
   
   //DatabaseBase::AccessMode dbMode;
   //if( RunInfo::runType() == RunInfo::Manager )
   //   dbMode = FileManager::standardCacheMode();
   //else
   //   dbMode = FileManager::standardAccessMode();
   

   _mProofs   = 
    new MinimalProofDatabase( FileManager::MDBBasename(),mode,cache);
   _nProofs   =
    new NonminimalProofDatabase(FileManager::NDBBasename(),mode,cache);
   if( RunInfo::runType() == RunInfo::Manager )
   {
      _isoDB     =
        new IsoDatabase(FileManager::IsoDBBasename(), mode, cache);
   }

   _uState    =
     new UnknownStateDatabase(FileManager::SNUEBasename(),mode,cache);

   _iInfo     =
     new IrrelevantInfoDatabase( FileManager::SNIIBasename(), mode, iiMode );
   _snNodeDB  = new FRSDatabase( FileManager::SNBasename(), cache );
   _snChildDB = new VRSDatabase( FileManager::SNCBasename(), mode );

   FileManager::lockRequest( USysR );

   Gtimers::stop( 22 );
}

void Search::loadIsoDatabase()
{
   assert( RunInfo::runType() == RunInfo::Browser );
   assert( !_isoDB );

   // get the open modes for the databases
   //
   DatabaseBase::AccessMode mode  = FileManager::standardAccessMode();
   DatabaseBase::AccessMode cache = FileManager::standardCacheMode();

   _isoDB = new IsoDatabase(FileManager::IsoDBBasename(), mode, cache);
}

void Search::clearProofUsage()
{

   MinimalProofDatabase& mdb = minimalProofDatabase();

   cerr << "Clearing proof usage...";

   int ref; for ( ref=0; ref < mdb.size(); ref++ )
   {
      if ( ! mdb.exists(ref) ) continue;

      MinimalProof proof;
      mdb.getInfo( ref, proof );

      proof.notUsed();
      mdb.update( ref, proof );
   }

   NonminimalProofDatabase& ndb = nonminimalProofDatabase();

   for ( ref=0; ref < ndb.size(); ref++ )
   {
      if ( ! ndb.exists(ref) ) continue;

      NonminimalProof proof;
      ndb.getInfo( ref, proof );

      proof.notUsed();
      ndb.update( ref, proof );
   }

   cerr << "proof usage cleared.\n";
}



void Search::createRootNode()
{
   SearchNode s = SearchNode::createRootNode();
   
   //MinimalProof m( MinimalProof::Root );
   //int n = minimalProofDatabase().add( m, RBBPG() );
   //s.minimalProof( n );

   Array<SearchNode> C(1);
   Array<RBBPG> G(1);
   Array< NodeNumber > nums(1);

   C[0] = s;
   G[0] = RBBPG::emptyGraph();
   predictNodeNumbers( nums );

   nodeCreation( C, G, nums, 0, false );
}


void Search::predictNodeNumbers( Array<NodeNumber>& nums )
{
   for ( int i=0; i<nums.size(); i++ )
      nums[i] = NodeNumber( NodeNumber::nextNumber() + i );
}


Str Search::statusAsString( NodeNumber n )
{
   const char* s;
   SearchNode& sn = node( n );

   switch( sn.status() )
   {
      case SearchNode::Minimal: s = "Minimal"; break;
      case SearchNode::Unknown: s = "Unknown"; break;
      case SearchNode::Nonminimal: s = "Nonminimal"; break;
      case SearchNode::Irrelevant: s = "Irrelevant"; break;
      //case SearchNode::NotUsed: aassert(false);
      default: aassert(false);
   }

   return Str( s );
}

#if 0
char Search::statusAsChar( NodeNumber n )
{
   switch( status( n ) )
   {
      case Minimal: return 'M';
      case Unknown: return 'U';
      case Nonminimal: return 'N';
      case Irrelevant: return 'I';
      case NotUsed: aassert(false);
      default: aassert(false);
   }
}
#endif
