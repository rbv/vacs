
// -------------------------------------------
// ---------------- searchiter.c -------------
// -------------------------------------------

/*tex
 
\file{searchiter.c}
\path{src/search}
\title{Definitions for search class}
\classes{SearchNodeIter}
\makeheader
 
xet*/

#include <iostream>
/*
#include "general/database_v.h"
#include "general/database_f.h"
#include "general/stream.h"
#include "vacs/runinfo.h"
#include "search/proofs.h"
*/

#include "search/searchnode.h"
#include "search/search.h"

long long SearchNodeIter::iterations = 0;

NodeNumber SearchNodeIter::step( int dir )
{
   int size = search.numberOfNodes();

   // empty is a special case
   //
   if ( size == 0 ) return NodeNumber();

   // find next element
   //
   _pos += dir;


   for ( ; _pos >=0 && _pos < size; _pos += dir )
   {
      iterations++;
      int match;

      SearchNode& sn = search.node( NodeNumber(_pos) );

      SearchNode::Status status = sn.status();
      match = 0;
      switch ( status )
      {
         case SearchNode::Minimal:    match |= _status & Minimal; break;
         case SearchNode::Nonminimal: match |= _status & Nonminimal; break;
         case SearchNode::Unknown:    match |= _status & Unknown; break;
         case SearchNode::Irrelevant: match |= _status & Irrelevant; break;
         default: aassert( false );
      }
      assert( !(_status & SNegate) );
      if ( ! match ) continue;

      SearchNode::GrowStatus grow = sn.growStatus();
      match = 0;
      switch ( grow )
      {
         case SearchNode::Growable:    match |= _grow & Growable; break;
         case SearchNode::NotGrowable: match |= _grow & NotGrowable; break;
         case SearchNode::Grown:       match |= _grow & Grown; break;
         default: aassert( false );
      }
      assert( !( _grow & GNegate ) );
      if ( ! match ) continue;

      SearchNode::Membership family = sn.membership();
      match = 0;
      switch ( family )
      {
         case SearchNode::InF:         match |= _family & InFamily; break;
         case SearchNode::NotInF:      match |= _family & OutOfFamily; break;
         case SearchNode::NotComputed: match |= _family & NotComputed; break;
         default: aassert( false );
      }
      assert( !( _family & FNegate ) );
      if ( ! match ) continue;

      break;  // we have a match

   }  // for

   if ( _pos >= size ) { _pos = size; return NodeNumber(); }
   if ( _pos < 0 ) { _pos = -1; return NodeNumber(); }

   return NodeNumber( _pos );

}


void SearchNodeIter::moveToNode( NodeNumber n )
{
   _pos = n.getKey();

   int size = search.numberOfNodes();

   if ( _pos >= size ) _pos = size;
   if ( _pos < 0 ) _pos = -1;
}


void SearchNodeIter::forceMove( int k )
{
   _pos += k;

   int size = search.numberOfNodes();

   if ( _pos >= size ) _pos = size;
   if ( _pos < 0 ) _pos = -1;
}


