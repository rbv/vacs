
// -------------------------------------------
// ------------------ proofs.c ---------------
// -------------------------------------------

/*tex
 
\file{proofs.c}
\path{src/search}
\title{Definitions for proof classes}
\classes{}
\makeheader
 
xet*/

#include <string.h>

#include "general/stream.h"
#include "general/database_v.h"
#include "general/database_f.h"
#include "search/proof_db.h"
#include "bpg/bpgutil.h"
#include "family/family.h"

//----------------------------------------------------------------------

// play area for temporary filters for pruning loaded graphs

static bool pruneBoth( const RBBPG& graph )
{
#if 0

  BGraph G( graph );
  vertNum n = G.order();

  DegreeSequence Deg(n);
  AdjLists L(n);
  G.getAdjLists(L,Deg);

  int num = 0;
  VertSet deg2s(n);
  vertNum n1,n2;
  for (int i=0; i<n; i++)
  {
    if ( Deg[i] != 2 || G.isBoundary(i)==true ) continue;
    n1=L[i][0]; n2=L[i][1]; // neighbors
    if (G.isEdge(n1,n2)) return true;
  }
#endif

  return false;
}

static bool pruneMin( const RBBPG& G )
{
  if ( pruneBoth(G) ) return true;

  // put code here -----

  // -------------------

  return false;
}

static bool pruneNonmin( const RBBPG& G )
{
  if ( pruneBoth(G) ) return true;

  // put code here -----

  // -------------------

  return false;
}



//----------------------------------------------------------------------

ProofDatabaseBase::ProofDatabaseBase()
{
   isoHits = 0;
}

void ProofDatabaseBase::statistics( ostream& o )
{
   o 
   << "Iso hits:         " << isoHits << nl;
}

//----------------------------------------------------------------------

const char* MinimalProofDatabase::ExtInfo = ".Inf";
const char* MinimalProofDatabase::ExtPMP = ".PMP";

void MinimalProofDatabase::makeNames( const char* basename, char*& s1, char*& s2 )
{
   s1 = new char[ strlen( basename ) + strlen(ExtInfo) + 1 ];
   s2 = new char[ strlen( basename ) + strlen(ExtPMP) + 1 ];

   strcpy( s1, basename ); strcat( s1, ExtInfo );
   strcpy( s2, basename ); strcat( s2, ExtPMP );
}


MinimalProofDatabase::MinimalProofDatabase(
   const char* basename,
   DatabaseBase::AccessMode mode,
   DatabaseBase::AccessMode cacheMode
)
{
   char *s1, *s2;
   makeNames( basename, s1, s2 );

   //_infoDB = new FRSDatabase( s1, mode );
   _infoDB = new FRSDatabase( s1, cacheMode );
   _PMPDB  = new VRSDatabase( s2, mode );
   delete [] s1;
   delete [] s2;

   proofNumber.initNextFreeNumber( _infoDB->numberOfKeys() );
}

MinimalProofDatabase::~MinimalProofDatabase()
{
   delete _infoDB;
   delete _PMPDB;
}

bool MinimalProofDatabase::exists( int key )
{
   return _infoDB->exists( key );
}

void MinimalProofDatabase::flush()
{
   _PMPDB->flush();
   _infoDB->flush();
   //GraphDatabase::flush();
}

void MinimalProofDatabase::sync()
{
   _PMPDB->sync();
   _infoDB->sync();
   //GraphDatabase::sync();
}

// reset references
//
void MinimalProofDatabase::clearIsoDatabase()
{
   //cerr << "Clearing min db: " << lastIsoRef() << nl;

   // iterate over all proofs
   //
   for ( int ref=0; ref<size(); ref++ )
   {
      if ( ! exists(ref) ) continue;

      MinimalProof pf;
      getInfo( ref, pf );
      pf.isoRef(-1);
      update( ref, pf );
      //cerr << "Cleared isoref for: " << ref << ' ' << *pf << nl;
   }
   //cerr << "Minimal db clear complete" << nl;
}

void MinimalProofDatabase::statistics( ostream& o )
{
   o << "mimimal proof db:" << nl;
   o << "Size: " << size() << " mem: " << size() * sizeof(MinimalProof) << nl;
   ProofDatabaseBase::statistics(o);
}

void MinimalProofDatabase::getInfo( int key, MinimalProof& proof ) const
{
   _infoDB->get( key, (char*) &proof );
}

template<class U> bistream& operator>>( bistream&, PartialMinorProof& );

void MinimalProofDatabase::getPMP( int key, PartialMinorProof& pmp ) const
{
   bistream* is = _PMPDB->get( key );
   *is >> pmp;
   delete is;
}

int MinimalProofDatabase::add( const MinimalProof& proof )
{
   assert( ! proof.hasPMP() );

   proof.setCreation();
   int n = proofNumber.allocate();
   _infoDB->put( n, (char*) &proof );
   return n;
}

int MinimalProofDatabase::add( 
   const MinimalProof& proof, 
   const PartialMinorProof& pmp )
{
   assert( proof.hasPMP() );

   proof.setCreation();
   int n = proofNumber.allocate();
   _infoDB->put( n, (char*) &proof );
   {
#ifdef SUNPRO
      bostream s( bostream::MemFlag() );
#else
   bostream::MemFlag m;
   bostream s( m );
      //bostream s( CAST(this,bostream::MemFlag) );
#endif
      s << pmp;
      _PMPDB->put( n, s );
   }
   return n;
}

void MinimalProofDatabase::update( int n, const MinimalProof& proof )
{
   _infoDB->put( n, (char*) &proof );
}


void MinimalProofDatabase::dbCreate( const char* basename )
{
   char *s1, *s2;
   makeNames( basename, s1, s2 );

   //GraphDatabase::dbCreate( basename );
   FRSDatabase::dbCreate( s1, sizeof(MinimalProof) );
   VRSDatabase::dbCreate( s2 );
   delete [] s1;
   delete [] s2;
}

bool MinimalProofDatabase::dbExists( const char* basename )
{
   char *s1, *s2;
   makeNames( basename, s1, s2 );

   int cnt = 0;
   //if ( GraphDatabase::dbExists( basename ) ) cnt++;
   if ( FRSDatabase::dbExists( s1 ) ) cnt++;
   if ( VRSDatabase::dbExists( s2 ) ) cnt++;
   delete [] s1;
   delete [] s2;

   if ( cnt==2 ) return true;
   assert( cnt == 0 );

   return false;
}

/*
void MinimalProofDatabase::loadFromStream( bistream& s, int level, const NBBPG& prefix )
{
  MinimalProof proof;
  PartialMinorProof pmp;
  RBBPG graph;

  int loaded = 0;
  int addedI = 0;
  int addedN = 0;

  while(1)
  {
    if ( ! (loaded % 100) ) { flush(); cerr << loaded << ' ' << addedI+addedN << nl; }

    s >> proof;
    loaded++;

    if ( s.fail() )
    {
        cerr << "Error in minimal proof database\n";
        aasserten( false );
    }
    if ( proof.type() == MinimalProof::None ) break;

    proof.notUsed();

    s >> graph;
    if ( proof.hasPMP() ) s >> pmp;

    //cerr << graph << ' ';

    if ( ! BPG::equalUpToSmaller( prefix, graph ) ) 
    {
       //cerr << "not prefix\n";
       continue;
    }
    //else
    //   cerr << nl;

   if ( pruneMin( graph ) ) continue;

#if 0
   if ( loaded > 181999 ) { cerr << "limit" << nl; break; }
#endif
#if 0
    int isoRef = checkForIsomorphicEntry( graph );
    if ( isoRef <> -1 )
    {
    }
#endif
#if 0
   if ( checkForEqualEntry(graph) != -1 ) { continue; }
#endif
#if 0
   if ( ! RBBPG::isCanonic(graph) ) { cerr << "noncanonic" << nl; continue; }
#endif

    if ( proof.hasPMP() )
    {
      PartialMinorProof trash;
      if ( graph.length() > level )
         { add( proof, trash, graph ); addedI++; }
      else
         { addNoIso( proof, trash, graph ); addedN++; }
    }
    else
    {
      if ( graph.length() > level )
         { add( proof, graph ); addedI++; }
      else
         { addNoIso( proof, graph ); addedN++; }
    }

  }

  cerr << "Loaded " << loaded << ", added " << addedI+addedN << " minimal proofs" << nl;
  cerr << "       " << addedI << " with iso, " << addedN << " with no iso" << nl;
}
*/

//----------------------------------------------------------------------

const char* NonminimalProofDatabase::ExtInfo = ".Inf";

void NonminimalProofDatabase::makeNames( const char* basename, char*& s1 )
{
   s1 = new char[ strlen( basename ) + strlen(ExtInfo) + 1 ];
   strcpy( s1, basename ); strcat( s1, ExtInfo );
}


NonminimalProofDatabase::NonminimalProofDatabase(
   const char* basename,
   DatabaseBase::AccessMode mode,
   DatabaseBase::AccessMode cacheMode
)
{
   char* s;
   makeNames( basename, s );
   //_infoDB = new FRSDatabase( s, mode );
   _infoDB = new FRSDatabase( s, cacheMode );
   delete [] s;
   proofNumber.initNextFreeNumber( _infoDB->numberOfKeys() );
}

NonminimalProofDatabase::~NonminimalProofDatabase()
{
   delete _infoDB;
}

bool NonminimalProofDatabase::exists( int key )
{
   return _infoDB->exists( key );
}

void NonminimalProofDatabase::flush()
{
   _infoDB->flush();
   //GraphDatabase::flush();
}

void NonminimalProofDatabase::sync()
{
   _infoDB->sync();
   //GraphDatabase::sync();
}

void NonminimalProofDatabase::statistics( ostream& o )
{
   o << "nonmimimal proof db:" << nl;
   o << "Size: " << size() << " mem: " << size() * sizeof(NonminimalProof) << nl;
   ProofDatabaseBase::statistics(o);
}

void NonminimalProofDatabase::getInfo( int key, NonminimalProof& proof ) const
{
   _infoDB->get( key, (char*) &proof );
}

int NonminimalProofDatabase::add( 
   const NonminimalProof& proof
)
{
   proof.setCreation();

   int n = proofNumber.allocate();
   _infoDB->put( n, (char*) &proof );
   return n;
}

void NonminimalProofDatabase::update( int n, const NonminimalProof& proof )
{
   _infoDB->put( n, (char*) &proof );
}


void NonminimalProofDatabase::dbCreate( const char* basename )
{
   char *s1;
   makeNames( basename, s1 );

   //GraphDatabase::dbCreate( basename );
   FRSDatabase::dbCreate( s1, sizeof(NonminimalProof) );
   delete [] s1;
}

bool NonminimalProofDatabase::dbExists( const char* basename )
{
   char *s;
   makeNames( basename, s );

   int cnt = 0;
   //if ( GraphDatabase::dbExists( basename ) ) cnt++;
   if ( FRSDatabase::dbExists( s ) ) cnt++;
   delete [] s;

   if ( cnt==1 ) return true;
   assert(cnt == 0 );

   return false;
}

/*
void NonminimalProofDatabase::loadFromStream( bistream& s, int level, const NBBPG& prefix, bool loadAll )
{
  NonminimalProof proof;
  RBBPG graph;

  int loaded = 0;
  int addedI = 0;
  int addedN = 0;

  while(1)
  {
    if ( ! (loaded % 100) ) { flush(); cerr << loaded << ' ' << addedI+addedN << nl; }

    //if ( loaded > 40000 ) break;

    s >> proof;
    loaded++;

    if ( s.fail() )
    {
        cerr << "Error in nonminimal proof database\n";
        aasserten( false );
    }
    if ( proof.type() == NonminimalProof::None ) break;

    proof.notUsed();

    s >> graph;

    if ( ! loadAll )
    {
      if ( proof.type() == NonminimalProof::NotSaved ) continue;
      if ( proof.type() == NonminimalProof::GiveUp ) continue;
    }

    if ( ! BPG::equalUpToSmaller( prefix, graph ) ) 
    {
      //cerr << "not prefix\n";
      continue;
    }


    //cerr << proof.typeAsString() << nl;

    //if ( checkForIsomorphicEntry( graph ) != -1  ) continue;

   if ( pruneNonmin( graph ) ) continue;

#if 0
   if ( ! Family::member(graph) ) { cerr << "non member" << nl; continue; }
#endif
#if 0
   if ( loaded > 115999 ) { cerr << "limit" << nl; break; }
#endif
#if 0
   if ( checkForEqualEntry(graph) != -1 ) { continue; }
#endif
#if 0
    if ( ! RBBPG::isCanonic(graph) ) { cerr << "noncanonic" << nl; continue; }
#endif

    if ( graph.length() > level )
       { add( proof, graph ); addedI++; }
    else
       { addNoIso( proof, graph ); addedN++; }

  }

  cerr << "Loaded " << loaded << ", added " << addedI + addedN
       << " nonminimal proofs" << nl;
  cerr << "       " << addedI << " with iso, " << addedN << " with no iso" << nl;
}
*/

//----------------------------------------------------------------------

const char* UnknownStateDatabase::ExtInfo = ".Inf";
const char* UnknownStateDatabase::ExtPMP = ".PMP";

void UnknownStateDatabase::makeNames( const char* basename, char*& s1, char*& s2 )
{
   s1 = new char[ strlen( basename ) + strlen(ExtInfo) + 1 ];
   s2 = new char[ strlen( basename ) + strlen(ExtPMP) + 1 ];

   strcpy( s1, basename ); strcat( s1, ExtInfo );
   strcpy( s2, basename ); strcat( s2, ExtPMP );
}

UnknownStateDatabase::UnknownStateDatabase(
   const char* basename,
   DatabaseBase::AccessMode mode,
   DatabaseBase::AccessMode cacheMode
)
{
   char *s1, *s2;
   makeNames( basename, s1, s2 );

   _infoDB = new FRSDatabase( s1, cacheMode );
   //_PMPDB  = new VRSDatabase( s2, mode );
   delete [] s1;
   delete [] s2;
   _numUnknown = 0;
   proofNumber.initNextFreeNumber( _infoDB->numberOfKeys() );
   freeNumbers = new Queue<int>();
}

UnknownStateDatabase::~UnknownStateDatabase()
{
   delete _infoDB;
   //delete _infoDB;
   delete freeNumbers;
}

// Should really be checking the free list as well
bool UnknownStateDatabase::exists( int key )
{
   return _infoDB->exists( key );
}

void UnknownStateDatabase::flush()
{
   // Mark all unused entries
   Queue<int> *newq = new Queue<int>();
   int key;
   UnknownState us;
   us.isoRef( -1 );
   while ( freeNumbers->deque( key ) )
   {
       update( key, us );
       newq->enque( key );
   }
   delete freeNumbers;
   freeNumbers = newq;

   //_PMPDB->flush();
   _infoDB->flush();
   //GraphDatabase::flush();
}

void UnknownStateDatabase::sync()
{
   //_PMPDB->sync();
   _infoDB->sync();
   //GraphDatabase::sync();
}

// reset iso references
//
void UnknownStateDatabase::clearIsoDatabase()
{
   //cerr << "Clearing unk db: " << size() << nl;

   // iterate over all proofs
   //
   for ( int ref = 0; ref < size(); ref++ )
   {
      if ( ! exists(ref) ) continue;

      UnknownState pf;
      getInfo( ref, pf );
      pf.isoRef(-1);
      update( ref, pf );
      //cerr << "Cleared isoref for: " << ref << ' ' << *pf << nl;
   }
   //cerr << "Unknown db clear complete" << nl;
}

void UnknownStateDatabase::statistics( ostream& o )
{
   o << "unknown state db:" << nl;
   o << "Size: " << size() << " mem: " << size() * sizeof(UnknownState) << nl;
   ProofDatabaseBase::statistics(o);
}

void UnknownStateDatabase::getInfo( int key, UnknownState& state ) const
{
   _infoDB->get( key, (char*) &state );
}

#if 0
void UnknownStateDatabase::getPMP( int key, PartialMinorProof& pmp ) const
{
   bistream* is = _PMPDB->get( key );
   if ( !is ) return;
   *is >> pmp;
   delete is;
}
#endif

int UnknownStateDatabase::allocate()
{
   int key;
   if ( freeNumbers->deque( key ) )
      return key;
   return proofNumber.allocate();
}

int UnknownStateDatabase::add( 
   const UnknownState& proof
)
{
   _numUnknown++;
   int n = allocate();
   _infoDB->put( n, (char*) &proof );

   return n;
}

void UnknownStateDatabase::update( int n, const UnknownState& proof )
{
   _infoDB->put( n, (char*) &proof );
}

#if 0
void UnknownStateDatabase::update( int n, const PartialMinorProof& pmp )
{
   bostream s( CAST(this,bostream::MemFlag) );
   s << pmp;
   _PMPDB->put( n, s );
}
#endif


void UnknownStateDatabase::del( int key )
{
   //_PMPDB->del( n );
   _numUnknown--;
   freeNumbers->enque( key );
}

void UnknownStateDatabase::dbCreate( const char* basename )
{
   char *s1, *s2;
   makeNames( basename, s1, s2 );

   //GraphDatabase::dbCreate( basename );
   FRSDatabase::dbCreate( s1, sizeof(UnknownState) );
   //VRSDatabase::dbCreate( s2 );
   delete [] s1;
   delete [] s2;
}

bool UnknownStateDatabase::dbExists( const char* basename )
{
   char *s1, *s2;
   makeNames( basename, s1, s2 );

   int cnt = 0;
   //if ( GraphDatabase::dbExists( basename ) ) cnt++;
   if ( FRSDatabase::dbExists( s1 ) ) cnt++;
   //if ( FRSDatabase::dbExists( s2 ) ) cnt++;
   delete [] s1;
   delete [] s2;

   if ( cnt==1 ) return true;
   assert( cnt == 0 );

   return false;
}


//----------------------------------------------------------------------

const char* IrrelevantInfoDatabase::ExtInfo = ".Inf";

void IrrelevantInfoDatabase::makeNames( const char* basename, char*& s1 )
{
   s1 = new char[ strlen( basename ) + strlen(ExtInfo) + 1 ];
   strcpy( s1, basename ); strcat( s1, ExtInfo );
}

IrrelevantInfoDatabase::IrrelevantInfoDatabase(
   const char* basename,
   DatabaseBase::AccessMode mode,
   DatabaseBase::AccessMode cacheMode
)
{
   (void) mode;

   char* s1;
   makeNames( basename, s1 );

   _infoDB = new FRSDatabase( s1, cacheMode );
   delete [] s1;
}

IrrelevantInfoDatabase::~IrrelevantInfoDatabase()
{
   delete _infoDB;
}

void IrrelevantInfoDatabase::flush()
{
   _infoDB->flush();
}

void IrrelevantInfoDatabase::sync()
{
   _infoDB->sync();
}

void IrrelevantInfoDatabase::statistics( ostream& o )
{
   //o << "irrelevant info db:" << nl;
}

void IrrelevantInfoDatabase::loadNumberOfKeys()
{
   initNextFreeNumber( _infoDB->numberOfKeys() );
}

void IrrelevantInfoDatabase::getInfo( int key, IrrelevantInfo& info ) const
{
   _infoDB->get( key, (char*) &info );
}

int IrrelevantInfoDatabase::add( const IrrelevantInfo& info )
{
   int n = allocate();
   _infoDB->put( n, (char*) &info );
   return n;
}

void IrrelevantInfoDatabase::dbCreate( const char* basename )
{
   char *s1;
   makeNames( basename, s1 );

   FRSDatabase::dbCreate( s1, sizeof(IrrelevantInfo) );
   delete [] s1;
}

bool IrrelevantInfoDatabase::dbExists( const char* basename )
{
   char *s1;
   makeNames( basename, s1 );

   int cnt = 0;
   if ( FRSDatabase::dbExists( s1 ) ) cnt++;
   delete [] s1;

   if ( cnt==1 ) return true;
   assert(cnt == 0 );

   return false;
}
