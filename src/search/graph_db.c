// -------------------------------------------
// ------------------ iso_db.c ---------------
// -------------------------------------------

/*tex
 
\file{iso_db.c}
\path{src/search}
\title{Definitions for proof classes}
\classes{}
\makeheader
 
xet*/

#include <string.h>
#include "general/database_v.h"
#include "general/database_f.h"
#include "general/stream.h"
#include "general/dtime.h"
#include "bpg/bpggrph.h"
#include "search/graph_db.h"
#include "search/search.h"
#include "family/probinfo.h"
#include "array/hugearray.h"
#include "bpg/bpg.h"

//-----------------------------------------------------------------------

const char* GraphDatabase::ExtG = ".Grf";

void GraphDatabase::makeNames( const char* basename, char*& s1 )
{
   s1 = new char[ strlen( basename ) + strlen(ExtG) + 1 ];
   strcpy( s1, basename ); strcat( s1, ExtG );
}

GraphDatabase::GraphDatabase( const char* basename, 
   DatabaseBase::AccessMode mode,
   DatabaseBase::AccessMode /* cacheMode */
)
{
   cached = true;
   //if ( cacheMode & DatabaseBase::Cache ) cached = true;

   char* s1;
   makeNames( basename, s1 );
   _graphDB = new VRSDatabase( s1, mode );
   delete [] s1;
}

GraphDatabase::~GraphDatabase()
{
   if ( cached ) clearGraphCache();
   delete _graphDB;
}

int GraphDatabase::nextFreeNumber()
{
   return _graphDB->lastUsedKey() + 1;
}

void GraphDatabase::add( int key, const RBBPG& G )
{
#if defined(SUNPRO) 
   bostream s( bostream::MemFlag() );
#else
   bostream::MemFlag m;
   bostream s( m );
#endif
   s << G;
   _graphDB->put( key, s );

   if ( cached ) cacheAdd( G, key );
}

void GraphDatabase::graph( int key, RBBPG& G )
{
   bistream* s = _graphDB->get( key );
   *s >> G;
   delete s;
}

void GraphDatabase::sync()
{
}

void GraphDatabase::flush()
{
   _graphDB->flush();
}

void GraphDatabase::dbCreate( const char* basename )
{
   char* s1;
   makeNames( basename, s1 );
   VRSDatabase::dbCreate( s1 );
   delete [] s1;
}

bool GraphDatabase::dbExists( const char* basename )
{
   char* s1;
   makeNames( basename, s1 );
   int r = 0;
   if ( VRSDatabase::dbExists( s1 ) ) r++;
   delete [] s1;
   return r;
}

// -- Graph db cache routines
void GraphDatabase::clearGraphCache()
{
  for ( int i=0; i<_cache.size(); i++ )
    for ( int j=0; j<_cache[i].size(); j++ )
       if ( _cache[i][j] ) delete _cache[i][j];
}

void GraphDatabase::clearGraphCacheForLength( int len )
{
  for ( int i=0; i<_cache.size(); i++ )
  {
    int j = len - i;
    if ( j < 0 ) break;
    if ( j >= _cache[i].size() ) continue;
    if( ! _cache[i][j] ) continue;
    //aassert( _cache[i][j] != (void*) -1 );
    cerr << "Released from graph db: " << _cache[i][j]->size() << nl;
    delete _cache[i][j];
    _cache[i][j] = 0;
  }
}

void GraphDatabase::cacheAdd( const RBBPG& G, int ref )
{

   //cerr << "Cache add...";
   int v = G.vertices();
   int e = G.edges();

   // sizing operations
   if ( v >= _cache.size() ) _cache.resize( v+1 );
   if ( e >= _cache[v].size() ) _cache[v].resize( e+1, (DyArray<char>*) 0 );
   if ( ! _cache[v][e] ) 
   {
      // init as empty
      _cache[v][e] = new DyArray<char>( sizeof(int) );
      //  *((int*) (_cache[v][e]->baseAddr()) ) = -1;
   }
   //aassert( _cache[v][e] != (void*) -1 );

   int len = G.length();
   assert( len == v + e );

   DyArray<char>& A = *_cache[v][e];
   int sz = A.size();
   //cerr << "**" << sz << nl;

   //A.resize( sz + sizeof(int) + len );
   A.resizeAndOverAllocate( sz + sizeof(int) + len, min(sz,10000) );

   char* offset = (char*) A.baseAddr() + sz - sizeof(int);

   //  *((int*) offset) = ref; // store ref
   memcpy( offset, &ref, sizeof(int) );
   offset += sizeof(int);

   G.copyToMem( offset ); // store G
   offset += len;

   //  *((int*) offset) = -1; // put eof flag back
   int minus1 = -1;
   memcpy( offset, &minus1, sizeof(int) );
   //cerr << "complete\n";
}

void GraphDatabase::checkForEqualEntry(
   const Array<RBBPG>& graphs,       // graphs to check
   const Array<bool>& mask,          // only check true entries
   Array<int>& result )              // iso results (-1 ==> not found)
{
   assert( cached );

   //cerr << "Eql check...";

   for ( int i=0; i<graphs.size(); i++ )
   {
      if ( !mask[i] ) continue;

      result[i] = -1; // assume the worst

      int v = graphs[i].vertices();
      int e = graphs[i].edges();

      if ( v >= _cache.size() || e >= _cache[v].size() ) 
         { continue; } // no cache entries, so not found

      if ( ! _cache[v][e] ) { continue; }

      DyArray<char>& A = *_cache[v][e];

      // put graph into a chunk of memory
      int len = graphs[i].length();
      assert( len == v + e );
      char* G = new char[ len ];

      graphs[i].copyToMem( G );

      // iterate through the array
      char* offset = (char*) A.baseAddr();
      while( true )
      {
         int ref;
         // = *((int*) offset);
         memcpy( &ref, offset, sizeof(int) );
         if ( ref == -1 ) break; // end of list
 
         offset += sizeof(int);

         if ( memcmp( offset, G, len ) == 0 )
            { result[i] = ref; break; } // found

         offset += len;

      } // loop thru db

      delete [] G;

   } // loop thru passed assay of graphs

   //cerr << "complete\n";
}


#if 1
int GraphDatabase::checkForEqualEntry( const RBBPG& graph )
{
   assert( cached );

   //cerr << "Eql check...";

   int result = -1; // assume the worst

   int v = graph.vertices();
   int e = graph.edges();

   if ( v >= _cache.size() || e >= _cache[v].size() ) 
         { return -1; } // no cache entries, so not found

   if ( ! _cache[v][e] ) { return -1; }

   DyArray<char>& A = *_cache[v][e];

   // put graph into a chunk of memory
   int len = graph.length();
   assert( len == v + e );
   char* G = new char[ len ];

   graph.copyToMem( G );

   // iterate through the array
   char* offset = (char*) A.baseAddr();
   while( true )
   {
      int ref;
      // = *((int*) offset);
      memcpy( &ref, offset, sizeof(int) );
      if ( ref == -1 ) break; // end of list
 
      offset += sizeof(int);

      if ( memcmp( offset, G, len ) == 0 )
         { result = ref; break; } // found

      offset += len;

   } // loop thru db

   delete [] G;

   return result;
}


#endif

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

void IsoDBItem__compute(
        const Array<RBBPG>& graphs,
        const Array<bool>&  mask,
        Array<IsoDBItem1>&  isoReps1,
        Array<IsoDBItem2>&  isoReps2
     )
{
   int n = graphs.size();
   assert( n == mask.size() && n == isoReps1.size() && n == isoReps2.size() );
   if ( n == 0 ) return;

   IsoStats::startInit( graphs[0].length() );
   for ( int i=0; i<n; i++ )
      if ( mask[i] )
         initIsoDBItems( graphs[i], isoReps1[i], isoReps2[i] );

   IsoStats::stopInit();
}

void IsoDBItem__checkInternalIso(
        const Array<RBBPG>& graphs, 
        const Array<IsoDBItem1>& isoReps1,
        const Array<IsoDBItem2>& isoReps2,
        const Array<bool>&  mask,
        Array<int>&   result,
        int parseLen
     )
{
   int n = isoReps1.size();
   assert( n == mask.size() && n == result.size() && n == isoReps2.size() );
   if ( n == 0 ) return;

   // We must respect canonicity, returning the minimal representative amongst the siblings
   bool (BPG::*less)(const BPG &) const;
   if ( ProbInfo::getBool( ProbInfo::LexicographicCanonicity ) )
      less = &RBBPG::operator<;
   else
      less = &RBBPG::canonicLess;

   IsoStats::startCheck( parseLen );
   result.fill( -1 );

   for ( int i=0; i<n; i++ )
   {
      if ( !mask[i] || result[i] != -1  ) continue;

      for ( int j=i+1; j<n; j++ )
      {
         if ( !mask[j] || result[j] != -1 ) continue;

         //cerr << isoReps1[i] << ' ' << isoReps1[j] << nl;
         if ( isoReps1[i] == isoReps1[j] && isoReps2[i] == isoReps2[j] )
         {
            if ( (graphs[i].*less)( graphs[j] ) )
               result[j] = i;
            else
            {
               result[i] = j;
               break;
            }
         }
      }
   }
   IsoStats::stopCheck();
}

//-----------------------------------------------------------------------

long long IsoStats::iso1Checks = 0;
int IsoStats::isoHits = 0;
int IsoStats::last_len = 0;

int IsoStats::isoInits[MAX_TPARSE_LEN] = {};
long long IsoStats::iso2Checks[MAX_TPARSE_LEN] = {};
double IsoStats::isoInitTimes[MAX_TPARSE_LEN] = {};
double IsoStats::isoCheckTimes[MAX_TPARSE_LEN] = {};
double IsoStats::isoAddTime = 0.0;
double IsoStats::isoClearTime = 0.0;

void IsoStats::statistics( ostream& o )
{
   int totalInits = 0;
   long long totalIso2Checks = 0;
   double totalInitTime = 0.0;
   double totalCheckTime = 0.0;
   int first = -1, last = 0;

   for (int i = 0; i < MAX_TPARSE_LEN; i++)
   {
      totalInits += isoInits[i];
      totalIso2Checks += iso2Checks[i];
      totalInitTime += isoInitTimes[i];
      totalCheckTime += isoCheckTimes[i];

      if (isoInits[i] || iso2Checks[i])
      {
         if (first == -1)
            first = i;
         last = i;
      }
   }

   o 
   << "Total iso inits:          " << totalInits << nl
   << "Total iso init time:      " << totalInitTime << nl
   << "Total iso type 1 checks:  " << iso1Checks << nl
   << "Total iso type 2 checks:  " << totalIso2Checks << nl
   << "Total iso check time:     " << totalCheckTime << nl
   << "Total iso hits:           " << isoHits << nl
   << "Iso database add time:    " << isoAddTime << nl
   << "Iso database clear time:  " << isoClearTime << nl
   << "t-parse len   Inits     Init time    Avg ms   Iso2 checks   Check time   Avg ms*" << nl;

   for (int i = first; i <= last; i++)
   {
      char buf[90], buf2[10] = "-        ", buf3[10] = "-";
      if ( isoInits[i] )
          snprintf( buf2, 10, "%-10.3g", 1000 * (double)isoInitTimes[i] / isoInits[i] );
      if ( iso2Checks[i] )
          snprintf( buf3, 10, "%-7.3g", 1000 * (double)isoCheckTimes[i] / iso2Checks[i] );
      snprintf( buf, 90, "%-14d%-10d%-13.2f%s%-14lld%-13.2f%s\n", i, isoInits[i], isoInitTimes[i], buf2, iso2Checks[i], isoCheckTimes[i], buf3 );
      o << buf;
   }

   o << "*Not average time per Iso2 check: includes all checking\n";
   
   printIsoAlgStats( o );
}

void IsoStats::startInit(int len)
{
   assert(len < MAX_TPARSE_LEN);
   isoInitTimes[len] -= Dtime::CPUtime();
   last_len = len;
}

void IsoStats::stopInit()
{
   isoInitTimes[last_len] += Dtime::CPUtime();
   last_len = 0;
}

void IsoStats::startCheck(int len)
{
   assert(len < MAX_TPARSE_LEN);
   isoCheckTimes[len] -= Dtime::CPUtime();
   last_len = len;
}

void IsoStats::stopCheck()
{
   isoCheckTimes[last_len] += Dtime::CPUtime();
   last_len = 0;
}

void IsoStats::startAdd(int len)
{
   assert(len < MAX_TPARSE_LEN);
   isoAddTime -= Dtime::CPUtime();
   last_len = len;
}

void IsoStats::stopAdd()
{
   isoAddTime += Dtime::CPUtime();
   last_len = 0;
}

void IsoStats::startClear()
{
   isoClearTime -= Dtime::CPUtime();
   last_len = 1;
}

void IsoStats::stopClear()
{
   isoClearTime += Dtime::CPUtime();
}

//-----------------------------------------------------------------------

const char* IsoDatabase::Ext1 = ".Is1";
const char* IsoDatabase::Ext2 = ".Is2";

void IsoDatabase::makeNames( const char* basename, char*& s1, char*& s2 )
{
   s1 = new char[ strlen( basename ) + strlen(Ext1) + 1 ];
   s2 = new char[ strlen( basename ) + strlen(Ext2) + 1 ];
   strcpy( s1, basename ); strcat( s1, Ext1 );
   strcpy( s2, basename ); strcat( s2, Ext2 );
}

void IsoDatabase::truncate_invalid( DatabaseBase::AccessMode mode )
{
   cerr << "Repairing Iso database:\n";

   // First find the size of these; we can't refer
   // to nonexistant nodes.

   MinimalProofDatabase *_mProofs   = 
    new MinimalProofDatabase( FileManager::MDBBasename(),mode,mode);
   NonminimalProofDatabase *_nProofs   =
    new NonminimalProofDatabase(FileManager::NDBBasename(),mode,mode);

   int db1Size = _iso1DB->numberOfKeys();
   int db2Size = _iso2DB->lastUsedKey() + 1;

   // It's not necessary to modify these databases. They don't contain
   // isorefs. The are pointed into only by isorefs; anything orphaned
   // becomes wasted memory.

   int minDBSize = _mProofs->size();
   int nonminDBSize = _nProofs->size();
   delete _mProofs;
   delete _nProofs;

   int deleted_minimals = 0;
   int deleted_nonminimals = 0;

   for (int key = 0; key < db1Size; key++)
   {
      IsoDBItem1 iso1;
      _iso1DB->get( key, (char*)&iso1, true ); // read from file, bypassCache==true

      // This doesn't delete the nodes; it just makes them unused, wasting memory.
      
      if ( iso1.proofRef().status() == SearchNode::Minimal ) {
         if ( iso1.proofRef().minimalProof() >= minDBSize ) {
            iso1.proofRef().setUnused();
            deleted_minimals++;
            _iso1DB->put( key, (char*)&iso1 );
         }
      } else if ( iso1.proofRef().status() == SearchNode::Nonminimal ) {
         if ( iso1.proofRef().nonminimalProof() >= nonminDBSize ) {
            iso1.proofRef().setUnused();
            deleted_nonminimals++;
            _iso1DB->put( key, (char*)&iso1 );
         }
      }
   }

   printf("Orphaned %d minimal isodb1 items, %d nonminimal isodb1 items\n", deleted_minimals, deleted_nonminimals);

   if ( db1Size < db2Size )
   {
      cerr << "Truncating Iso DB 2 to length " << db1Size << "\n";
      _iso2DB->truncate( db1Size );
   }
   else if ( db2Size < db1Size )
   {
      cerr << "Truncating Iso DB 1 to length " << db2Size << "\n";
      _iso2DB->truncate( db2Size );
   }

   delete _iso1DB;
   delete _iso2DB;
   // Halt
   aassert( false );
}

// Load isodb records for all t-parses parse length (not depth!) of
// minlen or greater.
IsoDatabase::IsoDatabase( const char* basename, 
                          DatabaseBase::AccessMode mode,
                          DatabaseBase::AccessMode cacheMode,
                          int minlen,
                          bool resumingRun
)
    : _cache2_used( 0 )
{
   double time = Dtime::CPUtime();

   _hashtable.set_deleted_key( IsoDBItem() );

   char* s1;
   char* s2;

   makeNames( basename, s1, s2 );

   // Initially don't open with a cache; don't want that in
   // truncate_invalid (though maybe the override_cache arg to get()
   // is enough?)
   _iso1DB = new FRSDatabase( s1, mode );
   _iso2DB = new VRSDatabase( s2, mode );

   minCachedLen = minlen;

   IsoStats::startAdd( 1 );

   int dbSize = _iso2DB->lastUsedKey() + 1;
   if ( _iso1DB->numberOfKeys() != dbSize )
   {
      // Ideally would also check that the proofrefs in the database are valid,
      // which truncate_invalid checks., but damn slow anyway.
      cerr << "!!!!!!!!!!!!!!!! IsoDatabase iso1 and iso2 DBs have different lengths: " <<
         _iso1DB->numberOfKeys() << " vs " << dbSize << " !!!!!!!!!!!!!!!!\n";
      truncate_invalid( mode );
   }

   // Reopen
   delete _iso1DB;
   _iso1DB = new FRSDatabase( s1, cacheMode );

   _cache2.setObjectSize( sizeof(IsoDBItem2) );
   _cache2.resize( dbSize );
   _cache2_used.extendTo( dbSize );

   // Min load factor of 0 prevents the hastable from ever shrinking.
   // The overhead (for 64 bit) is 2.5 bits per hashtable bucket, which
   // is fairly  negligible even at very low occupancy.
   // However there is additional malloc overhead.
   _hashtable.min_load_factor( 0.0 );
   // _hashtable.resize() indicates expected number of elements to be inserted
   _hashtable.resize( dbSize ? dbSize : 5000 );

   // If resumingRun == false, then
   // any iso entries for nodes that were unknown when the database was last
   // saved are now invalid because the unknown DB has been deleted. Ignore
   // them; they merely become wasted space in the DB files.
   // Similarly for irrelevant (ECanonic) nodes

   for (int i = 0; i < dbSize; i++)
   {
      IsoDBItem1 &iso1 = getIso1( i );
      if ( resumingRun == false) {
         if ( iso1.proofRef().status() == SearchNode::Unknown || iso1.proofRef().status() == SearchNode::Irrelevant )
            iso1.proofRef().setUnused();
      }
      if ( ! iso1.proofRef().isUsed() )
         continue;
      if ( iso1.parseLen() < minCachedLen)
         continue;

      bistream* is = _iso2DB->get( i );
      new( &getIso2( i ) ) IsoDBItem2();
      *is >> getIso2( i );
      delete is;
      _cache2_used.set_bit( i );

      _hashtable.insert( IsoDBItem( i, &iso1, &getIso2( i ) ) );
   }

   time = Dtime::CPUtime() - time;
   log() << "Loaded " << dbSize << " IsoDBItem2s from " << s2 << "; total load time " << time << 's' << nl;

   IsoStats::stopAdd();

   delete [] s1;
   delete [] s2;
}

void IsoDatabase::eraseIso2( int key )
{
   if ( _cache2_used.get_bit( key ) )
   {
       getIso2( key ).~IsoDBItem2();
       _cache2_used.set_bit( key, false );
   }
}

IsoDatabase::~IsoDatabase()
{
   for (int i = size() - 1; i >= 0; i--)
   {
       eraseIso2( i );
   }

   delete _iso1DB;
   delete _iso2DB;
}

IsoDBItem1 &IsoDatabase::getIso1( int key )
{
   return *(IsoDBItem1 *)_iso1DB->getCache( key );
}

IsoDBItem2 &IsoDatabase::getIso2( int key )
{
   return *(IsoDBItem2 *)_cache2.offset( key );
}

void IsoDatabase::add( int key, const RBBPG& G )
{
   IsoStats::startInit( G.length() );
   IsoDBItem1 iso1;
   IsoDBItem2 iso2;
   initIsoDBItems( G, iso1, iso2 );
   IsoStats::stopInit();
   add( key, iso1, iso2 );
}

/*
void IsoDatabase::checkCorruption()
{
   for (isohashset_t::iterator it2 = _hashtable.begin(); it2 != _hashtable.end(); ++it2)
       assert( it2->iso2->isOK() );
}
*/

void IsoDatabase::add( int key, const IsoDBItem1& iso1, const IsoDBItem2& iso2 )
{
   assert( iso1.parseLen() >= minCachedLen );
   IsoStats::startAdd( iso1.parseLen() );
   int dbSize = size();
   if ( key < dbSize )
   {
      // While overwriting parts of the iso db would be safe if not in use, any existing code which does so is broken
      cerr << "isodb overwriting add " << key << ", db size = " << _hashtable.size() << "," << size() << nl;
      aassert( false );

      // We're overwriting an existing graph in the database. It may or may not be
      // isomorphic to its replacement, but needs to be removed from the hashtable first
      int count = _hashtable.erase( IsoDBItem( 0, &getIso1( key ), &getIso2( key ) ) );
      aassert( count == 1 );
      // Don't delete cached Iso2 item: will assign to it
   }

   _iso1DB->put( key, (char*) &iso1 );

#if defined(SUNPRO) 
   bostream s( bostream::MemFlag() );
#else
   bostream::MemFlag m;
   bostream s( m );
#endif
   s << iso2;
   _iso2DB->put( key, s );

   assert( key <= dbSize );

   if ( key == dbSize )
   {
      _cache2.resize( key + 1 );
      _cache2_used.extendTo( key + 1 );
      new( &getIso2( key ) ) IsoDBItem2( iso2 );
      _cache2_used.set_bit( key );
   }
   else
      getIso2( key ) = iso2;

   IsoDBItem item( key, &getIso1( key ), &getIso2( key ) );
   std::pair< isohashset_t::iterator, bool > ret = _hashtable.insert( item );
   assert( ret.second );  // check no duplicates

   IsoStats::stopAdd();
}

int IsoDatabase::add( const RBBPG& G )
{
   int key = size();
   add( key, G );
   return key;
}

int IsoDatabase::add( const IsoDBItem1& iso1, const IsoDBItem2& iso2 )
{
   int key = size();
   add( size(), iso1, iso2 );
   return key;
}

bool IsoDatabase::remove( int key )
{
   IsoDBItem1 *iso1 = &getIso1( key );
   IsoStats::startAdd( iso1->parseLen() );
   int count = _hashtable.erase( IsoDBItem( 0, iso1, &getIso2( key ) ) );
   eraseIso2( key );
   IsoStats::stopAdd();
   return count == 1;
}

void IsoDatabase::proofRef( int isoRef, ProofRef ref )
{
   assert( isoRef < size() );
   IsoDBItem1 &iso1 = getIso1( isoRef );
   iso1.proofRef() = ref;
   _iso1DB->put( isoRef, (char*)&iso1 );
}

// Clear up to but not including len (parse length, not depth!)
void IsoDatabase::clearIsoCacheUpToLength( int len )
{
   if ( len < minCachedLen ) return;

   int cleared = 0, toobig = 0;
   int dbSize = size();
   IsoStats::startClear();
   for ( int i = 0; i < dbSize; i++ )
   {
       int thislen = getIso1( i ).parseLen();
       if ( thislen >= minCachedLen && thislen < len )
       {
           if ( _cache2_used.get_bit( i ) )
           {
               cleared++;
               int count = _hashtable.erase( IsoDBItem( 0, &getIso1( i ), &getIso2( i ) ) );
               aassert( count == 1 );
               eraseIso2( i );
           }
       }
       else
           if (thislen >= minCachedLen)
               toobig++;
   }
   IsoStats::stopClear();
   cerr << nl << "Cleared from isodb: " << cleared << " remaining: " << toobig << " out of total " << size() << nl;

   minCachedLen = len;
}

void IsoDatabase::addHit( const IsoDBItem &item )
{
   switch ( item.iso1->proofRef().status() )
   {
   case SearchNode::Unknown:
      search.unknownStateDatabase().addHit();
      break;
   case SearchNode::Nonminimal:
      search.nonminimalProofDatabase().addHit();
      break;
   case SearchNode::Minimal:
      search.minimalProofDatabase().addHit();
      break;
   }
}

int IsoDatabase::checkForIsomorphicEntry(
      IsoDBItem1 &rep1,
      IsoDBItem2 &rep2 )
{
   assert( rep1.parseLen() >= minCachedLen );
   IsoStats::startCheck( rep1.parseLen() );
   IsoDBItem isoItem( 0, &rep1, &rep2 );
   isohashset_t::iterator it = _hashtable.find( isoItem );
   int ret = -1;
   if ( it != _hashtable.end() )
   {
      addHit( *it );
      ret = it->isoref;
   }
   IsoStats::stopCheck();
   return ret;
}

void IsoDatabase::checkForIsomorphicEntry(
      const Array<IsoDBItem1>& reps1,
      const Array<IsoDBItem2>& reps2,
      const Array<bool>& mask,
      Array<int>& result,
      int parseLen )
{
   int num = reps1.size();
   assert( num == reps2.size() && num == mask.size() && num == result.size() );
   if ( num == 0 ) return;

   IsoStats::startCheck( parseLen );

   IsoDBItem isoItem( 0, NULL, NULL );

   for (int i = 0; i < reps1.size(); i++)
   {
       result[i] = -1;
       if ( mask[i] )
       {
           assert( reps1[i].parseLen() >= minCachedLen );
           isoItem.iso1 = &reps1[i];
           isoItem.iso2 = &reps2[i];
           isohashset_t::iterator it = _hashtable.find( isoItem );

           if ( it != _hashtable.end() )
           {
               addHit( *it );
               result[i] = it->isoref;
           }
       }
   }
   IsoStats::stopCheck();
}

int IsoDatabase::checkForIsomorphicEntry( const RBBPG& graph )
{
   assert( graph.length() >= minCachedLen );

   IsoStats::startInit( graph.length() );
   IsoDBItem1 rep1;
   IsoDBItem2 rep2;
   initIsoDBItems( graph, rep1, rep2 );
   IsoDBItem isoItem( 0, &rep1, &rep2 );
   IsoStats::stopInit();

   int result = -1;
   IsoStats::startCheck( graph.length() );
   isohashset_t::iterator it = _hashtable.find( isoItem );

   if ( it != _hashtable.end() )
   {
       addHit( *it );
       result = it->isoref;
   }

   IsoStats::stopCheck();
   return result;
}

// Not tested!
void IsoDatabase::clearIsoDatabase()
{
   _hashtable.clear();

   for (int i = size() - 1; i >= 0; i--)
   {
       eraseIso2( i );
   }
   _cache2.resize( 0 );
   _cache2_used.recreate( 0 );
   _iso1DB->clearDatabase();
   _iso2DB->clearDatabase();
   minCachedLen = 0;
}

void IsoDatabase::statistics( ostream &os )
{
    os << "Iso database size: " << size() << " hashtable size: " << _hashtable.size() << " buckets: " << _hashtable.bucket_count() << nl;
    os << "Iso database min len: " << minCachedLen << nl;
}

void IsoDatabase::sync()
{
   _iso1DB->sync();
}

void IsoDatabase::flush()
{
   _iso1DB->flush();
   _iso2DB->flush();
}

void IsoDatabase::dbCreate( const char* basename )
{
   char* s1;
   char* s2;
   makeNames( basename, s1, s2 );
   FRSDatabase::dbCreate( s1, sizeof( IsoDBItem1 ) );
   VRSDatabase::dbCreate( s2 );
   delete [] s1;
   delete [] s2;
}

void IsoDatabase::dbDelete( const char* basename )
{
   char* s1;
   char* s2;
   makeNames( basename, s1, s2 );
   FRSDatabase::dbDelete( s1 );
   VRSDatabase::dbDelete( s2 );
   delete [] s1;
   delete [] s2;
}

bool IsoDatabase::dbExists( const char* basename )
{
   char* s1;
   char* s2;
   makeNames( basename, s1, s2 );
   int r = 0;
   if ( FRSDatabase::dbExists( s1 ) ) r++;
   if ( VRSDatabase::dbExists( s2 ) ) r++;
   delete [] s1;
   delete [] s2;
   assert( r != 1 );
   return r;
}

