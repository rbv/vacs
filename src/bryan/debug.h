#ifndef DEBUG
#define DEBUG

#include <iostream>

class Debug {
public:
	Debug(const char* where);
	~Debug();
};

#endif
