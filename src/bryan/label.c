#include "label.h"

Label NullLabel(Label::Null);
Label MinLabel(Label::Min);
Label MaxLabel(Label::Max);

ostream& operator<<(ostream& os, const Label& L)
{ 
	if (L == NullLabel) 
		os << '-';
	else
		os << (char) (L.x -1 + 'a'); 
	return os;
}


Label Int2Label(const int& i) 
{
	return Label(i+1);
}


Label operator+(const Label& a, const Label& b)
{
	Label rval(a);
	return  rval += b;
}
