#include "vs_graph.h"
#include <math.h>	// for ceil and sqrt
#include <string.h> // for strlen
#include "vlist.h"
#include "label.h"



istream& operator>>(istream& is, VS_Graph& G)
{
	int i,j,k;
	int nv; 
	is >> nv;

	assert(nv <= MaxLabel.Index());
		// Next create the vertex set 
	for (i=0;i<nv;i++) {
		G.VSet.AddElem(Int2Label(i));
		}

	// cout  << "Vertex set: " << G.VSet << "  Edges set:\n";
		// Create the adj. lists.
   for (i = 0; i<nv; i++){
	   for (j=0; j<nv; j++){
		   is >> k;
           if ( k  == 1){
			   // cout  << '(' << Int2Label(i) << ',' << Int2Label(j) << ")  ";
			   G.AdjList[i].AddElem(Int2Label(j));
			   G.AdjList[j].AddElem(Int2Label(i));
               }
           }
       }
	// cout  << "\n";
	return is;
}


ostream& operator<<(ostream& os, VS_Graph& G)
{
   Label  nv = G.VSet.Size();
   os << "Graph Number: " << G.GraphNumber << '\n';
   os << "Vertex:   Adjacency List \n";
   for (Label v = MinLabel; v <= nv; v++) {
	   os << v << "         " << G.AdjList[v.Index()] << '\n';
	   }
   return os;
}

