
A BSR message indicates that a function in your
program is about to read beyond the current stack
pointer.  The data beyond the stack pointer is
subject to change without notice, for example if
your program takes a context switch or a signal;
thus the value read by this access is not
reliable.

A common cause of a BSR is a function returning a
pointer to a local variable that has gone out of
scope.  If the caller attempts to use that
variable, this error may result.  To keep the
value valid after the called function has
returned, make such variables static.


An ABR message indicates that your program is
about to read a value from before the beginning or
after the end of an allocated block.

Common causes include:
- making an array too small (e.g. failing to
  account for the terminating NULL in a string);
- forgetting to multiply by sizeof(type) when
  allocating for an array of objects;
- using an array index too large or negative;
- failing to NULL terminate a string; or
- being off-by-one in copying elements up or down
  an array.
