#ifndef VS_GRAPH
#define VS_GRAPH

#include <iostream>
#include "b_defs.h"
#include "label.h"
#include "vlist.h"


class VS_Graph {
   VList  VSet;
   VList  AdjList[MaxN];
public:
   int 	 GraphNumber;
   // Old version:  VS_Graph(const char* gstr, int gnum);
   // New version:
   VS_Graph(int num) { GraphNumber = num; }

   friend istream& operator>>(istream& is, VS_Graph& G);


   VList Get_VSet() {
      return VSet;
      }
   int  Get_Order() {
      return VSet.Size();
      }
   VList& Get_Adj(const Label& v) {
//	  assert( VSet.Contains(v));
	  int i = v.Index();
	  return AdjList[i];
	  }
   friend ostream& operator<<(ostream& os, VS_Graph& G);
};


#endif // VS_GRAPH
