
#ifndef VLIST
#define VLIST


#include <iostream>
#include "b_defs.h"
#include "label.h"



class VList{  // esssentially a set object
	unsigned long int s;
	Label pos;        // iterator position index
	Bool IterActive;  // true if iterator is active
	void init() 
		{
			pos = 0;
			s=0L;
			IterActive = FALSE;
		};

public:
	VList() {init();}
	VList(const Label& k) 
		{
			assert(k>=NullLabel && k<=MaxLabel);
			init();
			if (k != NullLabel)
				s= (unsigned long)1<< k.Index();
		}
	VList(const VList& x) 
		{	init(); s=x.s; }

		// sizeof operator
	int Size() const;

		// assignment operator
	VList& operator=(const VList& x ) {init(); s = x.s; return *this;}

		// set inclusion testing
	Bool Contains(Label k) const 
		{	
			unsigned long i = ((unsigned long)1<< k.Index());
			return (Bool)( i == (s & i) ) ;
		}
	Bool Contains(const VList& x)  const
		{	return (Bool)(x.s == ( s & x.s ));}

	// get any label from list
	Label  GetAny() ;

		// simple iterator operations -- only one iterator can be in 
		// use at any time and no changes are allowed during the iteration
	Label& First() ;
	Label& Next() ;

   // set intersection
	VList& operator^=(const VList& x) {
      assert(!IterActive);
      s &= x.s;
      return *this;
      }
   // set union
	VList& operator+=(const VList& x) {
		assert(!IterActive);
		s |= x.s;
		return *this;
	   }
   // set difference
   VList& operator-=(const VList& x) {
      assert(!IterActive);
	  assert(Contains(x));
	  s ^= x.s;
	  return *this;
	  }


   Bool operator==(const VList& x) { return (Bool)(x.s == s);   }
   Bool operator!=(const VList& x) { return (Bool)(x.s != s);   }

   // set union with {k}
	void AddElem(const Label& k) {
		assert(!IterActive);
		VList x(k);
		s |= x.s;
		}

	void VList::DeleteElem(const Label& k) {
		assert(!IterActive);
		VList x(k);
		s ^= x.s;
		}

	friend ostream& operator<<(ostream& os, VList& x);
};


VList operator^(const VList& x, const VList& y);
VList operator+(const VList& x, const VList& y);
VList operator-(const VList& x, const VList& y);

extern VList  EmptyVList;  

#endif // VLIST
