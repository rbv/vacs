#ifndef LVERTEX
#define LVERTEX

#include "b_defs.h"
#include "label.h"
#include "vlist.h"

class Layout;
// class LayoutVertex
class  LayoutVertex {
   Label graph_label;
   Label layout_label;
   VList InList;
   VList OutList;
   friend Layout;

public:
   LayoutVertex(){/* null body*/ }
   LayoutVertex(const LayoutVertex& v){
		graph_label = v.graph_label;
		layout_label = v.layout_label;
		InList = v.InList;
		OutList = v.OutList;
		}
   ostream& PrintHeader(ostream& os);
   ostream& PrintInfo(ostream& os);
}; // class LayoutVertex;
#endif
