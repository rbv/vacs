#include "layout.h" 
#include "vlist.h"


VList EmptyVList;

int VList::Size() const
{
	int k = 0;
	for (Label i=MinLabel; i<MaxLabel; i++)
		if (Contains(i))
			k++;
	return k;
}



Label VList::GetAny() {
	for (Label rval = MinLabel; rval <= MaxLabel; rval++) 
		if (Contains(rval))
			return rval;
	return NullLabel;
}




Label& VList::First() {
   assert(!IterActive);
   IterActive = TRUE;
   for (pos = MinLabel; pos <= MaxLabel && !Contains(pos); pos++) {
	    }
   if (!Contains(pos)){
	    IterActive = FALSE;
		pos =  NullLabel;
       }
	return pos;
}

Label& VList::Next()   {
   assert(IterActive);
   for (pos++ ; pos <= MaxLabel ; pos++) {
		if (Contains(pos))
			break;
	    }
   if (!Contains(pos)){
	    IterActive = FALSE;
		pos = NullLabel;
       }
	return pos;
}





// set intersection
VList operator^(const VList& x, const VList& y)
{
   VList rval(x); 
   return (rval ^= y);
}

// set union
VList operator+(const VList& x, const VList& y)
{
   VList rval(x); 
   return rval += y;
}

// set difference
VList operator-(const VList& x, const VList& y)
{
   VList rval(x); 
   return rval -= y;
}

ostream& operator<<(ostream& os, VList& x)
{
	os << '{';
	for (Label i=MinLabel; i != MaxLabel; i++)  {
		if (x.Contains(i)){
			os << i;
			i++; 
			break;
			}
		}
	for ( ; i != MaxLabel; i++)  {
		if (x.Contains(i)){
			os << ',' << i;
			}
		}
	os << '}';
	return os;
};


