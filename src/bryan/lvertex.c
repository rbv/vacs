#include "lvertex.h"
#include "label.h"
#include "vlist.h"


ostream& LayoutVertex::PrintHeader(ostream& os)
{
	os << "Labels (graph,layout), Adjacency lists (in,out)\n";
   return os;
}

ostream& LayoutVertex::PrintInfo(ostream& os)
{
   os << '(' << graph_label << ',' << layout_label << ") (" << InList << ',' << OutList << ") \n"; 
   return os;
}

