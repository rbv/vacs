#ifndef LAYOUT
#define LAYOUT

#include "b_defs.h"
#include "debug.h"

#include "array/array.h"

#include "label.h"
#include "vlist.h"
#include "lvertex.h"
#include "vs_graph.h"




// Class Layout
class Layout {
   int 				LayoutLevel;
   int				GraphNumber;
   LayoutVertex		LV[MaxN]; // all the vertices indexed by graph label
   int				Order; // Inv.Size() + OutV.Size()
   Label			ActiveV[MaxVS+1]; // graph labels of active vertices 
   int				FL[MaxVS+1];
   Label			Best[MaxVS+1];
   VList			Back[MaxVS+1];
   Label			Last;  // layout label used
   Label			LL[MaxN];  
		/* LL is an array of graph labels in layout order
		   LL[0] = first vertex in layout
		   LL[Last.Index()] = last vertex in layout
         */
   void   addV( const Label& v, const Label& b );
   void   absorb(VList& C, Array<Label>& CLay, Label ci );
   Bool   vs1(VList& T, Array<Label>& CLay );
   VList  get_an_Out_Component(const Label& v);
   VList  get_an_In_Component(const Label& v);
   void   update();
public:
   VList          InV;  // indexed by graph label
   VList          OutV; // indexed by graph label
   int            NumActive;	// number of vertices active now.
   Layout*        Next; 
	// provide a means for something outside to make a list
	// or stack of layouts.

   // Layout( VS_Graph& G);
   Layout( );
   Layout( const Layout* L);
   Layout& operator=(VS_Graph& G);
   void  AddVertex( const Label& v );
   void  ComponentRealign();
   void  ComponentAbsorption();

   friend ostream& operator<<(ostream& os, Layout& L);
};

#endif //LAYOUT
