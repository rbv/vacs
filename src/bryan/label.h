
#ifndef LABEL
#define LABEL

#include <iostream>
#include "b_defs.h"

/*
	- Labels are used to identify vertices in a graph or layout. As such
	they are just an arbritary value.
	- We display a label as a lower case character.
	- We store a label as an unsigned char.
	- We reserve the value 0 for the Null label and impose a 
	maximum value of Max = 31.  
	- Labels can also be used to index arrays. Since array indexing
	starts at 0 we provide a method Index() which returns the label's
	char value less 1.
	This opens up lots of possibilities for off by one errors.  To reduce
	the risk I suggest you use only to label methods below.
*/

typedef unsigned char LabelType;

class Label {
	LabelType x;
public:
	typedef enum{Null=0, Min = 1, Max=31};
	Label(){ x = Null;}
	Label(const char& v){ x = (LabelType)v;}
	Label(const unsigned char& v){ x = (LabelType)v;}
	Label(const int& v){ x = (LabelType)v;}
	Label(const unsigned int& v){ x = (LabelType)v;}
	Label(const Label& v){x = v.x; }
	Label& operator=(const Label& s)			{ x = s.x; return *this;}
	Label& operator=(const int& n)			{ x = (LabelType) n; return *this;}
	Label& operator=(const unsigned int& n)	{ x = (LabelType) n; return *this;}
	Label& operator=(const char& n)			{ x = (LabelType) n; return *this;}
	Label& operator=(const unsigned char& n)	{ x = (LabelType) n; return *this;}
	int Index()const { return (int) x-1;}
	Bool operator==(const Label& v) const { return x==v.x;}
	Bool operator!=(const Label& v) const{ return x!=v.x;}
	Bool operator<=(const Label& v) const { return x<=v.x;}
	Bool operator>=(const Label& v) const { return x>=v.x;}
	Bool operator>(const Label& v)  const { return x>v.x;}
	Bool operator<(const Label& v)  const { return x<v.x;}
	void operator++() { if (x<=Max) x++;}
	void operator--() { if (x!=0) x--;}
	Label& operator+=(const Label& v) { x += v.x-1; return *this; }

	friend ostream& operator<<(ostream& os, const Label& L);
	friend Label Int2Label(const int& i);
};

// end class Label

extern Label NullLabel;
extern Label MinLabel;
extern Label MaxLabel;

Label operator+( const Label& a, const Label& b);
#endif
