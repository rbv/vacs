
// main.C
#include <iostream>
#include <stdlib.h>    // for exit()
#include <fstream>    // for ifstream, ofstream

#include "b_defs.h"
#include "debug.h"

#include "label.h"
#include "vlist.h"

#include "layout.h"
#include "vs_graph.h"


class LayoutStack 
{
	Layout* bottom;
	Layout* top;
public:
	LayoutStack(){ bottom = top = 0; }
	void Push( Layout* L) ;
	Layout* Pop();
};

void LayoutStack::Push( Layout* L) 
{
	if (top == 0)
		bottom = top = L;
	else {
		top->Next = L;
		top = L;
	}
}
Layout* LayoutStack::Pop()
{
	if (bottom == 0)
		return 0;
	if (top == bottom)
		{
		Layout* tmp = top;
		top = bottom = 0;
		return tmp;
		}
	else
		{
		Layout* tmp = bottom;
		bottom = bottom->Next;
		tmp->Next = 0;
		return tmp;
		}
	}


Layout* Search( LayoutStack* P)
{
	Layout* L;
	while ( L = P->Pop()) {
		// cout << "Popped: \n" << *L << "\nNow CA it.\n";;

		L->ComponentAbsorption();
		// cout << *L;

		if (L->NumActive == 0)
			return L;
		else {
			for (Label v =  L->OutV.First(); v != NullLabel; v = L->OutV.Next())
				{
				Layout* H = new Layout(L);
				if (!H) {
					cerr << "Out of memory!\n";
					exit(1);
					}
				// cout << "New layout with vertex " << v << " added\n";
				H->AddVertex(v);
				P->Push(H);
				}//end for
			delete L;
			}//end else
	}//end while
}//end search




int main(int argc, char* argv[])  // access command-line arguments
{
   if (argc != 2)      // test number of arguments
   {
      cerr << "USAGE: "<< argv[0] <<" file \n";
      exit(-1);
   }


   ifstream source;
   source.open(argv[1],ios::nocreate); // source file must be there
   if (!source)
   {
      cerr << "Cannot open source file " << argv[1] <<
	       " for input\n";
      exit(-1);
   }
   int Gnum = 0;
   VS_Graph G(Gnum);
   int NumOf;
   source >> NumOf;
	LayoutStack P;
	Layout L;
   while (!source.eof() && NumOf--) {
		source >> G;
		G.GraphNumber = ++Gnum;
		cout << G;

		L = G;
		// cout << L;

		Label v;
		for (v=L.OutV.First(); v != NullLabel; v = L.OutV.Next()) {
			Layout* H = new Layout(&L);
			H->AddVertex(v);
			P.Push(H);
			if ( H = Search(&P))
				{
				cout << "Graph " << Gnum << " has vertex seperation <= 2\n"
					<< *H << "\n\n\n";
				break;
				}
			else
				cout << "Graph " << Gnum << " has vertex seperation >2\n\n\n";
			}
	   }
   source.close();
   return 1;
} // main


// end of main.C

