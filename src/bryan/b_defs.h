#ifndef B_DEFS
#define B_DEFS

#include <iostream>

#include "assert.h"

#define MaxN  30
// maximum number of vertices allowed in a graph
#define MaxVS  2  
// maximum vertex seperation this system can handle
#define MaxC  15 
// maximum number of components expected during execution


#ifndef Bool
	typedef char Bool;
#endif
#ifndef FALSE
#define FALSE 0
#define TRUE !FALSE
#endif

#endif 
