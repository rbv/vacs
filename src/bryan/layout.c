
#include "debug.h"
#include "array/matrix.h"
#include "array/array.h"
#include <iomanip.h>  
#include "vlist.h"
#include "lvertex.h"
#include "layout.h"
#include <stdlib.h>

#define INFINITY 9999


void Layout::update()
{
	cout << "Update :  \n";
	int i,j,k; // index variables
		// n is the number of vertices in the layout
	int n = InV.Size();
	if (n<1)
		return;
		/*
			To update the layout means to find the active vertex set and
			the associated FL and Best and Back vectors.  To do this
			we need information about all the truncated layouts.  A
			j-truncated layout is a layout that includes only the first
			j vertices;  the rest are put back into the outside world.
		*/

		// This array of vertex lists will record the active set 
		// for each vertex
	Array<VList> active(n);
		// The associated Fl and Best information will be recorded in
	Array<int> 	fl(n);
	Array<Label> best(n);
		// As we proceed through the various truncations 
		// we need to track the vertices we have worked with
	VList curV;
		// we need some temporary storage for the truncated layouts.
		// all that is essential is the out adjacency lists
	Array<VList> truncOut(n);

		// Begin consideration of the truncated layouts.
	for(i=0; i<n; i++) {
		
		curV.AddElem(LL[i]);

		k = LL[i].Index();
		if (k <0 ) {
cout <<" Index out of range!\n";
cout << *this;
exit(1);
}

		truncOut[i] = LV[k].OutList + ( (InV - curV) ^ LV[k].InList);

		 cout << "truncOut["<<i<<"] = " << truncOut[i] << " ";
		 cout 
			<< k << ' '
			<< LL[i] << ' '
			<< LV[k].OutList << " +  ( (" 
			<< InV << " - " << curV << ") ^ " 
			<< LV[k].InList << ") = " << truncOut[i] 
			<< "\n";



		// find active set for vertex at position i
		for (j=0; j<=i; j++) {
			if(truncOut[j].Size() > 0)
				active[i].AddElem(LL[j]);
		}//end for
		// cout << "active[" << i <<"] = " << active[i] << "\n";
	}//end for

		// now that we know the active sets for each vertex 
		// we can find FL and Best for each vertex
	for (i=0; i<n; i++) { // initialize the fl array
		fl[i] = active[i].Size();
		}
	for (i=0; i<n; i++) { // now find the smallest fl value to the right
		int f = INFINITY; 
		int fx; 
		for (j = i; j<n; j++) {
			if ( f > fl[j] ) {
				f = fl[j];
				fx = j;
			}//end if
		}//end for i
			// record best FL and Best for this vertex
		fl[i] = f;
		best[i] = Int2Label(fx);
	}//end for i

	 /*
	 // cout  << "fl = " << fl << "  best = " << best 
		<< "  active[n-1] = " << active[n-1] << "\n";
		*/

		// Now we must save in the layout; the active, 
		// FL, Best, and Back vectors 
		// It is active[n-1] that contains the active vertices
		// for the current layout.
	i = 0; // use i to count the number of active vertices
	for (j=0; j<n; j++) { // for each vertex in the layout
		if ( active[n-1].Contains(LL[j])){
			ActiveV[i] = LL[j];
			FL[i] = fl[j];
			Best[i] = best[j];
			if (j == 0 )
				Back[i] = EmptyVList;
			else
				Back[i] = active[j-1];
			i++;
		}//end if
	}//end for j
	NumActive = i;
	cout << " :Update\n";
}//end function update



void Layout::ComponentAbsorption()
{
	int i,j,k;  // index variables
	// (1)  get all components in graph outside the layout
	VList Y[MaxC];
	VList Y_tmp[MaxC];
	int z = 0;
	VList outer_ones_left = OutV;
	VList tmp = get_an_Out_Component(outer_ones_left.GetAny());
	while (tmp != EmptyVList) {
		assert(z<MaxC);
		Y[z] = tmp;
		z++;
		outer_ones_left -= tmp; // delete tmp from OutV
		tmp = get_an_Out_Component( outer_ones_left.GetAny());
		}// end while
	// (2) main while loop -- absorb as many components as possible
	// in other words keep trying until no component can be absorbed
	Bool some_change = TRUE;
	while (some_change && z > 0 ) {
		some_change = FALSE;
			// transfer components
		for (i=0; i<z;i++){
			Y_tmp[i] = Y[i];
			Y[i] = NullLabel;
			}
		int z_tmp = z;
		z = 0;
			// (2.a) try the components one at a time
		for (i=0; i<z_tmp; i++) {
			VList C = Y_tmp[i];
			cout  << "Can we absorb component C = " << C << "?\n";
				// collect the active vertices that become inactive
			VList I = EmptyVList; 
			for (k=0; k<NumActive; k++) 
				if (C.Contains(LV[ActiveV[k].Index()].OutList))
				// then it becomes inactive
					I.AddElem(ActiveV[k]);
cout << "If so then the vertices in I = " << I << " become inactive.\n";
				// get the component "index"
			for(int k = NumActive; k>0; k--)
				if ( (LV[ActiveV[k-1].Index()].OutList ^ C)  != EmptyVList ){
					j = k-1;
					break;
					}
			cout 	<< "The layout has room if FL["
					<< ActiveV[j] << "] = " << FL[j]
					<< " > |" << I << "| = " << I.Size() << "\n";
			if ( FL[j] > I.Size() ) { // then there may be room
				k = MaxVS - FL[j] + I.Size();
				assert(k>=0 && k<2);
				VList T = C + I;
					// Now we need to know if vs(T) <= k 
					// and if so we need its layout.
				Array<Label> CLay(MaxN);
				if ( vs1(T,CLay) ) {
					absorb(C,CLay,Best[j]);
					some_change = TRUE;
					}// fi
				else { // stick C back onto the list
					cout << "Can not absorb C because T+C has vs > then there is room.\n";
					Y[z] = C;
					z++;
				}//end else
			}// end if there may be room
		}// end for
	}// end while
}// end function component absorption



Bool Layout::vs1(VList& T, Array<Label>& CLay )
{
	int i,j,k;
	int m = 0;
	int n = T.Size();
	// cout  << "See if T = " << T << " has vs <= 1 and if so get its layout.\n";
	if (n==1) {
		// cout   << "Isolated vertex has vs = 0.\n";
		CLay[0] = T.GetAny();
		return TRUE;
		}
		// Set up a label index for T

	Array<Label> L(n);
	Label u,v;
	for (i=0, u = T.First(); u!=NullLabel; i++, u=T.Next()) 
		L[i] = u;

		// Set up adjacency and distance matrices for T
	SquareMatrix<int> A(n);
	SquareMatrix<int> D(n);
	A.fill(0);
	D.fill(INFINITY);

	for (i=0;i<n;i++)
	{
		k = L[i].Index(); // get ith active vertex's index into LV
		VList tmp = (LV[k].OutList + LV[k].InList); 
			// assemble its entire adjacency list and 
		tmp ^= T;
			// intersect with T
			// Now for each vertex in T adjacent to the ith active vertex
		for(v = tmp.First(); v!=NullLabel; v=tmp.Next())
			for(j=0;j<n;j++) // find its index in the
				if (v==L[j]) // subgraph T
				{ // and update the arrays
					A(i,j) = 1;
					A(i,i) += 1;
					D(i,j) = 1;
				}// end for for if
	}//end for i
	
	// cout << "Adjacency matrix for T " << A;

		// Count the edges
	m = 0;
	for (i=0;i<n;i++)
		m += A(i,i);
	m /= 2;

	// cout  << "There are " << m << " edges in T.\n";

	if (m != n-1) {
		// cout  << "Too bad, T contains a cycle.\n";
		return FALSE;
		}
	
		// For each degree 3 vertex in T count the number of adjacent 
		// degree 2 vertices
	for(i=0;i<n;i++) {
		if (A(i,i) >=3) {
			j = 0;
			for (k=0;k<n;k++){
				if (i != k  && A(i,k)==1 && A(k,k) >=2)
					j++;
			}//end for k
			if ( j >= 3 ) {
				/*
				cout 	<< "Too bad, T contains " 
						<< j 
						<< " deg. 2 vertices adjacent to a degree 3 vertex\n";
				*/
				return FALSE;
				}
		}//end if
	}// end for i

	// cout  << "T has vs = 1.  Now find a layout.\n";

		// OK now T has past the test and it does have VS == 1 !
		// Now lets get its layout

		// (1) compute the distance matrix
	for(i=0;i<n;i++)
		for(j=0;j<n;j++)
			for(k=0;k<n;k++) {
				m = D(j,i) + D(i,k);
				if ( m < D(j,k) )
					D(j,k) = m;
	}//end all 3 fors
	// cout << "Computed distance matrix" << D;

		// (2) find the max in D -- the longest path in T
	m = 0;
	int a,b;
	for(i=0;i<n;i++)
		for(j=i+1;j<n;j++) {
			assert(D(i,j) != INFINITY);
			if (D(i,j) > m) {
				a = i;
				b = j;
				m = D(i,j);
			}//end if
	}//end 2 fors
	
	 /*
	 cout << "The longest path in T is from " 
			<< a << " to " 
			<< b << " with length " 
			<< m << '\n';
	*/

		// (3) do a BFS starting with a to get the labeling

		// we need a very simple Queue
	Array<int> Que(n);
	Que.fill(0);
	int h = 0; // head of Que
	int t = 0; // tail of Que
		// and an array indicating the BFI ordering
		// the extra entry is a sentinal used to terminate the BFS
	Array<int> BFI(n+1);
	BFI.fill(0);
		// m will track the BFI index
	m = 0;  
	CLay[m++] = L[a];
	BFI[a] = m;
	Que[t++] = a; // put a at the head of the queue
		// now do BFS
	while (m < n){
		k = Que[h++];
		for (i=0;i<n;i++) 
			if (i != k && A(i,k) && (BFI[i] == 0) ) {
				CLay[m++] = L[i];
				BFI[i] = m;
				Que[t++] = i;
		}//end for and if
	}//end while

		//Lets see the results
	/*
	cout << "Result of breadth first search "
		<< "BFI array  " << BFI 
		<< '\n'
		<<"CLay " << CLay
		<< '\n';
	*/
	return TRUE;

}//end method vs1




void Layout::ComponentRealign()
{
}



void Layout::absorb(VList& C, Array<Label>& CLay, Label ci )
{
/*
	C contains the list of labels to absorb into the layout
	ci is the position where these labels go
	So we have to shift some labels to the right to make room.
	CLay holds the layout order to use when adding the labels in C
*/
	Label n = Int2Label(C.Size());
	Label q,w;
	cout 
			<< "\nAbsorbing C:" << C 
			<< " to the right of " << ci 
			<< " as given in this layout:\n = " 
			<< CLay << "\n"
			<< "Shift number: " << n  << ' '
			<< " + Last = " << Last+n << "\n"
			<< "Into current layout: \n" << *this ;

	int i,j,k;
		// shift the vertices [ci, ci+1, ..., Last] to the right by n postions
	for (w= Last; w > ci; w--) {
		q = w+n;  
		i = w.Index();
		j = LL[i].Index();
		k = q.Index();
		LV[j].layout_label = q;  // new layout label
		LL[k] = LL[i];
		cout << " q,i,j,k,LV[j].layout_label,LL[k]\n"
			<< q << ' '
			<< i << ' '
			<< j << ' '
			<< k << ' '
			<< LV[j].layout_label << ' '
			<< LL[k] << ' '
			<< "\n";
		}
	Last += n ;
	cout << "\nAfter shift \n" << *this << "\n";
	j=0;
	while (CLay[j] != NullLabel)
		{
		if (C.Contains(CLay[j])) {
			ci++;
			cout << "Adding " << CLay[j] << " into position " << ci << "\n";
			addV(CLay[j++],ci);
		}
		else
			j++;
		}
	update();
	cout << "Absorb done new layout: \n" << *this << "\n";
}



Layout::Layout(const Layout* L)
{
	GraphNumber = L->GraphNumber;
	LayoutLevel = 1 + L->LayoutLevel;
	OutV = L->OutV; 
	InV = L->InV;
	NumActive = L->NumActive;
	Last = L->Last;
	Order = L->Order;
	int i;
	for (i=0;i<NumActive;i++) {
		ActiveV[i] = L->ActiveV[i];
		FL[i] = L->FL[i];
		Best[i] = L->Best[i];
		Back[i] = L->Back[i];
	}
	for(i=0;i<MaxN;i++) {
		LL[i] = L->LL[i];
		LV[i] = L->LV[i];
	}
	Next = 0;
}



/*
Layout::Layout(VS_Graph& G)
{
   GraphNumber = G.GraphNumber;
   LayoutLevel = 0;
   OutV = G.Get_VSet();
   Order = G.Get_Order();
   NumActive = 0;
   for (Label v= OutV.First(); v != NullLabel; v = OutV.Next()){
	   int k = v.Index();
	   LV[k].graph_label = v;
	   LV[k].OutList  = G.Get_Adj(v);
	   }
	Next = 0;
}
*/

Layout::Layout()
{
}

Layout& Layout::operator=(VS_Graph& G)
{
   GraphNumber = G.GraphNumber;
   LayoutLevel = 0;
   OutV = G.Get_VSet();
   Order = G.Get_Order();
   NumActive = 0;
   for (Label v= OutV.First(); v != NullLabel; v = OutV.Next()){
	   int k = v.Index();
	   LV[k].graph_label = v;
	   LV[k].OutList  = G.Get_Adj(v);
	   }
	Next = 0;
	return *this;
}




void Layout::addV( const Label& v, const Label& b)
{
	InV.AddElem(v);
	OutV.DeleteElem(v);
	int k = v.Index();
	LV[k].graph_label = v;
	LV[k].layout_label = b;
	LL[b.Index()] = v;
	for(int i=0; i<Order; i++) {
		if (i != k) {
			if ( LV[i].OutList.Contains(v)) {
				LV[i].InList.AddElem(v);
				LV[i].OutList.DeleteElem(v);
				}
			}
		}
}


void Layout::AddVertex( const Label& v)
{
	Last++;
	addV(v,Last);
	update();
}







VList Layout::get_an_Out_Component(const Label& v)
{
	// cout << "Get an out component starting with vertex "<< v ;
	VList s;
	VList vl(v);
	while (s != vl) {
		VList newV = vl - s;
		s = vl;
		for (Label u = newV.First(); u != NullLabel; u = newV.Next() ){
			vl += LV[u.Index()].OutList;
			}
		}
	assert( v==NullLabel ? vl == EmptyVList : TRUE);
	// cout << " : " << vl << '\n';
	return vl;
}


VList Layout::get_an_In_Component(const Label& v)
{
	// cout << "Get an In component starting with vertex "<< v << '\n';
	VList s;
	VList vl(v);
	while (s != vl) {
		VList newV = vl - s;
		s = vl;
		for (Label u = newV.First(); u != NullLabel; u = newV.Next()) {
			vl += LV[u.Index()].InList;
			}
		}
	return vl;
}



ostream& operator<<(ostream& os, Layout& L)
{
	os 	<< "Graph number: " << L.GraphNumber
		<< "  Layout level: " << L.LayoutLevel 
		<< "  Order: " << L.Order 
		<< '\n'
		<< "Vertex sets --  In: " << L.InV 
		<< "  Out: " << L.OutV 
		<< '\n'
		<< "Labels (graph,layout), Adjacency lists (in,out)\n";
	for (int i = 0; i<L.Order; i++)  {
		L.LV[i].PrintInfo(os); 
		}
	os << "Current layout: ";
	for(i=0;i<=L.Last.Index();i++)
		os << L.LL[i] << ' ';
	os << '\n';
	if (L.NumActive == 0)
		os << "There are no active vertices\n";
	else if (L.NumActive == 1) 
		{
		os << "There is 1 active vertex : "
			<< L.ActiveV[0] 
			<< " FL = " << L.FL[0]
			<< " Best = " << L.Best[0]
			<< " Back = " << L.Back[0]
			<< "\n";
		}
	else {
		os << "There are " << L.NumActive << " active vertices\n";
		for (i=0;i<L.NumActive;i++){
			os  << "Active[" << i << "] = " << L.ActiveV[i] 
				<< "  FL = " << L.FL[i]
				<< "  Best = " << L.Best[i]
				<< "  Back = " << L.Back[i]
				<< "\n";
		}
	}
	os << "Last: " << L.Last << "\n";
	return os;
}


#undef INFINITY
