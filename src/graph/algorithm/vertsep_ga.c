
/*
 * Find pathwidth of trees. 
 */

/*tex

\file{vertsep_ga.c}
\path{src/graph/algorithm}
\title{Definitions for finding pathwidth of trees fast}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "graph/algorithm/pathwidth_ga.h"
#include "graph/graph.h"
#include "graph/rtree.h"
#include "array/dyarray.h"

//#define DEBUG
//#include <iostream>

class VS_Label : public DyArray< bool >
{
public:

  bool _critical;

  VS_Label() : DyArray< bool >(0) { _critical = false; }

  VS_Label(vertNum d) : DyArray< bool >(d) { _critical = false; }

  ~VS_Label() {}

  bool critical( vertNum ) const; 

  void setCritical() { _critical = true; } //cerr << "setting critical\n"; }
  void unsetCritical() { _critical = false; } //cerr << "unsetting critical\n"; }

  bool column(vertNum i) const
   {
    assert( i<length() );
    return get(i);
   }

  void setColumn(vertNum i)
   {
    assert( i<length() );
//    if ( i<length() ) resize(i+1);
    put(i,true);
   }

  void unsetColumn(vertNum i)
   {
    assert( i<length() );
    put(i,false);
   }

  vertNum separation() const { return length()-1; }

  void carry() { append(true); }

  friend ostream& operator<<(ostream &o, const VS_Label &L);
};

bool VS_Label::critical(vertNum bit) const
{
   assert(bit <= separation());

   if (_critical) return true;

   // check for partial label.
   //
   if (separation() < 0 || column(bit)==false) return false;

   // look for other indicators.
   //
   for (int i=0; i<bit; i++) 
    {
     if (column(i)==true) return true;
    }

   return false;
}

ostream& operator<<(ostream &o, const VS_Label &label)
{
  for (int i=label.separation(); i>=0; i--)
   {
    o << (label.column(i) ? '1' : '0');
   }

  return o << (label._critical ? '1' : '0');
}

/*
 * 4 actions for add_labels function below.
 */
static void action1(vertNum i, vertNum &avoid, bool &carry, VS_Label &label)
{
   //cerr << "action1 i/avoid " << i << '/' << avoid << nl;

   carry = true;

   for (int c = i; c>=avoid; c--) label.unsetColumn(c);

   avoid = avoid + 1;

//   cerr << "action1 label = " << label << nl;
}

static void action2(vertNum i, vertNum &avoid, bool &carry, VS_Label &label)
{
   //cerr << "action2 i/avoid " << i << '/' << avoid << nl;

   carry = false;

   for (int c = i-1; c>=avoid; c--) label.unsetColumn(c);

   label.setColumn(i);

   label.setCritical();

   avoid = i;
}

static void action4(vertNum i, vertNum &avoid, bool &carry, VS_Label &label)
{
   //cerr << "action4 i/avoid " << i << '/' << avoid << nl;

   carry = false;

   for (int c = i-1; c>=avoid; c--) label.unsetColumn(c);

   label.setColumn(i);

   label.unsetCritical();

   avoid = i;
}

static void add_labels( const Array< VS_Label > &childLabels, 
			VS_Label &label )
{
   bool carry = false;

   vertNum d = 0;

   vertNum m = childLabels.length();

   int i; for (i=0; i<m; i++) 
    if (childLabels[i].separation() > d) d = childLabels[i].separation();

   label.resize(d+1, false);
     
   vertNum avoid = 0; // used to avoid repeatedly setting the same bits to 0

   VertArray columnCounts(d+1), criticalCounts(d+1);

   columnCounts.fill(0);
   criticalCounts.fill(0);

//cerr << "add_labels:  m = " << m << " d  = " << d << nl;
   
   // do our summing
   //
   for (int j=0; j<m; j++)
    {
#ifdef DEBUG
cerr << " counts " << columnCounts << ' ' << criticalCounts << nl;
cerr << " to count -- " << childLabels[j] << " (vs=" <<
    childLabels[j].separation() << ")" <<  nl;
#endif

     int i; for (i=0; i<=childLabels[j].separation(); i++)
      {
       if (childLabels[j].column(i))
        {
         columnCounts[i] = columnCounts[i]+1;
         //
         if (childLabels[j].critical(i)) 
          {
           criticalCounts[i] = criticalCounts[i]+1;
          }
        }
      } // i
    }

#ifdef DEBUG
cerr << " Counts " << columnCounts << ' ' << criticalCounts << nl;
#endif

//cerr << "	Before setColumn(0)/carry: " << label << "/" << carry << nl;
   // for i = 0
   //
   if (columnCounts[0]>0) carry = true;
   else 		  label.setColumn(0);
//cerr << "	After setColumn(0)/carry: " << label << "/" << carry << nl;

   for (i=1; i<=d; i++)
    {
//cerr << "i=" << i << " has initial label " << label << nl;
     switch (columnCounts[i])
      {
       case 0:		if (carry) 
                          {
                            action4(i, avoid, carry, label);
                          }
			break;

       case 1:		if (criticalCounts[i]==0) 
                          {
                            action4(i, avoid, carry, label);
                          }
                        else if (carry)
                          {
                            action1(i, avoid, carry, label);
                          }
                        else
                          {
                            //action3(i, avoid, carry, label);
         //  cerr << "0) action3 label: "  << label << nl;
			    label.setColumn(i);
         //  cerr << "1) action3 label: "  << label << nl;
                          }
                        break;

       case 2:		if (criticalCounts[i]==0)
                          {
                            action2(i, avoid, carry, label);
                            break;
                          }
                        // else do default case

       default:		action1(i, avoid, carry, label);
      }

    }

   if (carry) label.carry();
}

static void compute_label( const RootedTree &T, vertNum root, VS_Label &label )
{
#ifdef DEBUG
   cerr << "compute_label at root " << root << nl;
#endif

   vertNum numChildren = T.children(root);

   if (numChildren == 0) 
    {
      label.resize(1);
      label.setColumn(0);
      return; 
    }

   Array< VS_Label > childLabels(numChildren);
   //
   for (int i=0; i<numChildren; i++)
    {
      compute_label( T, T.child(root,i), childLabels[i] );
#ifdef DEBUG
      cerr << " childLabels["<< i << "] = " << childLabels[i] << nl;
#endif
    }

   // Combine labels from children for root's label
   //
   add_labels( childLabels, label );

#ifdef DEBUG
   cerr << "label of " << root << " = " << label << nl;
#endif
}

/*
 * global entry point to compute vertex separation / pathwidth
 */
vertNum pathwidth( const RootedTree &T )
{
   VS_Label label;

   compute_label( T, T.root(), label);

   return label.separation();
}
