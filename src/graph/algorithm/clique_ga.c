/*****************************************************************************
*
* Program Module: clique.c
*
* Author: Michael J. Dinneen
*
* Version/Edit I/02
* Edit Date:  22-Jul-92 
*
******************************************************************************
*
*  M O D I F I C A T I O N    H I S T O R Y
*
*  Ver/Edit  	  Date		Reason
*  --------    -----------      ------
*  I/01		17-Jul-91	Release version.
*
*  I/02		22-Jul-92	C++ general version.
*
*****************************************************************************/

//#define WATCH

//#include "array/array.h"
//#include "array/dyarray.h"
#include "graph/graph.h"
#include "general/bitvect.h"
//#include "general/dybitvect.h"

#define MAXORDER 100

/*
 * Clique number of graph. 
 */
vertNum clique(const Graph &G)
 {
     vertNum n = G.order();

     assert( n <= MAXORDER );

     /*
      * Set up search structure. (vertex spans and degrees)
      */
     //
     BitVector span[MAXORDER]; 
     //
     vertNum deg[MAXORDER];
     //
     vertNum degForward[MAXORDER];
     //
     int i; for (i=0; i<n; i++) 
      {
       deg[i]=degForward[i]=0;

       span[i].resize(n);

       span[i].clearAll();

       span[i].setBit(i);
      }
     //
     for (i=0; i<n-1; i++)
      {
       for (int j=i+1; j<n; j++) 
         if (G.isEdge(i,j)) 
          { 
            span[i].setBit(j);
            span[j].setBit(i);
            //
	    deg[i]++;
	    deg[j]++;
            //
	    degForward[i]++;
          }
      }

     /*
      * Local variables related to clique backtrack algorithm.
      */
     //
     vertNum curClique=0;
     //
     vertNum maxClique=1;
     //
     vertNum v[MAXORDER];	// Prospective clique vertices.
     //
     BitVector curSpan(n);
     //
     BitVector maxSpan[MAXORDER];
     //
     for (i=0; i<n; i++) maxSpan[i].resize(n);

     /*
      * Find first maximal clique.
      */
     v[curClique++]=0; 
     
     maxSpan[0]=span[0];

     curSpan.clearAll();
     //
     curSpan.setBit(0);

   while(1)
//New_start:
    {
     for (i=v[0]+1; i<n && maxSpan[curClique-1] > curSpan; i++)
      {
        if ( deg[i] >= maxClique && maxSpan[curClique-1].getBit(i) ) 
        
          if ( curSpan < span[i] )
            {
             curSpan.setBit(i);

             maxSpan[curClique].andOf( maxSpan[curClique-1], span[i] );

             v[curClique++]=i;
            }
      }

  if (maxClique<curClique)
   {
     maxClique=curClique;
#ifdef WATCH
     cerr << "Clique (" << maxClique << ") :" << v << nl;
#endif
   }

   for (curClique=curClique-1; curClique>0; curClique--)
    {

#ifdef WATCH
cerr << "curClique=" << curClique+1 << "; maxClique= " << maxClique << nl;
cerr << "curSpan: " << curSpan << nl;
#endif

     //curSpan &= (mask - (1<<v[curClique]));
     //
     curSpan.clearBit( v[curClique] );

     for (i=v[curClique]+1; i<n && maxSpan[curClique-1]>curSpan; i++)
      {
        if (deg[i] >= maxClique && maxSpan[curClique-1].getBit(i)) 
         
          if ( curSpan < span[i] )
            {
             curSpan.setBit(i);

             maxSpan[curClique].andOf( maxSpan[curClique-1], span[i] );

             v[curClique++]=i;
            }
      }

     if (maxClique<curClique)
      {
        maxClique=curClique;

#ifdef WATCH
     cerr << "Clique (" << maxClique << ") :" << v << nl;
#endif
      }

    } /* next curClique */

   /*
    * See if can start with new vertex.
    */
   for (i=v[0]+1; i<n; i++)
     if (degForward[i] >= maxClique)
      {
       v[0]=i; 
       maxSpan[0]=span[i];

       curSpan.clearAll();
       curSpan.setBit(i);
       curClique=1;

//     goto New_start;
       break;
      }

   if (i==n) return maxClique;

  } // while not done

 }
