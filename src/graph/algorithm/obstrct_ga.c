
/*tex
 
\file{obstruction_ga.c}
\path{src/graph}
\title{Check for Obstructions}
\classes{}
\makeheader
 
xet*/

#include "graph/graph.h"
#include "graph/algorithm/degree_ga.h"
//#include "graph/graphutil.h"

// debug
//#include <iostream>

/*
 * See if a graph is an minor obstruction with respect to a membership
 * function.
 */
bool minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ) )
 {
   vertNum n = G.order();

   /*
    * Confirm graph isn't in family.
    */
   if ( familyTest( G ) ) return false;

   /*
    * For every edge check that if it is deleted/contracted then
    * the resulting graph is in the family.
    */
   Graph H(G);
   //
   int i; for (i=0; i<n-1; i++ )
     for ( int j=i+1; j<n; j++)
       if ( H.isEdge(i,j) )
          {
            // delete edge
            //
            H.rmEdge(i,j);
            //
//cerr << "delete edge (" << i << ',' << j << ")\n";
            if ( familyTest( H ) == false ) return false;
            //
            H.addEdge(i,j);
          }
   /*
    */
   for ( i=0; i<n-1; i++ )
     for ( int j=i+1; j<n; j++)
       if ( G.isEdge(i,j) )
          {
            // contract edge
            //
            Graph H(G);
            //
            H.contractEdge(i,j);  
            //
//cerr << "contract edge (" << i << ',' << j << ")\n";
            if ( familyTest( H ) == false ) return false;
          }

   /*
    * We found one!
    */
   return true;
 }

/*
 * See if a graph is an minor obstruction with respect to a membership
 * function but do a pretest first.
 */
bool minorObstruction( const Graph &G, bool (*familyTest)( const Graph& ),
				       bool (*preTest)( const Graph& ) )
 {

   if ( preTest(G) ) return minorObstruction( G, familyTest );
	
   else 	     return false;
  
 }

// ---------------------------------------------------------------------
