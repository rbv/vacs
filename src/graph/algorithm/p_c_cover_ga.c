
/*
 * Check if a graph (within-k-vertices) has no path/cycles greater than l.
 */

#include "stdtypes.h"
#include "graph/graph.h"

bool maxPath(const Graph &G, vertNum k);
bool maxCycle(const Graph &G, vertNum k);

// generalized 1-VC family
//
bool within1maxPath(const Graph &G, vertNum l)
{
  int n = G.order();

  if (n<2+l) return true;

  for (int i=0; i<n; i++)
  {
    Graph H(G);

    H.rmNode(i);

    if ( maxPath(H,l) ) return true;
  }
  return false;
}

// generalized 2-VC family
//
bool within2maxPath(const Graph &G, vertNum l)
{
  int n = G.order();

  if (n<3+l) return true;

  for (int i=0; i<n; i++)
  {
    Graph H(G);
    H.rmNode(i);

    for (int j=0; j<n-1; j++)
    {

     Graph K(H);
     K.rmNode(j);

     if ( maxPath(K,l) ) return true;
    }
  }

  return false;
}

// generalized 3-VC family
//
bool within3maxPath(const Graph &G, vertNum l)
{
  int n = G.order();

  if (n<4+l) return true;

  for (int i=0; i<n; i++)
  {
    Graph H(G);
    H.rmNode(i);

    for (int j=0; j<n-1; j++)
    {

      Graph K(H);
      K.rmNode(j);

      for (int k=0; k<n-2; k++)
      {

        Graph I(K);
        I.rmNode(k);
   
        if ( maxPath(I,l) ) return true;
      }
    }
  }

  return false;
}

// ---------------------------------------------------------------------


// generalized 1-FVS family
//
bool within1maxCycle(const Graph &G, vertNum l)
{
  int n = G.order();

  if (n<1+l) return true;

  for (int i=0; i<n; i++)
  {
    Graph H(G);

    H.rmNode(i);

    if ( maxCycle(H,l) ) return true;
  }
  return false;
}

// generalized 2-FVS family
//
bool within2maxCycle(const Graph &G, vertNum l)
{
  int n = G.order();

  if (n<2+l) return true;

  for (int i=0; i<n; i++)
  {
    Graph H(G);
    H.rmNode(i);

    for (int j=0; j<n-1; j++)
    {

     Graph K(H);
     K.rmNode(j);

     if ( maxCycle(K,l) ) return true;
    }
  }

  return false;
}
