
/*
 * See if a graph is embeddible on a genus g surface.
 */

/*tex

\file{genus_ga.c}
\path{src/graph/algorithm}
\title{Definition of genus brute force functions}
\classes{}
\makeheader

xet*/

#include "graph/graph.h"
#include "graph/indgrph.h"
#include "graph/stdgraph2.h"

#include "graph/algorithm/genus_ga.h"
//#include "graph/algorithm/planar_ga.h"
//#include "graph/algorithm/torodial_ga.h"
//
//extern bool planar( const Graph &G );
//extern bool torodial( const Graph &G );
//
//extern bool planarBlock( const InducedGraph &B );
//extern bool torodialBlock( const InducedGraph &B );
//

/*
 * See if a graph is embeddible on genus g.
 */
bool genusBrute(const Graph &G, int g)
 {

  switch (g)
   {
     case 0: return planar( G );

     case 1: if ( planar( G ) ) return true;
             else		return torodial( G );

     default: aassert(false);
   }

  return false;

 }

/*
 * See if a block is embeddible on genus g.
 */
bool genusBlock(const InducedGraph &B, int g)
 {
  vertNum n = B.order();
  vertNum m = B.size();

  // fast tests.
  //
  if ( m < 9 || ( n < 5 && m == 10 ) ) return true;	// no K5 or K3,3
  //
  if ( g < m/6 - n/2 + 1 ) return false;

  switch (g)
   {
     case 0: return planarBlock( B );

     case 1: if ( planarBlock( B ) ) return true;
             else	 	     return torodialBlock( B );

     default: aassert(false);
   }

  return false;

 }

/*
 * Find the lowest genus that can be embedded on.
 */
int genusBlock(const InducedGraph &B)
 {
  for (int i=0; true; i++) if ( genusBlock( B, i ) ) return i;
 }


/*
 * Find the lowest genus a graph that can be embedded on.
 * ( Genus of graph = sum of block's genuses. ) 
 */
int genusBrute(const Graph &G)
 {
  int g = 0;
 
  Blocks blocks;
  //
  G.getBlocks( blocks );
  
  InducedGraph *gp;

  blocks.start();
  //
  while ( gp = blocks.next() )
   {
     g += genusBlock( *gp );
   }
  //
  blocks.stop();
 
  return g;
 }

