
/*
 * ShortParse class used by pathwidth algorithm.
 */

/*tex

\file{shortparse.c}
\path{src/graph/algorithm}
\title{Definitions for short-form path-decompositions}
\classes{}
\makeheader

xet*/

#include "graph/algorithm/shortparse.h"

//#define DEBUG2

// -----------------------------------------------------------------------

// Easy implementation for now.
//
bool ShortParse::contains( const VertSet &S ) const
 {
#ifdef DEBUG2
cerr << "ShortParse::contains() : S = " << S << ' ' << S.size() << ' ' << S.baseAddr() << nl;
//cerr << "Parse= " << *this << nl;
cerr << "Boundary = " << boundary() << ' ' << boundary().baseAddr() << nl;
#endif

  VertSet S2(S);

#ifdef DEBUG2
cerr << "S2 = " << S2 << ' ' << S2.size() << ' ' << S2.baseAddr() << nl;
//cerr << "Parse= " << *this << nl;
cerr << "Boundary = " << boundary() << ' ' << boundary().baseAddr() << nl;
#endif
  S2.minus(boundary());
#ifdef DEBUG1
cerr << S2 << ' ' << S2.size() << nl;
cerr << "Interior = " << interior() << nl;
#endif
  S2.minus(interior());
#ifdef DEBUG1
cerr << S2 << ' ' << S2.size() << nl;
#endif

  // Assuming no duplicates!
  //
  if (S2.size() == 0) return true;
  else return false;
 }

// -----------------------------------------------------------------------

// Start iterator for partial parses of length
//
void ShortParses::lookup(vertNum l)
 {
//cerr << "lookup(" << l << ")\n";
   _at1 = l-boundarySize();
   _at2 = -1;

   assert(_at1>=0 && _at1<_sList->size());

//cerr << "before sortUp()\n"; dump(_at1);
   (*_sList)[_at1].sortUp();
//cerr << "after sortUp()\n"; dump(_at1);
 }

//
// Check for any parses of a certian length.
//
bool ShortParses::something(vertNum l) 
 { 
   return (*_sList)[l-boundarySize()].size() ? true : false;
 }

//
// return next parse of setup length.
//
const ShortParse* ShortParses::nextParse()
 {
#ifdef DEBUG
cerr << "nextParse() " << _at1 << ' ' << _at2 << nl;
//dump(_at1);
#endif

   assert(_at1 >= 0);

   const ShortParse *P; 

   _at2++;

   int listSize = (*_sList)[_at1].size();

#ifdef DEBUG
cerr << "listSize = " << listSize << nl;
#endif

   if (_at2 >= listSize) { _at1 = -1; return 0;}

   if (_at2 == 0) 
    {
      //P = &(_sList->get(_at1)[0]);
      P = &((*_sList)[_at1][0]);
#ifdef DEBUG
cerr << "P: " << *P << nl;
#endif
      return P;
    }

   // Else make sure we don't do duplicate work.
   //
   ShortParse T(  ((*_sList)[_at1])[_at2-1] );

#ifdef DEBUG
cerr << "T: " << T << nl;
#endif

   for (; _at2 < listSize; _at2++)
    {
     //P = &(_sList->get(_at1)[_at2]);
     P = &(((*_sList)[_at1])[_at2]);

#ifdef DEBUG
cerr << "P: " << *P << nl;
#endif

     if ( T == *P ) continue;
     else return P;
    }

   _at1 = -1;
   return 0;
 }

// equality test
//
bool ShortParse::operator==(const ShortParse &S) const
 {
#ifdef DEBUG
//cerr << *this << "==" << S << nl;
#endif
  if ( interiorLen() != S.interiorLen() ) return false;

  if (interior()==S.interior() && boundary()==S.boundary()) return true;
  else return false;
 }

// less than
//
bool ShortParse::operator<(const ShortParse &S) const
 {
//cerr << "operator<(const ShortParse " << S << ") with " << *this << nl;

  if (interiorLen() < S.interiorLen()) return true;
  if (interiorLen() > S.interiorLen()) return false;

  SortableArray<vertNum> I1(interior()), I2(S.interior()),
                         B1(boundary()), B2(S.boundary());

  /*
   * Make equal boundaries consecutive in sorted list.
   */
  if (B1<B2) return true;
  else
  if (B1>B2) return false;
  else
  if (I1<I2) return true;
  else
  return false;
 }

/*
 * Test for valid parses.
 */
bool ShortParses::findOdd(vertNum l)
 {
  (void) l;
  aassert(false);
  return false;
 }
//
bool ShortParses::findEven(vertNum l)
 {
  (void) l;
  aassert(false);
  return false;
 }
