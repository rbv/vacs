
/*tex

\file{matching_ga.c}
\path{src/graph/algorithm}
\title{Maximum matching algorithms}
\classes{}
\makeheader

xet*/

/*****************************************************************************
*
* Program Module: matching.c  
*
* Author: Michael J. Dinneen
*
* Version/Edit I/01
* Edit Date:  11-Aug-91 
*
* Developed for: Los Alamos National Laboratory
*
******************************************************************************
*
*  M O D I F I C A T I O N    H I S T O R Y
*
*  Ver/Edit  	  Date		Reason
*  --------    -----------      ------
*  I/01		11-Aug-91	Release version.
*  II/01	09-Apr-94	C++ version for VACS.
*
******************************************************************************
*
* P R O G R A M   D E S C R I P T I O N
*
* VERSION I:
*
* This module finds the order of the largest matching of a graphs with 10 
* or fewer vertices. (Graffiti)
* Edges of the matching are (m_vert[i], m_match[i]) for 0 <= i < matching(a,n)
*
* Usage:
*       #include "matrix.c"
*       #include "matrix2.c"
*       #include "matching.c"
*       .
*       .
*       read_graph(sz);
*       build_mat();
*       n=real_adj(a);
*       .
*       .
*       match = matching(a,n);
*
* VERSION II:
*
* Still brute force but no longer limited to 10 vertices.
*
* 	#include "graph/algorithms/matching_ga.h"
*	Graph G; 
*	Matching M;
*	.
*	.
*       vertNum match = matching( G, M );
*
****************************************************************************/

#include "graph/algorithm/matching_ga.h"

/*
 * Largest Matching of a graph.
 */
vertNum matching( const Graph &G, Matching &M, vertNum fixedK )
 {
//cerr << "Matching (fixedK = " << fixedK << "):" << G;

     vertNum n = G.order();

     VertArray vert(n);
     VertArray match(n);
     //
     M.vert.resize(n);
     M.match.resize(n);

     //short int used[10];
     //
     Array<bool> used(n);

     vertNum cur_match=0;
     vertNum isolated=0;
     vertNum max_match=0;
     vertNum per_match=n/2;

     if (fixedK) per_match = min(per_match,fixedK);

   int i,j,m;

   //for (i=0; i<n; i++) used[i]=0;
   //
   used.fill(false);

//Add_matching: (now the next while Loop)

  while ( 1 /* max_match < per_match || no more backtracking */ )
  {
   bool breakFlag = false;

   /*
    * Be greedy to find a matching.
    */
   for (i = cur_match ? vert[cur_match-1]+1 : 0; i < n-1; i++)
    {
     if (used[i]) continue;

     for (m=i+1; m<n; m++)
       {
         if (used[m]) continue;

         if (G.isEdge(m,i))
          {
/*
printf("1. adding (%d,%d)\n",i,m);
*/
	   vert[cur_match]=i;
	   match[cur_match]=m;
	   used[m]=true;
	   cur_match++;
           break;
	  }
       }
    } 

/*
printf("cur_match=%d; isolated=%d\n",cur_match,isolated);
*/

   if (max_match < cur_match-isolated) 
    {
      max_match=cur_match-isolated;
      for (i=j=0; i<cur_match; i++)
        {
         if (match[i]<n)
           {
             M.vert[j]=vert[i];
             M.match[j]=match[i];
             j++;
           }
        }

      assert(j==max_match);

      if (max_match >= per_match) break; // done!
    }

#if 0 /* see previous line */

   if (cur_match-isolated >= per_match) 
    {
      M.vert.resize(per_match);
      M.match.resize(per_match);

      return per_match;
    }

#endif

   /*
    * Backtrack phase to try and improve matching.
    */
   for (j=cur_match-1; j>=0; j--)
    {
     if (match[j] == n) 
      {
	isolated--;
      }
     else
     if (match[j] == n-1) 
      {
	used[match[j]]=false;
	match[j]=n;
	isolated++;
	cur_match=j+1;
	//goto Add_matching;
	breakFlag = true;
        break;
      }
     else
      {
        used[match[j]]=false;

        /*
         * Try to just alter current match.
         */
	for (m=match[j]+1; m<n; m++)
	 {
	 if (used[m]) continue;

	 if (G.isEdge(m,vert[j]))
	  {
/*
printf("2. adding (%d,%d)\n",vert[j],m);
*/
	   used[m]=true;
	   match[j]=m;
	   cur_match=j+1;
           //goto Add_matching;
           breakFlag = true;
           break; 
	  }
         }

       if (breakFlag) break;

       /*
        * See if we can replace this match with a later one.
        */
       for (i = vert[j]+1; i<n-1; i++)
        {
         if (used[i]) continue;

         for (m=i+1; m<n; m++)
           {
             if (used[m]) continue;

             if (G.isEdge(m,i))
              {
/*
printf("3. adding (%d,%d)\n",i,m);
*/
               vert[j]=i;
 	       used[m]=true;
	       match[j]=m;

	       cur_match=j+1;
	       //goto Add_matching;
               breakFlag = true;
               break;
	      }
           }

	 if (breakFlag) break;

        }  // next i

       /*
        * All of the above failed so backtrack. (next j)
        */

      } // else

     if (breakFlag) break;

    }  // next j

   if ( ! breakFlag ) break;  // finished bactracking.

  } // while per_match > max_match

  M.vert.resize(max_match);
  M.match.resize(max_match);

//cerr << "returning matching = " << max_match << nl;

  return max_match;
 }
