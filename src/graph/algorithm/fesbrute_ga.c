
/*
 * See if a graph has a k-feedback edge set.
 */

/*tex

\file{fesbrute_ga.c}
\path{src/graph/algorithm}
\title{Definition of feedback edge set brute force functions}
\classes{}
\makeheader

xet*/


#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/graph.h"
//#include "general/sets.h"
//#include "general/queue.h"
#include "array/array.h"
//#include "general/bitvect.h"
#include "graph/algorithm/cycle_ga.h"

// #define WATCHB
#ifdef WATCHB
#include <iostream>
#endif

//----------------------------------------------------------------------

struct fesEdge
{
 int i, j;
};

/*
 * Pick each of the graph's edges and see if it is is acyclic.
 */
bool fesBrute1(const Graph &G_in) 
{
  if ( acyclic(G_in) ) return true; 

  Graph &G = CAST(G_in,Graph);

  int n = G.order();
  int m = G.size();

  Array<fesEdge> ej(m);

  int cnt = 0;
  int i; for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) { ej[cnt].i=i; ej[cnt].j=j; cnt++; }

  bool find = false;
  //
  for (i=0; i<m-1 && !find; i++)
  {
    G.rmEdge( ej[i].i, ej[i].j );

    if ( acyclic(G) ) find = true;

    G.addEdge( ej[i].i, ej[i].j );
  }

  return find;
}

/*
 * Pick all 2 subsets of a graph's edges and see if G\{e1,e2} is acyclic.
 */
bool fesBrute2(const Graph &G_in) 
{
  if ( acyclic(G_in) ) return true; 

  Graph &G = CAST(G_in,Graph);

  int n = G.order();
  int m = G.size();

  Array<fesEdge> ej(m);

  int cnt = 0;
  int i; for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) { ej[cnt].i=i; ej[cnt].j=j; cnt++; }

  bool find = false;
  //
  for (i=0; i<m-1 && !find; i++)
  {
    G.rmEdge( ej[i].i, ej[i].j );

    for (int j=i+1; j<m && !find; j++)
    {
      G.rmEdge( ej[j].i, ej[j].j );

      if ( acyclic(G) ) find = true;

      G.addEdge( ej[j].i, ej[j].j );
    }

    G.addEdge( ej[i].i, ej[i].j );
  }

  return find;
}

/*
 * Pick all 3 subsets of a graph's edges and see if G\{e1,e2,e3} is acyclic.
 */
bool fesBrute3(const Graph &G_in) 
{
  if ( acyclic(G_in) ) return true; 

  Graph &G = CAST(G_in,Graph);

  int n = G.order();
  int m = G.size();

  Array<fesEdge> ej(m);

  int cnt = 0;
  int i; for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) { ej[cnt].i=i; ej[cnt].j=j; cnt++; }

  bool find = false;
  //
  for (i=0; i<m-2 && !find; i++)
  {
    G.rmEdge( ej[i].i, ej[i].j );

    for (int j=i+1; j<m-1 && !find; j++)
    {
      G.rmEdge( ej[j].i, ej[j].j );

      for (int k=j+1; k<m && !find; k++)
      { 
        G.rmEdge( ej[k].i, ej[k].j );

        if ( acyclic(G) ) find = true;
 
        G.addEdge( ej[k].i, ej[k].j );
      }

      G.addEdge( ej[j].i, ej[j].j );
    }

    G.addEdge( ej[i].i, ej[i].j );
  }

  return find;
}

/*
 * Pick all 4 subsets of a graph's edges and see if G\{e1,e2,e3,e4} is acyclic.
 */
bool fesBrute4(const Graph &G_in) 
{
  Graph &G = CAST(G_in,Graph);

  int n = G.order();
  int m = G.size();

  if (m<=6) return true;

  Array<fesEdge> ej(m);

  int cnt = 0;
  int i; for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) { ej[cnt].i=i; ej[cnt].j=j; cnt++; }

  bool find = false;
  //
  for (i=0; i<m-3 && !find; i++)
  {
    G.rmEdge( ej[i].i, ej[i].j );

    for (int j=i+1; j<m-2 && !find; j++)
    {
      G.rmEdge( ej[j].i, ej[j].j );

      for (int k=j+1; k<m-1 && !find; k++)
      { 
        G.rmEdge( ej[k].i, ej[k].j );

        for (int l=k+1; l<m && !find; l++)
        { 
          G.rmEdge( ej[l].i, ej[l].j );

          if ( acyclic(G) ) find = true;

          G.addEdge( ej[l].i, ej[l].j );
        }

        G.addEdge( ej[k].i, ej[k].j );
      }

      G.addEdge( ej[j].i, ej[j].j );
    }

    G.addEdge( ej[i].i, ej[i].j );
  }

  return find;
}

bool fesBrute(const Graph &G, int k)
{
#if 0
  switch (k)
  {
    case 1: return fesBrute1(G);
    case 2: return fesBrute2(G);
    case 3: return fesBrute3(G);
    case 4: return fesBrute4(G);
    case 0: return acyclic(G);
    default: aassert(false);
  }
  return false;
#else
 int n = G.order();
 int m = G.size();
 int c = G.components();
 
 return k <= m-n+c;
#endif
}

int fesBrute(const Graph &G)
{
 int n = G.order();
 int m = G.size();
 int c = G.components();
 
 return m-n+c;
}
// --------------------------------------------------------------------

bool fvsBrute( const Graph &G, int k);

/*
 * Pick each of the graph's vertices and edges and see if it is is acyclic.
 */
bool vej11Brute(const Graph &G_in) 
{
//cerr << "vej11Brute:" << G_in << nl;
  if ( fvsBrute(G_in,1) ) return true;

  Graph &G = CAST(G_in,Graph);

  int n = G.order();
  int m = G.size();

  Array<fesEdge> ej(m);

  int cnt = 0;
  int i; for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) { ej[cnt].i=i; ej[cnt].j=j; cnt++; }

  bool find = false;
  //
  for (i=0; i<m-1 && !find; i++)
  {
    G.rmEdge( ej[i].i, ej[i].j );

    //if ( acyclic(G) ) find = true;
    if ( fvsBrute(G,1) ) find = true;

    G.addEdge( ej[i].i, ej[i].j );
  }

  return find;
}

bool vej12Brute(const Graph &G_in) 
{
  if ( fvsBrute(G_in,1) ) return true;

  Graph &G = CAST(G_in,Graph);

  int n = G.order();
  int m = G.size();

  Array<fesEdge> ej(m);

  int cnt = 0;
  int i; for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) { ej[cnt].i=i; ej[cnt].j=j; cnt++; }

  bool find = false;
  //
  for (i=0; i<m-2 && !find; i++)
  {
  G.rmEdge( ej[i].i, ej[i].j );

  for (int j=i+1; j<m-1 && !find; j++)
  {
    G.rmEdge( ej[j].i, ej[j].j );

    //if ( acyclic(G) ) find = true;
    if ( fvsBrute(G,1) ) find = true;

    G.addEdge( ej[j].i, ej[j].j );
  }
  G.addEdge( ej[i].i, ej[i].j );
  }

  return find;
}
