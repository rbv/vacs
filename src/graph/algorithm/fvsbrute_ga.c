
/*
 * See if a graph has a k-feedback vertex set.
 */

/*tex

\file{fvsbrute_ga.c}
\path{src/graph/algorithm}
\title{Definition of feedback vertex set brute force functions}
\classes{}
\makeheader

xet*/


#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/graph.h"
#include "general/sets.h"
#include "general/queue.h"
#include "array/array.h"
#include "general/bitvect.h"

// #define WATCHB
#ifdef WATCHB
#include <iostream>
#endif

//----------------------------------------------------------------------

/*
 * See if a graph has any cycles.
 */
static bool acyclic( const Graph &G, const BitVector &missing )
 {
  int n = G.order();

  BitVector span(n);
  //
  span.clearAll();

  Queue< vertNum > bfs;

  Array< int > parent(n);
  //
  parent.fill(-1);

  vertNum i;
  //
  for (int r=0; r < n-1; r++)	// We can skip last vertex.
   {
    if (span.getBit(r) || missing.getBit(r)) continue;// find next component.

    span.setBit(r);
    bfs.enque(r); 
    
    while (bfs.deque(i))
     for (int j=0; j<n; j++)
     {
      if ( i==j || missing.getBit(j) || parent[i]==j) continue;

      if ( G.isEdge(i,j) )
       { 
        if (span.getBit(j))
         {
          while (bfs.deque(i));
          return false;
         }
        else
         {
          bfs.enque(j);
	  span.setBit(j); 
          parent[j]=i;
         }
       }
     } // j and while

   } // i, r

#ifdef WATCHB
  cerr << "Induced graph is acyclic\n";
#endif

  return true;
 }


/*
 * Pick all k subsets of a graph and see if G\{v1,v2,...vk} is acyclic.
 */
bool fvsBrute(const Graph &G, int k)
 {
  int n = G.order();
  BitVector missing(n);

  Subsets vert(n,k);

  while (vert.nextset())
   {
    missing.clearAll();
    for (int i=0; i<k; i++) missing.setBit(vert.item(i));

#ifdef WATCHB
  cerr << "Membership test for:\n";
  cerr << G << nl;
  for (i=0; i<k; i++) cerr << vert.item(i) << ' ';
  cerr << nl;
#endif

    if ( acyclic(G, missing) ) { return true; }
   }

  return false;
 }

/*
 * Find the least k such that k is a k-fvs.
 */
vertNum fvsBrute(const Graph &G)
 {
  vertNum n = G.order();

  if ( G.size()==0 || n<2 ) return 0;

  vertNum yesK=n-1, noK=0, k;

  while ( yesK - noK > 1 )
   {
    k = (yesK - noK)/2 + noK;

    if ( fvsBrute( G, k ) ) yesK=k; else noK=k;
   }

  return yesK;
 }

static bool acyclic( const Graph& G, vertNum v1, vertNum v2 )
 {
  int n = G.order();

  Array<bool> span(n);
  //
  span.fill( false );

  Queue< vertNum > bfs;

  Array< int > parent(n);
  //
  parent.fill(-1);

  vertNum i;
  //
  for (int r=0; r < n-1; r++)	// We can skip last vertex.
   {
    if (span.get(r) || r==v1 || r==v2 ) continue;// find next component.

    span.put(r, true);
    bfs.enque(r); 
    
    while (bfs.deque(i))
     for (int j=0; j<n; j++)
     {
      if ( i==j || j==v1 || j==v2 || parent[i]==j) continue;

      if ( G.isEdge(i,j) )
       { 
        if (span.get(j))
         {
          while (bfs.deque(i));
          return false;
         }
        else
         {
          bfs.enque(j);
	  span.put(j,true); 
          parent[j]=i;
         }
       }
     } // j and while

   } // i, r

  return true;
 }


bool fvsBrute2(const Graph &G )
 {
  int n = G.order();

  for ( vertNum v1=0; v1<n-1; v1++ )
     for ( vertNum v2=v1+1; v2<n; v2++ )
       if ( acyclic(G, v1,v2) ) { return true; }

  return false;
 }

bool fvsBrute2I( const Graph &G, int i)
{ aassert(i==2); return fvsBrute2(G); }

// "All" k-subsets are FVS
//
bool allFVS_1( const Graph &G )
{
  int n = G.order();
  if (n <= 3) return true;
  BitVector missing(n);

  for (int i=0; i<n; i++)
  {
    missing.clearAll();
    missing.setBit(i);

    if ( acyclic(G, missing) == false ) { return false; }
  }

  return true;
}

bool allFVS_2( const Graph &G )
{
  int n = G.order();
  if (n <= 4) return true;

  for ( vertNum v1=0; v1<n-1; v1++ )
     for ( vertNum v2=v1+1; v2<n; v2++ )
       if ( acyclic(G, v1,v2) == false ) { return false; }

  return true;
}
