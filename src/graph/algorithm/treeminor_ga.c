
/*tex
 
\file{treeminor_ga.c}
\path{src/graph/algorithm}
\title{Boolean functions to test for tree minors.}
\classes{}
\makeheader
 
xet*/

#include "graph/tree.h"
#include "graph/ltree.h"
#include "graph/forest.h"

// debug
//#include <iostream>

// ---------------------------------------------------------------------

#if 0
// strict minors
//
bool isMinor(const LTree &super, const LTree &sub)
{
  if (super.order() <= sub.order()) return false;
}

bool isMinor(const LForest &super, const LTree &sub) 
{
}

bool isMinor(const LForest &super, const LTree &sub)
{
}

bool isMinor(const LForest &super, const LForest &sub)
{
}
#endif
