
#include "graph/graph.h"
#include "graph/stdgraph2.h"

static DegreeSequence deg;

/*
 * Check a graph for K_4 as a minor.
 */
bool k4_minor(const Graph &G)
{
   vertNum n = G.order();

   if (n<4) return false;

   deg.resizeNoCopy(n);
   //
   G.degVec(deg);

   bool change = false;

   Graph newG(G);

   for (int i=0; i<n; i++)
   {
     switch(deg[i]) 
     {
      case 0:  
      case 1: { 
	 	newG.rmNode(i); 
		return k4_minor(newG); 
	      } 
      case 2: { 
		for (int j=0; j<n; j++)
                { 
                  if (i==j) continue;
                  if (newG.isEdge(i,j)) 
                  {
                    newG.contractEdge(i,j);
		    return k4_minor(newG);
                  }
                } 
              }
     }//switch 

   } //for

   return true;

}
