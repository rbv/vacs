
/*tex
 
\file{orders_ga.c}
\path{src/graph/algorithm}
\title{Partial orders for graph structures.}
\classes{Tree,Graph}
\makeheader
 
xet*/

#include "graph/tree.h"
#include "graph/ltree.h"
#include "graph/graph.h"
#include "graph/stdgraph2.h"
#include "graph/algorithm/matching_ga.h"

// debug
//#include <iostream>

// ---------------------------------------------------------------------


//#include "set/skiplist.h"
//typedef SkipList<vertNum,vertNum> nodeDict;
typedef IterSet<vertNum> nodeDict;

bool isRootedHomeo(const Tree &T /* super */  , vertNum r1,
                   const Tree &P /* pattern */ , vertNum r2)
{
  vertNum n1=T.order(),
          n2=P.order();

  assert(r1<n1 && r2<n2 && r1>=0 && r2>=0);

  // fast checks.
  //
  if (n2>n1) return false;
  if (n2==1) return true;

  Array<bool> mark(n1);
  mark.fill(false);

  nodeDict PLeaves(-1);
  //
  // Get initial dictionary of P's leaves.
  //
  int i; for (i=0; i<n2; i++)
   {
    if (i==r2) continue;

    if (P.isLeaf(i)) PLeaves.add(i);
   }
  
  // search order within host tree
  //
  VertArray seq(n1);
  T.reverseBFS(seq,r1);

  VertArray parents(n2);
  P.getParents(parents,r2);

  Array<nodeDict> Sr(n1);

  VertArray children(n1-1), children2(n2-1);
  vertNum childCount, childCount2;

  Matching M;

  // Assign this dictionary to all of T's leaves.
  //
  for (i=0; i<n1; i++)
   {
    vertNum node = seq[i];

    mark[node]=true; // anticipate.

    if ( node!=r1 && T.isLeaf(node)==true )
     { 
       Sr[node]=PLeaves;
       continue;
     }

    // first find all children of this node and add in the P roots
    // 
    childCount=0;
    //
    int j; for (j=0; j<T.degree(node); j++)
     {
      vertNum child = T.neighbor(node,j);   

      if ( ! mark[child] ) continue;

      children[childCount++] = child;
      Sr[node].addUniqueSet(Sr[child]);
     }
  
    //if (childCount==1) continue;

    // Now try for other sub-root nodes of P.
    //
    for (j=0; j<n2; j++)
     {
       if (Sr[node].contains(j)) continue;

       vertNum deg_j = P.degree(j);

       childCount2 = 0;

       int k; for (k=0; k<deg_j; k++)
        {
         vertNum child = P.neighbor(j,k);   

         if ( parents[child] != j ) continue;
   
         children2[childCount2++]=child;
        }

       assert(childCount2 == (j==r2 ? deg_j : deg_j-1));
       //
       if ( childCount2 > childCount ) continue;

       Graph G(childCount+childCount2);

       for (     k=0; k<childCount; k++)
        for (int l=0; l<childCount2; l++)
         {
           if ( Sr[children[k]].contains(children2[l]) )
            {
             	G.addEdge(k,l+childCount);
            }
         }

       /*
        * Now test for matching.
        */

       if ( matching(G,M,childCount2) >= childCount2 ) 
        {
         if (j==r2) return true;
         Sr[node].add(j);
        }

     } // next pattern node j 

   } // next host node i;

  return false;
}

#define WILDCARD_LABELS

// Slight modification from above -- assures labels match.
//
bool isRootedHomeo(const LTree &T /* super */  , vertNum r1,
                   const LTree &P /* pattern */ , vertNum r2)
{
  vertNum n1=T.order(),
          n2=P.order();

  assert(r1<n1 && r2<n2 && r1>=0 && r2>=0);

  // fast checks.
  //
  if (n2>n1) return false;
  if (n2==1) 
  {
    vertNum node;
    return T.labelIndex(P.getLabel(r2),node);
  }

  Array<bool> mark(n1);
  mark.fill(false);

  nodeDict PLeaves /* (-1) */;
  //
  // Get initial dictionary of P's leaves.
  //
  int i; for (i=0; i<n2; i++)
   {
    if (i==r2) continue;

    if (P.isLeaf(i)) PLeaves.add(i);
   }
  
  // search order within host tree
  //
  VertArray seq(n1);
  T.reverseBFS(seq,r1);

  VertArray parents(n2);
  P.getParents(parents,r2);

  Array<nodeDict> Sr(n1);

  // reuseable objects for below.
  VertArray children(n1-1), children2(n2-1);
  vertNum childCount, childCount2;
  //
  Matching M;

  // Assign this dictionary to all of T's leaves.
  //
  for (i=0; i<n1; i++)
   {
    vertNum node = seq[i];

    mark[node]=true; // anticipate.

    Label nodeL = T.getLabel(node);

    if ( node!=r1 && T.isLeaf(node)==true )
     { 
       vertNum node2;

       PLeaves.start();
       //
       while ( 1 ) 	// just get leaves with same label
        {
          node2 = PLeaves.next();
	  if (node2 == PLeaves.emptyFlag()) break;

#ifdef WILDCARD_LABELS
          if ( P.getLabel(node2) == nodeL ||
               P.getLabel(node2) == LTree::NoLabel ) Sr[node].add(node2);
#else
          if ( P.getLabel(node2) == nodeL ) Sr[node].add(node2);
#endif
        }
       //
       PLeaves.stop();

//cerr << "leaf node " << node << " set to " << Sr[node] << nl;

       continue;
     }

    // First find all children of this node and add in the P roots.
    // 
    childCount=0;
    //
    int j; for (j=0; j<T.degree(node); j++)
     {
      vertNum child = T.neighbor(node,j);   

      if ( ! mark[child] ) continue;

      children[childCount++] = child;
      Sr[node].addUniqueSet(Sr[child]);
     }

//cerr << "node " << node << " unioned from children " << Sr[node] << nl;
  
    // Now try for other sub-root nodes of P.
    //
    for (j=0; j<n2; j++)
     {
#ifdef WILDCARD_LABELS
       if ( (P.getLabel(j) != nodeL && P.getLabel(j) != LTree::NoLabel) || 
	    Sr[node].contains(j) ) continue;
#else
       if ( P.getLabel(j) != nodeL || Sr[node].contains(j) ) continue;
#endif

       vertNum deg_j = P.degree(j);

       childCount2 = 0;

       int k; for (k=0; k<deg_j; k++)
        {
         vertNum child = P.neighbor(j,k); 

         if ( parents[child] != j ) continue;
   
         children2[childCount2++]=child;
        }

       assert(childCount2 == (j==r2 ? deg_j : deg_j-1));
       //
       if ( childCount2 > childCount ) continue;

       Graph G(childCount+childCount2);

       for (     k=0; k<childCount; k++)
        for (int l=0; l<childCount2; l++)
         {
//cerr << "testing child " << children[k] << " contains " << children2[l] <<nl;
//vertNum tmp = children[k];
//cerr << "contained in " << Sr[tmp] << " == "; 
           if ( Sr[children[k]].contains(children2[l]) )
            {
//cerr << "yes\n";
             	G.addEdge(k,l+childCount);
            }
//else cerr << "no\n";
         }

       /*
        * Now test for matching.
        */

//cerr << "trying to add subroot " << j << nl;

       if ( matching(G,M,childCount2) >= childCount2 ) 
        {
         if (j==r2) return true;
         Sr[node].add(j);
        }

     } // next pattern node j 

//cerr << "node " << node << " after subroot adds " << Sr[node] << nl;

   } // next host node i;

//cerr << "not rootedHomeo\n";

  return false;
}
