
/*
 * Check if a graph is planar.
 */

/*tex

\file{planar_ga.c}
\path{src/graph/algorithm}
\title{Planar membership test.}
\classes{}
\makeheader

xet*/

#if DOLEDA
#  include "graph/ledagraph.h"
#  include "LEDA/graph_alg.h"
#  define SPLIT_BLOCKS
#elif DOLEMON
#  include "lemon/list_graph.h"
#  include "lemon/planarity.h"
#else
#  define PLANAR(g) (aassert(false), false)
#endif

#include "general/bitvect.h"
#include "graph/graph.h"	// Must be after ledagraph.h!
#include "graph/indgrph.h"
#include "general/gtimers.h"	

//----------------------------------------------------------------------

// export these functions.
//

// -------------------------------------------------------------------
#if DOLEDA


bool planarBlock(const InducedGraph &B)	// I.e., biconnected graph
 {

//cerr << "planar_induced_block( " << B << " )\n";

  LedaGraph LG(B);

//cerr << "LedaGraph:\n" << LG << nl;

#if 0
  if ( PLANAR(LG) ) // return true;
  { cerr << "is planar\n"; return true; }
  else { cerr << "not planar\n"; return false; }
#else
  if ( PLANAR(LG) ) return true;
  else return false;
#endif
 }

bool planarBlock(const Graph &B)	// I.e., biconnected graph
 {
  LedaGraph LG(B);

  cerr << "LedaGraph:\n" << LG << nl;

  if ( PLANAR(LG) ) return true;
  else return false;
 }

// -------------------------------------------------------------------
#elif DOLEMON
// -------------------------------------------------------------------

bool planar(const InducedGraph &B)	// I.e., biconnected graph
{
    return planar( Graph(B) );
}

bool planar(const Graph &B)	// I.e., biconnected graph
{
   Gtimers::start( 29, "lemon::ListGraph" );
   lemon::ListGraph lgraph;
   typedef lemon::ListGraph::Node Node;

   std::vector<Node> nodes( B.order(), Node() );
   for (int i = B.order() - 1; i >= 0; i--)
      nodes[i] = lgraph.addNode();

   for (int i = B.order() - 1; i >= 0; i--)
   {
       BitVector neighbours = B.neighbors( i );
       for (int i2 = i + 1; i2 < neighbours.size(); i2++ )
       {
           if ( neighbours.get_bit( i2 ) )
               lgraph.addEdge( nodes[i], nodes[i2] );
       }
   }
  
   Gtimers::stop( 29 );
   Gtimers::start( 30, "lemon::checkPlanarity" );

   bool ret = lemon::checkPlanarity( lgraph );
   Gtimers::stop( 30 );
   return ret;
}

// -------------------------------------------------------------------
#else

bool planar(const InducedGraph &B)
{
   aassert( false );
   return false;
}

bool planar(const Graph &B)
{
   aassert( false );
   return false;
}

// -------------------------------------------------------------------
#endif

#ifdef SPLIT_BLOCKS

bool planar(const InducedGraph &G) // via checking blocks
 {

//cerr << "planar_induced( " << G << " )\n";

  Blocks blocks;
  //
  G.getBlocks( blocks );

  InducedGraph *igp;

  blocks.start();
  //
  while ( igp = blocks.next() )
   {
     if ( !planarBlock( *igp ) ) return false;
   }
  //
  blocks.stop();

  return true;
 }

bool planar(const Graph &G) // via checking blocks
{
  Blocks blocks;
  //
  G.getBlocks( blocks );

  InducedGraph *igp;

  blocks.start();
  //
  while ( igp = blocks.next() )
   {
//cerr << "igp = " << *igp << nl;
     if ( !planarBlock( *igp ) ) return false;
   }
  //
  blocks.stop();

  return true;
}

#else

bool planarBlock(const InducedGraph &B)
{ return planar( B ); }

bool planarBlock(const Graph &B)
{ return planar( B ); }

#endif // SPLIT_BLOCKS

//------------------------------------------------------------------

#include "stdtypes.h"
#include "general/sets.h"
#include "general/bitvect.h"

//
// Within k vertices of planar.
//
bool planar(const Graph &G, vertNum k)
 {

//cerr << "planar-" << k << nl << G << nl;

  if (k==0) return planar(G);

  /*
   * Pick all k subsets of a graph and see if G\{v1,v2,...vk} is planar.
   */
  vertNum n = G.order();

  if ( k >= n-1 ) return true;

  BitVector missing(n);

  InducedGraph H(G);

  Subsets vert(n,k);

  while (vert.nextset())
   {
    missing.clearAll();
    //
    for (int i=0; i<k; i++) missing.setBit(vert.item(i));
    //
    missing.complement();

    H.setIndex( missing );

    if ( planar(H) ) { return true; }
   }

  return false;

 }

//
// Within k edges of planar.
//
bool planarEdge(const Graph &G, vertNum k)
 {

//cerr << "planar-" << k << nl << G << nl;

  if (k==0) return planar(G);

  if ( planar(G) ) return true;

  /*
   * Pick all k subsets of the graphs edges and see if remaining is planar.
   */
  vertNum n = G.order();
  vertNum m = G.size();

  if ( k >= m-8 ) return true;

  Array<vertNum> Ei(m), Ej(m);

  int count = 0;
  for (int i=0; i<n-1; i++)
  for (int j=i+1; j<n; j++)
   if (G.isEdge(i,j)) { Ei[count]=i; Ej[count++]=j; }

  assert(count == m);

  Subsets ej(m,k);

  Graph H(G);
  //
  while (ej.nextset())
  {
    int i; for (i=0; i<k; i++) H.rmEdge(Ei[ej.item(i)],Ej[ej.item(i)]);
    //
    if ( planar(H) ) { return true; }
    //
    for (i=0; i<k; i++) H.addEdge(Ei[ej.item(i)],Ej[ej.item(i)]);
  }

  return false;

 }

//------------------------------------------------------------------

bool outerplanar(const Graph& G)
{
  int n = G.order();
  if (n < 4) return true;

  int m = G.size();
  if (m > 2*n-3) return false;


//cerr << "G:\n" << G;
  Graph H(G,1);

  for (int i=0; i<n; i++) H.addEdge(i,n);
//cerr << "H:\n" << H;

  return planar(H);  
}

bool outerplanar1(const Graph& G)
{
  int n = G.order();
  if (n < 5) return true;

  Graph H(G);
  Array<vertNum> missing(n);

  // For every vertex, replace the vertex with an apex vertex to test outerplanarity
  for (int i=0; i<n; i++) 
  {
    int added = 0;

    for (int j=0; j<n; j++)
    {
      if (i==j || H.isEdge(i,j)) continue;

      missing[added++] = j;
      H.addEdge(i,j);
    }

    if ( planar(H) ) return true;

    while (added) H.rmEdge(i,missing[--added]);
  }

  return false;
}
