
/*
 * Find the minimum paths.
 */

/*tex

\file{mincycle_ga.c}
\path{src/graph/algorithm}
\title{Finds shortest paths.}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "general/bitvect.h"
#include "general/queue.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "graph/graph.h"
#include "graph/algorithm/path_ga.h"

#include <iostream>

//----------------------------------------------------------------------

/*
 * Returns the size of smallest path between vertices u and v.
 */

// Warning! If called many times it would be wise to use distance matrix.
//
vertNum minPath( const AdjLists &L,
                  vertNum u, vertNum v, 
	          vertNum maxLen )
 {
  vertNum n = L.size();
  
  vertNum maxSize = maxLen ? maxLen : n;

  assert( u >=0 && u < n );
  assert( v >=0 && v < n );
  assert( u != v );

  BitVector seen(n);
  //
  seen.clearAll();

  Queue< vertNum > toDo[2];
  //
  toDo[0].enque(u);
  //
  seen.setBit(u);

  vertNum level = 0;
  //
  vertNum vert;

  while ( level < maxSize )
   {

    vertNum parity = level % 2;

//cerr << "level = " << level << nl;
//cerr << "queue: " << CAST(  toDo[ parity ], DyArray<vertNum> ) << nl;

    while ( toDo[ parity ].deque(vert) )
     {

      for ( int i=0; i<L[vert].size(); i++ )
       {
        vertNum neighbor = L[vert][i];

        if ( neighbor == v ) 
         {
          toDo[0].empty();
          toDo[1].empty();

          return level+1;
         }

        if ( seen.getBit(neighbor) == false )
         {
           seen.setBit(neighbor);

           toDo[ !parity ].enque(neighbor);
         }
       }
      
     }

    level++;
   }

  toDo[0].empty();
  toDo[1].empty();

  return maxSize+1;

 }


vertNum minPath( const Graph &G, 
		vertNum u, vertNum v, vertNum maxSize)
 {
  vertNum n = G.order();

  AdjLists L(n);

  G.getAdjLists( L );

  return minPath( L, u, v, maxSize );
 }

/**********************************************************************/

/*
 * Returns "a" smallest path between vertices u and v.
 */
vertNum minPath( Path &C, 
                  const AdjLists &L, 
                  vertNum u, vertNum v, 
  		  vertNum maxLen )
 {
  vertNum n = L.size();
  
  vertNum maxSize = maxLen ? maxLen : n;

  C.resize(maxSize);

//cerr << "minPath between " << u << ' '  << v << nl;

  assert( u >=0 && u < n );
  assert( v >=0 && v < n );
  assert( u != v );

  VertArray seen(n);  	// parent assigned means vertex has been seen
  //
  seen.fill(n);

  Queue< vertNum > toDo[2];
  //
  toDo[0].enque(u);
  //
  seen[u]=u;		// no parent but seen

  vertNum level = 0;
  //
  vertNum vert;

  while ( level < maxSize )
   {

    vertNum parity = level % 2;

    while ( toDo[ parity ].deque(vert) )
     {

//cerr << "vert = " << vert << nl;

      for ( int i=0; i<L[vert].size(); i++ )
       {
        vertNum neighbor = L[vert][i];

        if ( neighbor == v ) 
         {
          toDo[0].empty();
	  toDo[1].empty();

//cerr << "seen: " << seen << nl;

          level++;

          C[level]=v;
          C[level-1]=vert;
          //
          for (int j=level-2; j>=0; j--)
           {
            C[j] = seen[C[j+1]];
           }
          //
//cerr << "C[*]: " << C << nl;
          assert( C[0] == u );
          
          C.resize(level+1);

          return level;
         }

        if ( seen[neighbor] == n )
         {
           seen[neighbor] = vert;

//cerr << "enqueing " << neighbor << nl;

           toDo[ !parity ].enque(neighbor);
         }
       }
      
     }

    level++;
   }

  toDo[0].empty();
  toDo[1].empty();

  return maxSize+1;

 }

vertNum minPath( Path &P, const Graph &G, 
		vertNum u, vertNum v, vertNum maxSize) 
 {
  vertNum n = G.order();

  AdjLists L(n);

  G.getAdjLists( L );

  return minPath( P, L, u, v, maxSize );
 }

