/*****************************************************************************
*
* Program Module: chromatic.c
*
* Author: Michael J. Dinneen
*
* Developed for: Los Alamos National Laboratory
*
******************************************************************************
*
*  M O D I F I C A T I O N    H I S T O R Y
*
*  Ver/Edit  	  Date		Reason
*  --------    -----------      ------
*  II/01	29-Jul-91	Changed standard backtracking to
*				recursive maximal independence version.
*
****************************************************************************/

#include "graph/graph.h"
#include "graph/algorithm/chromatic_ga.h"
#include "graph/indgrph.h"
#include "array/array.h"
#include "general/bitvect.h"
#include "general/queue.h"
//#include "set/iterset.h"

//#define WATCH
//#define WATCH_I
//#define WATCH_G

static Queue<vertNum> chiQue(100);		
//
/*
 * See if graph is 2 or 3 colorable.
 */
vertNum greedy23Chromatic( const InducedGraph &G, VertArray &vert )
 {
   vertNum n = G.order();

#ifdef WATCH_G
   cerr << "greedy23:" << G;
#endif

   BitVector span(n);
   //
   BitVector colorMap(n);
   //
   span.clearAll();

   /*
    * Start coloring at first vertex.
    */
//   Queue<vertNum> chiQue(n);		
   chiQue.empty();
   //
   chiQue.enque(0);
   //
   vertNum maxColors = 0;
   //
   vertNum colored = 0;

   vertNum i;				// Current vertex to color.

   /*
    * Do a breadth-first search coloring.
    */
   while ( colored < n)
    {

     while ( chiQue.deque(i) )
      {
#ifdef WATCH_G
   cerr << "To color: " << i << nl;
#endif

        if ( span.getBit(i) ) continue;
 
        /*
         * Check for neighbor colors.
         */
        colorMap.clearAll();
        //
        int j; for (j=0; j<n; j++)
         {
          if ( G.isEdge(i,j)==true )
           {
             if ( span.getBit(j) ) colorMap.setBit(vert[j]);
             else chiQue.enque(j);
           }
         }
    
        /*
         * See if a previous color can be used.
         */
        for (j=0; j<maxColors; j++)
         {
          if ( colorMap.getBit(j)==false )
           {
             vert[i]=j;

             break;
           }
         }

	/*
         * Else need a new color.
         */ 
        if ( j==maxColors ) 			
         {
           vert[i]=maxColors++;
  
           if ( maxColors == 4 ) return 4;	// Terminate greedy early.
         }

#ifdef WATCH_G
   cerr << " colored: " << vert[i] << nl;
#endif

        span.setBit(i);
        //
        colored++;

      } // next queue component node.

     /*
      * Scan for new components to color.
      */
     for (i=1; i<n; i++)
      {
       if ( span.getBit(i)==0 ) 
        {
          chiQue.enque(i);
 
          break;
        }
      }

    } // while not all colored

  return maxColors;

 }

/*
 * Get a greedy bound on the needed colors.
 */
vertNum greedyChromatic( const InducedGraph &G, 
		         VertArray &vert, 
  			 vertNum limit )
 {
   assert( limit > 1 );

   vertNum n = G.order();
 
   vertNum maxCount=1;

   for (int i=0; i<n; i++)
    {
     for (int c=0; c<maxCount; c++)
       {
         for (int j=0; j<n; j++) 
          if (G.isEdge(i,j) && vert[j]==c) goto Next_c;

         vert[i]=c; goto Next_i;
Next_c: ;
       }
     vert[i]=maxCount;
     maxCount++;

     if (maxCount==limit) return limit;	

Next_i: ;
    }

   return maxCount;
 }


/*
 * Recursively color an induced graph.
 */
vertNum chromatic( const InducedGraph &G_orig )
 {

#ifdef WATCH
cerr << "chromatic: " << nl;
cerr << G;
#endif

     vertNum n = G_orig.order(); 
     //
     VertArray vert(n);

     /*
      * Check for bipartite.
      */
     vertNum greedy = greedy23Chromatic( G_orig, vert );

     if ( greedy <= 3 ) return greedy;

     InducedGraph G(G_orig);
     //
     VertArray adjItem(n); 
    
     /*
      * Try to delete isolated and end-vertices.
      */
     vertNum numEnds=0;
     //
     vertNum maxDeg=0, degIdx=0;
     //
     DegreeSequence deg(n);
     //
     deg.fill(0);			// Do our degree stuff local!
     //
     int i; for (i=0; i<n; i++)
       {
         for (int j=i+1; j<n; j++) 	
          if ( G.isEdge(i,j) )
            {
              adjItem[i]=j; deg[i]++; 
              adjItem[j]=i; deg[j]++; 
            }

         if ( deg[i] <= 1 ) vert[numEnds++]=i;
         //
         if ( deg[i] > maxDeg )
          {
            maxDeg=deg[i];
            degIdx=i;
          }
       }
     //
     switch (numEnds) 
       {
        case 0: break;

#if 0
        default:

                for (i=0; i<numEnds; i++)
                 {
                   G.rmNode(vert[i]);
  
                   if ( deg[vert[i]]==1 ) deg[ adjItem[vert[i]] ]--;
  
                   deg[vert[i]]=deg[--n];
                 }  

		 /*
		  * Recompute maximum degree and index.
		  */
                 maxDeg=degIdx=0;
                 //
                 for (i=0; i<n; i++)
                  {
		   if ( deg[i] > maxDeg )
		    {
		      maxDeg=deg[i];
		      degIdx=i;
		    }
                  }

#else
        case 1: G.rmNode(vert[0]);	// Vertex n-1 goes to vert[0].

                if ( deg[vert[0]]==1 ) deg[ adjItem[vert[0]] ]--;

                deg[vert[0]]=deg[--n];

#define SLOPPY
#ifdef SLOPPY
                /*
		 * Readjust maximum index if needed;
		 */
                if ( degIdx==n ) degIdx=vert[0];
		// if ( maxDeg==n ) maxDeg--;
		
#else
		/*
		 * Recompute maximum degree and index.
		 */
                maxDeg=degIdx=0;
                
                for (i=0; i<n; i++)
                 {
		  if ( deg[i] > maxDeg )
		   {
		     maxDeg=deg[i];
		     degIdx=i;
		   }
                 }
#endif

                break;

        default: G.minus( vert, numEnds );

                 return chromatic( G );
#endif

       } // switch any prunies

     /*
      * Check for a vertex connected to all neighbors.
      */
     if ( maxDeg >= n-1 ) 	// ">" just in case of leaf delete above.
       {
#ifdef WATCH_I
cerr << "maxDeg == n-1 @ " << maxDeg << nl;
#endif
         G.rmNode(degIdx);
         return chromatic( G ) + 1;
       }

     /*
      * Else, continue finding maximal independece sets.
      */
     G.nodeSwap( 0, degIdx );

#ifdef WATCH_I
     cerr << G;
#endif

     BitVector span(n);
     //
     Array< BitVector > touch(n);
     //
     VertArray indSet(n);
     
     /*
      * Initialize with pre-maximal independence set.
      */
     //
     for (i=0; i<n; i++) 
      {
       touch[i].resize(n);

       touch[i].clearAll();

       touch[i].setBit(i);
#if 0
       for (int j=0; j<deg[i]; j++) 
        { 
          touch[i].setBit(adjLists[i][j]);
        }
      }
     //
#else
      }
     //
     for (i=0; i<n-1; i++)
      {
       for (int j=i+1; j<n; j++) 
         if (G.isEdge(i,j)) 
          { 
            touch[i].setBit(j);
            touch[j].setBit(i);
          }
      }
#endif
     //
     indSet[0]=0;			// Vertex 0 is fixed.
     indSet[1]=0; 			// Will be incremented != 0.
     //
     vertNum indSetCount=2;
     //
     vertNum bestColor=n;

     /*
      * Find maximal independent sets containing vertex 0.
      */
     while ( indSetCount > 1 )
      {
       span.clearAll();
       //
       for ( i=0, indSetCount--; i < indSetCount; i++ ) 
        {
          span.orOf( span, touch[indSet[i]] );
        }

       /*
        *  Keep adding vertices until graph is covered.
        */
       for ( int j = indSet[indSetCount] + 1; j<n; j++)       
        {
          if ( span.isBit(j)==false )           /* Update local span. */
            {
             indSet[indSetCount++] = j;
             span.orOf( span, touch[j] );
            }
        }

       if ( span.full()==false ) continue;
       
#ifdef WATCH_I
cerr << nl << "ind: ("<< indSetCount << ") " << indSet << nl;
#endif

       /*
        * Update best coloring.
        */
       InducedGraph H( G );
       //
       H.minus( indSet, indSetCount );

       vertNum thisColor = chromatic( H ) + 1;

       if ( thisColor < bestColor ) bestColor = thisColor;

      } // while another maximal independent set.
 
     return bestColor;
 }

