
/*
 * Check if a graph can be embeded on the torus.
 */

/*tex

\file{torodial_ga.c}
\path{src/graph/algorithm}
\title{Torodial membership test.}
\classes{}
\makeheader

xet*/

#include "graph/graph.h"	
#include "graph/indgrph.h"	

//----------------------------------------------------------------------

// export these functions.
//

bool torodialBlock(const InducedGraph &B)	// I.e., biconnected graph
 {
  (void) B;
  return false;
 }

bool torodial(const InducedGraph &G) // via checking blocks
 {
  Blocks blocks;
  //
  G.getBlocks( blocks );

  InducedGraph *igp;

  blocks.start();
  //
  while ( igp = blocks.next() )
   {
     if ( !torodialBlock( *igp ) ) return false;
   }
  //
  blocks.stop();

  return true;
 }

//--------------------- Graph Parameters -------------------------

bool torodialBlock(const Graph &B)	// I.e., biconnected graph
 {
  (void) B;
  return false;
 }

bool torodial(const Graph &G) // via checking blocks
 {
  Blocks blocks;
  //
  G.getBlocks( blocks );

  InducedGraph *igp;

  blocks.start();
  //
  while ( igp = blocks.next() )
   {
     if ( !torodialBlock( *igp ) ) return false;
   }
  //
  blocks.stop();

  return true;
 }

//------------------------------------------------------------------

