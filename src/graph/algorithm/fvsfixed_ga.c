
/*
 * See if a graph has a k-feedback vertex set. ( O(n^2) fixed-k version)
 */

/*tex

\file{fvsfixed_ga.c}
\path{src/graph/algorithm}
\title{Fixed k feedback vertex set membership test}
\classes{}
\makeheader

xet*/


#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "graph/algorithm/cycle_ga.h"
#include "graph/graph.h"
//#include "graph/reduce.h"

//#define WATCHB
#ifdef WATCHB
#include <iostream>
#endif

void removeDegree0to2( AdjLists &L ); // should be in some header ???
void removeVertex( AdjLists &Lnext, vertNum v );

//----------------------------------------------------------------------

bool fvsFixed(const AdjLists &Linput, int k)
 {
  if (k<0) return false;

  AdjLists L(Linput);

  if ( L.size() <= k+2 ) return true;

#ifdef WATCHB
  cerr << "fvsFixed: " << k << nl;
  cerr << L << nl;
#endif

  removeDegree0to2( L );

  Cycle C;

  vertNum Lgirth = girth( C, L );

  if ( Lgirth > L.size() ) return true;	// graph is acyclic

  //
  //  Use this fact:  If a graph H of minimum degree 3 (or deg 2 with 3-cycle)
  // has girth (length of a shortest cycle) > 2k then fvs(H) > k.
  //
//  if ( Lgirth/2 > k ) return false;
cerr << "Check me!" << nl;
assert(false);

#ifdef WATCHB
cerr << "after preprocessing with girth " << Lgirth << nl;
cerr << L <<nl;
#endif

  for ( int i=0; i<Lgirth; i++)
   {
    AdjLists Lnext(L);
    //
    removeVertex( Lnext, C[i] );

    if ( fvsFixed( Lnext, k-1 ) ) return true;
   }

  return false;
 }

bool fvsFixed(const Graph &G, int k)
 {
  int n = G.order();

#ifdef WATCHB
  cerr << "fvsFixed: " << k << nl;
  cerr << G << nl;
#endif

  AdjLists L(n);
  //
  G.getAdjLists( L );

  return fvsFixed(L, k);
 }

/*
 * Find the least k such that k is a k-fvs.
 */
vertNum fvsFixed(const Graph &G)
 {
  vertNum n = G.order();

  AdjLists L(n);
  //
  G.getAdjLists( L );

  if ( n<2 ) return 0;

  vertNum yesK=n-1, noK=0, k;

  while ( yesK - noK > 1 )
   {
    k = (yesK - noK)/2 + noK;

    if ( fvsFixed( L, k ) ) yesK=k; else noK=k;
   }

  return yesK;
 }


