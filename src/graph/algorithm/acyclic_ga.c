
/*tex
 
\file{acyclic_ga.c}
\path{src/graph/algorithm}
\title{See if a graph has NO cycles}
\classes{}
\makeheader
 
xet*/

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "graph/graph.h"
#include "graph/indgrph.h"
//#include "graph/algorithm/cyclic_ga.h"		// for acyclic()
//#include "array/array.h"

//------------------------------------------------------------------------

bool acyclic( const InducedGraph &G )
 {

  vertNum n = G.order();

  if ( n <= 2 ) return true;

  VertArray E_m( n );

  int i; for (i=0; i<n; i++) E_m[i]=i; 

  for (vertNum v1=0; v1<n-1; v1++)
   for (vertNum v2=v1+1; v2<n; v2++)
     {

      if ( ! G.isEdge(v1,v2) ) continue;

	  /*
           * If vertices are connected then we have a cycle.
           */
          if ( E_m[ v1 ] == E_m[ v2 ] ) 
           {
             return false;
           }

	  /*
           * Merge components into one equivalence class.
           */
          int other=E_m[ v2 ];
          //
          for (i=0; i<n; i++) 
           if (E_m[ i ] == other) E_m[ i ] = E_m[ v1 ];

     } // for

  return true;

 }

bool acyclic( const Graph &G ) // lazy!!!
 {
  InducedGraph H(G);
  return acyclic(H);
 }

