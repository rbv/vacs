
/*
 * Check if a graph has no cycles of length greater than k.
 */

#include "stdtypes.h"
#include "general/bitvect.h"
#include "graph/stdgraph.h"
#include "graph/graph.h"
#include "graph/algorithm/cycle_ga.h"
#include "graph/indgrph.h"

//#include <iostream>

//----------------------------------------------------------------------


bool maxCycleLen3(const Graph &G)
{
  if (acyclic(G)) return true;

  Blocks Blks; 
  G.getBlocks(Blks);

  bool ans = true;
  InducedGraph *pIG;
  
  Blks.start();
  //
  while ( pIG = Blks.next() )
  {
    // actually 'getBlocks' method only finds >= 4 vertex blocks
    //
    if (pIG->order() > 3) { ans = false; break; }
  }
  Blks.stop();

  return ans;
}

// export this function.
//
bool maxCycle(const Graph &G, vertNum k)
{
  switch(k)
  {
   case 3: return maxCycleLen3(G);
   case 2: return acyclic(G);
   case 1: return G.order() == 1;
   default: aassert(false);
  }

  return false;
}
