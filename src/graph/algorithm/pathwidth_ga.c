
/*
 * Find pathwidth and path-decompositions.
 */

/*tex

\file{pathwidth_ga.c}
\path{src/graph/algorithm}
\title{Definitions for finding pathwidth and path-decompositions}
\classes{}
\makeheader

xet*/


#include "graph/algorithm/pathwidth_ga.h"
#include "graph/algorithm/shortparse.h"
#include "graph/graph.h"
#include "graph/indgrph.h"
#include "general/sets.h"
#include "general/dtime.h"

// Do we want to use the linear-time tree algorithm?
//
#define USETREEALG
//
#ifdef USETREEALG
#include "graph/tree.h"
#include "graph/rtree.h"
#endif

// The following 4 #defines may conflict
//
#define COMPLETE_SEARCH
#define LAZY

// Use the following for good approximations
//
//#define BIG_BITES
#define GREEDY

//#define DEBUG
//#define DEBUG2
//
#include <iostream>

// temp.
extern void neighborhood( const AdjLists &A, const VertSet &of, VertSet &is );

//----------------------------------------------------------------------

/*
 * Global data for pathwidth membership.
 */
static ShortParses *SParses;
static vertNum fixed_k;
static AdjLists Adj(0);
static DegreeSequence Deg(0);

//----------------------------------------------------------------------

struct EdgePair { int i, j; };

bool DKL_GML( const Graph &G, vertNum k )
{
  vertNum n = G.order();
  vertNum m = G.size();

  aassert(m <= 32);

  Array<uchar> state(1<<m);
  //Array<int> state(1<<m);
  state.fill(0);

  if ( k >= n ) return true;

  assert( G.connected() );

  Deg.resizeNoCopy(n);
  Deg.fill(0);
  Array<EdgePair> ej(m);
  //
  int cnt = 0;
  for (int i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if (G.isEdge(i,j)) 
    { 
      Deg[i]++; Deg[j]++;
      ej[cnt].i=i; ej[cnt].j=j; cnt++; 
    }
 
  //cerr << G;
  //cerr << "Deg: " << Deg << nl;

  assert(cnt == m);
  
  DegreeSequence tmpDeg(n);
  //
  for (int depth=1; depth<=m; depth++)
  {
    Subsets choose(m,depth);

    while (choose.nextset())
    {
      int at = choose.intBitVec();

      int minVal = k+1;

      for (int i=0; i<depth; i++)
      {
        int left = at-(1<<choose.item(i));
        int costS_i = state[left];
        //cerr << "left state["<<left<<"]=" << costS_i << nl;

        if (costS_i > k) continue;

        tmpDeg.fill(0);
        //
        int j; for (j=0; j<depth; j++)
        {
          if (i==j) continue;
          tmpDeg[ej[choose.item(j)].i]++;
          tmpDeg[ej[choose.item(j)].j]++;
        }

        int ejCost = 2; // compute cost of left with ej[choose.item(i)] 
        //
        for (j=0; j<n; j++)
        {
          if (tmpDeg[j] > 0 && tmpDeg[j] < Deg[j])
          if (ej[choose.item(i)].i != j &&
              ej[choose.item(i)].j != j) ejCost++;
        } 

        minVal = min( minVal, max(costS_i, ejCost) );
      } 

      state[at]=minVal;
      //cerr << "final state["<< at <<"]=" << state[at] <<nl;

    }

  }
 
  //cerr << "state: " << state << nl;
  //cerr << "gml=" << int(state[state.size()-1]) <<nl; 
  return (int(state[state.size()-1]) <= k);

}

//----------------------------------------------------------------------

static void Init_Add( const VertSet &Bndry )
{

#ifdef DEBUG
cerr << "Init_Add: " << Bndry << nl;
#endif

   // Big enough?
   //
   if (Bndry.size() == fixed_k+1) 
    {
      ShortParse newP(Bndry);

      SParses->addParse(newP);

      return;
    }

#ifdef GREEDY

   // Otherwise add some neighbors.
   //
   VertSet N;

   neighborhood( Adj, Bndry, N );

   int n = N.size();

//cerr << Bndry << " neighbors: " << N << nl;

   // Do we need to expand or have just enough vertices?
   //
   if ( n + Bndry.size() <= fixed_k+1 ) 
    {
      VertSet B2(Bndry);
      B2.append(N);

      Init_Add( B2 );
      return;
    }

   // Else, choose just the right amount from new onion shell.
   //
   int k = fixed_k + 1 - Bndry.size();

   Subsets choose(n,k);

   while (choose.nextset())
    {

     VertSet B2(Bndry/*, k*/);

//cerr << "(Bndry,k): (" << Bndry << ',' << k << ")\n";
//cerr << "B2(Bndry,k): " << B2 << nl;

     for (int i=0; i<k; i++) 
      {
       B2.append( N[choose.item(i)] );
      }

//cerr << "B2: " << B2 << nl;

     ShortParse newP(B2);

//cerr << "newP = " << newP << nl;

     SParses->addParse(newP);
    }

#else
#ifdef LAZY

   // Else, fill in new onion shell from remaining nodes.
   //
   int k = fixed_k + 1 - Bndry.size();

   vertNum n = Adj.size();
   //
   VertSet N(n);
   // 
   for (int i=0; i<Adj.size(); i++) N[i]=i;

   N.minus(Bndry);
   //
   n = N.size();
   
   Subsets choose(n,k);

   while (choose.nextset())
    {

     VertSet B2(Bndry/*, k*/);

//cerr << "(Bndry,k): (" << Bndry << ',' << k << ")\n";
//cerr << "B2(Bndry,k): " << B2 << nl;

     for (int i=0; i<k; i++) 
      {
       B2.append( N[choose.item(i)] );
      }

//cerr << "B2: " << B2 << nl;

     ShortParse newP(B2);

//cerr << "newP = " << newP << nl;

     SParses->addParse(newP);
    }

#else

   // Otherwise add one neighbor at a time.
   //
   VertSet N;

   neighborhood( Adj, Bndry, N );

//cerr << Bndry << " neighbors: " << N << nl;

   for (int i=0; i<N.size(); i++)
    {
      VertSet B2(Bndry);
      B2.append(N[i]);

      Init_Add( B2 );
    }

#endif
#endif

}

static void Loop_Add( const ShortParse &P, const VertSet &F )
{
#ifdef DEBUG
cerr << "Loop_Add: P=" << P << " F=" << F << nl;
#endif

   // Current interior and prospective interior lengths.
   //
   vertNum iSize=P.interiorLen();
   //
   vertNum fSize=F.size();

   // Current boundary and interior.
   //
   VertSet B(P.boundary());
   //
   VertSet I(P.interior());

   // Fast index (maybe?)
   //
   VertArray F_index(fSize);
   //
   /*
    * Find indices in the boundary for these "pull-off" vertices.
    * (Note that both lists are sorted.)
    */
   int i=0;
   int k; for (k=0; k<=fixed_k; k++)
//   for (int i=0; i<fSize; i++)
    { 
      if (B[k] == F[i]) F_index[i++]=k;
      if (i==fSize) break;
    }

   // Find neighbors of P2 = P = P.interior() \cup P.boundary() 
   //
   VertSet N,P2(I);
   //
   P2.append(B);
   //
   neighborhood( Adj, P2, N );

//cerr << "P2 = " << P2 << " N = " << N << nl;

   int n = N.size();

#ifndef BIG_BITES

   // Make room for extended interior.
   //
   I.extend(1);

   for (i=0; i<fSize; i++)	// for all pull-offs
   for (k=0; k<n; k++) 		// replace with a neighbor
    {
      vertNum idx = F_index[i];

      I[iSize]=B[idx];
      B[idx]=N[k];

      ShortParse newP(I,B);
      SParses->addParse(newP);

      B[idx]=I[iSize];	// clean up
    }

#else

   // Check for finishing parse (or squeeze).
   //
   if (fSize >= n)  // just do it!
    {

      // Keep it the same width.
      //
      for (i=0; i < n; i++) 
       { 
        I.append(F[i]);
//cerr << "I=" << I << nl;
        B[F_index[i]]=N[i];
//cerr << "B=" << B << nl;
       }

      ShortParse newP(I,B);
//cerr << "do it newP is " << newP << nl;
      SParses->addParse(newP);

      return;
    }
   else
      for (i=0; i < fSize; i++) 
       { 
        I.append(F[i]);
       }

   /*
    * Else we find all combinations for (maximally) extended parse.
    */
   B.minus(F); // fSize < n

//cerr << "B.minus(F) is " << B << nl;

   // Else, choose just the right amount from new onion shell.
   //
   vertNum bSize = B.size();
 
   assert(fSize == fixed_k + 1 - bSize);

   Subsets choose(n,fSize);

   B.extend(fSize);
   //
   while (choose.nextset())
    {
     for (int i=0; i<fSize; i++) B[bSize+i] = N[choose.item(i)];

     ShortParse newP(I,B);

//cerr << "choose it newP is " << newP << nl;

     SParses->addParse(newP);
    }
#endif
}

bool funnelPathwidth(const Graph &G, vertNum k)
{
  //cerr << G;
  vertNum n = G.order();
  fixed_k = k;

#ifdef COMPLETE_SEARCH
  SParses = new ShortParses( n-fixed_k, fixed_k+1 );
#else
  SParses = new ShortParses( (n-fixed_k)/2 + 1, fixed_k+1 );
#endif

  Deg.resize(n);
  Adj.resize(n);
  //
  G.getAdjLists( Adj, Deg );
  
#ifdef DEBUG
cerr << Adj << nl;
#endif

  VertSet Bndy;

  /*
   * Initialize
   */
  int i; for (i=0; i<n; i++)
   {
    if ( Deg[i] <= fixed_k )
      {
        Bndy = Adj[i];
        Bndy.append(i);  // closed neighborhood.

        Init_Add( Bndy );
      }
   }
         
     
  /*
   * Build up k-paths
   */
#ifdef COMPLETE_SEARCH
  for (int l=fixed_k+1; l < n; l++)
#else
  for (int l=fixed_k+1; l <= (n-fixed_k+1) / 2; l++)
#endif
   {
     const ShortParse *P = 0;
     //
     SParses->lookup(l);
     //
     while ( P = SParses->nextParse() )
      {
//cerr << "P = " << *P << nl;

        const VertSet B = P->boundary();

        VertSet F;

        for (i = 0; i < B.size(); i++)
         {
#ifdef DEBUG2
cerr << "B[i] = " << B.get(i) << nl;
cerr << "Adj[B.get(i)] = " << Adj[B.get(i)] << nl;
#endif
          if ( P->contains(Adj[B.get(i)]) ) F.append(B.get(i));
#ifdef DEBUG2
cerr << "F = " << F << " B[i] = " << B.get(i) << nl;
#endif
         }

        if (F.size()) Loop_Add( *P, F );
      }

   } 

  bool answer=false;
  
#ifdef COMPLETE_SEARCH

  answer = SParses->something(n);

#else

  // l--;
  //
  // Now check even and odd cases for smooth path-decomposition
  //
  if (l % 2) // even
   {
     if (SParses->findEven(l)) answer=true;
   }
  else // odd
   {
     if (SParses->findOdd(l)) answer=true;
   }
#endif

  delete SParses;
  SParses = 0;

  return answer;
}

extern bool k4_minor(const Graph&);
extern bool SiddProbe(const Graph&);

/*
 * Brute force search for pathwidth k without remembering path-decompositions.
 */
bool pathwidthC(const Graph &G, vertNum k)
{
  vertNum n = G.order();

  if ( k >= n-1 ) return true;

  //assert( G.connected() );

  //bool ans = DKL_GML(G,k+1);

#ifdef USETREEALG
  if (G.size() == n-1)
  {
    Tree T(G); 
    RootedTree R(T); 
    if (pathwidth(R) <= k) { /*assert(ans==true);*/ return true; }
    else		   { /*assert(ans==false);*/ return false; }
  }
  //else if (k==1) { /*assert(ans==false);*/ return false; } // k_3 obstructs
#endif

  assert(k==2);
  //cerr << "G = " << G;
  bool ansS = SiddProbe(G);
  //if (ansS) cerr << "ans = yes\n";
  if (ansS==false) return ansS;

  if (k==2 && k4_minor(G)) { cerr << "found k_4\n"; return false; }
  Dtime timer;
  timer.start();
  if (n>12 && funnelPathwidth(G,k)) 
  {  
    cerr << "found funnel; time = " << timer() << nl; 
#if 0
    aassert(DKL_GML(G,k+1));
    cerr << "found DKL; time = " << timer() << nl; 
    cerr << "for n=" << n << " m=" << G.size() << nl;
#endif
    return true; 
  }  

  timer.start();
  bool ans = DKL_GML(G,k+1);
  cerr << "DKL time = " << timer() << nl;
  return ans;
}


/*
 * fixed k pathwidth test.  Arbitrary graphs
 */
bool pathwidth(const Graph &G, vertNum k)
{
   if (G.connected()) return pathwidthC( G, k ); 

//cerr << "not connected" << nl;

   Components C;
   //
   G.components(C);

   InducedGraph *igp;

   bool answer = true;

   C.start();
   //
   while ( igp = C.next() )
    {
     Graph H(*igp);

     if ( pathwidthC( H, k ) == false ) 
      {
       answer = false;
       break;
      }
    }
   //
   C.stop();
  
   return answer;
}

/*
 * Find the least k such that there is a path-decomposition of width k.
 */
vertNum pathwidth(const Graph &G)
{
  vertNum n = G.order();

  if ( G.size()==0 || n<2 ) return 0;

  for (int k=1; k<n-1; k++) if ( pathwidth( G, k ) ) return k;

  return n-1;
}
