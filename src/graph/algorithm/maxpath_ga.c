
/*
 * Check if a graph has no paths of length greater than k.
 */

/*tex

\file{maxpath_ga.c}
\path{src/graph/algorithm}
\title{Checks longest paths <= k.}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "general/bitvect.h"
#include "graph/stdgraph.h"
#include "graph/graph.h"

//#include <iostream>

//----------------------------------------------------------------------

static int max_path_len = 0;

static bool path_gt_k(const Graph &G, BitVector span,
             vertNum path_len, vertNum last_vert)
 {
  if (path_len > max_path_len /* k */) return true;

  BitVector neighbors = G.neighbors(last_vert);

  BitVector next_vert_list = neighbors - span;
  if (!next_vert_list) return false;

  BitVector next_span = span; // (G -> vertices());

  for (int i=0; i < G.order(); i++)
   if (next_vert_list.get_bit(i))
    {
//   next_span = span;
     next_span.set_bit(i);

     if (path_gt_k(G, next_span, path_len+1, i)) return true;

     next_span.clr_bit(i);
    }

  return false;
 }

// export this function.
//
bool maxPath(const Graph &G, vertNum k)
 {
  max_path_len = k;				// set up static "k"

  BitVector span(G.order());

  for (int i=0; i < G.order()-1; i++)		// avoid last vertex.
   {
    span.clr_all();
    span.set_bit(i);
    if (path_gt_k(G, span, 0, i)) return false;
   }

  return true;
 }
