
/*tex
 
\file{distance_ga.c}
\path{src/graph/algorithm}
\title{Computes distance matrix for graphs}
\classes{}
\makeheader
 
xet*/

#include "graph/graph.h"
#include "graph/algorithm/distance_ga.h"

/*
 * Returns distance matrix for a graph.
 */
void dist_mat(const Graph &G, DistanceMatrix &d)
  {
   int i,j,k;
   vertNum order = G.nodes();
//cerr << "dist:  order = " << order << nl;

   /*
    * Resize distance matrix if needed.
    */
   assert(order == d.dim());
   // d.setDim(order);

   /*
    * Look at all entries for now (digraph version).
    */
   for (i=0; i<order; i++)              // Initial distances known.
    {
     for (j=i+1; j<order; j++)		
       if (G.isEdge(i,j)) { d.put(i,j,1); d.put(j,i,1); }
	else 		  { d.put(i,j,order); d.put(j,i,order); }
    }
   for (i=0; i<order; i++) d.put(i,i,0);

   for (k=0; k<order; k++)
    for (i=0; i<order; i++)
     for (j=0; j<order; j++)
       if ( d.at(i,j) > d.at(i,k) + d.at(k,j) ) d(i,j) = d.at(i,k) + d.at(k,j);

   return;
  }

