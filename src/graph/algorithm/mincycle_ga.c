
/*
 * Find the girth and/or smallest cycle in a graph.
 */

/*tex

\file{mincycle_ga.c}
\path{src/graph/algorithm}
\title{Finds girth and/or return smallest cycle.}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
//#include "general/bitvect.h"
#include "general/queue.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "graph/graph.h"
#include "graph/algorithm/cycle_ga.h"
#include "graph/algorithm/path_ga.h"

#include <iostream>

//----------------------------------------------------------------------

struct VertLevel
 {
  vertNum level, path;

  bool operator==(const VertLevel VL) const
   {
    if ( level == VL.level && path == VL.path ) return true;
    else					return false;
   }
 };

/*
 * Returns the size of smallest cycle containing vertex v.
 */
vertNum minCycle( const AdjLists &L,
                  vertNum v, vertNum maxLen )
 {
  vertNum n = L.size();
  
  vertNum maxSize = maxLen ? maxLen : n;

//cerr << "minCycle at vertex " << v << " with bound of " << maxSize << nl;

  assert( v >=0 && v < n );

  VertLevel vlVoid;
            vlVoid.level = n+1;
            vlVoid.path = n;
  //
  Array< VertLevel > atVL(n);
  //
  atVL.fill( vlVoid );
  //
  atVL[v].level = 0;

  Queue< vertNum > toDo[2];

  /*
   * Set up distinct paths from vertex v.
   */

  vertNum level = 1;
  //
  vertNum vert;

  int i; for (i=0; i<L[v].size(); i++)
   {
     vert = L[v][i];

     atVL[vert].level = level;
     atVL[vert].path = i;

     toDo[1].enque(vert);
   }

  bool knowMin = false;

  while ( level <= (maxSize-1)/2 )
   {

    vertNum parity = level % 2;

//cerr << "level = " << level << nl;
//cerr << "queue: " << CAST(  toDo[ parity ], DyArray<vertNum> ) << nl;

    while ( knowMin == false && toDo[ parity ].deque(vert) )
     {

      for ( i=0; i<L[vert].size(); i++ )
       {
        vertNum neighbor = L[vert][i];

        // if ( neighbor == v ) { assert(level == 1); continue; }

        vertNum nLevel = atVL[neighbor].level;

        if ( nLevel < level ) { assert( nLevel == level-1 ); continue; }

        if ( nLevel == level )
         {
          if ( atVL[neighbor].path != atVL[vert].path )	// min cycle found
           {
             maxSize = 2*level;

             knowMin = true; 
             break;
           }
         }
        else if ( nLevel == level+1 )
         {
          if ( atVL[neighbor].path != atVL[vert].path )
           {
             maxSize = 2*level + 1;
           }
         }
        else	// unseen vertex
         {
          atVL[neighbor].level = level+1;
          atVL[neighbor].path = atVL[vert].path;

          toDo[ !parity ].enque(neighbor);
         }

       } // next neighbor of vert
      
     } // next vert at level

    level++;

   } // span next level

  toDo[0].empty();
  toDo[1].empty();

  return maxSize+1;

 }


vertNum minCycle( const Graph &G, vertNum v, vertNum maxSize )
 {
  vertNum n = G.order();

  AdjLists L(n);

  G.getAdjLists( L );

  return minCycle( L, v, maxSize );
 }

//----------------------------------------------------------------------

/*
 * Returns the length of the smallest cycle (girth).
 */
vertNum girth( const AdjLists &L, vertNum maxLen )
 {
  vertNum n = L.size();

  vertNum maxSize = maxLen ? maxLen : n;

  vertNum smallest = maxSize+1;

  for (int i=0; i<n-2; i++)
   {
     vertNum cycleLen = minCycle( L, i, smallest-1 );

     if (cycleLen < smallest) smallest = cycleLen;
   }

  return smallest;
 }

vertNum girth( const Graph &G, vertNum maxSize )
 {
  vertNum n = G.order();

  AdjLists L(n);

  G.getAdjLists( L );

//cerr << "AdjLists:\n" << L << nl;

  return girth( L, maxSize );
 }

/************************************************************************
 * The following functions do the same as above but also returns cycle. *
 ***********************************************************************/

// Currently need this if cycle is desired. (minpath.o)
//
vertNum minPath( Path &P, const AdjLists &L, 
                 vertNum u, vertNum v, vertNum maxLen );

/*
 * Returns smallest cycle containing vertex v.
 */
vertNum minCycle( Cycle &C, 
                  const AdjLists &L,
                  vertNum v, vertNum maxLen )
 {
  vertNum n = L.size();
  
  vertNum maxSize = maxLen ? maxLen : n;

//cerr << "minCycle at vertex " << v << " with bound of " << maxSize << nl;

  assert( v >=0 && v < n );

  VertLevel vlVoid;
            vlVoid.level = n+1;
            vlVoid.path = n;
  //
  Array< VertLevel > atVL(n);
  //
  atVL.fill( vlVoid );
  //
  atVL[v].level = 0;

  // Keep track of the different paths from vertex v.
  //
  vertNum pA = v, pB, pC = v;
 
  Queue< vertNum > toDo[2];

  /*
   * Set up distinct paths from vertex v.
   */

  vertNum level = 1;
  //
  vertNum vert;

  int i; for (i=0; i<L[v].size(); i++)
   {
     vert = L[v][i];

     atVL[vert].level = level;
     atVL[vert].path = i;

     toDo[1].enque(vert);
   }

  bool knowMin = false;

  while ( level <= (maxSize-1)/2 )
   {

    vertNum parity = level % 2;

//cerr << "level = " << level << nl;
//cerr << "queue: " << CAST(  toDo[ parity ], DyArray<vertNum> ) << nl;

    while ( knowMin == false && toDo[ parity ].deque(vert) )
     {

      int i; for ( i=0; i<L[vert].size(); i++ )
       {
        vertNum neighbor = L[vert][i];

        vertNum nLevel = atVL[neighbor].level;

        if ( nLevel < level ) { assert( nLevel == level-1 ); continue; }

        if ( nLevel == level )
         {
          if ( atVL[neighbor].path != atVL[vert].path )	// min cycle found
           {
             pA = vert;
             pB = neighbor;

             maxSize = 2*level;

             knowMin = true; 
             break;
           }
         }
        else if ( nLevel == level+1 )
         {
          if ( atVL[neighbor].path != atVL[vert].path )
           {
             if ( pC != v ) continue; // skip repeat lengths

             pA = vert;
             pC = neighbor;

             int j;
             for (j=0; j<L[neighbor].size(); j++)
              {
                pB = L[neighbor][j];

                if ( atVL[pB].path == atVL[neighbor].path &&
                     atVL[pB].level == atVL[neighbor].level-1 ) break;
              } 
             //
             assert( j<L[neighbor].size() );

             maxSize = 2*level + 1;
           }
         }
        else	// unseen vertex
         {
          atVL[neighbor].level = level+1;
          atVL[neighbor].path = atVL[vert].path;

          toDo[ !parity ].enque(neighbor);
         }

       } // next neighbor of vert
      
     } // next vert at level

    level++;

   } // span next level

  toDo[0].empty();
  toDo[1].empty();

  /*
   * Abort if bound has been exceeded.
   */
  if ( pA == v ) return maxSize+1;

  Path pathA(maxSize), pathB(maxSize); 

  vertNum pAlen = minPath( pathA, L, v, pA );
  vertNum pBlen = minPath( pathB, L, v, pB );

  C.resize(maxSize+1);

  vertNum idx = 0;
  //
  for (i=0; i<=pAlen; i++) C[idx++] = pathA[i];
  //
  if ( maxSize % 2 ) C[idx++] = pC;
   
  for (i=pBlen; i>0; i--) C[idx++] = pathB[i];

  assert( idx == maxSize+1 );

  return maxSize+1;

 }

vertNum minCycle( Cycle &C,
                  const Graph &G, vertNum v, vertNum maxSize )
 {
  vertNum n = G.order();

  AdjLists L(n);

  G.getAdjLists( L );

  return minCycle( C, L, v, maxSize );
 }

//----------------------------------------------------------------------

/*
 * Returns the length of the smallest cycle (girth).
 */
vertNum girth( Cycle &C,
               const AdjLists &L, 
               vertNum maxLen )
 {
  vertNum n = L.size();

  vertNum maxSize = maxLen ? maxLen : n;

  Cycle smallestC(maxSize);

  vertNum smallest = maxSize+1;

  for (int i=0; i<n-2; i++)
   {
     vertNum cycleLen = minCycle( C, L, i, smallest-1 );

     if (cycleLen < smallest) 
      {
       smallest = cycleLen;
       smallestC = C;
      }
   }

  C = smallestC;
  //
  return smallest;
 }

vertNum girth( Cycle &C,
               const Graph &G, vertNum maxSize )
 {
  vertNum n = G.order();

  AdjLists L(n);

  G.getAdjLists( L );

  return girth( C, L, maxSize );
 }

