/*
 * Compute exact pathwidth of a graph. O(nk 2^n) time for pw = k.
 *
 * The algorithm used for pathwidth is the one here:
 * http://www.sagemath.org/doc/reference/sage/graphs/graph_decompositions/vertex_separation.html
 * with some additional optimisations.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <algorithm>

// NAUTY
#include "gtools.h"

// A few modifications that might help on large/difficult graphs

//#define INIT_DEGREE_HEURISTIC
// Define at most one of the following
//#define SELECT_ISOLATED
//#define CHECK_ISOLATED

//#define DBG(x) x
#define DBG(x)

#undef POPCOUNT
template<class Type>
inline int POPCOUNT( Type a )
{
#if __GNUC__
   return __builtin_popcount(a);
#else
   int cnt=0;
   char *cptr = (char *) &a;

   for (int i=0; i<sizeof(Type); cptr++, i++)
      for (int j=0; j<8; j++)
         if (*cptr & (1<<j)) cnt++;

   return cnt;
#endif
}

#define MAXVERT 16
#define MAXEDGES ((MAXVERT * MAXVERT) / 2)

// Preallocated table
char glbounds[1 << MAXVERT];
//setword ggraph[MAXVERT];

class PWGraph
{
  public:
   int order;
   setword *graph;
   char *lbounds;
   int cost;
   int g_displacement;
   int nodes, nodes2;
   //std::pair<int, int> degrees[32];
    
   PWGraph( int order_, setword *graph_ )
   {
      order = order_;
      graph = graph_;
      g_displacement = 8 * sizeof(setword) - order;
      //graph = new setword[order]();
      //memset(graph, 0, sizeof(setword)*order);

      lbounds = glbounds;     
      //lbounds = new char[1 << order];
      memset( lbounds, -1, 1 << order );
      nodes = nodes2 = 0;
   }

   ~PWGraph()
   {
      //delete graph;
      //delete lbounds;
   }

   void indent( setword n )
   {
      for (int i = POPCOUNT(n); i; i--) printf(" ");
   }

   // eliminated & neighbourset use from-right numbers, graph uses from-left (BAD)
   int visit( int depth, setword eliminated, setword neighbourset, int newest )
   {
      nodes++;
      setword old = neighbourset;
      // Skip on initial call.
      // Compiler will optimise away this computation when not needed.
      if ( eliminated )
      {
         neighbourset = ((neighbourset | graph[newest]) & ~eliminated);
      }
      char *entry = &lbounds[eliminated >> g_displacement];

      if ( *entry < 0 )
      {
         *entry = POPCOUNT( neighbourset );
      }
      //else
      //   return *entry;
         
      if ( *entry > cost || depth >= order - 1 )
      {
         DBG( indent(eliminated); printf("prune %04x, %d\n", eliminated, *entry) );
         return *entry;
      }
      nodes2++;

#ifdef SELECT_ISOLATED

//      if (depth >= order - 2)
      for (int i = 0; i < order; i++)
      {
         if ( ISELEMENT( &eliminated, i) )
            continue;
         if ( (graph[i] & ~eliminated) == 0 )
         {
            // Has no unselected neighours, so selecting it doesn't increase width
            setword newelim = eliminated;
            ADDELEMENT( &newelim, i );
            *entry = visit( depth + 1, eliminated, neighbourset, i );
            return *entry;
         }
      }

#endif

      DBG( indent(eliminated); printf("v(%d) %04x %04x->%04x newest=%d init=%d\n", cost, eliminated, old, neighbourset, newest, *entry) );

      int mini = order;
      for (int j = order - 1; j >= 0; j--)
//      for (int j = 0; j < order; j++)
      {
#ifdef INIT_DEGREE_HEURISTIC
         int i = degrees[j].second;
#else
         int i = j;
#endif

         if ( ISELEMENT( &eliminated, i ) )
            continue;
         setword newelim = eliminated;
         ADDELEMENT( &newelim, i );
         int temp = visit( depth + 1, newelim, neighbourset, i);
         mini = std::min(mini, temp);
         if (mini <= cost)
         {
            DBG( indent(eliminated); printf("re  %04x = %d (found %d child)\n", eliminated, *entry, mini) );
            return *entry;
         }
#ifdef CHECK_ISOLATED
         if ( (graph[i] & ~eliminated) == 0 )
         {
            // Has no unselected neighours, so selecting it doesn't increase width
            *entry = temp;
            return temp;
         }
#endif
      }
      assert( mini >= *entry );
      *entry = mini;

      DBG( indent(eliminated); printf("ret %04x = %d\n", eliminated, mini) );
      return mini;
   }

   void edge( int a, int b )
   {
      ADDELEMENT( graph + b, a );
      ADDELEMENT( graph + a, b );
   }

   int pathwidth()
   {
#ifdef INIT_DEGREE_HEURISTIC
      for (int k = 0; k < order; k++)
      {
         degrees[k] = std::make_pair( POPCOUNT(graph[k]), k );
      }
      std::sort( degrees, degrees + order );
#endif

      for (int k = 0; k < order - 1; k++)
      {
         cost = k;
         int temp = visit( 0, 0, 0, -1 );
         DBG( printf("loop cost=%d result=%d\n", k, temp) );
         assert( temp >= k );
         if ( temp == k )
            return k;
      }
      return order - 1;
   }

};

#ifdef FINDPW

#include <iostream>
#include <sstream>

using namespace std;

int main() {

   while (1)
   {
      int n;
      cin >> n >> ws;
      if ( !n || !cin ) break;
      if ( n > MAXVERT )
      {
         cerr << "n = " << n << " > MAXVERT = " << MAXVERT << ", recompile required.\n";
         abort();
      }

      setword graph_buf[MAXVERT] = {0};
      PWGraph G( n, graph_buf );
      for (int i = 0; i < n; i++)
      {
         string str;
         getline( cin, str );
         stringstream ss( str );
         while ( 1 )
         {
            int j;
            ss >> j;
            if ( !ss ) break;
            G.edge( i, j );
         }
      }
      cout << n << " vertices, pw " << G.pathwidth() << "\n";
   }

   return 0;
}

#endif

#if 0

// For testing
int main() {
   int BITS =18;
   int tnodes = 0, tnodes2 = 0;
   for (int gen = 0; gen < (1<<BITS); gen++)
   {
      PWGraph G(MAXVERT);
      int v2 = 5;
      for (int i = 0; i < BITS; i++) {
         if (gen & (1 << i))
         {
            int v1 = i % G.order;
            v2 = (++v2) % G.order;
            G.edge(v1, (i^7)% G.order);
            G.edge(MAXVERT-v1, (i^5)% G.order);
            G.edge(v1, v2);
            G.edge(v1, (i^gen+1)% G.order);
            G.edge(MAXVERT-v2, (i+gen)% G.order);
            G.edge(v2, (i^gen^7)% G.order);
         }
      }
      G.edge(8,1);
      G.edge(2,1);
      G.edge(3,2);
      G.edge(4,3);
      G.edge(9,10);
      G.edge(7,8);
      //       G.edge(1,3);
      int temp =G.pathwidth();
      //printf("%d pw %d nodes %d %d\n", gen, temp, G.nodes, G.nodes2);
//        printf("%d pw %d\n", gen, temp);
      tnodes+=G.nodes;
      tnodes2+=G.nodes2;
   }
   printf("nodes %d %d\n", tnodes, tnodes2);
   return 0;
}

#endif
