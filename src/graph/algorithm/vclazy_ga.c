
/*
 * See if a graph has a k-vertex cover set.
 */

/*tex

\file{vcLazy_ga.c}
\path{src/graph/algorithm}
\title{Definition of a lazy approach to k-vertex cover.}
\classes{}
\makeheader

xet*/


#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/graph.h"
#include "graph/indgrph.h"
#include "general/sets.h"
#include "general/bitvect.h"

//#define WATCHB
#ifdef WATCHB
#include <iostream>
#endif

//----------------------------------------------------------------------

/*
 * Pick all k subsets of a graph and see if G\{v1,v2,...vk} has
 * any edges.
 */
bool vcLazy(const Graph &G, vertNum k)
 {
  vertNum n = G.order();

  if ( k >= n-1 ) return true;

  BitVector missing(n);

  InducedGraph H(G);

  Subsets vert(n,k);

  while (vert.nextset())
   {
    missing.clearAll();
    //
    for (int i=0; i<k; i++) missing.setBit(vert.item(i));
    //
    missing.complement();

    H.setIndex( missing );

#ifdef WATCHB
//  cerr << "No edge test for:\n";
//  cerr << H << nl;
  cerr << nl;
  for (i=0; i<k; i++) cerr << vert.item(i) << ' ';
  cerr << nl;
#endif

    if ( H.size()==0 ) { return true; }
   }

  return false;
 }

#if 1
/*
 * Find the least k such that k is a k-vc.
 */
vertNum vcLazy(const Graph &G)
 {
  vertNum n = G.order();

  if ( G.size()==0 || n<2 ) return 0;

  vertNum yesK=n-1, noK=0, k;
 
  while ( yesK - noK > 1 )
   {
    k = (yesK - noK)/2 + noK;

#ifdef WATCHB
cerr << "checking for k=" << k << nl;
#endif

    if ( vcLazy( G, k ) ) yesK=k; else noK=k;
   }

  return yesK;
 }
#else
/*
 * Find the least k such that k is a k-vc.
 */
vertNum vcLazy(const Graph &G)
 {
  vertNum n = G.order();

  if ( G.size()==0 || n<2 ) return 0;

  for (int k=1; k<n-1; k++) if ( vcLazy( G, k ) ) return k;

  return n-1;
 }
#endif
