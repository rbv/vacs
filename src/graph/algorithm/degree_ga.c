
/*tex
 
\file{degree_ga.c}
\path{src/graph}
\title{Degree Sequence Query Functions}
\classes{}
\makeheader
 
xet*/

#include "graph/graph.h"
#include "graph/stdgraph2.h"

// debug
//#include <iostream>

// ---------------------------------------------------------------------

/*
 * Some standard pretests predefined.
 */
bool NoDegreeLessEqualKVertices( const Graph &G, int k )
 {
   vertNum n = G.order();

   DegreeSequence deg(n);
   //
   G.degVec(deg);

   for (int i=0; i<n; i++) if ( deg[i] <= k ) return false;

   return true;
 }  
//
bool NoDegreeZeroVertices( const Graph &G )
 {
   return NoDegreeLessEqualKVertices( G, 0 );
 }
bool NoDegreeZeroOrOneVertices( const Graph &G )
 {
   return NoDegreeLessEqualKVertices( G, 1 );
 }  
bool NoDegreeZeroToTwoVertices( const Graph &G )
 {
   return NoDegreeLessEqualKVertices( G, 2 );
 }  
