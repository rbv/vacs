
/*tex
 
\file{reduce.c}
\path{src/graph}
\title{Utility functions to reduce graphs}
\classes{}
\makeheader
 
xet*/

#include "graph/graph.h"

#include <iostream>

/*
 * Internal adjacency list routines.
 */

static void replaceLabel( AdjLists &L, vertNum oldL, vertNum newL )
 {
//cerr << "in replaceLabel" << nl;

  for (int i=0; i<oldL; i++) 		// only do this much
   for (int j=0; j<L[i].size(); j++)
    {
      if ( L[i][j] == oldL ) L[i][j] = newL;
    }
 }

static bool connectedAdjList( AdjLists &L, vertNum u, vertNum v )
 {
//cerr << "in connectedAdjList" << nl;

  for (int i=0; i<L[u].size(); i++ ) if ( L[u][i] == v ) return true;

  return false;
 }

static void contractAdjListEdge( AdjLists &L, vertNum u, vertNum v )
 {
  assert( connectedAdjList( L, u, v ) );

//cerr << "in contractAdjListEdge" << nl;

  vertNum n = L.size();

  /*
   * Make everything adjacent to v now adjacent to u.
   */
  int i; for (i=0; i < n; i++)
   {
    if (i==u || i==v) continue;

    for (int j=0; j < L[i].size(); j++)
      if ( L[i][j]==v ) L[i][j]=u;
   }

  /*
   * Now expand u's adjacency list.
   */
  int uIdx = L[u].size();

  L[u].extend(L[v].size()-2);

  for (i=0; i<L[v].size()-2; i++)
   {
    vertNum neighbor = L[v][i];

    if (neighbor != u) L[u][uIdx++]=neighbor;
   }

  vertNum neighbor1 = L[v][i];
  vertNum neighbor2 = L[v][i+1];
  //
  if (neighbor1 == u) 
   {
    L[u][L[u].index(v)]=neighbor2;
   }
  else if (neighbor2 == u)
   {
    L[u][L[u].index(v)]=neighbor1;
   }
  else
   {
    L[u][L[u].index(v)]=neighbor1;
    L[u][uIdx++]=neighbor2;
   }

   if ( v < L.size()-1 ) 
    {
     /*
      * Relabel references to last node as new v node.
      */
     for (int i = 0; i<L[n-1].size(); i++)
      {
       vertNum neighbor = L[n-1][i];

       if ( neighbor == v ) neighbor = u;

       for (int j=0; j<L[neighbor].size(); j++)
         if ( L[neighbor][j] == n-1 )
           {
              L[neighbor][j] = v;
              break;
           }
      }

     L.swap( v, n-1 );

    }
   //
   L.shrink(1);

 }

//------------------------------------------------------------------------
// Public functions.
//------------------------------------------------------------------------

/*
 * Removes a vertex from an AdjLists.
 */
void removeVertex( AdjLists &L, vertNum v )
 {
//cerr << "in removeVertex" << nl;

  vertNum n = L.size();

  assert( v>=0 && v < n );

  for (int i=0; i<L[v].size(); i++) 
   {
    vertNum neighbor = L[v][i];
  
    L[neighbor].swap( L[neighbor].index(v), L[neighbor].size()-1 );
    L[neighbor].shrink(1);
   }

  L.swap( v, n-1 );

  replaceLabel( L, n-1, v );

  L.shrink(1);
 }

/*
 * Removes all isolated vertices from graph.
 */
void removeDegree0( AdjLists &L )
 {
//cerr << "in removeDegree0" << nl;

   vertNum n = L.size();
   //
   vertNum newOrder = n;

   for (int i=0; i<newOrder; i++)
    {
     if ( L[i].size() == 0 ) 
      { 
       L.swap( i, --newOrder );

       replaceLabel( L, newOrder, i );

       i--;
      }
    }

   L.resize(newOrder);

   return;
 }

void removeDegree0( Graph &G )
 {
  G.rmIsoNodes();
 }


/*
 * Removes all degree one vertices from graph.
 */
void removeDegree1( AdjLists &L )
  {
//cerr << "in removeDegree1" << nl;

   vertNum n = L.size();
   //
   vertNum newOrder = n;

   for (int i=0; i<newOrder; i++)
    {
     if ( L[i].size() == 1 ) 
      { 
       // Remove neighbor's link.
       //
       vertNum neighbor = L[i][0];
        
       L[neighbor].swap( L[neighbor].index(i), L[neighbor].size()-1 );
       
       L[neighbor].shrink(1);
 
       // Now remove degree 1 vertex.
       //
       if ( i < --newOrder )
        {
         L.swap( i, newOrder );

         replaceLabel( L, newOrder, i );
        }

       i--;
      }
    }

   L.resize(newOrder);

   if ( newOrder !=n ) removeDegree1( L ); // May have caused more deg 1s.

   return;
  }

void removeDegree1( Graph &G )
  {
   (void) G;
   aassert(0);
  }

/*
 * Removes all degree zero or one vertices from graph.
 */
void removeDegree0or1( AdjLists &L )
  {
//cerr << "in removeDegree0or1" << nl;

   vertNum n = L.size();
   //
   vertNum newOrder = n;

   for (int i=0; i<newOrder; i++)
    {
     if ( L[i].size() <= 1 ) 
      { 

       if ( L[i].size() == 1 ) 
        { 
         // Remove neighbor's link.
         //
         vertNum neighbor = L[i][0];
        
         L[neighbor].swap( L[neighbor].index(i), L[neighbor].size()-1 );
       
         L[neighbor].shrink(1);
        }
 
       // Now remove degree 0 or 1 vertex.
       //
       if ( i < --newOrder )
        {
         L.swap( i, newOrder );

         replaceLabel( L, newOrder, i );
        }

       i--;
      }
    }

   L.resize(newOrder);

   if ( newOrder !=n ) removeDegree0or1( L ); // May have caused more deg 1s.

   return;
  }

void removeDegree0or1( Graph &G )
  {
   (void) G;
   aassert(0);
  }

/*
 * Contract out all degree two vertices from graph 
 * (without forming a multi-edge).
 */
void removeDegree2( AdjLists &L )
  {
//cerr << "in removeDegree2" << nl;

   vertNum n = L.size();

   for (int i=0; i<n; i++)
    {
     if ( L[i].size() == 2 ) 
      { 
       if ( connectedAdjList( L, L[i][0], L[i][1] ) == false )
        {
          contractAdjListEdge( L, i, L[i][0] );

          removeDegree2( L );
          break;
        }
      }
    }

   return;
  }

void removeDegree2( Graph &G )
  {
   (void) G;
   aassert(0);
  }

/*
 * Remove degree 0s and 1s and contract out all feasible degree 2s from graph.
 */
void removeDegree0to2( AdjLists &L )
  {
    removeDegree0or1( L );
    removeDegree2( L );
  }

void removeDegree0to2( Graph &G )
  {
    removeDegree0or1( G );
    removeDegree2( G );
  }
