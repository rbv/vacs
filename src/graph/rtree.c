
/***********************************************************************
 * Source for rooted trees.
 **********************************************************************/

/*tex

\file{rtree.c}
\path{src/graph}
\title{Source for rooted trees}
\classes{RootedTree}
\makeheader

xet*/


#include "graph/rtree.h"

static void set_parents( const Tree &T, vertNum root, VertArray &_parents )
 {
//cerr << "1) root " << root << " parent indices: " << _parents << nl;

   for (int i=0; i<T.degree(root); i++)
    {
     vertNum child = T.neighbor(root,i);

//cerr << "child=" << child << nl;

     if ( _parents.get(child) == T.order() ) 
      {
        _parents[child] = 0; // touched
        set_parents( T, child, _parents );
      }
     else // We have found parent's index.
      {
        _parents[root] = i;
      }
    }
//cerr << "2) root " << root << " parent indices: " << _parents << nl;
 }

/*
 * Tree constructor
 */
RootedTree::RootedTree(const Tree &T, vertNum root) 
           : Tree(T), _parents(T.order())
 {
  assert(root>=0 && root <= T.order());

  _root = root;

  _parents.fill(order());

  _parents[_root]=_root;  // touched flag. (i.e. != order())
  //
  set_parents( T, _root, _parents );

  //cerr << "final parent indices: " << _parents << nl;
 }

/*
 * I/O stuff (Adjency list format).
 */
ostream& operator<<( ostream& o, const RootedTree& T )
 {
   // o << nl << "Rooted tree at vertex " << T.root() << nl;

   for ( int i=0; i<T.order(); i++ )
   {
      o << i << (i==T.root() ? " * " : " : ") << T.adjLists[i] << nl;
   }

   return o;
 }


