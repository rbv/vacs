
/***********************************************************************
 * Source for Trees.
 **********************************************************************/

/*tex

\file{tree.c}
\path{src/graph}
\title{Source for Trees (and Labeled Trees)}
\classes{Tree}
\makeheader

xet*/


#include "graph/tree.h"
#include "graph/forest.h"
#include "general/queue.h"
#include "general/bitvect.h"
#include "graph/graph.h"

/*
 * Binary tree type constructors.
 */

Tree::Tree(const Tree &T1, const Tree &T2) : adjLists(T1._order + T2._order + 1)
 {
  _order = T1._order + T2._order + 1;

  adjLists[0].resize(2);
  adjLists[0][0]=1;
  adjLists[0][1]=T1._order;

  /*
   * Merge first tree.
   */
  adjLists[1].resize(T1.adjLists[0].size()+1);
  adjLists[1][0]=0;
  // 
  int adjLen=T1.adjLists[0].size();
  int j; for (j=0; j<adjLen; j++) adjLists[1][j]=T1.adjLists[0][j]+1;

  int i; for (i=1; i<T1._order; i++)
   {
     adjLen=T1.adjLists[i].size();

     adjLists[i+1].resize(adjLen);

     for (int j=0; j<adjLen; j++)
       adjLists[i+1][j]=T1.adjLists[i][j]+1;
   }

  /*
   * Merge second tree.
   */
  adjLists[T1._order+1].resize(T2.adjLists[0].size()+1);
  adjLists[T1._order+1][0]=0;
  // 
  adjLen=T2.adjLists[0].size();
  for (j=0; j<adjLen; j++) 
    adjLists[T1._order][j]=T2.adjLists[0][j]+T1._order+1;

  for (i=1; i<T2._order; i++)
   {
     adjLen=T2.adjLists[i].size();

     adjLists[i+T1._order+1].resize(adjLen);

     for (int j=0; j<adjLen; j++)
       adjLists[i+T1._order+1][j]=T2.adjLists[i][j]+T1._order+1;
   }

 }
//
Tree::Tree(const Tree &T1, const Tree &T2, vertNum u, vertNum v) : 
  	adjLists(T1._order + T2._order)
 {

  assert( u < T1._order && v < T2._order );
 
  _order = T1._order + T2._order;

  int adjLen;

  /*
   * Merge first tree.
   */
  int i; for (i=0; i<T1._order; i++)
   {
     adjLen=T1.adjLists[i].size();

     adjLists[i].resize( i==u ? adjLen+1 : adjLen );

     for (int j=0; j<adjLen; j++)
        adjLists[i][j]=T1.adjLists[i][j];

     if (i==u) adjLists[i][adjLen] = T1._order+v;
   }

  /*
   * Merge second tree.
   */
  for (i=0; i<T2._order; i++)
   {
     adjLen=T2.adjLists[i].size();

     adjLists[i+T1._order].resize( i==v ? adjLen+1 : adjLen );

     for (int j=0; j<adjLen; j++)
       adjLists[i+T1._order][j]=T2.adjLists[i][j]+T1._order;

     if (i==v) adjLists[i+T1._order][adjLen] = u;
   }

 }

Tree::Tree(const VertArray &LS) : adjLists( 1, LS.size() )
 {
   _order = 1;
   vertNum n=LS.size();

   assert(n>=1);		// Tree has at least one node.
   assert(LS[0]==0);		// Level sequences are start at 0 for root.
   
   VertArray levelParent(n);
   levelParent[0]=0;

   for (int i=1; i<n; i++)
    {
     vertNum level = LS[i];
     levelParent[level] = addLeaf( levelParent.get(level-1) );
    }
 }

Tree::Tree(const AdjLists &L) : adjLists( L )
 {
  _order = adjLists.size();

  //
  // Now should make sure we have a tree!  (lazy and not sufficient)
  //

  vertNum count = 0;
     
  for (int i=0; i<_order; i++) 
   {
    vertNum deg = adjLists[i].size();

    assert(deg); // connected!

    count += deg;
   }

  assert(count % 2 == 0); // partial undirected test.

  assert(count/2 == _order-1);  // connected?
  
 }

//-------------------------------------------------------------------------

/*
 * Tree from graph constructor.
 */
Tree::Tree(const Graph &G, vertNum v) : adjLists(1, G.order()-1)
 {

   vertNum maxOrder = G.order();

//   cerr << "maxOrder/root " << maxOrder << '/' << v << nl;

  assert( 0<=v && v<maxOrder );

   BitVector mark(maxOrder), spanned(maxOrder);
   //
   mark.clearAll();
   spanned.clearAll();

   _InternalNode node, nextNode;
   //
   node.graph = v;
   node.tree = 0;
   mark.setBit(v);
   _order = 1;

   Queue<_InternalNode> span;
   //
   span.enque(node);
         
   /*
    * Start a breadth-first search spanning tree from vertex v.
    */

   while( span.deque(node) )
    {
     if ( spanned.getBit(node.graph) == false )
      {
        spanned.setBit(node.graph);

//cerr << "adding graph vertex " << node.graph << " (" << 
//           node.tree << ")" << nl;
           
        /*
         * See if neighbors have been added to tree.
         */
        for (int i=0; i<maxOrder; i++)
         {
          if ( i==node.graph ) continue;
          if ( G.isEdge(i,node.graph)==true && mark.getBit(i)==false )
           {
            nextNode.graph = i;
            nextNode.tree = addLeaf(node.tree); 
            mark.setBit(i);

            span.enque(nextNode);
           }
         }
      } // if !marked
    } // while queue

 }

//---------------------------------------------------------------------

/*
 * Operations on vertices (nodes)
 */
vertNum Tree::addLeaf(vertNum v)
 {
   assert(v>=0 && v<_order);

   VertSet adj(1);
   adj[0]=v;

   adjLists.append( adj );		// Which statement should be first?
   adjLists[v].append( _order ); 

   return _order++;
 }

void Tree::rmLeaf(vertNum v)
 {
   assert( isLeaf(v) );

   register int u = adjLists[v][0];
   int uv = adjLists[u].index(v);

   if ( uv < adjLists[u].size()-1 ) 
             adjLists[u].swap( uv, adjLists[u].size()-1 );
   //
   adjLists[u].shrink(1);

   if ( v < adjLists.size()-1 ) 
    {
     /*
      * Relabel last node as new v node.
      */
     for (int i = 0; i<adjLists[_order-1].size(); i++)
      {
       vertNum neighbor = adjLists[_order-1][i];

       if ( neighbor == v ) continue;

       for (int j=0; j<adjLists[neighbor].size(); j++)
         if ( adjLists[neighbor][j] == _order-1 )
           {
              adjLists[neighbor][j] = v;
              break;
           }
      }

     adjLists.swap( v, _order-1 );
    }
   //
   adjLists.shrink(1);

   _order--;
 }


/*
 * Return the pieces of a tree after a vertex is removed.
 */
Forest* Tree::rmNode(vertNum v) const
 {
   assert(v>=0 && v<_order);

   VertArray map(_order);		// Index for subtrees.

   map.fill(_order);
   map[v]=0;				// A big fake.

   Forest *F = new Forest();

   /*
    * Forest will contain this many trees.
    */
   for (int i=0; i<adjLists[v].size(); i++)
    {
     /*
      * We do a breath first search away for each of the neighbors.
      */
     vertNum neighbor = adjLists[v][i];

     Queue<vertNum> toDo;
     //
     toDo.enque(neighbor);		// Starting place is now set.

     Tree *T = new Tree(); 	
     //
     map[neighbor]=0;

     vertNum span=0;
     //
     while (toDo.deque(span))
      {
        for (int j=0; j<adjLists[span].size(); j++)
         {
          neighbor = adjLists[span][j];

          if ( map[neighbor] == _order )
           {
             toDo.enque(neighbor);
           }
          else // parent
          if ( neighbor != v )
           {
             map[span] = T->addLeaf( map[neighbor] );
           }

         } // next j
 
        assert( map[span] != _order );
 
      } // while queue

     F->add(T);

    } // next i (v neighbor).

   return F;
 }

//------------------------------------------------------------------

/*
 * Operations on edges.  -- resulting vertex index is returned in u.
 */
void Tree::contract(vertNum &u, vertNum v)
 {
  assert( isEdge( u, v ) );

  if (isLeaf(v)) 
   { 
    if ( u == _order-1 ) u = v;

    rmLeaf(v); 
    return;
   }

  if (isLeaf(u)) 
   { 
    if ( v == _order-1 ) 
     {
      rmLeaf(u);	// rmLeaf alters _order and adjList so u==v.
     }
    else
     {
      rmLeaf(u);
      u = v;
     }
    return;
   }
  
  /*
   * Make everything adjacent to v now adjacent to u.
   */
  int i; for (i=0; i < _order; i++)
   {
    if (i==u || i==v) continue;

    for (int j=0; j < adjLists[i].size(); j++)
      if ( adjLists[i][j]==v ) adjLists[i][j]=u;
   }

  /*
   * Now expand u's adjacency list.
   */
  int uIdx = adjLists[u].size();

  adjLists[u].extend(adjLists[v].size()-2);

  for (i=0; i<adjLists[v].size()-2; i++)
   {
    vertNum neighbor = adjLists[v][i];

    if (neighbor != u) adjLists[u][uIdx++]=neighbor;
   }

  vertNum neighbor1 = adjLists[v][i];
  vertNum neighbor2 = adjLists[v][i+1];
  //
  if (neighbor1 == u) 
   {
    adjLists[u][adjLists[u].index(v)]=neighbor2;
   }
  else if (neighbor2 == u)
   {
    adjLists[u][adjLists[u].index(v)]=neighbor1;
   }
  else
   {
    adjLists[u][adjLists[u].index(v)]=neighbor1;
    adjLists[u][uIdx++]=neighbor2;
   }

   if ( v < adjLists.size()-1 ) 
    {
     /*
      * Relabel references to last node as new v node.
      */
     for (int i = 0; i<adjLists[_order-1].size(); i++)
      {
       vertNum neighbor = adjLists[_order-1][i];

       if ( neighbor == v ) neighbor = u;

       for (int j=0; j<adjLists[neighbor].size(); j++)
         if ( adjLists[neighbor][j] == _order-1 )
           {
              adjLists[neighbor][j] = v;
              break;
           }
      }

     adjLists.swap( v, _order-1 );

     if ( u == _order-1 ) u = v;

    }
   //
   adjLists.shrink(1);


   _order--;
 }

vertNum Tree::subdivide(vertNum u, vertNum v)
 {
   assert( isEdge( u, v ) );

   VertSet adj(2);
   adj[0]=u;
   adj[1]=v;

   adjLists.append( adj );		// Again, do this first?

   int i; for (i=0; i<adjLists[u].size(); i++)
    if ( adjLists[u][i] == v )
      {
        adjLists[u][i] = _order;
        break;
      }

   for (i=0; i<adjLists[v].size(); i++)
    if ( adjLists[v][i] == u )
      {
        adjLists[v][i] = _order;
        break;
      }

   return _order++;
 }


//------------------------------------------------------------------

/*
 * Tree's degree sequence.
 */
void Tree::degVec( DegreeSequence &deg, int flag) const
 {
  deg.resize( _order );

  for (int i=0; i<_order; i++) deg[i]=adjLists[i].size();

  if (flag) deg.sortDown();

  return;
 }

/*
 * Tree's center/bicenter.
 */
bool Tree::center( vertNum &c1, vertNum &c2 ) const
 {

  if (_order==1) { c1 = 0; return true; }	// Trivial case.

  DegreeSequence deg(_order);
  //
  degVec(deg);
  
  DegreeSequence del(_order);		// Keep track of edge deletes.
  //
  del.fill(0); 			


  vertNum pruned=1;			// One ahead of _order.

  while ( pruned < _order )
   {
    /*
     * Find/delete all end-vertices.
     */
    int i; for (i=0; i<_order; i++) 
     {
       if (deg[i]==1)
        {
         deg[i]=_order;				// Mark as deleted.
         pruned++;

         for (int j=0; j<adjLists[i].size(); j++)
          {
           vertNum neighbor = adjLists[i][j];

           /*
            * Check for bicenters.
            */
           if ( deg[neighbor]==1 )
            {
             c1 = i;
             c2 = neighbor;
             return false; 
            }
           else del[neighbor]++;
           //else del[neighbor]=del[neighbor]+1;
          }
        } // if

     } // next i

    /*
     * Update working degree sequence.
     */
    for (i=0; i<_order; i++) 
     {
      if (del[i])
       {
        deg[i] = deg[i]-del[i];
        del[i] = 0;
       }
     } // next i

   } // while pruned<_order

  /*
   * We have a center for this tree.
   */
  for (int i=0; i<_order; i++)
   {
     if (deg[i]==0)
      {
       c1 = i;
       return true;
      }
   }
  
  aassert(0);
  return false;
 }


//------------------------------------------------------------------

/*
 * Bitwise Copy of a Tree.
 */
Tree& Tree::operator=(const Tree &T)
 {
  _order = T.order();

  adjLists = T.adjLists;

  return *this;
 }

/*
 * I/O stuff (Adjency list format).
 */
ostream& operator<<( ostream& o, const Tree& T )
 {
//   o << nl << "Tree (" << T.order() << ")\n";
//   o << "------------------\n";

   for ( int i=0; i<T.order(); i++ )
   {
      o << i << " : " << T.adjLists[i] << nl;
   }

   return o;
 }

//----------------------------------------------------------------------

  /*
   * Find path between two vertices.
   */
void Tree::getPath( vertNum u, vertNum v, Path &p) const
 {
  assert( u < _order && v < _order );

  p.resize(_order);

  VertArray n(_order);		// used/seen neighbors

  p[0]=u;
  n[0]=0;
  //
  vertNum len = 0;

  while ( p[len] != v )
   {
//cerr << "len = " << len << nl;
//cerr << "p: " << p << nl;
//cerr << "n: " << n << nl;

    if ( n[len] < adjLists[ p[len] ].size() )
     {
       p[len+1] = adjLists[ p[len] ][ n[len] ];

       n[len]++;

       if ( len > 0 &&  p[len+1] == p[len-1] ) continue; 
       else				       n[++len] = 0;
     }
    else 
     {
       len--;
     }
   }
  
  return;
 }

//-----------------------------------------------------------------------

#if 0
// hack job here...
//
Graph::Graph(const Tree &T) 
 {
   vertNum *Tp = (vertNum*) &T;
   this = new Graph( *((AdjLists*) (Tp++)) );
 }
#else
Graph::Graph(const Tree &T) 
 {
  _order = _space = T.order();

  if (_order == 0) return;

  _adj = _allocate( _order );

  int i; for (i=0; i<_order; i++)
    for (int j=0; j<_order; j++) _adj[i][j] = false;

  for (i=0; i<_order; i++)
   {
    for (int j=0; j<T.degree(i); j++)
     {
       _adj[i][T.neighbor(i,j)] = true;
       _adj[T.neighbor(i,j)][i] = true; // just to make sure undirected
     }
   }
 }
#endif
