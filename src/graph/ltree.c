
/***********************************************************************
 * Source for Labeled Trees.
 **********************************************************************/

/*tex

\file{ltree.c}
\path{src/graph}
\title{Source for Labeled Trees}
\classes{LTree}
\makeheader

xet*/


#include "graph/ltree.h"
#include "graph/forest.h"
#include "graph/lgraph.h"
#include "general/bitvect.h"
#include "general/queue.h"
#include "array/dyarray.h"

const Label LTree::NoLabel = (Label) 0xFF;

/*
 * Binary tree type constructors.
 */

LTree::LTree(const LTree &T1, const LTree &T2, const Label &rootLabel) :
   Tree(T1._order + T2._order + 1), labels(T1._order + T2._order + 1)
 {
  _order = T1._order + T2._order + 1;

  adjLists.resize(T1._order + T2._order + 1);

  adjLists[0].resize(2);
  adjLists[0][0]=1;
  adjLists[0][1]=T1._order;
  labels[0]=rootLabel;

  /*
   * Merge first tree.
   */
  adjLists[1].resize(T1.adjLists[0].size()+1);
  adjLists[1][0]=0;
  labels[1]=T1.labels[0];
  // 
  int adjLen=T1.adjLists[0].size();
  int j; for (j=0; j<adjLen; j++) adjLists[1][j]=T1.adjLists[0][j]+1;

  int i; for (i=1; i<T1._order; i++)
   {
     adjLen=T1.adjLists[i].size();

     adjLists[i+1].resize(adjLen);

     for (int j=0; j<adjLen; j++)
       adjLists[i+1][j]=T1.adjLists[i][j]+1;

     labels[i+1]=T1.labels[i];
   }

  /*
   * Merge second tree.
   */
  adjLists[T1._order+1].resize(T2.adjLists[0].size()+1);
  adjLists[T1._order+1][0]=0;
  labels[T1._order+1]=T2.labels[0];
  // 
  adjLen=T2.adjLists[0].size();
  for (j=0; j<adjLen; j++) 
    adjLists[T1._order+1][j]=T2.adjLists[0][j]+T1._order+1;

  for (i=1; i<T2._order; i++)
   {
     adjLen=T2.adjLists[i].size();

     adjLists[i+T1._order+1].resize(adjLen);

     for (int j=0; j<adjLen; j++)
       adjLists[i+T1._order+1][j]=T2.adjLists[i][j]+T1._order+1;

     labels[i+T1._order+1]=T2.labels[i];
   }

 }
//
LTree::LTree(const LTree &T1, const LTree &T2, vertNum u, vertNum v ) :
   Tree(T1._order + T2._order), labels(T1._order + T2._order)
 {
  assert( u < T1._order && v < T2._order );

  _order = T1._order + T2._order;

  adjLists.resize(T1._order + T2._order);

  int adjLen;

  /*
   * Merge first tree.
   */
  int i; for (i=0; i<T1._order; i++)
   {
     adjLen=T1.adjLists[i].size();

//cerr << "first tree " << i << " adjLen " << adjLen << nl;

     adjLists[i].resize( i==u ? adjLen+1 : adjLen );

     for (int j=0; j<adjLen; j++)
       adjLists[i][j]=T1.adjLists[i][j];

     if (i==u) adjLists[i][adjLen] = T1._order + v;

     labels[i]=T1.labels[i];
   }

  /*
   * Merge second tree.
   */
  for (i=0; i<T2._order; i++)
   {
     adjLen=T2.adjLists[i].size();

//cerr << "second tree " << i << " adjLen " << adjLen << nl;

     adjLists[i+T1._order].resize( i==v ? adjLen+1 : adjLen );

     for (int j=0; j<adjLen; j++)
       adjLists[i+T1._order][j]=T2.adjLists[i][j]+T1._order;

     if (i==v) adjLists[i+T1._order][adjLen] = u;

     labels[i+T1._order]=T2.labels[i];
   }

//cerr << "ltree created \n" << *this; 
 }

//-------------------------------------------------------------------------
//

/*
 * Tree from bounded pathwidth graph graph constructor.
 *
 * ( See Tree(const Graph &G, vertNum v) )
 */
LTree::LTree(const LGraph &G, vertNum v) : 
      Tree(G.order()), labels(1, G.order()-1)
 {
   vertNum maxOrder = G.order();

   assert( 0<=v && v<maxOrder );

   BitVector mark(maxOrder);
   //
   mark.clearAll();

   _InternalNode node, nextNode;
   Queue<_InternalNode> span;
   
   node.graph = v;
   node.tree = 0;
   labels[0] = G.getLabel(v);
   //mark.setBit(v);
   _order = 1;

   span.enque(node);
         
   /*
    * Start a breadth-first search spanning tree from vertex v.
    */

   while( span.deque(node) )
    {
     if ( !mark.getBit(node.graph) )
      {
        mark.setBit(node.graph);

//cerr << "adding graph vertex " << node.graph << " (" << 
//           node.tree << ")" << nl;
           
        /*
         * See if neighbors have been added to tree.
         */
        for (int i=0; i<maxOrder; i++)
         {
          if (i==node.graph) continue;
          if ( G.isEdge(i,node.graph)==true && mark.getBit(i)==false )
           {
            
            nextNode.graph = i;
            nextNode.tree = addLeaf(node.tree, G.getLabel(i)); 

            span.enque(nextNode);
           }
         }
      } // if !marked
    } // while queue

 }


//---------------------------------------------------------------------

/*
 * Operations on vertices (nodes)
 */
vertNum LTree::addLeaf(vertNum v, const Label &l)
 {
#if 0
   Tree::addLeaf(v);
   labels.append( l );
   return _order;
#else
   assert(v>=0 && v<_order);

   VertSet adj(1);
   adj[0]=v;

   adjLists.append( adj );              // Which statement should be first?
   adjLists[v].append( _order );
   labels.append( l );

   return _order++;
#endif
 }

void LTree::rmLeaf(vertNum v)
 {
   assert( isLeaf(v) );

   register int u = adjLists[v][0];
   int uv = adjLists[u].index(v);

//cerr << "rmLeaf(" << v << ") neighbor " << u << " at index " << uv << nl;

   if ( uv < adjLists[u].size()-1 )
             adjLists[u].swap( uv, adjLists[u].size()-1 );
   //
   adjLists[u].shrink(1);

//cerr << " u shrunk: " << adjLists[u] << nl;

   if ( v < adjLists.size()-1 )
    {
     /*
      * Relabel last node as new v node.
      */
     for (int i = 0; i<adjLists[_order-1].size(); i++)
      {
       vertNum neighbor = adjLists[_order-1][i];

       if ( neighbor == v ) continue;

       for (int j=0; j<adjLists[neighbor].size(); j++)
         if ( adjLists[neighbor][j] == _order-1 )
           {
              adjLists[neighbor][j] = v;
              break;
           }
      }

//cerr << "unswapped " << adjLists << nl;

     adjLists.swap( v, _order-1 );
     //
     labels.swap( v, _order-1 );
    }
   
//cerr << "swapped " << adjLists << nl;

   adjLists.shrink(1);
   //
   labels.shrink(1);

   _order--;
 }


/*
 * Return the pieces of a labeled tree after a vertex is removed.
 */
LForest* LTree::rmNode(vertNum v) const
 {
   assert(v>=0 && v<_order);

//cerr << "removing vertex " << v << " in LTree:\n" << *this << nl;
//cerr << "will produce " << adjLists[v].size() << " subtrees\n";

   VertArray map(_order);		// Index for subtrees.

   map.fill(_order);
   map[v]=0;				// A big fake.

   LForest *F = new LForest();

   /*
    * Forest will contain this many trees.
    */
   for (int i=0; i<adjLists[v].size(); i++)
    {

//cerr << "doing subtree #" << i << nl;

     /*
      * We do a breath first search away for each of the neighbors.
      */
     vertNum neighbor = adjLists[v][i];

     Queue<vertNum> toDo;
     //
     toDo.enque(neighbor);		// Starting place is now set.

     LTree *T = new LTree(labels[neighbor]); 	
     //
     map[neighbor]=0;

     vertNum span=0;
     //
     while (toDo.deque(span))
      {

//cerr << "span = " << span << nl;

        for (int j=0; j<adjLists[span].size(); j++)
         {
          neighbor = adjLists[span][j];

//cerr << "neighbor = " << neighbor << nl;

          if ( map[neighbor] == _order ) 
           {
             toDo.enque(neighbor);
           }
          else // parent
          if ( neighbor != v ) 
           {
             map[span] = T->addLeaf(map[neighbor],labels[span]);
           }

         } // next j

        assert( map[span] != _order );
 
      } // while queue

//cerr << "adding subtree: \n" << *T << nl;

     F->add(T);	// pointer

    } // next i (v neighbor).

   return F;
 }

//------------------------------------------------------------------

/*
 * Operations on edges.		( new vertex index is returned in u )
 */
void LTree::contract(vertNum &u, vertNum v, const Label &l)
 {
  assert( isEdge( u, v ) );

  if (isLeaf(v))
   { 
    if ( u == _order-1 ) u = v;

    rmLeaf(v); 

    labels[u]=l;
    return;
   }

  if (isLeaf(u)) 
   { 
    if ( v == _order-1 ) 
     {
      rmLeaf(u);        // rmLeaf alters _order and adjList so u==v.
     }
    else
     {
      rmLeaf(u);
      u = v;
     }

    labels[u]=l;
    return;
   }

  
  /*
   * Make everything adjacent to v now adjacent to u.
   */
  int i; for (i=0; i < _order; i++)
   {
    if (i==u || i==v) continue;

    for (int j=0; j < adjLists[i].size(); j++)
      if ( adjLists[i][j]==v ) adjLists[i][j]=u;
   }

  /*
   * Now expand u's adjacency list.
   */
  int uIdx = adjLists[u].size();

  adjLists[u].extend(adjLists[v].size()-2);

  for (i=0; i<adjLists[v].size()-2; i++)
   {
    vertNum neighbor = adjLists[v][i];

    if (neighbor != u) adjLists[u][uIdx++]=neighbor;
   }

  vertNum neighbor1 = adjLists[v][i];
  vertNum neighbor2 = adjLists[v][i+1];
  //
  if (neighbor1 == u) 
   {
    adjLists[u][adjLists[u].index(v)]=neighbor2;
   }
  else if (neighbor2 == u)
   {
    adjLists[u][adjLists[u].index(v)]=neighbor1;
   }
  else
   {
    adjLists[u][adjLists[u].index(v)]=neighbor1;
    adjLists[u][uIdx++]=neighbor2;
   }

  labels[u]=l;
 
  if ( v < adjLists.size()-1 ) 
    {
     /*
      * Relabel references to last node as new v node.
      */
     for (int i = 0; i<adjLists[_order-1].size(); i++)
      {
       vertNum neighbor = adjLists[_order-1][i];

       if ( neighbor == v ) neighbor = u;

       for (int j=0; j<adjLists[neighbor].size(); j++)
         if ( adjLists[neighbor][j] == _order-1 )
           {
              adjLists[neighbor][j] = v;
              break;
           }
      }

     adjLists.swap( v, _order-1 );
     //
     labels.swap( v, _order-1 );

     if ( u == _order-1 ) u = v;
    }
   
   adjLists.shrink(1);
   //
   labels.shrink(1);

   _order--;
 }

vertNum LTree::subdivide(vertNum u, vertNum v, const Label &l)
 {
   assert( isEdge( u, v ) );

   VertSet adj(2);
   adj[0]=u;
   adj[1]=v;

   adjLists.append( adj );		// Again, do this first?
   labels.append( l );

   int i; for (i=0; i<adjLists[u].size(); i++)
    if ( adjLists[u][i] == v )
      {
        adjLists[u][i] = _order;
        break;
      }

   for (i=0; i<adjLists[v].size(); i++)
    if ( adjLists[v][i] == u )
      {
        adjLists[v][i] = _order;
        break;
      }

   return _order++;
 }


//----------------------------------------------------------------------

/*
 * Bitwise Copy of a Tree.
 */
LTree& LTree::operator=(const LTree &T)
 {
  _order = T.order();

  adjLists = T.adjLists;
  labels = T.labels;

  return *this;
 }

/*
 * I/O stuff (Adjency list format).
 */
ostream& operator<<( ostream& o, const LTree& T )
 {
   //o << nl << "Labeled Tree (" << T.order() << ")\n";
   //o << "---------------------------\n";

   for ( int i=0; i<T.order(); i++ )
   {
      o << i << " ("<< T.labels[i] << ") : " << T.adjLists[i] << nl;
   }

   return o;
 }


