
/***************************************************************************
 * Graph methods with BitVector use.
 ***************************************************************************/

/*tex

\file{graph_bv.c}
\path{src}
\title{Standard graph class implementation cont.}
\classes{}
\makeheader

xet*/

#include <iostream>

#include "graph/graph.h"
#include "general/bitvect.h"

/*****************************************************************************/

/*
 * Return neighborhood of a vertex.
 */
BitVector Graph::neighbors(vertNum vert) const
 {
  BitVector b_vec(_order);

  b_vec.clr_all();

  for (int i=0; i<_order; i++)
  {
   if (i==vert) continue;
   if (isEdge(vert,i)) b_vec.set_bit(i);
  }

  return b_vec;
 }

#if 1
//
// We now use arrays.
//
/*
 * Return the number of components of a graph.
 */
vertNum Graph::components() const
 {
  if (_order==1) return 1;

  BitVector span(_order), used(_order), to_try(_order), all(_order);

  int i, first=0;
  vertNum comp=1;
  span.clr_all();
  used.clr_all();
  to_try.clr_all();
  all.set_all();

//this->print();

 while(1)
 {
  used.set_bit(first);
  span.set_bit(first);
  span = span | this->neighbors(first);

#if 0
printf("\n 1) span "); span.print(stdout);
printf("\n 1) used "); used.print(stdout);
printf("\n 1) to_try "); to_try.print(stdout);
printf("\n 1) all "); all.print(stdout);
#endif

  /*
   * Span out from first vertex.
   */
  while (1)
   {
    to_try = span - used;

#if 0
printf("\n 2) span "); span.print(stdout);
printf("\n 2) used "); used.print(stdout);
printf("\n 2) to_try "); to_try.print(stdout);
#endif

    if (!to_try) break;

    for (i = first+1; i<_order; i++)
      if (to_try.get_bit(i))
        {
         used.set_bit(i);
         span = span | this->neighbors(i);

#if 0
printf("\n 3) span "); span.print(stdout);
printf("\n 3) used "); used.print(stdout);
printf("\n 3) to_try "); to_try.print(stdout);
#endif

        }
   }

  /*
   * Check for other components.
   */
  if (span == all) return comp;
  else comp++;

  for (i = first+1; used.get_bit(i); i++);

  first=i;

 } // while

#ifndef __DECCXX
 return comp;
#endif

}

#endif
