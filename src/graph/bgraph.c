
/***********************************************************************
 * Source for boundary graph class.
 **********************************************************************/
 
/*tex
 
\file{bgraph.c}
\path{src/graph}
\title{Boundary graph class.}
\classes{BGraph}
\makeheader
 
xet*/


#include "graph/bgraph.h"
#include "graph/lgraph.h"
#include "bpg/bpg.h"

BGraph::BGraph(const BGraph &B, vertNum extra) : 
        Graph( CAST(B,Graph), extra),
      	_bndry(B._bndry),
	_bFlag(B._bFlag)
{
 assert(extra >=0 );

 _bFlag.extend(extra,false);
}

BGraph::BGraph(vertNum n /* = 0 */, vertNum b /* = 0 */) 
       : Graph(n), _bndry(b), _bFlag(n)
   {
    assert(b<=n);

    _bFlag.fill(false);

    for (int i=0; i<b; i++)
     {
      _bndry[i] = i;
      _bFlag[i] = true;
     }
   }


BGraph::BGraph(const BPG &B, vertNum extra) :
        Graph(B.order()+extra),
        _bndry(B.boundarySize()), 
        _bFlag(B.order()+extra)
 {
//cerr << "BGraph(BPG) of " << B << " boundarySize=" << B.boundarySize() 
//     << " order=" << B.order() << " extra=" << extra << nl;

      assert(extra >= 0);

      _bFlag.fill(false);

      vertNum nextVert = 0;

      vertNum bSize = B.boundarySize();
      vertNum bLen = B.length();

//cerr << "bSize=" << bSize << " bLen=" << bLen << nl;

      /*
       * Keep a reference to each boundary vertex.
       */
      VertArray bndry(bSize);
      //
      int i; for (i=0; i<bLen; i++)
       {
         Operator op = B[i];

         if (op.isVertexOp())
          {
           vertNum vert;
           vert = op.vertex();
           
           bndry[vert] = nextVert++;
          }
         else if (op.isEdgeOp())
          {
           vertNum vert1 = op.vertex1();
           vertNum vert2 = op.vertex2();

           Graph::addEdge( bndry[vert1], bndry[vert2] );
          }
       } // end non-boundary build

      /*
       * Now set boundary in our BGraph.
       */
      vertNum nextBndry = 0;
      //
      for (i=0, nextVert=0; i<bLen; i++)
       {
         if (B[i].isVertexOp())
          {
           if (B.isBoundaryVertex(i)) 
            {
              _bFlag[nextVert] = true;
//              _bndry[nextBndry++] = B[i].vertex();
              _bndry[B[i].vertex()] = nextVert; nextBndry++;
            }
           nextVert++;
          }
       }

      assert(nextBndry == bSize);
 }

/*
 * Set/Query about boundary size.
 */
void BGraph::vertOp( vertNum bdy ) 		// Isolated vertex pulloff.
 {
  _bFlag[_bndry.get(bdy)] = false;
  _bFlag.append(true);

  _bndry[bdy] = Graph::addNode();
 }

void BGraph::vertEdgeOp( vertNum bdy )	 	// Vertex pulloff with edge.
 {
  vertNum v = Graph::addNode();

  Graph::addEdge(_bndry.get(bdy), v);

  _bFlag[_bndry.get(bdy)] = false;
  _bFlag.append(true);

  _bndry[bdy] = v;
 }

void BGraph::edgeOP( vertNum b1, vertNum b2 )
 {
  Graph::addEdge(_bndry.get(b1), _bndry.get(b2));
 }

void BGraph::concat(const BGraph &B)
{
 //cerr << "concat (1): " << *this << "with (2): " << B;
 //cerr << _bndry << _bFlag << nl;

 vertNum bs = boundarySize();
 assert(B.boundarySize() == bs);
 vertNum es = B.order()-boundarySize();

 vertNum oldOrder = order();

 VertArray map(B.order());
 
 //cerr << "Boundaries: " << _bndry << B._bndry << nl;

 //_bFlag.extend(es,false);
 // 
 if (es>0) addNode(es);

 //cerr << "after addNode(" << es << ") " << *this;
 //cerr << "Boundaries: " << _bndry << B._bndry << nl;

 vertNum Bcnt=0;

 int i; for (i=0; i<B.order(); i++)
  {
   if (B.isBoundary(i))
    {
     Bcnt++;
    }
   else
    {
     map[i] = oldOrder+i-Bcnt;
    }
  }

 assert(Bcnt==bs);

 //cerr << "Boundaries: " << _bndry << B._bndry << nl;
 for (i=0; i<bs; i++) map.put(B.boundary(i), boundary(i));

 //cerr << "map = " << map << nl;

 for (i=0; i<B.order()-1; i++)
   for (int j=i+1; j<B.order(); j++)
    {
     if (B.isEdge(i,j)) addEdge(map[i],map[j]); 
    }

// cerr << " = " << *this;
}

void BGraph::getGluedGraph(const BGraph &B1, const BGraph &B2, Graph &G)
{
 //cerr << "getGluedGraph (1): " << B1 << "with (2): " << B2;

 vertNum bs = B1.boundarySize();
 assert(B2.boundarySize() == bs);

 vertNum es = B2.order()-bs;

 vertNum order1 = B1.order(),
         order2 = B2.order();

 // Set up the correct order for G.
 //
 G.newGraph(order1 + es);

 // Move the BGraph B1 into the upper-left part of G's adjacency matrix.
 //
 int i; for (i=0; i<order1-1; i++)
  for (int j=i+1; j<order1; j++) 
   {
    if (B1.isEdge(i,j)) G.addEdge(i,j);
   }

 VertArray map(order2);
 
 vertNum Bcnt=0;

 for (i=0; i<order2; i++)
  {
   if (B2.isBoundary(i))
    {
     Bcnt++;
    }
   else
    {
     map[i] = order1+i-Bcnt;
    }
  }

 assert(Bcnt==bs);

 for (i=0; i<bs; i++) map.put(B2.boundary(i), B1.boundary(i));

 for (i=0; i<order2-1; i++)
   for (int j=i+1; j<order2; j++)
    {
     if (B2.isEdge(i,j)) G.addEdge(map[i],map[j]); 
    }

// cerr << " = " << *this;
}


/*
 * Isomorphism check.
 */
bool BGraph::operator==(const BGraph &B) const { (void) B; return false; }

BGraph::BGraph(const LGraph &LG, vertNum bndySz) :
        Graph(LG),
        _bndry(bndySz), 
        _bFlag(LG.order())
 {
//cerr << "BGraph(LGraph) of " << LG << " boundarySize=" << bndySz <<nl;

      assert(bndySz <= order());

      _bFlag.fill(false);

      vertNum cnt = 0;
      //
      for (int i=0; i<order(); i++)
       {
         Label vLabel = LG.getLabel(i);

         if (vLabel < bndySz)
          {
           assert(_bFlag[i] == false);
           _bFlag[i] = true;
           _bndry[vLabel] = i;
           cnt++;
          } 
       }
      //
      assert(cnt == bndySz);
 }


bool BGraph::isBiconnected( const BGraph& Gin )
{
   BGraph G(Gin);

   int bs = G.boundarySize();

   // make boundary biconnected
   //
   G.addEdge(G.boundary(0),G.boundary(bs-1));
   //
   int i; for (i=0; i<bs-1; i++)
   {
     G.addEdge(G.boundary(i),G.boundary(i+1)); 
   }

   for (i=0; i<G.order(); i++)
   {
     Graph graph(CAST(G,Graph));
     graph.rmNode(i);

     if ( G.connected() == false ) return false;
   }

   return true;
}
