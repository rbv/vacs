/***************************************************************************
 * Induced Subgraph functions.
 ***************************************************************************/

/*tex

\file{graph.c}
\path{src}
\title{Induced subgraph class implementation}
\classes{InducedGraph}
\makeheader

xet*/

#include "graph/indgrph.h"
//#include "general/queue.h"

/*****************************************************************************/

InducedGraph::InducedGraph(const Graph &G_param) : index( G_param.order() )
    {
     _order = G_param.order();

     G = &G_param;      		// Reference.

     for (int i=0; i<_order; i++) index[i]=i;
    }

InducedGraph::InducedGraph(const Graph &G_param, 
                           const VertSet &vertList) : index(vertList.size())
    {
     _order = vertList.size();

     index = vertList;                  // Copy!
     G = &G_param;      		// Reference.

     assert( _order > 0 );
     //
     for (int i=0; i<_order; i++)
       assert(index[i] >= 0 && index[i] < G_param.order());
    }

InducedGraph::InducedGraph(const Graph &G_orig, 
                           const VertArray &vertList, 
                           vertNum count): index(count)
    {
     _order = count;

     G = &G_orig;      		// Reference.

     for (int i=0; i< _order; i++)
      {
       assert( vertList[i] >= 0 && vertList[i] < G_orig.order() );
       index[i] = vertList[i];
      }

    }

InducedGraph::InducedGraph(const InducedGraph &S_param, 
                           const VertSet &vertList) : index(vertList.size())
    {
     _order = vertList.size();

     for (int i=0; i< _order; i++)
      {
       assert(vertList[i] >= 0 && vertList[i] < (S_param.index).size());
       index[i] = S_param.index[vertList[i]];
      }

     G = S_param.G;
    }

InducedGraph::InducedGraph(const InducedGraph &S_param, 
			   const VertArray &vertList, 
                           vertNum count): index(count)
    {
     _order = count;

     for (int i=0; i< _order; i++)
      {
       assert(vertList[i] >= 0 && vertList[i] < (S_param.index).size());
       index[i] = S_param.index[vertList[i]];
      }

     G = S_param.G;
    }

/*
 * Be cautious with these next methods!
 */
void InducedGraph::setIndex( const VertSet &vertList )
    {
     _order = vertList.size();

     index = vertList;                  // Copy!
    }

void InducedGraph::setIndex( const VertArray &vertList, vertNum count )
    {
     _order = count;

     for (int i=0; i< _order; i++)
      {
       index[i] = vertList[i];
      }

    }

/*****************************************************************************/

/*
 * I/O stuff (matrix text format).
 */
ostream& operator<<( ostream& o, const InducedGraph& M )
 {
   o << nl << "Graph (" << M.order() << ", " << M.size() << ")\n";
   o << "--------------------\n";

   for ( int i=0; i<M.order(); i++ )
   {
      o << "  ";
      for ( int j=0; j<M.order(); j++ )
      {
         o << (int) M.isEdge(i,j) << ' ';
      }
      o << nl;
   }

   return o;
 }

/*****************************************************************************/

/*
 * Graph size.
 */
edgeNum InducedGraph::size() const
 {
  vertNum sz = 0;
  for (int i=0; i<_order; i++)
   for (int j= i+1; j<_order; j++) if ( isEdge(i,j) ) sz++;
  return sz;
 }

/*****************************************************************************/
/*
 * Find sorted -- 1 or nonsorted -- 0 degree sequence.
 */
void InducedGraph::degVec(DegreeSequence &degvec, int flag) const
{ 
 
  for (int i=0; i<_order; i++)
   {
    int deg=0;
    for (int j=0; j<_order; j++) if ( isEdge(i,j) ) deg++;

    degvec[i]=deg;
   }

  if (flag) degvec.sortDown();

  return;
 }

//------------------------------------------------------------------------

/*
 * Construct graph given an induced graph.
 */
Graph::Graph(const InducedGraph &G)
 {
  vertNum n = G.order();

  _order = _space =  n;

  if (_order == 0) return;

  _adj = _allocate( _order );

  int i; for (i=0; i<n-1; i++)
   {
    for (int j=i+1; j<n; j++) 
     {
      _adj[i][j] = G.isEdge(i,j);
      _adj[j][i] = _adj[i][j];
     }
   }

  for (i=0; i<n; i++) _adj[i][i] = false;
 }

