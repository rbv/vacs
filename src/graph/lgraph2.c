
/***************************************************************************
 * Vertex labeled graph class with BPGs
 ***************************************************************************/

/*tex

\file{lgraph2.c}
\path{src/graph}
\title{Standard vertex labeled graph class implementation.}
\classes{LGraph}
\makeheader

xet*/

#include <iostream>

#include "graph/lgraph.h"
#include "bpg/bpg.h"
#include "graph/ltree.h"
#include "graph/forest.h"

/**************************************************************************/

/*
 * Construct and label graph via a boundaried graph.
 */
LGraph::LGraph(const BPG &bpg ) : Graph(bpg.order()), labels(bpg.order())
{
      /*
       * Build Graph
       */ 
      vertNum bSize = bpg.boundarySize();
      vertNum bLen = bpg.length();

      /*
       * Keep a pointer to each boundary vertex.
       */
      Array<vertNum> bndry(bSize);
      //
      vertNum newVertex=0;

      /* 
       * Mark boundary vertices with their label+1;.
       * (Other vertices are labeled zero).
       */
      labels.fill(0);

      /*
       * Make graph by looking at each of the operators.
       */
      for (int i=0; i<bLen; i++)
       {
         Operator op = bpg[i];

         if (op.isVertexOp())
          {
           vertNum vert;
           vert = op.vertex();
           
           if ( bpg.isBoundaryVertex(i) ) labels[newVertex]=vert+1;
           else                           labels[newVertex]=0;

           bndry[vert] = ++newVertex;
          }

         if (op.isEdgeOp())
          {
           vertNum vert1 = op.vertex1();
           vertNum vert2 = op.vertex2();

           addEdge(bndry[vert1], bndry[vert2]);
          }
       } // for OPs

}

/*
 * Construct and label graph via a labeled forest.
 */
LGraph::LGraph(const LForest &LF ) : Graph(0), labels(0)  // set below
{
  vertNum order = 0;

  LTree *ltree;

  // Find order of forest.
  //
  LF.start();
  //
  while ( ltree = LF.next() )
   {
    order += ltree->order();
   }
  //
  LF.stop();

  // Allocate space for expected order of labeled graph.
  //
  newGraph(order);
  //
  labels.resize(order);

  vertNum offset = 0;

  // Add edges to labeled graph.
  //
  LF.start();
  //
  while ( ltree = LF.next() )
   {
    for (int i=0; i<ltree->order(); i++)
     {
       setLabel( i+offset, ltree->getLabel(i) );

       for (int j=0; j < ltree->degree(i); j++)
        {
          addEdge(i+offset, ltree->neighbor(i,j)+offset);
        }
     }

    offset += ltree->order();
   }
  //
  LF.stop();

}

