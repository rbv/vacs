
/***************************************************************************
 * LEDA Graph constructor/functions.
 ***************************************************************************/

/*tex

\file{ledagraph.c}
\path{src/graph}
\title{Standard LEDA graph class implementation}
\classes{LedaGraph}
\makeheader

xet*/

#include "graph/ledagraph.h"

#include "array/array.h"
#include "graph/stdgraph.h"
#include "graph/graph.h"
#include "graph/indgrph.h"

#ifdef DOLEDA

/*
 * Construct LedaGraph initialized via local Graph.
 */
LedaGraph::LedaGraph(const Graph &G)
 {
  vertNum n = G.order();

  Array<node> nodes(n);

  for (int k=0; k < n; k++) nodes[k] = new_node();

  for (int i=0; i < n /* -1 */; i++)
    //for (int j=i+1; j < n; j++)  -- graph
    for (int j=0; j < n; j++)	// -- digraph
     { 
      if (i==j) continue;
      if ( G.isEdge(i,j) ) (void) new_edge( nodes[i], nodes[j] );
     }

  return;
 }

/*
 * Construct LedaGraph initialized via local InducedGraph.
 */
LedaGraph::LedaGraph(const InducedGraph &G)
 {
  vertNum n = G.order();

  Array<node> nodes(n);

  for (int k=0; k < n; k++) nodes[k] = new_node();

  for (int i=0; i < n /* -1 */; i++)
    //for (int j=i+1; j < n; j++)  -- graph
    for (int j=0; j < n; j++)	// -- digraph
     { 
      if (i==j) continue;
      if ( G.isEdge(i,j) ) (void) new_edge( nodes[i], nodes[j] );
     }

  return;
 }
#else
/*
 * Construct fake LedaGraph initialized via local Graph.
 */
LedaGraph::LedaGraph(const Graph &G) : Graph(G)
 {
 }

/*
 * Construct fake LedaGraph initialized via local InducedGraph.
 */
LedaGraph::LedaGraph(const InducedGraph &G) : Graph(G)
 {
 }

#endif
