
/***********************************************************************
 * Source for various node searching on Trees.
 **********************************************************************/

/*tex

\file{tree.c}
\path{src/graph}
\title{Source for Tree traversals}
\classes{Tree}
\makeheader

xet*/


#include "graph/tree.h"
#include "general/queue.h"
#include "general/stack.h"
#include "general/bitvect.h"

// hack from graph spaning tree constructor
//
void Tree::BFS( VertArray &seq, vertNum root) const
 {
   vertNum maxOrder = order();

   seq.resize(maxOrder);

   BitVector mark(maxOrder), spanned(maxOrder);
   //
   mark.clearAll();
   spanned.clearAll();

   vertNum node, nextNode;
   //
   node = root;
   mark.setBit(root);
   
   int seqIndex = 0;

   Queue<vertNum> span;
   //
   span.enque(node);
         
   /*
    * Start a breadth-first search spanning tree from vertex v.
    */

   while( span.deque(node) )
    {
     seq[seqIndex++] = node;

     //if ( spanned.getBit(node.graph) == false )
     assert( spanned.getBit(node) == false );
      {
        spanned.setBit(node);

        /*
         * See if neighbors have been added to tree.
         */
        for (int i=0; i<degree(node); i++)
         {
          if ( mark.getBit(adjLists[node][i])==false )
           {
            nextNode = adjLists[node][i];
            mark.setBit(nextNode);

            span.enque(nextNode);
           }
         }
      } // if !marked  -- (asserted)

    } // while queue

  assert(seqIndex==maxOrder);
 }

//---------------------------------------------------------------------

// hack from above BFS but with 'stack' instead of 'queue'
//
void Tree::DFS( VertArray &seq, vertNum root) const
 {
   vertNum maxOrder = order();

   seq.resize(maxOrder);

   BitVector mark(maxOrder), spanned(maxOrder);
   //
   mark.clearAll();
   spanned.clearAll();

   vertNum node, nextNode;
   //
   node = root;
   mark.setBit(root);
   
   int seqIndex = 0;

   Stack<vertNum> span;
   //
   span.push(node);
         
   /*
    * Start a depth-first search spanning tree from vertex v.
    */

   while( span.pop(node) )
    {
     seq[seqIndex++] = node;

     //if ( spanned.getBit(node.graph) == false )
     assert( spanned.getBit(node) == false );
      {
        spanned.setBit(node);

        /*
         * See if neighbors have been added to tree.
         */
        for (int i=0; i<degree(node); i++)
         {
          if ( mark.getBit(adjLists[node][i])==false )
           {
            nextNode = adjLists[node][i];
            mark.setBit(nextNode);

            span.push(nextNode);
           }
         }
      } // if !marked  -- (asserted)

    } // while stack

  assert(seqIndex==maxOrder);
 }

//---------------------------------------------------------------------

// hack from BFS method
//
void Tree::getParents( VertArray &seq, vertNum root ) const
 {
   vertNum maxOrder = order();

   seq.resize(maxOrder);

   BitVector mark(maxOrder), spanned(maxOrder);
   //
   mark.clearAll();
   spanned.clearAll();

   vertNum node, nextNode;
   //
   node = root;
   mark.setBit(root);
   
   Queue<vertNum> span;
   //
   span.enque(node);

   seq[root] = root; // special case.

   /*
    * Start a breadth-first search spanning tree from vertex v.
    */

   while( span.deque(node) )
    {
     //if ( spanned.getBit(node.graph) == false )
     assert( spanned.getBit(node) == false );
      {
        spanned.setBit(node);

        /*
         * See if neighbors have been added to tree.
         */
        for (int i=0; i<degree(node); i++)
         {
          if ( mark.getBit(adjLists[node][i])==false )
           {
            nextNode = adjLists[node][i];
            mark.setBit(nextNode);

            span.enque(nextNode);

            seq[nextNode] = node;
           }
         }
      } // if !marked  -- (asserted)

    } // while queue

 }
