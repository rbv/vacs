/***************************************************************************
 * Induced Subgraph functions requiring BitVect.
 ***************************************************************************/

/*tex

\file{indgrph.c}
\path{src}
\title{Induced subgraph class implementation}
\classes{InducedGraph}
\makeheader

xet*/

#include "graph/indgrph.h"
#include "general/bitvect.h"
#include "general/queue.h"

/*****************************************************************************/


/*
 * Be cautious with these next methods!
 */
void InducedGraph::setIndex( const BitVector &B )
 {
  _order = 0;

  for (int i=0; i<B.length(); i++)
   {
    if (B.getBit(i)) index[_order++]=i;
   }

  // assert( _order > 0 );
 }

/*****************************************************************************/

bool InducedGraph::connected() const
 {
  BitVector span(_order);

  Queue< vertNum > search(_order);

  span.clearAll();

  /*
   * Start our search from the first vertex.
   */
  span.setBit(0);
  //
  search.enque(0);
  //
  vertNum v;

  while( search.deque(v) )
   {
    for (int j=1; j<_order; j++)
      if ( span.getBit(j)==false && isEdge(v,j)==true )
       {
         span.setBit(j);
         //
         search.enque(j);
       }
   }

  return span.full();
 }

vertNum Graph::components( PIterSet< InducedGraph > &c ) const
 {
  VertArray verts(_order);
  //
  vertNum numVerts;
  //
  vertNum numComps=0;
  
  BitVector span(_order);

  Queue< vertNum > search(_order);
  //
  vertNum v;

  span.clearAll();

  while ( !span.full() )
   {
    /*
     * Start our search from a new component;
     */
    int i; for (i=numComps++; i<_order; i++) 
      if ( span.getBit(i)==0 ) break;

    span.setBit(i);
    //
    search.enque(i);
    //
    verts[0]=i;
    //
    numVerts=1;

    while( search.deque(v) )
     {
      for (int j=0; j<_order; j++)
        if ( span.isBit(j)==false && isEdge(v,j)==true )
         {
           span.setBit(j);
	   //
           search.enque(j);
           //
           verts[numVerts++]=j;
         }
     }
     
    InducedGraph *I = new InducedGraph( *this, verts, numVerts );
    //
    c.add(I);

   } // while another component exists 

  return numComps;
 }

vertNum InducedGraph::components( PIterSet< InducedGraph > &c ) const
 {
  VertArray verts(_order);
  //
  vertNum numVerts;
  //
  vertNum numComps=0;
  
  BitVector span(_order);

  Queue< vertNum > search(_order);
  //
  vertNum v;

  span.clearAll();

  while ( !span.full() )
   {
    /*
     * Start our search from a new component;
     */
    int i; for (i=numComps++; i<_order; i++) 
      if ( span.getBit(i)==0 ) break;

    span.setBit(i);
    //
    search.enque(i);
    //
    verts[0]=i;
    //
    numVerts=1;

    while( search.deque(v) )
     {
      for (int j=0; j<_order; j++)
        if ( span.isBit(j)==false && isEdge(v,j)==true )
         {
           span.setBit(j);
	   //
           search.enque(j);
           //
           verts[numVerts++]=j;
         }
     }
     
    InducedGraph *I = new InducedGraph( *this, verts, numVerts );
    //
    c.add(I);

   } // while another component exists 

  return numComps;
 }

vertNum InducedGraph::components( ) const
 {
  vertNum numComps=0;
  
  BitVector span(_order);

  Queue< vertNum > search(_order);
  //
  vertNum v;

  span.clearAll();

  while ( !span.full() )
   {
    /*
     * Start our search from a new component;
     */
    int i; for (i=numComps++; i<_order; i++) 
      if ( span.getBit(i)==0 ) break;

    span.setBit(i);
    //
    search.enque(i);

    while( search.deque(v) )
     {
      for (int j=0; j<_order; j++)
        if ( span.isBit(j)==false && isEdge(v,j)==true )
         {
           span.setBit(j);
	   //
           search.enque(j);
         }
     }
     
   } // while another component exists 

  return numComps;
 }

/*****************************************************************************/

/*
 * Determine if a vertex is a Cut Vertex of a connected graph.
 */
bool InducedGraph::isCutVertex( vertNum v ) const
 {
  assert(isNode(v) && connected());

  InducedGraph _G(*this);

  _G.rmNode(v);

// cerr << "isCutVertex - "<< v << ": " << nl << _G ;
  
  return (_G.connected()==false); 
 }

/*
 * Warning! Only finds blocks with 4 or more vertices.
 */
static vertNum connectedGetBlocks( const InducedGraph &G, Blocks &B )
 {
  vertNum n=G.order();

  if ( n <= 3 ) return 0;		// Don't care for these!

  vertNum num = 0;
  
  for (int i=0; i<n; i++)
   {
    if ( G.isCutVertex(i) )
     {
      InducedGraph I(G);
      //
      vertNum baseNode = I.reference(i);
      //
      I.rmNode(i);

      Components comps;
      //
      I.components( comps );

      InducedGraph *igp;

      comps.start();
      //
      while ( igp = comps.next() )
       {
	 igp->addNode(baseNode);
         //
	 num += connectedGetBlocks( *igp, B );
       }
      //
      comps.stop();

      return num;

     } // cut vertex found

   } // node Loop

  /*
   * Graph must be a block.
   */
  InducedGraph *newI = new InducedGraph(G);
  //
  B.add(newI);

  return 1;
 }

vertNum InducedGraph::getBlocks( Blocks &B ) const
 {
  vertNum num = 0;

  Components comps;
  //
  components( comps );

  InducedGraph *igp;

  comps.start();
  //
  while ( igp = comps.next() )
   {
     num += connectedGetBlocks( *igp, B );
   }
  //
  comps.stop();

  return num;
 }

//------------------------------------------------------------------------

/*
 * Determine the blocks of a graph.
 */
vertNum Graph::getBlocks( Blocks &B ) const
 {
  InducedGraph I(*this);
  //
  return I.getBlocks(B);
 }

/*****************************************************************************/

