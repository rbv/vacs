
/***************************************************************************
 * Graph matrix functions.
 ***************************************************************************/

/*tex

\file{graph.c}
\path{src}
\title{Standard graph class implementation}
\classes{}
\makeheader

xet*/

#define MODULE_OK

#include <iostream>
#include "graph/graph.h"

/*
 * internal memory management routines.
 */
bool** Graph::_allocate(vertNum n)
 {
  bool **a = new bool*[n];

  /*
   * Allocate the rest in one step.
   */
  a[0] = new bool[n*n];

  for ( int i=1; i<n; i++ ) 
     a[i] = a[0] + i*n;

  return a;
 } 
//
void Graph::_deallocate(bool **a)
 {
   delete [] a[0];
   delete [] a;
 }

/**************************************************************************/

/*
 * Construct graph given adjacency matrix.
 */
Graph::Graph(vertNum n, const bool **a)
 {
   assert( n>=0 );

  _order = _space =  n;

  if (_order == 0) return;

  _adj = _allocate( _order );

  for (int i=0; i<n; i++)
   {
    for (int j=0; j<n; j++) _adj[i][j] = a[i][j];
   }

 }

/*
 * Construct graph given adjacency list.
 */
Graph::Graph( const AdjLists &L )
 {
  _order = _space = L.size();

  if (_order == 0) return;

  _adj = _allocate( _order );

  int i; for (i=0; i<_order; i++)
    for (int j=0; j<_order; j++) _adj[i][j] = false;

  for (i=0; i<_order; i++)
   {
    for (int j=0; j<L[i].size(); j++) 
     {
       _adj[i][L[i][j]] = true;
       _adj[L[i][j]][i] = true; // just to make sure undirected
     }
   }

 }

/*
 * Construct empty graph.
 */
Graph::Graph(vertNum n)
 {
   assert( n>=0 );

  _order = _space =  n;
  
  if (_order == 0) return;

  _adj = _allocate( _order );

  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++) _adj[i][j] = 0;

 }

/*
 * Construct graph initialized via another graph.
 */
Graph::Graph(const Graph &G, vertNum extra)
 {
  assert( extra >= 0);

  _order = _space =  G._order + extra;

  if (_order == 0) return;

  _adj = _allocate( _order );

  int i; for (i=0; i < G._order; i++)
   {
    for (int j=0; j < G._order; j++) 
        _adj[i][j] = G._adj[i][j];

    for (int j2 = G._order; j2 < _order; j2++) _adj[i][j2] = 0;
   }

  for (i = G._order; i < _order; i++)
   {
    for (int j2 = 0; j2 < _order; j2++) _adj[i][j2] = 0;
   }

  return;
 }

void Graph::newGraph( vertNum n )
 {
   assert( n>=0 );

  if ( _space >= n )
   {
    _order = n;

    for (int i=0; i<n; i++)
      for (int j=0; j<n; j++) _adj[i][j] = 0;
   }
  else
   {
    if (_space) _deallocate(_adj);

    _order = _space =  n;
    
    _adj = _allocate( _order );
    
    for (int i=0; i<n; i++)
      for (int j=0; j<n; j++) _adj[i][j] = 0;
   } 
 }

/************************************************************************/

/*
 * Destructor for graphs.
 */
Graph::~Graph()
  {
   if (_space) _deallocate(_adj);
  }

/*****************************************************************************/

/*
 * I/O stuff (matrix text format).
 */
ostream& operator<<( ostream& o, const Graph& M )
 {
//   o << nl << "Graph (" << M.order() << ", " << M.size() << ")\n";
//   o << "--------------------\n";
   o << M.order() << nl;

   for ( int i=0; i<M.order(); i++ )
   {
      o << ' ';
      for ( int j=0; j<M.order(); j++ )
      {
         if (i==j) { o << "0 "; continue; }
         o << (int) M.isEdge(i,j) << ' ';
      }
      o << nl;
   }

   return o;
 }

istream& operator>>( istream& in, Graph& M )
 {
   /*
    * Currently can read only adjacency matrix with order as first line.
    */
   int n;
   in >> n;

   assert( n>=0 );

   if ( n > M._space )
    {
     if ( M._space > 0 )  Graph::_deallocate( M._adj );
  
     M._order = M._space = n;
     if ( n>0 ) M._adj = Graph::_allocate( n ); 
    } 
   else 
    {
     M._order = n;
    }

   int entry;
   for (int i=0; i<n; i++)
    {
     for (int j=0; j<n; j++) 
      {
	in >> entry;

        M._adj[i][j] = entry ? 1 : 0;
      }
    }

   return in; 
 }

/*****************************************************************************/

/*
 * Add verticies to graph.
 */
void Graph::addNode(vertNum n)
 {
  assert( n>=0 );

  if ( n > _space - _order )
   {
    bool **matrix = _allocate( _order + n );

    for (int i=0; i<_order; i++)
     {
      for (int j=0; j<_order; j++) matrix[i][j] = _adj[i][j];
     }

    if ( _space ) _deallocate(_adj);

    _adj = matrix;
    _space = _order + n;
   }

  /*
   * Now initialize new nodes with no edges.
   */
  for (int i=_order; i<_order+n; i++)
    for (int j=0; j<_order+n; j++)  _adj[j][i] = _adj[i][j] = 0;

  _order += n;

  return;
 }

/*****************************************************************************/

/*
 * Remove a vertex from the graph.
 */
void Graph::rmNode(vertNum v)
 {
  assert( v>=0 && v<order() );

  _order--;

  for (int i=v; i<_order; i++)
   {
     int j; for (j=0; j<v; j++)
      {
       _adj[i][j] = _adj[i+1][j];
       _adj[j][i] = _adj[j][i+1];
      }

     for  (j=v; j<_order; j++)
      {
       _adj[i][j] = _adj[i+1][j+1];
      }
   }

  return;
 }

/*****************************************************************************/

/*
 * Graph size.
 */
edgeNum Graph::size() const
 {
  vertNum sz = 0;
  for (int i=0; i<_order; i++)
   for (int j= i+1; j<_order; j++) if (_adj[i][j]) sz++;
  return sz;
 }

/*
 * Graph size.
 */
vertNum Graph::degree(vertNum i) const
 {
  vertNum sz = 0;
  for (int j= 0; j<_order; j++) if (_adj[i][j]) sz++;
  return sz;
 }

/*
 * Contract an edge in a graph.  (not digraphs)
 */
void Graph::contractEdge(vertNum v1, vertNum v2)
 {
  assert( isEdge(v1,v2) );

  for (int i=0; i<_order; i++)
   {
     if (i==v1 || i==v2) continue;
     if (isEdge(i,v2)) addEdge(i,v1);
   }

  rmNode(v2);
  return;
 }


/*
 * Take complement of graph. 
 */
void Graph::complement()
 {
   for (int i=0; i<_order; i++)
    {
     for (int j=0; j<_order; j++)
       if (i!=j) _adj[i][j] = (_adj[i][j] ? 0 : 1);
    }

   return;
 }


/****************** DEAL WITH LATER ***************************/

#if 0
// kev - hacked this in Oct 30 1992
void Graph::rmIsoNodes()
{
   aassert(false);
}

#else
// use arrays!
/*
 * Delete isolated vertices.
 */
void Graph::rmIsoNodes()
 {
   vertNum n=0;
   VertArray inx(_order); 

  
   DegreeSequence deg(_order);
   degVec(deg);

   int i; for (i=0; i<_order; i++)          /* Shift matrix to upper left. */
     if (deg[i]) inx[n++]=i;

   if (n==0) { n=1; inx[0]=0; }

   bool **adj = _allocate(n);

   for (i=0; i<n; i++)
    {
     for (int j=0; j<n; j++)
       adj[i][j] = _adj[inx[i]][inx[j]];
    }

   _deallocate(_adj);

   _adj = adj;
   _order = n;

 }
#endif

/*****************************************************************************/

#if 0
/*
 * System QSORT comparison functions.
 */
inline int Increasing(const void* p, const void* q)
 {
   return ((vertNum *) p)[0] - ((vertNum *) q)[0];
 }
inline int Decreasing(const void* p, const void* q)
 {
   return ((vertNum *) q)[0] - ((vertNum *) p)[0];
 }
#endif

/*****************************************************************************/
/*
 * Find sorted -- 1 or nonsorted -- 0 degree sequence.
 */
void Graph::degVec(DegreeSequence &degvec, int flag) const
{ 
  for (int i=0; i<_order; i++)
   {
    degvec[i] = 0;
    for (int j=0; j<_order; j++) if (_adj[i][j]) degvec[i]++;
   }

  if (flag) degvec.sortDown();

  return;
 }


/*****************************************************************************/

#if 0
/*
 * Print out adjacency matrix.
 */
// void Graph::print() const { print(stdout); }

void Graph::print(FILE *out, vertNum option) const
  {
//   vertNum option = (vertNum) opt;
   int i, j;

   if (_order)
   {
     if (option & MATRIX)
      {
      if (option & HEADER)
       {
        fprintf(out, "       ");
        for (j = 0; j<_order; j++) fprintf(out, "%2d ", j);
        fprintf(out, "\n");
        fprintf(out, "     +-");
        for (j = 0; j<_order; j++) fprintf(out, "---", j);
        fprintf(out, "\n");
       }

       for (i = 0; i<_order; i++)
        {
         if (option & HEADER) fprintf(out, "  %2d : ",i);
  
         for (j = 0; j<_order; j++) 
#ifdef ADJ_PLUSES
          if (i!=j) fprintf(out, "%2d ", _adj[i][j]);
          else      fprintf(out, " + ", _adj[i][j]);
#else
          fprintf(out, "%2d ", _adj[i][j]);
#endif
  
         fprintf(out, "\n");
        }

       option -= MATRIX;
       if (option > 0) fprintf(out, "\n");
  
      } // MATRIX

     if (option & LIST)
      {
       for (i = 0; i<_order; i++)
        {
         if (option & HEADER) fprintf(out, "  %3d : ",i);
  
         for (j = 0; j<_order; j++) 
          if (_adj[i][j]) fprintf(out, "%3d ", j);
  
         fprintf(out, "\n");
        }

       option -= LIST;
       if (option > 0) fprintf(out, "\n");
  
      } // LIST

     if (option & GRAPH_PS)
      {
       extern print_graph_ps(FILE*, const Graph*);
       print_graph_ps(out, this);

       option -= GRAPH_PS;
       if (option > 0) fprintf(out, "\n");
  
      } // GRAPH_PS

     if (option & DIGRAPH_PS)
      {
       extern print_digraph_ps(FILE*, const Graph*);
       print_digraph_ps(out, this);

       option -= DIGRAPH_PS;
       if (option > 0) fprintf(out, "\n");
  
      } // DIGRAPH_PS

     if (option & SPREMB)
      {
       extern print_graph_xged(FILE*, const Graph*);
       print_graph_xged(out, this);

       option -= SPREMB;
       if (option > 0) fprintf(out, "\n");
  
      } // DIGRAPH_PS
   }
//   else error("Empty matrix in Graph::print(_)");
  }

/*
 * Digraph size.
 */
edgeNum Graph::arcs() const
 {
  vertNum sz = 0;
  for (int i=0; i<_order; i++)
   for (int j=0 /* i+1 */ ; j<_order; j++) if (_adj[i][j]) sz++;
  return sz;
 }

/*
 * Bitwise Copy of a graph.
 */
void Graph::operator=(Graph &G)
 {
  int n=G._order;

  if ( _order != n )
   {
     _deallocate( _adj );
     if ( n>0 ) _adj = _allocate( n );
   }

  for (i=0; i<n; i++)
   {
    for (int j=0; j<n; j++) _adj[i][j] = G._adj[i][j];
   }

  return;
 }
#endif

/*****************************************************************************
 *
 * DATA structures for membership/invarient external functions.
 *
 *****************************************************************************/

void Graph::getAdjLists( AdjLists &L, DegreeSequence &deg ) const
 {

  vertNum n = order();

  L.resize( n );
  
  deg.resize( n );
  //
  deg.fill(0);

  // Overallocate for now.
  //
  int i; for (i=0; i<n; i++) L[i].resize(n-1);
   
  // Build lists.
  //
  for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if ( isEdge(i,j) )
      {
        L[i][deg[i]++] = j;
        L[j][deg[j]++] = i;
      }

  // Adjust lists so L[*].size() == deg[*];
  //
  for (i=0; i<n; i++) L[i].resize(deg[i]);

 }

void Graph::getAdjLists( AdjLists &L ) const
 {

  vertNum n = order();

  L.resize( n );
  
  VertArray deg( n );
  //
  deg.fill(0);

  // Overallocate for now.
  //
  int i; for (i=0; i<n; i++) L[i].resize(n-1);
   
  // Build lists.
  //
  for (i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if ( isEdge(i,j) )
      {
        L[i][deg[i]++] = j;
        L[j][deg[j]++] = i;
      }

  // Adjust lists so L[*].size() == deg[*];
  //
  for (i=0; i<n; i++) L[i].resize(deg[i]);

 }


void Graph::getAdjMatrix( AdjMatrix &M, DegreeSequence &deg ) const
 {

  vertNum n = order();

  M.setDim( n );
  //
  M.fillDiag( false );

  deg.resize( n );
  //
  deg.fill(0);

  for (int i=0; i<n-1; i++)
   for (int j=i+1; j<n; j++)
    if ( isEdge(i,j) )
      {
        M.put( i, j, true ); 
        M.put( j, i, true ); 
        deg[i]++;
        deg[j]++;
      }
    else
      {
        M.put( i, j, false ); 
        M.put( j, i, false ); 
      }

 }


/*****************************************************************************/

/*
 * Is the graph connected?
 */
bool Graph::connected() const
 {
  if (_order==1) return 1;

  Array<bool> seen(_order), to_search(_order);

  seen[0]=to_search[0]=true;
  int i; for (i=1; i<_order; i++) seen[i]=to_search[i]=false;

  bool repeat_flag=true;

  while (repeat_flag)
 {
  repeat_flag=false;

   for (i=0; i<_order; i++)
   {
    if (to_search[i]==false) continue;

    for (int j=0; j<_order; j++)
     {
      if (i==j || seen[j]==true) continue;

      if (isEdge(i,j)==true && to_search[j]==false)
        {
          repeat_flag = seen[j] = to_search[j] = true;
        }
     } // j
   } // i

 }// if (repeat_flag) goto To_repeat;

  bool result=true;
  for (i=0; i<_order; i++) if (seen[i]==false) result=false;

  return result;
}

/*
 * Remove edges attached to a vertex from graph.
 */
void Graph::rmEdges(vertNum v)
{
  assert(v>=0 && v<_order);
  for (int j=0; j<_order; j++) _adj[v][j] = _adj[j][v] = 0;
}
