
/***************************************************************************
 * Adjacency lists method (to be)
 ***************************************************************************/

/*tex

\file{adjlists.c}
\path{src/graph}
\title{Standard graph adjacency lists stuff}
\classes{} 
\makeheader

xet*/

#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"

/*****************************************************************************/

//
// Should be methods of some Graph class!
//

// open neighborhood.
//
void neighborhood( const AdjLists &A, const VertSet &of, VertSet &to)
 {
   vertNum n = A.size();
   vertNum m = of.size();

   assert( n >= m && m > 0 );

   to.resize(0);

   Array<bool> span(n);
   //
   span.fill(false);

   int i; for (i=0; i<m; i++)
    {
      for (int j=0; j < A[of[i]].size(); j++)
       {
         span[A[of[i]][j]] = true;
       }
    }

   for (i=0; i<m; i++) span[of[i]] = false;

   for (i=0; i<n; i++) if ( span[i] ) to.append(i);

   return;
 }

