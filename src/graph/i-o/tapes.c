/*****************************************************************************
*
* Program Module: tapes.c
* Author: Michael J. Dinneen
*
* Version/Edit I/01
* Edit Date:  08-Jan-92 
*
* Developed for: Reading READ's isomorpic tapes.
*
******************************************************************************
*
* This file contains the common functions that operate on the adjacency
* matrix of a graph.
*
* Format of the input file is as follows.  There are three 16-bit words
* for each 10-by-10 adjacency matrix but only the upper triangular part
* is kept.
*    Word 0: xxxxx (0,1) (0,2) (0,3) (0,4) (0,5) (0,6) (0,7)
*            (0,8) (0,9) (1,2) (1,3) (1,4) (1,5) (1,6) (1,7)
*    Word 1: xxxxx (1,9) (2,3) (2,4) (2,5) (2,6) (2,7) (2,8)
*            (2,9) (3,4) etc. (where 'xxxxx' denotes unused bit)
*
* After a conversion routine is called (build_mat()) the format is:
*    Word i: xxxxx xxxxx xxxxx xxxxx xxxxx xxxxx (i,9) (i,8)
*            (i,7) (i,6) (i,5) (i,4) (i,3) (i,2) (i,1) (i,0)
*
****************************************************************************/

/*tex

\file{tapes.c}
\path{src/graph/i-o}
\title{Standard 10 vertex graph reader from Read's Tapes.}
\classes{}
\makeheader

xet*/


/****************************************************************************
*
* D E C L A R A T I O N S   O F   C O M M O N   V A R I A B L E S
*
****************************************************************************/

#include <stdio.h>
#include <math.h>
#include "graph/i-o/tapes.h"

#define fact10 3628800
//#define NOCOMPLEMENT

/**************************************************************************/

/*
 * Count how many lit bits are in a byte.
 */
static int bits(uchar byte)
  {
   int count=0, i;

   for (i=0; i<8; i++)
    if (byte & (1<<i)) count++;

   return count;
  }

/*
 * Build adjacency matrix from upper-triangular 3 half-words.
 */
void Tape_graph::build_graph()
  {
   int bit = 15;
   int word = 0, i, j;
//   bool ej;

   delete orig_G;
   orig_G = new Graph(10);

   for (i = 0; i<10; i++) s_graph.m[i] = 0;

   for (i = s_graph.span = 0; i<10; i++)
     {
      for (j = i+1; j<10; j++)
       {
        /*
         * Note: Symmetric Matrix
         */
        if (!bit--) { word++; bit = 14; }
        s_graph.m[i] |= ((f_graph[word] & 1<<bit) != 0)<<j;
        s_graph.m[j] |= ((f_graph[word] & 1<<bit) != 0)<<i;

        if ((f_graph[word] & 1<<bit) != 0) orig_G->addEdge(i,j);
//        else    orig_G->rm_edge(i,j);

       }
// printf("span %d = %d; %d\n",i,s_graph.m[i],s_graph.span);
      s_graph.span |= s_graph.m[i];
     }

  return;
  }

/****************************************************************************/

/*
 * File opened ahead of time.
 */
Tape_graph::Tape_graph(FILE* in, vertNum sz, read_mode m)
 {
#ifdef DEBUG
   if (sz<0 || sz>45) error("Tape_graph::  bad size passed");
   if (!in) error("Tape_graph:: file not open");
#endif

  in_graphs=in;
  size = sz;
  mode = m;
  f_count = f_record = 0;

  orig_G = new Graph(1);
  G = new Graph(1);
  
 }

/*
 * Ask for input file and open file.
 */
Tape_graph::Tape_graph(read_mode m)
 {
   FILE* in=NULL;
   int siz;
   char f_name[40];

   while (in==NULL)
    {
      printf("What file to read? ");
      scanf("%s",f_name);
      putchar('\n');

      if (!(in=fopen(f_name,"r")))
       {
        printf("Error opening file %s!\n",f_name);
       }
      else
       {
        printf("Enter size of graphs for error checking: ");
        fflush(stdout);
        scanf("%d", &siz);
        putchar('\n');
       }
    }

//  printf("File opened.\n");

  in_graphs=in;
  size = (vertNum) siz;
  mode = m;
  f_count = f_record = 0;

  orig_G = new Graph(1);
  G = new Graph(1);
  
 }

Tape_graph::~Tape_graph()
 {
  delete orig_G;
  delete G;
 }

/***************************************************************************/

/*
 * Read next graph from file.
 */
uint Tape_graph::read_graph()
 {
  uchar b[6], a[4];
  int i, skip;

    /* f_record++;       Keep count of current record. */

    /*
     * Check for possible fortran record marker.
     */
    if ((b[0]=fgetc(in_graphs)) == 10)  skip = 1;
    else skip = 0;

    for (i = 1; i<6; i++)
      b[i] = fgetc(in_graphs);

    if (feof(in_graphs))
        {
          fclose(in_graphs);
          return 0;
        }

    f_aut = 0;
    for (i = 0; i<4; i++)
     {
      a[i] = fgetc(in_graphs);
      f_aut = (f_aut<<8) + a[i];
     }

    /*
     * If begining record marker than shift input by 1 byte.
     */
    if (skip == 1 && (f_aut == 0 || fact10 % f_aut ||
        (size > 0 && size != bits(b[0])+bits(b[1])+bits(b[2])+
                              bits(b[3])+bits(b[4])+bits(b[5]))
                              /* || f_record > 950 */ ))
     {
      f_record++;               /* Start new record. */

      b[0] = b[1];
      b[1] = b[2];
      b[2] = b[3];
      b[3] = b[4];
      b[4] = b[5];
      b[5] = a[0];

      a[0] = a[1];
      a[1] = a[2];
      a[2] = a[3];
      a[3] = fgetc(in_graphs);

      f_aut = a[0];
      f_aut = (f_aut<<8) + a[1];
      f_aut = (f_aut<<8) + a[2];
      f_aut = (f_aut<<8) + a[3];
     }

    if (feof(in_graphs))
        {
          fclose(in_graphs);
          return 0;
        }

  if (f_aut == 0 || fact10 % f_aut )    /* Notify user of bad graph. */
   {
    printf("Bad Graph %d aut = %d; record %d\n", f_count, f_aut, f_record);
    printf("b[] %d %d %d %d %d %d | %d %d %d %d SZ=%d\n",
      b[0],b[1],b[2],b[3],b[4],b[5],a[0],a[1],a[2],a[3],
      bits(b[0])+bits(b[1])+bits(b[2])+bits(b[3])+bits(b[4])+bits(b[5]));
    aassert(0);
   }

    /*
     * Keep a copy of file formated graph.
     */
    f_graph[0] = b[0]*256 + b[1];
    f_graph[1] = b[2]*256 + b[3];
    f_graph[2] = b[4]*256 + b[5];

// printf("%ld %ld %ld\n",f_graph[0],f_graph[1],f_graph[2]);

    return ++f_count;
}

/**************************************************************************/


bool Tape_graph::nextgraph()
 {
  switch (mode)
   {
    case ALL:
     if (f_count>0)
      if (G->order() < 10 /* orig_G.order() */)
        {
         G->addNode(1);
         return true;
        }
     // else { continue with NO_ISOLATED case. }

    case NO_ISOLATED:
      if (taken_complement)
       {
        if (!read_graph()) return false;
        build_graph();
#ifdef NOCOMPLEMENT
        taken_complement=true;
#else
        taken_complement=false;
#endif
       }
      else
       {
        orig_G->complement();
        taken_complement=true;
       }

      delete G;
      G = new Graph( *orig_G );
      G->rmIsoNodes();
      return true;
   }

  assert(false);
  return false;
 }

/***************************************************************************/

