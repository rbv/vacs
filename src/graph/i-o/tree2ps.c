
/***********************************************************************
 * Source for drawing trees.
 **********************************************************************/

/*tex

\file{tree2ps.c}
\path{src/graph/i-o}
\title{Source for drawing trees}
\classes{}
\makeheader

xet*/

#include <iostream>
#include <stdlib.h>

#include "graph/tree.h"
#include "graph/i-o/psspec.h"

// ------------------------------------------------------------------------
// 			General Tree Drawing Stuff
// ------------------------------------------------------------------------

//static const float inch = 72.0;

struct lNodeInfo
 {

  vertNum parent;		// Layed out from root node

  float x, y;			// Physical location of this node

  int width,			// How wide is the subtree pruned 
				// down from this node.

      depth;			// How far away from the root.

  int xOffset;			// Number of reserved node spaces
				// to the left of this node. 
 };

static int setWidths( const Tree &l, vertNum root, lNodeInfo *info )
 {
  int newWidth = 0;

  // do depths here too
  //
  info[root].depth = info[info[root].parent].depth+1;

  for (int i = 0; i < l.degree(root); i++)
   {
     vertNum child = l.neighbor(root,i);
     
     if ( child == info[root].parent ) continue;

     info[child].parent = root;
     info[child].xOffset = info[root].xOffset + newWidth;
     //
     newWidth += setWidths( l, child, info );
   }

  return info[root].width = max(1, newWidth);
 }

// ------------------------------------------------------------------------
// 			Bounded Box PostScript Tree Stuff
// ------------------------------------------------------------------------

static void BBplotEdges( ostream &out, const Tree &T, vertNum root, 
                        lNodeInfo *info, const PSSpec *specs )
 {
  for (int i = 0; i < T.degree(root); i++)
   {
     vertNum child = T.neighbor(root,i);
     
     if ( child == info[root].parent ) continue;

     if (specs)
      {
       out << "v" << root << " v" << child << " edge" <<
		specs->drawEdgeType(root,child, (const void*) &T) << nl;

       if (specs->getEdgeStrFnc())	// do ej labels
        {
          out << " (" 
              << specs->drawEdgeStr(root,child, (const void*) &T) << ") "
          << (info[root].x + info[child].x)/2.0 * inch << ' ' << 
          (info[root].y + info[child].y)/2.0 * inch << " label\n";
        }
      }
     else
      {
       out << "v" << root << " v" << child << " edge\n";
      }

     BBplotEdges( out, T, child, info, specs );
   }
 }

static void doBB( const Tree &T, const PSSpec *specs )
 {
   // HACK alert!!!
   //
   //ostream &out( specs!=0 ? specs->getOut() : cout);
   //ostream &out(specs ? cerr : cout);
   //ostream &out(specs->getOut()); 
   //ostream_withassign &out(cout);
   //
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   vertNum n = T.order();

   lNodeInfo *info = new lNodeInfo[n];

   /*
    * root node set up to get things rolling.
    */
   info[0].parent = 0;	// void value
   info[0].depth = 0;	// gets incremented to 1 (row #)
   info[0].xOffset = 0;	// this is fixed for root node
   
   vertNum maxWidth = setWidths( T, 0, info );

   vertNum maxDepth = 1;
   //
   int i; for (i=1; i<n; i++) 
     if ( info[i].depth > maxDepth ) maxDepth = info[i].depth;
  
   // Calculate how many pages we want (rows * columns).
   //
   int columns = maxWidth/10 + 1,
       rows = maxDepth/10 + 1;

   // think in terms of inches ( should be consistent with ltree.ps! )
   //
   float xspace = ((float) columns*7.75)/((float) maxWidth+1),
         yspace = ((float) rows*10.25)/((float) maxDepth+1);
 
   float PSWidth = 400.0, PSHeight = 400.0;
   //
   if (specs) 
    {
     PSWidth = specs->getWidth();
     PSHeight = specs->getHeight();    
    }

   out << "%!VACS tree2ps V1.0 (1993)\n";
   out << "\n%%BoundingBox 0 0 " << PSWidth << ' ' << PSHeight << nl;
   out << "\ngsave\n";
   out << PSWidth / (columns*7.25*inch) << ' '
       << PSHeight / (rows*10.25*inch) << " scale\n";

   // Print function heading.
   //
   out << "\n(" << getenv("VACS") << "/lib/ltree.ps) run\n%\n";


   /*
    * Now find/print locations for the vertices.
    */
   for (i=0; i<n; i++)
    {
     info[i].x = (info[i].xOffset+info[i].width/2.0)*xspace;
     info[i].y = (maxDepth - info[i].depth + 0.75 )*yspace;	// flip

     out << "/v" << i << 
            " { " << info[i].x << " inch " << info[i].y << " inch } def\n";
    }
   //
   out << nl;

   /*
    * Draw edges first.
    */
   BBplotEdges( out, T, 0, info, specs );
   //
   out << nl;

   /*
    * Now do nodes and their labels.
    */
   //
   for (i=0; i<n; i++)
    {
      if (specs)
       {
        out << "v" << i << " node" 
            << specs->drawNodeType(i, (const void*) &T) 
	  << "  (" << specs->drawNodeStr(i, (const void*) &T) << ") v" 
          << i << " label\n";
       }
      else
       {
        out << "v" << i << " node\n";
       }
    }

   //
   out << "\ngrestore\n";

   delete [] info;
}


// ------------------------------------------------------------------------
// 			DVI Special PostScript Tree Stuff
// ------------------------------------------------------------------------

static void doDVI( const Tree &T, const PSSpec *specs )
 {
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   vertNum n = T.order();

   lNodeInfo *info = new lNodeInfo[n];

   /*
    * root node set up to get things rolling.
    */
   info[0].parent = 0;	// void value
   info[0].depth = 0;	// gets incremented to 1 (row #)
   info[0].xOffset = 0;	// this is fixed for root node
   
   vertNum maxWidth = setWidths( T, 0, info );

   vertNum maxDepth = 1;
   //
   int i; for (i=1; i<n; i++) 
     if ( info[i].depth > maxDepth ) maxDepth = info[i].depth;
  
   // Calculate how many pages we want (rows * columns).
   //
   int columns = maxWidth/10 + 1,
       rows = maxDepth/10 + 1;

   // think in terms of inches ( should be consistent with ltree.ps! )
   //
   float xspace = ((float) columns*7.75)/((float) maxWidth /* +1 */),
         yspace = ((float) rows*10.25)/((float) maxDepth /* +1 */);
 
   float PSWidth = 400.0, PSHeight = 400.0;
   //
   if (specs) 
    {
     PSWidth = specs->getWidth();
     PSHeight = specs->getHeight();    
    }

//   out << "\\PSBox{" << PSWidth << "}{" << PSHeight << "}{\n";
   out << "\\makebox[" << PSWidth << "bp][l]{\n";
   out << "\\rule{0bp}{" << PSHeight << "bp}\n";
   out << "\\special{\"\n";

   out << "/Times-Roman findfont\n";
   out << "12 scalefont setfont\n";

   out << PSWidth / (columns*7.25*inch) << ' '
       << PSHeight / (rows*10.25*inch) << " scale\n";

   /*
    * Now find/print locations for the vertices.
    */
   for (i=0; i<n; i++)
    {
     info[i].x = (info[i].xOffset+info[i].width/2.0)*xspace;
     info[i].y = (maxDepth - info[i].depth + 0.75 )*yspace;	// flip

     out << "/v" << i << 
            " { " << info[i].x << " inch " << info[i].y << " inch } def\n";
    }
   //
   //out << nl;

   /*
    * Draw edges first.
    */
   BBplotEdges( out, T, 0, info, specs );
   //
   //out << nl;

   /*
    * Now do nodes and their labels.
    */
   //
   for (i=0; i<n; i++)
    {
      if (specs)
       {
        out << "v" << i << " node" 
            << specs->drawNodeType(i, (const void*) &T) << "  (" 
            << specs->drawNodeStr(i, (const void*) &T) 
            << ") v" << i << " label\n";
       }
      else
       {
        out << "v" << i << " node\n";
       }
    }

   //
   out << "}}\n";

   delete [] info;
}

// ------------------------------------------------------------------------
// 				Poster Tree Stuff
// ------------------------------------------------------------------------

static int edgeFunctions;	// How many postscript edge calls
//
static void plotEdges( ostream &out, const Tree &T, vertNum root, 
                       lNodeInfo *info, const PSSpec *specs )
 {
  for (int i = 0; i < T.degree(root); i++)
   {
     vertNum child = T.neighbor(root,i);
     
     if ( child == info[root].parent ) continue;

     if (edgeFunctions % 100 == 0)
       {
         if (edgeFunctions!=0) out << "\n} def\n";
         out << "\n/ltreeEdges" << edgeFunctions/100 << " { \n\n";
       }

     if (specs)
      {
       out << "  %v" << root << " v" << child << " edge" <<
		specs->drawEdgeType(root,child, (const void*) &T) << nl;
       out << "   " << info[root].x * inch << ' ' << info[root].y * inch << ' '
         << info[child].x * inch << ' ' << info[child].y * inch << 
         " edge" << specs->drawEdgeType(root,child, (const void*) &T) << nl;

       if (specs->getEdgeStrFnc())	// do ej labels
        {
          out << "    (" << specs->drawEdgeStr(root,child, (const void*) &T) 
          << ") " << (info[root].x + info[child].x)/2.0 * inch << ' ' << 
          (info[root].y + info[child].y)/2.0 * inch << " label\n";
        }
      }
     else
      {
       out << "  %v" << root << " v" << child << " edge\n";
       out << "   " << info[root].x * inch << ' ' << info[root].y * inch << ' '
         << info[child].x * inch << ' ' << info[child].y * inch << " edge\n";
      }

     edgeFunctions++;

     plotEdges( out, T, child, info, specs );
   }
 }

static void doP( const Tree &T, const PSSpec *specs )
 {
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   vertNum n = T.order();

   lNodeInfo *info = new lNodeInfo[n];

   /*
    * root node set up to get things rolling.
    */
   info[0].parent = 0;	// void value
   info[0].depth = 0;	// gets incremented to 1 (row #)
   info[0].xOffset = 0;	// this is fixed for root node
   
   vertNum maxWidth = setWidths( T, 0, info );

   vertNum maxDepth = 1;
   //
   int i; for (i=1; i<n; i++) 
     if ( info[i].depth > maxDepth ) maxDepth = info[i].depth;
  
   // Calculate how many pages we want (rows * columns).
   //
   int columns = maxWidth/10 + 1,
       rows = maxDepth/10 + 1;

   // think in terms of inches ( should be consistent with ltree.ps! )
   //
   float xspace = ((float) columns*7.75)/((float) maxWidth+1),
         yspace = ((float) rows*10.25)/((float) maxDepth+1);
 
   // Print function heading.
   //
   out << "\n(" << getenv("VACS") << "/lib/ltree.ps) run\n%\n";

   /*
    * Now find/print locations for the vertices.
    */
   for (i=0; i<n; i++)
    {
     info[i].x = (info[i].xOffset+info[i].width/2.0)*xspace + 0.25;
     info[i].y = info[i].depth*yspace + 0.25;

     out << "%/v" << i << 
            " { " << info[i].x << " inch " << info[i].y << " inch } def\n";
    }

   /*
    * Draw edges first.
    */
   //out << nl << "\n/ltreeEdges { \n\n";
   // 
   edgeFunctions = 0;
   //
   plotEdges( out, T, 0, info, specs );
   //
   out << "\n} def\n";	// keep this!

   /*
    * Now do nodes and their labels.
    */
   //
   for (i=0; i<n; i++)
    {
      if (i % 100 == 0)
       {
         if (i!=0) out << "\n} def\n";
         out << "\n/ltreeNodes" << i/100 << " { \n\n";
       }

      if (specs)
       {
        out << "  %v" << i << " node" 
		<< specs->drawNodeType(i,(const void*) &T) 
	<< "  (" << specs->drawNodeStr(i, (const void*) &T) 
	<< ") v" << i << " label\n";
        out << "   " << info[i].x * inch << ' ' << info[i].y * inch << 
          " node" << specs->drawNodeType(i, (const void*) &T) << 
          "  (" << specs->drawNodeStr(i, (const void*) &T) << ") " 
          << info[i].x * inch << ' ' << info[i].y * inch << " label\n";
       }
      else
       {
        out << "  %v" << i << " node" 
	  << "  () v" << i << " label\n";
        out << "   " << info[i].x * inch << ' ' << info[i].y * inch << " node" 
          << "  () " 
          << info[i].x * inch << ' ' << info[i].y * inch << " label\n";
       }
    }
   //
   out << "\n} def\n";

   /*
    * Print function trailer and envoke command.
    */
   out << "\n/ltree { gsave\n\n";
   //
   for (i=0; i <= edgeFunctions/100; i++) out << "  ltreeEdges" << i << nl;
   out << nl;
   for (i=0; i <= n/100; i++) out << "  ltreeNodes" << i << nl;
   //
   out << "\ngrestore } def\n\n";

   out << "{ltree} " << columns << ' ' << rows << " printposter\n";

   delete [] info;
}


// -----------------------------------------------------------------------

/**
 ** Top level PostScript drawing routine for trees.
 **/

void tree2ps( const Tree &T, const PSSpec *specs )
 {
   if (specs)
    {
      switch( specs->getFormat() )
       {
         case PSSpec::BoundingBox : doBB(T,specs); break;
         case PSSpec::DviSpecial :  doDVI(T,specs); break;
         case PSSpec::Poster :      doP(T,specs);
       }
    }
   else doBB(T,0);
 }

