
/***********************************************************************
 * Source for drawing graphs.
 **********************************************************************/

/*tex

\file{graph2ps.c}
\path{src/graph/i-o}
\title{Source for drawing graphs}
\classes{}
\makeheader

xet*/

#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "graph/graph.h"
#include "graph/i-o/psspec.h"

typedef Array<float> Floats;

#define SPRING_LAYOUT
#ifdef SPRING_LAYOUT
void springEmbedding( const Graph &G, Floats &X, Floats &Y );
#endif

// ------------------------------------------------------------------------
// 			General Graph Drawing Stuff
// ------------------------------------------------------------------------

//static const float inch = 72.0;	// Postscript points per inch.

static void plotEdges(ostream &out, const Graph &G, const PSSpec *specs)
 {
   vertNum n = G.order();

   for (int i=0; i<n-1; i++)
    for (int j=i+1; j<n; j++)
     {
      if (G.isEdge(i,j))
       {
         if (specs && specs->getEdgeTypeFnc())
          {
            out << "v" << i << " v" << j << " edge" <<
                     specs->drawEdgeType( i, j, (const void*) &G ) << nl;
    
            if (specs->getEdgeStrFnc())
             {
              aassert(false); // write when needed
             }
          }
         else
          {
           out << "v" << i << " v" << j << " edge\n";
          }
       }  
     }
 }

static void plotNodes(ostream &out, const Graph &G, const PSSpec *specs)
 {
   vertNum n = G.order();

   for (int i=0; i<n; i++)
    {
      if (specs)
       {
        out << "v" << i << " node" 
            << specs->drawNodeType(i, (const void*) &G) 
	  << "  (" << specs->drawNodeStr(i, (const void*) &G) << ") v" 
          << i << " label\n";
       }
      else
       {
        out << "v" << i << " node\n";
       }
    }
 }

// ------------------------------------------------------------------------

/*
 * Default circular layout of a graph.
 */
static void getCircular( vertNum n, Floats &x, Floats &y )
 {
  float dtheta = 2*M_PI/((float) n);
  float theta = M_PI_2;

  for (int i=0; i<n; i++)
   {
     x[i] = 0.5 + cos(theta)/2.0;
     y[i] = 0.5 + sin(theta)/2.0;

     theta -= dtheta;
   }
 }

// ------------------------------------------------------------------------
// 		Bounded Box PostScript Graph Stuff
// ------------------------------------------------------------------------

static void doBB( const Graph &G, const PSSpec *specs )
 {
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   vertNum n = G.order();

   float PSWidth = 600.0, PSHeight = 600.0;
   //
   if (specs) 
    {
     PSWidth = specs->getWidth();
     PSHeight = specs->getHeight();    
    }

   out << "%!VACS graph2ps V1.0 (1993)\n";
   out << "\n%%BoundingBox: 0 0 " << PSWidth << ' ' << PSHeight << nl;
   out << "\ngsave\n";

   //out << PSWidth / (columns*7.25*inch) << ' '
   //    << PSHeight / (rows*10.25*inch) << " scale\n";

   /*
    * Let us scale so label fonts look better. (for small dvi boxes)
    */
   if (PSHeight != 600.0)
    {
      // We want to keep circles so scale evenly.
      //
      out << PSHeight/600.0 << ' ' << PSHeight/600.0 << " scale\n";

      PSWidth = PSWidth/(PSHeight/600.0);
      PSHeight = 600; // PSHeight/(PSHeight/600.0);
    }

   // Print function heading.
   //
   out << "\n(" << getenv("VACS") << "/lib/graph.ps) run\n%\n";

   // Coordinates of vertices scaled to 1 inch box. (0.0 to 1.0)
   //
   Floats x(n), y(n);
  
   // default layout (add option to PSSpec later..)
   //
#ifdef SPRING_LAYOUT
   springEmbedding( G, x, y );
#else
   getCircular( n, x, y );
#endif

   /*
    * Now find/print locations for the vertices.
    */
   for (int i=0; i<n; i++)
    {
     x[i] = x[i]*PSWidth/inch;
     y[i] = y[i]*PSHeight/inch;

     out << "/v" << i << " { " << x[i] << " inch " << y[i] << " inch } def\n";
    }
   //
   out << nl;

   /*
    * Draw edges first.
    */
   plotEdges( out, G, specs );
   //
   out << nl;

   /*
    * Now do nodes and their labels.
    */
   plotNodes( out, G, specs );
   //
   out << "\ngrestore\n";
}


// ------------------------------------------------------------------------
// 			DVI Special PostScript Graph Stuff
// ------------------------------------------------------------------------

static void doDVI( const Graph &G, const PSSpec *specs )
 {
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   vertNum n = G.order();

   float PSWidth = 400.0, PSHeight = 400.0;
   //
   if (specs) 
    {
     PSWidth = specs->getWidth();
     PSHeight = specs->getHeight();    
    }


   //out << "\\makebox[" << PSWidth << "bp][l]{\n\\rm\\normalsize\n";
   out << "\\makebox[" << PSWidth << "bp][l]{\n";
   out << "\\rule{0bp}{" << PSHeight << "bp}\n";
   out << "\\special{\"\n";

   if (specs && (specs->getNodeStrFnc() || specs->getEdgeStrFnc()) )
    {
     out << "/Times-Roman findfont\n";
     out << "12 scalefont setfont\n";
    }

   //out << PSWidth / (columns*7.25*inch) << ' '
   //    << PSHeight / (rows*10.25*inch) << " scale\n";

   /*
    * Let us scale so label fonts look better. (for small dvi boxes)
    */
   if (PSHeight != 400.0)
    {
      // We want to keep circles so scale evenly.
      //
      out << PSHeight/400.0 << ' ' << PSHeight/400.0 << " scale\n";

      PSWidth = PSWidth/(PSHeight/400.0);
      PSHeight = 400; // PSHeight/(PSHeight/400.0);
    }

   // Coordinates of vertices scaled to 1 inch box. (0.0 to 1.0)
   //
   Floats x(n), y(n);
  
   // default layout (add option to PSSpec later..)
   //
#ifdef SPRING_LAYOUT
   springEmbedding( G, x, y );
#else
   getCircular( n, x, y );
#endif

   /*
    * Now find/print locations for the vertices.
    */
   for (int i=0; i<n; i++)
    {
     x[i] = x[i]*PSWidth/inch;
     y[i] = y[i]*PSHeight/inch;

     out << "/v" << i << " { " << x[i] << " inch " << y[i] << " inch } def\n";
    }

   /*
    * Draw edges first.
    */
   plotEdges( out, G, specs );

   /*
    * Now do nodes and their labels.
    */
   plotNodes( out, G, specs );

   out << "}}\n";
}

// ------------------------------------------------------------------------
// 			Poster Graph Stuff
// ------------------------------------------------------------------------


static void doP( const Graph &G, const PSSpec *specs )
 {
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   vertNum n = G.order();

   out << "not implemented yet\n";

   return;
}


// -----------------------------------------------------------------------

/**
 ** Top level PostScript drawing routine for graphs.
 **/

void graph2ps( const Graph &G, const PSSpec *specs )
 {
   if (specs)
    {
      switch( specs->getFormat() )
       {
         case PSSpec::BoundingBox : doBB(G,specs); break;
         case PSSpec::DviSpecial :  doDVI(G,specs); break;
         case PSSpec::Poster :      doP(G,specs);
       }
    }
   else doBB(G,0);
 }

