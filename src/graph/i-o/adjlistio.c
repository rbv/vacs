
/***************************************************************************
 * Input adjacency lists, etc. (ASCII)
 ***************************************************************************/

/*tex

\file{graphrepio.c}
\path{src/graph/i-o}
\title{Standard graph class representiion}
\classes{}
\makeheader

xet*/

#include <iostream>

#include "array/array.h"
#include "array/dyarray.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"

/*****************************************************************************/

typedef Array< DyArray< vertNum > > Goofcast;
//typedef DyArray< DyArray< vertNum > > Goofcast;
//istream& operator>>( istream& in, DyArray<DyArray<vertNum>>& L )
//istream& operator>>( istream& in, AdjLists& L )
istream& operator>>( istream& in, Goofcast& L )
 {
//cerr << "operator>>( AdjLists )\n";

   int n;	// Get first dimension.
   in >> n;

//cerr << "n = " << n << nl;

   assert( n>=0 );

   ((AdjLists&) L).resize(n);

   char c;
   //
   for (int i=0; i<n; i++) 
    { 
     c = 0;
     while (c != ':') { in >> c; }

     VertArray S;
     in >> S;
//cerr << S << nl;
     ((AdjLists&) L)[i] = S;

//cerr << i << ' ' << L[i] << nl;;
    }

   return in;
 }

