
/*************************************************************************
 *
 *  You might check this prog which is a straight implementation
 *  of an iterative solution of the spring model: compute the shortest
 *  path for all pairs of vertices and place springs of appropriate lengths
 *  between them. Then find the equilibrium state of this system of springs
 *  and points. - This model is standard, I don't have any reference handy.
 *
 *  The prog is in Turbo Pascal, but should be easily modifyable.
 *  Hope this helps. Johannes - joe@inuo30.mathematik.uni-jena.de}
 *
 *  Lazy conversion to C/C++ by 'mjd@csr.uvic.ca' (Feb 93)
 *
 ************************************************************************/

#include "stdtypes.h"
#include "array/array.h"
#include "array/sortarray.h"
#include "graph/graph.h"
#include "graph/algorithm/distance_ga.h"

#define START_RANDOM
#ifdef START_RANDOM
#include "general/random.h"
#endif

#include <iostream>
#include <math.h>

#if defined(SUNPRO) || defined(CRAY) || defined(__xlC__) || defined(__DECCXX) || defined(__GNUC__)
inline double sqr(double x) { return x*x; }
#else
extern double sqr(double);
#endif


typedef Array<float> Floats;

const float GS_eps = 0.001;
//const float GS_pi = M_PI;

static int n;   // Keep the order of graph global.

#if 0
const int GS_index = 50; 
static float f[GS_index][GS_index], 	// { spring constants }
        x[GS_index], y[GS_index], 	// { x, y coordinates }
        fx[GS_index], fy[GS_index], 	// { x, y comp of force at point }
        fs[GS_index]; 		// { = fx ^ 2 + fy ^ 2 }
#else
static float **f, 	// { spring constants }
        *x, *y, 	// { x, y coordinates }
        *fx, *fy, 	// { x, y comp of force at point }
        *fs; 		// { = fx ^ 2 + fy ^ 2 }
#endif

static void prepare( const Graph &G, DistanceMatrix &D )
{     
int i, j;

          // { compute shortest paths }
          //
	  dist_mat( G, D );

          // { compute spring constants }
          //
          for (i=0; i<n; i++) {
              for ( j = 0; j<n; j++ ) {
                  if (i != j) {
                     f[i][j] = 1.0 / sqr (D(i,j));
                  }
              }
          }

          // { distribute start points somewhere }
          //
#ifdef START_RANDOM 
 	  RandomInteger RN(250);

          for (i=0; i<n; i++)
          {
            x[i] = ((float) RN()-150) / 150.0;
            y[i] = ((float) RN()-150) / 150.0;
          }
#else
          for (i=0; i<n; i++) {
              x[i] = cos (i * 2 * M_PI / n);
              y[i] = sin (i * 2 * M_PI / n);
          }
#endif
}

static float xmin, xmax, ymin, ymax, xwdth, ywdth, scl;
//
static void convert(float rx, float ry, int &gx, int &gy)
{     
   gx = (int) ((rx - xmin) * scl + 0.5) + 100;
   gy = (int) ((ry - ymin) * scl + 0.5) + 200;
}
//
#define NO_SHOW
//#define VERT_SHOW
//#define EJ_SHOW
//#define NODE_SHOW
//
#ifdef NO_SHOW
static void show( )
#else
static void show( const Graph &G )
#endif
{     
float xscl, yscl;

float getmaxx = 400.0;
float getmaxy = 600.0;

          xmin = x [0]; xmax = xmin;
          ymin = y [0]; ymax = ymin;

          for (int i = 1; i<n; i++) {
              if (x [i] < xmin) xmin = x [i];
              if (x [i] > xmax) xmax = x [i];
              if (y [i] < ymin) ymin = y [i];
              if (y [i] > ymax) ymax = y [i];
          }

          xwdth = xmax - xmin; ywdth = ymax - ymin;
          xscl  = getmaxx / xwdth; yscl = getmaxy / ywdth;
          scl   = xscl; 
          if (yscl < scl) scl = yscl;
    
cout << "\ngsave\n";

#ifdef VERT_SHOW
int gx, gy;

          //cerr << "showing verts:\n";
          //
          for (i = 0; i<n; i++) 
           {
              convert (x [i], y [i], gx, gy);
              //cerr << "(" << gx << ", " << gy << ")\n";
              cout << "/v" << i << " { " << gx << " " << gy << " } def\n";
           }
          cout << nl;
#endif
#ifdef EJ_SHOW
int gxi, gyi, gxj, gyj;

          //cerr << "showing edges:\n";
          //
          //cleardevice;
          //
          for (i = 0; i < n - 1; i++) {
              for (int j = i + 1; j<n; j++) {
                  if (G.isEdge(i,j)) {
                     convert (x [i], y [i], gxi, gyi);
                     convert (x [j], y [j], gxj, gyj);
                     //line    (gxi, gyi, gxj, gyj);
                     //cerr << "(gxi, gyi, gxj, gyj)\n";
                     //cerr << "(" << gxi << ", " << gyi << ", " << 
                     //               gxj << ", " << gyj << ")\n";
                     cout << "v" << i << ' ' << "v" << j << " edge\n";
                  }
              }
          }
          cout << nl;
#endif
#ifdef NODE_SHOW

          //cerr << "showing node labels:\n";
          //
          for (i = 0; i<n; i++) 
           {
              cout << "v" << i << " vert (" << i << ") v" << i << " label\n";
           }
          cout << nl;
#endif

cout << "grestore showpage\n";
}


static void forces(const DistanceMatrix &D) // { compute forces at points }
{
int i, j;
float sx, sy, dx, dy, tmp;

       for (i=0; i<n; i++) {
              sx = 0; sy = 0;
              for ( j = 0; j<n; j++ ) {
                  if (i != j) {
                     dx = x[i] - x[j]; dy = y[i] - y[j];
                     tmp = f[i][j] * (1 - D(i,j)
                                            / sqrt (sqr (dx) + sqr (dy)));
                     sx = sx + tmp * dx; sy = sy + tmp * dy;
                  }
              }
              fx[i] = sx; fy[i] = sy;
              fs[i] = sqrt (sqr (sx) + sqr (sy));
          }
}

// { compute offset for point m }
//
static void handle(int m, const DistanceMatrix &D) 
{     
float fxx=0.0, fxy=0.0, fyy=0.0, dx, dy, tmp;
int i;

          for (i=0; i<n; i++) {
              if (i != m) {
                 dx  = x[m] - x[i]; dy = y[m] - y[i];
                 tmp = sqr (dx) + sqr (dy);
                 tmp = 1.0 / tmp / sqrt (tmp);
                 fxx = fxx + f[m][i] * (1 - D(m,i) * sqr (dy) * tmp);
                 fxy = fxy + f[m][i] * D(m,i) * dx * dy * tmp;
                 fyy = fyy + f[m][i] * (1 - D(m,i) * sqr (dx) * tmp);
              }
          }
          tmp   = sqr (fxy) - fxx * fyy;
          x[m] = x[m] + (fx[m] * fyy - fy[m] * fxy) / tmp;
          y[m] = y[m] + (fy[m] * fxx - fx[m] * fxy) / tmp;
}

static void loop(
#ifndef NO_SHOW
const Graph &G, 
#endif
const DistanceMatrix &D)
{     
bool leaving = false;
int maxind, i;
float max;

          //repeat  forces;
          //
          while (leaving==false)
           {
                  forces(D);

                  maxind = 0; max = fs[0];
                  for (i = 1; i<n; i++) {
                      if (fs[i] > max) {
                         maxind = i; max = fs[i];
                      }
                  }

#ifndef NO_SHOW
                  show(G);
#endif

                  if (max < GS_eps) leaving = true;
                  else 	  	    handle (maxind,D);

           } // until leaving or keypressed;
}

// exported layout function
//
void springEmbedding( const Graph &G, Floats &X, Floats &Y )
 {
    n = G.order();  //assert(n<=GS_index);

    f = new float*[n]; 	
    f[0] = new float[n*n];
    for (int nloop =1; nloop<n; nloop++) f[nloop] = f[0] + nloop*n;
    x = new float[n]; y = new float[n];
    fx = new float[n]; fy = new float[n];
    fs = new float[n];

    DistanceMatrix D(n);
    //
    prepare(G,D);

#ifndef NO_SHOW
    loop(G,D);
#else
    loop(D);
#endif


    int i; for (i=0; i<n; i++)
     {
       X[i] = x[i];
       Y[i] = y[i];
     }

    //cerr << "1) x=" << X << nl;
    //cerr << "1) y=" << Y << nl;

    float xMn = CAST(X,SortableArray<float>).min(); 
    float xMx = CAST(X,SortableArray<float>).max(); 
    float xDelta = xMx-xMn; 

    float yMn = CAST(Y,SortableArray<float>).min(); 
    float yMx = CAST(Y,SortableArray<float>).max(); 
    float yDelta = yMx-yMn; 

    float maxD = max(xDelta, yDelta);

    for (i = 0; i<n; i++)
     {
      X[i] = (X[i] - xMn)/maxD;
      Y[i] = (Y[i] - yMn)/maxD;
     }

    //cerr << "2) x=" << X << nl;
    //cerr << "2) y=" << Y << nl;

    delete [] f[0]; delete [] f;
    delete [] x; delete [] y;
    delete [] fx; delete [] fy;
    delete [] fs;

    return;
 }
