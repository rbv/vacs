
/***************************************************************************
 * Vertex labeled graph class member functions.
 ***************************************************************************/

/*tex

\file{lgraph.c}
\path{src/graph}
\title{Standard vertex labeled graph class implementation.}
\classes{LGraph}
\makeheader

xet*/

#include <iostream>

#include "graph/lgraph.h"


/**************************************************************************/

/*
 * Construct graph initialized via another graph.
 */
LGraph::LGraph(const LGraph &G, vertNum extra ) : 
  Graph(G,extra), labels(G.order()+extra)
 {
  for (int i = 0; i < G.order(); i++)
   {
     labels[i]=G.getLabel(i);
   }

  // remaining labels undefined.

  return;
 }

/**************************************************************************/


/*
 * The following two constructors are cfront 3.0 sorries.
 */
LGraph::LGraph(vertNum order,           // Constructor: adjacency matrix.
        const bool **M,                 // (order, matrix, label)
        const Label &l) :
    Graph( order, M), labels(order)
    {
      labels.fill(l);
    }

LGraph::LGraph(vertNum order,           // Constructor: Empty graph with order.
         const Label &l) :
    Graph( order ), labels(order)
    {
      labels.fill(l);
    }



/*****************************************************************************/

/*
 * I/O stuff (matrix text format).
 */
ostream& operator<<( ostream& o, const LGraph& M )
 {
   o << nl << "Labeled Graph (" << M.order() << ", " << M.size() << ")\n";
   o << "--------------------------\n";

   for ( int i=0; i<M.order(); i++ )
   {
      o << i << " (" << M.getLabel(i) << ") : ";
      for ( int j=0; j<M.order(); j++ )
      {
         if (i==j)	 o << "+ ";
         else		 o << (int) M.isEdge(i,j) << ' ';
      }
      o << nl;
   }

   return o;
 }

istream& operator>>( istream& in, LGraph& M )
 {
   /*
    * Currently can read only adjacency matrix with order as first line.
    */
   int n;
   in >> n;

   assert( n>=0 );

   if ( n > M._space )
    {
     if ( M._space > 0 )  Graph::_deallocate( M._adj );
  
     M._order = M._space = n;
     if ( n>0 ) M._adj = Graph::_allocate( n ); 
    } 
   else 
    {
     M._order = n;
    }

   int entry;
   for (int i=0; i<n; i++)
    {
     for (int j=0; j<n; j++) 
      {
	in >> entry;

        M._adj[i][j] = entry ? 1 : 0;
      }
    }

   /*
    * At least resize the label array ....
    */
   M.labels.resize(M.order());

   return in; 
 }

/*****************************************************************************/

/*
 * Add verticies to graph.
 */
void LGraph::addNode(vertNum n, const Label &l)
 {
  assert( n>0 );

  /*
   * Adjust label array's length.
   */
  labels.extend(n);

  if ( n > _space - _order )
   {
    bool **matrix = _allocate( _order + n );

    for (int i=0; i<_order; i++)
     {
      for (int j=0; j<_order; j++) matrix[i][j] = _adj[i][j];
     }

    if ( _space ) _deallocate(_adj);

    _adj = matrix;
    _space = _order + n;
   }

  /*
   * Now initialize new nodes with no edges and set labels.
   */
  for (int i=_order; i<_order+n; i++)
   {
    for (int j=0; j<_order+n; j++)  _adj[j][i] = _adj[i][j] = 0;

    labels[i]=l;
   }

  _order += n;

  return;
 }

/*****************************************************************************/

/*
 * Remove a vertex from the graph.
 */
void LGraph::rmNode(vertNum v)
 {
  assert( v>=0 && v<order() );

  /*
   * First remove the label for vertex v.
   */
  if ( v<order()-1 ) labels.swap(v, order()-1);
  labels.shrink(1);

  _order--;

  for (int i=v; i<_order; i++)
   {
     int j; for (j=0; j<v; j++)
      {
       _adj[i][j] = _adj[i+1][j];
       _adj[j][i] = _adj[j][i+1];
      }

     for (j=v; j<_order; j++)
      {
       _adj[i][j] = _adj[i+1][j+1];
      }
   }

  return;
 }

/*****************************************************************************/

/*
 * Contract an edge in a graph.  (not digraphs)
 */
void LGraph::contractEdge(vertNum v1, vertNum v2, const Label &l)
 { 
#if 0
  Graph::contractEdge(v1,v2);
#else
  assert( isEdge(v1,v2) );

  for (int i=0; i<_order; i++)
   {
     if (i==v1) continue;
     if (isEdge(i,v2)) addEdge(i,v1);
   }

  rmNode(v2);

#endif

  labels[v1]=l;

  return;
 }


/*
 * Bitwise Copy of a graph.
 */
void LGraph::operator=(LGraph &G)
 {
#if 0
  Graph::operator=(G);
#else
  int n=G._order;

  if ( _order != n )
   {
     _deallocate( _adj );
     if ( n>0 ) _adj = _allocate( n );
   }

  for (int i=0; i<n; i++)
   {
    for (int j=0; j<n; j++) _adj[i][j] = G._adj[i][j];
   }
#endif

  /*
   * Now assign labels.
   */
  labels = G.labels;

  return;
 }

