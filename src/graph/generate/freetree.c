/* 
 * Generates all free trees of a particular order. 
 *
 * Program by Michael J. Dinneen for CSc 528  -- "Spring" 1993.
 *
 * ( See SIAM J. Comput. Vol 15, No. 2, 1986;
 *   by R.A. Wright, B. Richmond, A. Odlyzko and B.D. McKay )
 *
 */


#include <iostream>
#include <stdio.h>

#include "assert.h"
#include "graph/tree.h"
#include "graph/stdgraph2.h"

const int infinity = 32767;

#define CALLBACK
#ifdef CALLBACK
//typedef void (*Callback_FNC)(Tree &T);	// allow modifications to tree.
static Tree::Callback_FNC TreeFunction = 0;
//
static void defaultTreeFnc(Tree &T) { cout << T; }
#endif

//#define NOOUT
#ifdef NOOUT
static unsigned int globalNum = 0;
#endif

//----------------------------------------------------------------------

static void outputTree(const VertArray &L)
 {
#ifdef CALLBACK

  Tree T(L);
  TreeFunction(T);

#else

  static num = 0;

#ifdef NOOUT
  globalNum++;
#else
  cout << L << ' ' << num++ << nl;
#endif

#endif
 }

//----------------------------------------------------------------------

static
void nextTree(  VertArray &L,   // level sequence of free tree
                VertArray &W,   // parent indices
                vertNum &p,     // largest index > 1
                vertNum &q,     // parent of "p"
                vertNum &h1,    // deapest nodes of L1 and L2
                vertNum &h2,
                vertNum &c,     // difference location of L1 and L2 
                vertNum &r )    // r=m-1
 {
  vertNum n = L.length();

  bool fixIt = false;

  /*
   * Do we have to make a big jump down the rooted tree list?
   */
  if ( c == n || p == h2 &&     
      ( L[h1] == L[h2] + 1 && n-1-h2 > r-h1 || 
        L[h1] == L[h2] && n-h2 < r-h1 ) 
     )
   {
     if ( L[r] > 2 )
      {
        p = r; q = W[r];
        if ( h1 == r ) h1--;
        fixIt = true;
      }
     else
      {
        p = r; r--; q = 1;
      }
   }

  /*
   * Keep track of pointers that need to be computed.
   */
  bool needr = false, needc = false, needh2 = false;

  assert(p!=infinity);

  if ( p <= h1 ) h1 = p-1;  // Case tree T2 is of height 1.

  /*
   * Find what initially needs to be computed.
   */
  if ( p <= r ) needr = true;
  else 
  if ( p <= h2 ) needh2 = true;
  else
  if ( L[h2] == L[h1]-1 && n-1-h2 == r-h1 ) 
   { 
     if ( p <= c ) needc = true; 
   }
  else c = infinity;

  /*
   * Keep track of current tree in case of a jump.
   */
  vertNum oldp = p,
          delta = q-p,
          oldLq = L[q], 
          oldWq = W[q]; 
          p = infinity;

  /*
   * Start doing a rooted tree update after "p".
   */
  for (int i=oldp; i<n; i++)
   {
     L[i] = L[i+delta];

     if ( L[i] == 1 ) W[i]=0;
     else
      {
        p = i;

        if ( L[i] == oldLq ) q = oldWq;
        else                 q = W[i+delta]-delta;

        W[i] = q;
      }

     if ( needr == true && L[i] == 1 ) 
      {
        needr = false;
        needh2 = true;
        r = i-1;
      }

     if ( needh2 == true && L[i] <= L[i-1] && i > r+1 )
      {
        needh2 = false;
        h2 = i-1;

        if ( L[h2] == L[h1]-1 && n-1-h2 == r-h1 ) needc = true;
        else                                      c = infinity;
      }

     if ( needc == true )
      {
        if ( L[i] != L[h1-h2+i]-1 ) 
         {
           needc = false;
           c = i;
         }
        else c = i+1;
      }

   } // for i

  /*
   * Do we need to make tree T2 a path? (jump down rooted tree list)
   */
  if ( fixIt == true )
   {
     r = n-h1-1;
 
     for (int i=r+1; i<n; i++)
      {
        L[i] = i-r;
        W[i] = i-1;
      }      
 
     W[r+1] = 0;
     h2 = n-1;
     p = n-1;
     q = p-1;
     c = infinity;
   }
  else
   {
     if ( p == infinity )
      {
        if ( L[oldp-1] != 1 ) p = oldp-1;
        else                  p = oldp-2;

        q = W[p];
      }

     if ( needh2 == true )
      {
        h2 = n-1;

        if ( L[h2] == L[h1]-1 && h1 == r ) c = n;
        else                               c = infinity;
      }
   } // fixIt

 } // nextTree

//----------------------------------------------------------------------

static void byHandFreeTrees(int n)
{
 VertArray L(n);

 switch(n)
  {
   case 1:  { L[0]=0; outputTree(L); break; }
   case 2:  { L[0]=0; L[1]=1; outputTree(L); break; }
   case 3:  { L[0]=0; L[1]=1; L[2]=2; outputTree(L); break; }
  }

 return;
}

#ifdef CALLBACK
void Tree::freeTrees(int n, Callback_FNC TreeFnc)
{
  if (TreeFnc) TreeFunction = TreeFnc;
  else         TreeFunction = defaultTreeFnc;

#else
void main(int argc, char *argv[])
{
   int n;

   if (argc==2)
    {
     sscanf(argv[1],"%d", &n);
     cout << "Starting to count " << n << "-trees\n";
    }
   else
    {
     cout << "What tree order? "; cout.flush();
     cin >> n; cout << nl;
    }
#endif

   assert(n>=1);

   if (n<4) 
    {
     byHandFreeTrees(n);
     return;
    }

   /*
    * Build lex largest free tree.
    */
   VertArray L(n);                 // contain level sequences of 0..n-1
   VertArray W(n);                 // parent indices of nodes

   vertNum p = n-(n==4)-1;         // largest index > 1
   vertNum q = p-1;                // parent of "p"

   vertNum k = n/2+1;
   vertNum r = k-1;                // m-1
   vertNum h1 = r;                 // root of principal subsequences
   vertNum h2 = n-1;  

   vertNum c = (n % 2 ? infinity : n);  // location of difference in L1 and L2 

   int i; for (i=0; i<k; i++) 
    {
     L[i] = i;                 // this tree is a path with root in middle.
     W[i] = i-1;               // parent of L[i]
    } 
   //
   for (k=2; i<n; i++, k++)    // continue...
    {
     L[i] = k-1;               // this tree is a path with root in middle.
     W[i] = (k==2 ? 0 : i-1);  // parent of L[i]
    } 

   outputTree(L);  // output first tree
   //
   do		   // find rest of them...
    {
      nextTree(L,W,p,q,h1,h2,c,r); 
      outputTree(L);

    } while ( q >= 0 );

#ifdef NOOUT
   cout << "Counted " << globalNum << " of them\n";
#endif
}
