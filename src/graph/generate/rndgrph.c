
/* 
 * Builds a set of random graphs for playing.
 */
#include <iostream>

#include "graph/graph.h"
#include "graph/stdgraph2.h"
#include "general/random.h"

//#define HH
//#define REG

main()
{
   vertNum n, num;

   cout << "What graph order? " << nl;
   cin >> n;
   cout << "How many graphs? " << nl;
   cin >> num;

#ifdef HH
   RandomInteger Rand(0xFFFFFFF); // why doesn't 0xFFFFFFFF work?
#else
   RandomInteger Rand(n);
#endif

   DegreeSequence deg(n);

   for (int k=0; k<num; k++)
    {
     Graph G(n);
 
     vertNum i,j;
     
#ifdef HH

     for (i=1; i<n; i++)
      {

       vertNum steps =  Rand() % (i+1);
//cerr << "\ni: " << i << " steps: " << steps << nl;
       //
       while (steps)
        {
         j = Rand() % i;
//cerr << " j: " << j;

         if (G.isEdge(i,j)==false)
          {
            G.addEdge(i,j);
            steps--;
          }
        }
      }

#else
#ifdef REG

     vertNum reg = n/4 + Rand()/2;
     //
     if (n % 2 == 1 && reg % 2 == 1) reg--;
     //
     //cerr << "reg: " << reg << nl;
     //
     deg.fill(0);

     vertNum oldi=0, oldj=0;
     vertNum oldI=0, oldJ=0;
 
     vertNum steps = (n*reg)/2;
     //
     while (steps)
      {
       i = Rand();
       //
       while (deg[i]==reg) i = Rand();

       bool another=false;
       //
       for (int v=0; v<n; v++) if (v!=i && deg[v]<reg) another=true;
       //
       if (!another) break;

       j = Rand();
       //
       while (deg[j]==reg || i == j) j = Rand();

       if (oldi==i && oldj==j && oldI==i && oldJ==j) break;
       else
        {
           oldI=oldi;
           oldJ=oldj;
           oldi=i;
           oldj=j;
        }

       //cerr << "Try edge: " << i  << ' ' << j << nl;

       if (G.isEdge(i,j)==false)
        {
          G.addEdge(i,j);
          steps--;
          deg[i]++;
          deg[j]++;
        }
      }

     //if (steps!=0) { k--; continue; } // try again

#else

#if 0
     vertNum steps = n*(n-1)/4 + (n/2-Rand());
     //
     //cout << "steps: " << steps << nl;

     while (steps)
      {
       i = Rand();
       j = Rand();

       while (i == j) j = Rand();

       if (G.isEdge(i,j)==false)
        {
          G.addEdge(i,j);
          steps--;
        }
      }
#else
     
     vertNum q = n/4 + Rand()/2 + 1;
     vertNum p = (q+2)/4 + Rand() % (q/2);

//cerr << "\n(p,q)=" << p << "," << q << nl;

     for (i=0; i<n-1; i++)
      {
       for (j=i+1; j<n; j++)
         if ( Rand() % q >= p )
          {
            G.addEdge(i,j);
          }
      }

#endif

#endif
#endif

     cout << G;

#ifdef DEGREESEQ
     G.degVec(deg, 0);
     cout << deg << nl; 
#endif
    }
}
