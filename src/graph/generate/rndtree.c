
/* 
 * Builds a set of random trees for playing.
 */
#include <iostream>

#include "graph/tree.h"
#include "graph/i-o/psspec.h"
#include "graph/stdgraph2.h"
#include "general/random.h"
#include "general/str.h"

//void tree2ps( const Tree &T, const PSSpec *spec );

Str nodestrfnc(vertNum i, const void *T = 0 )
 {
  return Str(i);
 }

PSSpec::PSNodeType nodetypefnc(vertNum i, const void *T = 0)
 {
  return (PSSpec::PSNodeType) (i % 6);
 }

Str edgestrfnc(vertNum i, vertNum j, const void *T = 0)
 {
  return Str(i)+","+Str(j);
 }

PSSpec::PSEdgeType edgetypefnc(vertNum i, vertNum j, const void *T = 0)
 {
  return (PSSpec::PSEdgeType) ((i + j) % 3);
 }


main()
{
   vertNum n, num;

   cout << "What tree order? " << nl;
   cin >> n;
   cout << "How many trees? " << nl;
   cin >> num;

   RandomInteger Rand(n*n);

   VertArray levels(n);

   PSSpec spec;
   //
   spec.setFormat(PSSpec::DviSpecial);
   //
   spec.setNodeStrFnc(nodestrfnc);
   spec.setNodeTypeFnc(nodetypefnc);
   spec.setEdgeStrFnc(edgestrfnc);
   spec.setEdgeTypeFnc(edgetypefnc);

   spec.setWidth(500);	// 7.75 * 10.25
   spec.setHeight(650);
   //spec.setWidth(100);	
   //spec.setHeight(100);

   //spec.setOut(cerr);

   for (int k=0; k<num; k++)
    {
     vertNum maxLevel = 0;
     levels[0]=0;

     for (int i=1; i<n; i++)
      {
       levels[i] = (Rand() % (maxLevel+1)) + 1;		// no more roots
       maxLevel = levels[i];			   	// force child	
      }

     //cout << "Level Sequence: " << levels << nl;

     Tree T(levels);
     //cout << T;

     tree2ps(T,&spec);
     cout << "\\hspace{1em}" << nl;
    }
}
