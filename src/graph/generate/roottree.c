/* 
 * Generates all rooted trees of a particular order.
 *
 * (See SIAM J. Comput. Vol 9, No. 4, 1980; Beyer & Hedetniemi)
 *
 */

#include <iostream>

#include "assert.h"
#include "graph/tree.h"
#include "graph/stdgraph2.h"

//#define DOPS

#ifdef DOPS
#include "graph/psspec1.h"

void tree2ps( const Tree &T, const PSSpec *spec );

const VertArray *DOPS_levels;
//
Str nodestrfnc(vertNum i, const void *T = 0)
 {
  return Str(DOPS_levels->get(i));
 }

PSSpec::PSNodeType nodetypefnc(vertNum i, const void *T = 0)
 {
  return PSSpec::BlackCircle;
 }

PSSpec spec(100,100,PSSpec::DviSpecial); 

#endif

void outputTree(const VertArray &L)
 {
  static num = 0;

#ifdef DOPS
	  Tree T(L);
	  //
	  DOPS_levels = &L;
	  //
	  tree2ps(T,&spec);
#else
	  //cerr << "Tree " << L << " has rank " << num++ << nl;
	  cout << L << ' ' << num++ << nl;
#endif
 }

void main()
{
   vertNum n;

   cout << "What tree order? "; cout.flush();
   cin >> n; cout << nl;

   VertArray L(n);		// contain level sequences of 0..n-1
   VertArray prev(n-1);		// parent indices of nodes
   VertArray save(n-1);		// temporary space for updating prev[]'s

#ifdef DOPS
   spec.setNodeStrFnc(nodestrfnc);
   spec.setNodeTypeFnc(nodetypefnc);
#endif

   /*
    * Build lex largest tree.
    */
   for (int i=0; i<n-1; i++) 
    {
     L[i]=i;				// this tree is a path
     prev[i]=i;				// parent of L[i] is prev[L[i]-1]
    } 
   //
   L[i] = i;
   //
   vertNum p = n-1;

   outputTree(L);
   //cerr << "p=" << p << " q=" << prev[L[p]-1] << nl;

   if ( n <= 2 ) return;
   //
   while ( p > 0 )
    {
      L[p] -= 1;				// set to same level as parent

      if ( p < n-1 && (L[p] != 1 || L[p-1] != 1) )
       {
         vertNum diff = p - prev[L[p]];	// size of repeat sequence

         while (p < n-1 )		// copy duplicate subsequences
          {
            save[p] = prev[L[p]];	// previous perent at this level
            prev[L[p]] = p;		// new local parent at this level
         
            p++;			// set next node's level
            L[p] = L[p-diff];
          }
       }

      while ( L[p] == 1 )		// update parent pointers
       {
         p--;
	 prev[L[p]] = save[p];
       }

      outputTree(L);
      //cerr << "p=" << p << " q=" << (p ? prev[L[p]-1] : 0) << nl;
    }
}
