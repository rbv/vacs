/* 
 * Reads in a list of level sequences and draws trees.
 */

#include <iostream>

#include "assert.h"
#include "graph/tree.h"
#include "graph/stdgraph2.h"

#include "graph/i-o/psspec.h"

//void tree2ps( const Tree &T, const PSSpec *spec );

const VertArray *DOPS_levels;
//
Str nodestrfnc(vertNum i, const void *T = 0)
 {
  return Str(DOPS_levels->get(i));
 }

PSSpec::PSNodeType nodetypefnc(vertNum i, const void *T = 0)
 {
  return PSSpec::BlackCircle;
 }

PSSpec spec(100,100,PSSpec::DviSpecial); 

void main()
 {
   vertNum num;
   vertNum n;

   //cout << "What tree order? "; cout.flush();
   cin >> n >> num;	// order of tree and number of them.

   cout << n << ' ' << num << nl;
   VertArray L(n);		// contain level sequences of 0..n-1

   spec.setNodeStrFnc(nodestrfnc);
   //
   spec.setNodeTypeFnc(nodetypefnc);

   vertNum rank;
   //
   while (num--)
    {
     cin >> L >> rank;
     // 
     Tree T(L);
     //
     DOPS_levels = &L;
     //
     cout << "% Tree " << L << " with rank " << rank << nl;
     //
     tree2ps(T,&spec);
    }
 }
