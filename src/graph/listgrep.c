
/***************************************************************************
 * Graph adjacency lists represention source.
 ***************************************************************************/

/*tex

\file{listgrep.c}
\path{src/graph}
\title{Standard graph adjacency lists representation}
\classes{ AdjListsGRep, ROAdjListsGRep }
\makeheader

xet*/

#include "graph/listgrep.h"
#include "general/stream.h"

/*
 * Construct graph initialized via another graph.
 */
AdjListsGRep::AdjListsGRep(const AdjListsGRep &G, vertNum extra)
 {
  assert( extra >= 0);

  _adj = G._adj;
  _adj.resize(G.order()+extra);
 }

/*****************************************************************************/

/*
 * Removes a vertex from an AdjLists.
 */
void AdjListsGRep::rmNode( vertNum v )
 {
  vertNum n = order();

  assert( v>=0 && v < n );

  for (int i=0; i<n; i++)
   {
    vertNum neighbor = _adj[v][i];

    _adj[neighbor].swap( _adj[neighbor].index(v), _adj[neighbor].size()-1 );
    _adj[neighbor].shrink(1);
   }

  _adj.swap( v, n-1 );

  _adj.shrink(1);
 }

void AdjListsGRep::addEdge( vertNum u, vertNum v ) 
 {
  _adj[u].append(v);
  _adj[v].append(u);
 }

void AdjListsGRep::rmEdge( vertNum u, vertNum v ) 
 {
  _adj[u].swap( _adj[u].index(v), _adj[u].size()-1 );
  _adj[u].shrink(1);
  _adj[v].swap( _adj[v].index(u), _adj[v].size()-1 );
  _adj[v].shrink(1);
 }

bool AdjListsGRep::isEdge( vertNum u, vertNum v ) const
 {
  vertNum n = _adj.size();

  assert(0<=u && u<n);
  assert(0<=v && v<n);
  assert(u != v);

  for (int i=0; i<_adj[u].size(); i++ ) if ( _adj[u][i] == v ) return true;

  return false;
 }

void AdjListsGRep::contract( vertNum u, vertNum v ) 
 {
  assert( isEdge(u,v ) );

  vertNum n = _adj.size();

  /*
   * Make everything adjacent to v now adjacent to u.
   */
  int i; for (i=0; i < n; i++)
   {
    if (i==u || i==v) continue;

    for (int j=0; j < _adj[i].size(); j++)
      if ( _adj[i][j]==v ) _adj[i][j]=u;
   }

  /*
   * Now expand u's adjacency list.
   */
  int uIdx = _adj[u].size();

  _adj[u].extend(_adj[v].size()-2);

  for (i=0; i<_adj[v].size()-2; i++)
   {
    vertNum neighbor = _adj[v][i];

    if (neighbor != u) _adj[u][uIdx++]=neighbor;
   }

  vertNum neighbor1 = _adj[v][i];
  vertNum neighbor2 = _adj[v][i+1];
  //
  if (neighbor1 == u)
   {
    _adj[u][_adj[u].index(v)]=neighbor2;
   }
  else if (neighbor2 == u)
   {

    _adj[u][_adj[u].index(v)]=neighbor1;
   }
  else
   {
    _adj[u][_adj[u].index(v)]=neighbor1;
    _adj[u][uIdx++]=neighbor2;
   }

   if ( v < _adj.size()-1 )
    {
     /*
      * Relabel references to last node as new v node.
      */
     for (int i = 0; i<_adj[n-1].size(); i++)
      {
       vertNum neighbor = _adj[n-1][i];

       if ( neighbor == v ) neighbor = u;

       for (int j=0; j<_adj[neighbor].size(); j++)
         if ( _adj[neighbor][j] == n-1 )
           {
              _adj[neighbor][j] = v;
              break;
           }
      }

     _adj.swap( v, n-1 );

    }
   //
   _adj.shrink(1);

 }


/*****************************************************************************/
// READ-ONLY adjacency list graph
/*****************************************************************************/

/*
 * Construct graph initialized via another graph.
 */
ROAdjListsGRep::ROAdjListsGRep(const ROAdjListsGRep &G)
 {
  _order = G.order();

  if (_order == 0) return;

  _sdeg = new edgeNum[_order];
  //
  _adj = new vertNum[G.size()*2];

  int i; for (i=0; i < _order; i++) _sdeg[i] = G._sdeg[i];

  for (i=0; i< (int) G.size()*2; i++) _adj[i] = G._adj[i];
 
  return;
 }

/*
 * Check for edge.
 */
bool ROAdjListsGRep::isEdge(vertNum i, vertNum j) const
 {
  assert(0<=i && i<order());
  assert(0<=j && j<order());
  
  int idx = 0;
  if (i>0) idx = _sdeg[i-1];

  while (idx< (int) _sdeg[i]) if (_adj[idx++]==j) return true;

  return false;
 }

