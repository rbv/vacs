
/***************************************************************************
 * MatrixGRep matrix represention source.
 ***************************************************************************/

/*tex

\file{matgrep.c}
\path{src/graph}
\title{Standard graph matrix representation}
\classes{ MatrixGRep, ROMatrixGRep }
\makeheader

xet*/

#include "graph/matgrep.h"
#include "graph/ggraph.d"

#include <iostream>

/*****************************************************************************/

GRepSize(MatrixGRep)
GRepDegree(MatrixGRep)
GRepNeighbor(MatrixGRep)
GRepContract(MatrixGRep)

/*
 * internal memory management routines.
 */
bool** MatrixGRep::_allocate(vertNum n)
 {
  bool **a = new bool*[n];

  /*
   * Allocate the rest in one step.
   */
  a[0] = new bool[n*n];

  for ( int i=1; i<n; i++ ) 
     a[i] = a[0] + i*n;

  return a;
 } 
//
void MatrixGRep::_deallocate(bool **a)
 {
   delete [] a[0];
   delete [] a;
 }

/**************************************************************************/

/*
 * Construct empty graph.
 */
MatrixGRep::MatrixGRep(vertNum n)
 {
   assert( n>=0 );

  _order = _space =  n;
  
  if (_order == 0) return;

  _adj = _allocate( _order );

  for (int i=0; i<n; i++)
    for (int j=0; j<n; j++) _adj[i][j] = 0;

 }

/*
 * Construct graph initialized via another graph.
 */
MatrixGRep::MatrixGRep(const MatrixGRep &G, vertNum extra)
 {
  assert( extra >= 0);

  _order = _space =  G._order + extra;

  if (_order == 0) return;

  _adj = _allocate( _order );

  int i; for (i=0; i < G._order; i++)
   {
    for (int j=0; j < G._order; j++) 
        _adj[i][j] = G._adj[i][j];

    for (int j2 = G._order; j2 < _order; j2++) _adj[i][j2] = 0;
   }

  for (i = G._order; i < _order; i++)
   {
    for (int j2 = 0; j2 < _order; j2++) _adj[i][j2] = 0;
   }

  return;
 }

/************************************************************************/

/*
 * Destructor for graphs.
 */
MatrixGRep::~MatrixGRep()
  {
   if (_space) _deallocate(_adj);
  }

/*****************************************************************************/

istream& operator>>( istream& in, MatrixGRep& M )
 {
   /*
    * Currently can read only adjacency matrix with order as first line.
    */
   int n;
   in >> n;

   assert( n>=0 );

   if ( n > M._space )
    {
     if ( M._space > 0 )  MatrixGRep::_deallocate( M._adj );
  
     M._order = M._space = n;
     if ( n>0 ) M._adj = MatrixGRep::_allocate( n ); 
    } 
   else 
    {
     M._order = n;
    }

   int entry;
   for (int i=0; i<n; i++)
    {
     for (int j=0; j<n; j++) 
      {
	in >> entry;

        M._adj[i][j] = entry ? 1 : 0;
      }
    }

   return in; 
 }

/*****************************************************************************/

/*
 * Add verticies to graph.
 */
void MatrixGRep::addNode(vertNum n)
 {
  assert( n>0 );

  if ( n > _space - _order )
   {
    bool **matrix = _allocate( _order + n );

    for (int i=0; i<_order; i++)
     {
      for (int j=0; j<_order; j++) matrix[i][j] = _adj[i][j];
     }

    if ( _space ) _deallocate(_adj);

    _adj = matrix;
    _space = _order + n;
   }

  /*
   * Now initialize new nodes with no edges.
   */
  for (int i=_order; i<_order+n; i++)
    for (int j=0; j<_order+n; j++)  _adj[j][i] = _adj[i][j] = 0;

  _order += n;

  return;
 }

/*****************************************************************************/

/*
 * Remove a vertex from the graph.
 */
void MatrixGRep::rmNode(vertNum v)
 {
  assert( v>=0 && v<order() );

  _order--;

  for (int i=v; i<_order; i++)
   {
     int j; for  (j=0; j<v; j++)
      {
       _adj[i][j] = _adj[i+1][j];
       _adj[j][i] = _adj[j][i+1];
      }

     for  (j=v; j<_order; j++)
      {
       _adj[i][j] = _adj[i+1][j+1];
      }
   }

  return;
 }

/*****************************************************************************/
// READ-ONLY matrix graph
/*****************************************************************************/

GRepSize(ROMatrixGRep)
GRepDegree(ROMatrixGRep)
GRepNeighbor(ROMatrixGRep)

// compute memory for upper trianglar adj matrix.
//
static int RO_space(int n) 
 {
  return n*(n-1)/2;
 }
//
static int RO_index(int i, int j, int n) // lazy!!!
 {
   assert(i<j);
   
   if (i==0) return j;
   else return n-1 + RO_index(i-1, j-1, n-1);
 }

/*
 * Construct graph initialized via another graph.
 */
ROMatrixGRep::ROMatrixGRep(const ROMatrixGRep &G)
 {
  _order = G.order();

  if (_order == 0) return;

  int space = RO_space(_order);
  //
  _adj = new bool[space];

  for (int i=0; i < space; i++)
   {
     _adj[i] = G._adj[i];
   }

  return;
 }

/*
 * Destructor.
 */
ROMatrixGRep::~ROMatrixGRep()
 {
  if (_order) delete [] _adj;
 }

/*
 * Check for edge.
 */
bool ROMatrixGRep::isEdge(vertNum i, vertNum j) const
 {
  vertNum u, v; 

  assert(0<=i && i<order());
  assert(0<=j && j<order());

  if (i<j) { u=i; v=j; }
  else     { u=j; v=i; }

  return _adj[RO_index(u,v,order())];
 }

