
// -------------------------------------------
// ---------------- bpggrph.c ---------------
// -------------------------------------------

/*tex
 
\file{bpggrph.c}
\path{src/bpg}
\title{Definitions of BPG ``graph'' structures}
\classes{}
\makeheader
 
xet*/

#include <iostream>

#include "bpg/bpg.h"
#include "graph/stdgraph2.h"

#include "bpg/bpggrph.h"

// At the expense of more source code (but less dynamic memory at
// run time) use the following define for distanceMatrix.
//
#define INPLACE_DIST

//--------------------------------------------------------------


// The boundary vertices end up in the initial positions
//  (0..bndrysize-1), in the appropriate places.
//
void adjacencyMatrix( const BPG &G, AdjMatrix &A )
{
   assert( G.boundaryType() == BPG::noBoundary || 
           G.boundaryType() == BPG::rightBoundary );

   int bs = G.boundarySize();
   int n = G.vertices();

   // check the size of the matrix
   //
   assert( A.dim() == n );

   // Compute a permutation of the vertices so that boundary comes first.
   //
   vertNum seen = 0;
   vertNum nextFree = ( G.boundaryType() == BPG::noBoundary ) ? 0 : bs;
   //
   VertArray map(n);
   //
   int i; for (i=0; i<G.size(); i++)
    {
       Operator op = G[ i ];
       //
       if ( op.isVertexOp() )
         {
           if ( G.isBoundaryVertex(i) ) map[seen++] = op.vertex();
           else  			map[seen++] = nextFree++; 
         }
    }
   //
   assert( nextFree == n && seen == n );

   // create a incidence-matrix-like thing

   // init to all false
   //
   A.fill( false );

   // current boundary pointers into adj matrix.
   //
   VertArray pos( bs );

   for ( i = seen = 0; i < G.size(); i++ )  // reset for second pass.
   {
      Operator op = G[ i ];

      if ( op.isVertexOp() )
      {
         pos[ op.vertex() ] = seen++;
      }

      if ( op.isEdgeOp() )
      {
         int r = map[ pos[ op.vertex1() ] ];
         int c = map[ pos[ op.vertex2() ] ];
         //
         A.put( r, c, true );
         A.put( c, r, true );
      }

   } // for i

}

//--------------------------------------------------------------

#ifdef INPLACE_DIST

// The boundary vertices end up in the initial positions
//  (0..bndrysize-1), in the appropriate places.
//
static void FloydSetup( const BPG &G, DistanceMatrix &D, vertNum n )
{
   assert( G.boundaryType() == BPG::noBoundary || 
           G.boundaryType() == BPG::rightBoundary ||
           G.boundaryType() == BPG::leftBoundary );

   int bs = G.boundarySize();

//cerr << "Floyd: " << G << nl;

   //
   // Compute a permutation of the vertices so that boundary comes first.
   //
   vertNum seen = 0;
   vertNum nextFree = ( G.boundaryType() == BPG::noBoundary ) ? 0 : bs;
   //
   VertArray map(n);
   //
   int i; for (i=0; i<G.size(); i++)
    {
       Operator op = G[ i ];
       //
       if ( op.isVertexOp() )
         {
           if ( G.isBoundaryVertex(i) ) map[seen++] = op.vertex();
           else  			map[seen++] = nextFree++; 
         }
    }
   //
   assert( nextFree == n && seen == n );

   // init all to "infinity"
   //
   D.fill( n );

   VertArray pos( bs );

   for ( i = seen = 0; i < G.size(); i++ )  // reset for second pass.
   {
      Operator op = G[ i ];

      if ( op.isVertexOp() )
      {
         D.put( seen, seen, 0 );  	// zero distance to itself
         pos[ op.vertex() ] = seen++;
      }

      if ( op.isEdgeOp() )
      {
         int r = map[ pos[ op.vertex1() ] ];
         int c = map[ pos[ op.vertex2() ] ];
         //
         D.put( r, c, 1 );
         D.put( c, r, 1 );
      }

   } // for i

//cerr << "Mat: " << D << nl;
}

#endif

// The boundary vertices end up in the initial positions
//  (0..bndrysize-1), in the appropriate places.
//
void distanceMatrix( const BPG &G, DistanceMatrix &D )
{
   int order = G.vertices();

   D.setDim( order );

   int i,j,k;

#ifdef INPLACE_DIST

   FloydSetup( G, D, order );

#else

   // Use the vertex permutation given by the adjacency matrix computation.
   //
   AdjMatrix A(order);
   //
   adjacencyMatrix( G, A );

   // Fill in initial distances known.  (for Floyd's algorithm)
   //
   for (i=0; i<order; i++)              
    {
     for (j=0; j<order; j++)
       if (A.at(i,j)==true) 	D.put(i,j,1); 
       else 			D.put(i,j,order);
    }
   //
   for (i=0; i<order; i++) D.put(i,i,0);

#endif

   // Iterate over all distances.
   //
   for (k=0; k<order; k++)
    for (i=0; i<order; i++)
     for (j=0; j<order; j++)
       if ( D.at(i,j) > D.at(i,k) + D.at(k,j) ) 
        {
          D(i,j) = D.at(i,k) + D.at(k,j);
        }

   //cerr << "distance matrix of " << G;
   //if ( G.boundaryType() == BPG::rightBoundary ) cerr << " RBBG";
   //cerr << D << nl;

   return;
}

//--------------------------------------------------------------
 
// places boundary at first bs() positions.
//
void degreeSequence( 
   const BPG& G, 
   DegreeSequence& D, 
   IsomorphismBoundaryType boundaryType
)
{

   int bs = G.boundarySize();
   int vOps = G.vertices();

   D.resize( vOps );

   int nextFree = bs;
   int i;

   // I'm tired and lazy...let's just make sure it's a plain RBBPG
   //
   assert( G.boundaryType() == BPG::rightBoundary );

   Array<bool> seen( bs );
   seen.fill( false );

   // skip the boundary (see lazy note above)
   //
   int sz = G.size();
   for ( i=0; i<sz; i++ )
   {
      Operator op = G[ i ];

      if ( op.isVertexOp() )
      {
         vertNum v = op.vertex();

         if ( seen[ v ] )
            D[ nextFree++ ] = D[ v ];
         else
            seen[ v ] = true;

         D[ v ] = 0;
      }

      if ( op.isEdgeOp() )
      {
         D[ op.vertex1() ]++;
         D[ op.vertex2() ]++;
      }

   }

   assert( nextFree == vOps );

   switch ( boundaryType )
   {
      case noBoundary: 
         D.sortUp();
         break;

      case freeBoundary: 
         D.sortUp( 0, bs-1 ); 
         if ( vOps > bs )
            D.sortUp( bs, vOps-1 );	// mjd change July 93
         break;

      case fixedBoundary: 
         if ( vOps > bs )
            D.sortUp( bs, vOps-1 );
         break;
   }

   return;
}

//-------------------------------------------------------------------------

// Compute the distance matrix and cache it.
//
void BPG::distanceMatrix( DistanceMatrix& D ) const
{
   ::distanceMatrix( *this, D );
}

// Compute the vertex partition and cache it.
//
void BPG::vertPartition( VertPartition& /* vp */ ) const
{
aassert(false);
//cerr << "computeVertPartition: vertices()=" << vertices() << nl;
//cerr << "*distanceMatrix() = " << *distanceMatrix() << nl;

   //((BPG*)this)->_vertPartition =
      //new VertPartition( vertices(), *distanceMatrix() );

}

//-------------------------------------------------------------------------
