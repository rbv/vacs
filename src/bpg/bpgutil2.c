
// -------------------------------------------
// ---------------- bpgutil2.c ---------------
// -------------------------------------------

/*tex
 
\file{bpgutil2.c}
\path{src/bpg}
\title{Definitions of bounded pathwidth graph utility functions}
\classes{}
\makeheader
 
xet*/

#include "bpg/bpg.h"
#include "family/family.h"
#include "general/random.h"
#include "graph/stdgraph2.h"
#include "general/str.h"

#include "bpg/bpgutil2.h"
#include "bpg/bpgutil.h"

#include <iostream>


//--------------------------------------------------------------

#if 0
/*
 * See bpggrph.c for degSeq/distMat/adjMat
 */

// The boundary vertices end up in the initial positions
//  (0..bndrysize-1), in the appropriate places.

void distanceMatrix( const BPG &G, DistanceMatrix &D )
{
   //cerr << "--- " << G << nl;

   int bs = G.boundarySize();
   int vert = G.vertices();

   // check the size of the matrix
   //
   assert( D.dim() == vert );

   // create a incidence-matrix-like thing

   // init to all infinity
   //
   D.fill( vert );

   // process the graph right-to-left, so the boundary
   // can be handled easily

   int nextFree = 0;

   Array<int> pos( bs );

   if ( G.boundaryType() == BPG::rightBoundary )
   {
      for ( int i = 0; i < bs; i++ )
      {
         pos[ i ] = nextFree++;
      }

      for ( i = G.size()-1; i >= 0; i-- )
      {
         Operator op = G[ i ];

         if ( op.isVertexOp() )
         {
            // hack to fix - make sure this is not the leftmost vertex op
            bool leftMost=true;
            for ( int j=i-1; j>=0; j-- )
               if ( G[j] == op ) leftMost = false;
         
            if ( ! leftMost )
            {
               pos[ op.vertex() ] = nextFree++;
            }
         }

         if ( op.isEdgeOp() )
         {
            int r = pos[ op.vertex1() ];
            int c = pos[ op.vertex2() ];
            //cerr << "--- " << r << ' ' << c << nl;
            D.put( r, c, 1 );
            D.put( c, r, 1 );
         }

      } // for i

   }   // if bType
   else if ( G.boundaryType() == BPG::noBoundary )
   {
      for ( int i = 0; i < G.size(); i++ )
      {
         Operator op = G[ i ];

         if ( op.isVertexOp() )
         {
            pos[ op.vertex() ] = nextFree++;
         }

         if ( op.isEdgeOp() )
         {
            int r = pos[ op.vertex1() ];
            int c = pos[ op.vertex2() ];
            D.put( r, c, 1 );
            D.put( c, r, 1 );
         }

      } // for i
   }
   else
   {
      aassert( false ); // case not handled
   }


   // fill in diagonal with 0
   for ( int i = 0; i < vert; i++ )
      D.put( i, i, 0 );

   //cerr << "------\nDist matrix called" << nl << G << D << nl;
   assert( nextFree == vert );

}

#endif

//--------------------------------------------------------------

// returns true if a mulit-edge conflict occurrs.
//
bool updateBndryIncidenceMatrix( const BPG& G, BndryIncidenceMatrix& M, 
        int start, int end )
{
   int e = ( end != -1 ) ? end : G.length();

   assert( e >= 0 && e <= G.length() );
   assert( start < e );

   for ( int i = start; i < e; i++ )
   {
      Operator op = G[ i ];

      if ( op.isVertexOp() )
      {
         // clear row/col
         M.fillRow( op.vertex(), false );
         M.fillCol( op.vertex(), false );
      }

      if ( op.isEdgeOp() )
      {
         int r = op.vertex1();
         int c = op.vertex2();

         // check for multi edge
         if ( M.at( r, c ) ) return true;

         M.put( r, c, true );
         M.put( c, r, true );
      }

   } // for i

   return false;
}

//--------------------------------------------------------------


void reverseAndStrip( const BPG& G1, BPG& G2 )
{
   int bs = G1.boundarySize();
   int size = G1.size() - bs;

   Array<bool> seen( bs );
   seen.fill( false );

   G2.resize( size );

   int j = G2.size();

   for ( int i = 0; i < G1.size(); i++ )
   {
      Operator op = G1[i];

      if ( op.isVertexOp() )
         if ( ! seen[ op.vertex() ] )
         {
            seen[ op.vertex() ] = true;
            continue;
         }

      G2.put( --j, op );
   }

   assert( j == 0 );

}

//--------------------------------------------------------------

bool nextMinor( BPG& g, SearchMinor::ReductionType& type, int& pos, bool compute )
{

   if ( type == SearchMinor::DeleteVertex )
   {
      // isolated vertex deletions
      //
      while ( ++pos < g.size() )
      {
         if ( g.isIsoNonboundaryVertex( pos ) )
         {
            if ( compute ) g.deleteIsoVertex( pos );
            return true;
         }
      }
      // no more iso vertices!
      type = SearchMinor::DeleteEdge;
      pos = -1;
   }

   if ( type == SearchMinor::DeleteEdge )
   {
      // edge deletions
      //
      while ( ++pos < g.size() )
      {
         if ( g[pos].isEdgeOp() )
         {
            if ( compute ) g.deleteEdge( pos );
            return true;
         }
      }
      // no more edges!
      type = SearchMinor::ContractEdge;
      pos = -1;
   }

   if ( type == SearchMinor::ContractEdge )
   {
      // isolated vertex deletions
      //
      while ( ++pos < g.size() )
      {
         if ( g.isNonboundaryEdge( pos ) )
         {
            if ( compute ) g.contractEdge( pos );
            return true;
         }
      }
      // no more non-bndry edges!
      // All done.
   }

   return false;

}

//--------------------------------------------------------------

bool randomMinorSearch( const BPG& g )
{

   // make a virtual ctor call
   //
   BPG* minor = g.copy();

   int len = minor -> length();

   RandomInteger r( len );

   while ( true )
   {
      // check for empty graph (remember! maybe boundaried)
      //
      if ( minor->isEmpty() ) return false;

      // pick an operator
      int pos = r();

      // find next non-noOp
      while ( pos<len && (*minor)[pos].isNoOp() ) pos++;

      if ( pos==len )  // ran off end...go backards
      {
         while ( pos>=0 && (*minor)[pos].isNoOp() ) pos--;
         assert( pos == -1 );
      }

      // pos points to op to process

      if ( (*minor)[pos].isVertexOp() )
      {
         // Vertex op ... delete if it's isolated
         //
         if ( minor->isIsoNonboundaryVertex( pos ) )
            minor->deleteIsoVertex( pos );
         else
            continue;  // no change to minor
      }
      else
      {
         // edge op, delete or contract?
         //
         if ( r() % 2  ||  ! minor->isNonboundaryEdge( pos ) )
            minor->deleteEdge( pos );
         else
            minor->contractEdge( pos );
      }

      // check congruence
      //
      if ( checkCongruent( g, *minor ) ) return true;

   } // while

}


//--------------------------------------------------------------

bool checkCongruent( const BPG& g, const BPG& h )
{

   if ( !Family::congruenceAvailable() ) 
    {
      cerr << "congruence not available\n";
      return false;
    }

   Congruence* C1 = Family::newCongruence();
   Congruence* C2 = Family::newCongruence();

   C1->compute( g );
   C2->compute( h );

   bool result = *C1 == *C2;

   delete C1;
   delete C2;

   return result;

}

//--------------------------------------------------------------

// Do in-place minimization of an out-of-family graph
// (that is, find an obstruction which is a minor)
//
// Just do edge deletions and vertex deletions for now.
// Valid operations are int the range start <= i < end,
//    so pass 0, g.length() for whole graph.
//
// checkonly: return whether g is an obstruction; otherwise always return true
//
static bool minimizeMinors( BPG& g, int start, int end, bool checkOnly = false )
{

   // outer loop to make sure we've gotten everything...
   bool didOne = true;

   while ( didOne )
   {
      didOne = false;

      // Try contract each edge
      int i; for ( i=end-1; i>=start; i-- )
      {
         if ( ! g.isNonboundaryEdge(i) ) continue;

         BPG* minor = g.copy();
         minor->contractEdge( i );
   
         if ( ! Family::member( *minor ) )
         {
            if ( checkOnly ) { delete minor; return false; }
            g.update( *minor );
            didOne = true;
         }

         delete minor;
      }

      // Delete each edge
      for ( i=end-1; i>=start; i-- )
      {
         if ( ! g[i].isEdgeOp() ) continue;

         BPG* minor = g.copy();
         minor->deleteEdge( i );
   
         if ( ! Family::member( *minor ) )
         {
            if ( checkOnly ) { delete minor; return false; }
            g.update( *minor );
            didOne = true;
         }

         delete minor;
      }

   } // while


   // Delete isolated vertices
   for ( int i=end-1; i>=start; i-- )
   {
      if ( g.isIsoNonboundaryVertex(i) )
      {
         BPG* minor = g.copy();
         minor->deleteIsoVertex( i );

         if ( ! Family::member( *minor ) )
         {
            if ( checkOnly ) { delete minor; return false; }
            g.update( *minor );
         }

         delete minor;
            
      }
   }

   return true;
}


bool isObstruction( BPG& g )
{
   return minimizeMinors( g, 0, g.length(), true );
}


void obstMinimize( BPG& g )
{
   minimizeMinors( g, 0, g.length() );
}


void minimizeRHS( CBBPG& g )
{
   minimizeMinors( g, g.center(), g.length() );
}


//--------------------------------------------------------------

#if 0

// Not completed yet
//
void computeVertexLadder( const BPG& g, Array<int> ladder, int& maxDepth )
{

   int bs = g.boundarySize();

   ladder.resize( g.vertices() );

   SortableArray<int> frontDepth( bs );
   SortableArray<int> backDepth( bs );

   for ( int i=0; i<bs; i++ )
   {

      // find the depth to boundary
      //
      int t = 0;

      for ( int j=0; j<g.size() && !g.isBoundaryVertex(j); j++ )
         if ( g[j].isVertexOp() && g[j].vertex()==i ) t++;

      frontDepth[i] = t;

      // find depth from boundary to end
      //

      t = 0;

      for ( j++; j<g.size(); j++ )
         if ( g[j].isVertexOp() && g[j].vertex()==i ) t++;

      backDepth[i] = t;
   }

   // Position of boundary in the ladder
   //
   int bPos = frontDepth.max();

   // total depth of the ladder
   //
   maxDepth = frontDepth.max() + backDepth.max();

   // build the ladder

   for ( int j = 0; j < g.size(); j++ )
   {
      Operator op = g[i];

      if ( ! op.isVertexOp() ) continue;


   }

}

#endif

#if 0

/*
 * Random RBBPG generator -- uses "fromString( Str, BPG& )
 */
void randomBPG( int n, BPG &g )
{
   static char *bOps = "0123456789abcdef";

   int bs = Operator::boundarySize();

   int totalOps = bs*bs;  	// heavy on the duplicate edge ops.

   RandomInteger r( totalOps );

   Str s( Str::withLength( 3*n ) );

   for (int i=0; i<n; i++)
    {
      int opIndex = r();
 
      if ( opIndex < bs )
       {
         s[3*i]=bOps[opIndex];
         s[3*i+1]=',';
         s[3*i+2]=' ';
       }
      else
       {
         opIndex -= bs;
         s[3*i]=bOps[ opIndex/bs ];
         s[3*i+1]=bOps[ opIndex % bs ];
         s[3*i+2]=',';

         if ( s[3*i]==s[3*i+1] ) i--;		// take care of non-edges.
       }
    }

   if ( s[3*i-1] == ',' ) s[3*i-1]=' ';
   else s[3*i-2] = ' ';

   RBBPG::fromString( s, g );

cerr << "before " << g;

   g.removeMultipleEdges();
}

#else

/*
 * Random RBBPG generator -- doesn't use "fromString( Str, BPG& )
 */
void randomBPG( int n, BPG &g, int percentEdges )
{
   int bs = Operator::boundarySize();

   // Use an array of flags to make sure that edges have 
   // vertices before them.
   //
   Array<bool> vFlag( bs );

   if ( g.boundaryType() == BPG::noBoundary )
      vFlag.fill( false );
   else
      vFlag.fill( true );

   /*
    * Equal distibrution of operators if no edge percent given.
    */
   if ( percentEdges == -1 )
    {
      percentEdges = 100*bs/(2+bs);
    }

   RandomInteger type(100);

   RandomInteger r( bs );

   // Virtual call to init boundary
   //
   g.setEmpty();

   int start = g.length();
   int end = n + g.length();

   g.resize( end );

   for (int i=start; i<end; )
   {
      if ( type() >= percentEdges )
      {
         // vertex op
         //
         Operator op = Operator::vertexOperatorReturnFailure( r() );
         if ( ! op.isNoOp() )
            g.setOp( i++, op );

         vFlag[ op.vertex() ] = true;
      }
      else
      {
         // edge op

         Operator op = Operator::edgeOperatorReturnFailure( r(), r() );

         if ( op.isNoOp() ) continue;

         // make sure vertices exist
         if ( ! vFlag[ op.vertex1() ] ) continue;
         if ( ! vFlag[ op.vertex2() ] ) continue;

         g.setOp( i++, op);
      }
    }

//cerr << "before " << g;

   assert( g.isValid() );

   g.removeMultipleEdges();
   //
   g.removeIsolatedVertices();
   //
   g.compress();
}

#endif

//-------------------------------------------------------------------------

bool QCanonicCheck( const RBBPG& G )
{
   //cerr << "Q Canonic check: " << G << ' ';

   int sz = G.length();

   Operator z1 = G.getOp( sz-1 );
   Operator z2 = G.getOp( sz-2 );

   return QCanonicCheck( z1, z2 );
}

// Is z1 allowed to follow z2 in a canonic t-parse?
// These quick canonic tests are valid whether using <_c or lexicographic
// ordering to define t-parse canonicity
bool QCanonicCheck( Operator z1, Operator z2 )
{

   if ( z1.isEdgeOp() )
   {
      // make sure edges are in order
      if ( z2.isEdgeOp() && z2 > z1 )
      {
         //cerr << "QCC: " << G << " no; edge order" << nl;
         return false;
      }
   }

   if ( z1.isVertexOp() )
   {
      // make sure vertex can't move left one pos
      if ( z2.isEdgeOp() && 
           z2.vertex1() != z1.vertex() && z2.vertex2() != z1.vertex() )
      {
         //cerr << "QCC: " << G << " no; v-e shift" << nl;
         return false;
      }

      if ( z2.isVertexOp() && z2 > z1 )
      {
         //cerr << "QCC: " << G << " no; v-v shift" << nl;
         return false;
      }
   }

   return true;
}

// Assuming parentGraph is quick-canonic, is it still quick-canonic when z1 is appended?
bool QCanonicCheck( Operator z1, const RBBPG& parentGraph )
{
   int sz = parentGraph.length();

   if ( !QCanonicCheck( z1, parentGraph[sz - 1] ) )
      return false;

   if ( !ProbInfo::getBool( ProbInfo::LexicographicCanonicity ) )
   {
      // Only valid for <_c
      if ( z1.isVertexOp() )
      {
         // Check whether an earlier edge op should come after this vertex op
         // (Note that this never triggers for a,0-apex-pathwidth search)
      
         vertNum v = z1.vertex();
         for ( int i = sz-1; ; i-- )
         {
            Operator op = parentGraph.getOp(i);
            if ( op.isVertexOp() ) break;
            if ( op.vertex1() != v && op.vertex2() != v )
            {
               //cerr << "no; wrong edges" << nl;
               return false;
            }
         }
      }
   }

   // oh well, call brute force
   //
   //return isCanonic( G );

   return true;

}


#if 0
bool isCanonic( const RBBPG& G )
{

   //cerr << "brute force... ";

   // lasy for now...always call brute force alg

   bool can = RBBPG::isCanonic( G );

   //cerr << ( can ? "canonic" : "non canonic" ) << nl;

   return can;

#if 0
   int l = G.length();
   int bs = G.boundarySize();

   assert(bs<=l);

   // verify initial bndry
   //
   for ( int i=0; i<bs; i++ )
      assert( ( ! G.get(i).isVertexOp() ) || G.get(i).vertex() != i );
      
   // Keep track of consecutive vertices (should increase in order)
   //
   int lastVop = G.get(bs-1).vertex();

   if (i==l) return false; 	// okay and done

   // verify initial edge order
   //
   bool lastWasEdge = false;
   // 
   Operator op;
   //
   do 
   {
      op = G.get(i);

      assert(op.isNoOp() == false);

      if ( op.isVertexOp() )
       {
         lastWasEdge = false;
         if (op.vertex() < lastVop) return true; 
         else 			    lastVop = op.vertex();
         break;
       } 

      // Must be an edge operator.
      // (invalidate consecutive test)
      //
      lastVop = -1;

      // check for edges out of order
      //
      if ( lastWasEdge==true && op < G.get(i-1) ) return true; 
       
      lastWasEdge = true;

   } while( ++i < l );
   
   if (i==l) return false; 	// okay and done #2

   // else continue with standard processing
   //
   vertNum currentV = op.vertex();
   //
   while ( ++i < l ) 
   {
      op = G.get(i);

      if ( op.isVertexOp() )
       {
         currentV = op.vertex();
         //if (currentV < lastVop) return true; 
         //else 	    	         lastVop = currentV;
         lastWasEdge = false;
       }
      else if ( op.isEdgeOp() )
       {
         // check for inappropiate edge.
         //
         if ( currentV != op.vertex1() && currentV != op.vertex2() )
            return true; 

         // check for edges out of order
         //
         if ( lastWasEdge == true && op < G.get(i-1) ) return true; 

         lastWasEdge = true;
         lastVop = -1; 		// Okay, we'll let any vertex op slide by
       }
      else // NoOp
       {
         assert(false);
       }
   } 

   return false;		// okay and done #3
#endif
}
#endif

//-------------------------------------------------------------------------
