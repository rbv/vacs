/*
 * See if BPG is acyclic.  (multi-graph version)
 */

/*tex
 
\file{acyclic_b.c}
\path{src/bpg/algorithm}
\title{Function to test if a BPG is acyclic}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "bpg/bpg.h"
#include "array/array.h"
//OLD: #include "general/queue.h"

bool acyclic(const BPG &B)
 {
  //OLD: Que<Vertex> eq_class;

  int bndry = B.boundarySize();

  Array<int> E_m( bndry );

  int i; for (i=0; i<bndry; i++) E_m[i]=i; 

  int eq_class = bndry-1; //NEW:

  for (int m=0; m < B.length(); m++)
   {
    Operator op = B[ m ]; // get the operator at position m

    if ( op.isVertexOp() )
        {
	  /*
           * All isolated boundary vertices should be in a distinct
	   * equivalence class. 
           */
         //OLD: if (eq_class.something()) E_m[ op.vertex() ] = eq_class.deque();

         E_m[ op.vertex() ] = ++eq_class; //NEW:
         
        }
    else if ( op.isEdgeOp() )
        {
          vertNum v1 = op.vertex1(),
                  v2 = op.vertex2();

	  /*
           * If boundary vertices are connected then we have a cycle.
           */
          if ( E_m[ v1 ] == E_m[ v2 ] ) 
           {
             return false;
           }

	  /*
           * Merge components into one equivalence class.
           */
          vertNum other=E_m[v2];
          //
          for (i=0; i<bndry; i++) 
           if (E_m[ i ] == other) E_m[ i ] = E_m[ v1 ];

          //OLD: eq_class.enque(E_m[ v2 ]);
        } // else
     } // for

  return true;

 }

