/*
 *  Compute canonic form of a BPG
 */

/*tex
 
\file{canocic_b.c}
\path{src/bpg/algorithm}
\title{Compute canonic form of a BPG}
\classes{}
\makeheader
 
xet*/

#define MODULE_OK

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "bpg/bpg.h"
#include "graph/bgraph.h"
#include "general/dtime.h"

//#include "bpg/algorithm/canonic_b.h"

// output control...
#define DEBUG 0

#if DEBUG >= 1
#define DEBUG1(s) s
#else
#define DEBUG1(s) 
#endif

#if DEBUG >= 2
#define DEBUG2(s) s
#else
#define DEBUG2(s) 
#endif

#if DEBUG >= 3
#define DEBUG3(s) s
#else
#define DEBUG3(s) 
#endif

//-------------------------------------------------------------------

// static data structures

static bool moduleInUse = false;
static int timeoutCheck;
static float timeout;
static bool timeExceeded;
static Dtime timer;

static int bs;  // boundary size
static int n;   // number of vertices
static int len; // number of operators

// the graph used to track edge usage and give us vertex labels
//
static BGraph* G;

// the current partial parse, and its length
//
static RBBPG parse;
static int pos;
static bool equalToBound;
static bool checkOnly;

// lex bound on target (best found so far)
//
static RBBPG bound;

// graph vertices currently on the bndry
//
static Array<vertNum> currentBndry;

// false iff a bndry vertex has been placed using the op
// i.e. is currentBndry[i] a boundary vertex
//
static Array<bool> vOpAvailable;

// true iff graph vertex has been placed
// used to test quickly if vertex is available for placement
//
static Array<bool> vertexPlaced;


//-------------------------------------------------------------------

static void debug1()
{
   int i;
   cerr << "----------------" << nl;
   cerr << "   pos:   " << pos << nl;
   cerr << "   equal: " << int(equalToBound) << nl;
   cerr << "   parse: ";
      for ( i=0; i<pos; i++ ) cerr << parse[i] << ' '; cerr << nl;
   cerr << "   bound: ";
      for ( i=0; i<bound.length(); i++ ) cerr << bound[i] << ' '; cerr << nl;
   cerr << "   bndry: ";
      for ( i=0; i<bs; i++ ) cerr << currentBndry[i] << ' '; cerr << nl;
   cerr << "G: " << *G;
   cerr << "----------------" << nl;
   
}

static void debug2( int pos )
{
   (void) pos;
}

//----------------------

static bool boundEqual()
{
   for ( int i=0; i<pos; i++ )
   {
      if ( parse[i] != bound[i] ) { return false; }
   }

   // equal...
   return true;
}

static bool boundExceeded()
{
   for ( int i=0; i<pos; i++ )
   {
      if ( parse[i] > bound[i] ) { return true; }
      if ( parse[i] < bound[i] ) { return false; }
   }

   // equal...
   return false;
}

static bool boundBeaten()
{
   for ( int i=0; i<pos; i++ )
   {
      if ( parse[i] > bound[i] ) { return false; }
      if ( parse[i] < bound[i] ) { return true; }
   }

   // equal...
   return false;
}

//----------------------

static void updateBound()
{
   bound = parse;
   equalToBound = true;
}

//----------------------

static bool boundObtained( int checkTo )
{
   aassert(false); // update if needed
   for ( int i=0; i<checkTo; i++ )
      if ( parse[i] != bound[i] ) return false;
   return true;
}

//----------------------

// insert edges for vertex in currentBndry[vPos]
// return false if can't perform insertions
//
static bool insertEdges( int vPos, int& pos, bool greaterOnly = false )
{
   vertNum v = currentBndry[vPos];
   bool allPlaced = true;

   // scan edges, to make sure all are placable
   //
   for ( int u=0; u<n; u++ )
   {
      if ( v == u ) continue;
      if ( G->isEdge( v, u ) )
      {
         int otherVPos = currentBndry.index( u );

         if ( otherVPos == bs )
         {
            // no can do
            return false;
            //allPlaced = false;
            //break;
         }
      }
   }

   for ( vertNum vPos2=0; vPos2 < bs; vPos2++ )
   {
      vertNum v2 = currentBndry[ vPos2 ];

      if ( v2 == -1 ) continue;
      if ( greaterOnly && vPos2 < vPos ) continue;

      if ( v == v2 ) continue;

      if ( G->isEdge( v, v2 ) )
      {
         parse.setOp( pos++, Operator::edgeOperator( vPos, vPos2 ) );
      }
   }

   return true;
}

//----------------------

// recursive search
// on entry:
// on exit:
//
static bool canonicDFS()
{
   DEBUG2( cerr << "Entry:" << nl; debug1(); )

   if ( timeout > 0.0 && timeoutCheck++ > 50 )
   {
     timeoutCheck = 0;
     timeout -= timer();
     if ( timeout <= 0.0 )
     {
        timeExceeded = true;
        return true;
     }
   }

   if ( pos == len )
   {
      // all vertices have been placed
      // insert boundary edges

      //bool allPlaced = true;
      for ( int i=0; i<bs; i++ )
      {
         if ( G->degree( currentBndry[i] ) != 0 )
         {
            DEBUG2( cerr << "Exit: boundary edges not present" << nl; )
            return false;
         }
      }

      // we've found a parse...check against bound
      if ( boundBeaten() )
      {
         DEBUG1( cerr << "Found parse: " << parse << " (using)" << nl; )

         if ( checkOnly ) return true;

         // use it as new bound
         updateBound();
      }
      else
      {
         DEBUG1( cerr << "Found parse: " << parse << " (not using)" << nl; )
      }

      // keep going (ugh!)
      return false;
   }

   // We haven't finished placing stuff...make a recursive call for each op

   bool equalOnEntry = equalToBound; // save to restore after recursion
   equalToBound = false;

   parse.setOp( pos, Operator::noOp() );

   // iterate over all vertex ops and vertices
   //
   while(1)
   {
      // increment the operator

      Operator op = parse[ pos ];

      if ( op.isNoOp() )
      {
         // first pass through loop
         op.firstOp();
      }
      else
      {
         op++;

         // not first pass
         if ( op.isNoOp() )
         {
            // no more vertex ops...backup

            if ( pos == 0 )
            {
               // end of the search!
               DEBUG2( cerr << "Exit: search complete" << nl; )
               return false;
            }

            DEBUG2( cerr << "Exit: last op" << nl; debug1(); )

            return false;
         } // if last op

      } // if not first op

      DEBUG3( cerr << "Operator: " << op << nl; )
      parse.setOp( pos, op );

      // check lex order
      //
      if ( equalOnEntry )
      {
         if ( op == bound[pos] ) equalToBound = true;
         if ( op > bound[pos] )
         {
            DEBUG3( cerr << "bound exceeded" << nl; )
            return false;
         }
      }

      if ( op.isVertexOp() )
      {
         vertNum v = op.vertex();
         vertNum oldBndryVertex = currentBndry[ v ];

         // is vertex legal? (bndry violation)
         if ( ! vOpAvailable[ v ] )
         {
            DEBUG3( cerr << op << ": not avail" << nl; )
            continue;
         }

         // have all edges been placed?
         if ( oldBndryVertex != -1 )
         {
            if ( G->degree( oldBndryVertex ) != 0 )
            {
               DEBUG3( cerr << op << ": edges not all placed" << nl; )
               continue;
            }
         }

         vertNum g = -1;

         // loop thru each vertex assigned to the vop
         while(1)
         {
            // increment the vertex

            bool incOk = false;
            g++;
            for ( ; g<n; g++ )
            {
               if ( ! vertexPlaced[g] ) { incOk = true; break; }
            }

            if ( ! incOk )
            {
               // no more vertices
               break;
            }

            // all's ok...go deeper

            DEBUG3( cerr << "   assigning vertex " << g << nl; )

            vertexPlaced[g] = true;
            currentBndry[v] = g;

            if( G->isBoundary( g ) )
               vOpAvailable[ v ] = false;

            pos++;
            bool ret = canonicDFS();
            if ( equalToBound ) equalOnEntry = true;
            pos--;
            if ( ret ) return true;

            vertexPlaced[g] = false;
            vOpAvailable[v] = true;
         }
         currentBndry[v] = oldBndryVertex;
      }
      else
      {
         // edge op 
         vertNum v1 = op.vertex1();
         vertNum v2 = op.vertex2();
         vertNum g1 = currentBndry[v1];
         vertNum g2 = currentBndry[v2];

         // is it an edge?
         if ( ! G->isEdge( g1, g2 ) )
         {
            continue;
         }

         // kill edge, assign op and recurse
         G->rmEdge( g1, g2 );
         pos++;
         bool ret = canonicDFS();
         if ( equalToBound ) equalOnEntry = true;
         pos--;
         if ( ret ) return true;

         G->addEdge( g1, g2 );

      } // edge op

   } // iterate over all vertex ops and vertices

}

//----------------------

// setup initial recursive call and do it
//
static bool internalCanonicSearch()
{
   DEBUG2( cerr << "graph: " << *G << nl; )

   // code is non-reentrant
   aassert( ! moduleInUse );

   moduleInUse = true;

   bs = Operator::boundarySize();
   n = G->order();
   len = bound.length();

   vOpAvailable.resize( bs );
   vertexPlaced.resize( n );
   parse.resize( len );
   currentBndry.resize( bs );

   // prepare for initial call
   vOpAvailable.fill( true );
   vertexPlaced.fill( false );
   currentBndry.fill( -1 );
   equalToBound = true;

   timer.start();

   // make initial call
   bool ret = canonicDFS();

   moduleInUse = false;

   return ret;
} 

//------------------------------------------------------------------

// Entry points
 
bool RBBPG::isCanonic( const RBBPG& orig, int &to )
{
   checkOnly = true;
   bound = orig;
   G = new BGraph( bound );
   timeout = to;
   timeExceeded = false;
   timeoutCheck = 0;
   bool ret = internalCanonicSearch();
   delete G;

   if ( timeExceeded ) { to=0; return true; }

   return ! ret;
}


bool RBBPG::findCanonic( const RBBPG& orig, RBBPG& can )
{
   checkOnly = false;
   bound = orig;
   G = new BGraph( bound );
   timeout = 0;
   timeExceeded = false;
   timeoutCheck = 0;
   internalCanonicSearch();
   delete G;

   can = bound;

   return true;
}


bool RBBPG::findCanonic( const BGraph&, RBBPG& )
{
   aassert(false);
   return true;
}

