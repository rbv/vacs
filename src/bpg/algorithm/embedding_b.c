/*
 * Supports at most 16 boundary vertices, and at most genus 7 (actual limit MAX_GENUS)
 */

// Define to compile as a standalone program running testcases
//#define TESTING

#include <assert.h>
#include <vector>
#include <set>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <string.h>
#include <ctype.h>
#include <boost/smart_ptr/shared_ptr.hpp>

#ifndef TESTING
#include "bpg/algorithm/embedding_b.h"
#endif
#include "general/murmurhash2.h"
#include "general/memuse.h"
// maybe vector<bool> instead?
//#include "general/bitvect.h"

//(setq c-basic-offset 3)

//#define DBG(x) x
#define DBG(x)
#define DBG2(x)
#define DBG_REDUCE(x)

// Throw away 2-gons
#define SRULE1
// Do no other embeddings when a 2-gon embedding possible
#define SRULE2

#define MAX_GENUS 2
// Plus one for genus zero, plus one while joining
// It would be double while joining, except joining faces from two embedding with
// total genus greater than the genus limit doesn't happen.
#define MAX_BOUNDARIES (MAX_GENUS + 2)

// Define to prevent boundary orientations from being normalised away
//#define PRESERVE_ORIENTATION
#ifdef PRESERVE_ORIENTATION
# define ORIENTATIONS 2
#else
# define ORIENTATIONS 1
#endif

using std::vector;
using std::set;
using std::multiset;
using std::ostream;
using std::cerr;
using std::cout;
using boost::shared_ptr;

// Increment an iterator, since STL iterators don't all define operator+
template<typename T>
T incIt( T it, int amount = 1 )
{
   T ret( it );
   std::advance( ret, amount );
   return ret;
}

template<typename T>
struct deref_comp
{
   bool operator() ( const shared_ptr<T> &lhs, const shared_ptr<T> &rhs )
   {
      //cerr << ".";
      return *lhs < *rhs;
   }
};

// Quick job. Not too fancy
template<class T>
class inplace_vector
{
   T *_begin, *_end;
#ifndef NDEBUG
   int _maxsize;
#endif

   // Not copyable
   inplace_vector( const inplace_vector &rhs );
   void operator=( const inplace_vector &rhs );

  public:

   inplace_vector( T *begin, int maxsize )
      : _begin( begin ), _end( begin )
#ifndef NDEBUG
      , _maxsize( maxsize )
#endif
   {}

   ~inplace_vector() { clear(); }

   void push_back( const T &value )
   {
      assert( size() < _maxsize );
      new ( _end++ ) T( value );
   }
   void pop_back() {
      assert( _begin != _end );
      _end--;
   }
   T &back() { return *(_end - 1); }

   int size() const { return _end - _begin; }
   //void resize( int newsize ) { _end = _begin + newsize; }  // Only for POD

   T &operator[]( int index ) { return _begin[index]; }
   const T &operator[]( int index ) const { return _begin[index]; }

   T *begin() { return _begin; }
   T *end() { return _end; }
   const T *begin() const { return _begin; }
   const T *end() const { return _end; }

   void clear()
   {
      while (_end > _begin)
         (--_end)->~T();
   }

   void swap( inplace_vector<T> &rhs )
   {
      std::swap( _begin, rhs._begin );
      std::swap( _end, rhs._end );
#ifndef NDEBUG
      std::swap( _maxsize, rhs._maxsize );
#endif
   }  
};


class Face;
class Embedding;
class Component;
class EmbedState;
//typedef multiset<shared_ptr<Face>, deref_comp<Face> > FaceSet;
typedef vector<shared_ptr<Face> > FaceSet;

struct SurfaceParam
{
   union
   {
      struct
      {
         int handles;
         int crosscaps;
         int apices;
      };
      int params[3];
   };

   SurfaceParam() : handles(0), crosscaps(0), apices(0) {}

   void invalid() 
   {
      handles = -1;
      crosscaps = -1;
      apices = -1;
   }

   bool isInvalid() const { return handles < 0; }

   // Can all embedded graphs on *this be extended to rhs?
   bool isSubsurfaceOf( const SurfaceParam &rhs ) const
   {
      return handles <= rhs.handles && crosscaps <= rhs.crosscaps && apices <= rhs.apices;
   }

   bool operator==( const SurfaceParam &rhs ) const
   {
      return memcmp( params, rhs.params, sizeof( params ) ) == 0;
   }

   // Linear ordering
   bool operator<( const SurfaceParam &rhs ) const
   {
      return memcmp( params, rhs.params, sizeof( params ) ) < 0;
   }

   SurfaceParam minimum( const SurfaceParam &rhs ) const
   {
      SurfaceParam ret;
      ret.handles = std::min( handles, rhs.handles );
      ret.crosscaps = std::min( crosscaps, rhs.crosscaps );
      ret.apices = std::min( apices, rhs.apices );
      return ret;
   }

   SurfaceParam operator+( const SurfaceParam &rhs ) const
   {
      SurfaceParam ret;
      ret.handles = handles + rhs.handles;
      ret.crosscaps = crosscaps + rhs.crosscaps;
      ret.apices = apices + rhs.apices;
      return ret;
   }

   SurfaceParam operator-( const SurfaceParam &rhs ) const
   {
      SurfaceParam ret;
      ret.handles = handles - rhs.handles;
      ret.crosscaps = crosscaps - rhs.crosscaps;
      ret.apices = apices - rhs.apices;
      return ret;
   }
};


ostream &operator<<( ostream &os, const SurfaceParam surface )
{
   os << " handles=" << surface.handles << " crosscaps=" << surface.crosscaps << " apices=" << surface.apices;
   return os;
}

enum dir_t
{
   FORWARDS = 1,
   BACKWARDS = 2,
   BOTHDIRS = 3,
};

// Element of a dihedral group
struct dihedral_t
{
   int pos;
   int dir;  // -1 or 1
};

struct BoundaryDesc
{
   int start;  // index in vertices
   int len;
   // Whether the canonical form is the reverse of the stored form or not.
   // Note that this does not actually affect the orientation of the boundary,
   // it's only used for getting the canonical form.
   // Is meaningless if the boundary hasn't been normalised
   // See also Boundary::normalise()
   bool reversed;
};

// Wrapper around a BoundaryDesc
// Does not own the memory it references!
template<class vertex_t, class face_t, class boundary_t>
class BoundaryBase
{
protected:
   vertex_t *vertices;
   int len;  // TODO: replace with desc->len
   //bool reversed;
   boundary_t *desc;

public:

   typedef std::reverse_iterator<const char *> reverse_const_iterator;

   BoundaryBase( face_t &face, boundary_t &desc );

   vertex_t *begin() { return vertices; }
   vertex_t *end() { return vertices + len; }
   const vertex_t *begin() const { return vertices; }  // FIXME: this makes ConstBoundary pointless
   const vertex_t *end() const { return vertices + len; }
   vertex_t &last() { return *(vertices + len - 1); }
   int size() const { return len; }
   bool reversed() const { return desc->reversed; }
   // len can not be changed!

   // a bitmask giving the vertices on this boundary
   //unsigned int mask;

   vertex_t &vert( int index )
   {
      // Single wrap around might be faster than modulus?
      if ( index < 0 )
         return vertices[ index + len ];
      if ( index >= len )
         return vertices[ index - len ];
      return vertices[ index ];
   }

   vertex_t &operator[]( int index )
   {
      return vertices[ index ];
   }

   //void hash();

   // 0 or 1
   //int parity();

   bool isSupBoundaryOf( const BoundaryBase &other, dir_t &allowed_dirs ) const;

   void print( ostream &os ) const;

   bool operator<( const BoundaryBase &rhs ) const
   {
      // This is a pseudo-lexicographical order: sorted first by length
      if ( size() != rhs.size() )
         return size() < rhs.size();
      if ( reversed() ^ rhs.reversed() )
         return std::lexicographical_compare( begin(), end(), reverse_const_iterator( rhs.end() ), reverse_const_iterator( rhs.begin() ) );
      return memcmp( begin(), rhs.begin(), size() ) < 0;
   }

   bool operator==( const BoundaryBase &rhs ) const
   {
      if ( size() != rhs.size() )
         return false;
      if ( reversed() ^ rhs.reversed() )
         return std::equal( begin(), end(), reverse_const_iterator( rhs.end() ) );
      return memcmp( begin(), rhs.begin(), size() ) == 0;
   }
};

typedef BoundaryBase<const char, const Face, const BoundaryDesc> ConstBoundary;

class Boundary : public BoundaryBase<char, Face, BoundaryDesc>
{
public:
   Boundary( Face &face, BoundaryDesc &desc )
      : BoundaryBase<char, Face, BoundaryDesc>( face, desc )
   {}

   // Reverse vertices and flip 'reversed'
   void reverse();

   void normalise();

   operator ConstBoundary() { return *reinterpret_cast<ConstBoundary *>( this ); }
};

class Face
{
   friend class BoundaryIterator;
   template<class vertex_t, class face_t, class boundary_t>
   friend class BoundaryBase;

   vector<char> vertices;
   bool normalised;  // If false, boundaries are not sorted, and hash and mask are invalid
   bool unnormalised_boundaries;  // Any unnormalised boundaries?
   mutable uint64_t hash;

   // a bitmask giving the vertices on the boundaries of this face
   mutable uint32_t mask;

   // The boundaries of a face are converted to canonical representation and sorted
   // so that ordering faces is easy. See Boundary::normalise(), Face::normalise()
   //vector<BoundaryDesc> boundaries;
   inplace_vector<BoundaryDesc> boundaries;
   BoundaryDesc _boundary_buf[ MAX_BOUNDARIES ];

public:
   uint64_t getHash() const
   {
      if ( !normalised )
         const_cast<Face *>(this)->normalise();
      return hash;
   }

   uint32_t getMask() const
   {
      if ( !normalised )
         const_cast<Face *>(this)->normalise();
      return mask;
   }

   // See EmbedState::assertHaveEmbedding()
   int getLengthsSeq() const;

   char vertex( int index ) const
   {
      assert( boundaries.size() == 1 );
      assert( index >= 0 && index < vertices.size() );
      return vertices[index];
   }

private:

   BoundaryDesc &lastBoundary() { return *(boundaries.end() - 1); }

   Boundary boundary( int index )
   {
      assert( index < boundaries.size() );
      Boundary ret( *this, boundaries[index] );
      //ret.vertices = &vertices[boundaries[index].start];
      //ret.len = boundaries[index].len;
      return ret;
   }

   ConstBoundary boundary( int index ) const
   {
      assert( index < boundaries.size() );
      return ConstBoundary( *this, boundaries[index] );
   }

   // Add a copy of a boundary
   void copyBoundary( ConstBoundary &b )
   {
      BoundaryDesc desc = { (int)vertices.size(), b.size(), b.reversed() };
      boundaries.push_back( desc );
      vertices.insert( vertices.end(), b.begin(), b.end() );
      normalised = false;
   }

   // Copy all boundaries from another Face, except one or two
   void copyBoundariesExcept( const Face &rhs, int b1, int b2 = -1 )
   {
      for (int i = 0; i < rhs.boundaries.size(); i++ )
      {
         if ( i != b1 && i != b2 )
         {
            ConstBoundary B = rhs.boundary( i );
            copyBoundary( B );
         }
      }
   }

   // Copy each boundaries except one ('except') to one of two new faces.
   // face1Subset is a bitvector giving the boundaries (after 'except' is removed and
   // remaining are renumbered) going to face1
   void splitBoundariesBetween( Face &face1, Face &face2, int except, int face1Subset ) const
   {
      for (int i = 0; i < boundaries.size(); i++ )
      {
         if ( i != except )
         {
            ConstBoundary B = boundary( i );
            if ( face1Subset & 1 )
               face1.copyBoundary( B );
            else
               face2.copyBoundary( B );
            face1Subset >>= 1;
         }
      }
   }

   // After constructing a Face, all boundaries MUST be normalised:
   // if they are copied from another Face, they are, otherwise
   // normaliseNewBoundary() must be called

public:

   // Warning: calling normalise() before all boundaries have been added to a
   // Face is an ERROR! It screws up the orientations

   // Adds a new length zero boundary. To be followed by addVertex() calls
   // len is a length hint
   void addBoundary( int len = 0 )
   {
      assert( !unnormalised_boundaries );
      assert( boundaries.size() < MAX_BOUNDARIES );
      BoundaryDesc desc = { (int)vertices.size(), len, false };
      boundaries.push_back( desc );
      normalised = false;
      unnormalised_boundaries = true;
      //return boundaries.size() - 1;
   }

   // Add vertex to the last boundary
   // final_check: do a wrap-around duplication check
   void addVertex( int v, bool final_check = false, bool resize = true )
   {
      BoundaryDesc &b = lastBoundary();
      Boundary B( *this, b );
      if ( B.size() && B.last() == v )
         // Don't add duplicates
         return;
      if ( final_check && B.size() && B[0] == v )
         // If the last vertex is the same as the first, skip it
         return;
      vertices.push_back( v );
      if ( resize )
         b.len++;
      normalised = false;
      unnormalised_boundaries = true;
   }

   // For use only if the final_check argument isn't correctly passed to addVertex():
   // After the last vertex of a boundary has been added, check whether it is a
   // wrap-around duplicate and should be removed.
   void checkFinalVertex()
   {
      assert( unnormalised_boundaries );
      BoundaryDesc &b = lastBoundary();
      assert( b.len );
      if ( b.len > 1 && vertices[b.start] == vertices.back() )
      {
         vertices.pop_back();
         b.len--;
         // Impossible for further vertices to need removing, as the final two vertices in 
         // the boundary can't have both equal vertices[b.start]
      }
   }

   // remove the last, empty, boundary
   void removeNewBoundary()
   {
      assert( boundaries.back().len == 0 );
      boundaries.pop_back();
      unnormalised_boundaries = false;
   }

   // normalise the last boundary
   void normaliseNewBoundary()
   {
      Boundary( *this, lastBoundary() ).normalise();
      unnormalised_boundaries = false;
   }

   // A string containing only vertex numbers and single non-digit characters between groups of numbers
   void buildFromString( const char *str )
   {
      addBoundary();
      while ( *str )
      {
         char next = *(str + 1);
         if ( !isdigit( *str ) )
         {
            assert( isdigit( next ) );
            checkFinalVertex();
            normaliseNewBoundary();
            if ( next )
               addBoundary();
         }
         else
            addVertex( *str - '0' );
         str++;
      }
      checkFinalVertex();
      normaliseNewBoundary();
      normalise();
   }

private:

   bool boundariesEqual( const BoundaryDesc &d1, const BoundaryDesc &d2 ) const;

   struct BoundaryComparer
   {
      const Face &parent;
      BoundaryComparer( const Face &face ) : parent( face ) {}

      bool operator()( const BoundaryDesc &d1, const BoundaryDesc &d2 ) const
      {
         ConstBoundary B1( parent, d1 ), B2( parent, d2 );
         return B1 < B2;
      }
   };

   // Assumes all boundaries are normalised,
   // sorts boundaries and computes hash and mask
   void normalise();

   bool isSubFaceOfEx( int bnum, const Face &other, vector<bool> &used_boundaries, int bit_offset, int &bitnum, dir_t &allowed_dirs ) const;
   bool isSubFaceOfRecurse( int bnum, const Face &other, vector<bool> &used_boundaries, int bit_offset, dir_t allowed_dirs ) const;

   // No copy ctor (shouldn't be needed, not implemented)
   Face( const Face &rhs );

public:
   Face( int capacity = 0 );

   // Combine two faces, copying the boundaries from both.
   // If reflectSecond is true, the reflection F2 is used
   Face( const Face &F1, const Face &F2, bool reflectSecond );

   int size() const { return vertices.size(); }
   int numBoundaries() const { return boundaries.size(); }

   // Used as a heuristic
   int maxBoundaryLength() const;
   // Number of boundaries with length >= 3
   int numLongBoundaries() const;

   // Is identical to its reflection
   bool orientationless() const { return numLongBoundaries() == 0; }

   bool addEdge( int v1, int v2, const Embedding *parent, Embedding &templ, Component &result, bool joining ) const;
   void removeVertex( int v1, FaceSet &result ) const;

   bool hasAdjacentVertices( int v1, int v2 ) const;

   bool isSubFaceOf( const Face &other, vector<bool> &used_boundaries, int bit_offset ) const;

   void print( ostream &os ) const;

   void checkValid() const;
   
   bool operator<( const Face &rhs ) const;
   bool operator==( const Face &rhs ) const;
};

//typedef vector<BoundaryDesc>::const_iterator const_desc_iterator;
typedef const BoundaryDesc * const_desc_iterator;

struct BoundaryIterator
{
   const Face *parent;
   const_desc_iterator b_it;
   ConstBoundary curBoundary;
   const char *v_it;

   BoundaryIterator( const Face &parent_ )
   : parent( &parent_ ), curBoundary( *parent, parent->boundaries[0] )
   {
      assert( parent->boundaries.size() );
      assert( curBoundary.size() );
      b_it = parent->boundaries.begin();
      v_it = curBoundary.begin();
   }

   // Returns whether we can stepped to the next boundary
   bool increment()
   {
      if ( ++v_it == curBoundary.end() )
      {
         if ( ++b_it != parent->boundaries.end() )
         {
            curBoundary = ConstBoundary( *parent, *b_it );
            v_it = curBoundary.begin();
            assert( curBoundary.size() );
         }
         return true;
      }
      return false;
   }
   
   BoundaryIterator &operator++()
   {
      increment();
      return *this;
   }

   const char &operator*()
   {
      return *v_it;
   }

   int boundaryNum() const
   {
      return b_it - parent->boundaries.begin();
   }

   int vertexNum() const
   {
      return v_it - curBoundary.begin();
   }

   ConstBoundary &boundary()
   {
      return curBoundary;
   }

   bool done() const
   {
      return b_it == parent->boundaries.end();
   }

   // Whether these two iterators point to adjacent vertices on the same boundary
   bool adjacent( const BoundaryIterator &rhs ) const
   {
      if ( b_it != rhs.b_it )
         return false;
      int dist = std::abs( v_it - rhs.v_it );
      return dist == 1 || dist == (curBoundary.size() - 1);
   }      
};

class Embedding
{
   friend class Face;
   friend class Component;

   const Component *parent;

   SurfaceParam surface;
   // Gives the vertices treated as apices
   uint32_t apex_mask;

   FaceSet faces;  // may have identical faces
   uint64_t hash;
   bool normalised;

   //Embedding *faceless_template;

   void reduceFaces();

   // comparison function
   static bool faceLengthHeuristic( const Face *lhs, const Face *rhs );

public:

/*
   // Create the unique embedding for the disconnected bs-boundaried graph w/ no internal vertices
   Embedding( int bs, int handles = 0, int crosscaps = 0 );
*/

   // Public only for debug use
   Embedding( const Component *parent, int capacity = 0 );

   // Create an embedding with a single specified vertex
   Embedding( int vertex, const Component *parent );

   // Copy, with reserve capacity in faces, and a possible change of parent
   Embedding( const Embedding &rhs, int extra_faces, const Component *parent = NULL );

   // Create new embeddings
   static void joinWithEdge( Component &result, const Embedding &E1, const Embedding &E2, int v1, int v2 );

   void addEdge( Component &result, int v1, int v2 ) const;
   void removeVertex( Component &result, int v1 ) const;

   void checkValid( uint32_t expected_mask ) const;

   void addFace( shared_ptr<Face> face ) { faces.push_back( face ); }
   void addFaces( FaceSet::const_iterator it1, FaceSet::const_iterator it2 ) { faces.insert( faces.end(), it1, it2 ); }
   void addFaceFromString( const char *str );

   // Sort faces and compute hash
   void normalise();

   bool isSubEmbeddingOf( const Embedding &rhs ) const;

   void copyTopology( const Embedding &rhs )
   {
      surface = rhs.surface;
   }

   // For now we don't allow treating a handle on a non-orientiable surface
   // as two crosscaps
   // Queries the parent component
   bool unusedHandles() const;
   bool unusedCrosscaps() const;
   bool unusedApices() const;

   void useHandle()
   {
      assert( unusedHandles() );
      surface.handles++;
   }
   void useCrosscap()
   {
      assert( unusedCrosscaps() );
      surface.crosscaps++;
   }
   void useApex( int vertex )
   {
      assert( unusedApices() );
      assert( !(apex_mask & (1 << vertex)) );
      apex_mask |= 1 << vertex;
      surface.apices++;
   }

   void print( ostream &os ) const;

   uint64_t getHash() const
   {
      if ( !normalised )
         const_cast<Embedding *>(this)->normalise();
      return hash;
   }

   bool operator<( const Embedding &rhs ) const;
   bool operator==( const Embedding &rhs ) const;
};

class Component
{
   friend class EmbedState;

   set<Embedding> embeddings;  // duplicate embeddings are redundant

   set<Embedding> *newstate;  // New embeddings while building the next state are put in here

   uint32_t mask;  // vertices in this component
   int totalsize;  // Sum of all unique embeddings built for this component and its ancestors
   int numvertices;

   // The biggest surface (maximum number of handles, etc) currently usable in this component. Continually updated
   SurfaceParam maxsurface;

   // Only used by EmbedState copy-ctor
   Component( const Component &rhs );

   Component &operator=( const Component &rhs ); // { abort(); }

   void reduce();

public:
   // The empty graph
   Component();

   // An embedding of K_1
   Component( int v1 );

   // Destructively create a new component by joining together two components with a new edge
   static Component *joinWithEdge( Component &C1, Component &C2, int v1, int v2 );

   void addEmbedding( const Embedding &embedding, bool nonminimal = false );

   void addEdge( int v1, int v2 );
   void removeVertex( int v1 );

   uint32_t getMask() const { return mask; }
   bool isEmpty() const { return embeddings.empty(); }
   // For debugging only
   uint64_t getHash() const;

   void print( ostream &os ) const;
   void printstats( ostream &os ) const;
   int numEmbeddings() const;

   int maxHandles() const { assert( maxsurface.handles > -1 ); return maxsurface.handles; }
   int maxCrosscaps() const { assert( maxsurface.crosscaps > -1 ); return maxsurface.crosscaps; }
   int maxApices() const { assert( maxsurface.apices > -1 ); return maxsurface.apices; }

   // The minimum number used by any embedding
   SurfaceParam minSurface() const;

   // Set the max number of handles, crosscaps, etc, and drop illegal embeddings
   void setLimits( SurfaceParam &new_surface );

   int assertHaveEmbedding( int seqLen, int sidesSeq[] ) const;

   // Only to be used to order distinct components of the same graph
   static bool orderComponents( const Component *lhs, const Component *rhs ) { return lhs->mask < rhs->mask; }

   bool leq( const Component &rhs ) const;
   bool identical( const Component &rhs ) const;
};

class EmbedState
{
   vector<Component *> components;
   int boundarysize;
   //int covering_faces;  // if non-zero, maximum number of faces required for covering

   int maxsize;  // Maximum number of embeddings
   int totalsize;  // Sum of number of embeddings at every state of the t-parse
   int embeddings_built;

   static int _embeddings_built;

   // Maximum allowable in any complete embedding
   SurfaceParam surface;

   // Amount used by interior components which have been dropped from the state
   SurfaceParam interior_surface;

   // Sum of minimums of all components
   SurfaceParam min_surface;

   mutable bool sorted;  // components sorted?

   void sort() const;

   // Update limits for all components after one has changed
   void updateLimits(); // vector<Component *>::iterator component );
   void updateStats();

public:
   // Create state for the disconnected bs-boundaried graph w/ no internal vertices
   EmbedState( int bs, int handles = 0, int crosscaps = 0, int apices = 0 );
   EmbedState( const EmbedState &rhs );
   ~EmbedState();

#ifndef TESTING
   void compute( const BPG &G );
#endif
   void compute( int ops[] );

   // Form complete graph on boundary, minus some number of edges
   void K( int vertices, int edges = 0 );

   // Reduce all components (try harder to throw away redundant embeddings)
   void reduce();

   void addEdge( int v1, int v2 );
   void addVertex( int v1 );

   bool isEmpty() const;
   int numComponents() const { return components.size(); }
   int numEmbeddings() const;
   int handles() const { return min_surface.handles + interior_surface.handles; }
   int crosscaps() const { return min_surface.crosscaps + interior_surface.crosscaps; }
   int apices() const { return min_surface.apices + interior_surface.apices; }

   void print( ostream &os ) const;
   void printstats( ostream &os ) const;

   Component *addDebugComponent( int bs );
   void addDebugEmbedding( const Embedding &embedding );

   // For debugging only
   uint64_t getHash() const;

   void checkValid() const;
   int assertHaveEmbedding( int seqLen, int sidesSeq[], uint32_t mask = 0 ) const;

   static void incEmbeddingsBuilt() { _embeddings_built++; }
   int embeddingsBuilt() const { assert( !_embeddings_built ); return embeddings_built; }

   // Comparison function which should actually be used for comparing the state of a minor!
   bool leq( const EmbedState &rhs ) const;

   // whether the states have identical sets of embeddings, which is much stricter than desired
   bool identical( const EmbedState &rhs ) const;
};

//------------------------------------------------------------------------------------------------

int EmbedState::_embeddings_built = 0;

//------------------------------------------------------------------------------------------------

template<class vertex_t, class face_t, class boundary_t>
BoundaryBase<vertex_t, face_t, boundary_t>::BoundaryBase( face_t &face, boundary_t &desc_ )
{
   desc = &desc_;
   vertices = &face.vertices[desc_.start];
   len = desc_.len;
   //reversed = desc.reversed;
}

template<class vertex_t, class face_t, class boundary_t>
void BoundaryBase<vertex_t, face_t, boundary_t>::print( ostream &os ) const
{
   os << " ( ";
   for (int j = 0; j < size(); j++)
      os << (int)vertices[j] << ' ';
   os << ")";
   if ( reversed() )
      os << "^";
}

void Boundary::reverse()
{
   desc->reversed ^= true;
   char buf[ size() ];
   char *dest = buf + size() - 1;
   for (char *p = begin(); p != end(); )
      *dest-- = *p++;
   std::copy( buf, buf + size(), begin() );
}

void Boundary::normalise()
{
   // Find starting point and direction of reading which minimises the visited
   // vertices in lexicographical order.
   //
   // We want to preserve the relative orientations of the different boundaries
   // of a face, but don't preserve the relative orientations between faces
   // unless PRESERVE_ORIENTATION is set (useful for constructing an actual
   // embedding).
   //
   // So we normalise each boundary, preserving the orientation at this
   // step. Then we sort in Face::normalise the boundaries by canonical, not
   // actual representation. In order to fold together Faces which are mirror
   // images, we reverse all boundaries if necessary so that the first is
   // unreversed. Alternatively, it would be faster to take the orientation of
   // the first boundary into account when comparing all others, but that's much
   // more work to code.

   assert( size() );
   if ( size() == 1 )
      return;

   dihedral_t buf1[ size() * 2 ], buf2[ size() * 2 ];

   // Candidate starting points
   inplace_vector<dihedral_t> options( buf1, size() * 2 );
   // Scratch space
   inplace_vector<dihedral_t> options2( buf2, size() * 2 );

   //cerr << "normalising boundary"; print( cerr );

   char searchval = *std::min_element( begin(), end() );
   for (int i = 0; i < size(); i++)
   {
      assert( vertices[i] >= 0 && vertices[i] <= 15 );
      if ( vertices[i] == searchval )
      {
         options.push_back( (dihedral_t){ i, 1 } );
#ifndef PRESERVE_ORIENTATION
         options.push_back( (dihedral_t){ i, -1 } );
#endif
      }
   }

#ifdef PRESERVE_ORIENTATION
   assert( options.size() < size() );
#else
   assert( options.size() < 2 * size() );
#endif

   int depth = 1;
   while ( options.size() > 1 && depth < size() )
   {
      int min = 999;
      for (int i = 0; i < options.size(); i++)
      {
         int val = vert( options[i].pos + options[i].dir * depth );
         if ( val < min )
         {
            options2.clear();
            min = val;
         }
         if ( val == min )
            options2.push_back( options[i] );
      }

      options.swap( options2 );
      options2.clear();
      depth++;
   }


   int start = options[0].pos;

   //cerr << "\nchoose start " << start << "\n";

   if ( options[0].dir == -1 )
   {
      // Reverse. Store the canonical form backwards (i.e. last vertex is the
      // start) without changing orientation
      desc->reversed = true;

      start = (start + 1) % size();
/*
      if ( start != size() - 1 )
      {
         char buf[ size() ];
         char *dest = buf + size() - 1;
         for (char *p = begin() + start; p >= begin(); )
            *dest++ = *p--;
         for (char *p = end() - 1; p > begin() + start; )
            *dest++ = *p--;
         std::copy( buf, dest, begin() );
      }
*/
   }
   
   if ( start )
   {
      // Rotate starting point of vertices in a buffer, and copy back
      //vector<int> buf( size() );
      //vector<int>::iterator finish;
      char buf[ size() ];
      char *finish;
      finish = std::copy( begin() + start, end(), buf );
      finish = std::copy( begin(), begin() + start, finish );
      //vertices.swap( buf );
      std::copy( buf, finish, begin() );
   }

   //cerr << " now "; print( cerr );
}

template<class vertex_t, class face_t, class boundary_t>
bool BoundaryBase<vertex_t, face_t, boundary_t>::isSupBoundaryOf( const BoundaryBase &other, dir_t &allowed_dirs ) const
{
   // Check whether this boundary is a super-boundary (superior) of the other
   // Both should be normalised of course

   // See Face::isSubFaceOf about allowed_dirs.
   // Note that we could try both directions if both are allowed, and if both match avoid
   // unecessarily narrowing/restricting allowed_dirs

   // Size 1 should be handled elsewhere
   assert( other.size() > 1 );

   assert( allowed_dirs != 0 );

   if ( other.size() > size() )
      return false;

   if ( other.size() == size() )
   {
      // Check equality. If they are equal, then it is with matched orientations
      //return *this == other;
      // Have to be careful to respect allowed_dirs
      // If 

      if ( allowed_dirs & FORWARDS )  // && !(reversed() ^ rhs.reversed()) )  // too tricky; try later
      {
         if ( memcmp( begin(), other.begin(), size() ) == 0 )
         {
            // Incidentally, in case it
            allowed_dirs = FORWARDS;
            return true;
         }
      }
      else
      {
         if ( std::equal( begin(), end(), reverse_const_iterator( other.end() ) ) )
         {
            allowed_dirs = BACKWARDS;
            return true;
         }
      }
      return false;
   }

   for (int i = 0; i < size(); i++)
   {
      if ( vertices[i] == other.vertices[0] )
      {

         //tryMatch<1>(  );

         // Greedily choose matching vertices

         if ( allowed_dirs & FORWARDS )
         {

            int this_index = i, other_index = 1;
            for (int j = 1; j < size(); j++)
            {
               if ( ++this_index == size() )
                  this_index = 0;

               if ( vertices[this_index] == other.vertices[other_index] )
               {
                  if ( ++other_index == other.size() )
                  {
                     allowed_dirs = FORWARDS;
                     return true;
                  }
               }
            }
         }

#ifndef PRESERVE_ORIENTATION
         // Reverse?

         if ( allowed_dirs & BACKWARDS )
         {

            int this_index = i, other_index = 1;
            for (int j = 1; j < size(); j++)
            {
               if ( this_index == 0 )
                  this_index = size() - 1;
               else
                  this_index--;

               if ( vertices[this_index] == other.vertices[other_index] )
               {
                  if ( ++other_index == other.size() )
                  {
                     allowed_dirs = BACKWARDS;
                     return true;
                  }
               }
            }
         }
#endif

         // Failure
         continue;
      }
   }
   return false;
}

/*
  unsigned int Boundary::hash()
  {
  unsigned int ret = 0;
  if ( len < 8 )
  {
  for (int i = 0; i < len; i++)
  ret = (ret << 4) | vertices[i];
  }
  else
  {
  ret = Murmurhash2( begin(), len, 0 ) | ((uint64_t)1 << 63);
  }
  return ret
  }
*/

//------------------------------------------------------------------------------------------------

Face::Face( int capacity )
   : boundaries( _boundary_buf, MAX_BOUNDARIES )
{
   normalised = false;
   unnormalised_boundaries = false;
   vertices.reserve( capacity );
   //boundaries.reserve( 2 );
}

Face::Face( const Face &F1, const Face &F2, bool reflectSecond )
   : vertices( F1.vertices.size() + F2.vertices.size() ),
     boundaries( _boundary_buf, MAX_BOUNDARIES )
{
   normalised = false;
   unnormalised_boundaries = false;
   //vertices.reserve( F1.vertices.size() + F2.vertices.size() + 2 );
   //boundaries.reserve( 2 );

#if 0
   copyBoundariesExcept( F1, -1 );
   copyBoundariesExcept( F2, -1 );  // WRONG: need to reflect
#else
   // premature optimisation

   // Copy vertices
   vector<char>::iterator it;
   it = std::copy( F1.vertices.begin(), F1.vertices.end(), vertices.begin() );
   if ( !reflectSecond )
      std::copy( F2.vertices.begin(), F2.vertices.end(), it );
   else
      std::copy( F2.vertices.rbegin(), F2.vertices.rend(), it );

   // Copy boundaries
   //boundaries = F1.boundaries;
   const_desc_iterator it2;
   for (it2 = F1.boundaries.begin(); it2 != F1.boundaries.end(); ++it2)
      boundaries.push_back( *it2 );
   for (it2 = F2.boundaries.begin(); it2 != F2.boundaries.end(); ++it2)
   {
      BoundaryDesc desc = { it2->start + (int)F1.vertices.size(), it2->len, reflectSecond ^ it2->reversed };
      boundaries.push_back( desc );
   }
#endif

   normalise();
}

// Check whether some boundary has these two vertices adjacent
bool Face::hasAdjacentVertices( int v1, int v2 ) const
{
   assert( (mask | (1 << v1) | (1 << v2)) == mask );

   BoundaryIterator b1( *this ), next( *this );
   for ( ; !b1.done(); )
   {
      ++next;
      if ( *b1 == v1 || *b1 == v2 )
      {
         if ( b1.boundaryNum() == next.boundaryNum() )
         {
            if ( *next == v1 || *next == v2 )
               return true;
         }
         else
         {
            // Check wraparound
            ConstBoundary B = b1.boundary();
            if ( B.size() > 1 )
               if ( B[0] == v1 || B[0] == v2 )
                  return true;
         }
      }
      b1 = next;
   }
   return false;
}

// Add every way of adding an edge to this face to 'result'
// Returns true if at least one new face is constructed
bool Face::addEdge( int v1, int v2, const Embedding *parent, Embedding &templ, Component &result, bool joining ) const
{
   bool ret = false;
   if ( v1 > v2 )
      std::swap( v1, v2 );

   // Simplification rules:
   // -If we have a single boundary, which is just v1, v2, then the two new identical faces are redundant; throw away one
   // --better: if v1, v2 are consecutive, throw away the new 2-gon formed (do nothing!)
   // -The same vertex should not appear twice on a boundary consecutively (remove the loop)

   // Note that a vertex can occur on two different boundaries on non-planar surfaces!!

   BoundaryIterator b1( *this );
   //int b1, b2, b_end = boundaries.size();  // boundary indices
   for ( ; !b1.done(); ++b1 )
   {
      if ( *b1 == v1 )
      {
         DBG2( cerr << "-found endpoint1 in boundary " << b1.boundaryNum() << " vertex " << b1.vertexNum() << "\n" );
         BoundaryIterator b2( *this );
         for ( ; !b2.done(); ++b2 )
         {
            if ( *b2 == v2 )
            {
               DBG2( cerr << "--found endpoint2 in boundary " << b2.boundaryNum() << " vertex " << b2.vertexNum() << "\n" );
               // A way to place the edge

               // TODO: use i1, i2 everywhere?
               int i1 = b1.vertexNum(), i2 = b2.vertexNum();

               if ( b1.boundaryNum() != b2.boundaryNum() )
               {
                  // Join two boundaries together. New boundary is 2 larger:
                  // b1 and b2 are added twice

                  //Boundary B1 = boundary( b1 ), B2 = boundary( b2 );
                  ConstBoundary B1 = b1.boundary(), B2 = b2.boundary();

                  shared_ptr<Face> newface( new Face( vertices.size() + 2 ) );
                  newface->copyBoundariesExcept( *this, b1.boundaryNum(), b2.boundaryNum() );

                  //cerr << "new face with other boundaries:\n"; newface->print( cerr );

                  newface->addBoundary();

                  for ( int i = b1.vertexNum(); i < B1.size(); i++ )
                     newface->addVertex( B1[i] );
                  for ( int i = 0; i <= b1.vertexNum(); i++ )
                     newface->addVertex( B1[i] );

                  for ( int i = b2.vertexNum(); i < B2.size(); i++ )
                     newface->addVertex( B2[i] );
                  for ( int i = 0; i < b2.vertexNum(); i++ )
                     newface->addVertex( B2[i] );
                  newface->addVertex( B2[ b2.vertexNum() ], true );  // final_check

                  newface->normaliseNewBoundary();
                  newface->normalise();

                  Embedding newembedding( templ, 1 );
                  newembedding.addFace( newface );
                  newembedding.normalise();
                  result.addEmbedding( newembedding );
                  ret = true;
               }
               else
               {
                  // Split the face into two

                  assert( !joining );

                  ConstBoundary B = b1.boundary();

                  if ( i1 > i2 )
                     std::swap( i1, i2 );

                  // Possible optimisation: a lot of results of embedding an edge on a handle
                  // or crosscap seem to produce the same result when using different occurrences of
                  // the same vertex on a boundary.

                  if ( templ.unusedHandles() )
                  {
                     // FIXME: making assumptions about how faces can be embedded on genus > 0 surfaces:
                     // we are going to assume that we only need to consider handles when adding an edge to
                     // a boundary that is already a loop?? (not implemented)
                     // Maybe that is wrong and should not be needed:
                     // INSTEAD, when forming the loop in the first place, form it around a handle, creating
                     // a two-boudnary face. necessary?

                     // FIXME: prove that routing an edge through more than one handle or crosscap results in
                     // a face where theextra hadnles or crosscaps can be split off again, in order words,
                     // doing so is never necessary.
                     // Multiple handles proved.

                     // An alternative way to embed: split the boundary into two belonging to the same face,
                     // routing the edge along a handle we'll assign to the face

                     shared_ptr<Face> newface( new Face( vertices.size() + 2 ) );
                     newface->copyBoundariesExcept( *this, b1.boundaryNum() );

                     newface->addBoundary();

                     // final_check not required
                     for ( int i = i1; i <= i2; i++ )
                        newface->addVertex( B[i] );

                     newface->normaliseNewBoundary();

                     newface->addBoundary();

                     // final_check not required
                     for ( int i = i2; i < B.size(); i++ )
                        newface->addVertex( B[i] );
                     for ( int i = 0; i <= i1; i++ )
                        newface->addVertex( B[i] );

                     newface->normaliseNewBoundary();

/*
                     // One alternative way to embed: add two copies of the same boundary,
                     // routing the edge along a handle we'll assign to the face

  // Code to join the boundary to a copy of itself

                     for ( int i = b1.vertexNum(); i < B.size(); i++ )
                        newface->addVertex( B[i] );
                     for ( int i = 0; i <= b1.vertexNum(); i++ )
                        newface->addVertex( B[i] );

                     // Reverse second boundary
                     for ( int i = b2.vertexNum(); i >= 0; i-- )
                        newface->addVertex( B[i] );
                     for ( int i = B.size() - 1; i > b2.vertexNum(); i-- )
                        newface->addVertex( B[i] );
                     newface->addVertex( B[ b2.vertexNum() ], true );  // final_check

                     newface->normaliseNewBoundary();
*/

                     newface->normalise();

                     Embedding newembedding( templ, 1 );
                     newembedding.useHandle();
                     newembedding.addFace( newface );
                     newembedding.normalise();
                     result.addEmbedding( newembedding );
                     ret = true;
                  }

                  if ( templ.unusedCrosscaps() )
                  {
                     // One alternative way to embed: reverse part of the boundary
                     // routing the edge through a crosscap we'll assign to the face
                     // FIXME: we are only adding one of the two potential orientations! That's probably wrong! So:
                     assert( numBoundaries() == 1 );

                     shared_ptr<Face> newface( new Face( vertices.size() + 2 ) );
                     newface->copyBoundariesExcept( *this, b1.boundaryNum() );
                     newface->addBoundary();

                     // final_check not required
                     for ( int i = i1; i <= i2; i++ )
                        newface->addVertex( B[i] );
                     for ( int i = i1; i >= 0; i-- )
                        newface->addVertex( B[i] );
                     for ( int i = B.size() - 1; i >= i2; i-- )
                        newface->addVertex( B[i] );

                     newface->normaliseNewBoundary();
                     newface->normalise();

                     Embedding newembedding( templ, 1 );
                     newembedding.useCrosscap();
                     newembedding.addFace( newface );
                     newembedding.normalise();
                     result.addEmbedding( newembedding );
                     ret = true;
                  }

#ifndef SRULE2
                  // if SRULE2, checked above
# ifdef SRULE1
                  if ( b1.adjacent( b2 ) )
                  {
                     // Ignore the 2-gon formed; so new embedding is identical to old
                     // We don't do this check for embeddings using handles or crosscaps. (It's probably not valid)

                     // parent is NULL when called from Embedding::join()
                     assert( parent );
                     result.addEmbedding( *parent, true );  // Nonminimal
                     ret = true;
                     DBG( cerr << "skipping 2-gon formation\n" );
                     continue;
                  }
# endif
#else
                  assert( !b1.adjacent( b2 ) );
#endif

                  // Add a new embedding for each way of distributing the remaining boundaries
                  // amongst the two new faces

                  // Note:  a--b--c--d--e , and adding bd edge: each of ab and de may
                  // or may not be inside the new face: this is handled because b, d both
                  // appear twice on the boundary1

                  // These subset bitvectors give subsets of boundaries after b1=b2 is removed
                  int maxSubset = (1 << (boundaries.size() - 1)) - 1;
                  assert( maxSubset >= 0 );
                  int face1Subset = maxSubset;

                  for ( ; face1Subset >= 0; face1Subset-- )
                  {
                     // i1, i2 will be duplicated, all other vertices are copied once, in original order
                  
                     shared_ptr<Face> newface1( new Face( vertices.size() ) );  // reserving more space than actually needed
                     shared_ptr<Face> newface2( new Face( vertices.size() ) );

                     //newface1.copyBoundariesExcept( *this, b1.boundaryNum() );
                     if ( maxSubset != 0 )
                        splitBoundariesBetween( *newface1, *newface2, b1.boundaryNum(), face1Subset );

                     // FIXME: don't recompute new face for every new embedding!!

                     newface1->addBoundary();

                     for ( int i = i1; i < i2; i++ )
                        newface1->addVertex( B[i] );
                     newface1->addVertex( B[i2], true );  // final_check

                     newface1->normaliseNewBoundary();
                     newface1->normalise();

                     newface2->addBoundary();

                     for ( int i = i2; i < B.size(); i++ )
                        newface2->addVertex( B[i] );
                     for ( int i = 0; i < i1; i++ )
                        newface2->addVertex( B[i] );
                     newface2->addVertex( B[i1], true );  // final_check

                     newface2->normaliseNewBoundary();
                     newface2->normalise();

                     Embedding newembedding( templ, 2 );
                     newembedding.addFace( newface1 );
                     newembedding.addFace( newface2 );
                     newembedding.normalise();
                     result.addEmbedding( newembedding );
                     ret = true;
                  }
               }
            }
         }
      }
   }
   return ret;
}

void Face::removeVertex( int v1, FaceSet &result ) const
{
   shared_ptr<Face> newface( new Face( vertices.size() ) );  // overallocation
   newface->addBoundary();

   BoundaryIterator b1( *this );
   bool removedVertex = false;
   while ( !b1.done() )
   {
      BoundaryIterator next( b1 );
      bool nextBoundary = next.increment();

      if ( *b1 == v1 )
         removedVertex = true;
      else
         newface->addVertex( *b1 );  // Don't need to do final_check (would not be possible anyway), handled below

      if ( nextBoundary )  // Also true on last iteration
      {
         if ( removedVertex )
         {
            if ( newface->lastBoundary().len == 0 )
            {
               // Removed single vertex
               assert( b1.boundary().size() == 1 );
               newface->removeNewBoundary();
            }
            else
            {
               newface->checkFinalVertex();
               newface->normaliseNewBoundary();
            }
            removedVertex = false;
         }
         else
            // Do not normalise unmodified boundaries
            newface->unnormalised_boundaries = false;
         if ( !next.done() )
            newface->addBoundary();
      }

      b1 = next;
   }

   newface->normalise();
   // TODO: consider ignoring duplicates and trivial faces, either here or in Embedding::addVertex
   result.push_back( newface );
}

void Face::normalise()
{
   assert( unnormalised_boundaries == false );

   //cerr << "normalising \n";  print( cerr );

   // All boundaries should be normalised
   std::sort( boundaries.begin(), boundaries.end(), BoundaryComparer( *this ) );

   if ( boundaries[0].reversed )
   {
      // In order to identify the face with its reflection, require the first
      // boundary is unreversed
#ifdef PRESERVE_ORIENTATION
      assert( false );
#endif

      for (int i = 0; i < boundaries.size(); i++)
      {
         Boundary B( *this, boundaries[i] );
         B.reverse();
      }
      assert( !boundaries[0].reversed );
   }

   // Now compute hash and mask.
   // MSB of hash marks whether the hash contains all information about the face,
   // or is just a hash.
   // Note: it is not necessary to take the 'reversed' flags of boundaries into
   // account in either of the two schemes, because taken together, the
   // boundaries are a canonical representation of the face WITHOUT needing to
   // know the 'reversed' flags.
   
   uint64_t ret = 0;
   if ( vertices.size() <= 12 )
   {
      //cerr << "hashing " << vertices.size() << " " << boundaries.size() << "\n";
      // 4 bits per vertex
      for (int i = 0; i < boundaries.size(); i++)
      {
         Boundary B = boundary( i );
         for (int j = 0; j < B.size(); j++)
            ret = (ret << 4) | B[j];
      }

      // 1 bit per vertex to mark boundary lens
      int shift = 60;
      for (int i = 0; i < boundaries.size(); i++)
      {
         shift -= boundaries[i].len;
         ret |= (uint64_t)1 << shift;
      }

      // total: at most 12*5 + 3 = 63
      assert( (ret >> 63) == 0 );
   }
   else
   {
      for (int i = 0; i < boundaries.size(); i++)
      {      
         ret = MurmurHash64A( &vertices[boundaries[i].start], boundaries[i].len, ret );
      }
      ret |= (uint64_t)1 << 63;
   }
   hash = ret;

   mask = 0;
   for (int i = 0; i < vertices.size(); i++)
      mask |= 1 << vertices[i];

   normalised = true;
}

int Face::maxBoundaryLength() const
{
   int ret = 0;
   for (int i = 0; i < boundaries.size(); i++)
      ret = std::max( ret, boundaries[i].len );
   return ret;
}

int Face::numLongBoundaries() const
{
   int ret = 0;
   for (int i = 0; i < boundaries.size(); i++)
      if ( boundaries[i].len >= 3 )
         ret++;
   return ret;
}

bool Face::isSubFaceOfEx( int bnum, const Face &other, vector<bool> &used_boundaries, int bit_offset, int &bitnum, dir_t &allowed_dirs ) const
{
   ConstBoundary B1 = boundary( bnum );
   //uint32_t mask = 0;

   if ( B1.size() == 1 )
      // True automatically for size 1 boundaries, since the mask matches, so the vertex is present
      return true;

   if ( B1.size() == 2 )
      // If the vertices appear adjacently, they don't imped any further matching
      // Note, also orientationless, ignore allowed_dirs
      if ( other.hasAdjacentVertices( B1[0], B1[1] ) )
         return true;

   // If there are multiple lhs boundaries, then their relative orientations matter!
   // We have to reverse either all or none while matching to the other face's boundaries.
   // allowed_directions details 

   for (int bi = 0; bi < other.boundaries.size(); bi++)
   {
      ConstBoundary B2 = other.boundary( bi );

      if ( !used_boundaries[ bit_offset + bi ] )
      {
         DBG_REDUCE(
            cerr << "Testing ";
            B2.print( cerr );
            cerr << " isSupBoundaryOf ";
            B1.print( cerr );
            );
   
         if ( B2.isSupBoundaryOf( B1, allowed_dirs ) )
         {
            DBG_REDUCE( cerr << ": success\n" );
            //mask |= 1 << bi;
            bitnum = bit_offset + bi;
            used_boundaries[ bit_offset + bi ] = true;
            return true;
         }
         else
            DBG_REDUCE( cerr << ": failure\n" );
      }
   }
   return false;
}

bool Face::isSubFaceOfRecurse( int bnum, const Face &other, vector<bool> &used_boundaries, int bit_offset, dir_t allowed_dirs ) const
{
   // Note allowed_dirs passed by value

   // FIXME: there's no branching on whether to choose boundary here or not! This is mostly greedy rather than brute force!!

   int bitnum = -1;
   if ( !isSubFaceOfEx( bnum, other, used_boundaries, bit_offset, bitnum, allowed_dirs ) )
      return false;
   // isSubFaceOfEx may have set bit bitnum in used_boundaries; we need to clear that bit if we backtrack
   // (Allowed_dirs also potentially modified)

   if ( bnum + 1 == boundaries.size() )
      return true;

   if ( isSubFaceOfRecurse( bnum + 1, other, used_boundaries, bit_offset, allowed_dirs ) )
      return true;

   // Failure; backtrack
   if ( bitnum != -1 )
   {
      assert( used_boundaries[ bitnum ] );
      used_boundaries[ bitnum ] = false;
   }
   return false;
}

bool Face::isSubFaceOf( const Face &other, vector<bool> &used_boundaries, int bit_offset ) const
{
   assert( (getMask() & other.getMask()) == getMask() );

   // Recurse to try every possible matching of boundaries of *this to boundaries of other
   bool ret = isSubFaceOfRecurse( 0, other, used_boundaries, bit_offset, BOTHDIRS );
/*
   if ( boundaries.size() > 1 && ret )
   {
      cerr << "Example of multi-boundaried face matching:\n";
      print( cerr );
      cerr << "\n TO \n";
      other.print( cerr );
   }
*/
   return ret;
}

bool Face::operator<( const Face &rhs ) const
{
   if ( this == &rhs )
      return false;
   bool ret;
   uint64_t h1 = getHash(), h2 = rhs.getHash();
   if ( (h1 & h2) >> 63 )
   {
      // Can't use hashes to check equality
      if ( h1 == h2 )
         ret = std::lexicographical_compare(
            boundaries.begin(), boundaries.end(), rhs.boundaries.begin(), rhs.boundaries.end(), BoundaryComparer( *this )
            );
      else
         ret = h1 < h2;
   }
   else
      ret = h1 < h2;
   //assert( (vertices < rhs.vertices) == ret );
   return ret;
}

bool Face::boundariesEqual( const BoundaryDesc &d1, const BoundaryDesc &d2 ) const
{
   ConstBoundary B1( *this, d1 ), B2( *this, d2 );
   return B1 == B2;
}

// Faster than two calls to operator<
bool Face::operator==( const Face &rhs ) const
{
   if ( this == &rhs )
      return true;
   bool ret;
   uint64_t h1 = getHash(), h2 = rhs.getHash();
   //cerr << "Face::operator== hashes=" << h1 << " " << h2 << "\n";
   if ( (h1 & h2) >> 63 )
   {
      // Can't use hashes to check equality
      if ( h1 == h2 )
      {
         if ( boundaries.size() != rhs.boundaries.size() )
             ret = false;
         else
         {
             ret = true;
             for (int i = 0; i < boundaries.size(); i++)
             {
                 if ( !boundariesEqual( boundaries[i], rhs.boundaries[i] ) )
                 {
                     ret = false;
                     break;
                 }
             }
         }
      }
      else
         ret = false;
   }
   else
      ret = h1 == h2;
   //assert( (vertices == rhs.vertices) == ret );
   return ret;
}

void Face::print( ostream &os ) const
{
   os << "  Face";
   for (int i = 0; i < boundaries.size(); i++)
      boundary( i ).print( os );
   if ( !normalised )
      os << " NOT NORM";
   os << " hash=";
   if ( !(getHash() >> 63) )
      os << "*";
   os << std::hex << getHash() << " mask=" << getMask() << std::dec;
   os << "\n";
}

void Face::checkValid() const
{
   assert( boundaries.size() );
   assert( boundaries.size() <= MAX_BOUNDARIES );
   // Check for consecutive dups
   for (int i = 0; i < boundaries.size(); i++)
   {
      ConstBoundary B = boundary( i );
      assert( B.size() );
      assert( B.size() == 1 || (B.last() != B[0]) );
      for (int j = 0; j < B.size() - 1; j++)
         assert( B[j] != B[j + 1] );
   }
}

// For debugging
int Face::getLengthsSeq() const
{
   vector<int> lengths;
   for (int i = 0; i < boundaries.size(); i++)
      lengths.push_back( boundaries[i].len );
   std::sort( lengths.begin(), lengths.end() );
   int ret = 0;
   for (int i = 0; i < lengths.size(); i++)
      ret = ret * 100 + lengths[i];
   return ret;
}

//------------------------------------------------------------------------------------------------

Embedding::Embedding( const Component *parent_, int capacity )
{
   faces.reserve( capacity );
   normalised = false;
   parent = parent_;
   apex_mask = 0x0;
}

Embedding::Embedding( const Embedding &rhs, int spare_faces, const Component *newparent )
{
   faces.reserve( rhs.faces.size() + spare_faces );
   faces.insert( faces.begin(), rhs.faces.begin(), rhs.faces.end() );

   if ( newparent )
      parent = newparent;
   else
      parent = rhs.parent;
   surface = rhs.surface;
   apex_mask = rhs.apex_mask;
   hash = rhs.hash;
   normalised = rhs.normalised;
}

// Boundary size is pathwidth plus one!
Embedding::Embedding( int vertex, const Component *parent_ )
{
   normalised = false;
   parent = parent_;
   apex_mask = 0;

   // "external" face
   shared_ptr<Face> newface( new Face( 1 ) );
   newface->addBoundary();
   newface->addVertex( vertex );
   newface->normaliseNewBoundary();
   // The new boundary is trivially normalised
   //newface->unnormalised_boundaries = false;
   addFace( newface );
   normalise();
}

/*
// Boundary size is pathwidth plus one!
Embedding::Embedding( int bs, int handles_, int crosscaps_ )
{
   unused_handles = handles_;
   unused_crosscaps = crosscaps_;
   handles = crosscaps = 0;
   // "external" face
   Face newface( bs );
   for (int i = 0; i < bs; i++)
   {
      newface.addBoundary();
      newface.addVertex( i );
      newface.normaliseNewBoundary();
      // The new boundary is trivially normalised
      //newface.unnormalised_boundaries = false;
   }
   faces.insert( newface );
   normalise();
}
*/

// static
void Embedding::joinWithEdge( Component &result, const Embedding &E1, const Embedding &E2, int v1, int v2 )
{
   // For every face of E1 containing v1,
   // and every face of E2 containing v2,
   // take the connected sum

   // TODO: possible optimisation: make E1 the larger embedding

   uint32_t mask1 = (1 << v1);
   uint32_t mask2 = (1 << v2);

   FaceSet::const_iterator face_it1, face_it2;
   for ( face_it1 = E1.faces.begin(); face_it1 != E1.faces.end(); ++face_it1 )
   {
      if ( (*face_it1)->getMask() & mask1 )
      {
         // Partial template
         Embedding templ_left( &result, E1.faces.size() );
         templ_left.surface = E1.surface + E2.surface;
         // Would like to test this, but result.maxsurface invalid
         //assert( templ_left.surface.isSubsurfaceOf( result.maxsurface ) );

         // Add all faces except this one
         templ_left.addFaces( E1.faces.begin(), face_it1 );
         templ_left.addFaces( incIt( face_it1 ), E1.faces.end() );

         for ( face_it2 = E2.faces.begin(); face_it2 != E2.faces.end(); ++face_it2 )
         {
            if ( (*face_it2)->getMask() & mask2 )
            {
               // Create a template with every face except (one copy of) this one
               // TODO: possible optimisation: skip over spans of identical faces

               Embedding templ_complete( templ_left, E1.faces.size() + E2.faces.size() );
               // Add all faces except this one
               templ_complete.addFaces( E2.faces.begin(), face_it2 );
               templ_complete.addFaces( incIt( face_it2 ), E2.faces.end() );

               DBG2( cerr << "Embedding::joinWithEdge(" << v1 << "," << v2 << ") complete template:\n"; templ_complete.print( cerr ) );

               Face newface( **face_it1, **face_it2, false );

               DBG2( cerr << "Embedding::joinWithEdge newface:\n"; newface.print( cerr ) );

               bool success = newface.addEdge( v1, v2, NULL, templ_complete, result, true );
               assert( success );

               // We also need to try joining *face_it1 with the reflection of *face_it2.
               // The reflection of a face is often identical, if so can skip it.
               if ( !(*face_it1)->orientationless() && !(*face_it2)->orientationless() )
               {
                  Face newface( **face_it1, **face_it2, true );

                  DBG2( cerr << "Embedding::joinWithEdge newface2:\n"; newface.print( cerr ) );

                  bool success = newface.addEdge( v1, v2, NULL, templ_complete, result, true );
                  assert( success );
               }
            }
         }
      }
   }
}

void Embedding::normalise()
{
   std::sort( faces.begin(), faces.end(), deref_comp<Face>() );

   // Apices and other parameters should be included in the hash.
   // However, ideally if we have two embeddings with identical faces but which
   // differ in exactly one parameter, we can throw one away.
   // (Doesn't isSubEmbedding do that?)
   const uint64_t m = 0xc6a4a7935bd1e995ULL;
   hash = surface.handles + (surface.crosscaps << 8) << (surface.apices << 16);
   hash *= m;
   for ( FaceSet::iterator it = faces.begin(); it != faces.end(); ++it )
   {
      hash += (*it)->getHash() ^ (hash >> 26);
      hash *= m;
   }

   normalised = true;
/*
  for ( set<Face>::iterator it = faces.begin(); it != faces.end(); ++it )
  {
  hash = Murmurhash64A( &*it->vertices.begin(), it->vertices.size() * sizeof(int), hash );
  }
*/
}

// Add every possible extension of this embedding by an edge to 'result'
void Embedding::addEdge( Component &result, int v1, int v2 ) const
{
   assert( v1 != v2 );
   unsigned int mask = (1 << v1) | (1 << v2);

   FaceSet::const_iterator it, temp_it;  //, new_face;

#ifdef SRULE2
   // Check whether there exists a 2-gon embedding. If so, all other embeddings
   // of the edge can be ignored

   for ( it = faces.begin(); it != faces.end(); ++it )
   {
      // Check it has both boundary vertices
      if ( ((*it)->getMask() & mask) == mask )
      {
         if ( (*it)->hasAdjacentVertices( v1, v2 ) )
         {
            result.addEmbedding( *this, true );  // Nonminimal
            return;
         }
      }
   }

#endif

   bool first = true;
   for ( it = faces.begin(); it != faces.end(); ++it )
   {
      // Check it has both boundary vertices
      if ( (*it)->getMask() & mask )
      {
         // Optimisation: skip over spans of identical faces
         if ( !first && (*it)->getHash() == (*(it - 1))->getHash() )
            continue;

         // Create a template with every face except (one copy of) this one
         Embedding templ( &result, faces.size() + 1 );
         templ.copyTopology( *this );
         templ.addFaces( faces.begin(), it );
         templ.addFaces( it + 1, faces.end() );

         //cerr << "TEMPLATE:\n"; templ.print(cerr);

         //set<Face> new_faces = it->addEdge( v1, v2 );

         (*it)->addEdge( v1, v2, this, templ, result, false );
      }
      first = false;
   }
}

void Embedding::removeVertex( Component &result, int v1 ) const
{
   unsigned int mask = 1 << v1;

   Embedding new_embedding( &result, faces.size() );
   new_embedding.copyTopology( *this );

   FaceSet::const_iterator it, temp_it;
   for ( it = faces.begin(); it != faces.end(); ++it )
   {
      // FIXME: when can we throw away duplicate faces and when can we not?

      if ( (*it)->getMask() == mask )
      {
         // Last remaining vertex; throw away face
         continue;
      }
      else if ( (*it)->getMask() & mask )
      {
         // Incident to the boundary vertex
         (*it)->removeVertex( v1, new_embedding.faces );
      }
      else
      {
         // The face doesn't have to be modified
         new_embedding.addFace( *it );
      }
   }

   // It's possible to form 1-gons and 2-gons, so get rid of those if possible
   new_embedding.reduceFaces();

   assert( new_embedding.faces.size() );
   result.addEmbedding( new_embedding );
}

void Embedding::reduceFaces()
{
   // Throw away faces which are redundant to other faces.
   // This is a much weaker version of Component::reduce, and only needs to
   // occur after removeVertex

   // FIXME! Use a stronger reduction rule some of the time to try to speed things up

   FaceSet::iterator it, it2;
   //for ( it = faces.begin(); it != faces.end(); )
   for (int i = 0; i < faces.size(); )
   {
      it = incIt( faces.begin(), i );
      i++;
      // Faces with more than one boundary are too complex to bother with...
      if ( (*it)->numBoundaries() == 1 && (*it)->size() <= 2 )
      {
         // Look around (not necessarily a single boundary this time)
         // It's OK to merge it into another size 1 or 2 face
         for ( it2 = faces.begin(); it2 != faces.end(); ++it2 )
         {
            if ( it2 != it && (((*it2)->getMask() & (*it)->getMask()) == (*it)->getMask()) )
            {
               if ( (*it)->size() == 2 )
               {
                  // Additional check for size 2 only
                  if ( !(*it2)->hasAdjacentVertices( (*it)->vertex( 0 ), (*it)->vertex( 1 ) ) )
                     continue;
               }

               DBG_REDUCE(
                  cerr << "dropping redundant face ";
                  (*it)->print( cerr );
                  cerr << " for ";
                  (*it2)->print( cerr );
                  );
               faces.erase( it );
               i--;
               break;
            }
         }
      }
   }
}

//static
bool Embedding::faceLengthHeuristic( const Face *lhs, const Face *rhs )
{
   return lhs->maxBoundaryLength() < rhs->maxBoundaryLength();
}

// Can any extension embeddable using lhs also be embedded using rhs?
// That is, is the rhs more "general"?
bool Embedding::isSubEmbeddingOf( const Embedding &rhs ) const
{
   // If the rhs uses more handles, etc, it's not a more general embedding
   if ( !rhs.surface.isSubsurfaceOf( surface ) )
      return false;

   FaceSet::const_iterator it, r_it;

   int total_boundaries = 0;
   int lhs_long_boundaries = 0, rhs_long_boundaries = 0;
   for ( it = faces.begin(); it != faces.end(); it++ )
   {
      lhs_long_boundaries += (*it)->numLongBoundaries();
   }

   for ( r_it = rhs.faces.begin(); r_it != rhs.faces.end(); r_it++ )
   {
      total_boundaries += (*r_it)->numBoundaries();
      rhs_long_boundaries += (*r_it)->numLongBoundaries();
   }

   // First, a quick check: since in this simple scheme, each len >= 3 boundary
   // matches at most one len >= 3 boundary, compare counts
   // TODO: prehaps recheck this in the loop below?
   if ( lhs_long_boundaries > rhs_long_boundaries )
      return false;

   // TODO (if more speed required): skip testing if lhs's longest boundary is longer than rhs's
   // (pre-calculate longest boundary)

   // The boundaries of rhs which have been used in a way that makes the full face
   // unavailable for further matching
   vector<bool> used_boundaries( total_boundaries, false );

   // Order rhs faces from fewest to most vertices (prehaps multi-boundaried
   // faces should at the end) in order to prefer using the "weakest" boundary possible
   // Should also do a separate pass for adjacent embedding of 2 vertex boundaries
   // Note: in practise, this made no difference to number of nodes found minimal :(

   vector<const Face *> rhs_faces;
   rhs_faces.reserve( total_boundaries );  // upper bound

   for ( r_it = rhs.faces.begin(); r_it != rhs.faces.end(); r_it++ )
      rhs_faces.push_back( &**r_it );

   std::sort( rhs_faces.begin(), rhs_faces.end(), Embedding::faceLengthHeuristic );

   for ( it = faces.begin(); it != faces.end(); it++ )
   {
      int bound_num = 0;
      vector<const Face *>::iterator r_it2;
      //for ( r_it = rhs.faces.begin(); r_it != rhs.faces.end(); r_it++ )
      for ( r_it2 = rhs_faces.begin(); r_it2 != rhs_faces.end(); r_it2++ )
      {
         if ( ((*it)->getMask() & (*r_it2)->getMask()) == (*it)->getMask() )
         {
            if ( (*it)->isSubFaceOf( **r_it2, used_boundaries, bound_num ) )
            {
               goto success;
            }
         }
         bound_num += (*r_it2)->numBoundaries();
         assert( bound_num <= total_boundaries );
      }
      return false;

     success:
      ;
   }
   return true;
}

bool Embedding::unusedHandles() const
{
   return parent->maxHandles() > surface.handles;
}

bool Embedding::unusedCrosscaps() const
{
   return parent->maxCrosscaps() > surface.crosscaps;
}

bool Embedding::unusedApices() const
{
   return parent->maxApices() > surface.apices;
}

bool Embedding::operator<( const Embedding &rhs ) const
{
   if ( getHash() != rhs.getHash() )
      return getHash() < rhs.getHash();

   // Don't trust the hash... full comparison
   if ( !(surface == rhs.surface) )
      return surface < rhs.surface;
   if ( faces.size() != rhs.faces.size() )
      return faces.size() < rhs.faces.size();
   assert( normalised );

   // Why doesn't faces < rhs.faces work?!
   FaceSet::const_iterator it = faces.begin(), r_it = rhs.faces.begin();
   for (  ; it != faces.end(); ++it, ++r_it )
      if ( !(**it == **r_it) )
         return **it < **r_it;
   // equal
   return false;
}

bool Embedding::operator==( const Embedding &rhs ) const
{
   if ( getHash() != rhs.getHash() )
      return false;
   //cerr << "Embedding::operator==\n";
   bool ret;
   if ( !(surface == rhs.surface) )
      ret = false;
   else
   {
      assert( normalised );
      ret = true;
      // Why doesn't faces == rhs.faces work?!
      FaceSet::const_iterator it = faces.begin(), r_it = rhs.faces.begin();
      for (  ; it != faces.end(); ++it, ++r_it )
      {
         if ( !(**it == **r_it) )
         {
            //cerr << "ineq\n";
            ret = false;
            break;
         }
      }
      //ret = std::equal( faces.begin(), faces.end(), rhs.faces.begin(), deref_comp<Face>() );
   }
   if ( !ret )
   {
      cerr << "Embedding::operator==: hash failed! ret=" << ret << "\n";
      print( cerr );
      rhs.print( cerr );
   }
   return ret;
}

void Embedding::addFaceFromString( const char *str )
{
   shared_ptr<Face> newface( new Face() );
   newface->buildFromString( str );
   addFace( newface );
   normalise();
}

void Embedding::print( ostream &os ) const
{
   os << " Embedding";
   if ( !normalised )
      os << " NOT NORM";
   os << " hash=" << std::hex << getHash() << std::dec
      << "\t\t" << surface << "\n";
   FaceSet::const_iterator it;
   for ( it = faces.begin(); it != faces.end(); ++it )
      (*it)->print( os );
}

void Embedding::checkValid( uint32_t expected_mask ) const
{
   //cerr << "checking validity:\n";
   //print(cerr);

   assert( faces.size() );

   uint32_t mask = 0;
   FaceSet::const_iterator it;
   for ( it = faces.begin(); it != faces.end(); ++it )
   {
      (*it)->checkValid();
      mask |= (*it)->getMask();
   }
   // Check every vertex appears
   assert( mask == expected_mask );

   // Check every edge appears exactly twice
}

//------------------------------------------------------------------------------------------------

Component::Component()
{
   mask = 0;
   totalsize = 0;
   numvertices = 0;
   newstate = NULL;
   maxsurface.invalid();
}

Component::Component( int vertex )
{
   embeddings.insert( Embedding( vertex, this ) );
   mask = 1 << vertex;
   totalsize = 1;
   numvertices = 1;
   newstate = NULL;
   maxsurface.invalid();
}

Component::Component( const Component &rhs )
{
   assert( !rhs.newstate );
   set<Embedding>::iterator it, it2;

   // Copy constructing Embeddings is not allowed, because it breaks the parent pointer
   for ( it = rhs.embeddings.begin(); it != rhs.embeddings.end(); ++it )
      embeddings.insert( Embedding( *it, 0, this ) );

   mask = rhs.mask;
   totalsize = rhs.totalsize;
   numvertices = rhs.numvertices;
   newstate = NULL;
   maxsurface = rhs.maxsurface;
}

// static
Component *Component::joinWithEdge( Component &C1, Component &C2, int v1, int v2 )
{
   Component &newcomp = *new Component();

   assert( !(C1.mask & C2.mask) );
   assert( ( 1 << v1 ) & C1.mask );
   assert( ( 1 << v2 ) & C2.mask );
   newcomp.mask = C1.mask | C2.mask;

   // No need for a separate set
   newcomp.newstate = &newcomp.embeddings;

   // Because we are joining, never need to use a handle or crosscap. What about
   // apices and covering faces? Need to set those here
   newcomp.maxsurface.invalid();

   // For every face of every embedding in C1 containing v1,
   // and every face of every embedding in C2 containing v2,
   // take the connected sum

   set<Embedding>::iterator em_it1, em_it2;

   for ( em_it1 = C1.embeddings.begin(); em_it1 != C1.embeddings.end(); ++em_it1 )
   {
      for ( em_it2 = C2.embeddings.begin(); em_it2 != C2.embeddings.end(); ++em_it2 )
      {
         // FIXME! do parent component pointers always point to newcomp?
         Embedding::joinWithEdge( newcomp, *em_it1, *em_it2, v1, v2 );
      }
   }

   newcomp.totalsize = C1.totalsize + C2.totalsize + newcomp.embeddings.size();
   newcomp.numvertices = C1.numvertices + C2.numvertices;
   newcomp.newstate = NULL;
   return &newcomp;
}

void Component::addEmbedding( const Embedding &embedding, bool nonminimal )
{
//   cerr << "addEmbedding\n";
   assert( newstate );
   assert( embedding.parent == this );
   newstate->insert( embedding );
   EmbedState::incEmbeddingsBuilt();
   // TODO: nominimal
}

void Component::addEdge( int v1, int v2 )
{
   newstate = new set<Embedding>;

   set<Embedding>::iterator it;
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
   {
      it->addEdge( *this, v1, v2 );
   }
   embeddings.swap( *newstate );
   delete newstate;
   newstate = NULL;
   totalsize += embeddings.size();

#ifndef NDEBUG
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
      it->checkValid( mask );
#endif
}

void Component::removeVertex( int v1 )
{
   assert( (1 << v1) & mask );

   newstate = new set<Embedding>;
   // TODO: removeVertex is pretty inefficient... but addEdge is the bottleneck, so not worth fixing

   set<Embedding>::iterator it;
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
   {
      it->removeVertex( *this, v1 );
   }
   embeddings.swap( *newstate );
   delete newstate;
   newstate = NULL;

   totalsize += embeddings.size();
   numvertices--;
   mask &= ~(1 << v1);

#ifndef NDEBUG
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
      it->checkValid( mask );
#endif
}

int Component::numEmbeddings() const
{
   // hacky...
   if ( newstate )
      return newstate->size();
   return embeddings.size();
}

SurfaceParam Component::minSurface() const
{
   SurfaceParam ret;
   ret.handles = 999;
   ret.crosscaps = 999;
   ret.apices = 999;
   set<Embedding>::iterator it;
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
   {
      ret = ret.minimum( it->surface );
   }
   return ret;
}

void Component::setLimits( SurfaceParam &new_surface )
{
#ifndef NDEBUG
   if ( !maxsurface.isInvalid() )
      assert( new_surface.isSubsurfaceOf( maxsurface ) );
#endif
   if ( !maxsurface.isSubsurfaceOf( new_surface ) )
   {
      // New surface smaller. Drop embeddings exceeding the new limit
      set<Embedding>::iterator it, next;
      for ( it = embeddings.begin(); it != embeddings.end();  )
      {
         next = incIt( it );
         if ( !it->surface.isSubsurfaceOf( new_surface ) )
         {
            embeddings.erase( it );
         }
         it = next;
      }
   }
   maxsurface = new_surface;
}

void Component::reduce()
{
   // Check the list of embeddings for embeddings that are inferior to others:
   // those should be removed

   // TODO: First sort embeddings by decreasing maximum boundary length?

   if ( isEmpty() )
      return;

   set<Embedding>::iterator it, it2, next;
   for ( it = embeddings.begin(); it != embeddings.end(); )
   {
      next = it;
      ++next;

      //for ( it2 = embeddings.begin(); it2 != it; ++it2 )
      for ( it2 = embeddings.begin(); it2 != embeddings.end(); ++it2 )
      {
         if ( it != it2 && it->isSubEmbeddingOf( *it2 ) )
         {
            embeddings.erase( it );
            break;
         }
      }

      it = next;
   }

   assert( !isEmpty() );
}

bool Component::leq( const Component &rhs ) const
{
   // Check whether every embedding for this component has a superior embedding
   // in rhs.
   assert( getMask() == rhs.getMask() );

   set<Embedding>::iterator l_it, r_it, r_it2;
   r_it = rhs.embeddings.begin();
   for ( l_it = embeddings.begin(); l_it != embeddings.end(); ++l_it )
   {
      // Skip embeddings not in *this
      // TODO: try faster hash comp
      //while ( r_it != rhs.embeddings.end() && r_it->getHash() < l_it->getHash() )
      while ( r_it != rhs.embeddings.end() && *r_it < *l_it )
         ++r_it;

      if ( r_it == rhs.embeddings.end() || !(*r_it == *l_it) )
      {
         // If no exact match, look for a superior

         for ( r_it2 = rhs.embeddings.begin(); r_it2 != rhs.embeddings.end(); ++r_it2 )
         {
            DBG_REDUCE(
               cerr << "Checking whether\n";
               l_it->print( cerr );
               cerr << "isSubEmbeddingOf\n";
               r_it2->print( cerr );
               );

            if ( l_it->isSubEmbeddingOf( *r_it2 ) )
            {
               DBG_REDUCE( cerr << "success!\n" );
               goto success;
            }
         }
         return false;

        success:
         continue;
      }
   }
   return true;
}

bool Component::identical( const Component &rhs ) const
{
   if ( getMask() != rhs.getMask() )
      return false;
   return embeddings == rhs.embeddings;
}

void Component::printstats( ostream &os ) const
{
   SurfaceParam minsurface = minSurface();
   os << "Component, vertices=" << numvertices << " mask=" << std::hex << mask << std::dec
      << " embeddings=" << embeddings.size() << " totalsize=" << totalsize
      << " handles:" << minsurface.handles << "-" << maxsurface.handles
      << " crosscaps:" << minsurface.crosscaps << "-" << maxsurface.crosscaps
      << " apices:" << minsurface.apices << "-" << maxsurface.apices << "\n";
}

void Component::print( ostream &os ) const
{
   printstats( os );
   set<Embedding>::iterator it;
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
      it->print( os );
}

// For debugging only
uint64_t Component::getHash() const
{
   uint64_t ret = 0ULL;
   set<Embedding>::iterator it;
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
      ret += it->getHash() ^ (ret >> 47);
   return ret;
}

// Assert that there is an embedding matching a sequence of side counts
// sidesSeq is a sorted sequence where each element encodes the length of each boundary:
// length(longest) + 100 * length(2nd longest) + 10000 * length(3rd longest) ...
int Component::assertHaveEmbedding( int seqLen, int sidesSeq[] ) const
{
   int matches_seen = 0;

   set<Embedding>::iterator it;
   for ( it = embeddings.begin(); it != embeddings.end(); ++it )
   {
      vector<int> sides;
      FaceSet::const_iterator faceit;
      for ( faceit = it->faces.begin(); faceit != it->faces.end(); ++faceit )
         sides.push_back( (*faceit)->getLengthsSeq() );
      std::sort( sides.begin(), sides.end() );
      if ( sides.size() == seqLen && memcmp( &*sides.begin(), sidesSeq, sizeof(int) * sides.size() ) == 0 )
         matches_seen++;
   }
   //assert( matches_seen );
   cerr << " matches: " << matches_seen << "\n";
   return matches_seen;
}

//------------------------------------------------------------------------------------------------

EmbedState::EmbedState( int bs, int handles, int crosscaps, int apices )
{
   sorted = false;
   boundarysize = bs;
   assert( handles >= 0 && crosscaps >= 0 && apices >= 0 );
   assert( handles <= MAX_GENUS );
   surface.handles = handles;
   surface.crosscaps = crosscaps;
   surface.apices = apices;
   min_surface.invalid();

   //embeddings.insert( Embedding( bs, genus ) );
   for (int i = 0; i < bs; i++)
      components.push_back( new Component( i ) );
   embeddings_built = maxsize = totalsize = bs;
   updateLimits();
}

EmbedState::EmbedState( const EmbedState &rhs )
{
   for (int i = 0; i < rhs.components.size(); i++)
      components.push_back( new Component( *rhs.components[i] ) );

   boundarysize = rhs.boundarysize;
   maxsize = rhs.maxsize;
   totalsize = rhs.totalsize;
   embeddings_built = rhs.embeddings_built;
   surface = rhs.surface;
   interior_surface = rhs.interior_surface;
   min_surface = rhs.min_surface;
   sorted = rhs.sorted;
}

EmbedState::~EmbedState()
{
   vector<Component *>::iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
   {
      delete *it;
      *it = NULL;
   }
}

// This could be more efficient: only examining the one or two components actually modified
void EmbedState::updateLimits()
{
   min_surface = SurfaceParam();

   vector<Component *>::iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
   {
      min_surface = min_surface + (*it)->minSurface();
   }

   SurfaceParam unused_surface = surface - min_surface - interior_surface;

   for ( it = components.begin(); it != components.end(); ++it )
   {
      SurfaceParam new_surface = unused_surface + (*it)->minSurface();
      (*it)->setLimits( new_surface );
   }
}

void EmbedState::updateStats()
{
   maxsize = std::max( maxsize, numEmbeddings() );
   embeddings_built += _embeddings_built;
   _embeddings_built = 0;
}

void EmbedState::addEdge( int v1, int v2 )
{
   DBG( cerr << "adding " << v1 << "," << v2 << "\n" );
   assert( _embeddings_built == 0 );

   uint32_t mask = (1 << v1) | (1 << v2);
   bool done = false;

   Component *match1 = NULL, *match2;
   vector<Component *>::iterator it, first_comp;
   for ( it = components.begin(); it != components.end(); ++it )
   {
      if ( ((*it)->getMask() & mask) == mask )
      {
         assert( !match1 );
         (*it)->addEdge( v1, v2 );
         totalsize += (*it)->numEmbeddings();
         done = true;
         break;
      }
      else if ( (*it)->getMask() & mask )
      {
         if ( !match1 )
         {
            // Note: might be a match for v2
            first_comp = it;
            match1 = *it;
         }
         else
         {
            // Join together the two components
            match2 = *it;
            // match1 should match v1
            if ( match2->getMask() & (1 << v1) )
               std::swap( match1, match2 );

            Component *newcomp = Component::joinWithEdge( *match1, *match2, v1, v2 );
            // TODO: what about the limits?
            totalsize += newcomp->numEmbeddings();
            delete match1;
            delete match2;
            *it = newcomp;
            *first_comp = NULL;
            components.erase( first_comp );
            
            done = true;
            break;
         }
      }

   }
   assert( done );

   // TODO: is this the best place?
   updateLimits();
   updateStats();
}

void EmbedState::addVertex( int v1 )
{
   DBG( cerr << "adding " << v1 << "\n" );
   assert( _embeddings_built == 0 );

   bool added = false;
   uint32_t mask = 1 << v1;

   vector<Component *>::iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
   {
      if ( (*it)->getMask() == mask )
      {
         // Just replace this component
         interior_surface = interior_surface + (*it)->minSurface();
         bool empty = (*it)->isEmpty();
         delete *it;

         // If the component is empty, it should stay empty
         // Note, if *it was empty, then its minSurface is garbage, so this isn't strictly needed
         if ( empty )
         {
            *it = new Component();
            (*it)->mask = 1 << v1;
         }
         else
         {
            *it = new Component( v1 );
            totalsize += 1;
         }
         added = true;
         break;
      }
      else if ( (*it)->getMask() & mask )
      {
         (*it)->removeVertex( v1 );
         totalsize += (*it)->numEmbeddings();
         break;
      }
   }

   if ( !added )
      components.push_back( new Component( v1 ) );
   //cerr << "EmbedState::addVertex added = " << added << " components = " << components.size() << "\n";

   // TODO: is this the best place?
   updateLimits();
   updateStats();

   sorted = false;

   // TODO: checkvalid
}

void EmbedState::compute( int ops[] )
{
   for ( int i = 0; ops[i] != -1; i++ )
   {
      int op = ops[i];

      if ( op < 10 )
         addVertex( op );
      else
         addEdge( op % 10, op / 10 );
   }
   reduce();
   sort();
}

#ifndef TESTING
void EmbedState::compute( const BPG &G )
{
   // adapted from BGraph::BGraph(const BPG &B, vertNum extra)

   unsigned char bndry[16] = {0};
   int bLen = G.length();
   int nextVert = 0;

   for (int i = 0; i < bLen; i++)
   {
      Operator op = G[i];

      if ( op.isVertexOp() )
      {
         vertNum vert = op.vertex();
         addVertex( vert );
         bndry[vert] = nextVert++;
      }
      else if ( op.isEdgeOp() )
      {
         vertNum vert1 = op.vertex1();
         vertNum vert2 = op.vertex2();

         addEdge( vert1, vert2 );
         //blissgraph.add_edge( bndry[vert1], bndry[vert2] );
      }
   }
   //reduce();
   sort();
}


/*
void EmbedState::lookahead( const BPG &G )
{
   // adapted from BGraph::BGraph(const BPG &B, vertNum extra)

   enum partition_t { before, boundary, after };

   BGraph B(G);

   vector<partition_t> partition( B.vertices(), before );

   // Map every vertex in the graph to 
   vector<int> component_ids( B.vertices(), -1 );

   // Map component ids to indices in 'components'
   vector<int> component_ids_to_indices;

   unsigned char bndry[16] = {0};
   int bLen = G.length();
   int nextVert = 0;

   for (int i = 0; i < bLen; i++)
   {
      Operator op = G[i];

      if ( op.isVertexOp() )
      {
         vertNum vert = op.vertex();
         addVertex( vert );
         bndry[vert] = nextVert++;
      }
      else if ( op.isEdgeOp() )
      {
         vertNum vert1 = op.vertex1();
         vertNum vert2 = op.vertex2();

         addEdge( vert1, vert2 );
         //blissgraph.add_edge( bndry[vert1], bndry[vert2] );
      }
   }
}
*/

#endif

void EmbedState::K( int vertices, int edges )
{
   edges = vertices * (vertices - 1) / 2 - edges;
   for (int i = 0; i < vertices; i++)
   {
      for (int j = i + 1; j < vertices; j++)
      {
         addEdge( i, j );
         if ( !--edges )
            return;
      }
   }
}

Component *EmbedState::addDebugComponent( int bs )
{
   boundarysize = bs;
   Component *ret = new Component();
   //newstate->insert( embedding );
   ret->mask = (1 << bs) - 1;
   ret->numvertices = bs;
   components.push_back( ret );
   updateLimits();
   return ret;
}

void EmbedState::addDebugEmbedding( const Embedding &embedding )
{
   assert( components.size() == 1 );
   //components[0]->addEmbedding( embedding );
   //_embeddings_built = 0;
   //assert( embedding.parent == components[0] );
   components[0]->embeddings.insert( embedding );
}

void EmbedState::reduce()
{
   vector<Component *>::iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
      (*it)->reduce();
}

bool EmbedState::isEmpty() const
{
   vector<Component *>::const_iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
   {
      if ( (*it)->isEmpty() )
         return true;
   }
   return false;
}

void EmbedState::sort() const
{
   EmbedState *thism = const_cast<EmbedState *>(this);
   std::sort( thism->components.begin(), thism->components.end(), Component::orderComponents ); //deref_comp<Component> );
   sorted = true;
}

bool EmbedState::leq( const EmbedState &rhs ) const
{
   if ( !sorted )
      sort();
   if ( !rhs.sorted )
      rhs.sort();
   
   // We assume that rhs is a transformation of lhs in such a way that if the
   // number of components has changed, then their congruence states are inequal.
   // This is not true in some degenerate cases, but I think it is most of the
   // time. More importantly, I don't see any other option.

   // Note that graphs below in the subgraph, (topological) minor, Y \Delta-transform,
   // H-bowtie and immersion orders never have fewer components if no vertices are
   // deleted.

   if ( components.size() != rhs.components.size() )
   {
      aassert( components.size() > rhs.components.size() );
      //return true; //false;
      return components.size() <= rhs.components.size();
   }

   for (int i = 0; i < components.size(); i++)
   {
      if ( components[i]->getMask() != rhs.components[i]->getMask() )
         //return true;
         aassert( false );
   }

   for (int i = 0; i < components.size(); i++)
   {
      if ( !components[i]->leq( *rhs.components[i] ) )
         return false;
   }
   return true;
}

bool EmbedState::identical( const EmbedState &rhs ) const
{
   if ( !sorted )
      sort();
   if ( !rhs.sorted )
      rhs.sort();

   if ( components.size() != rhs.components.size() )
      return false;

   for (int i = 0; i < components.size(); i++)
   {
      if ( !components[i]->identical( *rhs.components[i] ) )
         return false;
   }
   return true;
}

void EmbedState::checkValid() const
{
   int vertices = 0;
   uint32_t mask = 0;
   vector<Component *>::const_iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
   {
      vertices += (*it)->numvertices;
      assert( !(mask & (*it)->mask) );
      mask |= (*it)->mask;
   }
   assert( vertices == boundarysize );
}

void EmbedState::printstats( ostream &os ) const
{
   os << "--------\nEmbedState, embeddings stats: cur=" << numEmbeddings()
      << "/max=" << maxsize << "/total=" << totalsize << "/built=" << embeddingsBuilt()
      << " handles=" << handles() << " crosscaps=" << crosscaps() << " apices=" << apices()
      << " sorted=" << sorted << "\n";
}

void EmbedState::print( ostream &os ) const
{
   printstats( os );
   vector<Component *>::const_iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
      (*it)->print( os );
   os << "--------\n";
}

// For debugging only
uint64_t EmbedState::getHash() const
{
   uint64_t ret = 0ULL;
   vector<Component *>::const_iterator it;
   for ( it = components.begin(); it != components.end(); ++it )
      ret ^= (*it)->getHash();
   return ret;
}

int EmbedState::numEmbeddings() const
{
   int ret = 0;
   for (int i = 0; i < components.size(); i++)
   {
      assert( !components[i]->newstate );
      ret += components[i]->numEmbeddings();
   }
   return ret;
}

int EmbedState::assertHaveEmbedding( int seqLen, int sidesSeq[], uint32_t mask ) const
{
   if ( !mask )
   {
      assert( numComponents() == 1 );
      return components[0]->assertHaveEmbedding( seqLen, sidesSeq );
   }
   else
   {
      for (int i = 0; i < components.size(); i++)
      {
         if ( components[i]->getMask() == mask )
         {
            return components[i]->assertHaveEmbedding( seqLen, sidesSeq );
         }
      }
      assert( false );
      return 0;
   }
}

//------------------------------------------------------------------------------------------------
// Glue code

#ifndef TESTING

EmbedStateWrapper::EmbedStateWrapper( int bs, int handles, int crosscaps, int apices, int covering_faces )
{
   state = new EmbedState( bs, handles, crosscaps, apices );
}

EmbedStateWrapper::EmbedStateWrapper( const EmbedStateWrapper &rhs )
{
   state = new EmbedState( *rhs.state );
}

EmbedStateWrapper::~EmbedStateWrapper()
{
   delete state;
   state = NULL;
}

void EmbedStateWrapper::compute( const BPG &G )
{
   state->compute( G );
   //cerr << "computing state for " << G << "\tempty = " << state->isEmpty() << "\n";
}

bool EmbedStateWrapper::isEmpty() const
{
   return state->isEmpty();
}

bool EmbedStateWrapper::leq( const EmbedStateWrapper &rhs ) const
{
/*
   cerr << "LHS\n";
   state->print( cerr );
   cerr << "RHS\n";
   rhs.state->print( cerr );
*/
   return state->leq( *rhs.state );
}

bool EmbedStateWrapper::identical( const EmbedStateWrapper &rhs ) const
{
   return state->identical( *rhs.state );
}

int EmbedStateWrapper::size() const
{
   return state->numEmbeddings();
}

int EmbedStateWrapper::cost() const
{
   return state->embeddingsBuilt();
}

#endif // TESTING

//------------------------------------------------------------------------------------------------

#ifdef TESTING

int main()
{
//#if 0
   {
      EmbedState estate( 4 );

      estate.print( cout );

      estate.addEdge( 0, 2 );
      estate.print( cout );

      estate.addEdge( 2, 0 );
      estate.print( cout );

      estate.addEdge( 0, 1 );
      estate.print( cout );

      estate.addEdge( 0, 3 );
      estate.print( cout );

      estate.assertHaveEmbedding( 1, (int[]){ 6 } );

/*
  estate.addEdge( 1, 2 );
  estate.print( cout );

  estate.addEdge( 1, 0 );
  estate.print( cout );

  estate.addEdge( 0, 3 );
  estate.print( cout );

*/
      estate.addVertex( 2 );
      estate.print( cout );

      estate.addVertex( 0 );
      estate.print( cout );

      estate.addEdge( 0, 1 );
      estate.print( cout );

      estate.addEdge( 2, 0 );
      estate.print( cout );

      estate.addVertex( 2 );
      estate.print( cout );

      // To test: includes both orientations where distinct
   }
//#endif
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 1a: closing faces on the plane\n";

      EmbedState estate( 6 );

      estate.addEdge( 0, 1 );
      estate.addEdge( 1, 2 );
      estate.addEdge( 0, 2 );

      //estate.assertHaveEmbedding( 2, (int[]){ 3, 1010103 } );
      //estate.assertHaveEmbedding( 2, (int[]){ 103, 10103 } );
      estate.print( cout );
      estate.assertHaveEmbedding( 2, (int[]){ 3, 3 }, 0x7 );

      estate.addEdge( 3, 4 );
      estate.print( cout );
      estate.addEdge( 4, 5 );
      estate.print( cout );
      estate.addEdge( 3, 5 );

      //estate.assertHaveEmbedding( 3, (int[]){ 3, 3, 303 } );
      estate.assertHaveEmbedding( 2, (int[]){ 3, 3 }, 0x38 );
      estate.print( cout );

      estate.addEdge( 0, 3 );

      estate.print( cout );
      estate.assertHaveEmbedding( 3, (int[]){ 3, 3, 8 } );

      estate.addEdge( 1, 5 );
      estate.print( cout );
      estate.addEdge( 2, 4 );

      estate.assertHaveEmbedding( 5, (int[]){ 3, 3, 4, 4, 4 } );
      estate.print( cout );

      estate.addEdge( 1, 4 );
      estate.assertHaveEmbedding( 6, (int[]){ 3, 3, 3, 3, 4, 4 } );
      estate.addEdge( 2, 5 );
      assert( estate.isEmpty() );
      estate.print( cout );
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 2: disconnected nonplanar graph\n";
      EmbedState estate( 5 );
      estate.K( 5 );
      assert( estate.isEmpty() );
      for (int i = 0; i < 5; i++)
         estate.addVertex( i );
      assert( estate.isEmpty() );
      estate.K( 3 );
      assert( estate.isEmpty() );
   }

//#endif
//#if 0
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 3: Triangulated hexagon, plane\n";

      EmbedState estate( 7, 0 );
      for (int j = 0; j < 6; j++)
         estate.addEdge( j, 6 );

      for (int i = 0; i < 6; i++)
         estate.addEdge( i, (i + 1) % 6 );

      estate.print( cout );
      estate.printstats( cout );

      assert( estate.numEmbeddings() == ORIENTATIONS );
      estate.assertHaveEmbedding( 7, (int[]){ 3, 3, 3, 3, 3, 3, 6 } );
   }
//#endif
//#if 0
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 4a: torus C_4\n";

      EmbedState estate( 4, 1 );
      for (int i = 0; i < 4; i++)
      {
         estate.addEdge( i, (i + 1) % 4 );
         cout << "<<<<<<<<<<<, edge " << i << " " << (i+1)%4 << "\n";
         estate.print( cout );
      }
      estate.printstats( cout );
      estate.assertHaveEmbedding( 2, (int[]){ 4, 4 } );
      estate.assertHaveEmbedding( 1, (int[]){ 404 } );
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 4b: torus K_4\n";

      EmbedState estate( 4, 1 );
      for (int i = 0; i < 4; i++)
      {
         for (int j = i + 1; j < 4; j++)
         {
            estate.addEdge( i, j );
            //cout << "<<<<<<<<<<<, edge " << i << " " << j << "\n";
            //estate.print( cout );
         }
      }
      estate.printstats( cout );

      // Don't know if these are correct
      assert( estate.assertHaveEmbedding( 4, (int[]){ 3, 3, 3, 3 } ) == ORIENTATIONS );
      assert( estate.assertHaveEmbedding( 3, (int[]){ 3, 3, 303 } ) == 6 * ORIENTATIONS );
      assert( estate.assertHaveEmbedding( 2, (int[]){ 3, 9 } ) == 4 * ORIENTATIONS );
      assert( estate.assertHaveEmbedding( 2, (int[]){ 4, 8 } ) == 3 * ORIENTATIONS );
      assert( estate.numEmbeddings() == 14 * ORIENTATIONS );
   }
//#if 0
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 4c: torus K_5\n";

      EmbedState estate( 5, 1 );
      for (int i = 0; i < 5; i++)
      {
         for (int j = i + 1; j < 5; j++)
         {
            estate.addEdge( i, j );
            //estate.print( cout );
            //cerr << "substate " << i << " " << j << " " << estate.getHash() << "\n";
         }
      }
      //estate.print( cout );
      estate.printstats( cout );

      // 6 ways to embed K_5 on the torus
      assert( estate.numEmbeddings() ==
              estate.assertHaveEmbedding( 5, (int[]){ 3, 3, 3, 3, 8 } ) // x2
              + estate.assertHaveEmbedding( 5, (int[]){ 3, 3, 3, 4, 7 } )
              + estate.assertHaveEmbedding( 5, (int[]){ 3, 3, 4, 4, 6 } )
              + estate.assertHaveEmbedding( 5, (int[]){ 4, 4, 4, 4, 4 } )
              + estate.assertHaveEmbedding( 5, (int[]){ 3, 3, 4, 5, 5 } ) );

      // Don't know whether this is actually correct
      assert( estate.numEmbeddings() == 231 * ORIENTATIONS );
   }
//#endif
#if 0
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 5: torus K_7\n";

      EmbedState estate( 7, 1 );
      estate.K(7);
      //estate.print( cout );
      estate.printstats( cout );
      assert( estate.handles() == 1 );

      // One way to embed K_7 on the torus
      assert( estate.numEmbeddings() ==
              estate.assertHaveEmbedding( 14, (int[]){ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 } ) );

      // EmbedState, embeddings stats: cur=240/max=6120/total=39701/built=39701 handles=1 crosscaps=0
      //real	0m0.599s
      //user	0m0.586s
   }
//#endif
//#if 0
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 6a: torus obstruction\n";


      EmbedState estate( 6, 1 );
      for (int i = 0; i < 5; i++)
         for (int j = i + 1; j < 5; j++)
            estate.addEdge( i, j );
      assert( estate.handles() == 1 );

      for (int j = 0; j < 3; j++)
      {
         cout << j << " adding vertex\n";
         estate.printstats( cout );
         if ( j > 0 )
            estate.addVertex( 5 );
         cout << j << " adding edges\n";
         estate.printstats( cout );
         for (int i = 0; i < 5; i++)
         {
            if ( j == 2 && i == 4 )
               assert( !estate.isEmpty() );
            estate.addEdge( i, 5 );
            //cout << "j " << j << " i " << i << "\n";
            estate.printstats( cout );
         }
      }
      estate.print( cout );
      assert( estate.isEmpty() );

      //EmbedState, embeddings stats: cur=0/max=9000/total=28995/built=29270 handles=999 crosscaps=999
      //Component, vertices=6 mask=3f embeddings=0 totalsize=28996 handles:999-1 crosscaps:999-0
      //real	0m0.582s
      //user	0m0.563s
   }
#endif
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 6b: torus obstructions, K_5,K_5\n";

      // Try two edge deletions
      for (int j = 0; j < 3; j++)
      {
         EmbedState estate( 5, 1 );
         estate.K( 5, (j == 1 ? 1 : 0) );
         for (int i = 0; i < 5; i++)
            estate.addVertex( i );
         estate.K( 5, (j == 2 ? 1 : 0) );
         estate.printstats( cout );
         assert( estate.isEmpty() == (j <= 0) );
      }
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 7a: projective plane K_5\n";

      EmbedState estate( 5, 0, 1 );
      estate.K( 5 ) ;
      //estate.print( cout );
      estate.printstats( cout );

      int count1 = estate.assertHaveEmbedding( 6, (int[]){ 3, 3, 3, 3, 3, 5 } );
      int count2 = estate.assertHaveEmbedding( 6, (int[]){ 3, 3, 3, 3, 4, 4 } );

      // There are two ways of embedding K_5
      assert( estate.numEmbeddings() == count1 + count2 );
      assert( count1 == 12 );
      assert( count2 == 15 );  // I think?

   }
   // K_7 does not embed in P, but K_6 does. K_7 does not embed in the Klein bottle either
   // Maybe try embed the Franklin graph in the Klein bottle
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 7b: projective plane K_6\n";

      EmbedState estate( 6, 0, 1 );
      estate.K(6);
      estate.print( cout );
      estate.printstats( cout );
      // Don't know if this is correct
      assert( estate.numEmbeddings() == estate.assertHaveEmbedding( 10, (int[]){ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 } ) );
      assert( !estate.isEmpty() );
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 7c: projective plane K_7\n";

      EmbedState estate( 7, 0, 1 );
      estate.K(7);
      //estate.print( cout );
      estate.printstats( cout );
      assert( estate.isEmpty() );
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 7d: Klein bottle K_6\n";

      EmbedState estate( 6, 0, 2 );
      estate.K(6);
      //estate.print( cout );
      estate.printstats( cout );
      assert( !estate.isEmpty() );
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 7e: Klein bottle K_7\n";

      EmbedState estate( 7, 0, 2 );
      estate.K(7);
      //estate.print( cout );
      estate.printstats( cout );
      assert( estate.isEmpty() );
   }

   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 8: Projective plane obstruction, K_5,K_5\n";
      // Try two edge deletions
      for (int j = 0; j < 3; j++)
      {
         int mem = memory_used();
         EmbedState estate( 5, 0, 1 );
         estate.K( 5, (j == 1 ? 1 : 0) );
         for (int i = 0; i < 5; i++)
            estate.addVertex( i );
         estate.K( 5, (j == 2 ? 1 : 0) );
         estate.printstats( cout );
         assert( estate.isEmpty() == (j == 0) );
         cout << "mem usage: " << (memory_used() - mem) << "\n";
      }
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 9: Double torus, K_5,K_5\n";

      EmbedState estate( 5, 2 );
      estate.K( 5 );
      for (int i = 0; i < 5; i++)
         estate.addVertex( i );
      estate.K( 5 );
      estate.printstats( cout );
      assert( !estate.isEmpty() );
      assert( estate.numEmbeddings() == 231 * ORIENTATIONS );
   }
/*
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 9b: Double torus, K_6\n";

      EmbedState estate( 6, 2 );
      estate.K( 6 );
      estate.printstats( cout );
      assert( !estate.isEmpty() );
      assert( estate.numEmbeddings() == 360588 );  //Don't ask me!
   }
*/
//#endif
   {
/*    // Too difficult
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test 8: Boundary orientation\n";

      EmbedState estate( 8, 1 );
      for (int i = 0; i < 4; i++)
      {
      estate.addEdge( i, (i + 1) % 4 );
      estate.addEdge( 4 + i, 4 + (i + 1) % 4 );
      }

      for (int i = 0; i < 4; i++)
      {
      estate.addEdge( i, i + 4 );
      estate.addEdge( (i + 1) % 4, i + 4 );
      estate.addEdge( i, (i + 1) % 4 + 4 );
      }

      estate.print( cout );
      estate.printstats( cout );
*/
   }
#if 0
//#endif
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test X\n";

      EmbedState estate( 5 );
      EmbedState estatem( 5 );
      //estate.compute( (int[]){ 0,1,2,3,4,10,20,30,0,10,20,30,0,10,1,10,20,12, -1 } );

/*
// should be equal?
estate.compute( (int[]){ 0,1,2,3,4,10,20,30,0,10,20,30,40,0,14,1, -1 } );
estatem.compute( (int[]){ 0,1,2,3,4,10,20,30,0,10,20,30,0,14,1, -1 } );
// assert( estate.identical( estatem ) );

// equal states
estate.compute( (int[]){ 0,1,2,3,4,10,20,30,0,10,20,30,40,0,14,23, -1 } );
estatem.compute( (int[]){ 0,1,2,3,4,10,20,30,0,10,20,30,40,0,14, -1 } );
// assert( estate.identical( estatem ) );

// Both should reduce to (0,3,2,4) and (1) component embedding
estate.compute( (int[]){ 0,1,2,3,4,10,20,30,0,10,20,40,0,10,1,30, -1 } );
estatem.compute( (int[]){ 0,1,2,3,4,10,20,30,14,24,0,10,1,30, -1 } );
// assert( estate.identical( estatem ) );
      
// Non-trivial redundancies
// Ignoring trivial (2) component, other component should reduce to
// (0,1,4,3) and (0,3,4),(1,3) embeddings
estate.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,12,13,1,12,2, -1 } );
estatem.compute( (int[]){ 0,1,2,3,4,20,30,40,0,10,12,13,1,12,2, -1 } );
// assert( estate.identical( estatem ) );
*/
/*
  estate.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,20,30,40,0,10,1,12,13,24,2,12,1, -1 } );
  estatem.compute( (int[]){ 0,2,1,3,4,20,10,30,40,0,20,10,30,40,0,20,2,12,23,14,1, -1 } );
      
  estate.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,20,30,40,0,10,1,12,23,2,23,3,12, -1 } );
  //estatem.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,20,30,40,0,10,1,12,2,23,3,12, -1 } );
  estatem.compute( (int[]){ 1,0,2,3,4,10,12,13,14,1,10,12,13,14,1,12,23,2,23,3,12, -1 } );
*/
/*
  [0,1,2,3,4,10,20,30,40,0,10,12,13,23,24,2,20,12,34,4]
  on minors:
  [0,1,2,3,4,,20,30,40,0,10,12,13,23,24,2,20,12,34,4]
  [0,1,2,3,4,10,,30,40,0,10,12,13,23,24,2,20,12,34,4]
  [0,1,2,3,4,10,20,,40,0,10,12,13,23,24,2,20,12,34,4]
  [0,1,2,3,4,10,20,30,,0,10,12,13,23,24,2,20,12,34,4]
  [0,1,2,3,4,10,20,30,40,0,10,,13,23,24,2,20,12,34,4]
  [0,1,2,3,4,10,20,30,40,0,10,12,,23,24,2,20,12,34,4]
  [0,1,2,3,4,10,20,30,40,0,10,12,13,,24,2,20,12,34,4]
  [0,1,2,3,4,10,20,30,40,0,10,12,13,23,,2,20,12,34,4]
  [0,1,2,3,4,10,20,30,40,0,10,12,13,23,24,2,20,12,,4]
  [1,,2,3,4,,12,13,14,0,10,,,23,24,2,20,12,34,4]
  [2,1,,3,4,12,,23,24,0,10,,13,,,2,20,12,34,4]
  [3,1,2,,4,13,23,,34,0,10,12,,,24,2,20,12,,4]
  [4,1,2,3,,14,24,34,,0,10,12,13,23,,2,20,12,,4]
  [0,1,,3,4,10,,30,40,0,10,,13,,14,2,20,12,34,4]
  [0,1,3,,4,10,30,,40,0,10,13,,,34,2,20,12,,4]
  [0,1,4,3,,10,40,30,,0,10,14,13,34,,2,20,12,,4]
  [0,1,2,3,,10,20,30,,0,10,12,13,23,,2,20,12,,4]
*/

      // identical
      estate.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,12,13,23,24,2,20,12,34,4, -1 } );
      estatem.compute( (int[]){ 0,1,2,3,10,20,30,0,10,12,13,23,2,20,12,4, -1 } );

/*

// A minor requiring mapping two boundaries to one
estate.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,12,13,23,24,2,20,34,3,30,0,10,20,30,0,01,1,20,12,2,12,23, -1 } );
estatem.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,12,13,23,24,2,20,34,3,30,0,10,20,30,0,01,1,20,2,12,23, -1 } );

// A difficult case not handled! Mapping two components to a single boundary
estate.compute( (int[]){ 0,1,2,3,4,10, -1 } );
estatem.compute( (int[]){ 0,1,2,3,4, -1 } );
*/
      cerr << " (((((((((((( original ))))))))))\n";
      estate.print( cout );
      cerr << " (((((((((((( minor ))))))))))\n";
      estatem.print( cout );

      cerr << "---------------------------------------------------------------\n";
      cerr << "Identical = " << estate.identical( estatem ) << "\n";
      cerr << "original <= minor = " << estate.leq( estatem ) << "\n";
      cerr << "minor <= original = " << estatem.leq( estate ) << "\n";


      cerr << "Reduced faces..................................................\n";
      cerr << " (((((((((((( original ))))))))))\n";
      estate.reduce();
      estate.print( cout );
      cerr << " (((((((((((( minor ))))))))))\n";
      estatem.reduce();
      estatem.print( cout );

      cerr << "---------------------------------------------------------------\n";
      cerr << "Identical = " << estate.identical( estatem ) << "\n";
      cerr << "original <= minor = " << estate.leq( estatem ) << "\n";
      cerr << "minor <= original = " << estatem.leq( estate ) << "\n";


   }
#endif
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test Z\n";

      EmbedState estate( 5, 1, 0 );
      EmbedState estatem( 5, 1, 0 );
 
      estate.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,10,20,30,40,12,13,14,1,12,13,14,23,24,34, -1 } );
      estatem.compute( (int[]){ 0,1,2,3,4,10,20,30,40,0,20,30,40,12,13,14,1,12,13,14,23,24,34, -1 } );

      cerr << " (((((((((((( original ))))))))))\n";
      estate.print( cout );
      cerr << " (((((((((((( minor ))))))))))\n";
      estatem.print( cout );

      cerr << "Identical = " << estate.identical( estatem ) << "\n";
      cerr << "original <= minor = " << estate.leq( estatem ) << "\n";
      cerr << "minor <= original = " << estatem.leq( estate ) << "\n";
   }
   {
      cout << "+++++++++++++++++++++++++++\n";
      cout << "Test ZZ\n";

      EmbedState estate( 0, 2, 0 );
      {
         Component *comp = estate.addDebugComponent( 4 );
         Embedding E( comp );
         E.addFaceFromString( "123210,32,010123" );
         estate.addDebugEmbedding( E );
         //comp->addEmbedding( E );
      }

      EmbedState estate2( 0, 2, 0 );
      {
         Component *comp = estate2.addDebugComponent( 4 );
         Embedding E( comp );
         E.addFaceFromString( "01,0123,0123" );
         estate2.addDebugEmbedding( E );
      }
      estate.print( cout );
      estate2.print( cout );
      cerr << "original <= minor = " << estate.leq( estate2 ) << "\n";
      cerr << "minor <= minor = " << estate2.leq( estate ) << "\n";
   }


//#endif
   return 0;
}

#endif // TESTING
