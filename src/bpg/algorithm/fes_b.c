/*tex
 
\file{fes_b.c}
\path{src/bpg/algorithm}
\title{Function to test within k edges of acyclic}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "bpg/algorithm/acyclic_b.h"
#include "array/array.h"

bool fes1Member( const BPG &graph )
{
   assert( ! hasMultipleEdge( graph ) );

   if ( acyclic(graph) ) { return true; }

   BPG* graphCopy = graph.copy();
   BPG& G = *graphCopy;

   bool member = false;

   for ( int i=0; i<G.length(); i++ )
   {
      Operator op = G.getOp( i );
      if ( !op.isEdgeOp() ) continue;

      G.setOp(i,Operator());
      if ( acyclic(G) ) { member = true; break; }
      G.setOp(i,op);
   }

   delete graphCopy;
   return member;
}

bool fes2Member( const BPG &graph ) 
{
   assert( ! hasMultipleEdge( graph ) );

   //if ( acyclic(graph) ) { return true; }
   //if ( fes1Member(graph) ) { return true; }
   if (graph.edges() < 3) return true;

   BPG* graphCopy = graph.copy();
   BPG& G = *graphCopy;

   bool member = false;

   for ( int i=0; i<G.length()-1 && !member; i++ )
   {
      Operator op = G.getOp( i );
      if ( !op.isEdgeOp() ) continue;

      G.setOp(i,Operator());

      for ( int j=i+1; j<G.length() && !member; j++ )
      {
        Operator op2 = G.getOp( j );
        if ( !op2.isEdgeOp() ) continue;

        G.setOp(j,Operator());

        if ( acyclic(G) ) { member = true; }

        G.setOp(j,op2);
      }

      G.setOp(i,op);
   }

   delete graphCopy;
   return member;
}

bool fes3Member( const BPG &graph ) 
{
   assert( ! hasMultipleEdge( graph ) );

   //if ( acyclic(graph) ) { return true; }
   //if ( fes1Member(graph) ) { return true; }
   //if ( fes2Member(graph) ) { return true; }
   if (graph.edges() <= 6) return true;

   BPG* graphCopy = graph.copy();
   BPG& G = *graphCopy;

   bool member = false;

   for ( int i=0; i<G.length()-2 && !member; i++ )
   {
      Operator op = G.getOp( i );
      if ( !op.isEdgeOp() ) continue;

      G.setOp(i,Operator());

      for ( int j=i+1; j<G.length()-1 && !member; j++ )
      {
        Operator op2 = G.getOp( j );
        if ( !op2.isEdgeOp() ) continue;

        G.setOp(j,Operator());

        for ( int k=j+1; k<G.length() && !member; k++ )
        {
          Operator op3 = G.getOp( k );
          if ( !op3.isEdgeOp() ) continue;

          G.setOp(k,Operator());

	  if ( acyclic(G) ) { member = true; }

          G.setOp(k,op3);
        }

        G.setOp(j,op2);
      }

      G.setOp(i,op);
   }

   delete graphCopy;
   return member;
}


