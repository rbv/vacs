/*
 * See if a BPG has a smaller pathwidth.
 */

/*tex
 
\file{pathwidth_b.c}
\path{src/bpg/algorithm}
\title{Definition of pathwidth k membership/congruence}
\classes{PWChar, TypicalSeq}
\makeheader
 
xet*/

#include "bpg/bpg.h"
#include "bpg/algorithm/pathwidth_b.h"

// temp -- for debugging
//
#define F_DEBUG
#include <iostream>

ostream& operator<<( ostream &o, const TypicalSeq &T )
{
  return o << CAST(T, VertSet) << nl;
}

static vertNum fixedK;

/*************************************************************************/
//
// C L A S S   M E T H O D S
//

TypicalSeq typicalOps( const VertSet &S );
//
TypicalSeq::TypicalSeq( const VertSet &S )
 {
  vertNum l = S.size();

  assert(l>0);

  VertSet NoReps(l);
  vertNum n = 0;

  NoReps[n++]=S[0];
  
  for (int i = 1; i<l; i++)
   {
    if (S[i] != NoReps[n-1]) NoReps[n++]=S[i];
   }
  
  NoReps.resize(n);

  *this = typicalOps(NoReps);
 }

TypicalSeq::TypicalSeq( const TypicalSeq &S1, const TypicalSeq &S2 ) 
 {
cerr << "TypicalSeq(S1,S2):\n";
cerr << S1 << S2;
  vertNum l1 = S1.size();
  vertNum l2 = S2.size();

  if ( S1[l1-1] == S2[0] ) l1--; 	// cancel repetitions

  VertSet NoReps(l1+l2);
  
  int i; for (i=0; i<l1; i++) NoReps[i] = S1[i];
  for (    i=0; i<l2; i++) NoReps[l1+i] = S2[i];

  *this = typicalOps(NoReps);
 }

vertNum TypicalSeq::max() const
 {
   const VertSet &S = (*this);

   vertNum m = S[0];

   for (int i=1; i<S.size(); i++)
    {
     if (m<S[i]) m = S[i];
    }

   return m;
 }

const TypicalSeq& TypicalSeq::addScalar( vertNum scalar )
 {
   VertSet &S = (*this);

   for (int i=0; i<S.size(); i++)
    {
     S[i] = S[i] + scalar;
    }

   return *this;
 }

void TypicalSeq::split1( int idx, TypicalSeq &S1, TypicalSeq &S2 ) const
 {
   const VertSet &S = (*this);
   int l = S.size();

   assert(l>0);

   assert( idx >= 0 && idx < l );

   S1.resize(idx+1);
   S2.resize(l-idx);

   int i; for (i=0; i<=idx; i++)  S1[i] = S[i];
   for (    i=0; i<l-idx; i++) S2[i] = S[i+idx];

   return;
 }

void TypicalSeq::split2( int idx, TypicalSeq &S1, TypicalSeq &S2 ) const
 {
   const VertSet &S = (*this);
   int l = S.size();

   assert( idx > 0 && idx < l-1 );

   S1.resize(idx+1);
   S2.resize(l-idx-1);

   int i; for (i=0; i<=idx; i++)  S1[i] = S[i];
   for (    i=0; i<l-idx-1; i++) S2[i] = S[i+idx+1];

   return;
 }

//------------------------------------------------------------------------

vertNum PWChar::width() const
 {
  vertNum msz = 1;

  for (int i=0; i<_Z.size(); i++)
   {
    register int sz = _L[i].max();
    if ( sz > msz ) msz = sz;
   }
   
  return msz-1;
 }

void PWChar::forget( vertNum u )
 {
cerr << "PWChar::forget(" << u << ")\n";
cerr << *this << nl;

   vertNum fIndex = minIndex(u);  
   vertNum lIndex = maxIndex(u);

cerr << "min = " << fIndex << " max = " << lIndex << nl;

   // Remove u from interval model
   //
   int i; for (i=fIndex; i<=lIndex; i++)
    {
     _Z[i].remove(u);
    }
    
   /*
    * Check to see if we need to combine intervals.
    */
   bool leftOkay = false, 
        rightOkay = false;

   int sz = _Z.size();

   if ( fIndex == 0 || !(_Z[fIndex-1]==_Z[fIndex]) ) leftOkay = true;
   if ( lIndex == sz-1 || !(_Z[lIndex]==_Z[lIndex+1]) ) rightOkay = true;

if (leftOkay) cerr << "leftOkay\n" ;
if (rightOkay) cerr << "rightOkay\n" ;

   if ( leftOkay && rightOkay ) return;

   // check if only the first subset needs merging
   //
   if ( leftOkay == false && rightOkay == true )
    {
      _Z.shiftAndResizeLeft(fIndex,1);

      TypicalSeq newT(_L[fIndex-1],_L[fIndex]);

cerr << "newT = " << newT;

      _L[fIndex-1] = newT;
      
cerr << _L;

      _L.shiftAndResizeLeft(fIndex,1);

      return;
    }

   // check if only the last subset needs merging
   //
   if ( leftOkay == true && rightOkay == false )
    {
      _Z.shiftAndResizeLeft(lIndex,1);

      TypicalSeq newT(_L[lIndex],_L[lIndex+1]);
      _L[lIndex] = newT;
      
      _L.shiftAndResizeLeft(lIndex,1);

      return;
    }

   // both leftOkay == true && rightOkay == true
   //
   if ( fIndex != lIndex ) // we can do something like the above steps
    {
      for (i=fIndex; i<lIndex; i++) _Z[i]=_Z[i+1];
      for (i=lIndex; i<sz-2; i++) _Z[i]=_Z[i+2];
      _Z.resize(sz-2);

      TypicalSeq newT1(_L[fIndex-1],_L[fIndex]);
      TypicalSeq newT2(_L[lIndex],_L[lIndex+1]);

      _L[fIndex-1]=newT1;
      for (i=fIndex; i<lIndex-1; i++) _L[i]=_L[i+1];
      _L[lIndex-1]=newT2;
      for (i=lIndex; i<sz-2; i++) _L[i]=_L[i+2];
      _L.resize(sz-2);

      return;
    }

   // else we have to combine three typical sequences.
   //
   _Z.shiftAndResizeLeft(fIndex,2);

   TypicalSeq newT(_L[fIndex-1],_L[fIndex]);
   TypicalSeq newTT( newT, _L[lIndex+1] );
   _L[fIndex-1] = newTT;
      
   _L.shiftAndResizeLeft(fIndex,2);

   return;
 }

bool PWChar::hasVertex( vertNum u ) const
 {

//cerr << "hasVertex(" << u << ") :\n" << (*this);

  for (int i=0; i<_Z.size(); i++)
   {
//cerr << "i = " << i << nl;
    if ( _Z[i].index(u) < _Z[i].size() ) return true;
   }

  return false;
 }

bool PWChar::supportsEdge( vertNum u, vertNum v ) const
 {
  for (int i=0; i<_Z.size(); i++)
   {
    register int sz = _Z[i].size();
    if ( _Z[i].index(u) < sz && _Z[i].index(v) < sz ) return true;
   }

  return false;
 }

int PWChar::minIndex( vertNum u ) const
 {
  int i; for (i=0; i<_Z.size(); i++)
   {
    if ( _Z[i].index(u) < _Z[i].size() ) return i;
   }

  assert(false);
  return i;
 }

int PWChar::maxIndex( vertNum u ) const
 {
  int first = minIndex(u);

  int i; for (i=first+1; i<_Z.size(); i++)
   {
    if ( _Z[i].index(u) == _Z[i].size() ) break;
   }

  return i-1;
 }

//------------------------------------------------------------------------

PWChar_introduceIter::PWChar_introduceIter
   (
        const PWChar &C,
        vertNum vert,
        vertNum maxWidth,
        const VertSet *Overlap /* = 0 */
   )
 {
   assert( C.hasVertex(vert) == false );

   pC = &C;
   v = vert;
   k = maxWidth;
   pO = Overlap ? Overlap : new VertSet();

   /*
    * Compute min and max ranges for new "v" interval.
    */
   Zlen = pC->Z().size();

   maxStartZ = Zlen;			// indicates no bound 
   minEndZ = -1;			// dito
   
   int minI, maxI;
   //
   for (int i=0; i<pO->size(); i++)
    {
      minI = pC->minIndex(pO->get(i));
      if (minI>minEndZ) minEndZ = minI;
      
      maxI = pC->maxIndex(pO->get(i));
      if (maxI>maxStartZ) maxStartZ = maxI;
    }

   startInterval();

#ifdef F_DEBUG
cerr << "PWChar_introduceIter constructor for \n" << C;
#endif
 }

// blindly increment to the next internal interval
//
void PWChar_introduceIter::nextInterval()
 {
    int endLsize = pC->L()[endZ].size(); 

#ifdef F_DEBUG
cerr << "entry interval : [(" << startZ << ',' <<
        startL << "),(" << endZ << ',' << endL << ")]\n";
#endif

    // first check if endZ/L is at the last position
    // if so, then move start of the current interval forward
    //
    if ( endZ == Zlen-1 && endL == endLsize ) 	 
	{
	  int startLsize = pC->L()[startZ].size(); 

	  if ( startL == startLsize )
	   {
	     startZ++;
	     startL=-1;
	   }
	  else
	   {
	     startL++;
	   }

	  endZ = startZ;
	  endL = startL;

          if (endZ<=minEndZ)	// keep overlap restrictions.
           {
             if ( ! (endZ==minEndZ && endL>=0) )
              {
               endZ = minEndZ;
               endL = 0;
              }
           }
	}
    else 
	{
	  if ( endL < endLsize ) 
	   {
	    endL++;
	   }
	  else 
	   { 
	    endZ++; 
	    endL=-1;
	   }
	}
 
#ifdef F_DEBUG
cerr << "returns : [(" << startZ << ',' <<
        startL << "),(" << endZ << ',' << endL << ")]\n";
#endif
    return;
 }

// See if prospective interval from "nextInterval()" can be realized.
//
bool PWChar_introduceIter::maxRangeConflicts()
 {
   /*
    * By induction we only need to consider the interval width at the
    * right most point.
    */
#ifdef F_DEBUG
cerr << "maxRangeConflicts: " << endZ << ',' << endL << nl;
#endif

   // Easy cases to eliminate.
   //
   if ( endL == -1 || endL == pC->L()[endZ].size() ) return false;

cerr << "get(endL) = " << pC->L()[endZ].get(endL) << nl;

   // Check the last point.
   //
   if ( pC->L()[endZ].get(endL) <= k ) return false;

   // A conflict has occurred.  (temp. hack-update)
   //
   endZ = Zlen-1;	// force a move of the beginning of the interval
   endL = pC->L()[endZ].size();
   //
   nextInterval();
   
   return true;
 }

vertNum PWChar_introduceIter::gapWidth( int Zidx, int Lidx ) const
 {
#ifdef F_DEBUG
cerr << "gapWidth: " << Zidx << ',' << Lidx << nl;
#endif
//cerr << "Z:\n" << pC->Z() << nl;
//cerr << "L:\n" << pC->L() << nl;

   // Do easy cases first.
   //
   if (Lidx>=0 && Lidx<pC->L()[Zidx].size())
    {
      return pC->L()[Zidx][Lidx]+1;
    }
   else if ( Lidx == -1 )
    {
     if (Zidx==0) return 1; 	// overhangs left side
     	
     TypicalSeq TL = pC->L()[Zidx-1],
                TR = pC->L()[Zidx];

     return max(TL[TL.size()-1], TR[0]);
    }
   else // Lidx == *.size()
    {
     if (Zidx==Zlen-1) return 1; // overhangs right side

     TypicalSeq TL = pC->L()[Zidx],
                TR = pC->L()[Zidx+1];

//cerr << " TL = " << TL << " TR = " << TR << nl;

     assert(Lidx == TL.size());

     return max(TL[Lidx-1], TR[0]);
    }
 }

/*
 ************************************************************************
 */
bool PWChar_introduceIter::next( PWChar &NextC )
 {

cerr << "PWChar_introduceIter::next( PWChar &NextC )\n";

   // Original characteristic
   //
   const IntervalModel &Z = pC->Z();
   const TypicalList &L = pC->L();

   // friend access for in place space modification
   //
   IntervalModel &newZ = NextC._Z;
   TypicalList &newL = NextC._L;

   while (1)
    {
  
cerr << "while(1) " << startZ << ',' << startL << ' ' << endZ << ',' << endL << nl;

     // If the following happens we went too far.
     //
     if (startZ>=maxStartZ)
      {
//cerr << "if >= " << maxStartZ << nl;
        if ( startZ>maxStartZ || startZ==Zlen || startL == L[startZ].size() )
        {
//cerr << "return false\n";
        return false;
        }
      }

//cerr << "to call maxRangeConflics()\n";
  
     // Check and update range/bounds if proposed interval exceeds
     // our <= k pathwidth requirement.
     //
     if (maxRangeConflicts()) continue;
     else  		      break;

    } // while

   //
   // If we get here we should have a new characteristic to return.
   // In all cases we splice in a new start set and end set to account
   // for new vertex v's interval.
   //
   newZ.resize(Zlen+2);
   newL.resize(Zlen+2);

   /*
    * Now see how to create an interval model Z' from pC->Z()
    */

   // lengths of start and end L intervals
   //
   //int leftL = (startZ>-1 && startZ<Zlen) ? pC->L()[startZ].size() : 0; 
   //int rightL = (endZ>-1 && endZ<Zlen) ? pC->L()[endZ].size() : 0; 
   //
   int leftL = pC->L()[startZ].size(); 
   int rightL = pC->L()[endZ].size(); 

cerr << "leftL = " << leftL << " rightL = " << rightL << nl;

   // We have two views of a gap of L (indices -1 and size()).
   // The following does both cases.  -- HACK ALERT --

   int index=0,		// index of next new Z_i to fill in.
       gapFlag=0;


   // case 0: 	// |Z'| = |Z|
   //
   if ( (startL == -1 || startL == leftL) && 
        (endL == -1 || endL == rightL ) )
    {
       for (index = 0; index <= startZ-(startL==-1); index++) 
        {
         newZ[index]=Z[index];
         newL[index]=L[index];
        }
       
       SortedNums Zi;
       //
       if (index == 0 || index == Zlen) Zi = SortedNums();
       else		 	        Zi.join(Z[index-1], Z[index]);

       Zi.insert(v);
       newZ[index] = Zi;

       TypicalSeq Li( startWidth() );
       newL[index] = Li;

       gapFlag = ( ( startZ == endZ && startL == endL ) ||
                 ( startZ == endZ-1 && startL == leftL && endL == -1 ) );

//cerr << "gapFlag = " << gapFlag << nl;

       // Does the above interval serve both as start and stop interval set?
       //
       if ( gapFlag ) 
        {
         newZ.shrink(1); 
         newL.shrink(1);
        }
       else
        {
         // proceed with intermediate interval sets
         //
         for ( ; index < endZ+(endL==rightL); index++) 
          {
           Zi = Z[index];
           Zi.insert(v);
           newZ[index+1]=Zi;
  
           Li = L[index];
           Li.addScalar(1);
           newL[index+1]=Li;
          }
        
         // finally do end gap
         //  
         if  (index == 0 || index == Zlen) Zi = SortedNums();
         else				   Zi.join(Z[index-1], Z[index]);

         Zi.insert(v);
         newZ[index+1] = Zi;
  
         Li = TypicalSeq( endWidth() );
         newL[index+1] = Li;
        }

       for ( ; index < Zlen; index++) 
        {
         newZ[index+2-gapFlag]=Z[index];
         newL[index+2-gapFlag]=L[index];
        }
    }

   // case 1: 	// |Z'| = |Z| + 1 (only split left)
   //
   else if ( endL == -1 || endL == rightL )
    {
       for (index = 0; index < startZ; index++) 
        {
         newZ[index] = Z[index];
         newL[index] = L[index];
        }
       
       SortedNums Zi = Z[index];
       Zi.insert(v);

       TypicalSeq Li, Li2;
       L[index].split1(startL, Li, Li2);

       newZ[index] = Z[index];
       newL[index] = Li;

       newZ[index+1] = Zi;
       newL[index+1] = Li2.addScalar(1);

       for ( ++index; index < endZ+(endL==rightL); index++) 
        {
         Zi = Z[index];
         Zi.insert(v);
         newZ[index+1] = Zi;

         Li = L[index];
         newL[index+1] = Li.addScalar(1);
        }

       if (index == Zlen ) Zi = SortedNums();
       else		   Zi.join(Z[index-1], Z[index]);

       Zi.insert(v);
       newZ[index+1] = Zi;

       Li = TypicalSeq( endWidth() );
       newL[index+1] = Li;

       for ( ; index < Zlen; index++) 
        {
         newZ[index+2]=Z[index];
         newL[index+2]=L[index];
        }
    }

   // case 2: 	// |Z'| = |Z| + 1 (only split right)
   //
   else if ( startL == -1 || startL == leftL )
    {
       for (index = 0; index <= startZ-(startL==-1); index++) 
        {
         newZ[index] = Z[index];
         newL[index] = L[index];
        }
       
       SortedNums Zi;
       //
       if (index != 0) Zi.join(Z[index-1], Z[index]);

       Zi.insert(v);
       newZ[index] = Zi;

       TypicalSeq Li( startWidth() );
       newL[index] = Li;

       for ( ; index < endZ; index++) 
        {
         Zi = Z[index];
         Zi.insert(v);
         newZ[index+1] = Zi;

         Li = L[index];
         newL[index+1] = Li.addScalar(1);
        }

       Zi = Z[index];
       Zi.insert(v);

       TypicalSeq Li2;
       L[index].split1(endL, Li, Li2);

       newZ[index+1] = Zi;
       newL[index+1] = Li.addScalar(1);

       newZ[index+2] = Z[index];
       newL[index+2] = Li2;

       for ( ++index; index < Zlen; index++) 
        {
         newZ[index+2] = Z[index];
         newL[index+2] = L[index];
        }
    }

   // case 3: 	// |Z'| = |Z| + 2 (split left and right)
   //
   else
    {
       for (index = 0; index < startZ; index++) 
        {
         newZ[index] = Z[index];
         newL[index] = L[index];
        }
       
       SortedNums Zi = Z[index];
       Zi.insert(v);

       if (endZ > startZ) // can split start interval now
        {  

         TypicalSeq Li, Li2;
         L[index].split1(startL, Li, Li2);
  
         newZ[index] = Z[index];
         newL[index] = Li;
  
         newZ[index+1] = Zi;
         newL[index+1] = Li2.addScalar(1);
  
         for ( ++index; index < endZ; index++) // iterates at least once
          {
           Zi = Z[index];
           Zi.insert(v);
           newZ[index+1] = Zi;
  
           Li = L[index];
           newL[index+1] = Li.addScalar(1);
          }
  
         Zi = Z[index];
         Zi.insert(v);
  
         L[index].split1(endL, Li, Li2);
  
         newZ[index+1] = Zi;
         newL[index+1] = Li.addScalar(1);
  
         newZ[index+2] = Z[index];
         newL[index+2] = Li2;

        }
       else // we need to do a three-way split.
        {

         TypicalSeq Li, Li1, Li2, Li3;

         L[index].split1(startL, Li1, Li);

         Li.split1(endL-startL, Li2, Li3);
  
         newZ[index] = Z[index];
         newL[index] = Li1;
  
         newZ[index+1] = Zi;
         newL[index+1] = Li2.addScalar(1);

         newZ[index+2] = Z[index];
         newL[index+2] = Li3;

        }

       for ( ++index; index < Zlen; index++) 
        {
         newZ[index+2] = Z[index];
         newL[index+2] = L[index];
        }

    }

   //  Check for empty sets next to gaps.
   //
   if ( (startL == -1 || startL == leftL) || (endL == -1 || endL == rightL ) )
    { 
     if (!gapFlag) mergeDuplicates( newZ, newL );
    }

   nextInterval();	// set up for next call to this method.

   return true;
 }

void PWChar_introduceIter::mergeDuplicates( 
 IntervalModel &newZ, 
 TypicalList &newL )
 {

cerr << "mergeDuplicates : " << newZ;

   int shifted = 0, index;

   int leftL = pC->L()[startZ].size(); 
   int rightL = pC->L()[endZ].size(); 

   // check start gap
   //
   if ( startL == -1 || startL == leftL ) 
    {
      index = startZ+(startL==leftL);

cerr << "index 1 = " << index << nl;

      // check just after start new set
      //
      if (newZ[index]==newZ[index+1])
       {
         shifted++;
         newZ.shiftAndResizeLeft(index,1);
         newL[index] = TypicalSeq( newL[index], newL[index+1] );
         newL.shiftAndResizeLeft(index,1);
       }
    }

   if ( endL == -1 || endL == rightL ) 
    {
      index = endZ+(endL==rightL) - shifted; // assumes 2 new insertions

cerr << "index 2 = " << index << nl;

      // check just before end new set
      //
      if (newZ[index]==newZ[index+1])
       {
         newZ.shiftAndResizeLeft(index,1);
         newL[index] = TypicalSeq( newL[index], newL[index+1] );
         newL.shiftAndResizeLeft(index,1);
       }
    }
 }



/*************************************************************************/
//
// S U P P O R T   R O U T I N E S 
//

TypicalSeq typicalOps( const VertSet &S )
 {
   vertNum l = S.size();
   VertSet N(l);
   vertNum n = 0;

cerr << "typicalOps:" << S << " l = " << l << nl;

   bool changeFlag = false;

   // check for s_i, s_{i+1}, ..., s_j gap where s_j <= s_i
   // ( s_k >= both s_i and s_j for i<k<j )
   //
   int i; for (i=0; i<l-3; i++)
    {
//cerr << "1) i=" << i << nl;
      N[n++] = S[i];

      vertNum maxRange = S[i];
      bool lessChangeFlag = false;
      int markEq = i;
      //
      for (int j=i+1; j<l; j++)
       {
         if (S[j] == S[i]) markEq = j;

         if (S[j] < S[i])
          {
           if ( j-i > 2 ) 	// we found a s_j < s_i gap to shorten.
            {
              lessChangeFlag = changeFlag = true;
              N[n++] = maxRange;
              i = j-1;
            }
           //
           break; // j
          }
         else if (S[j]>maxRange) maxRange=S[j];
       }

      if ( lessChangeFlag==false && markEq-i > 2) // s_i == s_j gap
       {
         changeFlag = true;
         N[n++] = maxRange;
         i = markEq-1;
       }

    } // i

   if (changeFlag)
    {
     for (; i<l; i++) N[n++]=S[i];
     N.resize(n);
     return typicalOps( N );
    }

   //
   // check for s_i, s_{i+1}, ..., s_j gap where s_j > s_i
   // ( s_k >= both s_i and s_j for i<k<j )
   //
   for (n=0, i=l-1; i>2; i--)
    {
//cerr << "2) i=" << i << nl;

      N[n++] = S[i];

      vertNum maxRange = S[i];
      //
      for (int j=i-1; j>=0; j--)
       {
         if (S[j] < S[i])
          {
           if ( i-j > 2 ) 	// we found a s_j < s_i gap to shorten.
            {
              changeFlag = true;
              N[n++] = maxRange;
              i = j+1;
            }
           //
           break; // j
          }
         else if (S[j]>maxRange) maxRange=S[j];
       }

    } // i

   if (changeFlag)
    {
     for (; i>=0; i--) N[n++]=S[i];
     N.resize(n);
     N.reverse(0,n-1);
     return typicalOps( N );
    }
   else return ((TypicalSeq&) S);  // reference cast to avoid VertSet cntr.
    
 }

static bool PWCharEqual( const PWChar &C1, const PWChar &C2)
 {
   return  C1 == C2;
 } 

static void add_edge( vertNum u, vertNum v, const PWState *oFS, PWState *nFS )
 {
   nFS->purgeAll();

   PWChar *pC;

   oFS->start();
   //
   while ( pC = oFS->next() )
    {
      if ( pC->supportsEdge(u,v) ) nFS->addUnique( new PWChar(*pC) );
    }
   //
   oFS->stop();

//   nFS->removeEqualities( PWCharEqual );

   return;
 }

static void add_vertex( vertNum v, const PWState *oFS, PWState *nFS )
 {
   //cerr << "1) oFS iterators = " << oFS->numIterators() << nl;
   //cerr << "1) nFS iterators = " << nFS->numIterators() << nl;

   nFS->purgeAll();

   PWChar *pC;
   PWChar newC;

   oFS->start();
   //
   while ( pC = oFS->next() )
    {
      //assert(pC->hasVertex(v)==false);

      if (pC->hasVertex(v)) pC->forget(v);
      
//cerr <<"end forget()\n";

      PWChar_introduceIter PW_reps( *pC, v, fixedK );

      while ( PW_reps.next( newC ) )
       {
#ifdef F_DEBUG
cerr << "adding:\n" << newC << nl;
#endif
         nFS->addUnique( new PWChar(newC) );
       }
//cerr << "next oFS\n";
    }
   //
//cerr << "oFS->stop()\n";
   oFS->stop();

//cerr << "removeEqualities\n";

//   nFS->removeEqualities( PWCharEqual );

   //cerr << "2) oFS iterators = " << oFS->numIterators() << nl;
   //cerr << "2) nFS iterators = " << nFS->numIterators() << nl;
 }

#ifdef F_DEBUG

static void dumpState( const PWState *S )
 {
   cerr << "dumpStates:\n";

   PWChar *pC;
   
   S->start();
   //
   while ( pC = S->next() )
    {
      cerr << *pC;
    }
   //
   S->stop();
 }

#endif

/*************************************************************************/
//
//   P A T H - W I D T H
//
// The final state is returned in the third arg, unless it is 0.
// It defaults to 0 if not specified.
//
bool pathwidth( const BPG& B, int k, PWState *PW_state )
 {
  cerr << nl << "Finding Pathwidth " << k << " for " << B << nl;

  fixedK = k;

  int bndry = B.boundarySize();

  /*
   * Initialize a maximum path state class.
   */
  PWState *state[2];  // PWChars for even and odd posiitions.
  //
  state[0] = new PWState; state[1] = new PWState;

#if 0

  // Initialize with "null characteristic"
  //
  //PWChar nullC;
  //
  state[1]->add( PWChar::nullChar() );
  //
  int m = 0;

#else

  // Initialize with first vertex operator
  //
  state[0]->add( PWChar::K1Char(B[0].vertex()) );
  //
  int m = 1;

#ifdef F_DEBUG
cerr << 0 << " State: " << state[0]->size() << nl;
#endif

#endif

  /*
   * Crunch away through the operator string.
   */
  for (; m < B.length(); m++)
   {
      Operator op = B[m]; // get the operator at position m

      if ( op.isVertexOp() )
         {

          add_vertex( op.vertex(), state[(m-1)%2], state[(m)%2] );
	
         }// vert

       else if ( op.isEdgeOp() )
         {

          add_edge( op.vertex1(), op.vertex2(), state[(m-1)%2], state[(m)%2] );

         }// ej

       else    // hack job!! NoOp!!!
         {
          ;
         }

#ifdef F_DEBUG
cerr << m << " State: " << state[m%2]->size() << nl;
//cerr << (*state[m%2]) << nl;
dumpState(state[m%2]);
#endif

   // We assume that the graph is connected.
   //
   if ( !state[m%2]->size() ) break;

   }// m

  bool answer = state[m%2]->size() ? true : false;

  if ( PW_state ) 
   {
     delete PW_state;
     delete state[(m-1)%2];
     PW_state = state[m%2];
   }
  else
   {
     delete state[0]; 
     delete state[1]; 
   }
    
  return answer;
 }

