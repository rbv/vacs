
/*
 * See if a BPG has a k-feedback vertex set.  
 */

/*tex
 
\file{fvs_b.c}
\path{src/bpg/algorithm}
\title{Definition of feedback vertex set family functions}
\classes{FvsFamily,FvsCongruence,Parks,VertUnary,Witness}
\makeheader
 
xet*/

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/lgraph.h"
#include "bpg/bpgutil.h"
#include "graph/graph.h"
#include "graph/indgrph.h"
#include "graph/algorithm/cycle_ga.h"		// for acyclic()
//#include "array/array.h"
#include "general/bitvect.h"
#include "family/probinfo.h"
#include "general/bits.h"

#include "bpg/algorithm/fvs_b.h"


//--------------------------------------------------

bool fvsState::operator==( const fvsState &S ) const
{
#ifdef DFVS
     //cerr << "operator==\n" << *this << "\nand\n" << S << nl;
#endif
     //bool f1 = VertArray::operator==( CAST(S,VertArray) );
     //bool f2 = _witnesses == S._witnesses;
     return ( VertArray::operator==( CAST(S,VertArray) ) &&
              _witnesses == S._witnesses );

     //cerr << "f1 = " << (int) f1;
     //cerr << " f2 = " << (int) f2 << nl;
     //return f1 && f2;
}

// reset the state to a freash clean one.
//
void fvsState::reset(vertNum two2bSize)
   {
    resizeNoCopy(two2bSize);

    for (int i=0; i<=_maxKill; i++)
      {
 //cerr << "0) i=" << i << _witnesses[i] << nl;
       _witnesses[i].purgeAll();
       assert(_witnesses[i].isEmpty());
 //cerr << "1) i=" << i << _witnesses[i] << nl;
      }
   }


//--------------------------------------------------

// define static member used by callback
//
vertNum VertUnary::_v = 0;

// contains functions used in PCollection<T>::applyUnary()
//
VertUnary vertUnary;

//--------------------------------------------------


void VertUnary::pullOffBoundary( Park &p )
 {

//cerr << "pullOffBoundary " << _v << " in Park&\n" << p << nl;

  LTree *plt;

  vertNum vIndex;
  //
  vertNum degOfv;
 
  /*
   * Find tree with old boundary vertex.
   */
  bool found=false;
  //
  p.start();
  //
  while ( found==false && (plt = p.next()) )
   {

     if ( plt->labelIndex( _v, vIndex ) )
      {
        found = true;

//cerr << "  found tree at vertex " << vIndex << nl << *plt << nl;

        degOfv = plt->degree(vIndex);

        {
        vertNum i, nbr, nbr2;
	//
        switch (degOfv)
         {
           case 0: break;

           case 1: nbr  = plt->neighbor( vIndex, 0 );

//cerr << "case degree 1\n";

                   if ( plt->getLabel(nbr) == NOLABEL &&
                        plt->degree(nbr) == 2 )
                     {
                        plt -> contract( vIndex, nbr, _v );
//cerr << "contracted " << vIndex << " and " << nbr << nl;
//cerr << "tree: " << *plt << nl;
                     }

                   plt->rmLeaf(vIndex); 

//cerr << "remove Leaf " << vIndex << nl;
//cerr << "tree: " << *plt << nl;

                   break;
                  
           case 2: nbr  = plt->neighbor( vIndex, 0 );
                   nbr2 = plt->neighbor( vIndex, 1 );

                   if ( ! ( plt->getLabel(nbr) ==NOLABEL &&
                            plt->getLabel(nbr2)==NOLABEL &&
                            plt->degree(nbr) == 2 &&
                            plt->degree(nbr2)== 2 ) )
                   {
                     if ( plt->getLabel(nbr) ==NOLABEL ||
                          plt->getLabel(nbr2)==NOLABEL )
                     {
                       plt -> contract( vIndex, nbr, plt->getLabel(nbr) );
                     }
                     else
                     {
                       plt->setLabel( vIndex, NOLABEL );
                     }
            	     break;	
                   }
                   // Else we need two contractions and this is taken care
                   // of below in the default case.

           default:
//cerr << "case degree 'default:' " << degOfv << nl;

                   plt->setLabel( vIndex, NOLABEL );
  
                   i=0;
                   //
                   while ( i < plt->degree(vIndex) )
                    {
			nbr = plt->neighbor( vIndex, i );

//cerr << i << ") checking neighbor " << nbr << " with deg " 
//     << plt->degree(nbr) << " and label " << plt->getLabel(nbr) << nl;

			if ( plt->degree( nbr ) == 2 && 
			     plt->getLabel( nbr ) == NOLABEL )
                          {
                             /*
			      * Order of vIndex and nbr is serious!
			      */
                             plt->contract( vIndex, nbr, NOLABEL );

//cerr << "contracted " << vIndex << " and " << nbr << nl;
//cerr << "tree: " << *plt << nl;

                             i=0; // recheck new neighbors.
                          }
                        else i++;
                    }

         }}  // switch

      } // if _v found
   }
  //
  p.stop();

  /*
   * We may need to remove isolated tree.
   */
  if ( found==true && degOfv==0 ) p.purge( plt );

//cerr << "returned Park& \n" << p << nl;

  return;
 }

/**************************************************
 *
 * Class Parks member and associate functions.
 *
 *************************************************/

bool sameTree( const Park &p, Label v1, Label v2 )
 {

  LTree *plt;

  bool knowAnswer = false;
  bool answer = false;

  p.start();
  //
  while ( (plt = p.next()) && knowAnswer == false )
   {

    for (int i=0; i < plt->order() && knowAnswer == false; i++)
     {
       vertNum label = plt->getLabel(i);

       if ( label == v1 )	 
        {
          for (int j=i+1; j < plt->order() && knowAnswer == false; j++)
            if ( plt->getLabel(j) == v2 ) answer = knowAnswer = true;

          if ( knowAnswer == false ) knowAnswer = true;
        }
       else if ( label == v2 )
        {
          for (int j=i+1; j < plt->order() && knowAnswer == false; j++)
            if ( plt->getLabel(j) == v1 ) answer = knowAnswer = true;

          if ( knowAnswer == false ) knowAnswer = true;
        }

     } // check labels

   } // next tree
  //
  p.stop();

  return answer;
 }

Park* connectTrees( const Park &p, Label v1, Label v2 )
 {

  Park *newP = new Park;

  bool connected = false;

  LTree *plt;

  LTree *pv1=0, *pv2=0;
  //
  vertNum v1Pos, v2Pos=0;	// initialized to get rid of C++ warning

  p.start();
  //
  while ( plt = p.next() )
   {

    bool special = false;

    for (int i=0; i < plt->order() && connected == false; i++)
     {
       vertNum label = plt->getLabel(i);

       if ( label == v1 )	 
        {
          special = true;

          if (pv2)
           {
            v1Pos = i;
// cerr << "1) connecting trees at " << v1Pos <<" "<< v2Pos << nl << *plt << *pv2 << nl;
            newP -> add( new LTree( *plt, *pv2, v1Pos, v2Pos ) );
            connected = true;
           }
          else
           {
            pv1 = plt;
            v1Pos = i;
           }
        }
       else if ( label == v2 )
        {
          special = true;

          if (pv1)
           {
            v2Pos = i;
// cerr << "2) connecting trees at " << v1Pos <<" " << v2Pos << nl << *pv1 << *plt << nl;
            newP->add( new LTree( *pv1, *plt, v1Pos, v2Pos ) );
            connected = true;
           }
          else
           {
            pv2 = plt;
            v2Pos = i;
           }
        }

     } // check labels

    if ( ! special ) newP->add( new LTree( *plt ) );

   } // next tree
  //
  p.stop();

  return newP;
 }

Parks* splitTree( const Park &p, Label v1, Label v2 )
 {
  
//cerr << "  In splitTree on\n" << (Park) p << nl;

  Park *newP = new Park;

  bool splitTreeFound = false;

  LTree *plt, *toSplit;

  vertNum v1Idx, v2Idx;

  p.start();
  //
  while ( plt = p.next() )
   {
     if ( splitTreeFound==false && 
          plt->labelIndex(v1, v1Idx) && plt->labelIndex(v2, v2Idx) )
      {
        splitTreeFound = true; 
        toSplit = plt;
      }
     else newP -> add( new LTree( *plt ) );

   } // next tree
  //
  p.stop();

  assert(splitTreeFound);

//cerr << "to split\n" << *toSplit << "at " << v1Idx << " and " << v2Idx <<nl;

  Path splitPath;
  //
  toSplit->getPath( v1Idx, v2Idx, splitPath );

//cerr << "spliting path " << splitPath << " in\n" << *toSplit << nl;

  /*
   * Now loop through and all combination of split parks
   */
  Parks *newPs = new Parks();
  //
  for (int i=1; splitPath[i]!=v2Idx; i++)	// exclude end-points
   {			
     // Also exclude other boundary.
     //
     if ( toSplit->getLabel( splitPath[i] ) != NOLABEL ) continue;

     Park *addP = new Park( *newP );

     Park *splitForest = toSplit->rmNode( splitPath[i] );
     
     addP -> addSet( *splitForest );

     newPs -> add( addP );

//cerr << "begin splitForest -> purgeAll()\n";

// is this needed?  -- depends on addSet() 
//
//     splitForest -> purgeAll();  
 
#ifdef AUTODEL
      splitForest -> removeAll();  
#endif
      //
      delete splitForest;

//cerr << "splitForest -> purgeAll() end\n";
   }

#ifndef AUTODEL
  newP -> deleteAll();
#endif
  //
  delete newP;

  return newPs;
 }

// ------------------------------------------------

void Parks::getIsolated( Parks &p, vertNum v1, vertNum v2 )
 {

  p.purgeAll(); // store results here

  Park *pp;

  start();
  //
  while ( pp = next() )
   {
     if ( ! sameTree( *pp, v1, v2 ) ) p.add( new Park(*pp) );
   }
  //
  stop();

 }

void Parks::doEdgeOpCase4Eq( Parks &p, vertNum v1, vertNum v2 )
 {

  p.purgeAll(); // store results here

  Park *pp;

  start();
  //
  while ( pp = next() )
   {
//cerr << "connecting trees in park  " << *pp << nl;
     p.add( connectTrees( *pp, v1, v2 ) );
//cerr << "new tree added\n";
   }
  //
  stop();

 }

void Parks::doEdgeOpCase4Plus1( Parks &p, vertNum v1, vertNum v2 )
 {

//cerr << "doing EdgeOpCase4Plus1 on\n";

  Park *pp;

  Parks *fromSplitTree;
  //
  Parks splits;

  start();
  //
  while ( pp = next() )
   {
     fromSplitTree = splitTree( *pp, v1, v2 );
     //
     splits.addSet ( *fromSplitTree ); 	// pointer copy
     //
#ifdef AUTODEL
     fromSplitTree -> removeAll();	// no return to free store.
#endif
     delete fromSplitTree;		// no purge needed
   }
  //
  stop();

  splits.doEdgeOpCase4Eq( p, v1, v2 );
  
#ifndef AUTODEL
  splits.deleteAll();
#endif
 }

void Parks::addParks( const Parks &parks )
 {

//cerr << "begin Parks::addParks()\n";
//cerr << "*this:\n" << *this;
//cerr << "&parks:\n" << parks;

  Park *pp, *tp;

  /*
   * Go through each new park and add (copy) if *this doesn't have it.
   */
  parks.start();
  //
  while ( pp = parks.next() )
   {
     bool found = false;

     /*
      * Scan for existing park.
      */
     start();
     //
     while ( tp = next() )
      {
//cout << "Testing iso" << *tp;
//cout << "   With iso" << *pp;
        //if ( CAST(*tp,LForest) == CAST(*pp,LForest) )
        if ( *tp == *pp )
         {
//cout << "Parks equal\n" << *pp << nl;
           found = true;
           break;
         }
      } 
     //
     stop();

     if ( !found ) 
      {
//cout << "--adding:\n" << *pp << nl;

       add( new Park( *pp ) );
      }
  
   }
  //
  parks.stop();

//cerr << "finished addParks\n";
 }

/*************************************************************************/

#ifdef DFVS
//static void printFVSstate(int bndry, fvsState &S)
static void printFVSstate(int bndry, Array<Witness> &S)
{
#if 0
 for (int i=0; i<S.size(); i++)
  {
    BitVector B(i,bndry);
    //cerr << B << nl << S.get(i) << nl;
    cerr << B << nl;
    Witness w(S[i]);
    //Witness w;
    cerr << w << nl;
  }
#else
 cerr << "End FVSstate:\n" << S << nl;
#endif
} 
#endif
// The final state is returned in the third arg, unless it is 0.
// It defaults to 0 if not specified.
//
bool fvs( const BPG& B, int k, fvsState* FVS_state )
 {
#ifdef DFVS
//cerr << nl << "Finding fvs " << k << " for " << B << nl;
#endif
//cerr << nl << "Finding fvs " << k << " for " << B << nl;

  int bndry = B.boundarySize();
  int num_S = 1 << bndry;

#ifdef DOTIGHTEN
  Array<Witness>
#else
  fvsState 
#endif
           Te( num_S ), 	// State after even number of operators
           To( num_S );		// State after odd number of operators

  Witness Tp;			// Temporary witness.

  Graph G(bndry);		// Boundary induced graph.
  InducedGraph Gsubset(G);	// Subset of boundary induced graph.
  
  /*
   * Initialize the number of kill vertices used at the start. 
   */
  int s; for (s=0; s<num_S; s++)
   {
    Te[s].setKilled( bits(s) );   // Template function to count bits in s.

    /*
     * Find initial boundary graph.
     */
#ifdef ONLYUP2K
    if ( Te[s].killed() <= k )
#endif
     {
      Park park;
      //
      for (int i=0; i < bndry; i++)
       {
          if ( s & 1<<i ) continue;
  
          LTree* lboundary = new LTree( (Label) i );
  
          park.add( lboundary );
       }

      Te[s].setPark( park );
     }
#ifdef ONLYUP2K
    else Te[s].setKilled( k+1 );
#endif
     
   }

#ifdef DFVS
//cerr << "Init Te " << Te << nl;
#endif

  for (int m=0; m < B.length(); m++)
   {
    Operator op = B[ m ]; // get the operator at position m

#ifdef DFVS
//cerr << "--> Operator " << op << " : m = " << m << nl;
#endif

    if (m & 1)
     {
      for (s=0; s < num_S; s++)
       {
#ifdef DFVS
//cerr << "/* s = " << s << " */\n";
#endif
        if ( op.isVertexOp() )
         {
          vertNum v = op.vertex(); 

          if (s==0) G.rmEdges(v);

          if (s & (1<<v))			// Case 2:
           {

  		// Te[s] = min(To[s], To[s - (1<<v)]) + 1;
                //
                vertNum minKill = min(To[s].killed(), To[s - (1<<v)].killed());
                //
  		Te[s].setKilled( minKill + 1 );

//cerr << "2) minKill = " << minKill + 1 << nl;

#ifdef ONLYUP2K
                if ( minKill >= k ) 		// Don't waste time.
                  {
                      Te[s].setKilled( k+1 );
		      Te[s].purgeAll();

		      continue; 	
                  }
#endif

                if ( To[s].killed() == minKill )
                  {
                    Te[s].setParks( To[s] );

                    Te[s].doVertOpCase2(v);

//cerr << "done doVertOpCase2\n";

                    if ( To[s - (1<<v)].killed() == minKill )
                      {
                         Tp.setParks( To[s - (1<<v)] );
                         Tp.doVertOpCase2Minus(v);

//cerr << "done doVertOpCase2Minus\n";

                         Te[s].addParks( Tp );
                      }
                  }
                else
                  {
                    Te[s].setParks( To[s - (1<<v)] );

                    Te[s].doVertOpCase2Minus(v);

//cerr << "done doVertOpCase2Minus\n";
                  }

           }
          else					// Case 1:
           {

  		// Te[s] = min(To[s], To[s + (1<<v)]);
                //
                vertNum minKill = min(To[s].killed(), To[s + (1<<v)].killed());
                //
  		Te[s].setKilled( minKill );

//cerr << "1) minKill = " << minKill << nl;

#ifdef ONLYUP2K
                if ( minKill > k )             // Don't waste time.
                  {
                      Te[s].purgeAll();

                      continue;
                  }
#endif

                if ( To[s].killed() == minKill )
                  {
                    Te[s].setParks( To[s] );

                    Te[s].doVertOpCase1(v);

//cerr << "done doVertOpCase1\n";

                    if ( To[s + (1<<v)].killed() == minKill )
                      {
                         Tp.setParks( To[s + (1<<v)] );

                         Tp.doVertOpCase1Union(v);

//cerr << "done doVertOpCase1Union\n";

                         Te[s].addParks( Tp );
                      }
                  }
                else
                  {
                    Te[s].setParks( To[s + (1<<v)] );

                    Te[s].doVertOpCase1Union(v);

//cerr << "done doVertOpCase1Union\n";
                  }

           }

         }

      else if ( op.isEdgeOp() )

        {
          vertNum v1 = op.vertex1(),
                  v2 = op.vertex2();

          if (s==0) 
           { 
             assert( G.isEdge(v1,v2)==false); 	// no multi-edges.
             G.addEdge(v1,v2); 
           }

// if (s==0) cerr << "Graph (addEdge " << v1 << " " << v2 << ") : " << G << nl;

#ifdef ONLYUP2K
          /*
           * See if there is no hope.
           */
          if ( To[s].killed() > k )
            {
              //Te[s].setKilled( To[s].killed() );
              Te[s].setKilled( k+1 );

              Te[s].purgeAll();

              continue;
            }
#endif

          if (s & (1<<v1) || s & (1<<v2))	// Case 3:
           {
  		Te[s].setKilled( To[s].killed() );

  		Te[s].setParks( To[s] );
           }
          else					// Case 4:
           {
                Gsubset.setIndex( BitVector( ~s, bndry ) );   
                // 
                if (! acyclic(Gsubset) )	// Case 4a:
                  {
#ifdef ONLYUP2K
  		    Te[s].setKilled(k+1);

		    Te[s].purgeAll();
#else
  		    Te[s].setKilled( NOLABEL );		// 255
#endif
                    continue;
                  }

                To[s].getIsolated( Tp, v1, v2 );

                if ( Tp.size() )
                  {
                    Tp.doEdgeOpCase4Eq( Te[s], v1, v2 );

//cerr << "done doEdgeOpCase4Eq\n";

                    Te[s].setKilled( To[s].killed() );

                  }
                else
                  {
                    To[s].doEdgeOpCase4Plus1( Te[s], v1, v2 ); 

//cerr << "done doEdgeOpCase4Plus1\n";
                    
                    Te[s].setKilled( To[s].killed()+1 );

#ifdef ONLYUP2K
		    if ( Te[s].killed() > k ) 
                     { 
                      Te[s].setKilled( k+1 );
                      Te[s].purgeAll();
                     }
#endif
                  }
           }

        }// ej

      else    // hack job!! NoOp!!!
        {
  	  Te[s].setKilled( To[s].killed() );

          Te[s].setParks( To[s] );
        }

       }// s
     }// if m

                // ------------- //
    else	// Other parity. //
                // ------------- //

     {
      for (s=0; s < num_S; s++)
       {
#ifdef DFVS
//cerr << "/* s = " << s << " */\n";
#endif
        if ( op.isVertexOp() )
         {
          vertNum v = op.vertex(); 

          if (s==0) G.rmEdges(v);

          if (s & (1<<v))			// Case 2:
           {

  		// To[s] = min(Te[s], Te[s - (1<<v)]) + 1;
                //
                vertNum minKill = min(Te[s].killed(), Te[s - (1<<v)].killed());
                //
  		To[s].setKilled( minKill + 1 );

//cerr << "2) minKill = " << minKill + 1 << nl;

#ifdef ONLYUP2K
                if ( minKill >= k )             // Don't waste time.
                  {
                      To[s].setKilled( k+1 );
                      To[s].purgeAll();

                      continue;
                  }
#endif

                if ( Te[s].killed() == minKill )
                  {
                    To[s].setParks( Te[s] );

                    To[s].doVertOpCase2(v);

//cerr << "done doVertOpCase2\n";

                    if ( Te[s - (1<<v)].killed() == minKill )
                      {
                         Tp.setParks( Te[s - (1<<v)] );
                         Tp.doVertOpCase2Minus(v);

//cerr << "done doVertOpCase2Minus\n";

                         To[s].addParks( Tp );
                      }
                  }
                else
                  {
                    To[s].setParks( Te[s - (1<<v)] );

                    To[s].doVertOpCase2Minus(v);

//cerr << "done doVertOpCase2Minus\n";
                  }

           }
          else					// Case 1:
           {

  		// To[s] = min(Te[s], Te[s + (1<<v)]);
                //
                vertNum minKill = min(Te[s].killed(), Te[s + (1<<v)].killed());
                //
  		To[s].setKilled( minKill );

//cerr << "1) minKill = " << minKill << nl;

#ifdef ONLYUP2K
                if ( minKill > k )             // Don't waste time.
                  {
                      To[s].purgeAll();

                      continue;
                  }
#endif

                if ( Te[s].killed() == minKill )
                  {
                    To[s].setParks( Te[s] );

//cerr << " vertOP on " << v << nl << To[s] << nl;

                    To[s].doVertOpCase1(v);

//cerr << "done doVertOpCase1\n";

                    if ( Te[s + (1<<v)].killed() == minKill )
                      {
                         Tp.setParks( Te[s + (1<<v)] );

                         Tp.doVertOpCase1Union(v);

//cerr << "done doVertOpCase1Union\n";

                         To[s].addParks( Tp );
                      }
                  }
                else
                  {
                    To[s].setParks( Te[s + (1<<v)] );

                    To[s].doVertOpCase1Union(v);

//cerr << "done doVertOpCase1Union\n";
                  }

           }

         }

      else if ( op.isEdgeOp() )

        {
          vertNum v1 = op.vertex1(),
                  v2 = op.vertex2();

          if (s==0) 
           { 
             assert( G.isEdge(v1,v2)==false); 	// no multi-edges.
             G.addEdge(v1,v2); 
           }


//if (s==0) cerr << "Graph (addEdge " << v1 << " " << v2 << ") : " << G << nl;

#ifdef ONLYUP2K
          /*
           * See if there is no hope.
           */
          if ( Te[s].killed() > k )
            {
              //To[s].setKilled( Te[s].killed() );
              To[s].setKilled( k+1 );

              To[s].purgeAll();

              continue;
            }
#endif

          if (s & (1<<v1) || s & (1<<v2))	// Case 3:
           {
  	        To[s].setKilled( Te[s].killed() );

  		To[s].setParks( Te[s] );
           }
          else					// Case 4:
           {
                Gsubset.setIndex( BitVector( ~s, bndry ) );   
                // 
                if (! acyclic(Gsubset) )	// Case 4a:
                  {
#ifdef ONLYUP2K
  		    To[s].setKilled(k+1);

		    To[s].purgeAll();
#else
  		    To[s].setKilled( NOLABEL );		// 255
#endif
                    continue;
                  }

                Te[s].getIsolated( Tp, v1, v2 );

//cerr << "got Isolated = " << Tp.size() << nl;

                if ( Tp.size() )
                  {
                    Tp.doEdgeOpCase4Eq( To[s], v1, v2 );

//cerr << "done doEdgeOpCase4Eq\n";

                    To[s].setKilled( Te[s].killed() );
                  }
                else
                  {
                    Te[s].doEdgeOpCase4Plus1( To[s], v1, v2 ); 

//cerr << "done doEdgeOpCase4Plus1\n";
                    
                    To[s].setKilled( Te[s].killed()+1 );

#ifdef ONLYUP2K
		    if ( To[s].killed() > k ) 
                     {
    		      To[s].setKilled(k+1);
                      To[s].purgeAll();
                     }
#endif
                  }
           }

        }// ej

      else    // hack job!! NoOp!!!
        {
  	  To[s].setKilled( Te[s].killed() );

          To[s].setParks( Te[s] );
        }

       }// s

     }// else m

#if 0 /* #ifdef DFVS */
if (m & 1) cerr << " End of: " << m << " -- Te[] equals\n" << Te << nl;

else cerr << " End of: " << m << " -- To[] equals\n" << To << nl;
#endif

   }// for m

  bool answer = false;

  if (B.length() & 1)
   {
    for (s=0; s < num_S; s++) 
     {
       if (To[s].killed() <= k) { answer=true; break; }
     }
   }
  else
   {
    for (s=0; s < num_S; s++) 
     {
      if (Te[s].killed() <= k) { answer=true; break; }
     }
   }

#ifdef DOTIGHTEN

  if (B.length() & 1)
   {
    if ( FVS_state )
     {
       tighten(To,*FVS_state);
//       for (s=0; s < num_S; s++) Te[s].purgeAll();
     }
//    else	    
       for (s=0; s < num_S; s++) { Te[s].purgeAll(); To[s].purgeAll(); }

   }
  else
   {
    if ( FVS_state )
     {
       tighten(Te,*FVS_state);
//       for (s=0; s < num_S; s++) To[s].purgeAll();
     }
//    else	    
       for (s=0; s < num_S; s++) { Te[s].purgeAll(); To[s].purgeAll(); }

   }

#else /* no tighten */

  if (B.length() & 1)
   {
#ifdef DFVS
//cerr << "FVS state equals\n" << To << nl;
printFVSstate(bndry,To); // tighten( To );
#endif
    if ( FVS_state )
     {
       *FVS_state = To;
       for (s=0; s < num_S; s++) Te[s].purgeAll();
     }
    else	    
       for (s=0; s < num_S; s++) { Te[s].purgeAll(); To[s].purgeAll(); }

   }
  else
   {
#ifdef DFVS
printFVSstate(bndry,Te); // tighten( Te );
#endif
    if ( FVS_state )
     {
       *FVS_state = Te;
       for (s=0; s < num_S; s++) To[s].purgeAll();
     }
    else	    
       for (s=0; s < num_S; s++) { Te[s].purgeAll(); To[s].purgeAll(); }

   }

#endif

#if 0
#ifdef DOTIGHTEN
  if ( FVS_state!=0 && (ProbInfo::getInt(ProbInfo::CongruenceFlag) & 1) )
  {
   tighten( *FVS_state );
  }
#endif
#endif

  return answer;
 }

