/*
 * See if a BPG has a maximum path greater than k.  
 */

/*tex
 
\file{maxpath_b.c}
\path{src/bpg/algorithm}
\title{Definition of maximum path family functions}
\classes{MaxPathState}
\makeheader
 
xet*/

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "array/array.h"

#include "bpg/algorithm/maxpath_b.h"


// temp -- for debugging
//
#include <iostream>

/*************************************************************************/

void MaxPathState::newBoundaryVert(vertNum v)
 {
  assert( 0<=v && v<=1 );

  if (_bndryEj)
   {
    _maxPath[1-v] = max( _maxPath[1-v], (++_maxPath[v]) );
    _bndryEj = false;
   }
 
  _maxPath[v] = 0;
 }

void MaxPathState::addBoundaryEdge(vertNum i, vertNum j)
 {
  assert( 0<=i && i<=1 );
  assert( 0<=j && j<=1 );
  assert( i != j );

  assert( _bndryEj == false );

  _maxFound = max( _maxFound, _maxPath[i]+_maxPath[j]+1 );

  _bndryEj = true;
 }

/*************************************************************************/

// The final state is returned in the third arg, unless it is 0.
// It defaults to 0 if not specified.
//
bool maxPath( const BPG& B, int k, MaxPathState *MP_state )
 {
  cerr << nl << "Finding MaxPath " << k << " for " << B << nl;

  int bndry = B.boundarySize();

  /*
   * Initialize a maximum path state class.
   */
  MaxPathState *state = MP_state ? MP_state->init() : new MaxPathState;

  /*
   * Crunch away through the operator string.
   */
  int m; for (m=0; m < B.length(); m++)
   {
      Operator op = B[ m ]; // get the operator at position m

      if ( op.isVertexOp() )
         {
          vertNum v = op.vertex(); 

          state->newBoundaryVert(v);
	
         }// vert

       else if ( op.isEdgeOp() )
         {
          vertNum v1 = op.vertex1(),
                  v2 = op.vertex2();

          state->addBoundaryEdge(v1,v2);

         }// ej

       else    // hack job!! NoOp!!!
         {
          ;
         }

//cerr << m << " State: " << state->maxPath() << nl;

   }// m

cerr << m << " State: " << state->maxPath() << nl;

  int mp = state->maxPath();

  if ( !MP_state ) delete state;

  return ( mp <= k ? true : false );
 }

