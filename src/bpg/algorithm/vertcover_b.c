/*
 * See if a BPG has a k-vertex cover.  
 */

/*tex
 
\file{vertcover_b.c}
\path{src/bpg/algorithm}
\title{Definition of vertex cover family functions}
\classes{}
\makeheader
 
xet*/

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "array/array.h"
#include "general/bits.h"

#include "bpg/algorithm/vertcover_b.h"


// temp -- for debugging
//
#include <iostream>

/*************************************************************************/

#if 0
// OLD ALGORITHM!

// The final state is returned in the third arg, unless it is 0.
// It defaults to 0 if not specified.
//
bool vertCover( const BPG& B, int k, vertexCoverState* VC_state )
 {
  //cerr << nl << "Finding vertCover " << k << " for " << B << nl;

  int bndry = B.boundarySize();
  int num_S = 1 << bndry;

  vertexCoverState Te( num_S ), 	// State after even number of operators
                   To( num_S );		// State after odd number of operators

  /*
   * Initialize the number of kill vertices used at the start. 
   */

  for (int s=0; s<num_S; s++)
    Te[s] = bits(s);   // Template function to count bits in s.

//cerr << "Init Te " << Te << nl;

  for (int m=0; m < B.length(); m++)
   {
    Operator op = B[ m ]; // get the operator at position m

    if (m & 1)
     {
      for (s=0; s < num_S; s++)
       {
        if ( op.isVertexOp() )
         {
          vertNum v = op.vertex(); 

          if (s & (1<<v))
           {
  		Te[s] = min(To[s], To[s - (1<<v)]) + 1;
                if (Te[s]>k+1) Te[s]=k+1; 
           }
          else
           {
  		Te[s] = min(To[s], To[s + (1<<v)]);
           }
         }
      else if ( op.isEdgeOp() )
        {
          vertNum v1 = op.vertex1(),
                  v2 = op.vertex2();

          if (s & (1<<v1) || s & (1<<v2))
           {
  		Te[s] = To[s];
           }
          else
           {
  		Te[s] = k+1;
           }
        }// ej

      else    // hack job!! NoOp!!!
        {
          Te[s]=To[s];
        }

       }// s
     }// if m
    else
     { 
      for (s=0; s < num_S; s++)
       {
        if ( op.isVertexOp() )
         {
          vertNum v = op.vertex(); 

          if (s & (1<<v))
           {
  		To[s] = min(Te[s], Te[s - (1<<v)]) + 1;
                if (To[s]>k+1) To[s]=k+1; 
           }
          else
           {
  		To[s] = min(Te[s], Te[s + (1<<v)]);
           }
         }
      else if ( op.isEdgeOp() )
        {
          vertNum v1 = op.vertex1(),
                  v2 = op.vertex2();

          if (s & (1<<v1) || s & (1<<v2))
           {
  		To[s] = Te[s];
           }
          else
           {
  		To[s] = k+1;
           }
        }// ej

      else    // hack job!! NoOp!!!
        {
          To[s]=Te[s];
        }

       }// s

     }// else m

//cerr << m << " Te " << Te << nl;

//cerr << m << " To " << To << nl;

   }// m

  if (B.length() & 1)
   {
    if ( VC_state ) *VC_state = To;
    for (s=0; s < num_S; s++) if (To[s] <= k) return true;
   }
  else
   {
    if ( VC_state ) *VC_state = Te;
    for (s=0; s < num_S; s++) if (Te[s] <= k) return true;
   }

  return false;
 }
#endif 

/*************************************************************************/

bool vertCover( const BPG& G, int k, vertexCoverState* stateParam )
{
  //cerr << nl << "Finding vertCover " << k << " for " << G << nl;

  int bndry = G.boundarySize();
  int num_S = 1 << bndry;

  vertexCoverState _state( num_S );
  vertexCoverState _last( num_S );

  vertexCoverState* state = &_state;
  vertexCoverState* last = &_last;

  /*
   * Initialize the number of kill vertices used at the start. 
   */

  int s; for (s=0; s<num_S; s++)
    state->put( s, min( bits(s), k+1 ) );

   //cerr << "Init state " << *state << nl;

   for (int m=0; m < G.length(); m++)
   {
      vertexCoverState* tPtr = last;
      last = state;
      state = tPtr;

      Operator op = G[ m ]; // get the operator at position m

      for (s=0; s < num_S; s++)
      {
         if ( op.isVertexOp() )
         {
            vertNum v = op.vertex(); 

            if (s & (1<<v))
            {
               state->put( s, last->get( s - (1<<v) ) + 1 );
               if (state->get(s) > k+1) state->put( s, k+1 );
            }
            else
            {
               state->put( s, last->get( s ) );
            }
         }
         else if ( op.isEdgeOp() )
         {
             vertNum v1 = op.vertex1(),
                     v2 = op.vertex2();

             if (s & (1<<v1) || s & (1<<v2))
             {
                state->put( s, last->get( s ) );
             }
             else
             {
                state->put( s, min( 
                   last->get( s + (1<<v1) ), 
                   last->get( s + (1<<v2) )
                ) );
             }
         }// ej
         else    // hack job!! NoOp!!!
         {
            state->put( s, last->get(s) );
         }

      }// for s

      //cerr << m << " state " << *state << nl;

      if ( state->get(0) == k+1 )
      {
         // all infinity -> done, state will not change
         break;
      }

   }// for m

   if ( stateParam )
   {
      *stateParam = *state;
   }

   return state->get(0) <= k;
}


