// -------------------------------------------
// ---------------- genus_b.c ----------------
// -------------------------------------------

/*tex

\file{genus_b.c}
\path{src/bpg/algorithm}
\title{Calculate the genus of a BPG graph}
\classes{GenusState}
\makeheader

xet*/


#include "stdtypes.h"

#include "general/queue.h"

#include "bpg/bpg.h"

#include "graph/stdgraph2.h"
#include "graph/algorithm/genus_ga.h"
#include "graph/lgraph.h"

#ifdef CRAY
const Label NOLABEL=255;
#endif

//
// MOVE to a header file (and change to a class) !!!
//
struct genusState
 {
  LGraph WEB;		// Witness Embeded Boundaried-graph.
  int BlockSum;		// Genus of part of forgotten BPG.
 };

//-----------------------------------------------------------------------

static int madeBlock( LGraph &Web, const vertNum v )
 {
  vertNum n = Web.order();

  AdjLists L(n);
  DegreeSequence deg(n);
  //
  Web.getAdjLists( L, deg );

  switch ( deg[v] )
   {
     case 0:	// Irrevalent pull-off vertex.
     case 1:
		Web.rmNode(v);
		return 0;

     default:	// See if a new non-boundary block is created.

	    {
		Graph Block(1);		// Try to build a block.

  		VertArray map(n);	// Keep index of orig. vertices.

                map.fill(n);
  		//
		map[v]=0;

                Queue<vertNum> DFS;

                DFS.enque(v);

                vertNum vert, neighbor;
		//
 		int bndyCount = 0;
		//
                while( DFS.deque(vert) && bndyCount < 2 )
                  {
  		   for (int i=0; i < deg[vert]; i++)
                    {
       		      neighbor = L[vert][i];

		      if ( map[neighbor] == n )
                        {
			  map[neighbor] = Block.addNode();

			  if ( Web.getLabel(neighbor) != NOLABEL ) bndyCount++;
   			}

		      Block.addEdge( map[vert], map[neighbor] );
                    }
                  }

		/*
		 * Did we not find a block?
		 */
		if ( bndyCount >= 2 )	// Can only check for contractible v.
                 {
                   if ( deg[v] == 2 )
		     {
       		       if ( Web.isEdge( L[v][0], L[v][1] ) == false &&
        		    ( Web.getLabel(L[v][0]) != NOLABEL ||
        		      Web.getLabel(L[v][1]) != NOLABEL ) )
			  Web.contractEdge( v, L[v][0], Web.getLabel(L[v][0]) );	
                     }

                    return 0;
                 }

		/*
		 * We can remove a block from WEB.
		 */
                LGraph newWeb( n - Block.order() + bndyCount );

		// keep non-mapped and boundary vertices
		//
                int i; for (i=0; i<n; i++) 
                 if ( map[i] < n && Web.getLabel(i) == NOLABEL ) map[i]=n+1;
                  
		vertNum u=0;
                //
	        for (i=0; i<n-1; i++)
                 {
                  if ( map[i] != n ) continue;

		  map[i] = u++;
                  //	
		  newWeb.setLabel( map[i], Web.getLabel(i) );

                  for (int j=0; j<i; j++)
		   {
                    if ( map[j] < n ) 
		     {
		      if ( Web.isEdge(i,j) ) newWeb.addEdge( map[i], map[j] );
                     }
                   }
                 }
                  
		Web = newWeb;
		//
                return genusBrute( Block );
            }

   } // switch 
              
 }

/*
 * Membership test for genus k.
 */
bool genus( const BPG &B, int k, genusState *state )
 {

  LGraph Web;			// working witness

  int blockSum=0;  		// genus sum of forgotten blocks

  bool answer = true;

  for (int m=0; m < B.length() && answer == true; m++)
   {
      Operator op = B[ m ]; 	// get the operator at position m

      if ( op.isVertexOp() )
         {
           vertNum v = op.vertex(); 

           vertNum Vidx = 0;
           //
           if ( Web.isLVertex( (Label) v, Vidx ) )
            {

              Web.setLabel( Vidx, NOLABEL );

              /*
               * Update "genusState".  If a component contains only one
      	       * boundary vertex remove that component from Web.  Otherwise,
               * check and contract a degree 2 Vidx.
               */
              blockSum += madeBlock( Web, Vidx );

            }

	   // Add the new boundary vertex (label=v).
           //
           Web.addNode( 1, (Label) v);	
         }

      else if ( op.isEdgeOp() )
        {
          Label v1 = op.vertex1(), // vertNums
                v2 = op.vertex2();

          do  		// Add edges until next vertexOp or end of BPG.
            {
              assert ( Web.isLEdge( v1, v2 ) == false );

              Web.addLEdge( v1, v2 ); 

            } while ( m < B.length() && ( op = B[++m], op.isEdgeOp() ) );
          //
          m--;

          answer = genusBrute( Web, k-blockSum );
     
        }

   }// for m


  if ( state )
    {
      state -> WEB = Web;
      state -> BlockSum = blockSum;
    }

  return answer;

 }

