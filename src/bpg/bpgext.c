
// -------------------------------------------
// ---------------- bpgext.c -----------------
// -------------------------------------------

/*tex
 
\file{bpgext.c}
\path{src/bpg}
\title{Definitions of bounded pathwidth graph extension functions}
\classes{}
\makeheader
 
xet*/

#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "general/random.h"
#include "family/family.h"
#include "family/probinfo.h"
#include "search/graph_db.h"
#include <iostream>

static RandomInteger randGen( 100 );

static bool extendOutOfFamily1Try( const RBBPG& G, CBBPG& K, 
                                   const randomExtensionSearchInfo& info )
{
   bool keepConn = info.ExtnFlags & ExtensionConnected;
   bool keepTree = info.ExtnFlags & ExtensionTree;
   bool abidePretest = info.ExtnFlags & ExtensionPretest;
//if (abidePretest) cerr << "yes master\n";

   // We'll hack this graph
   //
   K.loadLhs( G, info.ExtnMaxLength );

   // current 'real' size of K (i.e. extension insertion point)
   //
   int pos = G.length();

   int bs = K.boundarySize();

   Array<int> connArray;
   updateConnectivityArray( K, connArray, 0, pos );

   BndryIncidenceMatrix M( bs );
   M.fill( false );

   // Shouldn't be any multi-edges anywhere in G.
   //
   Assert(! updateBndryIncidenceMatrix( K, M, 0, pos ) );

   //cerr << "start graph " << K << nl;

   bool success = false;

   int checkMembership = 0;

   //while( 1 )
   int limit; for (limit = 5000; limit > 0; --limit)
   {

      Operator op;

      randGen.modulus( 100 );
      if ( randGen() < info.ExtnEdgeWeight )
      {
         // edge op
         //
         op = Operator::randomEdgeOp( randGen );

         //cerr << "rand edge op " << op << nl;
         
         // would this make it multi-edged?
         //
         if ( M.at( op.vertex1(), op.vertex2() ) ) continue;

         if ( keepTree )
         {
            int v1 = op.vertex1();
            int v2 = op.vertex2();
            if ( connArray[v1] == connArray[v2] ) continue;
         }

         //cerr << "   op ok" << nl;
      }
      else
      {
         // vertex op
         //
         op = Operator::randomVertexOp( randGen );

         if ( keepConn )
         {
            int v = op.vertex();
            int compv = connArray[v];
            bool ok = false;
            for ( int j=0; j<bs; j++ )
               if ( v != j && connArray[j] == compv ) ok = true;
            if ( !ok ) continue;
         }

         //cerr << "rand vertex op " << op << nl;
         
         //cerr << "   op ok" << nl;
      }

      //cerr << "op=" << op << nl;

      // it's ok to add the op
      //
      assert( abidePretest || K.getOp( pos ).isNoOp() );
      K.setOp( pos, op );

      Assert(! updateBndryIncidenceMatrix( K, M, pos, pos+1 ) );
      updateConnectivityArray( K, connArray, pos, pos+1 );
      
      pos++;

      //cerr << "current graph " << K << nl;

      // ok, the graph has now been extended - test it if last op was an edge
      //
      if ( op.isEdgeOp() )
      {
        if ( abidePretest )
        {
          //cerr << "K=" << K << nl;
          int fail = Family::nonminimalPretests().applyBGraphTests( BGraph(K) );
          if ( fail != -1 )
          {
	    //cerr << "failed pretest " << fail << " at pos=" << pos << nl;
            pos--;
            // Note random amount trimmed off!
            randGen.modulus( 100 );
            int backup;
            for (backup = pos; 
                 backup > G.length() && randGen() < info.ExtnEdgeWeight/2; 
                 backup-- )
            {
              K.setOp( backup, Operator() );
            }

            pos = backup;
            assert(pos>=G.length());

            M.fill(false);
            Assert(! updateBndryIncidenceMatrix( K, M, 0, pos ) );
            connArray.resize(0);
            updateConnectivityArray( K, connArray, 0, pos );

            continue;
          }
        }

        checkMembership++;
        if ( checkMembership > info.ExtnSkip )
        {
           checkMembership = 0;
           if ( !Family::member( K ) )
           {
              success = true;
              break;
           }
        }
      }

      // can it get any longer?
      //
      if ( pos >= K.length() ) break;

   } // while

   //if (abidePretest) Family::nonminimalPretests().releaseBGraph();

   if (limit==0) 
   {
       cerr << " >> Warning!  Extension limit exceeded for " << G << nl;
       cerr << "    at postion " << pos << " in " << K << nl;
   }


   if ( ! success ) return false;

   // resize K to get rid of extra no-ops
   //
   K.resize( pos );

   return true;
}



bool extendOutOfFamily( const RBBPG& G, CBBPG& K, 
                        const randomExtensionSearchInfo& info, int run )
{
   if ( ProbInfo::getBool( ProbInfo::ExtnDeterministic ) )
   {
      IsoDBItem1 I1;
      IsoDBItem2 I2;
      initIsoDBItems( G, I1, I2, false );
      randGen.setSeed( (uint32_t)I1.getHash() + run * 0x3dab15f7 );
      // NOTE: this isn't enough; the effect of the random operators picked still depends
      // on the BPG. We would need to permute the random ops by the canon form -> G permutation
   }

   //cerr << "entry" << nl;
   for ( int i=0; i<info.ExtnTries; i++ )
   {
      //cerr << "call" << nl;
      if ( extendOutOfFamily1Try( G, K, info ) )
         return true;
   }
   return false;
}


