
/*
 *  Convert a BPG to PostScript format (sends to ostream).
 *  (Function prototype done in "psspec.h" ?)
 */

/*tex
 
\file{bpg2ps.c}
\path{src/bpg/i-o}
\title{Convert a BPG to PostScript format}
\classes{}
\makeheader
 
xet*/


#include <iostream>
#include <stdlib.h>

#include "stdtypes.h"
#include "array/array.h"
#include "array/sortarray.h"
#include "general/random.h"
#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "graph/i-o/psspec.h"

/*************************************************************************/

// Pointer to current callback object (defaults to BPG that we're drawing)
//
static const void *_callObj = 0;

/*************************************************************************/

static void plotEdge(ostream &out, vertNum v1, vertNum v2, 
                     int i, const PSSpec *specs)
 {
     /* 
      * Bluntly speaking, edge callbacks are somewhat screwed!
      * (First of two node parameters is the edge index into the BPG.)
      */
     if (specs && specs->getEdgeTypeFnc())
      {
        out << "v" << v1 << " v" << v2 << " edge" <<
                 specs->drawEdgeType( i, 0, _callObj ) << nl;

        if (specs->getEdgeStrFnc())
         {
          aassert(false); // write when needed
         }
      }
     else
      {
       out << "v" << v1 << " v" << v2 << " edge\n";
      }
 }

static void plotEdges(ostream &out, float PSWidth, float PSHeight,
                      const BPG &bpg, const PSSpec *specs)
 {
      /*
       * See how much work to do.
       */
      vertNum bSize = bpg.boundarySize();
      vertNum bLen = bpg.length();

      /*
       * How many pegs in our ladder representation?
       */
      VertArray bWidth(bSize);
      SortableArray<int> lft(bSize), rght(bSize);

      /*
       * Count the number of vertex pull-offs left/right of boundary.
       */
      //
      int i; for (i=0; i<bSize; i++)
       {
        steps( bpg, i, lft[i], rght[i] );
       }
      //
      int maxLeft = lft.max();
      int maxRight = rght.max();
      //
      int maxWidth = maxLeft + maxRight + 1;  // okay to assume bounary step

      /*
       * Allign boundary vertices.
       * (maxRight equals zero for noboundary or rightboundary graphs)
       */
      for (i=0; i<bSize; i++)
       {
        // if (lft[i]<maxLeft) bWidth[i] = bWidth[i] - (maxLeft - lft[i]);
        bWidth[i] = maxRight + lft[i] + 1;
       }

      /*
       * Assign (x,y) positions to each node.
       */ 
      float xoff = .1, yoff = .1;
      float xspace = (1.0-xoff)/((float) maxWidth+1);
      float yspace = (1.0-yoff)/((float) bSize);

      /*
       * Keep track of each boundary vertex.
       */
      vertNum vertID = 0;
      Array<vertNum> bndry(bSize);

      /*
       * Make graph by looking at each of the operators.
       */
      RandomInteger RND(1);		// Some randomness to see all edges.

      for (i=0; i<bLen; i++)
       {
         Operator op = bpg[i];

         /*
          * Assign an (x,y) coordinate to each node.
          */
         if (op.isVertexOp())
          {
           vertNum vert;
           vert = op.vertex();
           
           bndry[vert] 	= vertID++;

           out << "/v" << bndry[vert] << " { " << 
	    ( 1.0-xoff-xspace*bWidth[vert]-- - 
//             (vert-bSize/2)*(vert-bSize/2)/(50.0) * PSWidth << ' ' <<
            (vert-bSize/2)*(vert-bSize/2)*xspace/10.0) * PSWidth << ' ' <<
	    ( 1.0-yoff-vert*yspace -RND()/1000.0 ) * PSHeight << " } def\n";

          }

         /*
          * Draw the edges.
          */
         if (op.isEdgeOp())
          {
           vertNum vert1 = op.vertex1();
           vertNum vert2 = op.vertex2();

           plotEdge(out, bndry[vert1], bndry[vert2], i, specs);
          }

       }
 }

static void plotNodes(ostream &out, const BPG &bpg, const PSSpec *specs)
 {
  
    for (int i=0, vertID=0; i<bpg.length(); i++)
     {
       Operator op = bpg[i];

       if (op.isVertexOp())
        {
          if (specs)
           {
               // Ouch!  We loose the computed vertID on callbacks.
               //
               if ( specs->getNodeTypeFnc() )
                {
                  out << 'v' << vertID << " node"
                    << specs->drawNodeType( i, _callObj );
                }
               else
                {
                  if ( bpg.isBoundaryVertex(i) )
                    out << 'v' << vertID << " node3";
                  else
                    out << 'v' << vertID << " node0";
                }

               if (specs->getNodeStrFnc())
                { 
                  out<<  "  (" << specs->drawNodeStr( i, _callObj )
                               << ") v" << vertID << " label\n";
                }
               else out << nl;
           }
          else
           {
               if ( bpg.isBoundaryVertex(i) )
                 out << 'v' << vertID << " node3 (" << op.vertex() << ") v"
                            << vertID << " label\n";
               else
                 out << 'v' << vertID << " node0\n";
           }

          vertID++;
        }
     }
 }


// ------------------------------------------------------------------------
//                    DVI Special PostScript BPG Stuff
// ------------------------------------------------------------------------

static void doDVI(const BPG &bpg, const PSSpec *specs) 
 {
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   float PSWidth = 0.0, PSHeight = 600.0;
   //
   if (specs)
    {
     PSWidth = specs->getWidth();
     PSHeight = specs->getHeight();
    }

   // See if we need to compute a proportional width.
   //
   if (PSWidth==0.0)
    {
     float ss = (float) steps(bpg);
     float bs = (float) bpg.boundarySize();

     PSWidth = PSHeight * ss/bs /* * 0.8 */;

     // !!! caller should check for new width if 0 was set !!!
     //
     // if (specs) ((PSSpec*) specs)->setWidth(PSWidth + 0.5);  
    }

   out << "\\makebox[" << PSWidth << "bp][l]{\\rm\\normalsize\n";
   out << "\\rule{0bp}{" << PSHeight << "bp}\n";
   out << "\\special{\"\n";

   //out << "/Times-Roman findfont\n";
   //out << "12 scalefont setfont\n";

   /*
    * Let us scale so label fonts look better. (for small dvi boxes)
    */
   if (PSHeight != 600.0)
    {
      // We want to keep circles so scale evenly.
      //
      out << PSHeight/600.0 << ' ' << PSHeight/600.0 << " scale\n";

      PSWidth = PSWidth/(PSHeight/600.0);
    }

   plotEdges(out, PSWidth, 600.0, bpg, specs);

   plotNodes(out, bpg, specs);

   out << "}}\n";
 }



// ------------------------------------------------------------------------
//                    Bounded Box PostScript of a BPG
// ------------------------------------------------------------------------

/*
 *  Convert a bounded pathwidth graph to PostScript.
 */
static void doBB(const BPG &bpg, const PSSpec *specs)
{
   ostream *outp = &cout;
   //
   if (specs && specs->getOut()) outp = specs->getOut();
   //
   ostream &out(*outp);

   float PSWidth = 600.0, PSHeight = 600.0;
   //
   if (specs)
    {
     PSWidth = specs->getWidth();
     PSHeight = specs->getHeight();
    }


   /*
    * General Postscript comments and bounding box.
    */
   out << "%!VACS bpg2ps V1.1 (1993)\n";
   out << "%%BoundingBox: 0 0 " << PSWidth << ' ' << PSHeight << "\n%\n";
   //
   out << "%  BPG: " << bpg << nl;
   //
   out << "%  Order: " << bpg.vertices() << tab <<
	       "Size: " << bpg.edges() << "\n";
   out << "%  Width: " << bpg.boundarySize()-1 << tab <<
	       "Length: " << bpg.length() << "\n\n";

   /*
    * Make postscript viewable with ghostscript (gs); 
    */
   out << "(" << getenv("VACS") << "/lib/bpg.ps) run\n%\n";	

   /*
    * Call the common BPG laddar layout routine.
    */
   plotEdges(out, PSWidth, PSHeight, bpg, specs);

   out << nl;

   /* 
    * Mark boundary vertices with a square and the rest with circles.
    * (specs functions can override this.)
    */
   plotNodes(out, bpg, specs);
}

//-----------------------------------------------------------------------

/**
 ** Top level PostScript drawing routine for Parsed BPGs.
 **/

void bpg2ps(const BPG &B, const PSSpec *specs, const void *callObj)
 {
   if (specs)
    {
      // Do we want to use another object for the callback functions?
      //
      if (callObj) _callObj = callObj;
      else         _callObj = (const void *) &B;

      switch( specs->getFormat() )
       {
         case PSSpec::BoundingBox : doBB(B,specs); break;
         case PSSpec::DviSpecial :  doDVI(B,specs); break;
         case PSSpec::Poster :      aassert(false);
       }
    }
   else doBB(B,0);
 }

