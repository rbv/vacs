
/*
 *  Graph conversion routines.
 */

/*tex
 
\file{bpg2graph.c}
\path{src/bpg/i-o}
\title{Conversion routines between graph formats}
\classes{}
\makeheader
 
xet*/

#include "graph/graph.h"
#include "bpg/bpg.h"
#include "array/array.h"

// Mainly used for brute force membership tests in the "Graph" world.
//
/*
 *  Convert a (right/no boundary) BPG to a Graph.  (boundary gets lost)
 */
void bpg2graph(const BPG &bpg, Graph &G )
 {
      /*
       * Build a new Graph.
       */ 
      G.newGraph( bpg.order() );

      vertNum nextVert = 0;

      vertNum bSize = bpg.boundarySize();
      vertNum bLen = bpg.length();

      /*
       * Keep a reference to each boundary vertex.
       */
      VertArray bndry(bSize);

      for (int i=0; i<bLen; i++)
       {
         Operator op = bpg[i];

         if (op.isVertexOp())
          {
           vertNum vert;
           vert = op.vertex();
           
           bndry[vert] = nextVert++;
          }

         else if (op.isEdgeOp())
          {
           vertNum vert1 = op.vertex1();
           vertNum vert2 = op.vertex2();

#if 0
//
//Assert catch
//
if (bndry[vert1]==bndry[vert2]) 
 { cerr << "Equal: " << bpg << nl; assert(false); }

if (bndry[vert1]>=bpg.order() ) 
 { cerr << "order1: " << bpg << nl; assert(false); }

if (bndry[vert2]>=bpg.order() ) 
 { cerr << "order2: " << bpg << nl; assert(false); }
//
#endif
         if ( bndry[vert1] != bndry[vert2] )
           G.addEdge( bndry[vert1], bndry[vert2] );
          }

       }

 }

