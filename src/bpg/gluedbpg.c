
 
// -------------------------------------------
// --------------- gluedbpg.c ----------------
// -------------------------------------------

/*tex
 
\file{gluedbpg.c}
\path{src/bpg}
\title{Definition of glued bounded pathwidth graph classe}
\classes{GluedBPG}
\makeheader
 
xet*/

#include "bpg/gluedbpg.h"

#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "family/family.h"


GluedBPG::GluedBPG( const BPG& left, const BPG& right )
{
   // keep a copy of right, reversed and with bndry removed.
   //
   ::reverseAndStrip( right, glue );

   int size = left.size() + glue.size();

   // record the position of the start of the free area
   //
   glueStart = left.size();

   g1.resize( size );
   g2.resize( size );

   // copy left into g1
   //
   for ( int i=0; i < left.size(); i++ )
      g1.put( i, left[i] );

}


void GluedBPG::setG2( const BPG& left )
{
   // 2nd graphs must be shorted than original left graph
   //
   assert( left.size() + glue.size() <= g2.size() );

   for ( int i=0; i < left.size(); i++ )
      g2.put( i, left[i] );
}


bool GluedBPG::performTest( PermEnum* perm )
{
   int bs = Operator::boundarySize();

   PermEnum p( bs );

   do
   {
      // apply p to glue
      //
      for ( int i=0; i<glue.size(); i++ )
      {
         Operator op = glue[i].permute( p );
         g1.put( i+glueStart, op );
         g2.put( i+glueStart, op );
      }

      if ( ! Family::member( g1 ) && Family::member( g2 ) )
      {
         if ( perm ) *perm = p;
         return true;
      }

   } while ( p.next() );

   return false;
}
