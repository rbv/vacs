//-------------------------------------------------
//--------------- oparray.c -----------------------
//-------------------------------------------------

/*tex
 
\file{oparray.c}
\path{src/bpg}
\title{Definition of operator array class}
\classes{OpArray}
\makeheader
 
xet*/

#include "bpg/oparray.h"

int OpArray::numVertexOps() const
{
   int c = 0;
   for ( int i=0; i<size(); i++ )
      if ( (*this)[i].isVertexOp() ) c++;

   return c;
}

int OpArray::numEdgeOps() const
{
   int c = 0;
   for ( int i=0; i<size(); i++ )
      if ( (*this)[i].isEdgeOp() ) c++;

   return c;
}

int OpArray::ithVertex( int target ) const
{
   assert( target > 0 );

   int c = 0;
   for ( int i=0; i<size(); i++ )
   {
      if ( (*this)[i].isVertexOp() ) c++;
      if ( c == target ) return i;
   }
   return -1;
}

int OpArray::ithEdge( int target ) const
{
   assert( target > 0 );

   int c = 0;
   for ( int i=0; i<size(); i++ )
   {
      if ( (*this)[i].isEdgeOp() ) c++;
      if ( c == target ) return i;
   }
   return -1;
}

void OpArray::trim( int amount )
{
   for (int i = size() - 1; i >= 0; i--)
   {
      Operator op = (*this)[i];
      if ( !op.isNoOp() )
      {
         // support for trimming noops from end
         if ( amount == 0 )
         {
            resize( i + 1 );
            return;
         }
         amount--;
      }
   }
   resize( 0 );
}

// Forward vertex searches ----------------------------------------

// return position of next vertex operator in forward search
int OpArray::nextVertexOp( int i ) const
{
   for ( int j=i; j<size(); j++ )
      if ( (*this)[j].isVertexOp() ) return j;
   return -1;
}

// return position of next vertex operator = vOp in forward search
int OpArray::nextVertexOp( int i, Operator anOp ) const
{
   assert( ! anOp.isNoOp() );

   for ( int j=i; j<size(); j++ )
      if ( (*this)[j].isVertexOp() )
         if ( anOp.incident( (*this)[j] ) ) return j;
   return -1;
}

// return position of next vertex operator = vOp1 or vOp2 in forward search
int OpArray::nextVertexOp( int i, Operator vOp1, Operator vOp2 ) const
{
   assert( vOp1.isVertexOp() && vOp2.isVertexOp() );
   for ( int j=i; j<size(); j++ )
      if ( (*this)[j] == vOp1  || (*this)[j] == vOp2 ) return j;
   return -1;
}


// Backward vertex searches ----------------------------------------

// return position of next vertex operator in backward search
int OpArray::prevVertexOp( int i ) const
{
   for ( int j=i; j>=0; j-- )
      if ( (*this)[j].isVertexOp() ) return j;
   return -1;
}

// return position of next vertex operator = vOp in backward search
int OpArray::prevVertexOp( int i, Operator anOp ) const
{
   assert( ! anOp.isNoOp() );

   for ( int j=i; j>=0; j-- )
      if ( (*this)[j].isVertexOp() )
         if ( anOp.incident( (*this)[j] ) ) return j;
   return -1;
}

// return position of next vertex operator = vOp1 or vOp2 in backward search
int OpArray::prevVertexOp( int i, Operator vOp1, Operator vOp2 ) const
{
   assert( vOp1.isVertexOp() && vOp2.isVertexOp() );
   for ( int j=i; j>=0; j-- )
      if ( (*this)[j] == vOp1  || (*this)[j] == vOp2 ) return j;
   return -1;
}


// Forward edge searches ----------------------------------------

// return position of next edge operator in forward search
int OpArray::nextEdgeOp( int i ) const
{
   for ( int j=i; j<size(); j++ )
      if ( (*this)[j].isEdgeOp() ) return j;
   return -1;
}

// return position of next edge operator = eOp in forward search
int OpArray::nextEdgeOp( int i, Operator eOp ) const
{
   assert( eOp.isEdgeOp() );
   for ( int j=i; j<size(); j++ )
      if ( (*this)[j] == eOp ) return j;
   return -1;
}

// return position of next edge operator incident to op (an edge or vertex op)
int OpArray::nextEdgeOpIncident( int i, Operator op ) const
{
   for ( int j=i; j>=0; j-- )
      if ( (*this)[j].isEdgeOp() )
         if ( (*this)[j].incident( op ) ) return j;
   return -1;
}


// Backward edge searches ----------------------------------------

// return position of next edge operator in backward search
int OpArray::prevEdgeOp( int i ) const
{
   for ( int j=i; j>=0; j-- )
      if ( (*this)[j].isEdgeOp() ) return j;
   return -1;
}

// return position of next edge operator = eOp in backward search
int OpArray::prevEdgeOp( int i, Operator eOp ) const
{
   assert( eOp.isEdgeOp() );
   for ( int j=i; j>=0; j-- )
      if ( (*this)[j] == eOp ) return j;
   return -1;
}

// return position of next edge operator incident to op (an edge or vertex op)
int OpArray::prevEdgeOpIncident( int i, Operator op ) const
{
   for ( int j=i; j>=0; j-- )
      if ( (*this)[j].isEdgeOp() )
         if ( (*this)[j].incident( op ) ) return j;
   return -1;
}


