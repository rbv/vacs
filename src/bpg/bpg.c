 
// -------------------------------------------
// ------------------- bpg.c -----------------
// -------------------------------------------
 
/*tex
 
\file{bpg.c}
\path{src/bpg}
\title{Definition of bounded pathwidth graph classes}
\classes{BPG, RBBPG, CBBPG}
\makeheader
 
xet*/


// Function definitions for class BPG (bounded pathwidth graph)

#include "bpg/bpg.h"

//  Need the global functions to compute degree sequence and distance matrix.
//
#include "bpg/bpgutil.h"
#include "bpg/bpggrph.h"

// need the isomorphism stuff.
//
#include "isomorphism/iso_graph.h"

#include <strstream>
#include <ctype.h>
#include "general/str.h"
#include <string.h>

//---------------------------------------------------------------------


// ---- Utility functions

// convert hex char to int value
//
static uchar hexToInt( char c )
{
   if ( isdigit(c) ) return c - '0';
   return toupper(c) - 'A';
}

//---------------------------------------------------------------------

// --- Class BPG

// dtor
//
BPG::~BPG()
{
   clearCache();
}

void BPG::copyToMem( char* mem ) const
{
   memcpy( mem, baseAddr(), length() );
}

// Lexicographical compare for noop-less t-parses
bool BPG::operator<( const BPG& g ) const
{
   int sz = size();

   assert( sz == g.size() ); // lazy for now

   for ( int i=0; i<sz; i++ )
   {
      if ( getOp(i) < g.getOp(i) ) return true;
      if ( getOp(i) > g.getOp(i) ) return false;
   }
   return false;
}

// Lexicographical compare for t-parses with noops
bool BPG::compare( const BPG& rhs ) const
{
   int i = 0, j = 0;
   for (;;)
   {
      while ( i < size() && getOp(i).isNoOp() )
         i++;
      while ( j < rhs.size() && rhs.getOp(j).isNoOp() )
         j++;

      if ( i == size() )
      {
         if ( j == rhs.size() )
            return false;  // equal
         return true;  // lhs shorter
      }
      if ( j == rhs.size() )
         return false;  // lhs longer
         
      if ( getOp(i) < rhs.getOp(j) ) return true;
      if ( getOp(i) > rhs.getOp(j) ) return false;

      i++;
      j++;
   }
}


// Less-than canonic ordering <_c used to define canonical t-parses
// 
bool BPG::canonicLess( const BPG& rhs ) const
{
   assert( size() == rhs.size() );

   // Compare vertex op positions
   for ( int i=0; i < size(); i++ )
   {
      Operator o1 = getOp(i);
      Operator o2 = rhs.getOp(i);
      assert( !o1.isNoOp() && !o2.isNoOp() );
      if ( o1.isVertexOp() != o2.isVertexOp() )
      {
         //if ( vertices() != rhs.vertices() )
         //  return vertices() < rhs.vertices();
         if ( o1.isVertexOp() && !o2.isVertexOp() )
            return true;
         return false;
      }
   }

   // Compare vertex ops

   for ( int i=0; i < size(); i++ )
   {
      Operator o1 = getOp(i);
      Operator o2 = rhs.getOp(i);
      if ( o1.isVertexOp() )
      {
         if ( o1 != o2 )
            return o1 < o2;
      }
   }

   // Compare edge ops

   for ( int i=0; i < size(); i++ )
   {
      Operator o1 = getOp(i);
      Operator o2 = rhs.getOp(i);
      if ( o1.isEdgeOp() )
      {
         if ( o1 != o2 )
            return o1 < o2;
      }
   }

   // equal
   return false;
}

#if 1 /* mjd's version */
int BPG::fromString( Str str, BPG& g )
{
   int ops = str.countChar( ',' ) + 1;
   g.resize( ops );

   if ( ops == 0 ) return -1; // empty string -- return

   int len = str.length();
   char* s = new char[len];
   strncpy(s, (char*) str, len);

   int pos; for (pos=0; pos<len; pos++) if ( ! isxdigit(s[pos]) ) s[pos]=' ';

   istrstream i( s );

   char c1, c2;
   Operator op;

   for (pos=0; pos < ops; pos++)
   {

//cerr << "pos=" << pos << nl;

      i >> std::ws;
      if ( ! i.get(c1) ) { delete [] s; return pos; } 

//cerr << "c1="<< c1 << nl;
      
      if ( i.get(c2) )  // there's another char
      {

//cerr << "c2="<< c2 << nl;

         if ( ! isxdigit(c2) )
         {
             i.putback(c2); // vertex op
             op = Operator::vertexOperatorReturnFailure( hexToInt(c1) );
             g.setOp( pos, op );
         }
         else // edge operator
         {
             op = Operator::edgeOperatorReturnFailure( hexToInt(c1), 
							hexToInt(c2) );
             g.setOp( pos, op );
         }
      }
      else 
      {
         // vertex op at end of string
         op = Operator::vertexOperatorReturnFailure( hexToInt(c1) );
         g.setOp( pos, op );
      }

   } // for loop

   delete [] s;
   return -1;
}
#else
int BPG::fromString( Str s, BPG& g )
{
   int ops = s.countChar( ',' ) + 1;

   g.resize( ops );

   if ( ops == 0 ) return -1; // empty string -- return

   istrstream i( (char*) s );

   char c, c1, c2;

   int pos = 0;

   bool ok = false;

   Operator op;

   i >> ws;
   i.get(c1);
   if ( c1 != '[' ) i.putback(c1); // ignore leading [

   while( true )
   {

cerr << "pos=" << pos << nl;

      i >> ws;

      if ( ! i.get(c1) ) break; // no more, expecting an op

cerr << "c1="<< c1 << nl;
      
      if ( ! isxdigit(c1) ) break; // looking for hex char
      
      if ( i.get(c2) )  // there's another char
      {

cerr << "c2="<< c2 << nl;

         if ( ! isxdigit(c2) )
         {
             i.putback(c2); // vertex op
             op = Operator::vertexOperatorReturnFailure( hexToInt(c1) );
             if ( op.isNoOp() ) break;
             g.setOp( pos++, op );
         }
         else // edge operator
         {
             op = Operator::edgeOperatorReturnFailure( 
                     hexToInt(c1), hexToInt(c2) );
             if ( op.isNoOp() ) break;
             g.setOp( pos++, op );
         }
      }
      else 
      {
         // vertex op at end of string
         op = Operator::vertexOperatorReturnFailure( hexToInt(c1) );
         if ( op.isNoOp() ) break;
         g.setOp( pos++, op );
      }

      i >> ws;

      // check for end
      //
      if ( ! i.get(c) ) { ok = true; break; }

      if ( c == ']' )
      {
         ok = true;
         i.get(c);  // absorb eof
         break;
      }
      else
      {
       cerr << "eof char=" << int(c) << nl;
      }

      // check for ','
      //
      if ( c != ',' ) break;

   } // while true

   // make sure that is was eof
   //
   if ( !ok /* || !i.eof() */ )
      if ( i.tellg() == -1 ) return s.length();
      else return i.tellg();

   assert(ops == pos);
   return -1;
}
#endif

bool BPG::equalUpToSmaller( const BPG& g1, const BPG& g2 )
{
   int sz = min( g1.size(), g2.size() );
   for ( int i=0; i<sz; i++ )
      if ( g1.getOp(i) != g2.getOp(i) ) return false;
   return true;
}

//---------------------------------------

// --- Information ---

bool BPG::isIsolatedVertex( int index ) const
{
   Operator op = getOp( index );

   if ( ! op.isVertexOp() ) return false;

   // scan forward looking for op
   //
   for ( int i = index+1; i < size(); i++ )
   {
      Operator op2 = getOp( i );
      if ( op == op2 ) return true;  // isolated

      if ( op2.isEdgeOp() && op2.edgeHasVertex( op ) )
         return false;
   }

   return true; // no edge!

}

void BPG::getEdgeOpEndpoints( int index, int &v1, int &v2 ) const
{
   vertNum bSize = boundarySize();
   vertNum bLen = length();
   assert( index < bLen );

   VertArray bndry(bSize);

   for (int i=0, nextVert=0; i<index; i++)
   {
      Operator op = getOp(i);
      if (op.isVertexOp())
      {
         vertNum vert;
         vert = op.vertex();
         bndry[vert] = nextVert++;
      }
   }
   Operator op = getOp( index );
   assert( op.isEdgeOp() );

   v1 = bndry[op.vertex1()];
   v2 = bndry[op.vertex2()];
}

//---------------------------------------

// --- Minor operations ---

void BPG::deleteIsoVertex( int index )
{
   // clear the caches
   //
   changed();

   // get the vertex
   //
   Operator v = getOp(index);

   assert( isIsolatedVertex(index) );

   // delete to the right if possible
   //
   int delPos = index;
   int len = length();
   for ( int i=index+1; i<len; i++ )
      if ( getOp(i) == v )
         { delPos = i; break; }

   // set vertex to noOp
   //
   setOp( delPos, Operator::noOp() );

}


void BPG::deleteVertex( int index )
{

   // clear the caches
   //
   changed();

   // get the vertex
   //
   Operator v = getOp(index);

   assert( v.isVertexOp() );

   // set vertex to null
   //
   setOp( index, Operator::noOp() );

   // kill all edges incident
   //
   for ( int i=index+1; i<size(); i++ )
   {
      Operator op = getOp(i);

      // check if vertex left bndry
      //
      if ( op == v ) break;

      // kill edge if incident
      //
      if ( op.isEdgeOp() && op.edgeHasVertex( v ) )
         setOp( i, Operator::noOp() );
   }

}

//---------------------------------------

void BPG::deleteEdge( int index )
{
   assert( (*this)[index].isEdgeOp() );

   changed();

   setOp( index, Operator::noOp() );
}
//---------------------------------------

typedef Array<bool> ArrayBoolean;

void BPG::contractEdge( int ePos )
{
   if ( leftOfBoundary( ePos ) )
      leftOfBoundaryContract( ePos );
   else
      rightOfBoundaryContract( ePos );
}


void BPG::leftOfBoundaryContract( int ePos )
{
   changed(); // clear caches

   Operator e = getOp( ePos );  // get the edge

   assert( e.isEdgeOp() );  // make sure it is an edge

   bool isBndryEdge = isBoundaryEdge( ePos );
   setOp( ePos, Operator() ); // kill the edge

   int p1 = nextVertexOp( ePos+1, e );

   //cerr << "Hello! " << ePos << ' ' << *this <<  nl;
   // kmc - apr 96 - bndry-bndry edges
   if ( false && isBndryEdge )
   {
     assert( p1 == -1 );
     assert( boundaryType() == rightBoundary );

     int p = prevVertexOp( ePos-1, e );

     Operator a = e.vertexOperator1();
     Operator b = e.otherVertex( a );

     if ( getOp(p) == a )
     {
       b = a;
       a = e.otherVertex( b );
     }

     for ( int i = p+1; i<size() ; i++ )
     {
       Operator op = getOp( i );
       Operator z;

       // stop if b leaves boundary
       if ( op == b ) break;

       if ( ! op.isEdgeOp() ) continue; // only edges need be changed

       // multigraph only! if ( op == e ) continue;  // ignore edge (a,b)
       aassert( op != e );

       if ( op.edgeHasVertex( b ) )  // an edge (b,?)
       {
         z = op.otherVertex( b );  // edge is (b,z)
         op = a.edgeOperator( z );  // make it (a,z)
         setOp( i , op );
       }
     }
     removeMultipleEdges();

     // I'm worried - safety check
     //
     if ( ! isValid() )
     {
        cerr << CAST(*this,Array<Operator>) << nl;
        aassert( false );
     }

     return;
   }

   // internal check - one of them must be non-bndry
   //
   assert( p1 != -1 );

   Operator b = getOp( p1 );

   Operator a = e.otherVertex( b );

   // find first to join
   //
   int p2 = prevVertexOp( ePos-1, e );

   // internal check:
   //
   assert( p2 != -1 );

   Operator firstToJoin = getOp( p2 );

   setOp( p2, Operator() ); // kill the vertex op

   int i;

   for ( i = p2+1; i < p1; i++ )
   {

      Operator op = getOp( i );
      Operator z;

      if ( ! op.isEdgeOp() ) continue; // only edges need be changed

      // multigraph only! if ( op == e ) continue;  // ignore edge (a,b)
      aassert( op != e );
	
      if ( op.edgeHasVertex( b ) )  // an edge (b,?)
      {
         z = op.otherVertex( b );  // edge is (b,z)
         op = a.edgeOperator( z );  // make it (a,z)
         setOp( i , op );
         
      }

   }  // for i

   // now, if (a) left first, we have to rearrange the rest of the graph
   //
   if ( firstToJoin == a )
   {
      for ( i = p2; i >= 0; i-- )
      {
         setOp( i, getOp(i).exchangeVertices( a, b ) );
      }
   }


   removeMultipleEdges();

   // I'm worried - safety check
   //
   if ( ! isValid() )
   {
      cerr << CAST(*this,Array<Operator>) << nl;
      aassert( false );
   }


} 


void BPG::rightOfBoundaryContract( int ePos )
{
   changed(); // clear caches

   Operator e = getOp( ePos );  // get the edge

   assert( e.isEdgeOp() );  // make sure it is an edge

   setOp( ePos, Operator() ); // kill the edge

   int p1 = prevVertexOp( ePos-1, e ); 

   // should check - this should be non-bndry
   //
   assert( ! isBoundaryVertex( p1 ) );

   Operator b = getOp( p1 );
   Operator a = e.otherVertex( b );

   // not yet... setOp( p1, Operator() ); // kill the vertex op

   int p2 = nextVertexOp( ePos+1, e );
   if ( p2 == -1 ) p2 = length();

   int i;

   for ( i = p1+1; i < p2; i++ )
   {

      Operator op = getOp( i );
      Operator z;

      if ( ! op.isEdgeOp() ) continue; // only edges need be changed

      assert( op != e );
	
      if ( op.edgeHasVertex( b ) )  // an edge (b,?)
      {
         z = op.otherVertex( b );  // edge is (b,z)
         op = a.edgeOperator( z );  // make it (a,z)
         setOp( i , op );
         
      }

   }  // for i

   // now, if (a) left first, we have to rearrange the rest of the graph
   //
   if ( p2 != length() && getOp(p2) == a )
   {
      for ( i = p2; i < length(); i++ )
      {
         setOp( i, getOp(i).exchangeVertices( a, b ) );
      }
   }

   // kill a vertex op (right if possible)
   //
   if ( p2 != length() )
      setOp( p2, Operator() );
   else
      setOp( p1, Operator() );

   removeMultipleEdges();

   // I'm worried - safety check
   if ( ! isValid() )
   {
      cerr << CAST(*this,Array<Operator>) << nl;
      aassert( false );
   }

}  // contract edge

//---- Utility ------

// Remove multi-edges

bool BPG::removeMultipleEdges()
{

   // !!!
   // Efficiency can be improved: only need to use 1/2 of matrix
   // !!!

   // Hacked from hasMultipleEdge

   int bs = boundarySize();

   // create a incidence-matrix for the boundary
   SquareMatrix<bool> M( bs );

   // init to all false
   //
   M.fill( false );

   bool changed = false;

   for ( int i = 0; i < size(); i++ )
   {
      Operator op = getOp( i );

      if ( op.isVertexOp() )
      {
         // clear row/col
         M.fillRow( op.vertex(), false );
         M.fillCol( op.vertex(), false );
      }

      if ( op.isEdgeOp() )
      {
         int r = op.vertex1();
         int c = op.vertex2();

         // check for multi edge
         if ( M.at( r, c ) ) 
         {
            setOp( i, Operator::noOp() );
            changed = true;
         }

         M.put( r, c, true );
         M.put( c, r, true );
      }

   } // for i

   return changed;
}

//--------------------------------------------------------------

bool BPG::removeIsolatedVertices()
{
   bool found = false;

   for ( int i=0; i<size(); i++ )
   {
      if ( getOp(i).isVertexOp() )
         if ( isIsoNonboundaryVertex( i ) )
         {
            setOp( i, Operator::noOp() );
            found = true;
         }
   }
   return found;
}

//--------------------------------------------------------------

// cache gets cleared!! fix this later
//
void BPG::compress()
{

   int i, j;
   for ( i=0, j=0; i < length(); i++ )
   {
      Operator op = getOp(i);
      if ( ! op.isNoOp() )
         setOp( j++, op );
   }
   resize( j );
}


//---------------------------------------------------------------
//------------ Class NBBPG --------------------------------------
//---------------------------------------------------------------

NBBPG::NBBPG( const BPG& G, int start, int end )
{
   assert( start <= end );

   resize( end-start );

   for( int i=0; i<end-start; i++ )
      put( i, G.get( i+start ) );
}

// create from a RBBPG
//
NBBPG::NBBPG( const RBBPG& g ) : BPG( g )
{
}

NBBPG NBBPG::emptyGraph()
{
   return NBBPG();
}


bool NBBPG::isValid() const
{
   return true;
}


bool NBBPG::isEmpty() const
{
   // check for any operators
   //
   for ( int i=0; i<size(); i++ )
      if ( ! getOp(i).isNoOp() ) return false;

   return true;
}

void NBBPG::setEmpty()
{
   resize( 0 );
}


// Boundary

bool NBBPG::isBoundaryVertex( int ) const
{
   return false;
}


bool NBBPG::isBoundaryEdge( int ) const
{
   return false;
}


bool NBBPG::isNonboundaryEdge( int index ) const
{
   return getOp(index).isEdgeOp();
}


bool NBBPG::isIsoNonboundaryVertex( int index ) const
{
   return isIsolatedVertex( index );
}


bool NBBPG::leftOfBoundary( int ) const
{
   return false;
}

//---------------------------------------------------------------
//------------ Class RBBPG --------------------------------------
//---------------------------------------------------------------

/*
 * Base class definition of prepended boundary.
 */
int RBBPG::fromString( Str s, BPG& g )
 {
   if (s[1+(s[0]=='[')]==',') return BPG::fromString( s, g );  // no iso verts

   const char *bOps = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,";

   int bs = Operator::boundarySize();

   Str newStr( Str::withLength( bs*2 + s.length() ) );

   int i; for (i=0; i < bs*2; i++) newStr[i] = bOps[i];

   for (i=0; i < s.length(); i++) newStr[2*bs+i] = s[i];

   int errNum = BPG::fromString( newStr, g );
   //
   return (errNum < 0 ? errNum : errNum - bs*2);
 }


// lazy copy...fix later
//
RBBPG::RBBPG( const RBBPG& G, const NBBPG& E )
{
   int gs = G.size();
   int es = E.size();

   resize( gs + es );

   int i;
   for ( i=0; i<gs; i++ ) setOp( i, G.getOp( i ) );
   for ( i=0; i<es; i++ ) setOp( i+gs, E.getOp( i ) );
}


bool RBBPG::isValid() const
{
   int bs = boundarySize();
   Array<bool> b( bs );
   b.fill( false );

   int i; for ( i=0; i<size(); i++ )
      if ( getOp(i).isVertexOp() )
         b[ getOp(i).vertex() ] = true;
      else if ( getOp(i).isEdgeOp() )
         if ( !b[ getOp(i).vertex1() ] || !b[ getOp(i).vertex2() ] )
            return false;

   for ( i=0; i<bs; i++ )
      if ( ! b[i] ) return false;

   return true;
}

void RBBPG::setEmpty()
{
   int bs = Operator::boundarySize();

   resize( bs );

   for ( int i = 0; i < bs; i++ )
      setOp( i, Operator::vertexOperator( i ) );

}

bool RBBPG::isEmpty() const
{
   
   // check for just boundary

   int bs = boundarySize();
   Array<bool> b( bs );
   b.fill( false );

   for ( int i=0; i<size(); i++ )
   {
      Operator op = getOp(i);
      if ( op.isNoOp() ) continue;
      if ( op.isEdgeOp() ) return false; // edge => non empty

      if ( b[ op.vertex() ] ) return false; // seen vertex twice
      b[ op.vertex() ] = true;
   }

   return true;
}


bool RBBPG::isBoundaryVertex( int index ) const
{
   // get the vertex
   //
   Operator v = getOp(index);

   if ( ! v.isVertexOp() ) return false;

   // check for the vertex op in the rest of the graph
   //
   for ( int i=index+1; i<size(); i++ )
      if ( getOp(i) == v ) return false; // non-boundary

   return true;
}


bool RBBPG::isBoundaryEdge( int index ) const
{
   // get the edge
   //
   Operator e = getOp(index);

   if ( ! e.isEdgeOp() ) return false;

   Operator v1 = e.vertexOperator1();
   Operator v2 = e.vertexOperator2();

   for ( int i=index+1; i<size(); i++ )
      if ( getOp(i) == v1 || getOp(i) == v2 ) return false;

   return true;
}

bool RBBPG::isNonboundaryEdge( int index ) const
{
   // get the edge
   //
   Operator e = getOp(index);

   if ( ! e.isEdgeOp() ) return false;

   Operator v1 = e.vertexOperator1();
   Operator v2 = e.vertexOperator2();

   for ( int i=index+1; i<size(); i++ )
      if ( getOp(i) == v1 || getOp(i) == v2 ) return true;

   return false;
}



bool RBBPG::isIsoNonboundaryVertex( int index ) const
{
   Operator op = getOp( index );

   if ( ! op.isVertexOp() ) return false;

   // scan forward looking for op
   //
   for ( int i = index+1; i < size(); i++ )
   {
      Operator op2 = getOp( i );
      if ( op == op2 ) return true;  // it's isolated and not boundary

      if ( op2.isEdgeOp() && op2.edgeHasVertex( op ) )
         return false;  // not isolated
   }
   return false; // it's a bndry vertex
}


bool RBBPG::leftOfBoundary( int ) const
{
   return true;
}

RBBPG RBBPG::emptyGraph()
{
   int bs = Operator::boundarySize();

   RBBPG G( bs );

   for ( int i = 0; i < bs; i++ )
      G.setOp( i, Operator::vertexOperator( i ) );

   return G;
}


//---------------------------------------------------------------
//------------ Class CBBPG --------------------------------------
//---------------------------------------------------------------


// lazy copy...fix later
//
CBBPG::CBBPG( const RBBPG& G, const NBBPG& H )
{
   int gs = G.size();
   int hs = H.size();

   resize( gs + hs );

   bPos = gs;

   int i;

   for ( i=0; i<gs; i++ )
      setOp( i, G.getOp( i ) );

   for ( i=0; i<hs; i++ )
      setOp( i+gs, H.getOp( i ) );
}


CBBPG::CBBPG( const RBBPG& G, int rhsSize )
{
   loadLhs( G, rhsSize );
}


// lazy copy...fix later
//
void CBBPG::loadLhs( const RBBPG& G, int rhsSize )
{
   assert( rhsSize >= 0 );

   int ts = size(); 
   int gs = bPos = G.size();

   resize( gs + rhsSize );

   int i; for (i=0; i<gs; i++ ) setOp( i, G.getOp( i ) );
   for (; i<ts; i++ ) setOp( i, Operator::noOp() );  // mjd
}


// lazy copy...fix later
//
void CBBPG::getRhsAsExtension( NBBPG& E )
{
   // get rhs length
   //
   int l = size() - bPos;
   assert( l >= 0 );

   E.resize( l );

   for ( int i=0; i<l; i++ )
      E.setOp( i, getOp( bPos+i ) );
   
}


bool CBBPG::isValid() const
{
   int bs = boundarySize();
   Array<bool> b( bs );
   b.fill( false );

   int i; for ( i=0; i<bPos; i++ )
      if ( getOp(i).isVertexOp() )
         b[ getOp(i).vertex() ] = true;

   for ( i=0; i<bs; i++ )
      if ( ! b[i] ) return false;

   return true;
}

void CBBPG::setEmpty()
{
   aassert( false );
}

bool CBBPG::isEmpty() const
{
   int bs = boundarySize();
   Array<bool> b( bs );
   b.fill( false );

   int i; for ( i=0; i<bPos; i++ )
   {
      if ( getOp(i).isEdgeOp() ) return false; // an edge!

      if ( getOp(i).isVertexOp() )
      {
         int v = getOp(i).vertex();
         if ( b[ v ] ) return false;  // seen vertex op twice
         b[ v ] = true;
      }
   }

   // check the right side
   //
   for ( i=bPos; i<length(); i++ )
      if ( ! getOp(i).isNoOp()) return false; // another edge or vertex

   return true;
     
}


bool CBBPG::isBoundaryVertex( int index ) const
{
   // get the vertex
   //
   Operator v = getOp(index);

   if ( ! v.isVertexOp() ) return false;

   int i;

   /*
    * Check for another closer vertex v.
    */
   if (index >= bPos) return false;
   else
   for ( i=index+1; i<bPos; i++ )
     if ( getOp(i) == v ) return false;

   return true;

}
 

bool CBBPG::isIsoNonboundaryVertex( int index ) const
{
   if ( isBoundaryVertex( index ) ) return false;

   // use BPG method for now -- inefficient
   if ( isIsolatedVertex( index ) ) 
      return true;

   return false;
}

bool CBBPG::isBoundaryEdge( int index ) const
{
   if ( ! getOp(index).isEdgeOp() ) return false;

   return ! isNonboundaryEdge( index );
}

bool CBBPG::isNonboundaryEdge( int index ) const
{
   // get the edge
   //
   Operator e = getOp(index);

   if ( ! e.isEdgeOp() ) return false;

   Operator v1 = e.vertexOperator1();
   Operator v2 = e.vertexOperator2();

   if ( index < bPos )
   {
      // left side - look to the right, to bPos
      //
      for ( int i=index+1; i<bPos; i++ )
         if ( getOp(i) == v1 || getOp(i) == v2 ) return true;
   }
   {
      // right side - look to the left, to bPos
      //
      for ( int i=index-1; i>=bPos; i-- )
         if ( getOp(i) == v1 || getOp(i) == v2 ) return true;
   }

   return false;

}


bool CBBPG::leftOfBoundary( int index ) const
{
   return index < bPos;
}


CBBPG CBBPG::emptyGraph()
{
   int bs = Operator::boundarySize();

   CBBPG G( bs );

   for ( int i = 0; i < bs; i++ )
      G.setOp( i, Operator::vertexOperator( i ) );

   G.bPos = bs;

   return G;
}


//---------------------------------------------------------------
//------------ Class LBBPG ------------------------------------
//---------------------------------------------------------------


void LBBPG::appendToAxiom( const NBBPG& e )
{
   int bs = e.boundarySize();
   resize( bs + e.size() );

   // insert axiom
  int i; for ( i=0; i<bs; i++ ) setOp( i, Operator::vertexOperator( i ) );

   for ( i=0; i<e.size(); i++ ) setOp( i+bs, e.getOp(i) );

}

bool LBBPG::isBoundaryVertex( int index ) const
{
   Operator v = getOp(index);

   if ( ! v.isVertexOp() ) return false;

   for ( int i=0; i<index; i++ )
      if ( getOp(i) == v ) return false;

   return true;
}


BPG* LBBPG::copy() const { aassert(false); return 0; }
void LBBPG::update( const BPG& ) { aassert(false); }
BPG::BoundaryType LBBPG::boundaryType() const { return leftBoundary; }
void LBBPG::setEmpty() { aassert(false); }
bool LBBPG::isValid() const { aassert(false); return false; }
bool LBBPG::isEmpty() const { aassert(false); return false; }
bool LBBPG::isIsoNonboundaryVertex( int ) const {aassert(false); return false; }
bool LBBPG::isBoundaryEdge( int ) const { aassert(false); return false; }
bool LBBPG::isNonboundaryEdge( int ) const { aassert(false); return false; }
bool LBBPG::leftOfBoundary( int ) const { aassert(false); return false; }
