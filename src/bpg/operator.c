
// -------------------------------------------
// --------------- operator.c ----------------
// -------------------------------------------

/*tex
 
\file{operator.c}
\path{src/bpg}
\title{Definition of operator class}
\classes{Operator}
\makeheader
 
xet*/

#include "bpg/operator.h"

#include "general/permutes.h"
#include "general/random.h"

// Methods for class Operator

// statics
//
int Operator::bs;
//
uchar Operator::_firstOpValue;
uchar Operator::_firstVertexOpValue;
//
Operator* Operator::enumToOpTable;
//
int* Operator::opToEnumTable;

// Class initialisation - allocate and init the static table

void Operator::initTables( int boundarySize )
{

   // get the boundary size from runInfo (for code readability)
   bs = boundarySize;

   _firstOpValue = Operator( 0 ).op;
   _firstVertexOpValue = Operator( 0 ).op;

   enumToOpTable = new Operator[ 256 ];
   opToEnumTable = new int[ 256 ];

   int k = 0; // for indexing table
   int i, j;

   for ( i=0; i<bs; i++ )
   {
      Operator theOp = Operator( i );
      enumToOpTable[ k ] = theOp;
      opToEnumTable[ theOp.op ] = k++;
   }

   for ( i=0; i<bs; i++ )
      for ( j=i+1; j<bs; j++ )
      {
         Operator theOp = Operator( i, j );
         enumToOpTable[ k ] = theOp;
         opToEnumTable[ theOp.op ] = k++;
      }

   Operator theOp = Operator::noOp();
   enumToOpTable[ k ] = theOp;
   opToEnumTable[ theOp.op ] = k++;

   assert( k == bs*(bs+1)/2 + 1 );

}


void Operator::destroyTables()
{
   delete [] enumToOpTable;
   delete [] opToEnumTable;
}


//-----------------------------------------

// postfix only  (removed dummy int arg?!? cfront bug?)
//
Operator Operator::operator++()
{

   assert( ! isNoOp() );
   *this = Operator::enumToOp( opToEnum() + 1 );
   return *this;

}

Operator Operator::operator++( int )
{

   assert( ! isNoOp() );
   *this = Operator::enumToOp( opToEnum() + 1 );
   return *this;
}

bool Operator::isLastVertexOp() const
{
   assert( isVertexOp() );
   return vertex() == bs - 1;
}

//-----------------------------------------

bool Operator::incident( const Operator Op )
{
   if (isNoOp() || Op.isNoOp()) return false;

   if ( isVertexOp() )
      if ( Op.isVertexOp() )
         return *this==Op;
      else
         return lowNibble()==Op.lowNibble() || lowNibble()==Op.highNibble();
   else
      if ( Op.isVertexOp() )
         return lowNibble()==Op.lowNibble() || highNibble()==Op.lowNibble();
      else
         return lowNibble()==Op.lowNibble() ||
                lowNibble()==Op.highNibble() ||
                highNibble()==Op.lowNibble() ||
                highNibble()==Op.highNibble();

}

//-----------------------------------------

bool Operator::operator<( const Operator Op ) const
{

   assert( !isNoOp() && !Op.isNoOp() );

   if ( isVertexOp() )
   {
      if ( Op.isEdgeOp() ) return true;
      if ( vertex() < Op.vertex() ) return true; else return false;
   }

   if ( Op.isVertexOp() ) return false;

   // both edges...

   if ( vertex1() < Op.vertex1() ) return true;
   if ( vertex1() > Op.vertex1() ) return false;
   if ( vertex2() < Op.vertex2() ) return true;
   return false;
}

//-----------------------------------------

Operator Operator::randomVertexOp( RandomInteger &rand )
{
   rand.modulus( bs );
   return( vertexOperator( rand() ) );
}

Operator Operator::randomEdgeOp( RandomInteger &rand )
{
   rand.modulus( bs );
   int o1 = rand();
   rand.modulus( bs - 1 );
   int o2 = rand();
   if ( o2 >= o1 )
      o2++;
   return( edgeOperator( o1, o2 ) );
}


//-----------------------------------------

Operator Operator::changeVertex( Operator v1, Operator v2 )
{

   assert( v1.isVertexOp() && v2.isVertexOp() );

   aassert( false ); // not implemented

   return v1; // to avoid compile error
}


Operator Operator::exchangeVertices( Operator v1, Operator v2 )
{
   assert( v1.isVertexOp() && v2.isVertexOp() );

   if ( isVertexOp() )
   {
      if ( *this == v1 ) return v2;
      if ( *this == v2 ) return v1;
      return *this;
   }

   if ( isEdgeOp() )
   {
      if ( edgeHasVertex( v1 ) )
      {
         Operator z = otherVertex( v1 );
         if ( z == v2 )
            return *this;  // edge is v1,v2
         else
            return v2.edgeOperator( z );  // edge is v1,z
      }

      // edge does not have v1

      if ( edgeHasVertex( v2 ) )
      {
         return v1.edgeOperator( otherVertex( v2 ) ); // edge is v2, ?
      }

      return *this;  // edge does not have v1 or v2
   }

   // must be noOp -- do nothing
   return *this;

}

//-----------------------------------------

Operator Operator::permute( const PermEnum& p )
{

   if ( isVertexOp() )
   {
      return vertexOperator( p[ vertex() ] );
   }

   if ( isEdgeOp() )
   {
      return edgeOperator( p[ vertex1() ], p[ vertex2() ] );
   }

   // must be noOp -- do nothing
   return *this;

}

