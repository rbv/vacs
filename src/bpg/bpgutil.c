
// -------------------------------------------
// ---------------- bpgutil.c ----------------
// -------------------------------------------

/*tex
 
\file{bpgutil.c}
\path{src/bpg}
\title{Definitions of bounded pathwidth graph utility functions}
\classes{}
\makeheader
 
xet*/

#include "bpg/bpg.h"
#include "family/family.h"
#include "general/random.h"
#include "graph/stdgraph2.h"
#include "general/str.h"

#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"

#include <iostream>


//--------------------------------------------------------------

// check if the graph has a multiple edge
//
bool hasMultipleEdge( const BPG &G )
{

   // !!!
   // Efficiency can be improved: only need to use 1/2 of matrix
   // !!!

   int bs = G.boundarySize();

   // create a incidence-matrix for the boundary
   SquareMatrix<bool> M( bs );

   // init to all false
   //
   M.fill( false );

   bool ret = updateBndryIncidenceMatrix( G, M );

   return ret;
}

//--------------------------------------------------------------

// Implementation could be much better.
// This is quick and dirty.
//
bool hasIsoNonboundaryVertex( const BPG& g )
{
   for ( int i=0; i<g.size(); i++ )
   {
      if ( g[i].isVertexOp() )
         if ( g.isIsoNonboundaryVertex( i ) ) return true;
   }
   return false;
}

//-------------------------------------------------------------------------


bool findAllDangles( 
   const BPG& G,
   Array<int> *U, 
   Array<int> *V,
   Array<int> *UV
)
{
   assert( G.boundaryType() == BPG::rightBoundary );

   if (U)   U->resize(0);
   if (V)   V->resize(0);
   if (UV) UV->resize(0);

   bool hasDangle = false;

   int l = G.length();

   for ( int i=0; i<l; i++ )
   {
      Operator u = G.get( i );

      // make sure it is a vertex, and it isn't boundary
      //
      if ( ! u.isVertexOp() ) continue;
      //
      if ( G.isBoundaryVertex( i ) ) continue;

      // check if u is degree 1 and get edge if it is
      //
      int ePos = -1; // holds the edge that we found, -1 if none or > 1
      //
      for ( int j=i+1; j<l; j++ )
      {
         Operator z = G[ j ];
         if ( z == u ) break;  // end of search of u's edges

         if ( z.isEdgeOp() && z.edgeHasVertex( u ) )
         {
            if ( ePos != -1 ) { ePos = -1; break; } // degree > 1
            ePos = j;
         }
      }

      if ( ePos != -1 ) // u is degree 1
      {
         hasDangle = true;

         // get v
         Operator v = G.get( ePos ).otherVertex( u );

         // find where v enter's
         int vPos = G.prevVertexOp( ePos-1, v );
         assert( vPos != -1 );

         if ( U )   U->append( i );
         if ( V )   V->append( vPos );
         if ( UV ) UV->append( ePos );
      }

   } // for i

   return hasDangle;
}


bool hasDangleWithOtherVertexDegreeGT1( const BPG& G )
{
   Array<int> U;
   Array<int> V;
   Array<int> UV;

   findAllDangles( G, &U, &V, &UV );

   bool found = false;

   // go through the v's of the dangles , looking for degree > 1
   //
   for ( int i=0; i<V.length(); i++ )
   {
      int vPos = V[ i ];
      Operator v = G[ vPos ];

      // this should be replaced with deg(vPos), but I can't find it (:.
      int deg = 0;
      for ( int j = vPos+1; j < G.length(); j++ )
      {
         Operator z = G.get( j );

         if ( z == v ) break; // v left boundary -- no edge

         if ( z.isEdgeOp() && z.edgeHasVertex( v ) ) deg++;
      }

      if ( deg > 1 ) { found = true; break; } // all done

   } // for i

/*
   cerr << "dangle: " << int(found) 
        << " u = " << U 
        << " v = " << V 
        << " g = " << G 
        << nl;
*/

   return found;
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

bool isDisconnected( const BPG& G )
{
   assert( G.boundaryType() == BPG::rightBoundary || 
           G.boundaryType() == BPG::noBoundary );

   int bs = G.boundarySize();

   int compNum = 1;  // current component number

   bool connected = true;

   Array<int> comp( bs );  // components (comp[i]==comp[j] iff same comp)

   // initial component
   //
   comp.fill( 0 );

   for ( int i=0; i<G.length(); i++ )
   {
      Operator op = G[i];

      if ( op.isVertexOp() )
      {
         vertNum v = op.vertex();
         int compv = comp[v];

         if ( !compv ) // first vertex n pos v - special case
         {
            comp[v] = compNum++;
            continue;
         }

         bool vConnected = false; 

         // check if v is connected to anything else on the bndry
         //
         for ( int j=0; j<bs; j++ ) 
            if ( j != v && compv == comp[j] ) { vConnected = true; break; }

         if ( !vConnected ) 
         {
            connected = false;
            break;
         }

         // assign a new component number to v
         comp[v] = compNum++;
         continue;
      }

      if ( op.isEdgeOp() )
      {
         vertNum v1 = op.vertex1();
         vertNum v2 = op.vertex2();

         int compv1 = comp[v1];
         int compv2 = comp[v2];

         for ( int j=0; j<bs; j++ )
            if ( comp[j] == compv2 ) comp[j] = compv1;
      }
   }

   if ( connected && G.boundaryType() == BPG::noBoundary )
   {
      // make sure boundary is connected too

      // Vertex operators might be missing, carefully skip those
      int anycomp = -1;
      for ( int j=0; j<bs; j++ )
         if ( comp[j] ) { anycomp = comp[j]; break; }
      assert( anycomp > 0 );

      for ( int j=0; j<bs; j++ )
         if ( comp[j] && anycomp != comp[j] ) { connected = false; break; }
   }

   //cerr << "disc: " << int(!connected) << ' ' << G << nl;
   return !connected;
}

//-------------------------------------------------------------------------

template<class _BPG>
bool isBiconnected( const _BPG& G )
{
   assert( G.boundaryType() == BPG::rightBoundary || 
           G.boundaryType() == BPG::noBoundary );
   //cerr << "checking " << G << nl;
   for (int i=0; i<G.size(); i++)
   {
     Operator op = G.getOp( i );
     if ( ! op.isVertexOp() ) continue;
     if ( G.isBoundaryVertex(i) ) continue;

     _BPG graph(G);
     graph.deleteVertex( i );

     if ( isDisconnected(graph) ) return false;
   }

   return true;
}

template
bool isBiconnected( const RBBPG& G );
template
bool isBiconnected( const NBBPG& G );

// Slighty optimised version of the above:
// we assume that the pretest has already been run on all prefixes
bool isBiconnectedTest( const RBBPG& G )
{
   Operator op = G.getOp( G.length() - 1 );
   if ( ! op.isVertexOp() )
      return true;

   return isBiconnected( G );

   /* Find the new interior vertex... but it's no use
   int new_interior_op = G.prevVertexOp( G.length() - 2, op );
   if ( new_interior_op < 0 )
      return false;
   */
}

//-------------------------------------------------------------------------

bool hasNonboundaryDeg2( const BPG& G )
{
  for (int i=0; i<G.length(); i++ )
  {
    if ( ! G[i].isVertexOp() ) continue;
    if ( G.isBoundaryVertex(i) ) continue;

    int deg = 0;
    for ( int j=i+1; j<G.length() && G[j] != G[i]; j++ )
      if ( G[j].isEdgeOp() && G[j].edgeHasVertex( G[i] ) )
        deg++;

     if ( deg == 2 ) return true;
  }
  return false;
}


bool hasBridge( const BPG& graph )
{
   // make a copy for working on
   BPG* graphCopy = graph.copy();
   BPG& G =  *graphCopy;

   bool bridge = false;

   assert( ! hasMultipleEdge( G ) );

   for ( int i=0; i<G.length(); i++ )
   {
      Operator op = G.getOp( i );
      if ( !op.isEdgeOp() ) continue;

      G.setOp(i,Operator());
      if ( isDisconnected(G) ) { bridge = true; break; }
      G.setOp(i,op);
   }

   delete graphCopy;
   return bridge;
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

void updateConnectivityArray( const BPG& G, Array<int>& array, 
  int start, int end  )
{

   int bs = G.boundarySize();

   assert( end <= G.size() );
   assert( start >=0 && start <= end );

   if ( array.size() == 0 )
   {
      assert( start == 0 ); 

      array.resize( bs+1 );

      for ( int i=0; i<=bs; i++ ) array[i] = i;
   }

   int compNum = array[bs];

   for ( int i=start; i<end; i++ )
   {
      Operator op = G[i];

      if ( op.isVertexOp() )
      {
         vertNum v = op.vertex();

         // assign a new component number to v
         array[v] = compNum++;
         continue;
      }

      if ( op.isEdgeOp() )
      {
         vertNum v1 = op.vertex1();
         vertNum v2 = op.vertex2();

         int compv1 = array[v1];
         int compv2 = array[v2];

         // Make compv2 an unusable component #
         //
         for ( int j=0; j<bs; j++ )
            if ( array[j] == compv2 ) array[j] = compv1;
      }
   }

   array[ bs ] = compNum;

}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

bool isAcyclic( const BPG& G )
{

   int bs = G.boundarySize();
   int l = G.size();

   Array<int> array( bs );

   int i; for ( i=0; i<bs; i++ ) array[i] = i;

   int compNum = bs;

   for ( i=0; i<l; i++ )
   {
      Operator op = G[i];

      if ( op.isVertexOp() )
      {
         vertNum v = op.vertex();

         // assign a new component number to v
         array[v] = compNum++;
         continue;
      }

      if ( op.isEdgeOp() )
      {
         vertNum v1 = op.vertex1();
         vertNum v2 = op.vertex2();

         int compv1 = array[v1];
         int compv2 = array[v2];

         if ( compv1 == compv2 ) return false;

         // Make compv2 an unusable component #
         //
         for ( int j=0; j<bs; j++ )
            if ( array[j] == compv2 ) array[j] = compv1;
      }
   }

   return true;

}

//-------------------------------------------------------------------------

// ladder layout parameters.
//
//
int steps( const BPG &G, int v, int &left, int &right )
 {
#if 0
//cerr << "steps for G = " << G << " v = " << v << nl;

for (int t=0; t<G.length(); t++)
 if ( G[t].isVertexOp()==true && G[t].vertex()==v && 
      G.isBoundaryVertex(t)==true )  cerr << " *** boundary at " << t << nl;
#endif

    assert( v>=0 && v<G.boundarySize() );

    /*
     * How many left/right pegs in our ladder representation?
     */
    left = right = 0;
    int *add = &left;

    /*
     * Count the number of each vertex pull-offs.
     */
    for (int i=0; i<G.length(); i++)
     {
       Operator op = G[i];

       if ( op.isVertexOp()==true && op.vertex()==v )
        {
         if ( G.isBoundaryVertex(i) ) add = &right;
         else			      (*add)++;
        }
     }

// mjd tmp. test
//
//assert( add == &right);

  // Take into account the possibility of no boundary vertex.
  //
  return left+right+(add==&right);  

 }

int steps( const BPG &G )
 {
   int bs = G.boundarySize();

   SortableArray<int> lft( bs ), rght( bs );

   for (int i=0; i<bs; i++) steps( G, i, lft[i], rght[i] );

   return lft.max() + rght.max() + 1;
 };
