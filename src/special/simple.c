
#include "general/op_eq.h"

#include "bpg/operator.h"
#include "search/nodenum.h"
#include "search/searchnode.h"

// Specialisation for simple data type

bool simpleType( const int& )   { return true; }
bool simpleType( const char& )  { return true; }
bool simpleType( const short& ) { return true; }
bool simpleType( const long& )  { return true; }
bool simpleType( const uint& )   { return true; }
bool simpleType( const uchar& )  { return true; }
bool simpleType( const ushort& ) { return true; }
bool simpleType( const ulong& )  { return true; }

bool simpleType( const Operator& )  { return true; }
bool simpleType( const NodeNumber& )  { return true; }
bool simpleType( const SearchNode& )  { return true; }
