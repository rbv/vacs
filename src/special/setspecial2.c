
/*tex
 
\file{uocp.c}
\path{src/special}
\title{Specializations for set classes}
\classes{}
\makeheader
 
xet*/

#ifdef SUNPRO

#include <iostream>
#include "set/uocp.h"
#include "set/uociter.h"

#ifndef PCOLLECTION_NEW
#define PCOLLECTION_NEW( T ) 						\
PCollection< T >::PCollection( const PCollection< T >& C )		\
   : Collection< T* >( C ) 						\
{									\
   for (int i=0; i<Array< T* >::size(); i++)				\
      if ( (*this)[i] ) (*this)[i] = new T( *(*this)[i] );		\
}									\
									\
void PCollection< T >::copy( const PCollection< T >& C )		\
{									\
   Collection< T* >::operator=( C );					\
   for (int i=0; i<Array< T* >::size(); i++)				\
    if ( (*this)[i] ) (*this)[i] = new T( *(*this)[i] );		\
}
#endif

#include "graph/indgrph.h"

PCOLLECTION_NEW( InducedGraph )

#endif
