
// This is not a template file!

/*tex
 
\file{arrayspecial.c}
\path{src/special}
\title{Specializations for array classes}
\classes{}
\makeheader
 
xet*/

#include <stdlib.h>

#include "array/array.h"

//-------------------------------------------------------------
// Simple sorting

typedef int (*CompareFunction)( const void*, const void* );

static int intCompare( const int* i, const int* j ) 
{ return *i - *j; }

template<>
void Array<int>::simpleSortUp()
{
   qsort( baseAddr(), size(), sizeof(int), (CompareFunction) intCompare );
}

static int shortCompare( const short* i, const short* j ) 
{ return *i - *j; }

template<>
void Array<short>::simpleSortUp()
{
   qsort( baseAddr(), size(), sizeof(short), (CompareFunction) shortCompare );
}

//-------------------------------------------------------------

#if 0
#include "array/dyarray.h"

class L_InternalNode;

DyArray<L_InternalNode>::DyArray<L_InternalNode>(int,int) 
{
 aassert(false);
}
#endif


//-------------------------------------------------------------


//template class Array<bool>;
