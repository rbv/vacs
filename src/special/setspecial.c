
/*tex
 
\file{uocp.c}
\path{src/special}
\title{Specializations for set classes}
\classes{}
\makeheader
 
xet*/

#if defined(SUNPRO) || defined(CANONIC)

#include <iostream>
#include "set/uocp.h"
#include "set/uociter.h"

#ifndef PCOLLECTION_NEW
#define PCOLLECTION_NEW( T ) 						\
PCollection< T >::PCollection( const PCollection< T >& C )		\
   : Collection< T* >( C ) 						\
{									\
   for (int i=0; i<Array< T* >::size(); i++)				\
      if ( (*this)[i] ) (*this)[i] = new T( *(*this)[i] );		\
}									\
									\
void PCollection< T >::copy( const PCollection< T >& C )		\
{									\
   Collection< T* >::operator=( C );					\
   for (int i=0; i<Array< T* >::size(); i++)				\
    if ( (*this)[i] ) (*this)[i] = new T( *(*this)[i] );		\
}
#endif

#ifndef PCOLLECTION_DEL
#define PCOLLECTION_DEL( T )
/* something */
#endif

#ifndef P2COLLECTION_NEW
#define P2COLLECTION_NEW( T ) 						\
void PCollection< T >::addSetDeep( const PCollection< T > &C )		\
{		\
  for (int i=0; i< C.Array< T* >::size(); i++ )		\
   {		\
     if ( C[i] != _empty ) add( new T( *C[i] ) );		\
   }		\
}		\
\
void PCollection< T >::addDeep( const T *a )		\
{		\
     add( new T( *a ) );		\
}
#endif

#include "graph/ltree.h"

PCOLLECTION_NEW( Tree )

PCOLLECTION_NEW( LTree )

P2COLLECTION_NEW( LTree )

/*
void PCollection< LTree >::addSetDeep( const PCollection< LTree > &C )
{
  for (int i=0; i< C.Array< LTree* >::size(); i++ )
   {
     if ( C[i] != _empty ) add( new LTree( *C[i] ) );
   }
}
void PCollection< LTree >::addDeep( const LTree *a )
{
     add( new LTree( *a ) );
}
*/

PCOLLECTION_NEW( PIterSet<LTree> )

P2COLLECTION_NEW( PIterSet<LTree> )

#include "bpg/bpg.h"

PCOLLECTION_NEW( NBBPG )

#include "bpg/algorithm/pathwidth_b.h"

PCOLLECTION_NEW( PWChar)

#if defined(CANONIC)
#include "graph/indgrph.h"
PCOLLECTION_NEW( InducedGraph )
#endif


#endif
