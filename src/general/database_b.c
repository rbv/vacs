
// -------------------------------------------
// --------------- database_b.c --------------
// -------------------------------------------
 
/*tex
 
\file{database_b.c}
\path{src/general}
\title{Definition of database base class}
\classes{DatabaseBase}
\makeheader
 
xet*/

#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
//#ifndef __DECCXX
//#include <sysent.h>
//#endif
#include <unistd.h>
#include <iostream>
#include <strstream>
#include <sys/types.h>

#include "array/dyarray.h"
#include "array/hugearray.h"
#include "general/database_b.h"
#include "general/gtimers.h"

#include "general/stream.h"

const int DatabaseBase::CreateFileProt = 0660;

// statics
//
bool DatabaseBase::createFlag;

// -------------------------------------------------------------------

struct DatabaseFilenames
{
   char *kfName, *dfName, *cfName, *lfName;

   DatabaseFilenames( const char *basename )
   {
      int len = strlen( basename );

      kfName = new char[ len + 5 ];
      dfName = new char[ len + 5 ];
      cfName = new char[ len + 5 ];
      lfName = new char[ len + 5 ];

      strcpy( kfName, basename ); strcpy( dfName, basename );
      strcpy( cfName, basename ); strcpy( lfName, basename );

      strcat( kfName, ".key" ); strcat( dfName, ".dat" );
      strcat( cfName, ".ctl" ); strcat( lfName, ".lck" );
   }

   ~DatabaseFilenames()
   {
      delete [] kfName;
      delete [] dfName;
      delete [] cfName;
      delete [] lfName;
   }
};

// -------------------------------------------------------------------

void DatabaseBase::initialize( const char* filename, 
			       AccessMode accessMode, 
			       bool keyed )
{
   writeSync = accessMode & WriteSync;
   //readLock = accessMode & ReadLock;
   //writeLock = accessMode & WriteLock;
   allowWrite = accessMode & WriteAccess;
   cache = accessMode & Cache;

   _cache = 0;
   if ( cache )
      _cache = new HugeArray;

   error = NoError;
   kf = df = cf = -1; // lf = -1;  

   DatabaseFilenames names( filename );

   int openMode;
   if ( createFlag )
   {
      openMode = O_RDWR | O_CREAT;
   }
   else if ( allowWrite )
   {
      openMode = O_RDWR;
      if ( writeSync )
         openMode = openMode | O_SYNC;  // Equivalent to fsync after writes
   }
   else
      openMode = O_RDONLY;

   if (keyed) kf = open( names.kfName, openMode, CreateFileProt );
   df = open( names.dfName, openMode, CreateFileProt );
   cf = open( names.cfName, openMode, CreateFileProt );

   if (keyed) { if ( kf == -1 ) { setError( ErrNoKF ); } }
   else if ( df == -1 ) { setError( ErrNoDF ); }
   else if ( cf == -1 ) { setError( ErrNoCF ); }

   //cerr << "database init called\n";
   return;
}

DatabaseBase::DatabaseBase( const char* filename, AccessMode accessMode,
			     bool keyed )
    : basename( filename ),
      _dirtyRecords( 0 )
{
   //cerr << "database ctor called\n";
   initialize( filename, accessMode, keyed );
}

DatabaseBase::~DatabaseBase()
{

   if ( kf != -1 ) aasserten( close( kf ) == 0 );
   if ( df != -1 ) aasserten( close( df ) == 0 );
   if ( cf != -1 ) aasserten( close( cf ) == 0 );

   //_persistentLock = false;
   //if ( lf != -1 ) _unlock();

   //if (lfName) delete lfName;
   if( _cache ) delete _cache;
   //cerr << "database dtor called\n";
}

DatabaseBase::DatabaseBase()
    : _dirtyRecords( 0 )
{
   assert( createFlag );
}

bool DatabaseBase::dbExists( const char* basename, bool keyed )
{
   DatabaseFilenames names( basename );

   int openMode = O_RDWR;

   int kf = -1;
   if (keyed) kf = open( names.kfName, openMode, CreateFileProt );
   int df = open( names.dfName, openMode, CreateFileProt );
   int cf = open( names.cfName, openMode, CreateFileProt );
   int lf = open( names.lfName, openMode, CreateFileProt );

   aassert( lf == -1 );

   int sum = 0;
   if ( keyed ) if ( kf != -1 ) sum++;
   if ( df != -1 ) sum++;
   if ( cf != -1 ) sum++;

   if ( kf != -1 ) aassert( close( kf ) == 0 );
   if ( df != -1 ) aassert( close( df ) == 0 );
   if ( cf != -1 ) aassert( close( cf ) == 0 );

   if ( sum == 0 ) return false;

   if ( keyed ) { assert( sum == 3 ); } else { assert( sum == 2 ); }
   return true;
}

void DatabaseBase::dbDelete( const char *basename )
{
   DatabaseFilenames names( basename );
   unlink( names.kfName );
   unlink( names.dfName );
   unlink( names.cfName );
   unlink( names.lfName );
}

bool DatabaseBase::fileIsEmpty( int fd )
{
   off64_t r;

   r = lseek64( fd, 0, SEEK_END );
   if ( r == -1 ) { setError( FileError ); return false; }

   return r == 0;
}

void DatabaseBase::setError( Error e )
{
   error = e;
   if ( kf != -1 ) close( kf );
   if ( df != -1 ) close( df );
   if ( cf != -1 ) close( cf );

   //_persistentLock = false;
   //if ( lf != -1 ) _unlock();

   kf = df = cf = -1; //lf = -1;

   cerr << "Fatal database error: " << error 
#ifndef CRAY
<< ' ' << errno 
#endif
<< nl;
   aassert( false );
}


off64_t DatabaseBase::positionFile( int fd, off64_t pos, int whence )
{
   assert( fd != -1 );
   off64_t r = lseek64( fd, pos, whence );
   assert( r != -1 );
   return r;
}

#ifdef __xlC__
extern "C" int fsync(int);
#endif

void DatabaseBase::flush()
{
   if ( df != -1 ) aasserten( ::fsync( df ) == 0 );
   if ( kf != -1 ) aasserten( ::fsync( kf ) == 0 );
   if ( cf != -1 ) aasserten( ::fsync( cf ) == 0 );
}

/*
void DatabaseBase::checkIntegrity( bool rebuild )
{
   aassert(false);
}
*/

void DatabaseBase::recordDirty( int record )
{
   if ( record >= _dirtyRecords.size() )
       _dirtyRecords.extendTo( record + 1 );
   _dirtyRecords.set_bit( record );
}

// -------------------------------------------------------------------
// 	------------------------------------------------------
// -------------------------------------------------------------------

#ifdef OLD_LOCK_STUFF

const int DatabaseBase::LockRetryDelay = 1000000;
const int DatabaseBase::LockRetryCount = 20;
//
bool DatabaseBase::_persistentLock = false;

void DatabaseBase::lockSystem()
{
   assert( _persistentLock == false || AlreadyLocked == false);  //FIXME
   _lockSystem();
   _persistentLock = true;
}

void DatabaseBase::unlockSystem()
{
   assert( _persistentLock == true || NotLocked == false);  //FIXME
   _unlockSystem();
   _persistentLock = false;
}

bool DatabaseBase::locked()
{
   return _persistentLock;
}

void DatabaseBase::_lock() { assert( _persistentLock ); return; }

void DatabaseBase::_lockSystem()
{
   //if ( _persistentLock ) return;

   static char hostname[20];

   int cnt = 0;
   int lf;
   while ( true )
   {
      // old code
      lf = open( "_lock", O_RDWR | O_EXCL | O_CREAT, CreateFileProt );
      if ( lf != -1 ) break;

      //lf = open( "_locktemp", O_RDWR | O_CREAT | O_TRUNC, CreateFileProt );
      //if ( lf == -1 ) aasserten(false);

      //char debug[20];
      //if ( ! hostname[0] ) gethostname( hostname, 19 );
      //sprintf( debug, "%d %s\0", getpid(), hostname );
      //write( lf, debug, strlen(debug) );

      //close( lf );
      //int r = link( "_locktemp", "_lock" );
      //if ( r == 0 ) break; // locked ok

      // lock failed

      aasserten( errno == EEXIST );

//      cerr << "Database lock (" << "_lock" << ", pid=" << getpid() 
//           << ")... sleeping" << nl;

      usleep( LockRetryDelay );

      if ( ++cnt > LockRetryCount )
      { 
         cnt = 0;
         //cerr << "Warning: database lock count exceeded, my pid = " 
         //     << getpid() << nl;
         usleep( LockRetryDelay );
         usleep( LockRetryDelay );
      } 
   }

   char debug[30];
   if ( ! hostname[0] ) gethostname( hostname, 19 );
   snprintf( debug, 30, "%d %s", getpid(), hostname );
   debug[29] = '\0';
   write( lf, debug, strlen(debug) );

   close( lf );
}


void DatabaseBase::_unlock() { return; }

void DatabaseBase::_unlockSystem()
{
   //if ( _persistentLock ) return;

   //assert( lf != -1 );
   //asserten( close( lf ) == 0 );
   unlink( "_lock" );
   //lf = -1;
}

#endif
