

//--------------------------------------
// ------------- iosext.c --------------
//--------------------------------------

/*tex
 
\file{iosext.c}
\path{src/general}
\title{Extensions to iostream}
\classes{}
\makeheader
 
xet*/

#include "vacs/iosext.h"

#include <iostream>


void get( istream& s, int& u )
{
   char* p = (char*) &u;

   for ( int i=0; i < sizeof u; i++ )
      s.get( p[i] );
}


void put( ostream& s, int u )
{
   char* p = (char*) &u;

   for ( int i=0; i < sizeof u; i++ )
      s.put( p[i] );
}

