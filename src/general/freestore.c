

// -----------------------------------------
// ------------- freestore.c ---------------
// -----------------------------------------

/*tex
 
\file{freestore.c}
\path{src/general}
\title{Free store check utility functions}
\classes{}
\makeheader
 
xet*/

#include <fstream>

#include "stdtypes.h"
#include "general/str.h"

#ifdef FILEMODE
   #include "runinfo.h"
#endif 


void classStatus( char* text, char* className, long long newCount, long long delCount )
{

   Str s1( className );
   s1.padTo( 16 );

   Str s2( text );
   s2.padTo( 16 );

#ifdef FILEMODE
   Str fn( "memory." );
   fn.append( Str( int( RunInfo::runType() ) ) );
   ofstream o( (char*) fn, ios::app );
   o    << int( RunInfo::runType() ) 
#else
  ostream& o( cerr );
#endif



   o    << ' ' << s1 
        << ' ' << s2 
        << "  new: " << newCount 
        << ", delete: " << delCount  
        << ", difference: " << delCount - newCount
        << nl;

#ifdef FILEMODE
   o.close();
#endif
}
