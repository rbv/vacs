
// -------------------------------------------
// ----------------- gtimers.c ---------------
// -------------------------------------------

/*tex
 
\file{gtimers.c}
\path{src/general}
\title{Definitions for Gtimers class}
\classes{Str}
\makeheader
 
xet*/

#include <iostream>
#include "general/gtimers.h"

double Gtimers::cpuTimes[Gtimers::NTIMES] = {};
double Gtimers::wallTimes[Gtimers::NTIMES] = {};
Dtime Gtimers::dtimers[Gtimers::NTIMES];
double Gtimers::stimers[Gtimers::NTIMES];
const char *Gtimers::names[Gtimers::NTIMES];
long long Gtimers::overhead = 0;


void Gtimers::start( int i, const char *name )
{
    assert( i < NTIMES );
    dtimers[i].start();
    stimers[i] = System::seconds();
    overhead++;
    if ( name )
        names[i] = name;
}

void Gtimers::stop( int i )
{
    assert( i < NTIMES );
    wallTimes[i] += System::seconds() - stimers[i];
    cpuTimes[i] += dtimers[i].stop();
}

void Gtimers::print( ostream &os, int num_to_total )
{
    double csum = 0.0, wsum = 0.0;
    char buf[100];

    os << "Global timers:\n";
    os << "ID\tName\t\t\tCPU time\tWall time\n";
    for (int i = 0; i < NTIMES; i++)
    {
        if ( i < num_to_total )
        {
            csum += cpuTimes[i];
            wsum += wallTimes[i];
        }
        const char *name = names[i];
        if ( ! name ) name = "";
        if ( cpuTimes[i] + wallTimes[i] != 0.0 )
        {
            snprintf( buf, 100, "%d:\t%-20s\t%8.2f\t%f\n", i, name, cpuTimes[i], wallTimes[i]);
            os << buf;
        }

        if ( i == num_to_total - 1 )
        {
            snprintf( buf, 100, "\tTotal               \t%8.2f\t%f\n", csum, wsum);
            os << buf;
        }
    }

    long long count = overhead;
    double best = 999.0, t;
    for (int j = 0; j < 5; j++)
    {
        t = System::seconds();
        for (int i = 0; i < 10000; i++)
        {
            start( i % NTIMES );
            stop( i % NTIMES );
        }
        t = System::seconds() - t;
        if ( t < best )
            best = t;
    }

    snprintf( buf, 100, "\t(estimated overhead:\t\t\t%f)\n", (count * best / 10000));
    os << buf;
}
