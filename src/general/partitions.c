
/*
 * Enumeration of partitions class.
 */

/*tex
 
\file{partitions.c}
\path{src/general}
\title{Definitions for enumeration of set partitions}
\classes{Partition}
\makeheader
 
xet*/

#include "general/partitions.h"

/*
 * Set up next partition.
 */
bool Partition::getNextPart()
{
  bool done = false;

  int j; for (j = _n-1; Value[j] == Maximum[j] && j > 0; j--); 

  if (j > 0)
  {
    Value[j] += 1;

    for (int i=j+1; i<_n; i++)
    {
     Value[i] = 0; 		// reset minimums past j.

     if (Value[j] == Maximum[j]) Maximum[i] = Maximum[j] + 1;
     else 			 Maximum[i] = Maximum[j];
    }
  }
  else done = true;

  return !done;
}

   /*
    * Get partitions in an array of lists format
    * (input contents are ignored)
    */
void Partition::getPartition(DyArray< DyArray< partType > > &P) const
{
 P.resize(_n);
 partType maxPart = 0;

 for (int i=0; i<_n; i++)
  {
   P[i].resize(0);  // -1 comparisons aren't allowed

   if (Value[i] > maxPart) 
    {
//     P[maxPart].resize(1);
//     P[Value[i]][0]=i;
     maxPart++; // == Value[i]
     assert(Value[i] == maxPart);
//     cerr << "0) maxPart = " << maxPart << nl;
    }
   /* else */ P[Value[i]].append(i);

//   cerr << P;
//   cerr << "1) maxPart = " << maxPart << nl;
  }

// cerr << "2) maxPart = " << maxPart << nl;
 P.resize(maxPart+1);
  
}


