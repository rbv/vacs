/************************************************************************
 *
 * Bit vector class definitions.
 *
 ***********************************************************************/

/*tex
 
\file{bitvect.c}
\path{src/general}
\title{Definitions for class BitVector}
\classes{BitVector}
\makeheader
 
xet*/

#include <utility>

#include "general/bitvect.h"
#include "general/murmurhash2.h"


// creates a copy
//
BitVector::BitVector( const BitVector& B )
{
   _len = B._len;
   _numWords = B._numWords;
   _mask =  B._mask;

   bv = new bitword[ _numWords ];

   for ( int i = 0; i < _numWords; i++ ) bv[ i ] = B.bv[ i ];
}

/*
 * Bit vector assignment.
 */
BitVector& BitVector::operator=( const BitVector& B1 )
 {
  if ( B1.length() != _len )
      resize( B1.length() );

  for (int i=0; i<B1._numWords; i++) bv[i] = B1.bv[i];

  return *this;
 }

void BitVector::swap( BitVector& B1 )
{
    std::swap( _len, B1._len );
    std::swap( _numWords, B1._numWords );
    std::swap( _mask, B1._mask );
    std::swap( bv, B1.bv );
}

// passed number of bits, no initialization
//
BitVector::BitVector( int size )
{
   _len = 0;
   bv = NULL;
   resize( size );
}

/*
 * Resize a bit vector (afterwards, contents are garbage)
 */
void BitVector::resize( int size )
{
   //assert( size>0 );

#ifdef __GNUC__
   if ( _len>0 ) this->~BitVector();
#else
   if ( _len>0 ) BitVector::~BitVector();
#endif

   _len = size;

   _numWords = ( _len - 1 ) / bits_per_word + 1;

   // calc _mask (_mask is for clearing unused end bits)
   if ( offset( _len ) == 0 )
      _mask = bitword( -1 );
   else
      _mask = bitword( -1 ) >> ( bits_per_word - offset( _len ) );

   bv = new bitword[ _numWords ];
   bv[_numWords - 1] = bitword(0);
}
   
#ifdef NOINLINE
bool BitVector::get_bit( int pos ) const
{
   assert( pos >= 0 && pos < _len ); //error("Bad arg to bit_vector::get_bit");

   return ( ( bv[ index(pos) ] & ( bitword(1) << offset(pos) ) ) != 0 );
}
	
void BitVector::clr_bit( int pos )
{
   assert( pos >= 0 && pos < _len ); // error("Bad arg to bit_vector::clr_bit");

   bv[ index(pos) ] &= ( ~( bitword(1) << offset(pos) ) );
}

void BitVector::set_bit( int pos )
{
   assert( pos >= 0 && pos < _len ); // error("Bad arg to bit_vector::set_bit");

   bv[ index(pos) ] |= ( bitword(1) << offset(pos) );
}

void BitVector::set_all( void )
{
   for (int i = 0; i < _numWords; i++ ) bv[ i ] = bitword(-1);
   bv[ _numWords - 1] &= _mask;
}

void BitVector::clr_all( void )
{
    for (int i = 0; i < _numWords; i++ ) bv[ i ] = bitword(0);
}

void BitVector::complement( )
{
   for (int i = 0; i < _numWords; i++ ) bv[ i ] = ~bv[ i ];
   bv[ _numWords - 1] &= _mask;
}
#endif

unsigned long long BitVector::hash()
{
   return MurmurHash64A( (char *)bv, _numWords * sizeof(bitword), 0 );
}

/*
 * Put bit vector to output stream.
 */
ostream& operator<< ( ostream& os, const BitVector &B )
 {
   os << '[';
   for ( int i = 0; i < B.length(); i++ ) 
      os << (B.get_bit(i) ? '1' : '0');
   os << ']';
   return os; 
 }

bostream& operator<< ( bostream& os, const BitVector &B )
{
   os << B._len;
   for ( int i = 0; i < B._numWords; i++ )
      os << B.bv[i];
   return os;
}

bistream& operator>> ( bistream& is, BitVector &B )
{
   int len;
   is >> len;
   B.resize(len);
   for ( int i = 0; i < B._numWords; i++ )
      is >> B.bv[i];
   return is;
}


#if NOINLINE
// See if we have a clean vector.
//
bool BitVector::operator!()
 {
  for (int i=0; i<_numWords; i++) if (bv[i]) return false;
  return true;
 }
#endif
  
// tests equality of whole vectors
//
bool operator==( const BitVector& B1, const BitVector& B2 )
{
   // error("Mismatch of BitVector sizes in operator==");
   //
   //assert( B1._len == B2._len );
   if ( B1._len != B2._len )
      return false;

   for (int i = 0; i < B1._numWords; i++)
      if ( B1.bv[ i ] != B2.bv[ i ] ) return false;

   return true;
}


// tests inclusion of whole vectors
//
bool operator<( const BitVector& B1, const BitVector& B2 )
{
   assert( B1._len == B2._len ); 

   for (int i = 0; i < B1._numWords; i++ )
      if ( ( B1.bv[ i ] &  ~B2.bv[ i ] ) != 0 ) return false;

   return( true );
}


bool operator>( const BitVector& B1, const BitVector& B2 )
{
   return B2 < B1;
}


/*
 * Bitwise AND two bit vectors.
 */
BitVector operator&( const BitVector& B1, const BitVector& B2 )
 {
  assert( B1.length() == B2.length() ); 

  BitVector tmp(B1.length());

  for (int i=0; i<B1._numWords; i++) tmp.bv[i] = B1.bv[i] & B2.bv[i];

  return tmp;
 }

/*
 * Bitwise OR two bit vectors.
 */
BitVector operator|( const BitVector& B1, const BitVector& B2 )
 {
  assert( B1._len == B2._len ); 

  BitVector tmp(B1.length());

  for (int i=0; i<B1._numWords; i++) tmp.bv[i] = B1.bv[i] | B2.bv[i];

  return tmp;
 }

/*
 * Bitwise difference of two bit vectors. (B1 - B2)
 */
BitVector operator-( const BitVector& B1, const BitVector& B2 )
 {
  assert( B1.length() == B2.length() ); 

  BitVector tmp(B1.length());

  for (int i=0; i<B1._numWords; i++)
    tmp.bv[i] = B1.bv[i] & (B1.bv[i] ^ B2.bv[i]);

  return tmp;
 }

//-------------------------------------------------------------------


/*
 * Bitwise AND with another vector.
 */
BitVector& BitVector::operator&=( const BitVector& B2 )
 {
  assert( _len == B2.length() ); 

  for (int i=0; i<_numWords; i++) bv[i] &= B2.bv[i];

  return *this;
 }

/*
 * Bitwise OR with another vector.
 */
BitVector& BitVector::operator|=( const BitVector& B2 )
 {
  assert( _len == B2.length() ); 

  for (int i=0; i<_numWords; i++) bv[i] |= B2.bv[i];

  return *this;
 }

/*
 * Bitwise not/invert/complement (~).
 */
BitVector BitVector::operator~()
 {
  BitVector tmp(length());

  for (int i=0; i<_numWords; i++) tmp.bv[i] = ~bv[i];

  tmp.bv[ _numWords - 1] &= _mask;

  return tmp;
 }


void BitVector::andOf( const BitVector& B1, const BitVector& B2 )
 {
  assert( length() == B1.length() && length() == B2.length() );

  for (int i=0; i<_numWords; i++) bv[i] = B1.bv[i] & B2.bv[i];
 }

void BitVector::orOf( const BitVector& B1, const BitVector& B2 )
 {
  assert( length() == B1.length() && length() == B2.length() );

  for (int i=0; i<_numWords; i++) bv[i] = B1.bv[i] | B2.bv[i];
 }


//-------------------------------------------------------------------


HugeBitVector::HugeBitVector( int size )
{
    _blocks.setObjectSize( sizeof(bitword) );
    _len = 0;
    extendTo( size );
}

void HugeBitVector::recreate( int size )
{
    _blocks.resize( 0 );
    _len = 0;
    extendTo( size );
}

void HugeBitVector::extendTo( int size )
{
    assert( size >= _len );
    _numWords = (size + bits_per_word - 1) / bits_per_word;
    _blocks.resize( _numWords );
    int oldNumSums = _counts.size();
    int numSums = (size + sumsize - 1) / sumsize;
    _counts.resize( numSums );
    for (int i = oldNumSums; i < numSums; i++)
        _counts[i] = 0;
    _len = size;
}

void HugeBitVector::clearAll()
{
    for (int i = 0; i < _counts.size(); i++)
        if ( _counts[i] )
        {
            _counts[i] = 0;
            for (int j = i * sumsize; j < (i + 1) * sumsize && j < _len; j += bits_per_word)
            {
                getBitword( j ) = bitword(0);
            }
        }
}

bool HugeBitVector::next_set_bit( int &bitno )
{
    bitno++;
    int wordno = bitno / bits_per_word;
    int firstbit = bitno % bits_per_word;
    while ( wordno < _numWords )
    {
        bitword word = *(bitword *)_blocks.offset( wordno );
        if ( word )
            for (int i = firstbit; i < bits_per_word; i++)
                if ( word & (bitword(1) << i) )
                {
                     bitno = i + wordno * bits_per_word;
                     return true;
                }
        wordno++;
        firstbit = 0;
    }
    return false;
}
