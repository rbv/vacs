/* This class is unfinished
 */

#include "stats.h"
#include <stdio.h>
#include <iostreams>


Stats::Stats(int bins, double lbound, double rbound, bool _autogrow) :
    autogrow(_autogrow), total_x(0), total_x2(0), counts(0),
    histMin(lbound), histMax(ubound)
{
    histogram.insert( histogram.end(), 0, bins );
    binWidth = (rbound - lbound) / bins;
}

Stats::Stats(int bins, double binsize) :
    autogrow(true), total_x(0), total_x2(0), counts(0),
    histMin(0.), histMax(binsize)
{
    histogram.push_back( 0 );
    binWidth = binsize;
}

void Stats::addDatum(double val) {
    counts++;
    total_x += val;
    total_x2 += val * val;
    if ( autoGrow ) {
        if ( val > histMax ) {
            int nbins = ceil((val - histMin) / binWidth);
            histMax = histMin + binWidth * nbins;
            histogram.insert( histogram.end(), nbins - histogram.size(), 0 );
        }
    }
    int bin = (val - histMin) / binWidth;
    bin = min( max( bin, 0 ), histogram.size() - 1 );
    histogram[bin]++;
}

void Stats::printHist() {
    for (int j = 0; j < histogram.size(); j++)
    {
        if ( j )
            cerr << ",";
        fprintf( stderr, "%4d", histogram[j] );
    }
    cerr << nl;
}
