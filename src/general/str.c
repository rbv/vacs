
// -------------------------------------------
// ----------------- strclass.c --------------
// -------------------------------------------

/*tex
 
\file{str.c}
\path{src/general}
\title{Definitions for string class}
\classes{Str}
\makeheader
 
xet*/

#include <iostream>
#include <strstream>
#include <ctype.h>
#include <stdio.h>

#include "stdtypes.h"

#include "general/str.h"


void Str::unhook( int newLength )
{
   if ( p->n > 1 )
   {
      p->n--;
      p = new srep;
   }
   else
      delete[] p->s;

   if ( newLength )
      p->s = new char[ newLength ];
   else
      p->s = 0;
}


Str::Str()
{
   p = new srep;
   p->s = 0;
}


Str::Str( const Str& x )
{
   x.p->n++;
   p = x.p;
}


Str::Str( const char* s )
{
   p = new srep;
   p->s = new char[ strlen(s)+1 ];
   strcpy( p->s, s );
}


Str::Str( const char s )
{
   p = new srep;
   p->s = new char[ 2 ];
   (p->s)[0] = s;
   (p->s)[1] = '\0';
}


Str::Str( int i )
{
   p = new srep;
   p->s = new char[ 20 ];

   ostrstream o( p->s, 20 );
   o << i << '\0';
   assert( o );
}


Str::~Str()
{
   if ( --p->n == 0 )
   {
      delete [] p->s;
      delete p;
   }
}


Str& Str::operator=( const char* s )
{
   unhook( strlen(s)+1 );

   strcpy( p->s, s );
   return *this;
}


Str& Str::operator=( const Str& x )
{
   x.p->n++;  // protect against st = st
   if ( --p->n == 0 )
   {
      delete [] p->s;
      delete p;
   }
   p = x.p;
   return *this;
}


Str Str::withLength( int len )
{
   Str a;
   assert( a.p->s == 0 );

   a.p->s = new char[ len+1 ];
   a.p->s[0] = '\0';

   return a;
}

//--------------------------------------------------------------------

int Str::words( char sep ) const
{

   // count occurences sep->nonsep transitions
   if ( length() == 0 ) return 0;

   int c = 0;

   if ( p->s[0] != sep ) c++; // beginning : special case

   for ( int i=0; i<length()-1; i++ )
      if ( p->s[i] == sep &&  p->s[i+1] != sep ) c++;

   return c;
}


Str Str::word( int num, char sep ) const
{

   int l = length();
   int i = 0;

   assert( num > 0 && num <= words() );

   int c = 0;
   if ( p->s[0] != sep ) c++;

   if ( c != num )
      for ( i=0; i<l-1; i++ )
      {
         if ( c == num ) break;
         if ( p->s[i] == sep && p->s[i+1] != sep ) c++;
      }

   // i is start of the word we want
   //
   int j = i;
   while ( j < l && p->s[j] != sep ) j++;

   int len = j - i;

   assert( len > 0 );

   Str w = Str::withLength( len );

   for ( int k=0; k<len; k++ )
      w.p->s[k] = p->s[i+k];

   w.p->s[len] = '\0';
   
   return w;
}


Str& Str::lowerCase()
{
   int l = length();

   for ( int i=0; i<l; i++ )
      p->s[i] = tolower( p->s[i] );

   return *this;
}


Str& Str::append( const Str& s )
{
   return append( s.p->s );
}

Str& Str::append( const char c )
{
   char t[2];
   t[0] = c;
   t[1] = '\0';
   return append( t );
}

Str& Str::append( const char* s )
{
   char* t;

   if ( p->s )
   {
      t = new char[ strlen(p->s) + strlen(s) + 1 ];
      strcpy( t, p->s );
      strcpy( t + strlen(p->s), s );
   }
   else
   {
      t = new char[ strlen(s) + 1 ];
      strcpy( t, s );
   }

   unhook();

   p->s = t;

   return *this;
}


Str& Str::padTo( int length, char pad )
{
   // BUG!!  Beware p->s == 0!!
   assert( p->s );

   int newlength = max( (int) strlen(p->s), length );
   char* t = new char[ newlength + 1 ];
   strcpy( t, p->s );
   for ( int i = strlen(p->s); i < newlength; i++ )
      t[i] = pad;
   t[ newlength ] = '\0';

   unhook();

   p->s = t;

   return *this;
}


Str& Str::padLeft( int length, char pad )
{
   // BUG!!  Beware p->s == 0!!
   assert( p->s );

   int newlength = max( (int) strlen(p->s), length );
   int oldstart = newlength - strlen(p->s);
   char* t = new char[ newlength + 1 ];
   strcpy( t+oldstart, p->s );
   for ( int i = 0; i < oldstart; i++ )
      t[i] = pad;

   unhook();

   p->s = t;

   return *this;
}


bool Str::asInteger( int& result ) const
{
   if ( sscanf( p->s, "%d%*s", &result ) != 1 ) return false;

   return true;
}


int Str::index( char target ) const
{
   char* a = strchr( p->s, target );

   if (a) return a - p->s;
   else   return -1;
}

int Str::countChar( char c ) const
{
   int cnt = 0;
   int len = strlen( p->s );
   for ( int i=0; i<len; i++ )
      if ( p->s[i] == c ) cnt++;

   return cnt;
}

Str& operator+=( Str& a, const Str& b )
{
   return a.append( b );
}

Str& operator+=( Str& a, const char* b )
{
   return a.append( b );
}

Str operator+( const Str& a, const char* b )
{
   Str s( a );
   return s.append( b );
}

Str operator+( const Str& a, const Str& b )
{
   Str s( a );
   return s.append( b );
}

Str operator+( const Str& a, const char b )
{
   Str s( a );
   return s.append( b );
}


ostream& operator<<( ostream& o, const Str& x )
{
   return o << x.p->s;
}

istrstream* Str::stream() const
{
   istrstream* is = new istrstream( p->s, length() );
   return is;
}


