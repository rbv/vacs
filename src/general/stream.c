 
// -------------------------------------------
// ---------------- stream.c ----------------
// -------------------------------------------
 
/*tex
 
\file{stream.c}
\path{src/general}
\title{Code for binary stream classes}
\classes{bistream, bostream}
\makeheader
 
xet*/

#include <string.h>
#include "error.h"
#include "assert.h"
#include "general/stream.h"

// Clean-up for buffers
char* _oBufferCopy = 0;
char* _iBufferCopy = 0;
static void oDealloc() { delete [] _oBufferCopy; }
static void iDealloc() { delete [] _iBufferCopy; }

//----------------------------------------
//--------- lowlvl_fstream ---------------
//----------------------------------------

template<class base_stream, int _openmode>
_lowlvl_fstream<base_stream, _openmode>::_lowlvl_fstream(int fd, int _dummy, int bufsize)
    : buf(dup(fd), (ios::openmode)_openmode, std::max( bufsize, 1 ) ),
      base_stream(&buf)
{
}

template<class base_stream, int _openmode>
_lowlvl_fstream<base_stream, _openmode>::_lowlvl_fstream(const char *name, ios::openmode mode)
    : base_stream(&buf)
{
    if (!buf.open( name, mode ))
        this->setstate(ios_base::failbit);
}

// Instantiations
template class _lowlvl_fstream<istream, ios::in>;
template class _lowlvl_fstream<ostream, ios::out>;
template class _lowlvl_fstream<iostream, (int)ios::in | (int)ios::out>;

//----------------------------------------
//------------ bostream ------------------
//----------------------------------------

const int bostream::_BufferSize = 10000;
bool  bostream::_bufferInUse = 0;
char* bostream::_buffer = 0;

bostream::bostream()
{
   //assert( false ); // is anybody using this?
   f = 0;
   s = 0;
}

bostream::bostream( const char* name, ios::openmode mode )
{
   s = 0;
   f = new lowlvl_ofstream( name, mode );
}

bostream::bostream( MemFlag )
{

   if ( _bufferInUse ) { aassert(false); } // single user only!
   _bufferInUse = true;

   // one-time allocation
   //
   if ( !_buffer )
   {
       _buffer = new char[ _BufferSize ]; 
       _oBufferCopy = _buffer;
       callOnExit(oDealloc);
   }

   f = 0;
   s = new ostrstream( _buffer, _BufferSize );
}

void bostream::openOnFD( int fd, int bufsize )
{
   assert(!s);

   if (f)
      close();
   // Trickery: zero size internal buffer, but exact meaning of 2nd & 3rd differs
   f = new lowlvl_ofstream( fd, 0, bufsize );
}

void bostream::close() {
   assert(f);
   delete f;
   f = NULL;
}

void bostream::flush() {
   if (f)
      f->flush();
}

bostream::~bostream()
{
   if ( s ) 
   {
      assert( _bufferInUse );
      _bufferInUse = false;
   }
   if ( f ) delete f;
   if ( s ) delete s;
}

int bostream::fd()
{
   assert(f);
#ifdef __GNUC__
   return dynamic_cast<gnu_filebuf*>(f->rdbuf())->fd();
#else
   return f->rdbuf()->fd();
#endif
}

int bostream::tellp() const
   { return f ? f->tellp() : s->tellp(); }

int bostream::pcount() const
   { assert( s ); return s->pcount(); }

char* bostream::buffer()
   { assert( s ); return _buffer; }

bostream& bostream::write( const char* m, int n )
{
   f ? f->write(m,n) : s->write(m,n);
   return *this;
}

#ifdef CRAY
bostream& bostream::operator<<( ulong l )  
{ 
  char &c = (char&) l; char *p = &c;
  assert( p[0] == 0 && p[1] == 0 && p[2] == 0 && p[3] == 0 );
  write(p+4,4);
  return *this;
}
bostream& bostream::operator<<( ushort i ) 
{ 
  char &c = (char&) i; char *p = &c;
  assert( p[0] == 0 && p[1] == 0 && p[2] == 0 && 
 	  p[3] == 0 && p[4] == 0 && p[5] == 0 );
  write(p+6,2);
  return *this;
}
bostream& bostream::operator<<( int i )  { return *this << (ulong&) i; }
bostream& bostream::operator<<( uint i ) { return *this << (ulong&) i; }
bostream& bostream::operator<<( long l ) { return *this << (ulong&) l; }
bostream& bostream::operator<<( short i ) { return *this << (ushort&) i; }
bostream& bostream::operator<<( const float i ) { return *this << (ulong&) i; }
#else
bostream& bostream::operator<<( int i )  { write((char*)&i,sizeof(int)); return *this; }
bostream& bostream::operator<<( uint i ) { write((char*)&i,sizeof(int)); return *this; }
bostream& bostream::operator<<( long l )  { write((char*)&l,sizeof(long)); return *this; }
bostream& bostream::operator<<( ulong l ) { write((char*)&l,sizeof(long)); return *this; }
bostream& bostream::operator<<( short i ) { write((char*)&i,sizeof(short)); return *this; }
bostream& bostream::operator<<( ushort i ){ write((char*)&i,sizeof(short)); return *this; }
bostream& bostream::operator<<( const float i )  { write((char*)&i,sizeof(float)); return *this; }
#endif

bostream& bostream::operator<<( schar c )
{ (f) ? f->put(c) : s->put(c); return *this;}
#if defined(__GNUC__)
bostream& bostream::operator<<( bool b )  { return *this << b; }
#endif
#if defined(__DECCXX)
bostream& bostream::operator<<( bool b )  { return *this << (uchar&) b; }
#endif
#ifndef CRAY
bostream& bostream::operator<<( uchar c )
{ (f) ? f->put(c) : s->put(c); return *this;}
#endif

bostream& bostream::operator<<( const char *sr )
{
 int len = strlen(sr);
 write(sr,len);
 return *this;
}
//----------------------------------------
//------------ bistream ------------------
//----------------------------------------

const int bistream::_BufferSize = 10000;
bool  bistream::_bufferInUse = 0;
char* bistream::_buffer = 0;

bistream::bistream()
{
   nonblocking = false;
   s = 0;
   f = 0;
}

bistream::bistream( const char* name, ios::openmode mode )
{
   nonblocking = false;
   s = 0;
   f = new lowlvl_ifstream( name, mode );
}

bistream::bistream( int len, MemFlag )
{
   // User must call preAllocBuffer before this ctor.
   assert( _bufferInUse ); 

   nonblocking = false;
   f = 0;
   s = new istrstream( _buffer, len );
}

void bistream::openOnFD( int fd, int bufsize, bool nonblocking_ )
{
   assert(!s);
   nonblocking = nonblocking_;

   if (f)
      close();
   // Trickery: zero size internal buffer, but exact meaning of 2nd & 3rd differs
   f = new lowlvl_ifstream( fd, 0, bufsize );
}

void bistream::close() {
   assert(f);
   delete f;
   f = NULL;
   nonblocking = false;
}

bistream::~bistream()
{
    if ( f ) delete f;
    if ( s ) delete s;
   _bufferInUse = false;
}

char* bistream::preAllocBuffer()
{
   assert( _bufferInUse==false );
   _bufferInUse = true;

   // one-time allocation
   //
   if ( !_buffer ) 
   {
       _buffer = new char[ _BufferSize ];
       _iBufferCopy = _buffer;
       callOnExit(iDealloc);
   }

   return _buffer;
}

int bistream::fd()
{
   assert(f);
#ifdef __GNUC__
   return dynamic_cast<gnu_filebuf*>(f->rdbuf())->fd();
#else
   return f->rdbuf()->fd();
#endif
}

int bistream::tellg() const
   { return f ? f->tellg() : s->tellg(); }

char* bistream::buffer()
   { assert( s ); return _buffer; }

bistream& bistream::read( char* m, int n )
{
  if ( f )
  {
/*
    if ( nonblocking )
    {

      // Support for nonblocking files
      for (;;) {
        int amount = f->readsome(m, n);
        n -= amount;
        m += amount;
        if ( f->bad() )
          cerr << "bistream fail, " << n << " left to read\n";
        if ( n <= 0 || f->bad() )
          break;
        aassert( f->eof() );
        f->setstate( f->rdstate() & ~ios::eofbit );
        usleep( 1000 );
      }
      //cerr << "f state " << f->rdstate() << nl;
      aassert( !f->eof() );

    }
    else
*/
      f->read(m, n);
  }
  else
    s->read(m,n);
  return *this;
}

bistream& bistream::operator>>( schar& c )
{ (f) ? f->get(c) :  s->get(c); return *this; }

#if defined(__GNUC__)
bistream& bistream::operator>>( bool& b ) { return *this >> b; }
#endif
#if defined(__DECCXX)
bistream& bistream::operator>>( bool& b ) { return *this >> (uchar&) b; }
#endif

#if defined(__GNUC__)
bistream& bistream::operator>>( uchar& c )
{ (f) ? f->get((char&)c) : s->get((char&)c); return *this; }
#elif !defined(CRAY)
bistream& bistream::operator>>( uchar& c )
{ (f) ? f->get(c) : s->get(c); return *this; }
#endif

#ifdef CRAY
bistream& bistream::operator>>( uint& i )
{
  i=0; read(((char*)&i)+4,4); return *this; 
}
bistream& bistream::operator>>( ushort& i )
{
  i=0; read(((char*)&i)+6,2); return *this; 
}
bistream& bistream::operator>>( long& l )
	 { return this->operator>>( (uint&) l ); }
bistream& bistream::operator>>( ulong& l )
	 { return this->operator>>( (uint&) l ); }
bistream& bistream::operator>>( int& i )
	 { return this->operator>>( (uint&) i ); }
bistream& bistream::operator>>( short& i ) 
	 { return this->operator>>( (ushort&) i ); }
bistream& bistream::operator>>( float& f )
	 { return this->operator>>( (uint&) f ); }
#else
bistream& bistream::operator>>( int& i )  { read((char*)&i,sizeof(int)); return *this;}
bistream& bistream::operator>>( uint& i ) { read((char*)&i,sizeof(int)); return *this;}
bistream& bistream::operator>>( long& l )  { read((char*)&l,sizeof(long)); return *this;}
bistream& bistream::operator>>( ulong& l ) { read((char*)&l,sizeof(long)); return *this;}
bistream& bistream::operator>>( short& i ) { read((char*)&i,sizeof(short)); return *this;}
bistream& bistream::operator>>( ushort& i ){ read((char*)&i,sizeof(short)); return *this;}
bistream& bistream::operator>>( float& i ) { read((char*)&i,sizeof(float)); return *this;}
#endif

//----------------------------------------
//------------ astream ------------------
//----------------------------------------

// temp
#define Sep ' '

astream& eol( astream& s ) { return s.put(nl); }

astream& putLeft( astream& s ) { return s.put('{'); }
astream& putRight( astream& s ) { return s.put('}'); }

astream& getLeft( astream& s )
{
   char c;
   s.get( c );
   assert( c == '{' );
   return s;
}

astream& getRight( astream& s )
{
   char c;
   s.get( c );
   assert( c == '}' );
   return s;
}


astream& astream::put( const char a )
{ lowlvl_fstream::operator<<(a); return *this; }

astream& astream::get( char& a )
//{ lowlvl_fstream::operator>>(a); return *this; }
{ return *this >> a; }


astream& astream::operator<<( int a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

astream& astream::operator<<( schar a )
{ lowlvl_fstream::operator<<(int(a)); put(Sep); return *this; }

astream& astream::operator<<( long a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

astream& astream::operator<<( short a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

astream& astream::operator<<( uint a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

#ifndef CRAY
astream& astream::operator<<( uchar a )
{ lowlvl_fstream::operator<<(int(a)); put(Sep); return *this; }
#endif

astream& astream::operator<<( ulong a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

astream& astream::operator<<( ushort a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

astream& astream::operator<<( const char* a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

astream& astream::operator<<( const float a )
{ lowlvl_fstream::operator<<(a); put(Sep); return *this; }

//--------------------------

astream& astream::operator>>( int& a )
{ lowlvl_fstream::operator>>(a); return *this; }

astream& astream::operator>>( schar& b )
{ int a; lowlvl_fstream::operator>>(a); b = a; return *this; }

astream& astream::operator>>( long& a )
{ lowlvl_fstream::operator>>(a); return *this; }

astream& astream::operator>>( short& a )
{ lowlvl_fstream::operator>>(a); return *this; }

astream& astream::operator>>( uint& a )
{ lowlvl_fstream::operator>>(a); return *this; }

#ifndef CRAY
astream& astream::operator>>( uchar& b )
{ int a; lowlvl_fstream::operator>>(a); b = a; return *this; }
#endif

astream& astream::operator>>( ulong& a )
{ lowlvl_fstream::operator>>(a); return *this; }

astream& astream::operator>>( ushort& a )
{ lowlvl_fstream::operator>>(a); return *this; }

/*
astream& astream::operator>>( char*& a )
{ lowlvl_fstream::operator>>( a ); return *this; }
*/

astream& astream::operator>>( float& a )
{ lowlvl_fstream::operator>>( a ); return *this; }


//----------------------------------------------------------

#ifdef CRAY

bool bstream::readyForRead(int)
{
 assert(false); // have to write something else
 return true;
}

#else

// milliseconds to wait (defaults to 0)
//#define POLL_WAIT 1

#include <poll.h>
#ifndef __DECCXX
#ifndef __xlC__
#ifndef __GNUC__
extern "C" int poll( struct pollfd *fds, unsigned long nfd, int timeout);
#endif
#endif
#endif

bool bstream::readyForRead(int POLL_WAIT)
{
  struct pollfd pfd;

  pfd.fd = bistream::fd();
  // To portably trigger on eof have to set both these bits
  pfd.events = POLLIN | POLLHUP;

  int pollreturn = poll( &pfd, 1, POLL_WAIT );
  aasserten( pollreturn != -1 );
 
//if (pollreturn != 0)
//cerr << "pollreturn = " << pollreturn << " revents = " << pfd.revents << nl;

  if (pollreturn==1) return true;
  else return false; 
}

bool bstream::readyForWrite(int POLL_WAIT)
{
  struct pollfd pfd;

  pfd.fd = bostream::fd();
  pfd.events = POLLOUT;

  int pollreturn = poll( &pfd, 1, POLL_WAIT );
  aasserten( pollreturn != -1 );
 
  if (pollreturn==1) return true;
  else return false; 
}
#endif
