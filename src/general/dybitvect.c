
// ------------------------------------------
// ---------------- dybitvect.c ---------------
// ------------------------------------------

/*tex
 
\file{dybitvect.c}
\path{src/general}
\title{Definitions for dynamic bitvector class}
\classes{DyBitVector}
\makeheader
 
xet*/

#ifndef _dybitvect_
#define _dybitvect_

#include "general/dybitvect.h"

// ------------------------------------------

/*
 * Reallocate the vector if need, and copy old ops into new
 */
void DyBitVector::resize( int newBitLen )
{
   assert( newBitLen > 0 );

   if ( newBitLen == _len ) return;

   _len = newBitLen;
   _numWords = ( _len - 1 ) / bits_per_word + 1;

   /*
    * calculate _mask (_mask is for clearing unused end bits)
    */
   if ( offset( _len ) == 0 ) 
      _mask = bitword( -1 );
   else 
      _mask = bitword( -1 ) >> ( bits_per_word - offset( _len ) );

   /*
    * Now reallocate only if needed.
    */
   if ( _numWords <= _space ) return;

   _space = _numWords;
   //
   delete [] bv;
   bv = new bitword[_numWords];

}

#endif

