
// -----------------------------------------
// --------------- assert.c ----------------
// -----------------------------------------

/*tex
 
\file{assert.c}
\path{src/general}
\title{Assertion utility functions}
\classes{}
\makeheader
 
xet*/

// We'll do assertions our way to cut down on object size
// and compile time.


#include <errno.h>
//#ifndef __DECCXX
//#include <sysent.h>
//#endif
#include <stdlib.h>
#include <iostream>

using namespace std;

extern "C"
{
int assertFail( const char* ex, const char* file, int line )
{
  cerr << "Assertion failed: " << ex 
       << ", file " << file 
       << ", line " << line 
       << '\n';
  abort();
  return 0;
}
}


extern "C"
{
int assertenFail( const char* ex, const char* file, int line )
{
  cerr << "Assertion failed: " << ex 
       << ", file " << file 
       << ", line " << line 
#ifndef CRAY
       << ", errno " << errno 
#endif
       << '\n';
  abort();
  return 0;
}
}

void assertFailMesg( const char *mesg )
 {
  cerr << "Error: " << mesg << '\n';
  abort();
 }
