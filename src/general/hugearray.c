
#include <unistd.h>
#include "array/hugearray.h"

const int HugeArray::ObjectsPerChunkLog2 = 10;

HugeArray::HugeArray()
{
   _size = 0;
   _objectSize = 0;
   _lastChunkSize = 0;
}

HugeArray::~HugeArray()
{
   for ( int i=0; i<_chunks.size(); i++ )
      delete [] _chunks[i];
}

void HugeArray::setObjectSize( int sz )
{
   assert( _objectSize == 0 );
   _objectSize = sz;
   _size = 0;
   _lastChunkSize = 0;
}

void HugeArray::clearAll()
{
   for ( int i=0; i<_chunks.size(); i++ )
      delete [] _chunks[i];

   _size = 0;
   _lastChunkSize = 0;
   _chunks.resize(0);
}

void HugeArray::resize( int newSize )
{
   assert( _objectSize );

   if ( newSize == size() ) return;

   if ( newSize < size() ) clearAll();

   int newChunks = chunk( newSize ) + 1;
   int oldChunks = _chunks.size();

   //cerr << "old chunks: " << oldChunks << nl;
   //cerr << "new chunks: " << newChunks << nl;
   //cerr << "bytes per chunk: " << bytesPerChunk() << nl;

   _chunks.resize( newChunks );
   for ( int i=oldChunks; i<newChunks; i++ )
   {
       _chunks[i] = new char[ bytesPerChunk() ];
       memset( _chunks[i], 0, bytesPerChunk() );
   }
   
   _size = newSize;
   _lastChunkSize = pos( newSize );
}


char* HugeArray::offset( int index )
{
   assert( index >= 0 && index < _size );
   return  _chunks[ chunk(index) ] + ( pos(index) * _objectSize );
}


void HugeArray::loadFromFD( int fd )
{
   for ( int i=0; i<_chunks.size()-1; i++ )
   {
      int r = ::read( fd, _chunks[i], bytesPerChunk() );
      aasserten( r == bytesPerChunk() );
   }

   if ( _lastChunkSize )
   {
      int r = ::read( fd,
                _chunks[_chunks.size()-1], 
                _lastChunkSize*_objectSize );
      aasserten( r == _lastChunkSize * _objectSize );
   }

}

/*
void HugeArray::saveToFD( int )
{
   aassert(false);
}
*/
