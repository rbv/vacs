#include "general/memuse.h"

#if defined(__GNUC__) && !defined(__APPLE__)

#include <malloc.h>

int memory_used()
{
   struct mallinfo info;
   info = mallinfo();
   return info.uordblks;
}

#else

int memory_used()
{
   return 0;
}

#endif
