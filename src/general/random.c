 
// -------------------------------------------
// ----------------- random.c ----------------
// -------------------------------------------

/*tex
 
\file{random.c}
\path{src/general}
\title{Definitions of random number generator class}
\classes{RandomInteger}
\makeheader
 
xet*/

#include "general/random.h"

#include "general/system.h"


// a 32-bit primitive CA
//
const ulong RandomInteger::_mask = 0x39028f82;


RandomInteger::RandomInteger( int modulus )
{
   assert( modulus );
   _min = 0;
   _max = modulus - 1;
   initSeed();
}

RandomInteger::RandomInteger( int min, int max )
{
   _min = min;
   _max = max;
   assert( max >= min );
   initSeed();
}

void RandomInteger::initSeed()
{
   //System wall;
   //_seed = long(wall);  // Set from system time.

   struct timeval tp;
   gettimeofday( &tp, 0 );
   _seed = tp.tv_usec;

   if (_seed == 0) _seed++;

   //cerr << "seed " << _seed << nl;

   // scramble it a bit
   //
   for ( int i = 0; i < 5; i++ )
      (*this)();
}

void RandomInteger::setSeed( ulong value )
{
   assert( value != 0 );
   _seed = value;

   // scramble it a bit
   //
   for ( int i = 0; i < 5; i++ )
      (*this)();
}

int RandomInteger::operator()()
{
   // run the CA for one time step
   //
   _seed = (_seed << 1) ^ (_seed >> 1) ^ (_seed & _mask);

   // this is an internal error, meaning that the CA was entered incorrectly
   //
   assert( _seed != 0 );

   // return _seed, moved into user range
   //
   return _seed % (_max-_min+1) + _min;

}

