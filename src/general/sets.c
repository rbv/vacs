
/*tex
 
\file{sets.c}
\path{src/general}
\title{Definitions for subset generation functions}
\classes{}
\makeheader
 
xet*/

#include "general/sets.h"

Subsets::Subsets(int sub_n, int sub_k) 
    { 
#if 1
      assert(sub_k>=0 && sub_n>=0); 
      assert(sub_k<=sub_n);
#else
      if (sub_k<0 || sub_n<0) error("zero or less passed to Subsets");
      if (sub_k>sub_n) error("Subset size greater then set size.");
#endif

      n = sub_n ? sub_n : 1;
      k = sub_k ? sub_k : 1;

      a = new int[k];
      for (int i=0; i<k; i++) a[i]=i;

      a[k-1] = sub_k ? a[k-1]-1 : n-2;
    }

bool Subsets::nextset() 
    {
     for (int i=k-1; i>=0; i--)
      {
       if (a[i] < n-(k-i)) 
        {
         a[i]++;
         for (int j=i+1; j<k; j++) a[j]=a[j-1]+1;
#ifdef WATCH
 printf("Subset (%d of %d) :",k,n);
 for (j=0; j<k; j++) printf(" %d ",a[j]); putchar('\n');
#endif
         return true;
        }
      }
     return false;
    }

unsigned int Subsets::intBitVec()
{
  unsigned int bv = 0;

  for (int i=0; i<k; i++) bv |= (1<<a[i]);

  return bv;
}

/*************************************************************************/

Pairs::Pairs(int set_n) 
    { 
#if 1
      assert(set_n>0);
#else
      if (set_n<=0) error("zero or less passed to Pairs");
#endif
      n=set_n; n2=n/2; 
      v1 = new int[n2];
      v2 = new int[n2];
      used = new int[n];

      int i; for (i=0; i<n2; i++) 
       { 
        v1[i]=2*i;
        used[2*i]=1;
        v2[i]=2*i+1; 
        used[2*i+1]=1;
       }
      v2[n2-1]--;  // For first matching.
      used[v2[n2-1]]++;

      for (i=2*(n2-1)+1; i<n; i++) used[i]=0;

    }

bool Pairs::nextpair() 
  {
   int step=-1;
   /*
    * Search for pair to exchange.
    */
   for (int i=n2-1; i>=0 && i<n2; i+=step)
    {
#ifdef WATCH
 printf("      (%d of %d) %d %d :",n2,n,i,step);
 for (int j=0; j<n2; j++) printf(" (%d %d) ",v1[j],v2[j]); putchar('\n');
 for (j=0; j<n; j++) printf(" %d",used[j]); putchar('\n');
#endif

     if (step<=0)
      {
       for (int j=v2[i]+1; j<n; j++)
	if (!used[j])
	 {
	  used[v2[i]]--;
	  v2[i]=j;
	  used[j]++;
	  step=+1;
	  goto Try_next; // To find another pair.
	 }
       used[v1[i]]--;
       used[v2[i]]--;
      }
     else // going forward
      {
       for (int j=0; j<n; j++)
	if (!used[j])
	 {
	  v1[i]=j;
	  v2[i]=j;
	  used[v1[i]]+=2;
	  step=0;
	  goto Try_next;  // To pair up v1.
	 }
      }

     step=-1;

Try_next: ;
    } // i

#ifdef WATCH
 printf("Pairs (%d of %d) %d %d :",n2,n,i,step);
 for (int j=0; j<n2; j++) printf(" (%d %d) ",v1[j],v2[j]); putchar('\n');
#endif

   if (step>0) return true; else return false;

  }

