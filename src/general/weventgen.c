
// -------------------------------------------
// --------------- weventgen.c ---------------
// -------------------------------------------

/*tex
 
\file{weventgen.c}
\path{src/general}
\title{Definitions of weighted event generator functions}
\classes{WeightedEventGenerator}
\makeheader
 
xet*/

#include "general/weventgen.h"


WeightedEventGenerator::WeightedEventGenerator( const Array< int >& A )
   : _w( A ), _r(1)
{
   recompute();
}


WeightedEventGenerator::WeightedEventGenerator( int size )
   : _w( size ), _r(1)
{
   _w.fill( 100 );
   recompute();
}


void WeightedEventGenerator::recompute()
{
   int sum = 0;
   for ( int i=0; i<_w.size(); i++ )
   {
      assert( _w[i] >= 0 );
      sum += _w[i];
   }

   _r.modulus( sum );
}


int WeightedEventGenerator::operator()()
{
   // get a random int
   //
   int r = _r();

   int s = 0;

   for( int i=0; ; i++ )
   {
      s += _w[i];
      if ( s > r ) return i;
   }

   aassert( false );
   return 0; // never get here, but keeps the compiler happy
}

