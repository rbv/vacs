
// -----------------------------------------
// ---------------- error.c ----------------
// -----------------------------------------

/*tex
 
\file{error.c}
\path{src/general}
\title{Program termination functions}
\classes{}
\makeheader
 
xet*/

#include "error.h"

#define MAX_EXITS 30

static ExitFunction exitFunctions[MAX_EXITS];
static int numExits = 0;

void Exit( int level )
{
   for ( int i=0; i<numExits; i++ ) exitFunctions[i]();
   exit( level );
}

void callOnExit( ExitFunction f )
{
   numExits++;
   if ( numExits > MAX_EXITS ) fatalError( "num exit functions exceeded" );
   exitFunctions[ numExits-1 ] = f;
}
