
// -------------------------------------------
// --------------- database_f.c --------------
// -------------------------------------------
 
/*tex
 
\file{database_f.c}
\path{src/general}
\title{Definition of fixed record size database class}
\classes{FRSDatabase}
\makeheader
 
xet*/

#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
//#ifndef __DECCXX
//#include <sysent.h>
//#endif
#include <unistd.h>

#include "array/dyarray.h"
#include "array/hugearray.h"
#include "general/database_f.h"

#include "general/stream.h"


FRSDatabase::FRSDatabase( const char* filename, AccessMode accessMode )
   : DatabaseBase( filename, accessMode, false )
{
   readControlFile();
   dataFileSeek = -1;
   if ( cache )
   {
      _cache->setObjectSize( ctl.recordSize );
      loadCache();
   }
}

FRSDatabase::FRSDatabase()
{
   dataFileSeek = -1;
}

FRSDatabase::~FRSDatabase()
{
  //flush();
}


void FRSDatabase::clearDatabase()
{
   ctl.numKeys = 0;
   writeControlFile();
   ftruncate( df, 0 );
   if ( cache ) { _cache->clearAll(); loadCache(); }
}


void FRSDatabase::dbCreate( const char* filename, int recordSize )
{
   createFlag = true;
   FRSDatabase db;
   db.initialize( filename, WriteAccess, false );
   db.ctl.format = DB_FORMAT;
   db.ctl.recordSize = recordSize;
   db.ctl.numKeys = 0;
   db.writeControlFile();
   createFlag = false;
}

void FRSDatabase::loadCache()
{
   positionDataFile( 0 );
   //cerr << "frs cache resized (load): " << ctl.numKeys << nl;
   _cache->resize( ctl.numKeys );
   _cache->loadFromFD( df );
}

void FRSDatabase::sync()
{
   if ( ! cache ) return;

   assert( !allowWrite );
   readControlFile();
   loadCache();
}

void FRSDatabase::truncate( int size )
{
   flush();
   aassert( size <= ctl.numKeys );
   aassert( size >= 0 );
   ctl.numKeys = size;
   writeControlFile();
   int ret = ftruncate( df, (off64_t)size * ctl.recordSize );
   if (ret)
   {
      perror("FRSDatabase::truncate()");
      aassert(false);
   }
   if ( cache ) { _cache->clearAll(); loadCache(); }
}

void FRSDatabase::flush()
{
   if (cache)
   {
      int numflushed = 0;
      int key = -1;
      while ( _dirtyRecords.next_set_bit( key ) )
      {
         numflushed++;
         assert( key < ctl.numKeys );
         positionDataFile( key );
         assert ( !error );
         int r = write( df, _cache->offset( key ), ctl.recordSize );
         if ( r != ctl.recordSize ) { setError( ErrNoDF ); return; }
      }

      _dirtyRecords.clearAll();
      //cerr << "flush " << (char*)basename << " " << numflushed << "/" << ctl.numKeys << nl;
   }

   writeControlFile();
   DatabaseBase::flush();
}

/*
void FRSDatabase::sync( int )
{
   // this method is extremely dangerous!
   aassert(false);
}
*/

void FRSDatabase::positionDataFile( int key )
{
   assert( df != -1 );
   if ( key == dataFileSeek ) return;
   off64_t r = lseek64( df, (off64_t)key * ctl.recordSize, SEEK_SET );
   assert( r != -1 );
   dataFileSeek = key;
}

void FRSDatabase::readControlFile()
{
   assert( cf != -1 );
   fsync( cf );
   off64_t r;
   r = lseek64( cf, 0, SEEK_SET );
   if ( r == -1 ) { setError( ErrNoCF ); return; }
   r = read( cf, (char*) &ctl, sizeof( ctl ) );
   if ( r !=sizeof(ctl) ) { setError( ErrNoCF ); return; }

   aassert( ctl.format == DB_FORMAT );

   //cerr << "frs ctl read " << ctl.numKeys << nl;

}


void FRSDatabase::writeControlFile()
{
   assert( cf != -1 );
   off64_t r;
   r = lseek64( cf, 0, SEEK_SET );
   if ( r == -1 ) { setError( ErrNoCF ); return; }
   r = write( cf, (char*) &ctl, sizeof( ctl ) );
   //cout << "wrote " << r << " bytes to ctl file\n";
   if ( r != sizeof( ctl ) ) {
      perror("writeControlFile");
      setError( ErrNoCF );
      return;
   }

   //cerr << "frs ctl written " << ctl.numKeys << nl;
}


bool FRSDatabase::exists( int key )
{
   if ( !cache ) readControlFile();
   return (key >= 0 ) && (key < ctl.numKeys);
}


int FRSDatabase::numberOfKeys()
{
   if ( !cache ) readControlFile();
   return ctl.numKeys;
}



char* FRSDatabase::getCache( int key )
{
   assert( cache );
   if ( key < 0 ) { setError( BadKey ); return 0; }
   if ( key > ctl.numKeys ) { setError( BadKey ); return 0; }

   //cerr << "cache get: " << key << ' ' << (void*) _cache->offset(key) << nl;

   return _cache->offset( key );
}


void FRSDatabase::get( int key, char* buffer, bool bypassCache )
{

   if ( key < 0 ) { setError( BadKey ); return; }

   if ( cache && !bypassCache )
   {
      if ( key >= ctl.numKeys ) { setError( BadKey ); return; }

      //cerr << "cache get: "<< key << ' ' << (void*) _cache->offset(key) << nl;
      memcpy( buffer, _cache->offset( key ), ctl.recordSize );
      return;
   }

   int r;

   if ( !cache ) readControlFile();
 
   if ( key >= ctl.numKeys ) { setError( BadKey ); return; }
 
   positionDataFile( key );
   r = read( df, buffer, ctl.recordSize );

   //cerr << "frs get " << ctl.recordSize << ' ' << key << ' ' << r;

   if ( r != ctl.recordSize ) { setError( ErrNoDF ); return; }
   dataFileSeek = key + 1;
 
}


void FRSDatabase::put( int key, char* buffer )
{
   if ( ! cache )
   {
      if ( key > ctl.numKeys ) assert( false );

      positionDataFile( key );
      assert ( !error );
      int r = write( df, buffer, ctl.recordSize );
      if ( r != ctl.recordSize ) { setError( ErrNoDF ); return; }
      dataFileSeek = key + 1;

      if ( key == ctl.numKeys )
      {
         ctl.numKeys++;
         writeControlFile();
      }

      return;
   }

   if ( ! allowWrite ) { setError( NoWrite ); return; }

   if ( key < 0 ) { setError( BadKey ); return; }
 
   // can only grow by 1 record at a time
   if ( key > ctl.numKeys ) assert( false );

   if ( key == ctl.numKeys )
   {
      _cache->resize( key+1 );
      ctl.numKeys++;
   }
 
   // write data to cache
   // make sure that buffer isn't the cache data already
   //
   if( _cache->offset( key ) != buffer )
      memcpy( _cache->offset( key ), buffer, ctl.recordSize );

   recordDirty( key );

 
}


#if 0
void FRSDatabase::put( int key, char* buffer )
{

   if ( ! allowWrite ) { setError( NoWrite ); return; }

   assert( cache ); // code getting tightened
 
   if ( key < 0 ) { setError( BadKey ); return; }
 
   int r;
   bool writeCtlFile = false;

   if ( key >= ctl.numKeys ) // grow the data file
   {
      char* tmp = new char[ ctl.recordSize ];
      memset( tmp, 0, ctl.recordSize );

      writeCtlFile = true;

      positionDataFile( 0, SEEK_END );

      for ( i=ctl.numKeys; i<=key; i++ )
      {
         int r = write( df, tmp, ctl.recordSize );
         if ( r != ctl.recordSize ) { setError( ErrNoKF ); return; }
      }
      delete [] tmp;
      ctl.numKeys = key + 1;
      dataFileSeek = key + 1;

      if ( cache )
      {
         //cerr << "frs cache resized : " << ctl.numKeys << nl;
         _cache->resize( ctl.numKeys );
      }
   }
 
   positionDataFile( key );
   if ( error ) return;
 
   r = write( df, buffer, ctl.recordSize );
   if ( r != ctl.recordSize ) { setError( ErrNoDF ); return; }
   dataFileSeek = key + 1;
   if ( writeCtlFile )
   {
      writeControlFile();
      if ( error ) return;
   }

   // write data to cache
   // make sure that buffer isn't the cache data already
   //
   if ( cache )
   {
      //cerr <<"cache put: " << key << ' ' << (void*) _cache->offset(key) << nl;
      if( _cache->offset( key ) != buffer )
         memcpy( _cache->offset( key ), buffer, ctl.recordSize );
   }

}
#endif

/*
void FRSDatabase::checkIntegrity( bool rebuild )
{
}
*/

void FRSDatabase::put( int key, bostream& s )
{
   assert( s.pcount() == ctl.recordSize );
   if ( s.type() != bostream::Memory ) { setError( BadStreamType ); return; }
   put( key, s.buffer() );
}

bistream* FRSDatabase::get( int key )
{
   char* buff = bistream::preAllocBuffer();
   get( key, buff );
   if ( error ) return 0;
   bistream* bi = new bistream( ctl.recordSize, bistream::MemFlag() );
   return bi;
}

