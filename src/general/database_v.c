
// -------------------------------------------
// --------------- database_v.c --------------
// -------------------------------------------
 
/*tex
 
\file{database_v.c}
\path{src/general}
\title{Definition of variable record size database class}
\classes{VRSDatabase}
\makeheader
 
xet*/

#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
//#ifndef __DECCXX
//#include <sysent.h>
//#endif
#include <unistd.h>
#include "general/database_v.h"

#include "general/stream.h"


VRSDatabase::VRSDatabase( const char* filename, AccessMode accessMode )
   : DatabaseBase( filename, accessMode, true )
{
   assert( !cache );
   //checkIntegrity();
   readControlFile();
   dataFileSeek = -1;
}


VRSDatabase::VRSDatabase()
{
   dataFileSeek = -1;
}

VRSDatabase::~VRSDatabase()
{
}

void VRSDatabase::sync()
{
   //if ( ! cache ) return;
   assert( !allowWrite );
   readControlFile();
}

void VRSDatabase::flush()
{
   DatabaseBase::flush();
}


void VRSDatabase::dbCreate( const char* filename )
{
   createFlag = true;
   VRSDatabase db;
   db.initialize( filename, WriteAccess, true );
   db.ctl.format = DB_FORMAT;
   db.ctl.numKeys = 0;
   db.writeControlFile();
   createFlag = false;
}


inline void VRSDatabase::positionKeyFile( int key, int whence )
{
   positionFile( kf, (off64_t)key * sizeof( Key ), whence );
}
 
inline off64_t VRSDatabase::positionDataFile( off64_t pos, int whence )
{
   if ( whence == SEEK_SET && pos == dataFileSeek )
      return pos;
   return dataFileSeek = positionFile( df, pos, whence );
}


void VRSDatabase::clearDatabase()
{
   ctl.numKeys = 0;
   writeControlFile();
   ftruncate( df, 0 );
   ftruncate( kf, 0 );
   dataFileSeek = -1;
   //if ( cache ) loadCache();
   //cerr << "vrs cleared" << nl;
}


void VRSDatabase::readControlFile()
{
   //FileManager::lockRequest( LSysR );

   assert( cf != -1 );
   off64_t r;
   r = lseek64( cf, 0, SEEK_SET );
   if ( r == -1 ) { setError( ErrNoCF ); return; }
   r = read( cf, (char*) &ctl, sizeof( ctl ) );

   aassert( ctl.format == DB_FORMAT );

   //FileManager::lockRequest( USysR );

   if ( r !=sizeof(ctl) ) { setError( ErrNoCF ); return; }
//cerr << "vrs ctl read " << ctl.numKeys << nl;

}

void VRSDatabase::writeControlFile()
{
   //FileManager::lockRequest( LSysW );

   assert( allowWrite );
   assert( cf != -1 );
   off64_t r;

   r = lseek64( cf, 0, SEEK_SET );
   if ( r == -1 ) { setError( ErrNoCF ); return; }

   r = write( cf, (char*) &ctl, sizeof( ctl ) );

   if ( r != sizeof( ctl ) ) { setError( ErrNoCF ); return; }

   //if ( writeSync ) fsync( cf );
   //cerr << "vrs ctl written " << ctl.numKeys << nl;
   //assert( ctl.numKeys != 15 );

   //FileManager::lockRequest( USysW );
}

/*
bool VRSDatabase::exists( int key )
{
   aassert( false );
   return false;
}
*/

int VRSDatabase::get( int key, char* buffer, int len )
{
   if ( key < 0 ) { setError( BadKey ); return 0; }

   Key k;
   int r;

   //if ( readLock ) _lock();
   //FileManager::lockRequest( LSysR );

   if ( ! cache ) readControlFile();

   if ( key >= ctl.numKeys ) { setError( BadKey ); return 0; }

   positionKeyFile( key );
   r = read( kf, (char*) &k, sizeof( k ) );
   if ( r != sizeof(k) ) { setError( ErrNoKF ); return 0; }

   if ( k.pos == -1 )
   {
      //if ( readLock ) _unlock();
      //FileManager::lockRequest( USysR );
      return 0; // { setError( NoEntry ); return 0; }
   }

   if ( k.len > len ) { setError( BufferTooSmall ); return 0; }

   positionDataFile( k.pos );
   r = read( df, buffer, k.len );
   if ( r != k.len ) { setError( ErrNoDF ); return 0; }
   dataFileSeek = k.pos + k.len;

   //if ( readLock ) _unlock();
   //FileManager::lockRequest( USysR );

   return k.len;
}


void VRSDatabase::put( int key, char* buffer, int len )
{
   if ( ! allowWrite ) { setError( NoWrite ); return; }

   if ( len <= 0 ) { setError( BadPutLen ); return; }
   if ( key < 0 ) { setError( BadKey ); return; }

   //if ( writeLock ) _lock();
   //FileManager::lockRequest( LSysW );

   Key k;
   memset( &k, 0, sizeof(k) );
   int r;
   bool writeCtlFile = false;

   if ( key >= ctl.numKeys ) // grow the key file
   { 
      k.pos = -1;
      k.len = 0;
      writeCtlFile = true;
      positionKeyFile( 0, SEEK_END );
      if ( error ) return;
      for ( int i=ctl.numKeys; i<key+1; i++ )
      {
         r = write( kf, (char*) &k, sizeof( k ) );
         if ( r != sizeof(k) ) { setError( ErrNoKF ); return; }
      }
      ctl.numKeys = key+1;
      k.pos = positionDataFile( 0, SEEK_END );
   } 
   else
   {
      positionKeyFile( key );
      if ( error ) return;
      r = read( kf, (char*) &k, sizeof(k) );
      if ( r != sizeof(k) ) { setError( ErrNoKF ); return; }

      if ( len > k.len )
         k.pos = positionDataFile( 0, SEEK_END );
      else
         k.pos = positionDataFile( k.pos );
   }
   if ( error ) return;

   k.len = len;

   positionKeyFile( key );
   if ( error ) return;

   r = write( kf, (char*) &k, sizeof(k) );
   if ( r != sizeof(k) ) { setError( ErrNoKF ); return; }

   r = write( df, buffer, len );
   if ( r != len ) { setError( ErrNoDF ); return; }
   dataFileSeek = k.pos + k.len;

   if ( writeCtlFile )
   {
      writeControlFile();
      if ( error ) return;
   }

   //if ( writeLock ) _unlock();
   //FileManager::lockRequest( USysW );
}


void VRSDatabase::del( int key )
{
   if ( ! allowWrite ) { setError( NoWrite ); return; }

   if ( key < 0 ) { setError( BadKey ); return; }

   //if ( writeLock ) _lock();
   //FileManager::lockRequest( LSysW );

   Key k;
   int r;

   positionKeyFile( key );
   if ( error ) return;
   r = read( kf, (char*) &k, sizeof(k) );
   if ( r != sizeof(k) ) { setError( ErrNoKF ); return; }

   k.pos = -1;
   k.len = 0;

   positionKeyFile( key );
   if ( error ) return;
   r = write( kf, (char*) &k, sizeof(k) );
   if ( r != sizeof(k) ) { setError( ErrNoKF ); return; }

   //if ( writeLock ) _unlock();
   //FileManager::lockRequest( USysW );
}

void VRSDatabase::truncate( int size )
{
   if ( size == 0 ) {
      clearDatabase();
      return;
   }

   flush();
   aassert( size <= ctl.numKeys );
   aassert( size >= 0 );
   ctl.numKeys = size;
   writeControlFile();

   // Skip truncation of the data file as this isn't right:
   // keys that are kept might point to the end of the data
   // file, because if a record is overwritten with a longer piece
   // of data, it goes to the end.
/*
   Key k;
   memset( &k, 0, sizeof(k) );
   int r;
   positionKeyFile( size );
   if ( error ) aassert( false );
   r = read( kf, (char*) &k, sizeof(k) );
   if ( r != sizeof(k) ) { setError( ErrNoKF ); aassert( false ); }
   int ret = ftruncate( kf, k.pos );
   if (ret)
   {
      perror("VRSDatabase::truncate() data file");
      aassert(false);
   }
*/

   int ret = ftruncate( kf, (off64_t)size * sizeof(Key) );
   if (ret)
   {
      perror("VRSDatabase::truncate() key file");
      aassert(false);
   }
   dataFileSeek = -1;
   // Don't handle the cache, because del() doesn't; don't know to.
}

int VRSDatabase::lastUsedKey()
{
   return ctl.numKeys - 1;
}

/*
void VRSDatabase::compress()
{
   // Not yet implemnted
   aassert( false );
}
*/

/*
void VRSDatabase::checkIntegrity( bool )
{
aassert(false);
}
*/

void VRSDatabase::put( int key, bostream& s )
{
   if ( s.type() != bostream::Memory ) { setError( BadStreamType ); return; }
#if 0
cerr << "size = " << s.pcount() << nl;
for ( int i=0; i<s.pcount(); i++ ) cerr << int(s.buffer()[i]) << ' ';
cerr << nl;
#endif
   put( key, s.buffer(), s.pcount() );
}

bistream* VRSDatabase::get( int key )
{
   char* buff = bistream::preAllocBuffer();
   int size = get( key, buff, bistream::bufferSize() );
   if ( error || size == 0 ) { bistream::releaseBuffer(); return 0; }
   bistream* bi = new bistream( size, bistream::MemFlag() );
   return bi;
}

