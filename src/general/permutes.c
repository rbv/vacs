
/*
 * Enumeration of permutations class.
 */

/*tex
 
\file{permutes.c}
\path{src/general}
\title{Definitions for enumeration of permutations class}
\classes{PermEnum}
\makeheader
 
xet*/

#include "general/permutes.h"

// temp debug function
//
#define prt() { for (int i=0; i<size(); i++)  \
                 printf("%d ", get(i)); printf("\n"); }

/*************************************************************************/

static permHash fact(int n)
 {
  permHash p =1;
  for (int i=2; i<=n; i++) p *= i;
  return p;
 }

/*
 * Copy ctor (added by kc)
 */
PermEnum::PermEnum( const PermEnum& p ) : PermArray(p)
{

#ifdef SN_RANKING
   _hash = p._hash;
   _maxHash = p._maxHash;
#endif

}

#ifdef SN_RANKING

/*
 * Constructor given S_n desired.
 */
PermEnum::PermEnum(int n) : PermArray(n)
 {
   _hash=0;
   unrank();			// sets array
   _maxHash = fact(size()) - 1; 
 }

/*
 * Constructor given a permutation array as intial value.
 */
PermEnum::PermEnum(PermArray P) : PermArray(P)
 {

#ifdef DEBUG
  /*
   * Make sure we have a valid permutation specification.
   */
#endif

   rank();			// sets _hash
   _maxHash = fact(size()); 
 }

/*
 * Internal routines.
 */

PermEnum::rank()
 {
  int i,j;
  permHash factorial; 
  permNum count;

  for ( i=factorial=1, _hash=count=0; 
        i < size();
        i++, _hash += count*factorial, factorial *= i )

        for ( int j=0; j<i; j++) 
          if ( _A_(j) > _A_(i) ) count++;
 }

PermEnum::unrank()
 {
  int i,j;
  permHash prevHash = _hash;
  permNum count;

  for ( i=0; i< size(); i++) _A_(i)=0;

  for ( i=size()-1; i>=0; i--)
   {
     \\ undo rank() stuff
   }
  }

/*************************************************************************/
#else
/*************************************************************************/

/*
 * Constructor given S_n desired.
 */
PermEnum::PermEnum(int n) : PermArray(n)
 {
   for (int i=0; i<n; i++) PermArray::put(i,i);
 }

/*
 * Some sequencing procedure that enumerates all permutations.
 */
bool PermEnum::next()
 {
  int n=size();

//  cerr << "start of next() ";
//  prt() ;

  for (int i=1; i < n; i++) 
   {
     if ( _A_(i) > _A_(i-1) )
      {
       int j; for (j=0; _A_(j) > _A_(i); j++);
//  cerr << "i j " << i << " " << j << nl;
//  prt() ;
       PermArray::swap(i,j);
//  cerr << " after swap " << nl;
//  prt() ;
       PermArray::reverse(i-1);
//  cerr << " after reverse " << nl;
//  prt() ;
       return true;
      }
   }

  return false;
 }


// next by skipping at position k
//
bool PermEnum::next( int k ) // true if decrement is possible at pos k
{

   int n = CAST(*this,PermArray).size();

   assert(k>=0); assert(k<n);

   //
   // fill in 0...k-1 (brutally!) and  perform decrement on k,...,n
   //

   Array<bool> used(n);
   used.fill(false);

   //used[i] = true whenever i appears in perm[k]...perm[n]
 
   int i; for (i=k+1; i<n; i++ ) used[_A_(i)] = true;

   int nextK=_A_(k);
   //
   while (used[--nextK] == true )
   {
   if (nextK==0) return false; // avoiding < 0 subscript.
   }

   _A_(k)=nextK;
   used[nextK]=true;

   int c;
   for ( i=0, c=0; i<k; i++ )
   {
      while( used[c] ) c++;
      _A_(i) = c++;
   }

   return true;
}

/*
 * Return a unique number between 0 and n!-1 for the permuation.
 */
permHash PermEnum::rank()
 {
  int i,j;
  permHash _hash=0, factorial=1;
  permNum count=0;

  for ( i=1; i < size();
        i++, _hash += count*factorial, factorial *= i )

        for ( j=0; j<i; j++) if ( _A_(j) > _A_(i) ) count++;

   return _hash;
 }

#endif
