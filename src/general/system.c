
// -----------------------------------------
// ---------------- system.c ----------------
// -----------------------------------------

/*tex
 
\file{system.c}
\path{src/general}
\title{System clocks, timers, etc.}
\classes{}
\makeheader
 
xet*/

#include "general/system.h"

// statics
//
struct timeval System::_tv; 
//
struct timezone System::_tz;
//
struct tm* System::_tm;
