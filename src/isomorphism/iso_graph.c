/***************************************************************************
* Graph isomorphism (operator==)
****************************************************************************/

/*tex
 
\file{iso_graph.c}
\path{src/isomorphism}
\title{Graph isomorphism}
\classes{}
\makeheader
 
xet*/

#include "graph/graph.h"
#include "graph/algorithm/distance_ga.h"
#include "isomorphism/iso_graph.h"
#include "isomorphism/schmidt_druffel/vertpart.h"

/*
 * Backtracking algorithm to test isomorphism using Vert_partition.
 */
bool Graph::operator==(const Graph &G) const
 {
  vertNum order = this->order();

  if (order != G.order()) return false;

  DistanceMatrix D1(order);  dist_mat( *this, D1 );
  DistanceMatrix D2(order);  dist_mat( G, D2 );

  const VertPartition class1( order, D1 );
  const VertPartition class2( order, D2 );

  if (class1.partitions != class2.partitions) return false;

#ifdef WATCH2
  cerr << "Passed inital conditions of operator==\n";
#endif

  VertArray class_map(order);

  if ( !(find_class_map(class1, class2, class_map)) ) return false;

  VertSearch S1(order);
  VertSearch S2(order);

  build_search(class1, class2, class_map, S1, S2);

  return G_iso( D1, D2, S1, S2, /* order,*/ 0, 0 );
 }

