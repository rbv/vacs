#include <bliss/graph.hh>

#include "general/gtimers.h"
#include "graph/bgraph.h"
#include "bpg/bpg.h"
#include "search/graph_db.h"
#include "isomorphism/bliss/isodb.h"
#include "isomorphism/schmidt_druffel/isodb.h"

namespace Bliss_Iso
{

#define SHS bliss::Graph
// shs_f and shs_fs fail on an assertion!
const static bliss::Graph::SplittingHeuristic splitting_heuristic = SHS::shs_fl;


void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary )
{
    IsoStats::addInit();
    I1.parseLen( G.length() );

    Gtimers::start( 25, "blissgraph" );

    bliss::Graph blissgraph( G.vertices() );

    // adapted from BGraph::BGraph(const BPG &B, vertNum extra)

    unsigned char bndry[16] = {0};
    int bLen = G.length();
    int nextVert = 0;

    for (int i = 0; i < bLen; i++)
    {
        Operator op = G[i];

        if (op.isVertexOp())
        {
            vertNum vert = op.vertex();
            if ( !ignoreBoundary && G.isBoundaryVertex(i) )
            {
                blissgraph.change_color( nextVert, 1 );
            }

            bndry[vert] = nextVert++;
        }
        else if (op.isEdgeOp())
        {
            vertNum vert1 = op.vertex1();
            vertNum vert2 = op.vertex2();

            blissgraph.add_edge( bndry[vert1], bndry[vert2] );
        }
    }

    Gtimers::stop( 25 );
    Gtimers::start( 26, "BLISS" );
    bliss::Stats stats;  // apparently not optional
    blissgraph.set_splitting_heuristic( splitting_heuristic );
    const unsigned int *c_order = blissgraph.canonical_form( stats, NULL, NULL );
    Gtimers::stop( 26 );
    assert(c_order);
    Gtimers::start( 27, "Canonform" );

    // Don't use this: have to permute to canonical form before taking hash,
    // and bliss's hash function is quite flawed
    //unsigned int hash = blissgraph.get_hash();

    BGraph bgraph(G);

#ifndef NDEBUG
    // check valid automorphism

    assert( blissgraph.get_nof_vertices() == G.order() );

/*
    cerr << nl << bgraph << "perm ";
    for (int i = 0; i < G.order(); i++)
        cerr << c_order[i] << " ";
    cerr << nl << "inv ";
*/

    // check c_order is a permutation
    BitVector used( G.order() );
    for (int i = 0; i < G.order(); i++)
        used.set_bit( c_order[i] );
    for (int i = 0; i < G.order(); i++)
        assert( used.get_bit( i ) );
#endif

    // Find inverse permutation
    int inverse[bgraph.order()];
    for (int i = 0; i < bgraph.order(); i++)
        inverse[c_order[i]] = i;

/*
    for (int i = 0; i < G.order(); i++)
        cerr << inverse[i] << " ";
    cerr << nl << nl;
*/

    I2.canonform.resize( (bgraph.order() + 1) * bgraph.order() );
    I2.canonform.clr_all();

    int bitno = 0, edges = 0;
    for (int i1 = 0; i1 < bgraph.order(); i1++ )
    {
        unsigned int v1 = inverse[i1];
        if ( !ignoreBoundary && bgraph.isBoundary( v1 ) )
            I2.canonform.set_bit( bitno );
        bitno++;
        // Append neighbours bitvector
        BitVector neighbours = bgraph.neighbors(v1);
        for (int i2 = 0; i2 < neighbours.size(); i2++ )
        {
            if ( neighbours.get_bit( inverse[i2] ) )
            {
                I2.canonform.set_bit( bitno );
                edges++;
            }
            bitno++;
        }
    }
    assert( bitno == I2.canonform.size() );
    assert( edges == 2 * bgraph.size() );
    int edges2 = 0;
    for (int i = 0; i < I2.canonform.size(); i++)
        if (I2.canonform.get_bit(i))
            edges2++;
    assert(edges2 == edges + (ignoreBoundary ? 0 : G.boundarySize()));

    Gtimers::stop( 27 );
    Gtimers::start( 28, "hash" );

    I1.hash = I2.canonform.hash();
    //cout << "h " << I1.hash << nl;

    Gtimers::stop( 28 );

#ifdef CHECK_ISO
    I2.graph = G;
    I2.sd_check.init( G, ignoreBoundary ? noBoundary : freeBoundary );
#endif
}

void printIsoAlgStats( ostream &os )
{
    os << "Using splitting heuristic '";
    switch(splitting_heuristic) {
    case SHS::shs_f:   os << "first"; break;
    case SHS::shs_fs:  os << "first_smallest"; break;
    case SHS::shs_fl:  os << "first_largest"; break;
    case SHS::shs_fm:  os << "first_max_neighbours"; break;
    case SHS::shs_fsm: os << "first_smallest_max_neighbours"; break;
    case SHS::shs_flm: os << "first_largest_max_neighbours"; break;
    }
    os << "'\n";
}

//-----------------------------------------------------------------------

bool IsoDBItem1::operator==( const IsoDBItem1& a ) const
{
    IsoStats::addIso1Check();
#ifdef CHECK_ISO
    return true; // do full test
#endif
    return hash == a.hash;
}

//-----------------------------------------------------------------------

bool IsoDBItem2::operator==( const IsoDBItem2& a ) const
{
    IsoStats::addIso2Check();
    bool ret = canonform == a.canonform;

#ifdef CHECK_ISO
    bool correct = sd_check == a.sd_check;
    if ( correct != ret )
    {
        BGraph b1(graph), b2(a.graph);
        cerr << "Iso error, bliss=" << ret << " sd=" << correct << ". G1:\n";
        cerr << graph << *this << nl;
        cerr << b1;
        cerr << "\nG2:\n";
        cerr << a.graph << a << nl;
        cerr << b2;
        aassert( false );
    }
#endif

    if (ret)
        IsoStats::addHit();
    return ret;
}

ostream& operator<<( ostream &os, const IsoDBItem2& rep )
{
    os << "Canon form: " << rep.canonform << nl;
}

bostream& operator<<( bostream& s, const IsoDBItem2& rep )
{
#ifdef CHECK_ISO
   s << rep.sd_check << rep.graph;
#endif
   return s << rep.canonform;
}

bistream& operator>>( bistream& s, IsoDBItem2& rep )
{
#ifdef CHECK_ISO
   s >> rep.sd_check >> rep.graph;
#endif
   return s >> rep.canonform;
}

}  // end namespace
