{
    int edges = 0;

    //cerr << bgraph;

    for (int v1 = 0; v1 < bgraph.order(); v1++ )
    {
        BitVector neighbours = bgraph.neighbors(v1);
        for (int v2 = 0; v2 < neighbours.size(); v2++ )
        {
            if ( neighbours.get_bit(v2) )
            {
                edges++;
                assert( bgraph.isEdge( c_order[v1], c_order[v2] ) );
                assert( bgraph.isBoundary( v1 ) == bgraph.isBoundary( c_order[v1] ) );
                assert( bgraph.isBoundary( v2 ) == bgraph.isBoundary( c_order[v2] ) );
            }
        }
    }
    assert( edges == G.edges() * 2 ) ;
}
