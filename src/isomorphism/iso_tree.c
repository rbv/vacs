
/*tex

\file{iso_tree.c}
\path{src/isomorphism}
\title{Source for Tree Isomorphism}
\classes{}
\makeheader

xet*/


#include "array/sortarray.h"

#include "graph/tree.h"
#include "graph/forest.h"


//------------------------------------------------------------------

/*
 * Tree's canonical rank sequence.
 */
TreeRank Tree::rank( vertNum root ) const
 {
//cerr << "Rank of tree at vertex " << root << " in \n";
//cerr << *this << nl;

  TreeRank R( _order );

  R[0]=_order;

  if ( _order==1 ) return R;

  Forest *F = rmNode( root );

  vertNum numTrees = degree(root);
  //
  SortableArray<TreeRank> subranks( numTrees );

  /*
   * Compute rank for all branches away from "root".
   */
  F->start();
  //
  int i; for (i=0; i < numTrees; i++)
   {
    subranks[i] = F->next()->rank();		// Desired root should be 0.
   }

  subranks.sortDown();

  /*
   * Finally, assign rank from subranks.
   */
  int rIndex=1;
  //
  for (i=0; i < numTrees; i++)
    for (int j=0; j < subranks[i].size(); j++)
     {
      R[rIndex++] = subranks[i][j];
     }

#ifndef AUTODEL
  F->deleteAll();
#endif

  delete F;

//cerr << "Rank equals: " << R << nl;

  return R;
 }

//------------------------------------------------------------------

/*
 * Isomorphism test.
 */
bool Tree::operator==(const Tree &T) const
 {
  if ( _order != T.order() ) return false;

  vertNum c1, c2, Tc1, Tc2;		// prospective center(s)

  bool haveCenter = center( c1, c2 );
  bool havTCenter = T.center( Tc1, Tc2 );

  if ( haveCenter != havTCenter ) return false;

  if ( haveCenter )			// Reduce to rooted tree isomorphism.
   {
     if ( degree(c1) != T.degree(Tc1) ) return false;  // fast check.

     return ( rank(c1) == T.rank(Tc1) );
   }
  else					// Make a artificial center.
   {
     Tree T1(*this);
     Tree T2(T);

     T1.subdivide( c1, c2 );
     T2.subdivide( Tc1, Tc2 );

     if ( degree(c1)+degree(c2) != T.degree(Tc1)+T.degree(Tc2) ) return false; 
    
     return ( T1.rank(T1.order()-1) == T2.rank(T2.order()-1) );
   }

 }

/*
 * Isomorphism reformulated for class Forest's "removeEqualities"
 */
bool eqTest( const Tree &T1, const Tree &T2 )
 {
  return T1 == T2;
 }

