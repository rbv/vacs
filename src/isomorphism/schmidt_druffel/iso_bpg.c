
/*
 * Isomorphism for BPG (bounded path graphs)
 */

/*tex
 
\file{iso_bpg.c}
\path{src/isomorphism}
\title{Bounded pathwidth graph isomorphism}
\classes{}
\makeheader
 
xet*/

#include "bpg/bpg.h"
#include "bpg/bpgutil.h"
#include "isomorphism/iso_graph.h"

// for debug
//
//#include "iostream"
 

bool isomorphic( const BPG& G1, const BPG& G2, IsomorphismBoundaryType bType )
{
   vertNum order = G1.vertices();
   
   // Check number vertices/edges.
   //
   if ( order != G2.vertices() ) return false;
   //
   if ( G1.edges() != G2.edges() ) return false;

   // Compute the distance matrices.
   //
   DistanceMatrix D1( order );
   DistanceMatrix D2( order );
   G1.distanceMatrix( D1 );
   G2.distanceMatrix( D2 );

#ifdef WATCH2
  cerr << "Distance 1: " << *D1 << nl;
  cerr << "Distance 2: " << *D2 << nl;
#endif
   
   // More stuff to cache.
   //
   VertPartition class1( G1.vertices(), D1 );
   VertPartition class2( G2.vertices(), D2 );

#ifdef WATCH2
  cerr << "Class 1: ";
  class1->print();
  cerr << "Class 2: ";
  class2->print();
#endif

   return isomorphic( D1, D2, class1, class2, bType );

}


bool isomorphic( 
   const DistanceMatrix& D1, 
   const DistanceMatrix& D2, 
   const VertPartition& class1, 
   const VertPartition& class2,
   IsomorphismBoundaryType bType
)
{
   //cerr << "***" << D1.dim() << ' ' << D2.dim() << nl;

   int order = D1.dim();

   if ( order != D2.dim() ) return false;

   if (class1.partitions != class2.partitions) return false;

   /*
    * Try to find a first trial mapping.
    */
   VertArray class_map(order);

   if ( !(find_class_map(class1, class2, class_map)) ) return false;

   VertSearch S1(order);
   VertSearch S2(order);

   build_search(class1, class2, class_map, S1, S2);

   int bSize = Operator::boundarySize();

   /*
    * Off to the backtracking isomorphism algorithm.
    */
   switch (bType)
    {
     case noBoundary:    return G_iso( D1, D2, S1, S2, 0, 0 );

     case freeBoundary:  return G_iso( D1, D2, S1, S2, 0, bSize );

     case fixedBoundary:  return G_iso( D1, D2, S1, S2, 0, bSize );
    }

  aassert(false);
  return false;
}

