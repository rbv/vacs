//
// isomorphim class member and utility functions.
//

/*tex
 
\file{isomorphism.c}
\path{src/isomorphism}
\title{Isomorphim class member and utility functions}
\classes{}
\makeheader
 
xet*/

#include <stdio.h>
//#define WATCH2
#include "isomorphism/iso_graph.h"

/*
 * vert_search comparison order.  (ADD to SortableArray class!)
 */
template<>
int compare( const VertSearchItem *s1, 
                    const VertSearchItem *s2 )
 {
   if ( s1 -> class_cnt > s2 -> class_cnt)
      return 1;
   if ( s1 -> class_cnt < s2 -> class_cnt)
      return -1;

   if ( s1 -> class_num > s2 -> class_num)
      return 1;
   if ( s1 -> class_num < s2 -> class_num)
      return -1;

   if ( s1 -> map_to < s2 -> map_to)
      return 1;
/* 
 * Next #if can probably be set to 1 for efficiency reasons but not
 * sure if it confuses system qsort
 */
#if 0 
   else
#else
   if ( s1 -> map_to > s2 -> map_to)
#endif
      return -1;

   return 0;
 }

/*************************************************************************/

/*TEX
 
need to tex the following...

 *
 * IDEA of _GRAPH ISOMORPHISM
 *
 * 1. Sort search structures.
 *
 * 2. Set current_class = class1[level]
 *    Set map_vertex = vert1[level]
 *
 * 3. For all vert2 in current_class DO:
 *    a) map vert1 to vert2
 *    b) build new class vectors (search structures) by
 *       refining old class vectors with distance matrices.
 *       Note: all assigned vertices have lower class numbers.
 *    c) if class counts consistent CALL answer = G_iso(level+1)
 *                  else try next vert2
 *    d) if answer = true then return true
 *                  else try next vert2
 *
 * 4. No more matchable vert2(s) so return false. 
 *
XET*/

/*
 * Isomorphism function based on distance matrices.
 */
bool G_iso( const DistanceMatrix &D1,         // Distance Matrices.
            const DistanceMatrix &D2,
            VertSearch &S1,                   // Unified search structures.
            VertSearch &S2,
//          vertNum order,                    // Order of graphs.
            vertNum level,                    // Count of mapped vertices.
            int boundary /* = 0 */ )          // Common boundary size.
 {
  vertNum order = S1.size();

  bool fixedBoundaryFlag = (boundary < 0) ? boundary = -boundary, true : false;
  bool answer = false;

#ifdef WATCH2
  if (level == order)
   {
    cerr << "Isomorphism: " << nl;
    for (int i=0; i<order; i++) cerr << " " << (int) S1[i].map_to ;
    cerr << '\n';
 
    return true;
   }
#else
  if (level == order) return true;
#endif

  //qsort(S1, order, sizeof(vert_search), search_order);
  //qsort(S2, order, sizeof(vert_search), search_order);
  S1.sort();
  S2.sort();

#ifdef WATCH2
  cerr << "Sorted at Level " << (int) level << ":\n";
 for (int I=0; I<order; I++)
  {
   S1[I].print(I);
   S2[I].print(I);
  }
#endif

  vertNum current_class = S1[level].class_num;
  vertNum map_from = S1[level].vertex;
  vertNum map_to;
  vertNum map_to_index = level;

  VertSearch S1a(order);		// New work space for next call.
  VertSearch S2a(order);

  VertArray C1a(order);			// Modified classes.
  VertArray C2a(order);

  VertArray K1a(order);			// Class count vectors.
  VertArray K2a(order);


  bool okay_flag = true;	// dumb cfront 3.0 needs help.

  while ( map_to_index < order && 
          S2[map_to_index].class_num == current_class )
   {

     map_to = S2[map_to_index].vertex;

     /*
      * Check boundary mapping condition.
      */
     if (boundary)
      if ( map_from < boundary && map_to >= boundary ||
           (fixedBoundaryFlag && map_from != map_to) )
        {
	 // goto Next_try;	 I want to do this! Dorky C++ errors.
          map_to_index++;
          continue;
        }

     /*
      * Compose distance matrices' column and row to class vectors.
      */
 int i; for (i=0; i<order; i++) C1a[i] = C2a[i] = order;

     /*
      * Make class assigments.
      */
     vertNum CLASS = 0;
	  
 for (i=0; i<order; i++)
  if (C1a[S1[i].vertex] == order)
   {
    C1a[S1[i].vertex] = CLASS; 

    int j; for (j=i+1; j<order; j++)			// Same graph.
	 if (C1a[S1[j].vertex] == order)
            if (S1[j].class_num == S1[i].class_num &&
                D1(S1[j].vertex,map_from) == D1(S1[i].vertex,map_from) &&
                D1(map_from,S1[j].vertex) == D1(map_from,S1[i].vertex))
		      		C1a[S1[j].vertex] = CLASS;

    for (j=0; j<order; j++)				// Second graph.
	 if (C2a[S2[j].vertex] == order)
            if (S2[j].class_num == S1[i].class_num &&
                D2(S2[j].vertex,map_to) == D1(S1[i].vertex,map_from) &&
                D2(map_to,S2[j].vertex) == D1(map_from,S1[i].vertex))
		      		C2a[S2[j].vertex] = CLASS;

    CLASS++;
   }

     /*
      * Make sure we have the same set of classes for both graphs.
      */
     for (i=0; i < order; i++) if (C2a[i] == order) 
      {
       okay_flag=false;
       break;
       //goto Next_try;
      }

if (okay_flag)  // #1
 {

     /*
      * Compute class count vectors and check for consistancy.
      */
     for (i=0; i < CLASS; i++) K1a[i] = K2a[i] = 0;

     for (i=0; i < order; i++) { K1a[C1a[i]]++; K2a[C2a[i]]++; }

     for (i=0; i < CLASS; i++) if (K1a[i] != K2a[i])
      {
       okay_flag=false;
       break;
       //goto Next_try;
      }

if (okay_flag)  // #2
 {

     /*
      * Adjust search structure for next recursion.
      */
     for (i=0; i<order; i++)
	  {
            int j = S1[i].vertex;
            S1a[j].class_cnt = K1a[C1a[j]];
            S1a[j].class_num = C1a[j];
	    S1a[j].vertex = j;
            S1a[j].map_to = S1[i].map_to;

            j = S2[i].vertex;
            S2a[j].class_cnt = K2a[C2a[j]];
            S2a[j].class_num = C2a[j];
	    S2a[j].vertex = j;
            S2a[j].map_to = S2[i].map_to;
	  }
     
     S1a[map_from].map_to = map_to;		// Add current vertex map.
     S2a[map_to].map_to = map_from;

#ifdef WATCH2
 cerr << "Send to Level " <<  (int) level + 1 << ":\n";
 for (i=0; i<order; i++)
  {
   S1a[i].print(i);
   S2a[i].print(i);
  }
#endif

     answer = G_iso( D1, D2, S1a, S2a, /* order, */  level + 1, 
		     fixedBoundaryFlag ? -boundary : boundary );

     if (answer == true) break;

//Next_try:

}}// okay_flags

     okay_flag = true;

     map_to_index++;

   }

  return answer;
 }

