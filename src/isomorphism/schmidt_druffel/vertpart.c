
/*
 * This file contains the vertex partioning class that aids isomorphism.
 */

/*tex
 
\file{vertpart.c}
\path{src/isomorphism}
\title{Vertex partioning class that aids isomorphism}
\classes{VertPartition}
\makeheader
 

\begin{desc}
  The VertPartion class vector for later isomorphism tests.
  Source: D. C. Schmidt and L. E. Druffel
          J. of ACM, Vol 23, page 444, (1976)
\end{desc}

xet*/

#include <stdio.h>
#include <stdlib.h>

#include "general/stream.h"
#include "isomorphism/iso_graph.h"

VertPartition::VertPartition()
{
}

VertPartition::VertPartition(vertNum order, const DistanceMatrix &D)
             : class_num(order), class_rep(order), class_cnt(order)
{
   init( order, D );
}

void VertPartition::init(vertNum order, const DistanceMatrix &D)
 {
   class_num.resize(order);
   class_rep.setDim(order);
   class_cnt.resize(order);

   //class_num.fill(0);
   class_rep.fill(0);
   class_cnt.fill(0);

  /*
   * Initialize data structure.
   */
  class_num.fill(order);
  VertMatrix X(order);
  X.fill(0);

  /*
   * Not needed clean-up!  (helps in debugging printouts)
   */
#ifdef WATCH2
  class_rep.fill(0); 
  class_cnt.fill(0); 
#endif

  /*
   * Build characteristic matrices.
   */
  for (int i=0; i<order; i++)
    for (int m=0; m<order-1; m++)
      for (int k=0; k<order; k++) if (D(i,k) == m+1) X(i,m)++;


  /*
   * Make class assigments.
   */
  vertNum CLASS = 0;
  
  for (int i=0; i<order; i++)
    if (class_num[i] == order)
     {
      class_num[i] = CLASS; 
      class_cnt[CLASS] = 1;

      for (int j=0; j<order-1; j++) class_rep(CLASS,j) = X(i,j);

      for (int j=i+1; j<order; j++)
        if (class_num[j] == order)
         {
          int k;
          for (k=0; k < order-1 && X(i,k) == X(j,k); k++)
              ;
          if (k == order-1)
            {
             class_num[j] = CLASS;
             class_cnt[CLASS]++;
            }
         }

      CLASS++;
     }

#ifdef WATCH2
  cerr << "Class vector (CLASS " << (int) CLASS << " ) ";
  for (int i=0; i<order; i++) cerr << " " << (int) class_num[i];
  cerr << '\n';
#endif
  
  partitions = CLASS;
 }

bostream& operator<<( bostream& s, const VertPartition& vp )
{
   return s << vp.partitions << vp.class_num << vp.class_rep << vp.class_cnt;
}

bistream& operator>>( bistream& s, VertPartition& vp )
{
   return s >> vp.partitions >> vp.class_num >> vp.class_rep >> vp.class_cnt;
}
