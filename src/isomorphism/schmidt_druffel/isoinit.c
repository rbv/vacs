
//
// Initialization routines for graph isomorphism.
//

/*tex
 
\file{isoinit.c}
\path{src/isomorphism}
\title{Initialization routines for graph isomorphism}
\classes{}
\makeheader
 
xet*/

/*
 * A routine that tries to find a plausible vertex map for possible
 * graph isomorpism.
 * The graph class vectors must have been defined beforehand.
 */

#include "isomorphism/iso_graph.h"

/*
 * Initialize (unify) start up vectors if possible.
 */
bool find_class_map( const VertPartition &class1, 
                     const VertPartition &class2,
                     VertArray &class_map )
 {

  /*
   * A fast check.  (This should have been done before this call.
   */
  if (class1.partitions != class2.partitions) return false;

  vertNum order = class_map.size();

  for (int i=0; i < class1.partitions; i++)
   {
    for (int j=0; j < class2.partitions; j++)
     {
      int k=0;
      while (k < order-1) 
        if (class1.class_rep(i,k) == class2.class_rep(j,k) ) k++;
        else k = order;

      if (k == order) continue /* j */;	// No representative match.

      if (class1.class_cnt[i] != class2.class_cnt[j])
         {
#ifdef WATCH2
 cerr << "count differ " << i << ' ' << j << ' ' << k << nl;
#endif         
          return false;
         }
      else
         {
    	  class_map[j] = i;
          goto Next_i;
         }

// Next_j: ;
      } // for j

    return false;			// No match for class representative.

Next_i: ;
   } // for i

  return true;
 }


/*
 *  A routine to build the VertSearch structures needed for 
 *  graph isomorphisms.  
 *  Two class vectors and an intial vertex map must have been
 *  defined beforehand.
 */
void build_search( VertPartition const &class1,
                   VertPartition const &class2,
		   VertArray const &class_map,
                   VertSearch &S1, VertSearch &S2)
 {
  vertNum order = class_map.size();

  for (int i = 0; i<order; i++)
   {
    S1[i].class_cnt = class1.class_cnt[class1.class_num[i]];
    S1[i].class_num = class1.class_num[i];
    S1[i].vertex = i;
    S1[i].map_to = order;		// Vacant map.
    
    S2[i].class_cnt = class2.class_cnt[class2.class_num[i]];
    S2[i].class_num = class_map[class2.class_num[i]];
    S2[i].vertex = i;
    S2[i].map_to = order;

#ifdef WATCH2
   S1[i].print(i);
   S2[i].print(i);
#endif
   }
 }

