
#include "search/graph_db.h"
#include "bpg/bpggrph.h"
#include "isomorphism/schmidt_druffel/isodb.h"
#include "general/murmurhash2.h"

namespace Schmidt_Druffel
{

void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary )
{
    IsomorphismBoundaryType bType = ignoreBoundary ? noBoundary : freeBoundary;
    I1.init( G, bType );
    I1.parseLen( G.length() );
    I2.init( G, bType );
}

void printIsoAlgStats( ostream &os )
{
}

//-----------------------------------------------------------------------

void IsoDBItem1::init( const RBBPG& G, IsomorphismBoundaryType bType )
{
   //vertices = G.vertices();
   //edges = G.edges();
   IsoStats::addInit();
   DegreeSequence D;
   degreeSequence( G, D, bType );
   int n = min( DegSeqSize, D.size() );
   for ( int i=0; i<n; i++ )
      degSeq[i] = D[i];
   for ( int i=n; i<DegSeqSize; i++ )
      degSeq[i] = 0;
   hash = MurmurHash64A( &degSeq[0], DegSeqSize, 0 );
}

bool IsoDBItem1::operator==( const IsoDBItem1& a ) const
{
   IsoStats::addIso1Check();
   if ( hash != a.hash ) return false;
   MArray<char> D1( (void*)   degSeq, DegSeqSize );
   MArray<char> D2( (void*) a.degSeq, DegSeqSize );
   if ( D1 != D2 ) return false;
   return true;
}

//-----------------------------------------------------------------------

void IsoDBItem2::init( const RBBPG& G, IsomorphismBoundaryType bType_ )
{
   distMat.fill( 0 );
   G.distanceMatrix( distMat );
   vertPart.init( distMat.dim(), distMat );
   bType = bType_;
}

bool IsoDBItem2::operator==( const IsoDBItem2& a ) const
{
   IsoStats::addIso2Check();
   if ( ::isomorphic( distMat, a.distMat, vertPart, a.vertPart, bType ) )
   {
       IsoStats::addHit();
       return true;
   }
   return false;
}

ostream& operator<<( ostream& os, const IsoDBItem2& rep )
{
   return os << rep.distMat << nl << "boundary " << rep.bType << nl;
}

bostream& operator<<( bostream& s, const IsoDBItem2& rep )
{
    return s << rep.vertPart << rep.distMat << (int)rep.bType;
}

bistream& operator>>( bistream& s, IsoDBItem2& rep )
{
    int temp;
    return s >> rep.vertPart >> rep.distMat >> temp;
    rep.bType = (IsomorphismBoundaryType)temp;
}

}  // end namespace
