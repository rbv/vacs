#include "../src/isomorphism/iso_graph.c"
#include "../src/isomorphism/iso_ltree.c"
#include "../src/isomorphism/iso_tree.c"

// SD is the default graph comparison algorithm even if not used for database matching...
#include "../src/isomorphism/schmidt_druffel/isodb.c"
#include "../src/isomorphism/schmidt_druffel/iso_bpg.c"
#include "../src/isomorphism/schmidt_druffel/isoinit.c"
#include "../src/isomorphism/schmidt_druffel/isomorphism.c"
#include "../src/isomorphism/schmidt_druffel/vertpart.c"

#ifdef USE_BLISS_ISOMORPHISM
#include "../src/isomorphism/bliss/isodb.c"
#endif

#ifdef USE_NAUTY_ISOMORPHISM
#include "../src/isomorphism/nauty/isodb.c"
#endif
