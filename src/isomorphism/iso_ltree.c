
/***********************************************************************
 * Source for Labeled Trees.
 **********************************************************************/

/*tex

\file{iso_ltree.c}
\path{src/isomorphism}
\title{Source for Labeled Tree Isomorphism}
\classes{LTree}
\makeheader

xet*/


#include "array/sortarray.h"

#include "graph/ltree.h"
#include "graph/forest.h"


//------------------------------------------------------------------

// Multi-query operations.
//
/*
 * Labeled Tree's canonical rank sequence.
 */
LTreeRank LTree::rank( vertNum root ) const
 {
//cerr << "Rank of tree at vertex " << root << " in \n";
//cerr << *this << nl;

  LTreeRank R( _order+_order );

  R[0]=_order;
  R[1]=labels[root];

  if ( _order==1 ) return R;

  LForest *F = rmNode( root );

  vertNum numTrees = degree(root);
  //
  SortableArray<LTreeRank> subranks( numTrees );

  /*
   * Compute rank for all branches away from "root".
   */
  F->start();
  //
  int i; for (i=0; i < numTrees; i++)
   {
    subranks[i] = F->next()->rank();		// Desired root should be 0.
   }

  subranks.sortDown();

  /*
   * Finally, assign rank from subranks.
   */
  int rIndex=2;
  //
  for (i=0; i < numTrees; i++)
    for (int j=0; j < subranks[i].size(); j++)
     {
      R[rIndex++] = subranks[i][j];
     }

#ifndef AUTODEL
  F->deleteAll();
#endif

  delete F;

//cerr << "Rank equals: " << R << nl;

  return R;
 }

/*
 * Isomorphism test.
 */
bool LTree::operator==(const LTree &T) const
 {
//cout << "Tree operator==\n";
//cout << *this << nl << T << nl;

  if ( _order != T.order() ) return false;

  vertNum c1, c2, Tc1, Tc2;		// prospective center(s)

  bool haveCenter = center( c1, c2 );
  bool havTCenter = T.center( Tc1, Tc2 );

  if ( haveCenter != havTCenter ) return false;

//cout << "centers match\n";

  if ( haveCenter )			// Reduce to rooted tree isomorphism.
   {
     if ( degree(c1) != T.degree(Tc1) ||
          labels[c1] != T.labels[Tc1] ) return false;  // fast check.

//cout << "rank1: " << rank(c1) << nl;
//cout << "rank2: " << T.rank(Tc1) << nl;

     return ( rank(c1) == T.rank(Tc1) );
   }
  else					// Make an artificial center.
   {
     LTree T1(*this);
     LTree T2(T);

     T1.subdivide( c1, c2, 0 );
     T2.subdivide( Tc1, Tc2, 0 );

     if ( degree(c1)+degree(c2) != T.degree(Tc1)+T.degree(Tc2) ) return false; 
    
//cout << "rank1: " << T1.rank(T1.order()-1) << nl;
//cout << "rank2: " << T2.rank(T2.order()-1) << nl;

     return ( T1.rank(T1.order()-1) == T2.rank(T2.order()-1) );
   }

 }

/*
 * Isomorphism reformulated for class Forest's "removeEqualities"
 */
bool eqTest( const LTree &T1, const LTree &T2 )
 {
  return T1 == T2;
 }

