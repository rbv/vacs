#include "general/gtimers.h"
#include "graph/bgraph.h"
#include "bpg/bpg.h"
#include "search/graph_db.h"
#include "isomorphism/nauty/isodb.h"
#include "isomorphism/schmidt_druffel/isodb.h"

#include <algorithm>

namespace NAUTY_Iso
{

void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary )
{
    IsoStats::addInit();
    I1.parseLen( G.length() );

    Gtimers::start( 25, "nauty graph" );

    int vertices = G.vertices();
#ifndef NDEBUG
    nauty_check( WORDSIZE, MAXM, vertices, NAUTYVERSIONID );
#endif
    assert( vertices <= 31 );

    graph ngraph[MAXN * MAXM];  // adj matrix rows
    graph canon_graph[MAXN * MAXM];
    int lab[MAXN];  // labelling
    int ptn[MAXN];  // partitioning
    int orbits[MAXN];
    DEFAULTOPTIONS_GRAPH(options);
    statsblk stats;
    setword workspace[50 * MAXM];

    options.getcanon = TRUE;
    options.defaultptn = FALSE;  // Provide a colouring
    options.tc_level = 0;  // assume that graphs are fairly easy
    //options.invarproc = adjtriang;
    //options.maxinvarlevel = 99;

    // adapted from BGraph::BGraph(const BPG &B, vertNum extra)

    unsigned char bndry[16] = {0};  // current boundary
    int bLen = G.length();
    int nextVert = 0;
    // indices into lab: move boundary vertices to the end
    int nextNonboundary = 0;
    int nextBoundary = vertices - 1;

    memset( ngraph, 0, sizeof(setword) * MAXM * vertices );

    for (int i = 0; i < bLen; i++)
    {
        Operator op = G[i];

        if (op.isVertexOp())
        {
            vertNum vert = op.vertex();
            if ( !ignoreBoundary && G.isBoundaryVertex(i) )
                lab[nextBoundary--] = nextVert;
            else
                lab[nextNonboundary++] = nextVert;

            bndry[vert] = nextVert++;
        }
        else if (op.isEdgeOp())
        {
            vertNum vert1 = bndry[op.vertex1()];
            vertNum vert2 = bndry[op.vertex2()];

            setword *sw;
            sw = &ngraph[vert1 * MAXM];
            ADDELEMENT( sw, vert2 );
            sw = &ngraph[vert2 * MAXM];
            ADDELEMENT( sw, vert1 );
        }
    }

    assert( nextNonboundary == nextBoundary + 1 );

    for (int i = 0; i < vertices; i++)
        ptn[i] = 1;
    if ( nextBoundary >= 0 )
        ptn[nextBoundary] = 0;  // Mark end of partition
    ptn[vertices - 1] = 0;

/*
    cout << "--------------------------\n";
    cout << "nextBoundary = " << nextBoundary << nl;

    cout << "initial lab: ";
    for (int i = 0; i < vertices; i++)
        cout << lab[i] << '(' << ptn[i] << ") ";
    cout << "\n";

    int deg[MAXN] = {0};
    int deg2[MAXN] = {0};
    for (int i = 0; i < vertices; i++)
        deg[i] = POPCOUNT(ngraph[i]);
    std::sort(&deg[0], &deg[vertices]);
*/

    Gtimers::stop( 25 );
    Gtimers::start( 26, "nauty" );

    nauty( ngraph, lab, ptn, NULL, orbits, &options, &stats, workspace, 50 * MAXM, MAXM, vertices, canon_graph );
    assert( stats.errstatus == 0 );

    Gtimers::stop( 26 );
    Gtimers::start( 27, "Canonform" );

    // Concatenate together the rows in order to minimise space
    // Note!! NAUTY numbers bits starting from the left bits, rather than the right bits as
    // is the norm on little endian.
    assert( MAXM == 1 );

    setword accum = 0, temp = 0;
    int usedbits = 0;
    for (int v = 0; v < vertices; v++)
    {
        temp = canon_graph[v];
        accum |= temp >> usedbits;
        usedbits += vertices;
        if ( usedbits >= 8 * sizeof( setword ) )
        {
            I2.canonform.append( accum );
            usedbits -= 8 * sizeof( setword );
            accum = temp << (vertices - usedbits);
        }
    }
    if ( usedbits )
        I2.canonform.append( accum );

    Gtimers::stop( 27 );
    Gtimers::start( 28, "hash" );

    I1.hash = I2.canonform.hash64();

/*
    for (int i = 0; i < vertices; i++)
        deg2[i] = POPCOUNT(canon_graph[i]);
    std::sort(&deg2[0], &deg2[vertices]);

    if ( memcmp(deg, deg2, vertices * sizeof(setword))  || edges1 != edges2)
    {
        BGraph bgraph(G);
        cerr << bgraph << nl;
        for (int i = 0; i < vertices; i++)
            fprintf(stderr,"%08x\n", canon_graph[i]);
            
        cerr << "lab: ";
        for (int i = 0; i < vertices; i++)
            cerr << lab[i] << '(' << ptn[i] << ") ";
        cerr << "\nh " << I1.hash << nl;
        cerr << I2.canonform << nl;
        cerr << "--------------------------\n";
        aassert(false);
    }
*/

    Gtimers::stop( 28 );

#ifdef CHECK_ISO
    I2.graph = G;
    I2.sd_check.init( G, ignoreBoundary ? noBoundary : freeBoundary );
#endif
}

void printIsoAlgStats( ostream &os )
{
    os << "Using NAUTY\n";
}

//-----------------------------------------------------------------------

bool IsoDBItem1::operator==( const IsoDBItem1& a ) const
{
    IsoStats::addIso1Check();
#ifdef CHECK_ISO
    return true; // do full test
#endif
    return hash == a.hash;
}

//-----------------------------------------------------------------------

bool IsoDBItem2::operator==( const IsoDBItem2& a ) const
{
    IsoStats::addIso2Check();
    bool ret = canonform == a.canonform;

#ifdef CHECK_ISO
    bool correct = sd_check == a.sd_check;
    if ( correct != ret )
    {
        BGraph b1(graph), b2(a.graph);
        cerr << "Iso error, nauty=" << ret << " sd=" << correct << ". G1:\n";
        cerr << graph;
        cerr << " item: " << *this << nl;
        cerr << b1;
        cerr << "\nG2:\n";
        cerr << a.graph << " item: " <<  a << nl;
        cerr << b2;
        aassert( false );
    }
#endif

    if (ret)
        IsoStats::addHit();
    return ret;
}

ostream& operator<<( ostream &os, const IsoDBItem2& rep )
{
    os << "Canon form: " << rep.canonform << nl;
}

bostream& operator<<( bostream& s, const IsoDBItem2& rep )
{
#ifdef CHECK_ISO
   s << rep.sd_check << rep.graph;
#endif
   return s << rep.canonform;
}

bistream& operator>>( bistream& s, IsoDBItem2& rep )
{
#ifdef CHECK_ISO
   s >> rep.sd_check >> rep.graph;
#endif
   return s >> rep.canonform;
}

}  // end namespace
