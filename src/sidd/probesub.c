#include "sidd/decls.h"
#include "graph/graph.h"

class Graph;
graph* get_graph(const Graph&);

#ifdef S_MEMORY
char* someMemory::_memory;
int someMemory::_used;
int someMemory::_size;
#endif

/*---------------------------main()-------------------------------------*/
/* this is the main function for the program to be used in conjunction  */
/* with the Gate Matrix program for experiments.                        */

bool SiddProbe(const Graph& iG)
{
  struct graph *G, *H;
  struct graph_list *connected, *this_component;
  //struct graph_list *bi_connected;
  int outcome = 0;

#ifdef S_MEMORY
  someMemory::create(iG.order());
#endif

      //G = (struct graph *) read_graph();
      G = (struct graph *) get_graph(iG);
//print_graph(G);
      H = (struct graph *) copy_graph(G);

      connected = (struct graph_list *) find_connected_components(H);

      H = (struct graph *) find_articulation(H, connected);
      G->articulation = (struct adj_list *) H->articulation;

      outcome = shrink_graph(G);

      this_component = (struct graph_list *) connected;
      while ((outcome == 0) && (this_component != NULL)) {
            outcome = find_621(H, this_component->component);
            this_component = this_component->next;
        }  

#ifdef S_MEMORY
      someMemory::destroy();
#else
      delete G; delete H;
#endif

      //cerr << "outcome = " << outcome << nl;
      return outcome == 0;
}
/*----------------------------------------------------------------------*/
