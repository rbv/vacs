//#include <stdio.h>
#include "sidd/decls.h"
#include <memory.h>

/*---------------------------cat_adj_lists()-----------------------------*/
/* function to concatenate two adjacency lists */
static adj_list *cat_adj_lists(adj_list *list1, adj_list *list2)
//struct adj_list *list1, *list2;
 {
   struct adj_list *temp_ptr;

   if (list2 == NULL) return( (struct adj_list *) list1);
   if (list1 == NULL) return( (struct adj_list *) list2);

   temp_ptr = (struct adj_list *) list1;
   while (temp_ptr->next != NULL) 
          temp_ptr = (struct adj_list *) temp_ptr->next; 
   temp_ptr->next = (struct adj_list *) list2;

   return( (struct adj_list *) list1);
 }
/*---------------------------minimum()-----------------------------------*/
/* function to find the minimum of two integers */
static int minimum(int x, int y)
//  int x, y;
   {
      if (x < y) return(x);
      else return(y);
   }
/*-----------------------recursive_art()---------------------------------*/
static adj_list *recursive_art( graph *G, int *dfs_number, int *low_val, 
				int *dfs_counter, int u, int v)
{
  struct adj_list *list_ptr, *new_ptr, *temp;
  int children_of_root = 0, found = 0;

  (*dfs_counter)++;
  dfs_number[u-1] = *dfs_counter;
  low_val[u-1] = *dfs_counter;
  list_ptr = (struct adj_list *) NULL;

  temp = (struct adj_list *) (G->vertex_table + u -1)->neighbours;

  while (temp != NULL) {
      if (dfs_number[temp->vertex -1] == 0) {
         if (dfs_number[u-1] == 1) children_of_root++;
         list_ptr = (struct adj_list *) cat_adj_lists(list_ptr,
         recursive_art(G, dfs_number, low_val, dfs_counter, temp->vertex, u));
         low_val[u-1] = minimum(low_val[u-1], low_val[temp->vertex -1]);

         if ( (dfs_number[u-1] > 1) && 
              (low_val[temp->vertex - 1] >= dfs_number[u-1]) ) 
            found = 1;
       }
      else {
         if (temp->vertex != v) 
         low_val[u-1] = minimum(low_val[u-1], dfs_number[temp->vertex -1]);
        }

      temp = (struct adj_list *) temp->next;
   }  /* end while */

  if ( (dfs_number[u-1] == 1) && (children_of_root > 1) ) found = 1;
 
  if (found == 1) {
#ifdef S_MEMORY
      new_ptr = (struct adj_list *) 
		someMemory::getMemory(sizeof(struct adj_list));
#else
      //new_ptr = (struct adj_list *) malloc(sizeof(struct adj_list));
      new_ptr = new adj_list;
#endif
      new_ptr->vertex = u;
      new_ptr->next = (struct adj_list *) list_ptr;
      list_ptr = (struct adj_list *) new_ptr;
/*
      printf("vertex-%d is an articulation point!\n", u);
*/
   }
  return ( (struct adj_list *) list_ptr);
}

/*-----------------------find_articulation()-------------------------------*/
/* function to find the articulation points of 
   a connected component H of a graph G.
   Algorithm 6.17, page 307, Horowitz, Sahni, "Computer Algorithms". 
*/

graph *find_articulation(graph *G, graph_list *connected)
{
  int *dfs_number, *low_val, dfs_counter;
  struct graph_list *H;

/*
  printf("Looking for articulation points ...\n");
*/
  H = (struct graph_list *) connected;

  // moved from while loop below
  int sz = G->size * sizeof(int);
  dfs_number = (int *) malloc(sz);
  low_val = (int *) malloc(sz);

  while (H != NULL) {
        dfs_counter = 0;

        memset(dfs_number, 0, sz);
        memset(low_val, 0, sz);
/*
        printf("Searching Connected Component-%d ...\n", H->label);
*/

        H->component->articulation = (struct adj_list *) 
                           recursive_art(G, dfs_number, low_val, &dfs_counter, 
                               H->component->vertex_table->label, 0);
        G->articulation = (struct adj_list *)
               cat_adj_lists(G->articulation, H->component->articulation);
/*
        printf("Component-%d, Size=%d :\n", H->label, H->component->size);
        print_graph(H->component);
*/
        H = (struct graph_list *) H->next; 
    }

    free((char*) dfs_number); 
    free((char*) low_val);

    return((struct graph *) G);
}
/*---------------------------THE END-------------------------------------*/
