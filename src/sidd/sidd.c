/*-----------------------------------------------------------------------*/
/* This file contains the functions used to search for the               */ 
/* minor containment of obstructions 4.2.1, 8.2.1, 8.2.2, 8.2.3, 8.2.4   */
/*-----------------------------------------------------------------------*/
//#include <stdio.h>
#include "sidd/decls.h"

/*----------------------introduce_vertex_weights()-----------------------*/
/* function to introduce weights to vertices                             */
/* every articulation point gets assigned a weight = 1. The rest are 0.  */

graph *introduce_vertex_weights( graph *G )
{
  struct vertex_record *vertex_ptr;
  struct adj_list *temp_ptr;

  temp_ptr = (struct adj_list *) G->articulation;

  while (temp_ptr != NULL) {
         vertex_ptr = (struct vertex_record *)
                       G->vertex_table + temp_ptr->vertex - 1;
         vertex_ptr->weight = 1;
         temp_ptr = (struct adj_list *) temp_ptr->next;
    }  /* end while */
  return((struct graph *) G);
}
/*----------------------delete_edge()-----------------------*/
graph *delete_edge(graph *G, int v1, int v2)
{
  struct adj_list *edge_ptr;
  struct vertex_record *vertex_ptr;

/*
  printf("Deleting edge (%d, %d) ...\n", v1, v2);
*/

  vertex_ptr = (struct vertex_record *) G->vertex_table + v1 - 1;

  (vertex_ptr->degree)--;
  if (vertex_ptr->degree == 0) G->size = G->size - 1;

  edge_ptr = (struct adj_list *) vertex_ptr->neighbours;
 
  if (edge_ptr->vertex == 0) {
/*
     printf("Edge does not exist!\n");
*/
     return((struct graph *) G);
   }

  if (edge_ptr->vertex == v2) { 
      vertex_ptr->neighbours = (struct adj_list *) edge_ptr->next;
      return((struct graph *) G);
   }

  while ( (edge_ptr->next != NULL) && (edge_ptr->next->vertex != v2) )
        edge_ptr = (struct adj_list *) edge_ptr->next;

  if (edge_ptr->next == NULL) return((struct graph *) G);

  if (edge_ptr->next->vertex == v2) edge_ptr->next = edge_ptr->next->next;

  return((struct graph *) G);
}
/*----------------------get_edge_ptr()-----------------------*/
adj_list *get_edge_ptr( graph *G, int v1, int v2)
{
  struct adj_list *edge_ptr;

  edge_ptr = (struct adj_list *) 
             (G->vertex_table + v1 - 1)->neighbours;
  while ( (edge_ptr != NULL) && (edge_ptr->vertex != v2) )
        edge_ptr = (struct adj_list *) edge_ptr->next;
  return((struct adj_list *) edge_ptr);
}

/*----------------------prune_degree_one()-----------------------*/
/* delete vertex v which has degree = 1.                          */

graph *prune_degree_one( graph *G, int v, int *found)
{
  struct vertex_record *vertex_ptr; 
  int /*degree,*/ neighbour;
  //extern struct graph *check_vertex();

/*
  printf("Pruning Degree One : vertex-%d\n", v);
*/

  vertex_ptr = (struct vertex_record *) G->vertex_table + v - 1;
  neighbour = (vertex_ptr->neighbours)->vertex;

  vertex_ptr->degree = 0;
  vertex_ptr->weight = 0;
  vertex_ptr->neighbours = (struct adj_list *) NULL;

  G = (struct graph *) delete_edge(G, neighbour, v);

  G = (struct graph *) check_vertex(G, neighbour, found);

  return((struct graph *) G);
}
/*----------------------prune_degree_two()-----------------------*/

graph *prune_degree_two( graph *G, int v, int *found)
{
  struct vertex_record *vertex_ptr; 
  struct adj_list *neighbour1, *neighbour2, *old_edge1, *old_edge2;
  int /*neighbour, degree,*/ total_wt /*, old_wt*/;
  //extern struct graph *check_vertex();

  vertex_ptr = (struct vertex_record *) G->vertex_table + v - 1;
  neighbour1 = (struct adj_list *) vertex_ptr->neighbours;
  neighbour2 = (struct adj_list *) vertex_ptr->neighbours->next;

  old_edge1 = (struct adj_list *)
              get_edge_ptr(G, neighbour1->vertex, neighbour2->vertex);
  old_edge2 = (struct adj_list *)
              get_edge_ptr(G, neighbour2->vertex, neighbour1->vertex);

/*
  if ((neighbour1->weight >= 6) && (neighbour2->weight >= 6)) {
     if (old_edge1 == NULL) {
        G = (struct graph *) 
            add_edge(G, neighbour1->vertex, neighbour2->vertex, 6);
        G = (struct graph *) 
            add_edge(G, neighbour2->vertex, neighbour1->vertex, 6);
      }
     else {
        old_edge1->weight = 6;
        old_edge2->weight = 6;
      }
     return((struct graph *) G);
   }
*/

  if ((neighbour1->weight >= 6) || (neighbour2->weight >= 6)) 
     return((struct graph *) G);

/*
  printf("Pruning Degree Two : vertex-%d\n", v);
*/

  vertex_ptr->degree = 0;
  vertex_ptr->neighbours = (struct adj_list *) NULL;

  total_wt = vertex_ptr->weight + neighbour1->weight + neighbour2->weight;
  vertex_ptr->weight = 0;

  if (total_wt > 3) total_wt = 3;

  G = (struct graph *) delete_edge(G, neighbour1->vertex, v);
  G = (struct graph *) delete_edge(G, neighbour2->vertex, v);

  if (old_edge1 == NULL) {
     G = (struct graph *) 
          add_edge(G, neighbour1->vertex, neighbour2->vertex, total_wt);
     G = (struct graph *) 
          add_edge(G, neighbour2->vertex, neighbour1->vertex, total_wt);
    }
  else {
        if (old_edge1->weight <= 2) {
           old_edge1->weight = total_wt;
           old_edge2->weight = total_wt;
          }
        else 
             if (total_wt == 3) {
                old_edge1->weight = old_edge1->weight + total_wt; 
                old_edge2->weight = old_edge2->weight + total_wt; 

                if (old_edge1->weight >= 9) {
                    *found = 1;
/*
                    printf("vertices- %d, %d, %d are part of an obstruction.\n",
                            v, neighbour1->vertex, neighbour2->vertex);
*/
                  }
              }
    }

  if (*found == 0)
      G = (struct graph *) check_vertex(G, neighbour1->vertex, found);

  if (*found == 0)
      G = (struct graph *) check_vertex(G, neighbour2->vertex, found);

  return((struct graph *) G);
}
/*----------------------check_vertex()----------------------------*/
/* check if vertex v has degree = 1 or 2 and if so prune it.     */

graph *check_vertex( graph *G, int v, int *found)
{
  struct vertex_record *vertex_ptr;
  //extern struct graph* prune_degree_one();
  //extern struct graph* prune_degree_two();

  vertex_ptr = (struct vertex_record *) G->vertex_table + v - 1;

  if (vertex_ptr->degree == 1)
      G = (struct graph *) prune_degree_one(G, v, found);
  else {
        if (vertex_ptr->degree == 2)
           G = (struct graph *) prune_degree_two(G, v, found);
    }

  return((struct graph *) G);
}
/*----------------------shrink_graph()---------------------------------*/
/* this is the function which shrinks the graph to search for          */
/*   K4 and the gango4.                                                */
/* The integer returned is as follows:                                 */
/*      0 => neither K4 nor gango4 was found                           */
/*      1 => only gango4 was found                                     */
/*      2 => only K4 was found                                         */
/*      3 => both K4 and gango4 were found                             */
/*---------------------------------------------------------------------*/ 

int shrink_graph( graph *H )
{
  int i, n, count;
  int found=0;
  
  n = H->size;

  H = (struct graph *) introduce_vertex_weights(H);
/*
  printf("Vertex Weights Introduced!\n");
  printf("The Graph Looks as Follows:\n");
  print_graph(H);

  printf("Shrinking the Graph...\n");
*/

  i=1; 
  while ((found == 0) && (i <= n)) {
     H = (struct graph *) check_vertex(H, i, &found);
     i++;
   }   /* end while */

  /* now count all vertices of degree >= 3 */
  count = 0;
  for (i=1; i<=n; i++) {
      if ( (H->vertex_table + i - 1)->degree >= 3) count++;
      if ( (H->vertex_table + i - 1)->degree == 2) found = 1;
    }
  /* if count > 3 then the graph contains K4 */
  if (count > 3) 
      if (found == 1) return(3);
      else return(2);
  else 
      if (found == 1) return(1);  
      else return(0);

}

/*-----------------------------------THE END---------------------------------*/
