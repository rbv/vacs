//#include <stdio.h>
#include "sidd/decls.h"

/*-----------------------recursive_dfs()---------------------------------*/

void recursive_dfs( graph *G, 
vertex_record *node, 
int *dfs_number, 
int *counter)
  {
     struct adj_list *temp;

     (*counter)++;
     dfs_number[node->label - 1] = *counter;

/*
     printf("vertex-%d visited-%d\n", node->label, *counter); 
*/

     temp = (struct adj_list *) node->neighbours;
     while (temp != NULL) {
/*
           printf("Looking at vertex-%d\n", temp->vertex);
*/
           if (dfs_number[temp->vertex - 1] == 0) 
   recursive_dfs(G, G->vertex_table + temp->vertex - 1, dfs_number, counter);
           temp = (struct adj_list *) temp->next;
       }  /* end while  */

  } /* end function */
/*-----------------------create_component()----------------------*/
static graph_list *create_component(int label, int size)
{
struct graph_list *temp;

#ifdef S_MEMORY
  temp = (struct graph_list *) someMemory::getMemory(sizeof(struct graph_list));
#else
  //temp = (struct graph_list *) malloc( sizeof (struct graph_list) );
  temp = new graph_list;
#endif
  temp->label = label;
  temp->component = (struct graph *) create_graph(size);
  temp->next  = (struct graph_list *) NULL;

  return( (struct graph_list *) temp );
} /* end of function create_component */
/*-----------------------find_connected_components()----------------------*/

graph_list *find_connected_components( graph *G )
{
  struct vertex_record *vertices, *temp_vertices, *temp_ptr;
  int n, dfs_counter=0, i, j, last = 0, comp_size, comp_number=0;
  struct graph_list *connected;
  struct graph_list *last_comp, *new_comp;
  int *dfs_number;  /* array of depth-first numbers indexed by vertex number */
 
#if 0
  extern recursive_dfs();
  extern struct graph_list *create_component();
  extern print_graph();
#endif

  n = G->size;
  vertices = (struct vertex_record *) G->vertex_table;

  dfs_number = (int *) calloc(n, sizeof(int));
  connected = (struct graph_list *) NULL;

  while (dfs_counter < n) {
        while (dfs_number[vertices->label - 1] != 0) vertices++;

/*
        printf("Doing DFS from vertex-%d...\n", vertices->label);
*/
        last = dfs_counter;
        recursive_dfs(G, vertices, dfs_number, &dfs_counter);

        comp_size = dfs_counter - last;
        comp_number++;

/*
        printf("Connected component-%d  Size=%d\n", comp_number, comp_size);
*/
  
        new_comp = (struct graph_list *) 
                     create_component(comp_number, comp_size);

        if (connected == NULL) connected = (struct graph_list *) new_comp;
        else last_comp->next = (struct graph_list *) new_comp;
        last_comp = (struct graph_list *) new_comp;

        temp_vertices = (struct vertex_record *) 
                                new_comp->component->vertex_table; 

        i=0;   /* i keeps track of the no. of vertices added to component */ 
        j = 0; /* j is the index for vertices 1 thru n */

        while (i < comp_size && j < n ) {
               if (dfs_number[j] > last) {
                   temp_ptr = (struct vertex_record *) 
                               temp_vertices + i; 
/*
                               temp_vertices + (dfs_number[j] - last - 1); 
*/
                   temp_ptr->label      = j+1;
                   temp_ptr->neighbours = (struct adj_list *) 
                                          (G->vertex_table+ j)->neighbours;
                   i++;
                }  
               j++;
         }  
   }  
  
/*
  printf("Depth First Search Completed!\n");
  for (i=0; i<n; i++) printf("vertex-%d visited-%d\n", i+1, dfs_number[i]); 
*/

  free((char*) dfs_number);

/*
  printf("There are %d Connected Components:\n", comp_number);

  new_comp = (struct graph_list *) connected;
  while (new_comp != NULL) {
    printf("Component-%d, Size=%d :\n", 
            new_comp->label, new_comp->component->size);
    print_graph(new_comp->component);
    new_comp = (struct graph_list *) new_comp->next;
   }
*/

  return((struct graph_list *) connected);

} /* end function */
