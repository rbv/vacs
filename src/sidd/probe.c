#include <stdio.h>
#include "sidd/decls.h"

/*---------------------------main()-------------------------------------*/
/* this is the main function for the program to be used in conjunction  */
/* with the Gate Matrix program for experiments.                        */

int main()
{
  struct graph *G, *H;
  struct graph_list *connected, *this_component;
  //struct graph_list *bi_connected;
  int outcome = 0;
  FILE *file_ptr;

      G = (struct graph *) read_graph();
      H = (struct graph *) copy_graph(G);
      connected = (struct graph_list *) find_connected_components(H);

      H = (struct graph *) find_articulation(H, connected);
      G->articulation = (struct adj_list *) H->articulation;

      outcome = shrink_graph(G);

      this_component = (struct graph_list *) connected;
      while ((outcome == 0) && (this_component != NULL)) {
            outcome = find_621(H, this_component->component);
            this_component = this_component->next;
        }  

      file_ptr = stdout;
      fprintf(file_ptr, "%d\n", outcome);

      return(outcome);
}
/*----------------------------------------------------------------------*/
