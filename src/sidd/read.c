#include <stdio.h>
#include "sidd/decls.h"

/*-----------------------create_graph()---------------------------------*/
graph *create_graph(int size)
{
   int i;
   struct graph *H;
   struct vertex_record *vertex_ptr;

#ifdef S_MEMORY
   H = (struct graph *) someMemory::getMemory(sizeof(struct graph));
   H->vertex_table = (struct vertex_record *) 
		someMemory::getZeroMemory(size*sizeof(struct vertex_record));
#else
   //H = (struct graph *) malloc(sizeof(struct graph));
   H = new graph;
   H->vertex_table = (struct vertex_record *) calloc(size, sizeof(struct vertex_record));
#endif
   H->size = size;
   H->articulation = (struct adj_list *) NULL;

   for (i=1; i<= size; i++) {
        vertex_ptr = (struct vertex_record *) H->vertex_table + i - 1;
        vertex_ptr->label  = i;
        vertex_ptr->degree = 0;
        vertex_ptr->weight = 0;
        vertex_ptr->neighbours = NULL;
     }

   return( (struct graph *) H);
}
/*-----------------------create_edge()---------------------------------*/
static adj_list *create_edge(int vertex, int wt)
{
  struct adj_list * new_ptr;
 
#ifdef S_MEMORY
  new_ptr = (struct adj_list *) someMemory::getMemory(sizeof(struct adj_list));
#else
  //new_ptr = (struct adj_list *) malloc(sizeof(struct adj_list));
  new_ptr = new adj_list;
#endif

    new_ptr->vertex = vertex;
    new_ptr->weight = wt;
    new_ptr->next   = NULL;
   
   return((struct adj_list *) new_ptr);

}
/*-----------------------add_edge()---------------------------------*/
graph *add_edge(graph *G, int v1, int v2, int wt)
{  
  struct adj_list *ptr1;
  struct vertex_record *temp_ptr;

    ptr1 =  (struct adj_list *) create_edge(v2, wt); 

    temp_ptr = (struct vertex_record *) (G->vertex_table + v1 -1);
    ptr1->next = (struct adj_list *) temp_ptr->neighbours;
    temp_ptr->neighbours = (struct adj_list *) ptr1;
    (temp_ptr->degree)++;
    
    return( (struct graph *) G);
}

/*-----------------------read_graph()---------------------------------*/
graph *read_graph()
{
struct graph *H;
int n, i, label, neighbour;

/*
   printf("Number of vertices in the graph =");
*/
   scanf("%d", &n);
/*
   printf("\n"); 
*/

   if (n > 0) {
    H = (struct graph *) create_graph(n);

/*
   printf("No. of vertices = %d\n", H->size);

   printf("Input the graph in the following format:\n");
   printf("Vertex# neighbour neighbour ... 0\n"); 
*/
   
   for (i=1; i<=n; i++) {
        scanf("%d", &label);
        scanf("%d", &neighbour);

        while(neighbour != 0) {
/*
           if (neighbour > label) {
               H = (struct graph *) add_edge(H, label, neighbour, 1);
               H = (struct graph *) add_edge(H, neighbour, label, 1);
             }
*/
           if (get_edge_ptr(H, label, neighbour) == NULL)
               H = (struct graph *) add_edge(H, label, neighbour, 1);
           if (get_edge_ptr(H, neighbour, label) == NULL)
               H = (struct graph *) add_edge(H, neighbour, label, 1);
             
           scanf("%d", &neighbour);
         }  /* end while */

     } /* end for */
  }  /* end if */
  else H = NULL;

 return( (struct graph *) H);
} /* end function read_graph()  */

/*-------------- generate_random_graph() -----------------------*/

/* This function generates a random connection graph of n nets.
   Probability[edge(i,j)=1] = Probability[random < chance].
   The graph is represented in the adjacency_list format.
*/

graph *generate_random_graph(int n, float probability)
{
  struct graph *G;
  int net1, net2;
  int chance;

/*
  printf("Number of vertices in the graph =");
  scanf("%d", &n);
  printf("\n");

  printf("Probability of an Edge =");
  scanf("%f", &probability);
  printf("\n");
*/

  if (n > 0) {
      G = (struct graph *) create_graph(n);

/*
      printf("No. of vertices = %d\n", G->size);
*/

      chance = 2147483647;                     /* i.e. 2^31 - 1   */
      chance = chance * probability;

      for (net1=1; net1<=n; net1++)
          for (net2=net1+1; net2<=n; net2++)
              if (random() <= chance) {
                 G = (struct graph *) add_edge(G, net1, net2, 1);
                 G = (struct graph *) add_edge(G, net2, net1, 1);
               } /* end if */

   }
  return((struct graph *) G);
} /* end of function */

/*-----------------------print_graph()----------------------------------*/
/* This function prints the connection graph to the standard output */
void print_graph( struct graph *H )
{
 int n, i;
 struct adj_list *edge_ptr;
 struct vertex_record *vertex_ptr;

 n = H->size;

 printf("Adjacency List Representation: (edge:weight)\n");

    for (i=1; i<=n; i++) {
        vertex_ptr = (struct vertex_record *) H->vertex_table + i - 1;
        printf("vertex-%2d, degree=%2d, weight=%d : ", 
                vertex_ptr->label, vertex_ptr->degree, vertex_ptr->weight);
        edge_ptr = (struct adj_list *) vertex_ptr->neighbours;
        while ( edge_ptr != NULL) {
              printf("%2d:%d ", edge_ptr->vertex, edge_ptr->weight);
              edge_ptr  = edge_ptr->next;
        } /* end while */
        printf("\n");
    } /* end for */
   
    edge_ptr = (struct adj_list *) H->articulation; 
    if (edge_ptr == NULL) 
           printf("There are no Articulation Points!\n");
    else {
          printf("The Articulation Points are:\n");
          while (edge_ptr != NULL) {
                 printf("%d ", edge_ptr->vertex);
                 edge_ptr = (struct adj_list *) edge_ptr->next;
            } /* end while */
      }  /* end else */
    printf("\n");
} /* end function print_graph()   */

/*--------------------------copy_graph()------------------------------*/
graph *copy_graph( graph *G )
{
   int i;
   struct graph *H;
   struct vertex_record *H_vertex_ptr, *G_vertex_ptr;
   struct adj_list *G_neighbour;

#ifdef S_MEMORY
   H = (struct graph *) someMemory::getMemory(sizeof(struct graph));
   H->vertex_table = (struct vertex_record *) 
	    someMemory::getZeroMemory(G->size*sizeof(struct vertex_record));
#else
   //H = (struct graph *) malloc(sizeof(struct graph));
   H = new graph;
   H->vertex_table = (struct vertex_record *) calloc(G->size, sizeof(struct vertex_record));
#endif
   H->size = G->size;
   H->articulation = (struct adj_list *) G->articulation;

   for (i=1; i<= H->size; i++) {
        H_vertex_ptr = (struct vertex_record *) H->vertex_table + i - 1;
        G_vertex_ptr = (struct vertex_record *) G->vertex_table + i - 1;
        H_vertex_ptr->label  = G_vertex_ptr->label; 
        H_vertex_ptr->weight = G_vertex_ptr->weight;
        G_neighbour = (struct adj_list *) G_vertex_ptr->neighbours;
        while (G_neighbour != NULL) {
            if (G_neighbour->vertex > H_vertex_ptr->label) {
               H = (struct graph *) 
                   add_edge(H, H_vertex_ptr->label, G_neighbour->vertex, 1);
               H = (struct graph *) 
                   add_edge(H, G_neighbour->vertex, H_vertex_ptr->label, 1);
             }
             
            G_neighbour = (struct adj_list *) G_neighbour->next;
         }
     }

   return( (struct graph *) H);

}
/*---------------------------THE END-------------------------------------*/

#include "graph/graph.h"

graph *get_graph(const Graph& iG)
{
   struct graph *H;
   int n = iG.order();

   if (n > 0) {
   H = create_graph(n);

   for (int i=1; i<n; i++) 
   {
    for (int j=i+1; j<=n; j++)
    {
           if (!iG.isEdge(i-1,j-1)) continue;
//cerr << "edge " << i << ' ' << j << nl;

           if (get_edge_ptr(H, i, j) == NULL)
               H = add_edge(H, i, j, 1);
           if (get_edge_ptr(H, j, i) == NULL)
               H = add_edge(H, j, i, 1);
             
    }
  }

  }  /* end if */
  else H = NULL;

 return H;
} /* end function read_graph()  */

