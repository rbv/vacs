//#include <stdio.h>
#include "sidd/decls.h"

/*----------------------prune_degree_one_621()-----------------------*/
/* delete vertex v which has degree = 1.                          */

graph *prune_degree_one_621( graph *G, int v )
{
  struct vertex_record *vertex_ptr; 
  int /*degree,*/ neighbour;
  //extern struct graph *check_vertex_621();

/*
  printf("Pruning Degree One : vertex-%d\n", v);
*/

  vertex_ptr = (struct vertex_record *) G->vertex_table + v - 1;
  neighbour = (vertex_ptr->neighbours)->vertex;

  vertex_ptr->degree = 0;
  vertex_ptr->weight = 0;
  vertex_ptr->neighbours = (struct adj_list *) NULL;

  G = (struct graph *) delete_edge(G, neighbour, v);

  G = (struct graph *) check_vertex_621(G, neighbour);

  return((struct graph *) G);
}
/*----------------------prune_degree_two_621()-----------------------*/

graph *prune_degree_two_621( graph *G, int v )
{
  struct vertex_record *vertex_ptr; 
  struct adj_list *neighbour1, *neighbour2, *old_edge1, *old_edge2;
  //int neighbour, degree, total_wt, old_wt;
  //extern struct graph *check_vertex_621();

/*
  printf("Pruning Degree Two : vertex-%d\n", v);
*/

  vertex_ptr = (struct vertex_record *) G->vertex_table + v - 1;
  neighbour1 = (struct adj_list *) vertex_ptr->neighbours;
  neighbour2 = (struct adj_list *) vertex_ptr->neighbours->next;

  if ((neighbour1->weight >= 3) && (neighbour2->weight >= 3)) 
      return((struct graph *) G);

  vertex_ptr->degree = 0;
  vertex_ptr->weight = 0;
  vertex_ptr->neighbours = (struct adj_list *) NULL;

  G = (struct graph *) delete_edge(G, neighbour1->vertex, v);
  G = (struct graph *) delete_edge(G, neighbour2->vertex, v);

  old_edge1 = (struct adj_list *)
              get_edge_ptr(G, neighbour1->vertex, neighbour2->vertex);
  old_edge2 = (struct adj_list *)
              get_edge_ptr(G, neighbour2->vertex, neighbour1->vertex);

  if (old_edge1 == NULL) {
     if ((neighbour1->weight >= 3) || (neighbour2->weight >= 3)) {
        G = (struct graph *) 
            add_edge(G, neighbour1->vertex, neighbour2->vertex, 3);
        G = (struct graph *) 
            add_edge(G, neighbour2->vertex, neighbour1->vertex, 3);
      }  /* end if */
     else {
        G = (struct graph *) 
            add_edge(G, neighbour1->vertex, neighbour2->vertex, 2);
        G = (struct graph *) 
            add_edge(G, neighbour2->vertex, neighbour1->vertex, 2);
      } /* end else */
   } /* end if */
  else {
           old_edge1->weight = 3;
           old_edge2->weight = 3;
   } /* end else */

      G = (struct graph *) check_vertex_621(G, neighbour1->vertex);
      G = (struct graph *) check_vertex_621(G, neighbour2->vertex);

  return((struct graph *) G);
}
/*----------------------check_vertex_621()----------------------------*/
/* check if vertex v has degree = 1 or 2 and if so prune it.     */

graph *check_vertex_621( graph *G, int v )
{
  struct vertex_record *vertex_ptr;
  //extern struct graph* prune_degree_one_621();
  //extern struct graph* prune_degree_two_621();

  vertex_ptr = (struct vertex_record *) G->vertex_table + v - 1;

  if (vertex_ptr->degree == 1)
      G = (struct graph *) prune_degree_one_621(G, v);
  else {
        if (vertex_ptr->degree == 2)
           G = (struct graph *) prune_degree_two_621(G, v);
    }

  return((struct graph *) G);
}
/*----------------------find_621()---------------------------------*/
int find_621( graph *G, graph *component )
{
  int i, n, count, vertex;
  
  n = component->size;

/*
  printf("Searching for Obstruction 6.2.1 ...\n");
*/

  i=1; 

  while (i <= n) {
     vertex = (component->vertex_table + i-1)->label;
     G = (struct graph *) check_vertex_621(G, vertex);
     i++;
   }   /* end while */


  count = 0;
  for (i=1; i<=n; i++) {
      vertex = (component->vertex_table + i-1)->label;
      if ( (G->vertex_table + vertex - 1)->degree >= 2) count++;
    }
  if (count >= 3) return(4);
  else            return(0);
/*
     printf("Obstruction 6.2.1 was found!\n");
     printf("Obstruction 6.2.1 NOT found!\n");
*/

}

/*-----------------------------------THE END---------------------------------*/
