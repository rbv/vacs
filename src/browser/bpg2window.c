
/*
 *  Draw a BPG in the X11 window.
 */

/*tex

\file{bpg2window.c}
\path{src/browser}
\title{Draw a BPG in the X11 Window}
\classes{}
\makeheader

xet*/


#include "browser/brstate.h"
#include "browser/browser.h"
#include "general/str.h"
#include "general/random.h"

/************************************************************************
 * Class Browser member functions.
 ************************************************************************/


/*
 * Draw graph. 
 */
void Browser::DrawGraph()
 {
      
     // clear window
     XClearWindow(viewWindow.display, viewWindow.window);
     if(toggleFlag) {
     // draw graph
        // get window sizes
        XWindowAttributes win_attr;
        if(!XGetWindowAttributes(viewWindow.display, viewWindow.window,
                                 &win_attr)) {
           cerr << "XGetWindowAttributes failure\n";
           return;
        }
        if (bState->nodeAvailable())
        {
            const BPG& bpg = (bState->node()).graph();
            /*
             * How much work do we need to do?
             */
            vertNum bSize = bpg.boundarySize();
            vertNum bLen = bpg.length();

            /*
             * How many pegs in our ladder representation?
             */
            VertArray bWidth(bSize);
            for (int i=0; i<bSize; i++) bWidth[i]=0;

            /*
             * Count the number of each vertex pull-offs.
             */
            for (i=0; i<bLen; i++)
             {
               Operator op = bpg[i];

               if (op.isVertexOp())
                {
                 bWidth[op.vertex()]++;
                }
             }

            /*
             * Find maximum width of ladder.
             */
            vertNum maxWidth = 1;
            for (i=0; i<bSize; i++)
              if (maxWidth < bWidth[i]) maxWidth=bWidth[i];

            /*
             * Assign (x,y) positions to each node.
             */
            float xoff = .1, yoff = .1;
            float xspace = (1.0-xoff)/((float) maxWidth+1);
            float yspace = (1.0-yoff)/((float) bSize);


            /*
             * Keep track of each boundary vertex.
             */
            vertNum vertID = 0;
            Array<vertNum> bndry(bSize);
            Array<int> xbndry(bSize);
            Array<int> ybndry(bSize);

            /*
             * Make graph by looking at each of the operators.
             */
            RandomInteger RND(50);        // Some randomness to see all edges.

            for (i=0; i<bLen; i++)
             {
               Operator op = bpg[i];

               if (op.isVertexOp())
                {
                 vertNum vert;
                 vert = op.vertex();

                 bndry[vert]  = vertID++;
                 // this data should be stored in array
                 xbndry[vert] = (int)
              (( 1.0-xoff-xspace*bWidth[vert]-- -RND()/800.0)*win_attr.width) ;
                 ybndry[vert] = (int)
                 (( 1.0-yoff-vert*yspace -RND()/1000.0 )*win_attr.height);
                 // cerr << "x = " << x << " y = " << y << nl;
                 XDrawRectangle(viewWindow.display, viewWindow.window,
                                viewWindow.gc,
                                xbndry[vert], ybndry[vert], 2, 2);
                }

               if (op.isEdgeOp())
                {
                 vertNum vert1 = op.vertex1();
                 vertNum vert2 = op.vertex2();
                 XDrawLine(viewWindow.display, viewWindow.window, 
                           viewWindow.gc,
                           xbndry[vert1], ybndry[vert1],
                           xbndry[vert2], ybndry[vert2]
                           );
                }

             }


             for (i=vertID=0; i<bLen; i++)
              {
                Operator op = bpg[i];

                if (op.isVertexOp())
                 {
                   if ( bpg.isBoundaryVertex(i) )
                      ;// vertID  bvert  op.vertex() vertID
                   else
                      ;// vertID  vert;

                   vertID++;
                 }
              }

             for (i=0; i<vertID; i++)
              {
                bool nonBoundary = true;

                for (int j=0; j<bSize; j++)
                  if (i == bndry[j])
                    {
                      ;// i  bvert (j) v i label
                      nonBoundary = false;
                    }

                if (nonBoundary); // v i vert
              }
        }
     }
 }
