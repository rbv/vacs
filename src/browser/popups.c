/*
 * The popup dialog windows used by the browser.
 */

/*tex
 
\file{popups.c}
\path{src/browser}
\title{The popup dialog windows used by the browser}
\classes{}
\makeheader
 
xet*/


#include "browser/brstate.h"
#include "browser/browser.h"

//#include <X11/Intrinsic.h>
//#include <X11/StringDefs.h>
//#include <X11/Shell.h>

//#include <X11/Xaw/Box.h>
//#include <X11/Xaw/Cardinals.h>
//#include <X11/Xaw/Command.h>
//#include <X11/Xaw/Dialog.h>
//#include <X11/Xaw/SimpleMenu.h>
//#include <X11/Xaw/SmeBSB.h>

#include <X11/bitmaps/xlogo16>

/***********************************************************************/

#include <stdio.h>

static int String2int(String s)
 {
   // lazy -- need to use stream stuff.
   int numb;
   sscanf(s,"%d",&numb);
   return numb;
 }

/*
 *  Do something useful with input of GraphNum.
 */
void okayKeyInput( Widget widget, XtPointer clientData, XtPointer)
{
    Widget dialog = XtParent(widget);
    String Skey = XawDialogGetValueString(dialog);
 
    if ( bState->toViaKey( String2int(Skey) ) )
     {
      xBrowser->updateScreen();
     }
    else
     {
      cerr << "Can't find node '" << Skey << "' !\n";
      return;
     }

    Widget popup = XtParent( (Widget) clientData);
    XtDestroyWidget(popup);
}

/*      
 * Destroy Popup Prompt
 */
void destroyPopupPrompt( Widget, XtPointer clientData, XtPointer)
{
    Widget popup = XtParent( (Widget) clientData);
    XtDestroyWidget(popup);
}


/*
 * Creates and pops up a Dialog widget (from a menu item!)
 * for inputing graph key number.
 */
void popupAskKey(Widget	button, XtPointer, XtPointer)
{
    Arg		args[5];
    Widget	popup, dialog;
    Position	x, y;
    Dimension	width, height;
    Cardinal	n;

//cerr << "Begin PopupPrompt\n";

    /*
     * This will position the upper left hand corner of the popup at the
     * center of the widget which invoked this callback, which will also
     * become the parent of the popup.  I don't deal with the possibility
     * that the popup will be all or partially off the edge of the screen.
     */
    n = 0;
    XtSetArg(args[0], XtNwidth, &width); n++;
    XtSetArg(args[1], XtNheight, &height); n++;
    XtGetValues(button, args, n);

    XtTranslateCoords(button, (Position) (width / 2), (Position) (height / 2),
		      &x, &y);
    n = 0;
    XtSetArg(args[n], XtNx, x);	n++;
    XtSetArg(args[n], XtNy, y);	n++;

//cerr << "Begin to createShell\n";

    /*
     * Class "button" is not a subclass of a Core widget so use its parent
     * as the parent of our popup shell.
     */
    popup = XtCreatePopupShell("prompt", transientShellWidgetClass, 
		XtParent(button), args, n);

//cerr << "popup created\n";

    /* 
     * The popup will contain a dialog box, prompting the user for input. 
     */
    dialog = XtCreateManagedWidget("keyDialog",dialogWidgetClass,popup,NULL,0);

//cerr << "dialog created\n";

    /*
     * The prompting message's size is dynamic; allow it to request resize. 
     */
    XawDialogAddButton(dialog, "ok", okayKeyInput, (XtPointer) dialog);
    XawDialogAddButton(dialog, "cancel", destroyPopupPrompt, (XtPointer)dialog);

//cerr << "dialog button created\n";

    /*
     * Popup and realize the dialog widget.
     */
    //XtPopup(popup, XtGrabNone);
    XtPopup(popup, XtGrabExclusive);
}

/***********************************************************************/

/*
 *  Do something useful with input of Graphs minor label.
 */
void okayLabelInput( Widget widget, XtPointer clientData, XtPointer)
{
    Widget dialog = XtParent(widget);
    String Slab = XawDialogGetValueString(dialog);

    if ( bState->toViaLabel( String2int(Slab) ) )
     {
      xBrowser->updateMinor();
     }
    else
      cerr << "Can't find minor '" << Slab << "' ^G!\n";

    Widget popup = XtParent( (Widget) clientData);
    XtDestroyWidget(popup);
}

/*
 * Creates and pops up a Dialog widget (from a menu item!)
 * for inputing graph minor label.
 */
void popupAskLabel( Widget button, XtPointer, XtPointer)
{
    Arg		args[5];
    Widget	popup, dialog;
    Position	x, y;
    Dimension	width, height;
    Cardinal	n;

//cerr << "Begin PopupPrompt\n";

    /*
     * This will position the upper left hand corner of the popup at the
     * center of the widget which invoked this callback, which will also
     * become the parent of the popup.  I don't deal with the possibility
     * that the popup will be all or partially off the edge of the screen.
     */
    n = 0;
    XtSetArg(args[0], XtNwidth, &width); n++;
    XtSetArg(args[1], XtNheight, &height); n++;
    XtGetValues(button, args, n);

    XtTranslateCoords(button, (Position) (width / 2), (Position) (height / 2),
		      &x, &y);
    n = 0;
    XtSetArg(args[n], XtNx, x);	n++;
    XtSetArg(args[n], XtNy, y);	n++;

//cerr << "Begin to createShell\n";

    /*
     * Class "button" is not a subclass of a Core widget so use its parent
     * as the parent of our popup shell.
     */
    popup = XtCreatePopupShell("prompt", transientShellWidgetClass, 
		XtParent(button), args, n);

//cerr << "popup created\n";

    /* 
     * The popup will contain a dialog box, prompting the user for input. 
     */
    dialog = XtCreateManagedWidget("labelDialog", dialogWidgetClass,
	      popup, NULL, 0 );

//cerr << "dialog created\n";

    /*
     * The prompting message's size is dynamic; allow it to request resize. 
     */
    XawDialogAddButton(dialog, "ok", okayLabelInput, (XtPointer) dialog);
    XawDialogAddButton(dialog, "cancel", destroyPopupPrompt, (XtPointer)dialog);

//cerr << "dialog button created\n";

    /*
     * Popup and realize the dialog widget.
     */
    //XtPopup(popup, XtGrabNone);
    XtPopup(popup, XtGrabExclusive);
}

/***********************************************************************/

/*
 * WARNING! (this is hard coded strings from "IteratorType")
 */
#define NUM_ITER_ITEMS 7

static char *menu_entry_names[] = {
  "All", 
  "Minimal", 
  "UnnknownOrMinimal", 
  "Unknown", 
  "NonIrrelevant", 
  "Memory", 
  "Dirty",
};

static Pixmap mark;

/*
 * Select callback for Iterator selection.
 */
void iterSelected( Widget w, XtPointer number, XtPointer)
{
    cerr << "Iterator item " << (int) number << " or "<< XtName(w) 
	    << " has been selected.\n";

//    XFreePixmap(XtDisplay(B->topLevel), mark);

    XtDestroyWidget(XtParent(w));
}

/*
 * Creates and pops menu to select Search iterator.
 */
void popupSelectIter(Widget button, XtPointer clientdata, XtPointer)
{
    Browser *B = (Browser *) clientdata;

    Arg		args[5];
    Widget	menu, entry;
    Position	x, y;
    Dimension	width, height;
    Cardinal	n;

cerr << "Begin Popup List\n";

    n = 0;
    XtSetArg(args[0], XtNwidth, &width); n++;
    XtSetArg(args[1], XtNheight, &height); n++;
    XtGetValues(button, args, n);

    XtTranslateCoords(button, (Position) (width / 2), (Position) (height / 2),
		      &x, &y);

    n = 0;
    XtSetArg(args[n], XtNx, x);	n++;
    XtSetArg(args[n], XtNy, y);	n++;

cerr << "Begin to createShell\n";

    /*
     * Class "button" is not a subclass of a Core widget so use its parent
     * as the parent of our popup shell.
     */
    menu = XtCreatePopupShell("menu", simpleMenuWidgetClass, 
	    XtParent(button), args, n);

    /*
     * Create the bitmap for marking selected items.
     */
    mark = XCreateBitmapFromData(XtDisplay(B->top()),
                                 RootWindowOfScreen(XtScreen(B->top())),
                                 (char *)xlogo16_bits,
                                 xlogo16_width, xlogo16_height);

    for (int i = 0; i < NUM_ITER_ITEMS ; i++) 
     {
        char *item = menu_entry_names[i];

        if (i) // test
          XtSetArg(args[0], XtNleftBitmap, None);
	else
          XtSetArg(args[0], XtNleftBitmap, mark);

        XtSetArg(args[1], XtNleftMargin, 30); 

        entry = XtCreateManagedWidget(item, smeBSBObjectClass, menu,
                                       args, TWO);
        XtAddCallback(entry, XtNcallback, iterSelected, (XtPointer) i);
     }


cerr << "popup created\n";

    /*
     * Popup and realize the dialog widget.
     */
    XtPopup(menu, XtGrabExclusive);
}

