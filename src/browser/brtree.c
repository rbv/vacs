// ------------------------------------------
// ---------------- brtree.c ----------------
// ------------------------------------------
 
/*tex
 
\file{brtree.c}
\path{src/browser}
\title{Definitions of browser tree functions}
\classes{}
\makeheader
 
xet*/

#include "browser/brstate.h"
#include "browser/browser.h"

// this is how we find X coordinate
// in our tree drawing algorythm
#define POSX(m,i,w,n) (int)(m+i*(w-2*m)/n)

#define DISTANCE(x,y,r,s) (float)(1.0*(x-r)*(x-r)+1.0*(y-s)*(y-s))
#define CHECK(x,y,r,s,dist) (DISTANCE(x,y,r,s) < (dist*dist))

/*
 *  Draw browser.
 */

void Browser::DrawTree()
{

   char c;

   // clear the browse window
   XClearWindow(viewWindow.display, treeWindow.window);

   if(!toggleFlag) return;

   // is there a current node?
   //
   if ( ! bState->nodeAvailable() ) { cerr << "No current node\n"; return; }

   // get window sizes
   //
   XWindowAttributes win_attr;
   if(!XGetWindowAttributes(viewWindow.display, treeWindow.window,
                            &win_attr)) {
       cerr << "XGetWindowAttributes failure\n";
       return;
   }

   // get the node's number
   //
   NodeNumber node = bState->nodeNumber();

   // the default (no parent) node's position
   //
   int nodeX = treeWindow.viewMargin;
   int nodeY = win_attr.height/2;

   // does it have a parent?
   //
   if ( bState->node().hasParent() ) {

       // set parent node position
       //
       int parentx = treeWindow.viewMargin;
       int parenty = treeWindow.viewMargin;

       // get parent number
       //
       NodeNumber parent = bState->nodeParent();

       // if node has a parent, then it has siblings
       // get siblings
       //
       const Array<NodeNumber>& siblings = bState->nodeSiblings();

       // get number of siblings
       //
       int numSiblings = siblings.size();

       // draw siblings
       //
       int siblingX;
       int siblingY;
       for ( int i = 0; i < numSiblings; i++ )
       {
          // cout << siblings[i] << nl; // print the sibling number
          
          // this is a core of our tree drawing algorythm
          // find X and Y position
          siblingX = POSX(treeWindow.viewMargin,i,win_attr.width,numSiblings);
          siblingY = (int) win_attr.height/2; 

          // draw arcs from parent to sibling
          DrawLineInTree(siblingX, siblingY,parentx, parenty);

          // if a sibling is the current node do not draw but store X position
          //
          if ( siblings[i] == node ) nodeX = siblingX;
          else {
              // draw sibling node
              //
              DrawNodeInTree(siblingX, siblingY, FALSE);
              c = search.statusAsChar( siblings[i] );
              DrawStringInTree(siblingX, siblingY, &c);
              // if no child draw mark
              if ( ! search.hasChildren( siblings[i] ) )
                  DrawMarkInTree(siblingX, siblingY);
          }
       }

       // draw parent node
       //
       DrawNodeInTree(parentx, parenty, FALSE);
       c = search.statusAsChar( parent );
       DrawStringInTree(parentx, parenty, &c);
   }

   // does the node have children?
   //
   if ( bState->node().status().childrenComputed() ) {

       // get children
       //
       const Array<NodeNumber>& children = bState->nodeChildren();

       // get number of children
       //
       int numChildren = children.size();
 
       // draw children
       //
       int childX;
       int childY;
       for ( int i = 0; i < numChildren; i++ ) {
          // cout << children[i] << nl; // print the sibling number
          
          // find X and Y position
          //
          childX = POSX(treeWindow.viewMargin,i,win_attr.width,numChildren);
          childY = (int) win_attr.height - treeWindow.viewMargin; 

          // draw arc from the current node to its child
          //
          DrawLineInTree(childX, childY, nodeX, nodeY );

          // draw child node
          //
          DrawNodeInTree(childX, childY, FALSE);
          c = search.statusAsChar( children[i] );
          DrawStringInTree(childX, childY, &c);
          // if no child draw mark
          if ( ! search.hasChildren( children[i] ) )
                 DrawMarkInTree(childX, childY);
       }
   }

   // draw the current node
   //
   DrawNodeInTree(nodeX, nodeY, TRUE);
   c = search.statusAsChar( node );
   DrawStringInTree(nodeX, nodeY, &c);
}

/*
 *   Select node in the tree.
 */

nodeKey Browser::SelectNodeFromTree(int x, int y)
{
// This subroutine is almost exact copy of the DrawTree one but it
// does not draw anything yet returns the Node number that is close
// to x and y coordinates of a selected point

   // is there a current node?
   //
   if ( !bState->nodeAvailable()) { cerr << "No current node\n"; return -1; }

   // get window sizes
   //
   XWindowAttributes win_attr;
   if(!XGetWindowAttributes(viewWindow.display, treeWindow.window,
                            &win_attr)) {
       cerr << "XGetWindowAttributes failure\n";
       return -1;
   }

   // get the node's number
   //
   NodeNumber node = bState->nodeNumber();

   // the default (no parent) node's position
   //
   int nodeX = treeWindow.viewMargin;
   int nodeY = win_attr.height/2;

   // does it have a parent?
   //
   if ( bState->node().hasParent() ) {

       // set parent node position
       //
       int parentx = treeWindow.viewMargin;
       int parenty = treeWindow.viewMargin;

       // get parent number
       //
       NodeNumber parent = bState->nodeParent();

       // check if close enough to the selected point if yes return it
       //
       if(CHECK(x,y,parentx,parenty,treeWindow.selMargin))
                return parent.getKey();

       // if node has a parent, then it has siblings
       // get siblings
       //
       const Array<NodeNumber>& siblings = bState->nodeSiblings();

       // get number of siblings
       //
       int numSiblings = siblings.size();

       // check sibling position against our selected point
       //
       int siblingX;
       int siblingY;
       for ( int i = 0; i < numSiblings; i++ )
       {
          // cout << siblings[i] << nl; // print the sibling number

          // find X and Y position
          siblingX = POSX(treeWindow.viewMargin,i,win_attr.width,numSiblings);
          siblingY = (int) win_attr.height/2;

          // check if close enough to the selected point if yes return it
          //
          if(CHECK(x,y,siblingX,siblingY,treeWindow.selMargin))
               return siblings[i].getKey();

       }

   } else {
       // if the node is root check position too
       if(CHECK(x,y,treeWindow.viewMargin,(int) win_attr.height/2,
                treeWindow.selMargin))
                    return node.getKey();
   }

   // does the node have children?
   //
   if ( bState->node().status().childrenComputed() ) {

       // get children
       //
       const Array<NodeNumber>& children = bState->nodeChildren();

       // get number of children
       //
       int numChildren = children.size();

       // draw children
       //
       int childX;
       int childY;
       for ( int i = 0; i < numChildren; i++ ) {
          // cout << children[i] << nl; // print the sibling number

          // find X and Y position
          //
          childX = POSX(treeWindow.viewMargin,i,win_attr.width,numChildren);
          childY = (int) win_attr.height - treeWindow.viewMargin;

          // check if close enough to the selected point if yes return it
          //
          if(CHECK(x,y,childX,childY,treeWindow.selMargin))
                return children[i].getKey();
       }
   }

   // in case the node is not found it returns -1
   //
   return -1;
}

/*
 *  Find new node and update browser.
 */
void Browser::GetNewNodeFromTree(int x, int y)
{
   NodeNumber::nodeKey number = SelectNodeFromTree(x, y);
   if(number < 0) return;
   
   if ( bState->toViaKey( number ) ) xBrowser->updateScreen();
}

/*
 * Draw node
 */
void Browser::DrawNodeInTree(int x, int y, bool sel)
{
  if(sel)
  XFillArc(viewWindow.display, treeWindow.window, viewWindow.gc,
          (int) ( x-treeWindow.selMargin/2 ),
          (int) ( y-treeWindow.selMargin/2 ),
          treeWindow.selMargin+1,
          treeWindow.selMargin+1,
          0, 360*64 );
  else
  XDrawArc(viewWindow.display, treeWindow.window, viewWindow.gc,
          (int) ( x-treeWindow.selMargin/2 ),
          (int) ( y-treeWindow.selMargin/2 ),
          treeWindow.selMargin,
          treeWindow.selMargin,
          0, 360*64 );

}

/*
 * Draw Mark
 */
void Browser::DrawMarkInTree(int x, int y)
{
    XDrawLine(viewWindow.display, treeWindow.window, viewWindow.gc,
              x+treeWindow.selMargin, y+treeWindow.selMargin/2,
              x-treeWindow.selMargin, y+treeWindow.selMargin/2);
    XDrawLine(viewWindow.display, treeWindow.window, viewWindow.gc,
              x+treeWindow.selMargin, y+treeWindow.selMargin/2,
              x+treeWindow.selMargin/2, y);
    XDrawLine(viewWindow.display, treeWindow.window, viewWindow.gc,
              x-treeWindow.selMargin/2, y,
              x-treeWindow.selMargin, y+treeWindow.selMargin/2);
}

/*
 * Draw string
 */
void Browser::DrawStringInTree(int x, int y, char* string)
{
   XDrawImageString(viewWindow.display, treeWindow.window, viewWindow.gc,
                    x-3,
                    y+treeWindow.selMargin+7,
                    string,
                    (int) strlen(string));
}

/*
 * Draw line
 */
void Browser::DrawLineInTree(int sx, int sy, int dx, int dy)
{
    XDrawLine(viewWindow.display, treeWindow.window, viewWindow.gc,
              sx, sy,
              dx, dy);
}
