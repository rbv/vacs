/*
 * Specialized translation functions for browser.
 */

/*tex
 
\file{actions.c}
\path{src/browser}
\title{Specialized translation functions for browser}
\classes{}
\makeheader
 
xet*/

#include <iostream>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
//#include <X11/Shell.h>

//#include <X11/Xaw/Box.h>
//#include <X11/Xaw/Cardinals.h>
//#include <X11/Xaw/Command.h>
//#include <X11/Xaw/Dialog.h>

/***********************************************************************/

#include "browser/browser.h"

/*
 * Action routine called by user typeing '\n' key in the Node popup dialog box.
 */
void endNodeInput( Widget widget, XEvent*, String*, Cardinal* )
{
    //cerr << "action routine called\n";

    Widget dialog = XtParent( widget );
    okayKeyInput( (Widget) widget, (XtPointer) dialog, (XtPointer) NULL);
}

/*
 * Action routine called by user typeing '\n' key in the Minor.
 */
void endMinorInput( Widget widget, XEvent*, String*, Cardinal* )
{
    Widget popup = XtParent( XtParent( widget ) );
    XtDestroyWidget(popup);
}

/*
 * Activated by the user typeing '\n' key in a the Glue BPG box.
 */
void setGlue( Widget, XEvent*, String*, Cardinal* )
{
cerr << "Will glue something\n";
}

void treeGraphEvent( Widget, XEvent *event, String*, Cardinal* )
{
    xBrowser->GetNewNodeFromTree(event->xbutton.x, event->xbutton.y);
}

