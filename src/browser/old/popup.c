/*
 * An C++ example of how to create a popup dialog box.
 */

#include <stdio.h>
#include <stdlib.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

#include <X11/Xaw/Box.h>
#include <X11/Xaw/Cardinals.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Dialog.h>

static String fallback_resources[] = {
    "*input: True",
    "*go.label: Press to see Simple Popup Demonstration",
    "*allowShellResize: True",
    "*Dialog*value: ",
    "*Dialog.label: What color should the main button be?",
    "*Dialog*value.translations: #override \\n <Key>Return: Ok()",
    "*Dialog*label.resizable: TRUE",
    NULL,
};

void Ok( Widget, XEvent*, String*, Cardinal* );

XtActionsRec actionTable[] = {
   {"Ok", Ok}
};


/*	Function Name: Quit
 */
void Quit( Widget widget, XtPointer client_data, XtPointer call_data)
{
    XtDestroyApplicationContext(XtWidgetToApplicationContext(widget));
    exit(0);
}


/*	Function Name: DestroyPopupPrompt
 */
void DestroyPopupPrompt( Widget	widget,	XtPointer client_data, 
		    XtPointer call_data)
{
    Widget popup = XtParent( (Widget) client_data);
    XtDestroyWidget(popup);
}


/*	Function Name: ConvertColor
 */
int ConvertColor( Widget w, char * color_name)
{
  XrmValue from, to;

  from.size = strlen(color_name) + 1;  
  from.addr = color_name;

/*
 * This conversion accepts a colorname from rgb.txt, or a #rrrgggbbb 
 * rgb color definition, or the special toolkit strings "XtDefaultForeground" 
 * and "XtDefaultBackground".
 */
  XtConvert(w, XtRString, (XrmValuePtr) &from, XtRPixel, (XrmValuePtr) &to);
  if (to.addr == NULL) {
      return(-1);
  }

  return( (int) *((Pixel *) to.addr) );
}

/*	Function Name: ColorTheButton
 *	Arguments: w - *** UNUSED ***.
 *                 client_data - a pointer to the dialog widget.
 *                 call_data - *** UNUSED ***.
 */
void ColorTheButton(Widget w, XtPointer client_data, 
		XtPointer call_data)
{
    Widget dialog = (Widget) client_data;
    Widget button  = XtParent(XtParent(dialog)); 
    int pixel;
    String cname = XawDialogGetValueString(dialog);
    Arg	args[3];

    pixel = ConvertColor(button, cname);

    if (pixel > 0) {
	XtSetArg(args[0], XtNbackground, (Pixel) pixel);
	XtSetValues(button, args, ONE);
	DestroyPopupPrompt(NULL, (XtPointer) dialog, (XtPointer)NULL);
    }
    else {			/* pixel < 0  ==> error. */
	char str[BUFSIZ];
	(void) sprintf(str, "Can't get color \"%s\".  Try again.",  cname);
	   
	XtSetArg(args[0], XtNlabel, str);
	XtSetArg(args[1], XtNvalue, "");
	XtSetValues(dialog, args, TWO);
    }
}

/*	Function Name: Ok
 *	Description: An action routine that is invoked from any child of 
 *                   the dialog widget.  This routine will also color the
 *                   command button that activated the popup.
 *	Arguments: widget - the child of the dialog that caused this action.
 */
void Ok( Widget widget, XEvent *event, String *params,	
		Cardinal *num_params)
{
    Widget dialog = XtParent(widget);

    ColorTheButton(widget, (XtPointer) dialog, (XtPointer) NULL);
}

/*	Function Name: PopupPrompt
 *	Description: Creates and pops up the Dialog widget
 *	Arguments: button - the command button that activated this function.
 */
void PopupPrompt(Widget	button, XtPointer client_data, 
			XtPointer call_data)
{
    Arg		args[5];
    Widget	popup, dialog;
    Position	x, y;
    Dimension	width, height;
    Cardinal	n;

    /*
     * This will position the upper left hand corner of the popup at the
     * center of the widget which invoked this callback, which will also
     * become the parent of the popup.  I don't deal with the possibility
     * that the popup will be all or partially off the edge of the screen.
     */
    n = 0;
    XtSetArg(args[0], XtNwidth, &width); n++;
    XtSetArg(args[1], XtNheight, &height); n++;
    XtGetValues(button, args, n);
    XtTranslateCoords(button, (Position) (width / 2), (Position) (height / 2),
		      &x, &y);

    n = 0;
    XtSetArg(args[n], XtNx, x);				n++;
    XtSetArg(args[n], XtNy, y);				n++;

    popup = XtCreatePopupShell("prompt", transientShellWidgetClass, button,
			       args, n);

    /* 
     * The popup will contain a dialog box, prompting the user for input. 
     */
    dialog = XtCreateManagedWidget("dialog", dialogWidgetClass, popup,NULL, 0);

    /*
     * The prompting message's size is dynamic; allow it to request resize. 
     */
    XawDialogAddButton(dialog, "ok", ColorTheButton, (XtPointer) dialog);
    XawDialogAddButton(dialog, "cancel", DestroyPopupPrompt,(XtPointer)dialog);

    /*
     * In this example, it is not necessary to call XtRealizeWidget.
     * There are two situations where you may want to realize the
     * popup before calling XtPopup:
     *
     * If you build your popups upon application startup, the time required
     * to execute XtPopup is reduced if you realize your popups first.
     *
     * If you want to insure that the popup is not clipped by screen
     * boundaries, realize the popup before calling XtGetValues on it.
     *
     * The dialog popup will allow user input to the rest of the application
     * when the grab kind is GrabNone.   Since I allow input to the rest of
     * the application and since I don't destroy the popup before creating a
     * new one, I am allowing multiple identical popups, one for each time 
     * the user clicks on the main application button.
     *
     * Try it with XtGrabExclusive instead of XtGrabNone.  Will the user
     * be able to quit the application while the popup is up?  Does the
     * command button which causes the popup to happen remain highlighted?
     */
    XtPopup(popup, XtGrabExclusive);
}

/*
 * Main program
 */
main(int argc, char *argv[])
{
    Widget	toplevel, button, box;
    XtAppContext app_con;

    toplevel = XtAppInitialize(&app_con, "Popup", NULL, ZERO,
			       &argc, argv, fallback_resources,
			       NULL, ZERO);

    XtAppAddActions(app_con, actionTable, XtNumber(actionTable));

    /* Two buttons, with callback routines, in a box. */

    box = XtCreateManagedWidget("box", boxWidgetClass, toplevel, NULL, ZERO);

    button = XtCreateManagedWidget("go", commandWidgetClass, box, NULL, ZERO);
    XtAddCallback(button, XtNcallback, PopupPrompt, NULL);

    button = XtCreateManagedWidget("quit", commandWidgetClass, box, NULL,ZERO);
    XtAddCallback(button, XtNcallback, Quit, NULL);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app_con);
}
