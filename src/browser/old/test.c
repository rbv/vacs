/*
 * MD's test of the athena text widgets with various compilers.
 */


#include <stdio.h>
#include <stdlib.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>

#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Paned.h>

#include <X11/Xaw/Cardinals.h>

String fallback_resources[] = { 
    "*input:				True",
    "*showGrip:				off",
    "?.?.text.preferredPaneSize:	200", 
    "?.?.text.textSource.editType:	edit",
    "?.?.text.scrollVertical:		whenNeeded",
    "?.?.text.scrollHorizontal:		whenNeeded",
    "?.?.text.autoFill:			on",
    "?.?.text.width:			650",
    "*clear.label:            		Clear Text",
    "*print.label:            		Print Text",
    NULL,
};
 
static void ClearText(Widget w, XtPointer text_ptr, XtPointer call_data)
{
    Widget text = (Widget) text_ptr;
    Arg args[1];

    XtSetArg(args[0], XtNstring, "");
    XtSetValues(text, args, ONE);
}

static void PrintText(Widget w, XtPointer text_ptr, XtPointer call_data)
{
    Widget text = (Widget) text_ptr;
    Arg args[1];
    String str;

    XtSetArg(args[0], XtNstring, &str);
    XtGetValues(text, args, ONE);

    fprintf(stdout, "Text String is:\n--------\n%s\n--------\n", str);
}

/*  
 * MAIN program
 */
main(int argc, char **argv)
{
    XtAppContext app_con;
    Widget toplevel, paned, clear, print, text;
    Arg args[1];

    toplevel = XtAppInitialize(&app_con, "Xtext", NULL, ZERO,
			       &argc, argv, fallback_resources,
			       NULL, ZERO);

    paned = XtCreateManagedWidget("paned", panedWidgetClass, toplevel, 
				  NULL, ZERO);

    clear = XtCreateManagedWidget("clear", commandWidgetClass, paned, 
				  NULL, ZERO);

    print = XtCreateManagedWidget("print", commandWidgetClass, paned, 
				  NULL, ZERO);

    XtSetArg(args[0], XtNstring, 
"You're traveling through another dimension -- a dimension not only\n\
of sight and sound but of mind. A journey into a wonderous land whose\n\
boundaries are that of imagination.  That's a signpost up ahead: your\n\
next stop -- the Twilight Zone");

    text = XtCreateManagedWidget("text", asciiTextWidgetClass, paned, 
				 args, ONE);

    XtAddCallback(clear, XtNcallback, ClearText, (XtPointer) text);
    XtAddCallback(print, XtNcallback, PrintText, (XtPointer) text);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app_con);
}

