/*
 * C++ version of some examples of the Athena widgets.
 */

#include <stdio.h>
#include <stdlib.h>

#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>

#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/Box.h>
//#include <X11/Xaw/Clock.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Dialog.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/List.h>
//#include <X11/Xaw/Logo.h>
#include <X11/Xaw/MenuButton.h>
#include <X11/Xaw/Scrollbar.h>
#include <X11/Xaw/SimpleMenu.h>
#include <X11/Xaw/SmeBSB.h>
#include <X11/Xaw/SmeLine.h>
//#include <X11/Xaw/StripChart.h>
#include <X11/Xaw/Paned.h>
#include <X11/Xaw/Toggle.h>
#include <X11/Xaw/Viewport.h>
#include <X11/Xaw/Cardinals.h>

String fallback_resources[] = { 
    "*input:                  True",
    "*Paned.width:            350",
    "*label.label:            VACS Browser",
    "*Dialog.label:           Please enter a BPG in text form.",
    "*Dialog.value:           (1,2,12,1,3,13)",
//    "*Dialog*command*label:   ok",
    "*Dialog*resizable:       True",
//    "*Viewport*allowVert:     True",
    "*Form*formLabel.label:   0",
    "*Form*resizable:       True",
//    "*StripChart*update:      1",
//    "*StripChart*jumpScroll:  1",
    "*Box*allowResize:        True",
    "*scrollbar*orientation:  horizontal",
    "*scrollbar*length:       100",
    "*text*height:            75",
    "*text*string:            Look ma,\\na text widget!",
    "*text*editType:          edit",
    "*text*scrollVertical:    whenNeeded",
    "*text*scrollHorizonal:   whenNeeded",
//    "*textFile*type:          file",
//   "*textFile*string:        /etc/motd",
//    "*textFile*scrollVertical:    whenNeeded",
//    "*textFile*scrollHorizonal:   whenNeeded",
//    "*textFile*height:        75",
    NULL,
};

/*	Function Name: Count
 *	Description: This callback routine will increment that counter
 *                   and display the number as the label of the widget passed 
 *                   in the closure.
 *	Arguments: widget - *** UNUSED ***
 *                 closure - a pointer to the label widge to display the 
 *                           string in.
 *                 callData - *** UNUSED ***
 *	Returns: none
 */
static void Count( Widget widget, XtPointer closure, XtPointer callData)
{
   Arg arg[1];
   char text[10];
   static int count = 0;

   sprintf( text, " %d ", ++count );
   XtSetArg( arg[0], XtNlabel, text );
   XtSetValues( (Widget)closure, arg, ONE );
}

/*	Function Name: CreateFormWithButtons
 *	Description: Creates a form widget that contains:
 *                   a label, command, toggle, and menu button.
 *	Arguments: parent - the parent of the form widget.
 *	Returns: none.
 */
static void CreateFormWithButtons(Widget parent)
{
    Widget form, label, button, menu;
    int i;
    Arg args[1];

    form = XtCreateManagedWidget( "form", formWidgetClass, parent,
				 NULL, ZERO );

    label = XtCreateManagedWidget("formLabel",labelWidgetClass,form,NULL,ZERO);

    XtSetArg(args[0], XtNfromHoriz, label);
    button = XtCreateManagedWidget("command",commandWidgetClass,form,args,ONE);
    XtAddCallback( button, XtNcallback, Count, (XtPointer) label );

    XtSetArg(args[0], XtNfromHoriz, button);
    button = XtCreateManagedWidget("toggle", toggleWidgetClass, form,args,ONE);
    XtAddCallback( button, XtNcallback, Count, (XtPointer) label );

    XtSetArg(args[0], XtNfromHoriz, button);
    button = XtCreateManagedWidget("menuButton", menuButtonWidgetClass, form,
				   args, ONE);

    menu = XtCreatePopupShell("menu", simpleMenuWidgetClass, button,NULL,ZERO);

    for (i = 0; i < 5; i++) {
	char buf[BUFSIZ];
	sprintf(buf, "menuEntry%d", i + 1);
	(void) XtCreateManagedWidget(buf, smeBSBObjectClass, menu,
				     NULL, ZERO);
	if (i == 2)
	    (void) XtCreateManagedWidget("menuButton", smeLineObjectClass, 
					 menu, NULL, ZERO);
    }
}

/*	Function Name: Syntax
 *	Description: Prints a the calling syntax for this function to stdout.
 *	Arguments: app_con - the application context.
 *                 call - the name of the application.
 *	Returns: none - exits tho.
 */
static void Syntax(XtAppContext app_con, char *call)
{
    XtDestroyApplicationContext(app_con);
    fprintf(stderr, "Usage: %s\n", call);
    exit(1);
}

/*	Function Name: Quit
 *	Description: Destroys all widgets, and returns.
 *	Arguments: widget - the widget that called this callback.
 *                 closure, callData - *** UNUSED ***.
 *	Returns: none
 */
static void Quit( Widget widget, XtPointer closure, XtPointer callData )
{
    XtDestroyWidget((Widget)closure);
    exit(0);
}

/************************************************************************/

void main(int argc, char **argv)
{
    Widget toplevel, outer, dialog, quit /* ,box, list, viewport */;
    XtAppContext app_con;

    toplevel = XtAppInitialize(&app_con, "Xwidgets", NULL, ZERO,
			       &argc, argv, fallback_resources,
			       NULL, ZERO);

    if (argc != 1) Syntax(app_con, argv[0]);

    outer = XtCreateManagedWidget( "paned", panedWidgetClass, toplevel,
				  NULL, ZERO);

    XtCreateManagedWidget( "label", labelWidgetClass, outer, NULL, ZERO);
    
    quit = XtCreateManagedWidget( "quit", commandWidgetClass, outer, 
				 NULL, ZERO);
    XtAddCallback( quit, XtNcallback, Quit, (XtPointer) toplevel);

    dialog = XtCreateManagedWidget("dialog", dialogWidgetClass, outer, 
				   NULL, ZERO);

#if 0
    viewport = XtCreateManagedWidget( "viewport", viewportWidgetClass, outer,
				     NULL, ZERO);

    list = XtCreateManagedWidget("list", listWidgetClass, viewport, 
				 NULL, ZERO);
#endif

//    XawDialogAddButton(dialog, "command", Okay, (XtPointer) list);

    CreateFormWithButtons(outer);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app_con);
}

