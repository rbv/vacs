/*
 * Browser prototype Class.
 */

/*tex
 
\file{browser.c}
\path{src/browser}
\title{Browser prototype class}
\classes{Broswer}
\makeheader
 
xet*/

#include "browser/brstate.h"
#include "browser/browser.h"
#include "general/str.h"

/************************************************************************
 * Class Browser member functions.
 ************************************************************************/

/*
 * MAIN Browser Constructor.
 */
Browser::Browser(XtAppContext appCon, Widget top)
{
    appContext = appCon;
    topLevel = top;

    panedLayout = XtCreateManagedWidget( "paned", panedWidgetClass, 
			topLevel, NULL, ZERO);

    /*
     * Now build the rest of the widget tree from the root (topLevel)
     *
     * The first level is explicitly stated while the remaining level(s)
     * of widgets are created by calling other internal class functions.
     */
    titleLabel = XtCreateManagedWidget( "title", labelWidgetClass, 
			panedLayout, NULL, ZERO);

    topBox = XtCreateManagedWidget("line1", boxWidgetClass, 
			panedLayout, NULL, ZERO);

     /*
      * Proof button items in createTopBox() need to refrence this.
      */
     proofText = XtCreateManagedWidget("proof", asciiTextWidgetClass, 
			panedLayout, NULL, ZERO);

     /*
      * Create system commands and information line.
      */
     createTopBoxButtons();
    
     graphNumLine = XtCreateManagedWidget("graphNumLine", labelWidgetClass, 
			panedLayout, NULL, ZERO);

     minorLine = XtCreateManagedWidget("minorLine", labelWidgetClass, 
			panedLayout, NULL, ZERO);

     glueDialog = XtCreateManagedWidget("glueie", dialogWidgetClass, 
			panedLayout, NULL, ZERO);

     /*
      * Add a row of test buttons.
      */
     createBoxOfButtons();

     /*
      * Add another box of buttons
      */
     createNodeButtons();

     /*
      *  Initialization of the window for graph to browse.
      */
   viewTree = XtCreateManagedWidget("viewTree", coreWidgetClass, 
			panedLayout, NULL, ZERO);

   // view margin
   treeWindow.viewMargin = 20;
   // selection margin
   treeWindow.selMargin = 8;

     /*
      *  Initialization of the window for graph to draw.
      */

   viewGraph = XtCreateManagedWidget("viewGraph", coreWidgetClass, 
			panedLayout, NULL, ZERO);
     
     /*
      * Finally, we are ready to run.
      */
     toggleFlag = false;
     quitFlag = false;

     updateScreen();
}

/*
 * Create a quit button and current status labels.
 */
void Browser::createTopBoxButtons()
{
    quitCommand = XtCreateManagedWidget( "quit", commandWidgetClass,
			topBox, NULL, ZERO);

    infoCommand = XtCreateManagedWidget( "info", commandWidgetClass,
			topBox, NULL, ZERO);

    proofButton = XtCreateManagedWidget("proofButton", menuButtonWidgetClass,
			topBox, NULL, ZERO );

    createProofMenu();     

    keyLabel = XtCreateManagedWidget( "keyLabel", labelWidgetClass,
			topBox, NULL, ZERO);

     /*
      * Add Callback function for quit button on this widget level.
      */
     XtAddCallback( quitCommand, XtNcallback, quit, (XtPointer) this );

     XtAddCallback( infoCommand, XtNcallback, textdump, (XtPointer) this );
}

/*
 * Create system menu to read/write proof.
 */
void Browser::createProofMenu()
{
    proofMenu = XtCreatePopupShell("menu", simpleMenuWidgetClass,
			proofButton, NULL, ZERO);

    saveProof = XtCreateManagedWidget("saveProof", smeBSBObjectClass, 
			proofMenu, NULL, ZERO);

    rereadProof = XtCreateManagedWidget("rereadProof", smeBSBObjectClass, 
			proofMenu, NULL, ZERO);

    (void) XtCreateManagedWidget("horizontal", smeLineObjectClass,
			proofMenu, NULL, ZERO);

    deleteProof = XtCreateManagedWidget("deleteProof", smeBSBObjectClass, 
			proofMenu, NULL, ZERO);

    /*
     * Assign callback functions to these buttons.
     */
    XtAddCallback( saveProof, XtNcallback, proofSave, (XtPointer) proofText );
    //
    XtAddCallback( rereadProof, XtNcallback, proofReread, 
		    (XtPointer) proofText  );
    //
    XtAddCallback( deleteProof, XtNcallback, proofDelete, 
		    (XtPointer) proofText );
}

/*
 * Create two command buttons to dump proof and glueable BPG.
 */
void Browser::createBoxOfButtons()
{
    boxOfButtons = XtCreateManagedWidget("dump", boxWidgetClass, 
			panedLayout, NULL, ZERO);

    minorButton = XtCreateManagedWidget("minorButton", menuButtonWidgetClass,
			boxOfButtons, NULL, ZERO );

    createMinorMenu();

    glueButton = XtCreateManagedWidget("glue", commandWidgetClass,
			boxOfButtons, NULL, ZERO );

    viewToggle = XtCreateManagedWidget("viewToggle", toggleWidgetClass,
			boxOfButtons, NULL, ZERO );

    /*
     * Assign callback function to these buttons.
     */

    XtAddCallback( glueButton, XtNcallback, dumpGlue, (XtPointer) glueDialog);
    //
    XtAddCallback( viewToggle, XtNcallback, toggleView, (XtPointer) this );
}

/*
 * Create the TRAVERSE node menu.
 */
void Browser::createNodeButtons()
{
    bottomBox = XtCreateManagedWidget("menu", formWidgetClass, 
			panedLayout, NULL, ZERO);

    viaGraphNum = XtCreateManagedWidget("viaGraphNum", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    prevGraphNum = XtCreateManagedWidget("prevGraphNum", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    nextGraphNum = XtCreateManagedWidget("nextGraphNum", commandWidgetClass, 
			bottomBox, NULL, ZERO);

/*
    setIterator =  XtCreateManagedWidget("setIterator", commandWidgetClass, 
			bottomBox, NULL, ZERO);
*/

    toParent = XtCreateManagedWidget("toParent", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    firstChild = XtCreateManagedWidget("firstChild", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    lastChild = XtCreateManagedWidget("lastChild", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    prevSibling = XtCreateManagedWidget("prevSibling", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    nextSibling = XtCreateManagedWidget("nextSibling", commandWidgetClass, 
			bottomBox, NULL, ZERO);

    /*
     * Assign callback functions to these buttons.
     */
    XtAddCallback( viaGraphNum, XtNcallback, popupAskKey, (XtPointer) this );
    //
    XtAddCallback( prevGraphNum, XtNcallback, pGraphNum, (XtPointer) this );
    //
    XtAddCallback( nextGraphNum, XtNcallback, nGraphNum, (XtPointer) this );
    //
    XtAddCallback( toParent, XtNcallback, toCreator, (XtPointer) this );
    //
    XtAddCallback( firstChild, XtNcallback, fChild, (XtPointer) this );
    //
    XtAddCallback( lastChild, XtNcallback, lChild, (XtPointer) this );
    //
    XtAddCallback( prevSibling, XtNcallback, pSib, (XtPointer) this );
    //
    XtAddCallback( nextSibling, XtNcallback, nSib, (XtPointer) this );
}

/*
 * Create Minor assignment menu.
 */
void Browser::createMinorMenu()
{
    minorMenu = XtCreatePopupShell("menu", simpleMenuWidgetClass,
			minorButton, NULL, ZERO);

    viaMinorLabel = XtCreateManagedWidget("viaMinorLabel", smeBSBObjectClass, 
			minorMenu, NULL, ZERO);

    firstMinor = XtCreateManagedWidget("firstMinor", smeBSBObjectClass, 
			minorMenu, NULL, ZERO);

    lastMinor = XtCreateManagedWidget("lastMinor", smeBSBObjectClass, 
			minorMenu, NULL, ZERO);

    prevMinor = XtCreateManagedWidget("prevMinor", smeBSBObjectClass, 
			minorMenu, NULL, ZERO);

    nextMinor = XtCreateManagedWidget("nextMinor", smeBSBObjectClass, 
			minorMenu, NULL, ZERO);


    /*
     * Assign callback functions to these buttons.
     */
    XtAddCallback(viaMinorLabel, XtNcallback, popupAskLabel, (XtPointer) this);
    //
    XtAddCallback( firstMinor, XtNcallback, fMinor, NULL );
    //
    XtAddCallback( lastMinor, XtNcallback, lMinor, NULL );
    //
    XtAddCallback( prevMinor, XtNcallback, pMinor, NULL );
    //
    XtAddCallback( nextMinor, XtNcallback, nMinor, NULL );

}

/************************************************************************/

 // Private screen updates.
 

/*
 * Fast info line of node and minor keys.
 */
void Browser::_updateKeys()
 {
   Str keys("Node: ");

   if (bState->nodeAvailable())
    {
     Str num( asString( bState->nodeNumber() ) );
     keys.append(num);
    }
   else
     keys.append("<none>");

   keys.append("  Minor: ");

   if ( bState->minorAvailable() )
    {
     Str num( (bState->minor()).label() );
     keys.append(num);
    }
   else
     keys.append("<none>");

   /*
    * Now set the widget's label.
    */
   Arg arg[1];
   XtSetArg( arg[0], XtNlabel, (char*)(keys) );
   XtSetValues( keyLabel, arg, ONE );
 }

/*
 * Graph (node) info line. 
 */
void Browser::_updateGraph()
 {
   Str graph("Node: ");

   if (bState->nodeAvailable())
    {
     graph.append(asString( (bState->node()).graph() ));
     graph.append("\nStatus: ");
     graph.append( search.statusAsString(bState->nodeNumber()) );
    }
   else {
     graph.append("\nStatus:");
    }

   /*
    * Now set the widget's label.
    */
   Arg arg[1];
   XtSetArg( arg[0], XtNlabel, (char*)(graph) );
   XtSetValues( graphNumLine, arg, ONE );
 }

/*
 * Minor info line. 
 */
void Browser::_updateMinor()
 {
   Str mStr("Minor: ");

   if (bState->minorAvailable())
    {
     mStr.append(asString( bState->minor() ));
     mStr.append("\nStatus: ");
     mStr.append( (bState->minor()).statusAsString() );
    }
   else
     mStr.append("\nStatus:");

   /*
    * Now set the widget's label.
    */
   Arg arg[1];
   XtSetArg( arg[0], XtNlabel, (char*)(mStr) );
   XtSetValues( minorLine, arg, ONE );
 }


/*
 * Do the X-window processing.
 * (Display/Invoke screen & Handle events.)
 *
 * Does not return to caller! --- Not true if following hack works.
 */
void Browser::activate()
 {
 // cerr << "Now will Realize VACS browser" << nl;

 XtRealizeWidget( topLevel );


 // cerr << "Everything Realized" << nl;

 // Get the view graph data: window, display and graphics context
 viewWindow.window = (Window) XtWindow(viewGraph);
 viewWindow.display = (Display*) XtDisplay(viewGraph);
 viewWindow.gc = (GC) DefaultGCOfScreen(XtScreen(viewGraph));

 XSetFunction(viewWindow.display, viewWindow.gc, GXset);

 // Get the browse window Xlib data: window
 // The display and graphics context are shared with the view window.
 treeWindow.window = (Window) XtWindow(viewTree);

 // cerr << viewWindow.window << "\n" ;
 // cerr << treeWindow.window << "\n" ;


 // XtAppMainLoop( appContext );
 //
 while ( ! quitFlag )
   {
    XEvent event;
    XtAppNextEvent( appContext, &event );
    XtDispatchEvent( &event );
   }
 }

