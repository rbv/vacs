
/*
 * All the event/callbacks associated with the VACS browser.
 */

/*tex
 
\file{callbacks.c}
\path{src/browser}
\title{All the event/callbacks associated with the browser}
\classes{}
\makeheader
 
xet*/

#include <iostream>

#include "stdtypes.h"

#include "browser/brstate.h"
#include "browser/browser.h"

/****************************
 * Do something with proof. *
 ****************************/

/*
 * Save proof.
 */ 
void proofSave( Widget, XtPointer textPtr, XtPointer)
{
    Widget text = (Widget) textPtr;
    Arg args[1];
    String str;

    XtSetArg(args[0], XtNstring, &str);
    XtGetValues(text, args, ONE);

    cerr << "Proof Save:" << nl << str << nl;
}

/*
 * Reread proof.
 */ 
void proofReread( Widget, XtPointer textPtr, XtPointer)
{
    Widget text = (Widget) textPtr;
    Arg args[1];
    String str;

    XtSetArg(args[0], XtNstring, &str);
    XtGetValues(text, args, ONE);

    cerr << "Proof Recovered.\n";
}

/*
 * Delete proof.
 */ 
void proofDelete( Widget, XtPointer textPtr, XtPointer)
{
    Widget text = (Widget) textPtr;
    Arg args[1];
    String str = XtMalloc(1);
    str[0]=0;

    XtSetArg(args[0], XtNstring, str);
    XtSetValues(text, args, ONE);

    cerr << "Proof deleted.\n";
}

//---------------------------------------------------

/*
 * Do something with glue BPG.
 */
void dumpGlue( Widget, XtPointer textPtr, XtPointer)
{
    Widget text = (Widget) textPtr;
    Arg args[1];
    String str;

    XtSetArg(args[0], XtNvalue, &str);
    XtGetValues(text, args, ONE);

    cerr << "BPG graph: " << str << nl;
}

//---------------------------------------------------

/*
 * The viewToggle button has been pressed.
 */
void toggleView( Widget, XtPointer browser, XtPointer)
{
//    cerr << "ViewToggle pressed." << nl;
    Browser *B = (Browser *) browser;
    B->ToggleFlag();
    B->updateMinor();
//    B->DrawTree();
//    B->DrawGraph();
}

//---------------------------------------------------

/*
 * Destroys all widgets, and returns.
 */
void quit( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    XtDestroyWidget( B->topLevel );

    B->quitFlag = true;
 }

/*
 * Do a textdump on the current node.
 */
void textdump( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;
   
    cerr << bState->node();
 }

/************************************************************************/

/*
 * Traverse searchnode callbacks. (see Popups for key# traverse)
 */
void pGraphNum( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toPrev())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no previous node under current Iterator!\n";
 }
//
void nGraphNum( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toNext())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no next node under current Iterator!\n";
 }
//
void toCreator( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toParent())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no parent node available!\n";
 }
//
void fChild( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toFirstChild())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no children!\n";
 }
//
void lChild( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toLastChild())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no children!\n";
 }
//
void pSib( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toPrevSib())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no previous sibling available!\n";
 }
//
void nSib( Widget, XtPointer browser, XtPointer)
 {
    Browser *B = (Browser *) browser;

    if (bState->toNextSib())
     {
      B->updateScreen();
     }
    else cerr << "Sorry, no next sibling available!\n";
 }

/************************************************************************/

/*
 * Traverse minor callbacks. (see Popups for label# traverse)
 */
void fMinor( Widget, XtPointer, XtPointer)
 {
    if (bState->toFirstMinor())
     {
      xBrowser->updateMinor();
     }
    else cerr << "Sorry, no minors available!\n";
 }
//
void lMinor( Widget, XtPointer, XtPointer)
 {
    if (bState->toLastMinor())
     {
      xBrowser->updateMinor();
     }
    else cerr << "Sorry, no minors available!\n";
 }
//
void pMinor( Widget, XtPointer, XtPointer )
 {
    if (bState->toPrevMinor())
     {
      xBrowser->updateMinor();
     }
    else cerr << "Sorry, no previous minor available!\n";
 }
//
void nMinor( Widget, XtPointer, XtPointer )
 {
    if (bState->toNextMinor())
     {
      xBrowser->updateMinor();
     }
    else cerr << "Sorry, no next minor available!\n";
 }
