/*
 * Browser state class (external file class routines).
 */

/*tex
 
\file{brstate.c}
\path{src/browser}
\title{Browser state class for external searchnode access.}
\classes{}
\makeheader
 
xet*/

#include <iostream>

#include "browser/brstate.h"
#include "search/search.h"


BrowserState *externalInit(int argc, char *argv[])
 {
   /*
    * Look for alternative driver filename.
    */
   char *driverFilename;

   if (argc > 1 && argv[1][0]!='-') driverFilename = argv[1];

   /*
    * Initialize runInfo by having it read the driver file
    * this call will not return if there is a problem
    */
   runInfo.initialize( driverFilename, RunInfo::Browser );

   // Tell search to load the search node status from the filesystem.
   //
   search.updateFromDisk();

   return new BrowserState();
 }

//-----------------------------------------------

void BrowserState::initMembers()
{
   sNode = 0;
   sNodeNum = 0;
   sMinor = 0;
}

//-----------------------------------------------

void BrowserState::update( NodeNumber n )
{

   // init all fields to 0
   //
   initMembers();

   sNode = search.node( n );
   sNodeNum = n;

   assert( sNode );

   delete sMinorIter;
   sMinorIter = 0;

   if ( sNode->status().minorsComputed() )
   {
      sMinorIter = new SearchMinorIterator( sNode->minors() );
      sMinor = sMinorIter->next();
   }

   NodeNumber oldParent = parent;

   parent = sNode->parent();

   if ( parent.isInvalid() )
   {
      delete siblings;
      siblings = 0;
   }

   // only get a new iterator if parent changes
   //
   if ( ! parent.isInvalid() && parent != oldParent )
   {
      delete siblings;

      if ( search.node(parent)->status().childrenComputed() )
      {
         siblings =
            new NodeNumberCollectionIter( search.node(parent)->children() );
         _siblings = search.node(parent)->children().asArray();
      }
      else
      {
         siblings = 0;
         _siblings.resize(0);
      }

   }

   if ( sNode->status().childrenComputed() )
      _children = sNode->children().asArray();
   else
      _children.resize(0);


/*
   // debug code

   if ( sNode->status().childrenComputed() )
      cerr << "Children: " << _children << nl;
   else
      cerr << "no children" << nl;

   if ( sNode->hasParent() )
      cerr << "Siblings: " << _siblings << nl;
   else
      cerr << "no siblings" << nl;
*/

   // Update the iterator!!
   //
   nodeIter.moveToNode( n );
}

//-----------------------------------------------

bool BrowserState::toParent()
{
   assert( sNode );

   if ( parent.isInvalid() )
      return false;

   update( parent );

   return true;
}


bool BrowserState::toFirstChild()
{
   if ( ! sNode->status().childrenComputed() ) return false;

   NodeNumberCollectionIter I( sNode->children() );

   NodeNumber n = I();

   if ( n.isInvalid() ) return false;

   update( n );

   return true;
}


bool BrowserState::toLastChild()
{
   if ( ! sNode->status().childrenComputed() ) return false;
 
   NodeNumberCollectionIter I( sNode->children() );

   I.end();
 
   NodeNumber n = I.prev();
 
   if ( n.isInvalid() ) return false;
 
   update( n );
 
   return true;
}


bool BrowserState::toNextSib()
{
   if ( !siblings ) return false;

   NodeNumber n = siblings->next();

   if ( n.isInvalid() ) return false;

   update( n );

   return true;
}


bool BrowserState::toPrevSib()
{
   if ( !siblings ) return false;

   NodeNumber n = siblings->prev();

   if ( n.isInvalid() ) return false;

   update( n );

   return true;
}


bool BrowserState::toFirstMinor()
{
   if ( ! sMinorIter ) return false;

   sMinorIter->start();

   SearchMinor* m = sMinorIter->next();

   if ( m )
   {
      sMinor = m;
      return true;
   }

   return false;
}


bool BrowserState::toLastMinor()
{
   if ( ! sMinorIter ) return false;

   sMinorIter->end();

   SearchMinor* m = sMinorIter->prev();

   if ( m )
   {
      sMinor = m;
      return true;
   }

   return false;
}


bool BrowserState::toNextMinor()
{
   if ( ! sMinorIter ) return false;

   SearchMinor* m = sMinorIter->next();
 
   if ( m )
   {
      sMinor = m;
      return true;
   }
 
   return false;
}


bool BrowserState::toPrevMinor()
{
   if ( ! sMinorIter ) return false;

   SearchMinor* m = sMinorIter->prev();
 
   if ( m )
   {
      sMinor = m;
      return true;
   }
 
   return false;
}


bool BrowserState::toNext()
{
   NodeNumber n = nodeIter.next();

//cerr << "next node " << n << nl;

   if ( n.isInvalid() ) return false;

   update( n );

   return true;
}



bool BrowserState::toPrev()
{
   NodeNumber n = nodeIter.prev();

   if ( n.isInvalid() ) return false;

   update( n );

   return true;
}


bool BrowserState::toViaKey( NodeNumber::nodeKey key )
{
   NodeNumber n( key );
 
   if ( n.isInvalid() ) return false;

   if ( search.isInvalid( n ) ) return false;
 
   update( n );
 
   return true;

}
