/*
 * Browser class main BROWSE program.
 */

/*tex
 
\file{browsermain.c}
\path{src/browser}
\title{Browser class main browse program}
\classes{}
\makeheader
 
\begin{desc}
Two global browser classes are defined for a X-window interface to
VACS.

\noindent {\bf class BrowserState *bState;}  

\noindent {\bf class Browser *xBrowser;}

\end{desc}
xet*/

#include <iostream>

#include "browser/browser.h"

/*
 * For people who don't want to hassle with resource files.
 */
String fallbackResources[] = { 
    "*BitmapFilePath: /net/cmc6/hannah/a/cdf/vacs/inc/bitmaps",
    "*Paned.width:            	600",
    "*title.label:            	VACS Browser",
    "*title.font: *-helvetica-bold-r-normal--*-120-*-*-*-*-*-*",
//    "*keyLabel.label: 		Node: <none>  Minor: <none>",
    "*keyLabel.borderWidth: 	0",

    "*proofButton.label:		proof",
    "*proofButton*saveProof.label:	Save",
    "*proofButton*rereadProof.label:	Reread",
    "*proofButton*deleteProof.label:	Delete",

    "*proof*preferredPaneSize: 100",
//    "*proof*string:            A human proof/comments will be entered here!",
    "*proof*editType:          edit",
    "*proof*scrollVertical:    whenNeeded",
    "*proof*scrollHorizontal:  whenNeeded",
    "*proof*autoFill:          on",

//    "*graphNumLine.label:	Node: <none> \\nStatus:",
    "*graphNumLine.justify:	left",
//    "*graphNumStat.label:	Status: <none>",
//    "*minorLine.label:		Minor: <none> \\nStatus:",
    "*minorLine.justify:	left",
//   "*minorStat.label:		Status: <none>",

    "*glueie.label:           	Please enter a glueable BPG in text form.",
    "*glueie.value:           	",
    "*glueie.resizable:       	True",
//    "*glueie*translations:	#override \\n <Key>Return: setGlue()",

    "*viaGraphNum.label:	#",
    "*prevGraphNum.label:	P",
    "*nextGraphNum.label:	N",
    "*prevGraphNum.horizDistance:  5",
    "*prevGraphNum.vertDistance:  80",
    "*nextGraphNum.horizDistance:  30",
    "*nextGraphNum.vertDistance:  80",
    "*viaGraphNum.horizDistance:  60",
    "*viaGraphNum.vertDistance:  80",

//    "*nodeButton*setIterator*menu*HorizontalMargins: 30",

    "*toParent.bitmap:	arrow_up",
    "*firstChild.bitmap:	arrow_down_left",
    "*lastChild.bitmap:		arrow_down_right",
    "*nextSibling.bitmap:	arrow_right",
    "*prevSibling.bitmap:	arrow_left",
    "*toParent.horizDistance:   100",
    "*toParent.vertDistance:    5",
    "*prevSibling.horizDistance:  50",
    "*prevSibling.vertDistance:  27",
    "*nextSibling.horizDistance:  150",
    "*nextSibling.vertDistance:  27",
    "*firstChild.horizDistance:  85",
    "*firstChild.vertDistance:  50",
    "*lastChild.horizDistance:  120",
    "*lastChild.vertDistance:  50",

    "*keyDialog.value: 			",
    "*keyDialog.label:			Enter a NodeNum",
    "*keyDialog*translations:	#override \\n <Key>Return: endNodeInput()",

    "*minorButton.label:	    	Pick Minor",
    "*minorButton*viaMinorLabel.label:	Via Label #",
    "*minorButton*firstMinor.label:	First Minor",
    "*minorButton*lastMinor.label:	Last Minor",
    "*minorButton*nextMinor.label:	Next Minor",
    "*minorButton*prevMinor.label:	Prev Minor",
    "*labelDialog.value:		",
    "*labelDialog.label:		Enter a minor label",
    "*labelDialog*translations:	#override \\n <Key>Return: endMinorInput()",
    "*viewTree.width: 600",
    "*viewTree.height: 200",
    "*viewTree*translations: #override \\n <Btn1Up>: treeGraphEvent()",
    "*viewGraph.width: 600",
    "*viewGraph.height: 100",

    NULL,
};

/*
 * We use some actions for faster entry of parameters in dialog boxes
 * and for event in the Tree window.
 */
XtActionsRec actionTable[] = {
   {"endNodeInput", endNodeInput},
   {"endMinorInput", endMinorInput},
   {"treeGraphEvent", treeGraphEvent},
   {"setGlue", setGlue}
};

extern BrowserState *externalInit(int argc, char *argv[]);

/*
 * Global browser classes.
 */
class BrowserState *bState;
//
class Browser *xBrowser;

/*
 * Our simple bootstrap program.
 */
int main(int argc, char *argv[])
{
    /*
     * Initialize runInfo and filesystem and
     * build the browser external data structure.
     */
    bState = externalInit(argc, argv);

    Widget toplevel;
    XtAppContext app_con;

    // cerr << "Create top widget\n";
    toplevel = XtAppInitialize(&app_con, "Browser", NULL, ZERO,
		      &argc, argv, fallbackResources, NULL, ZERO);

    // cerr << "Get application resources\n";
    XtAppAddActions(app_con, actionTable, XtNumber(actionTable));

    /*
     * Do our thing by creating X-window interface class.
     */
    // cerr << "Get browser data\n";
    xBrowser = new Browser(app_con, toplevel);

    // cerr << "Will now activate the Babe!\n";

    xBrowser->activate();

//    cerr << "Now ya all have a pleasant day.\n";

    XtDestroyApplicationContext( app_con );

    //delete xBrowser;
    //delete bState;
    return 0;
}


