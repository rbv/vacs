
// -------------------------------------------
// -------------- sm_manager.c ---------------
// -------------------------------------------

/*tex
 
\file{sm_manager.c}
\path{src/searchmain}
\title{File and system manager for search}
\classes{}
\makeheader

xet*/

#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "general/stream.h"
#include "general/system.h"
#include "general/dtime.h"
#include "general/gtimers.h"
#include "search/searchmain.h"
#include "search/searchutil.h"
#include "search/nodenum.h"
#include "vacs/message.h"

#include "family/family.h"
#include "family/probinfo.h"
#include "bpg/bpgutil2.h"
#include "search/search.h"
#include "search/nodeinfo.h"
#include "search/proof_db.h"
#include "search/searchminor.h"
#include "vacs/runinfo.h"
#include "general/random.h"
#include "vacs/filesystem.h"
#include "error.h"

#undef loop

// number of msecs to wait for read activity (0 for no wait)
//
#define POLL_TIMEOUT 0

// msec to sleep while waiting for workers
//
#define IDLESLEEP 2

// How often (in minutes) to flush (fsync) DBs and check number of complete search tree levels
// (frees unneeded graph databases cache entries)
//
#define TIMER 60

//-------------------------------------------------------

class Manager
{
  static bstream cmdFile;
  static Timer timer;
  static double growTime;  // seconds spent growing nodes
  static double mainLoopTime;  // seconds spent in main loop
  static double idleTime;  // seconds spent idling while not paused
  static double idlePausedTime;  // seconds spent idling while paused
  static bool verbose;
  static bool requestFlush;  // want to flush the DBs
  // The following are NO LONGER USED...
  static bool resetGrowIter;
  static bool growType;       // true = pick lowest unfinished level, false = pick highest unfinished level
  static int growTargetLevel; // set by findTargetLevel, -1 = none
  static int growTargetLevelCount;  // " -- number of growable minimal nodes on target level
  static NodeNumber targetSearchStartNode; // set by searchComplete -- first unknown or ungrown min node
  // ...End no longer used members
  static bool sendPMP;
  static bool sendMembership;

  static void followToUnknownNode( NodeNumber &n, SearchNode &sn );
  template<class ProofClass>
  static void updateProof( NodeNumber n, ProofClass &proof, SearchNode &sn );
  template<class ProofClass>
  static bool updateNodeCommon( NodeNumber &n, SearchNode &sn, ProofClass &proof );
  template<class ProofClass>
  static void updateNode( NodeNumber n, ProofClass &proof );
  static void updateNode( NodeNumber n, MinimalProof &proof, PartialMinorProof &pmp );

  static void minorsDistinguishedByRandomSearch();
  static void minorsDistinguishedByTestset();
  static void minorCongruentByTestset();
  static void minorCongruentViaDP();
  static void minimalByTightCong();
  static void minimalByOOF();
  static void nonminimalByOOF();
  static void giveUp();
  static void irrelevantByENoncanonic();
  static void irrelevantByHNoncanonic();

  static void clearCompleteLevel();
  static NodeNumber chooseMinimalGrow( bool resetIter );
  static bool searchComplete();
  static bool notInControl();
  static void growFromNode( NodeNumber n, Array<NodeNumber>& unknowns );
  static void findGrowTargetLevel();

public:

  static char server[STRMAX];
  static int  portNum;
  static bool preloadProofs;
  static NBBPG limitToPrefix;

  static void processLoop();

  static void initialize(int fd);
  static void stopSignal(int);
  static void usr1Signal(int);

  static void statistics();
};

bstream Manager::cmdFile;
char Manager::server[STRMAX];
int Manager::portNum;
NBBPG Manager::limitToPrefix;
bool Manager::preloadProofs;
bool Manager::resetGrowIter;
bool Manager::growType;
bool Manager::sendPMP;
bool Manager::sendMembership;
bool Manager::verbose;
bool Manager::requestFlush;
int Manager::growTargetLevel;
int Manager::growTargetLevelCount;
NodeNumber Manager::targetSearchStartNode;
double Manager::growTime;
double Manager::mainLoopTime;
double Manager::idleTime;
double Manager::idlePausedTime;
Timer Manager::timer;

//-------------------------------------------------------

void Manager::initialize(int fd) 
{ 
  cmdFile.openOnFD( fd, 1 << 16, true );
  while (!Message::getServerNames( server, portNum )) sleep(2);

  sendPMP = ProbInfo::getBool( ProbInfo::MessagesContainPMP );
  sendMembership = ProbInfo::getBool( ProbInfo::WorkersDoMembership );
  verbose = ProbInfo::getBool( ProbInfo::VerboseNodeLogging );

  if ( preloadProofs )
  {
    // No longer supported
    aassert(false);
    /*
    cerr << "*** Manager loading proofs..." << nl;

    bool loadAllNonmin = ProbInfo::getBool( ProbInfo::LoadAllNonminimal );

    BPG::fromString( ProbInfo::getString( ProbInfo::LimitToPrefix ), limitToPrefix );

    cerr << "Prefix limit: " << limitToPrefix << nl;

    int level = ProbInfo::getInt( ProbInfo::MaxEqualityIsoLevel ) 
                + Operator::boundarySize();
    cerr << "       no iso for up to length " << level << nl;

    bistream mProofs( "minimalProofs.sav" );
    if ( mProofs.fail() ) cerr << "Minimal data not found" << nl;
    else search.minimalProofDatabase().loadFromStream( mProofs, level, limitToPrefix );

    bistream nProofs( "nonminimalProofs.sav" );
    if ( nProofs.fail() ) cerr << "Nonminimal data not found" << nl;
    else search.nonminimalProofDatabase().loadFromStream( nProofs, level, limitToPrefix, loadAllNonmin );

    cerr << "...load complete" << nl;

    search.flush();
    */
  }

  resetGrowIter = true;
  growType = true; 
  timer.set( TIMER );
  growTime = 0.0;
  mainLoopTime = 0.0;
  idleTime = 0.0;
  idlePausedTime = 0.0;
}

void Manager::statistics()
{
    search.statistics( cout );
    cerr << search.numberOfNodes() << " nodes" << nl;
    cerr << SearchNodeIter::iterations << " SearchNodeIter steps" << nl;
    Gtimers::print( cout, 25 );
    cerr << "Spent " << mainLoopTime << "s in main loop" << nl;
    cerr << "Spent " << growTime << "s growing nodes" << nl;
    cerr << "Spent " << idleTime << "s idling waiting for level, " << idlePausedTime << "s idling waiting for resume" << nl;
}

void Manager::stopSignal(int)
{
   search.stopReceived = true;
}

void Manager::usr1Signal(int)
{
    Manager::requestFlush = true;
    signal(SIGUSR1, Manager::usr1Signal);
}

//--------------------------------------------------

// NO LONGER USED
void Manager::findGrowTargetLevel()
{
  SearchNodeIter iter(SearchNodeIter::Minimal,SearchNodeIter::Growable);

  iter.moveToNode( targetSearchStartNode );
  iter.forceMove(-1);

  // Find either the lowest (growType true) or the highest level with unfinished nodes
  int target = growType ? 9999 : -9999;
  NodeNumber n;
  int count = 0; // num on target level
  while( ( n = iter() ).isValid() )
  {
    int d = search.node(n).depth();
    if ( d == target ) count++; 
    else if ( growType && d < target ) { target=d; count=1; }
    else if ( !growType && d > target ) { target=d; count=1; }
  }

  if ( target == 9999 || target == -9999 )
    growTargetLevel = -1; // none found
  else
    growTargetLevel = target;

  growTargetLevelCount = count;

}

//--------------------------------------------------

void Manager::growFromNode( NodeNumber n, Array<NodeNumber>& unknowns )
{

   //cout << "Growing node: " << n << " (at depth " << search.node(n).depth() << ")\n";
   cout.flush();

   static int resetCount = 200;
   static int maxIsoLevel = -99;
   if ( maxIsoLevel == -99 )
      maxIsoLevel = ProbInfo::getInt( ProbInfo::MaxEqualityIsoLevel );

   bool assumeNC = false;
   int depth = search.node( n ).depth();
   if ( depth <= maxIsoLevel-1 )
      { assumeNC = true; }

   //log() << "Growing node: " << n << nl;
   assert( search.isValid( n ) );
   growNode( n, &unknowns, assumeNC, &(Manager::limitToPrefix) );

   resetCount--;
   if ( resetCount == 0 )
   {
      growType = !growType; // reverse
      resetCount = 200;
      resetGrowIter = true; // flag to chooseMinimalGrow
   }
}

//-------------------------------------------------------

// Look for a growable minimal node.
//
NodeNumber Manager::chooseMinimalGrow( bool /* resetIter */ )
{
   //log() << "Looking for minimal grow node...";

   static SearchNodeIter* mIter = 0;
   static SearchNodeIter* uIter = 0;
   static int lastCompleteLevel = -1;
   static NodeNumber prevFirstU;
   static NodeNumber prevFirstM;
   static bool checkUnk = true;
   static RandomInteger* rnd;
   int uLevel;

   // We iterate over a slice (subrange) of the list of all
   // NodeNumbers, starting from a lower bound prevFirstM on the
   // NodeNumber of the minimum growable node.

   if ( !mIter )
   { 
      // init code 
      mIter =
        new SearchNodeIter( SearchNodeIter::Minimal, SearchNodeIter::Growable );
      uIter =
        new SearchNodeIter( SearchNodeIter::Unknown, SearchNodeIter::Growable );

      prevFirstM = NodeNumber(0);
      prevFirstU = NodeNumber(0);

      // This option no longer exists, so 'checkUnk == true' block is orphaned. Old description:
      //   Don't do isomorphism testing between SearchNodes or save iso info in databases. Instead
      //   depend on workers testing canonicity thoroughly (Note: this doesn't affect worker behaviour!)
      //   Also changes grow node selection behaviour in some strange way (see Manager::chooseMinimalGrow)

      //if ( ProbInfo::getBool( ProbInfo::WorkersTestFullCanonic ) )
      //  checkUnk = false;

      rnd = new RandomInteger( 5 );
   }

  if ( !checkUnk )   // OBSOLETE: never taken
  {
    // Not doing node isomorphism testing... I don't know why
    // this is specific to that case!

    static bool growSkipFlag; // inited by initial resetGrowIter
    static int growSkip;

    for( int loop=0; loop<2; loop++ ) // execute at most 2 times
    {
      if (resetGrowIter)
      {
        // Pick level to grow nodes on
        findGrowTargetLevel();
        // If there are plenty available, randomize choice a bit
        growSkipFlag = ( growTargetLevelCount > 200 );
        growSkip = growSkipFlag ? (*rnd)() : 0;

        cerr << "Grow target level: " << growTargetLevel 
             << " (has " << growTargetLevelCount << " nodes)"
             << " skip=" << growSkip
             << " start=" << targetSearchStartNode
             << nl;
        mIter->end();
        resetGrowIter=false;
      }

      int skip = growSkip;
      while( 1 )
      {
        NodeNumber n = mIter->prev();
        if ( n.isInvalid() ) break;
        if ( search.node(n).depth() != growTargetLevel ) continue;
        // Randomize choice
        if ( skip-- == 0 ) return n;
      }

      // nothing on target level - reset and try again
      resetGrowIter = true;
    }

    return NodeNumber(); // nothing to do!
  }

   mIter->moveToNode( prevFirstM );
   mIter->forceMove( -1 );  // consider also prevFirstM
   //mIter->restart();
   NodeNumber n = mIter->next();

   if ( n.isInvalid() ) return n;

   // where's the 1st unk
   uIter->moveToNode( prevFirstU );
   uIter->forceMove( -1 );
   //uIter->restart();
   NodeNumber firstU = uIter->next();
   if ( firstU.isValid() )
   {
     uLevel = search.node(firstU).depth();
     prevFirstU = firstU;
   }
   else
   {
     uLevel = 100000; // infinity
     prevFirstU = search.numberOfNodes();
   }

   const SearchNode& sn = search.node( n );
   int level = sn.depth();

   // If there's an unknown on this level then we can't
   // set prevFirstM = m, and have to wait.
   // NOTE: I don't see why we can't return n without
   // updating prevFirstM.
   if ( level >= uLevel ) return NodeNumber();

   prevFirstM = n;
   return n;
}

#if 0 
// old code
static NodeNumber chooseMinimalGrow( bool resetIter )
{
   //log() << "Looking for minimal grow node...";

   static SearchNodeIter* mIter = 0;
   static SearchNodeIter* uIter = 0;
   static int lastLevel = -1;

   if ( !mIter )
   { 
      mIter =
        new SearchNodeIter( SearchNodeIter::Minimal, SearchNodeIter::Growable );
      uIter =
        new SearchNodeIter( SearchNodeIter::Unknown );
   }

   resetIter = true; // small problems right now...
   if ( resetIter ) mIter->restart();
   
   NodeNumber n = mIter->next();

   if ( n.isInvalid() ) return n;

   const SearchNode& sn = search.node( n );
   int level = sn.depth();
   aassert( level >= lastLevel );
   if ( level == lastLevel ) return n;

   // ok, no minimal's left on this level
   uIter->restart();
   NodeNumber m = uIter->next();

   // if no unknowns anywhere or first unknown is on diff level, then done
   if ( m.isInvalid() || search.node(m).depth() > lastLevel )
   { 
      log() << "Level " << lastLevel << " complete" << nl;

      lastLevel = level;

      cerr << "Level complete: " << n << " depth " << level << nl;
      cerr << "   last level = " << lastLevel << nl;
      log() << "   DB cleared" << nl;

      return n;
   }

   // there's an unknown on this level...have to wait
   return NodeNumber();
}
#endif

bool Manager::searchComplete()
{
   SearchNodeIter iter( SearchNodeIter::Unknown );
   NodeNumber firstU = iter.next();

   SearchNodeIter iter2( SearchNodeIter::Minimal, SearchNodeIter::Growable );
   NodeNumber firstM = iter2.next();

   if ( firstU.isInvalid() ) targetSearchStartNode = firstM;
   else if ( firstM.isInvalid() ) targetSearchStartNode = firstU;
   else targetSearchStartNode = NodeNumber(min(firstU.asInt(),firstM.asInt()));

   if ( firstU.isValid() ) return false;
   if ( firstM.isValid() ) return false;
   return true;
}

bool Manager::notInControl()
{
  int port;

  if ( ! Message::getServerNames( Manager::server, port ) ) return true;

  return port != Manager::portNum;
}

//-------------------------------------------------------

#if 0
static void exploitObstructionPrefixes( 
	const RBBPG& graph,
	const NBBPG& extension )
{
   FileManager::lockRequest( LSysW );

   log() << "Exploiting..." << nl
         << "   graph: " << graph << nl
         << "   extn:  " << extension << nl;

   RBBPG G( graph, extension );
   obstMinimize( G );
   G.compress();
   int gLen = G.length();

   RBBPG H( G );
   int len = gLen;

   assert( ! Family::member( H ) );

   log() << "   obstr" << H << nl;

   MinimalProofDatabase& mdb = search.minimalProofDatabase();

   if ( mdb.checkForIsomorphicEntry( H ) != -1 )
   {
      log() << "   obst already present" << nl;
      FileManager::lockRequest( USysW );
      return;
   }

   MinimalProof proof( MinimalProof::OutOfFamily );
   mdb.add( proof, H );

   while(1)
   {
      len--;
      H.resize( len );
      H.compress();

      log() << "   doing: " << H << ", length " << len << nl;

      if ( ! H.isValid() ) { log() << "   invalid" << nl; break; }
      if ( H.isEmpty() ) { log() << "   empty" << nl; break; }

      if( mdb.checkForIsomorphicEntry( H ) != -1 )
      {
         log() << "   already in db" << nl;
         break;
      }

      NBBPG dist( G, len, gLen );
      PartialMinorProof pmp;
      DistinguisherNumber distNum = pmp.addDistinguisher( dist );
      pmp.addAllReductions( H );

//log() << "-----------------------" << nl;
//log() << "***" << H << nl;

      const SearchMinorDictionary& sm = pmp.minors();
      MinorNumber i;
      ForAllDictionaryItems( sm, i )
         pmp.addProof( i, distNum );

//log() << "***" << H << nl;
//log() <<  pmp << nl;
//log() << "-----------------------" << nl;

      // check for unknown node using this proof

      UnknownStateDatabase& udb = search.unknownStateDatabase();
      int uref = udb.checkForIsomorphicEntry( H );
      if ( uref == -1 )
      {
         MinimalProof proof( MinimalProof::DistinguisherProof );
         proof.notUsed();
         mdb.add( proof, pmp, H );
      }
      else
      {
         UnknownState st;
         udb.getInfo( uref, st );
         assert( st.isUsed() );
         NodeNumber n = st.usedBy();

         MinimalProof proof( MinimalProof::DistinguisherProof );
         proof.usedBy( n );
         int mref = mdb.add( proof, pmp, H );

         SearchNode& sn = search.node( n );
         sn.status( SearchNode::Minimal );
         sn.minimalProof( mref );
         search.changeNode( n, sn );

         udb.del( uref );

         log() << "   changed unknown " << n << " to minimal." << nl;
      }

   }

   FileManager::lockRequest( USysW );
}
#endif

void Manager::clearCompleteLevel()
{
   static int lastCompleteLevel = 0;

   cerr << "Clear check...";

   // try next level
   // level is complete if (1) no unknowns on level or before, 
   // and (2) all nodes on level-1 and before are grown.

   NodeNumber n;

   // Clear up to but not including level
   int level = 999999; //min( uLevel-1, mLevel );

   int uLevel = 999999;
   SearchNodeIter uIter( SearchNodeIter::Unknown );
   while ( (n = uIter.next()).isValid() ) {
      uLevel = min( search.node(n).depth(), uLevel );
      level = min( uLevel-1, level );
      if ( level <= lastCompleteLevel ) { cerr << "no new.\n"; return; }
      if ( search.stopReceived )
          return;
   }
   
   int mLevel = 999999;
   SearchNodeIter mIter( SearchNodeIter::Minimal, SearchNodeIter::Growable );
   while ( (n = mIter.next()).isValid() ) {
      mLevel = min( search.node(n).depth(), mLevel );
      level = min( mLevel, level );
      if ( level <= lastCompleteLevel ) { cerr << "no new.\n"; return; }
      if ( search.stopReceived )
          return;
   }

   if ( mLevel == 999999 && uLevel == 999999 ) { cerr << "complete!\n"; return;}

   cerr << "Search complete from levels " << lastCompleteLevel+1 << " to " << level << nl;
   /*
   for ( int i=lastCompleteLevel+1; i<=level; i++ )
   {
      int j = i + Operator::boundarySize();
      search.minimalProofDatabase().clearGraphCacheForLength( j );
      search.nonminimalProofDatabase().clearGraphCacheForLength( j );
      search.unknownStateDatabase().clearGraphCacheForLength( j );
   }
   */
   search.isoDatabase().clearIsoCacheUpToLength( level + Operator::boundarySize() );

   lastCompleteLevel = level;
}

//--------------------------------------------------
// 	Manager static methods
//--------------------------------------------------

template<class ProofClass>
void Manager::updateProof( NodeNumber n, ProofClass &proof, SearchNode &sn )
{
   // Get isoref from the existing proof that this SearchNode uses
   proof.isoRef( search.isoRef( sn ) );
   proof.usedBy( n );
}

template<>
void Manager::updateProof( NodeNumber n, IrrelevantInfo &proof, SearchNode &sn )
{
}

void Manager::followToUnknownNode( NodeNumber &n, SearchNode &sn )
{
   sn = search.node( n );
   if ( sn.status() == SearchNode::Irrelevant )
   {
       // A t-parse lower in the canonic ordering was found, and this was marked isomorphic.
       // Get that node (possibly repeatedly)
       IrrelevantInfo info;
       search.irrelevantInfoDatabase().getInfo( sn.irrelevantInfo(), info );
       assert( info.type() == IrrelevantInfo::Isomorphic );
       //search.delUnknownState( n );
       n = info.isomorphic();
       followToUnknownNode( n, sn );
   }
}

template<class ProofClass>
bool Manager::updateNodeCommon( NodeNumber &n, SearchNode &sn, ProofClass &proof )
{
   char inFamily;
   if ( sendMembership ) cmdFile >> inFamily;

   // Find true node ID and load
   followToUnknownNode( n, sn );
   // check status of node
   //
   if ( sn.status() != SearchNode::Unknown )
   {
      cerr << "WARNING:   not unknown...ignoring" << nl;
      aassert( false );
      return false;
   }

   if ( sendMembership )
   {
      sn.membership( inFamily ? SearchNode::InF : SearchNode::NotInF );
      if ( inFamily && ProofClass::status_enum == SearchNode::Minimal )
          // Delayed marking of growable
          sn.growStatus( SearchNode::Growable );
   }

   updateProof( n, proof, sn );  // isoref from old UnknownState db entry given to proof
   search.delUnknownState( n );
   return true;
}

template<class ProofClass>
void Manager::updateNode( NodeNumber n, ProofClass &proof )
{
   SearchNode sn;
   if ( updateNodeCommon( n, sn, proof ) )
   {
      int pfNum = search.addToDB( proof );
      search.updateNodeAndIso( n, sn, ProofClass::status_enum, pfNum );
   }
}

void Manager::updateNode( NodeNumber n, MinimalProof &proof, PartialMinorProof &pmp )
{
   SearchNode sn;
   if ( updateNodeCommon( n, sn, proof ) )
   {
      int pfNum = search.addToDB( proof, pmp );
      search.updateNodeAndIso( n, sn, MinimalProof::status_enum, pfNum );
   }
}

void Manager::minorsDistinguishedByRandomSearch()
{
   NodeNumber n;
   //Array<MinorNumber> minors;
   //NBBPG E;
   PartialMinorProof pmp;

   cmdFile >> n;
   if ( sendPMP ) cmdFile >> pmp;

   log() << n << " Node dist from minors" << nl;

#if 0
   bool someOk = false;
   for ( int i=0; i<minors.size(); i++ )
   {
      if ( pmp.minorStatus(minors[i]) == PartialMinorProof::NotDistinguished )
         someOk = true;
   }

   if ( ! someOk )
   {
      log() << "  all already distinguished...ignoring" << nl;
      return;
   }

   DistinguisherNumber dist;

   log() << " dist " << E << nl;

   dist = pmp.checkForIsoDistinguisher( E );

   if ( dist != -1 )
   {
      log() << "   have iso copy...using it instead" << nl;
   }
   else
      dist = pmp.addDistinguisher( E );

   for ( i=0; i<minors.size(); i++ )
      if ( pmp.minorStatus(minors[i]) == PartialMinorProof::NotDistinguished )
         pmp.addProof( minors[i], dist );
#endif

   if ( pmp.proofIsComplete() )
   {
      MinimalProof proof( MinimalProof::DistinguisherProof );
      updateNode( n, proof, pmp );
   }
   else
   {
      aassert(false);
   }
   
   //exploitObstructionPrefixes( G, E );
}

//--------------------------------------------------

void Manager::minorsDistinguishedByTestset()
{
   NodeNumber n;
   PartialMinorProof pmp;

   cmdFile >> n;
   if ( sendPMP ) cmdFile >> pmp;

   //cerr << "Got dist testset data" << n << ' ' << minors << ' ' << testnums << nl;

   //log() << n << " Node testset dist from minors " << minors;
   log() << n << " Node testset dist from minors " << nl;

   //PartialMinorProof pmp;
   //search.unknownStatePMP( n, pmp );

#if 0
   bool someOk = false;
   for ( int i=0; i<minors.size(); i++ )
   {
      if ( pmp.minorStatus(minors[i]) == PartialMinorProof::NotDistinguished )
         someOk = true;
   }

   if ( ! someOk )
   {
      log() << "   all already distinguished...ignoring" << nl;
      return;
   }

   DistinguisherNumber dist;

  log() << " test# " << testnums << nl;

  // fix later, when pmp gets methods to handle test numbers
  dist = 0;

   for ( i=0; i<minors.size(); i++ )
      if ( pmp.minorStatus(minors[i]) == PartialMinorProof::NotDistinguished )
         pmp.addProof( minors[i], dist );

#endif

//cerr << "***" << pmp << nl;

   if ( pmp.proofIsComplete() )
   {
      MinimalProof proof( MinimalProof::Testset );
      updateNode( n, proof, pmp );
   }
   else
   {
      aassert(false);
      //search.updateUnknownStatePMP( n, pmp );
   }
}

//--------------------------------------------------

void Manager::minorCongruentByTestset()
{
      NodeNumber n;
      SearchMinor minor;
      cmdFile >> n >> minor;

      if ( verbose )
         log() << n << " CongruentByTestset to " << minor << nl;

      NonminimalProof proof( minor, NonminimalProof::Testset );
      updateNode( n, proof );
}

//--------------------------------------------------

void Manager::minorCongruentViaDP()
{
  NodeNumber n;
  SearchMinor minor;
  cmdFile >> n >> minor;

  if ( verbose )
     log() << n << " CongruentByDP to " << minor << nl;

/*
  if ( ProbInfo::getBool( ProbInfo::SaveNMProofs ) )
  {
*/
     NonminimalProof proof( minor );
     updateNode( n, proof );
/*
  }
  else
  {
     search.delUnknownState( n );
     search.updateNodeAndIso( n, SearchNode::Nonminimal, 0 );
  }
*/
}

//--------------------------------------------------

void Manager::minimalByTightCong()
{
  NodeNumber n;
  cmdFile >> n;

  if ( verbose )
      log() << n << " minimalByTightCong" << nl;

  MinimalProof proof( MinimalProof::TightCong );
  updateNode( n, proof );
}

//--------------------------------------------------

void Manager::minimalByOOF()
{
  NodeNumber n;
  cmdFile >> n;

  if ( verbose )
      log() << n << " minimal by OOF " << nl;

  MinimalProof proof( MinimalProof::OutOfFamily );
  updateNode( n, proof );
}

//--------------------------------------------------

void Manager::nonminimalByOOF()
{
      NodeNumber n;
      cmdFile >> n;

      log() << n << " nonminimal by OOF " << nl;

      if ( ProbInfo::getBool( ProbInfo::NonMinOOFRelevant ) )
      {
        NonminimalProof proof( NonminimalProof::OutOfFamily );
        updateNode( n, proof );
      }
      else
      {
        IrrelevantInfo proof( IrrelevantInfo::OutOfFamily );
        updateNode( n, proof );
      }
}

//--------------------------------------------------

void Manager::giveUp()
{
      NodeNumber n;
      cmdFile >> n;

      if ( ProbInfo::getBool( ProbInfo::DefaultToMinimal ) )
      {
          if ( verbose )
              log() << n << " defaulting to minimal" << nl;     
          MinimalProof proof( MinimalProof::GiveUp );
          updateNode( n, proof );
      }
      else
      {
          if ( verbose )
              log() << n << " nonminimal by wimpout " << nl;     
          NonminimalProof proof( NonminimalProof::GiveUp );
          updateNode( n, proof );
      }
}

//--------------------------------------------------

void Manager::irrelevantByENoncanonic()
{
      NodeNumber n;
      cmdFile >> n;

      if ( verbose )
          log() << n << " irrel by easy noncanonic " << nl;

      IrrelevantInfo proof( IrrelevantInfo::ENoncanonic );
      updateNode( n, proof );
}

void Manager::irrelevantByHNoncanonic()
{
      NodeNumber n;
      cmdFile >> n;

      if ( verbose )
          log() << n << " irrel by hard noncanonic " << nl;

      NonminimalProof proof( NonminimalProof::HNoncanonic );
      updateNode( n, proof );
}

//--------------------------------------------------

#if 0
void Manager::distinguisherSearchFailed()
{
  NodeNumber n;
  int tries;
  cmdFile >> n;
  cmdFile >> tries;
  SearchNode& sn = search.node( n );
  if ( sn.status() != SearchNode::Unknown )
  {
    log() << "   not unknown...distsearchfail" << nl;
    return;
  }
  UnknownState st;
  search.unknownState( n, st );
  st.incExtensionSearches( tries );
  search.updateUnknownState( n, st );
  log() << n << " Dist search failed " << tries << nl;
}
#endif

//--------------------------------------------------
//--------------------------------------------------

void Manager::processLoop()
{
  //bool locked = false;
  //bool tight = ProbInfo::getBool( ProbInfo::CongIsTight );

    cerr << search.nonminimalProofDatabase().size() << " nonmin proofs\n";
    cerr << search.minimalProofDatabase().size() << " min proofs\n";
    cerr << search.unknownStateDatabase().size() << " unknown proofs\n";
    cerr << search.irrelevantInfoDatabase().nextNumber() << " iinfo proofs\n" << nl;

  // what to do variables
  //
  uint doneCheck = 0;
  bool checkForMinimalGrowable = true;
  bool okay2grow = true;
  bool done = false;
  int maxNodes = ProbInfo::getInt( ProbInfo::MaxNodes );

  double startTime = System::seconds();

  // search tree grow variables
  // 
  NodeNumber n;
  Array< NodeNumber > unknownNodes;
  RBBPG G;

  // main loop
  //
  int idle = 0;

  while ( !search.stopReceived )
  {
    if ( idle >= 2 )
    {
        /*
        cerr << "waiting; num unknown = " << search.unknownStateDatabase().numUnknown()
             << " num growable minimal = " << search.growableMinimal()
             << nl;
        */
        usleep( 1000 * IDLESLEEP ); // nothing to do
        if ( okay2grow )
            idleTime += 0.001 * IDLESLEEP;
        else
            idlePausedTime += 0.001 * IDLESLEEP;
    }

    Gtimers::start( 16, "command processing" );

    // command processing
    // 
    while (1)
    {
      // check if a command is available
      //
      //if ( ! cmdFile.readyForRead(POLL_TIMEOUT) ) break; // no command

      if ( ! cmdFile.readyForRead(POLL_TIMEOUT) )
         { idle++; break; } // no command

      idle = 0;
      checkForMinimalGrowable = true;

      Message::WorkResultType command;
      
      cmdFile >> command;

      if ( cmdFile.eof() || cmdFile.fail() ) 
      {
         cerr  << "no dispatcher -- manager now stopping...\n";
         goto end;
      }

      //cerr << "cmdFile >> command : " << (int) command << nl;

      switch ( command )
      {
        case Message::MinimalByTestset:
        {
          minorsDistinguishedByTestset();
          break;
        }

        case Message::MinimalByExtn:
        {
          minorsDistinguishedByRandomSearch();
          break;
        }

        case Message::MinorCongTestset:
        {
          minorCongruentByTestset();
	  break;
        }

        case Message::MinorCongDyProg:
        {
          minorCongruentViaDP();
	  break;
        }

        case Message::MinimalByTightCong:
        {
          minimalByTightCong();
	  break;
        }

        case Message::InconclusiveMinimality:
        {
          giveUp();
	  break;
        }

        case Message::NonminimalByOOF:
        {
          nonminimalByOOF();
	  break;
        }

        case Message::MinimalByOOF:
        {
          minimalByOOF();
	  break;
        }

        case Message::ENoncanonic:
        {
          irrelevantByENoncanonic();
	  break;
        }

        case Message::HNoncanonic:
        {
          irrelevantByHNoncanonic();
	  break;
        }

        case Message::ProcessFinished:
        {
          cerr  << "dispatcher quitting -- manager now stopping...\n";
          goto end;
        }

        // Sync from dispatcher regarding too many unknown nodes
        //
        case Message::Pause:
        {
          okay2grow = false;
          cerr << "** dispatcher signals Pause **\n";
          break;
        }

        case Message::Resume:
        {
          okay2grow = true;
          cerr << "** dispatcher signals Resume **\n";
          break;
        }

        default:
          aassert( false );

      } // switch

#if 1
      // TMP: Update the database for dispatcher //mjd
      // 
      //FileManager::lockRequest( LSysW );
      //search.flush();   // here's the repeated manager flush
      //FileManager::lockRequest( USysW );
#endif

    } // command processing loop

    Gtimers::stop( 16 );

    //cerr << "checking for minimal grow\n";

    // Check for minimal grow where we just do a few then check for new message
    //
    bool log_once = true;
    if ( okay2grow && checkForMinimalGrowable ) // Don't waste CPU. 
    {
      growTime -= Dtime::CPUtime();

      int nodes_sent = 0;
      for ( int j = 10; j; j-- )
      {

        Gtimers::start( 17, "chooseMinimalGrow" );

        if ( ( n = chooseMinimalGrow( true ) ).isInvalid() )
        {
          // There aren't any minimal growable nodes left (waiting for workers)
          checkForMinimalGrowable = false;
          idle++;
          Gtimers::stop( 17 );
          break;
        }

        if ( log_once )
        {
          log_once = false;
          cout << "Growing " << n << " (depth " << search.node(n).depth() << ")\n";
        }

        Gtimers::stop( 17 );
      
        idle = 0;
        growFromNode( n, unknownNodes );

        if ( maxNodes && search.numberOfNodes() > maxNodes )
        {
            cerr << "Limit on nodes reached (" << maxNodes << "), stopping" << nl;
            growTime += Dtime::CPUtime();
            goto end;
        }

        Gtimers::start( 18, "send unknowns" );

        // send new unknown nodes to dispatcher
        assert(unknownNodes.size()>=0);
        for ( int i = 0; i < unknownNodes.size(); i++ )
        {
          //if (!i)
            // cerr << "Manager sending unknownNode : " << unknownNodes << nl;
          search.graph( unknownNodes[i], G );
          cmdFile << unknownNodes[i] << G;
          nodes_sent++;
        }
        cmdFile.flush();

        Gtimers::stop( 18 );

        // If we fill up the output buffer, we could block and deadlock
        // This is a kludge
        if ( nodes_sent > 20 )
            break;
      }

      growTime += Dtime::CPUtime();
    }

    if ( requestFlush )
    {
       cerr << "Database flush was requested\n";
       search.flush();
       requestFlush = false;
    }

    // occasional checks...
    if ( !!timer )
    {
       Gtimers::start( 19, "clearCompleteLevel" );

       double clearTime = System::seconds();

       // flush the db
       search.flush();
       // I don't know whether we actually need locks around this
       FileManager::lockRequest( LSysW );
       clearCompleteLevel();
       FileManager::lockRequest( USysW );

       // See if we are still in control.
       //
       if ( notInControl() )
       {
          cerr << "Manager not in control.\n";
          break;
       }

       clearTime = System::seconds() - clearTime;
       printf( "Check done in %.1fs\n", clearTime );
       // For rate limiting add, eg:  int(clearTime) * 35
       timer.set( TIMER );

       Gtimers::stop( 19 );
    }

    done = (search.growableMinimal() == 0 && search.unknownStateDatabase().numUnknown() == 0);

    if ( done ) //|| idle && ((doneCheck++) % 120000) == 0 )  // double check completion every 2 * IDLESLEEP minutes
    {
       Gtimers::start( 20, "searchComplete" );

       // Are we finished with the search?
       //
       if ( searchComplete() )
       {
          cerr << "Search complete.\n";
          Gtimers::stop( 20 );
          break;
       }
       if ( done )
       {
          cerr << "Search complete condition was incorrectly true: "
               << search.growableMinimal() << ' ' << search.unknownStateDatabase().numUnknown() << nl;
       }

       Gtimers::stop( 20 );
    }

  } // main loop

 end:

  if ( search.stopReceived )
  {
    // Anything printed to stderr will probably not be seen or turn up in the logs, because
    // the parent shell process has already quit: need to print to log().
    cerr << "Manager received quit signal.\n";
    log() << "Manager received quit signal.\n";
  }
  else if ( ! done )
    cerr << "But completion condition is false!\n";

  mainLoopTime = System::seconds() - startTime;
  cerr << "Manager stopping.\n";
  search.flush();
  cmdFile.close();  // This tells the dispatcher to shut down.

  statistics();

} // function processLoop

//-----------------------------------------------------
//----------  external linkage section ----------------
//-----------------------------------------------------

// swan off dispatcher and returns communication file descriptor
//
static int launchDispatcher()
{

   // flush std out and std err to prevent duplicate output
   //
   cout.flush();
   cerr.flush();

   int pid, 
       fd[2];

   aasserten( socketpair( AF_UNIX, SOCK_STREAM, 0, fd ) == 0 );

   //aasserten( fcntl( fd[0], F_SETFL, O_NONBLOCK ) == 0 );
   //aasserten( fcntl( fd[1], F_SETFL, O_NONBLOCK ) == 0 );

   if ( ( pid = fork() ) == 0 )
   {
      // close all other files besides socket, bypassing iostream
      //
      for ( int tfd = maxFile() - 1; tfd >= 3; tfd-- ) 
      {
        if (tfd != fd[1]) close( tfd );
      }

      // Pass in as an argument the communication channel left open
      // to the manager.
      //
      char fdStr[2];
      fdStr[0] = fd[1];
      fdStr[1] = 0;

      // Keep fd[1] open across execl
      if ( fcntl(fd[1], F_SETFD, 0) == -1 ) aasserten(false);

cerr << "execl fd = " << fd[1] << nl;
cerr.flush();

      execl(
         FileManager::executableFilename(),
         FileManager::executableFilename(),
         "Dispatch",
         runInfo.driverFilename(),
         fdStr,
         NULL
      );

      aasserten( false ); // only get here on an bad bad error
   } else {
      // Manager closes unneeded half, so that the socket is
      // closed when the Dispatcher terminates
      close(fd[1]);
   }

   if ( pid == -1 )
      aasserten( false );

   return fd[0];
}

// entry point for manager
//
void mainManager( int argc, char** argv )
{

   Manager::preloadProofs = false;

   if ( argc > 1 && !strcmp( argv[1], "preload" ) )
   {
      Manager::preloadProofs = true;
      argv[1] = argv[0];
      argc--;
   }
   
   if ( argc != 1  )
   {
       cerr << "Usage: search Manager driverfile [preload]" << nl;
       fatalError( "Bad args for manager" );
   }

   // Create the root of the tree
   //
   search.clearProofUsage();
   search.createRootNode();

   //if ( ! ProbInfo::getBool( ProbInfo::SaveNMProofs ) )
   //{
      NonminimalProof proof( NonminimalProof::OutOfFamily );
      search.nonminimalProofDatabase().add( proof );
   //}

   //FileManager::lockRequest( LSysW );
   //search.flush();
   //FileManager::lockRequest( USysW );

   // start dispatcher (and set up communication channel to dispatcher)
   //
   Manager::initialize( launchDispatcher() );

   signal(SIGTERM, Manager::stopSignal);
   signal(SIGINT, Manager::stopSignal);
   signal(SIGUSR1, Manager::usr1Signal);
   signal(SIGPIPE, SIG_IGN);

   // invoke processing loop 
   //
   Manager::processLoop();
}

