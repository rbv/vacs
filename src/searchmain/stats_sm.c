
// -------------------------------------------
// --------------- sm_stats.c ----------------
// -------------------------------------------

/*tex
 
\file{sm_stats.c}
\path{src/searchmain}
\title{Program to compile and print search statistics}
\classes{}
\makeheader
 
xet*/

#include "search/searchmain.h"

#include "set/set.h"
#include "error.h"
#include "search/searchminor.h"
#include "search/nodeinfo.h"
#include "search/searchnode.h"
#include "search/search.h"
#include "family/family.h"
#include "bpg/bpgutil.h"
#include "general/str.h"

#include "vacs/runinfo.h"
#include "vacs/filesystem.h"

#undef loop

static void mainLoop();


//-------------------------------------------

class rec
{
   int f;  // in family count
   int n;  // not in family count
   int z;  // not computed count
   int cnt[3];
public:
   rec() { cnt[0] = cnt[1] = cnt[2] =0; }

   int& operator[]( int i ) { assert( i >= 0 && i <= 2 ); return cnt[i]; }
};


struct Stats
{
   rec Total;

   rec Minimal;
      rec DistinguisherProof;
      rec MRoot;
      rec MExternal;
      rec MOutOfFamily;
      rec CongTight;
      rec MTestset;
      rec MGiveUp;

      rec MGrowable;
      rec MNotGrowable;
      rec MGrown;

   rec Nonminimal;
      rec HNoncanonic;
      rec CongruentMinor;
      rec NTestset;
      rec Parent;
      rec NExternal;
      rec NOutOfFamily;
      rec NGiveUp;
      rec NNotSaved;

      rec NGrowable;
      rec NNotGrowable;
      rec NGrown;

   rec Unknown;
      rec UGrowable;
      rec UNotGrowable;
      rec UGrown;

   rec Irrelevant;
      rec ENoncanonic;
      rec QNoncanonic;
      rec IOutOfFamily;
      rec IsoSibling;
      rec Nontree;
      rec Pretest;
      rec MultiGraph;
      rec Isomorphic;
      rec Disconnected;
      rec Oneconnected;
      rec Prefix;

};

static Stats S;


// pretest analysis
//
static bool Pretest = false;
Array< rec > UPretestStats;
Array< rec > NPretestStats;
Array< rec > NPretestStatsGU;
Array< rec > NPretestStatsCM;
Array< rec > NPretestStatsTS;
Array< rec > NPretestStatsOT;
Array< rec > MPretestStats;
Array< rec > APretestStats; // actual (irrel)

//-------------------------------------------


void mainStats( int argc, char** argv )
{

   // Look for pretest analysis arg
   if ( argc == 2 && ( !strcmp( argv[1], "p" ) ||
                       !strcmp( argv[1], "pretest" ) 
                     ) )
   {
      Pretest = true;
      argc--; argv[1] = argv[0]; argv++;
   }

   // check cmd line
   //
   if ( argc != 1 )
   {
     cerr << "Usage: stats [pretest]" << nl
          << " pretest: stats for running pretests on all non-irrelevant nodes" << nl;
     fatalError( "Bad arguments" );
   }

   // execute the main loop of the program
   //
   mainLoop();

}

//-----------------------------------------------------------


static void updateStats( NodeNumber n )
{
   const SearchNode& s = search.node( n );

   int m = s.membership();

   S.Total[m]++;

   switch ( s.status() )
   {
      case SearchNode::Minimal:
      {
         S.Minimal[m]++;
         MinimalProof proof;
         search.minimalProof( n, proof );
         switch( proof.type() )
         {
            case MinimalProof::External:    S.MExternal[m]++; break;
            case MinimalProof::DistinguisherProof: 
                                            S.DistinguisherProof[m]++; break;
            case MinimalProof::TightCong:   S.CongTight[m]++; break;
            case MinimalProof::Testset:     S.MTestset[m]++; break;
            case MinimalProof::GiveUp:      S.MGiveUp[m]++; break;
            case MinimalProof::OutOfFamily: S.MOutOfFamily[m]++; break;
            case MinimalProof::Root:        S.MRoot[m]++; break;
            default: aassert( false );
         }
         switch( s.growStatus() )
         {
            case SearchNode::Growable:    S.MGrowable[m]++; break;
            case SearchNode::NotGrowable: S.MNotGrowable[m]++; break;
            case SearchNode::Grown:       S.MGrown[m]++; break;
            default: aassert( false );
         }
         break;
      }

      case SearchNode::Unknown:
      {
         S.Unknown[m]++;
         switch( s.growStatus() )
         {
            case SearchNode::Growable:    S.UGrowable[m]++; break;
            case SearchNode::NotGrowable: S.UNotGrowable[m]++; break;
            case SearchNode::Grown:       S.UGrown[m]++; break;
            default: aassert( false );
         }
         break;
      }

      case SearchNode::Irrelevant:
      {
         S.Irrelevant[m]++;
         IrrelevantInfo info;
         search.irrelevantInfo( n, info );
         switch( info.type() )
         {
            case IrrelevantInfo::ENoncanonic:  S.ENoncanonic[m]++; break;
            case IrrelevantInfo::QNoncanonic:  S.QNoncanonic[m]++; break;
            case IrrelevantInfo::NonTree:      S.Nontree[m]++; break;
            case IrrelevantInfo::MultiGraph:   S.MultiGraph[m]++; break;
            case IrrelevantInfo::Isomorphic:   S.Isomorphic[m]++; break;
            case IrrelevantInfo::IsoSibling:   S.IsoSibling[m]++; break;
            case IrrelevantInfo::Disconnected: S.Disconnected[m]++; break;
            case IrrelevantInfo::Oneconnected: S.Oneconnected[m]++; break;
            case IrrelevantInfo::Prefix:       S.Prefix[m]++; break;
            case IrrelevantInfo::OutOfFamily:  S.IOutOfFamily[m]++; break;
            case IrrelevantInfo::Pretest:      S.Pretest[m]++;
                   APretestStats[ info.pretest() ][m]++;
                   break;
            default: aassert( false );
         }
         break;
      }

      case SearchNode::Nonminimal:
      {
         S.Nonminimal[m]++;
         NonminimalProof proof;
         search.nonminimalProof( n, proof );
         switch( proof.type() )
         {
            case NonminimalProof::HNoncanonic:    S.HNoncanonic[m]++; break;
            case NonminimalProof::External:       S.NExternal[m]++; break;
            case NonminimalProof::CongruentMinor: S.CongruentMinor[m]++; break;
            case NonminimalProof::Testset: 	  S.NTestset[m]++; break;
            case NonminimalProof::Parent:         S.Parent[m]++; break;
            case NonminimalProof::OutOfFamily:    S.NOutOfFamily[m]++; break;
            case NonminimalProof::GiveUp:         S.NGiveUp[m]++; break;
            case NonminimalProof::NotSaved:       S.NNotSaved[m]++; break;
            default: aassert( false );
         }
         switch( s.growStatus() )
         {
            case SearchNode::Growable:    S.NGrowable[m]++; break;
            case SearchNode::NotGrowable: S.NNotGrowable[m]++; break;
            case SearchNode::Grown:       S.NGrown[m]++; break;
            default: aassert( false );
         }
         break;
      }

      default: aassert( false );

   } 

   if ( s.status() == SearchNode::Irrelevant ) return;

   if ( Pretest )
   {
      const NonminimalPretests& p = Family::nonminimalPretests();
      RBBPG G;
      search.graph( n, G );

      p.setGraph( G );
      int i = p.applyAllTests();
      p.releaseGraph();
      //cerr << G <<  ' ' << i << nl;

      if ( i == -1 ) return;

      if ( s.status() == SearchNode::Unknown )
         UPretestStats[ i ][ m ]++;

      if ( s.status() == SearchNode::Nonminimal )
      {
         NPretestStats[i][m]++;
         NonminimalProof proof;
         search.nonminimalProof( n, proof );
         switch( proof.type() )
         {
           case NonminimalProof::CongruentMinor: NPretestStatsCM[i][m]++; break;
           case NonminimalProof::Testset:        NPretestStatsTS[i][m]++; break;
           case NonminimalProof::GiveUp:         NPretestStatsGU[i][m]++; break;
           default:                              NPretestStatsOT[i][m]++; break;
         }
      }

      if ( s.status() == SearchNode::Minimal )
         MPretestStats[ i ][ m ]++;


   }

}


//---------------------------------------------------------


#define POS1 20

static void print( int level, const char* label, rec r )
{

   Str s;

   if ( level > 1 ) s.append("   ");

   s.append( label );
   s.padTo( POS1 ); 
   s.append( Str( r[0]+r[1]+r[2] ).padLeft(10) );
   s.append( Str( r[1] ).padLeft(10) );
   s.append( Str( r[0] ).padLeft(10) );
   s.append( Str( r[2] ).padLeft(10) );

   cout << s << nl;
}

static void printStats()
{
   cout << "\t\t\t  Total\t  not in F      in F         NC" << nl;

   print( 1, "Total", S.Total );

   print( 1, "Minimal", S.Minimal );
      print( 2, "Root", S.MRoot );
      print( 2, "DistProof", S.DistinguisherProof );
      print( 2, "TightCongr", S.CongTight );
      print( 2, "MTestset", S.MTestset );
      print( 2, "Give up", S.MGiveUp );
      print( 2, "External", S.MExternal );
      print( 2, "OOF", S.MOutOfFamily );

   print( 1, "Nonminimal", S.Nonminimal );
      print( 2, "CongMinor", S.CongruentMinor );
      print( 2, "NTestset", S.NTestset );
      print( 2, "Parent", S.Parent );
      print( 2, "External", S.NExternal );
      print( 2, "OOF", S.NOutOfFamily );
      print( 2, "Give up", S.NGiveUp );
      print( 2, "Not saved", S.NNotSaved );
      print( 2, "HNoncanonic", S.HNoncanonic );

   print( 1, "Unknown", S.Unknown );
   print( 1, "Irrelevant", S.Irrelevant );
      print( 2, "ENoncanonic", S.ENoncanonic );
      print( 2, "QNoncanonic", S.QNoncanonic );
      print( 2, "OOF", S.IOutOfFamily );
      print( 2, "Nontree", S.Nontree );
      print( 2, "Pretest", S.Pretest );
      print( 2, "Disconnected", S.Disconnected );
      print( 2, "1-connected", S.Oneconnected );
      print( 2, "Prefix", S.Prefix );
      print( 2, "MultiGraph", S.MultiGraph );
      print( 2, "Isomorphic", S.Isomorphic );
      print( 2, "Isosibling", S.IsoSibling );
    
   cout << nl;
   print( 1, "Minimal", S.Minimal );
      print( 2, "Growable", S.MGrowable );
      print( 2, "Not Growable", S.MNotGrowable );
      print( 2, "Grown", S.MGrown );
   print( 1, "Unknown", S.Unknown );
      print( 2, "Growable", S.UGrowable );
      print( 2, "Not Growable", S.UNotGrowable );
      print( 2, "Grown", S.UGrown );

   if ( Pretest )
   {
      const NonminimalPretests p = Family::nonminimalPretests();

      cout << nl;
      int i;

      print( 1, "Pretests, last run:", S.Pretest );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), APretestStats[i] );

      print( 1, "Current: Minimal", S.Minimal );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), MPretestStats[i] );

      print( 1, "Current: Unknown", S.Unknown );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), UPretestStats[i] );
   
      print( 1, "Crnt: Nonminimal", S.Nonminimal );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), NPretestStats[i] );

      print( 1, "Crnt: NM Cng Mnr", S.Nonminimal );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), NPretestStatsCM[i] );

      print( 1, "Crnt: NM Testset", S.Nonminimal );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), NPretestStatsTS[i] );

      print( 1, "Crnt: NM Give Up", S.Nonminimal );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), NPretestStatsGU[i] );

      print( 1, "Crnt: NM Other", S.Nonminimal );
      for ( i=0; i<p.numberOfTests(); i++ )
         print( 2, p.name(i), NPretestStatsOT[i] );

   }
}


//-----------------------------------------------------------

static void mainLoop()
{

   const NonminimalPretests pretests = Family::nonminimalPretests();
   UPretestStats.resize( pretests.numberOfTests() );
   NPretestStats.resize( pretests.numberOfTests() );
   NPretestStatsCM.resize( pretests.numberOfTests() );
   NPretestStatsTS.resize( pretests.numberOfTests() );
   NPretestStatsGU.resize( pretests.numberOfTests() );
   NPretestStatsOT.resize( pretests.numberOfTests() );
   MPretestStats.resize( pretests.numberOfTests() );
   APretestStats.resize( pretests.numberOfTests() );

   SearchNodeIter I( SearchNodeIter::SAny );
   NodeNumber n;

   //DatabaseBase::lockSystem();
   FileManager::lockRequest( LSysR );

   while ( ( n = I() ).isValid() )
   {
      if ( ! ( n.asInt() % 200000 ) ) cerr << n << nl;
      updateStats( n );
   }
   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

   printStats();

}

