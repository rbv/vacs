 
/*tex
 
\file{human1_sm.c}
\path{src/searchmain}
\title{Human access to dispatcher's internal state.}
\classes{}
\makeheader
 
xet*/

#include <stdlib.h>
#include <string.h>

#include "vacs/message.h"
#include "general/stream.h"
#include "error.h"

// use a local class to scope data and methods
//
class Human1
{
   // process control
   int dispatchFD;
   bstream dispatch;

   // entry/exit
   void init();
   void cleanUp();

public:

   void mainLoop();

};

static Human1 human1;

//---------------------------------------------------------------

void Human1::init()
{
   // get connection to dispatcher
   //
   dispatchFD = Message::getDispatchFD();

   dispatch.openOnFD( dispatchFD, 0 );
   
   cout << "Backdoor initializing...";

   // empty buffer (may contain node to work on)
   //
   char junk;
   while (dispatch.readyForRead(2000)) dispatch >> junk;
}

void Human1::cleanUp()
{
   dispatch.close();
   cout << "Backdoor closing\n";
}

// -----------------------------------------------------------------
//       MAIN processing loop
// -----------------------------------------------------------------

void Human1::mainLoop()
{
  init();
    
  dispatch << Message::BackDoor;
  
  char line[256];
  strcpy( line, "h1util '" );
  int start = strlen( line );
  int pos = start;
  //
  bool done = false;
  while ( ! done && ! dispatch.eof() )	// for now just take what we get
  {
    char c;
    dispatch >> c;
    switch (c)
    {
      case '$': done = true; break;	// end of stream indicator
      //case '#': line[ pos++ ] = '\\'; line[ pos++ ] = c; break;
      case '\n':
       line[pos++] = '\'';
       line[pos] = 0;
       if ( strncmp( "Client", line+start, 6 ) == 0 )
         system( line );
       else
       {
         line[pos-1]=0;
         cout << line+start << nl;
       }
       pos = start;
       break;
      default: 
       line[ pos++ ] = c; break;
    }
  }

   cout << "Enter dispatcher file descriptors to close (0 to quit):\n"; 

   int w_fd1, w_fd2;

   while ( true )
   {
    cin >> w_fd1;
    if ( cin.fail() ) { cerr << "Bad input\n"; w_fd1 = 0; }

    dispatch << w_fd1;
    if ( dispatch.fail() ) break;

    dispatch >> w_fd2; // wait for acknowledgement 
    if ( w_fd2 == -1 || dispatch.eof() || dispatch.fail() ) break;

    if ( w_fd1 == w_fd2 ) cout << "Worker " << w_fd1 << " stopped\n";
   }

   cleanUp();
}

//-------------------------------------------------------------------

void mainHuman1( int argc, char** )
{

   if ( argc != 1 ) fatalError( "worker takes no args. " );

   human1.mainLoop();
}

