
// -------------------------------------------
// ------------- sm_classes.c ----------------
// -------------------------------------------

/*tex
 
\file{sm_classes.c}
\path{src/searchmain}
\title{Class memory testing}
\classes{}
\makeheader

xet*/

#if 0 && defined(FST)


#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include "search/searchmain.h"
#include "error.h"

#include "search/search.h"
#include "vacs/runinfo.h"
#include "search/searchnode.h"
#include "vacs/filesystem.h"

// -------------------------------------------------------------------
// --------- Free Store Testing --------------------------------------
// -------------------------------------------------------------------

void allClassesStatus()
{

   // initTables__8OperatorSFv();

   SearchNode::classStatus();
   SearchMinor::classStatus();
   BPG::classStatus();
   RBBPG::classStatus();
   CBBPG::classStatus();
   NBBPG::classStatus();

   // Templates...

   Array<short>::classStatus( "short" );
   Array<int>::classStatus( "int" );
   Array<char>::classStatus( "char" );
   Array<long>::classStatus( "long" );
   Array<uchar>::classStatus( "uchar" );
   Array<ushort>::classStatus( "ushort" );
   Array<uint>::classStatus( "uint" );
   Array<ulong>::classStatus( "ulong" );
   Array<Operator>::classStatus( "Operator" );
   Array<NodeNumber>::classStatus( "NodeNumber" );
   Array<SearchNode*>::classStatus( "SearchNode*" );
   Array<SearchMinor*>::classStatus( "SearchMinor*" );
   Array< SortableArray<short>* >::classStatus( " SA<short>*" );

}

#endif
