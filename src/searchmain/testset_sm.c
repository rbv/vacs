 
/*tex
 
\file{testset_sm.c}
\path{src/searchmain}
\title{External process to build testset database.}
\classes{}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "error.h"
#include "family/family.h"
//#include "family/testset/testset.h"

// -----------------------------------------------------------------
//       MAIN processing routine
// -----------------------------------------------------------------

void mainTestset( int argc, char** )
{
   if ( argc != 1 ) fatalError( "worker takes no args. " );

   assert( Family::testsetAvailable() );

   Family::buildTestset(); 
}

