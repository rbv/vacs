 
/*tex
 
\file{worker1_sm.c}
\path{src/searchmain}
\title{External process to determine if a node is minimal/nonminimal.}
\classes{}
\makeheader
 
xet*/

#include <strstream>
#include <signal.h>
//#ifdef __SUNPRO_CC
//extern "C" int sigblock(int);
//extern "C" int sigmask(int);
//extern "C" int sigsetmask(int);
//#endif
#include <fcntl.h>
#include <vector>

#include "family/probinfo.h"
#include "vacs/message.h"
#include "search/searchmain.h"
#include "search/searchnode.h"
#include "search/searchminor.h"
#include "search/searchutil2.h"
#include "search/nodeinfo.h"
#include "search/search.h"

#include "family/family.h"
#include "vacs/filesystem.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "error.h"
#include "general/dtime.h"
#include "general/gtimers.h"
#include "general/random.h"

//--------------------------------------------------------------------------

static bool waitingForDispatcher = true;

// Iterrupt handler to catch dispatcher stop signal.
//
static void dispatchStopSignal(int) 
{
   if ( waitingForDispatcher ) return;
   cerr << "Stop signal received from dispatcher.\n";
   exit(1);
}

//--------------------------------------------------------------------------

// Define this to test the correctness of a DP congruence when "test cong minimal" is true,
// not just the minimality results
//#define TEST_CONG_CORRECTNESS

#define HIST_BUCKETS 10

// use a local class to scope data and methods
//
class Worker1
{
   // process control
   int dispatchFD;
   bstream dispatch;
   float MaxEasyCanonicTime;
   bool stopReceived;

   // methods - workers return true if problem solved
   bool readProblem();
   void work();
   bool runCanonicCheck();
   bool runOOFCheck();
   bool runRandomExtnSearch();
   bool runUniversalDistinguisherSearch();
   bool runRandomMinorSearch();
   bool runCongruenceCheck();
   bool runTestset();
   bool runTildeMinorSearch();
   bool assumeNonminimal();

void runFEStest(); // tmp hack

   // search control info
   randomExtensionSearchInfo info;
   int maxExtnRuns;
   int canonicTimeout;
   bool verbose;
   bool useTestset;
   bool tildeMinorSearch;
   bool univDistSearch;
   //bool testCanonic;
   bool sendPMP;
   bool sendMembership;
   int testCongruencePermill;
   bool testCongruence;
   char inFamily;

   // specific problem info
   NodeNumber nodeNum;
   PartialMinorProof* pmp;
   RBBPG graph;

   // entry/exit
   void init();
   void cleanUp();

   // Used for "test cong tightness" option only
   struct CongStats
   {
      int total;
      int congMinimal;
      int minimal;
      int nonminimal;
      int extnRunsHisto[HIST_BUCKETS];

      CongStats()
      {
         //total = congMinimal = minimal = nonminimal = sumExtnRuns = 0;
         memset( this, 0, sizeof *this );
      }
   };
   std::vector<CongStats> congStats;  // indexed by vertices
   int extnRuns;
   Message::WorkResultType result;
   bool ranCongruenceCheck;
   bool congMinimalResult;  // Whether congruence shows node is minimal
   void setResult( Message::WorkResultType _result, int runs = -1 );
   void printCongTestResults();
   void updateCongTestResults();

   // CPU limit variables
   //
   Dtime ourClock;
   float maxTime;

   float oofTime;
   float canonicTime;
   float tildeMinorTime;
   float univDistTime;
   float congTime;
   float randDistTime;
   float testsetTime;

   int numProbs;  // number completed

   RandomInteger randGen;

public:

   Worker1() : randGen( 1000 ) {}

   // CPU limit methods
   //
   void noClock() { maxTime = -1.0; }

   void startClock(int limit) { ourClock.start(); maxTime = limit; }

   bool moreTime();
   static void stopSignal(int);

   //
   void mainLoop();
   //

};


bool Worker1::moreTime()
{
  if ( maxTime < 0 ) return true;	// no checking

  maxTime -= ourClock();

  //cerr << "time left: " << maxTime << " secs\n";

  if ( maxTime > 0 ) return true;

  return false;
}

//---------------------------------------------------------------
static Worker1 worker1;
//---------------------------------------------------------------

void Worker1::stopSignal(int) 
{
   worker1.stopReceived = true;
}

void Worker1::init()
{
   // This is just the cutoff for reporting as easy/hard. See canonicTimeout instead.
   // This is relevant for whether the node gets saved as irrelevant or nonminimal:
   // if the later, future nodes are checked for being isomorphic to it. Hence might not
   // be worth doing isomorphism testing against really easy non-canonic t-parses
   MaxEasyCanonicTime = 1.0;

   stopReceived = false;
   numProbs = 0;

   // get connection to dispatcher
   //
   dispatchFD = Message::getDispatchFD();

   dispatch.openOnFD( dispatchFD );

   // Set up asynchronous i/o from o/s for this socket
   //
   //sigblock(sigmask(SIGIO)); // turn off for now though
   aassert( fcntl( dispatchFD, F_SETOWN, getpid() ) >= 0);
#ifndef CRAY
#ifndef __xlC__
#ifndef __SUNPRO_CC
//   aassert( fcntl( dispatchFD, F_SETFL, FASYNC ) >= 0);
#endif
#endif
#endif
   
   // get the driver params we need
   //
   maxExtnRuns = ProbInfo::getInt( ProbInfo::ExtnRuns );
   useTestset = ProbInfo::getBool( ProbInfo::UseTestset );
   sendPMP = ProbInfo::getBool( ProbInfo::MessagesContainPMP );
   sendMembership = ProbInfo::getBool( ProbInfo::WorkersDoMembership );
   if ( ProbInfo::getBool( ProbInfo::TestCongTightness ) )
      testCongruencePermill = ProbInfo::getInt( ProbInfo::TestCongPermill );
   else
      testCongruencePermill = 0;
   tildeMinorSearch = ProbInfo::getBool( ProbInfo::TildeMinorSearch );
   univDistSearch = ProbInfo::getBool( ProbInfo::UnivDistSearch );
   canonicTimeout = ProbInfo::getInt( ProbInfo::CanonicTimeout );
   verbose = ProbInfo::getBool( ProbInfo::VerboseNodeLogging );

   info.ExtnEdgeWeight = ProbInfo::getInt( ProbInfo::ExtnEdgeWeight );
   info.ExtnTries = ProbInfo::getInt( ProbInfo::ExtnTries );
   info.ExtnMaxLength = ProbInfo::getInt( ProbInfo::ExtnMaxLength );
   info.ExtnSkip = ProbInfo::getInt( ProbInfo::ExtnSkip );

   if ( ProbInfo::getBool( ProbInfo::Connected ) )
      info.ExtnFlags |= ExtensionConnected;
   if ( ProbInfo::getBool( ProbInfo::KeepTree ) )
      info.ExtnFlags |= ExtensionTree;
   if ( ProbInfo::getBool( ProbInfo::PretestExtns ) )
      info.ExtnFlags |= ExtensionPretest;

   pmp = 0;

   cerr << "worker initialized. PID = " << getpid() << "\n";
   log() << nl << "Sizes: order edges Times: "
         << "oofTime canonicTime tildeMinorTime univDistTime congTime randDistTime testsetTime" << nl;
}

void Worker1::cleanUp()
{
   cerr << "worker stopping\n";
   cerr << "done; consumed " << Dtime::CPUtime() << " sec CPU time; processed " << numProbs << " nodes\n";
   Gtimers::print( cerr, 25 );
   if ( testCongruencePermill > 0 )
      printCongTestResults();
   Family::printstats( cerr );
}

void Worker1::printCongTestResults()
{
   cerr << "Congruence tightness testing results:\n";
   char buf[120];
   snprintf( buf, 120, "vertices      nodes    cong min    minimal    nonminimal    extn runs 0..%d (%d buckets)\n", maxExtnRuns, HIST_BUCKETS );
   cerr << buf;
   for (int i = 1; i < congStats.size(); i++)
   {
      CongStats &stats = congStats[i];
      int (&hist)[HIST_BUCKETS] = stats.extnRunsHisto;
      snprintf( buf, 120, "%8d %10d %9d %10d   %11d    ",
                i, stats.total, stats.congMinimal, stats.minimal, stats.nonminimal );
      cerr << buf;
      for (int j = 0; j < HIST_BUCKETS; j++)
      {
          if ( j )
              cerr << ",";
          snprintf( buf, 120, "%4d", hist[j] );
          cerr << buf;
      }
      cerr << nl;
   }
}

// -----------------------------------------------------------------
//       MAIN processing loop
// -----------------------------------------------------------------

void Worker1::mainLoop()
{
    init();

    while( !stopReceived )
    {
        // See if we have CPU time left
        //
        if( !moreTime() )
        {
            cleanUp();
            exit(17);
        }

        Gtimers::start( 1, "readProblem" );

        if ( !readProblem() )
            break;

        Gtimers::stop( 1 );

        numProbs++;
        //sigsetmask(0); 			// enable all signals
        waitingForDispatcher = false;

        oofTime = canonicTime = tildeMinorTime = univDistTime = -1.0;
        congTime = randDistTime = testsetTime = -1.0;

        Gtimers::start( 2, "work" );

        work();
        if ( sendMembership )
            dispatch << inFamily;
        //dispatch << Message::ProcessFinished;
        dispatch.flush();

        if ( testCongruence )
           updateCongTestResults();

        Gtimers::stop( 2 );

        //sigblock(sigmask(SIGIO)); 	// turn off SIGIO signal
        waitingForDispatcher = true;


        //cerr << "work complete ";
        log() << "Sz " << graph.order() << ' ' << graph.edges()
              << " T: " 
              << oofTime << ' ' << canonicTime << ' ' << tildeMinorTime << ' '
              << univDistTime << ' ' <<  congTime << ' ' << randDistTime << ' '
              << testsetTime << nl;

#ifndef __xlC__
        //sigpause(0);			// wait here
#endif
      
    }
    if ( stopReceived )
        cerr << "Worker quitting due to signal\n";
    //
    cleanUp();
}

void Worker1::updateCongTestResults()
{
   int vertices = graph.vertices();

   if ( vertices >= congStats.size() )
      congStats.resize( vertices + 1 );

   CongStats &stats = congStats[vertices];

   if ( ranCongruenceCheck )
   {
      stats.total++;
      if ( extnRuns > -1 )
         stats.extnRunsHisto[ (extnRuns - 1) * HIST_BUCKETS / maxExtnRuns ]++;

      if ( congMinimalResult )
         stats.congMinimal++;

#ifndef TEST_CONG_CORRECTNESS
      if ( !congMinimalResult )
         stats.nonminimal++;
      else
#endif
      {

         switch ( result )
         {
            case Message::MinimalByExtn:
            case Message::MinimalByTestset:
               stats.minimal++;
               aassert( congMinimalResult );
               break;
            case Message::MinorCongTestset:
               stats.nonminimal++;
               break;
            case Message::InconclusiveMinimality:
               if ( congMinimalResult )
               {
                  // Congruence could not find any congruent minors, but random distinguisher search
                  // didn't find everything to be noncongruent either
                  cerr << "disagreement on " << vertices << " vertex graph:\n";
                  cerr << graph << nl;
                  cerr << "Random distinguishers could not distinguish minors:\n";

                  Array<MinorNumber> minors;
                  pmp->minorsDistinguishedByDistinguisher( -1, minors );
                  for (int i = 0; i < minors.size(); i++)
                  {
                     const SearchMinor& minor = pmp->minors().get( minors[i] );
                     RBBPG minorGraph( graph );
                     minor.calcMinor( minorGraph );
                     cerr << "   " << minorGraph << nl;
                  }

               }
               break;
            default:
               aassert( false );
         }
      }
   }
}

void Worker1::setResult( Message::WorkResultType _result, int runs )
{
   result = _result;
   extnRuns = runs;
}

bool Worker1::readProblem()
{
   testCongruence = (randGen() < testCongruencePermill);
   ranCongruenceCheck = false;
   congMinimalResult = false;
   setResult( Message::BackDoor );

   // request a problem (announcing ready state at end of work)
   //dispatch << Message::Worker1Request;

    //cerr << "waiting for work\n";
//cerr << "a) nodeNum = " << nodeNum << nl;

   // blocks until work arrives
   //
   dispatch >> nodeNum; 

//cerr << "b) nodeNum = " << nodeNum << nl;

   if (dispatch.eof())
   {
    cerr << "eof -- dispatch connection closed\n";
    return false;
   }

   if (dispatch.fail()) 
   {
    cerr << "fail -- dispatch connection closed\n";
    return false;
   }

   dispatch >> graph;

   if ( verbose ) {
       //cerr << "received " << nodeNum << nl;
       log() << "received " << nodeNum << nl; //" " << graph << nl;
   }
   //log().flush();

#if 0
   if ( nodeNum.isInvalid() )
   {
      dispatch << Message::ProcessFinished;
      return false;
   }
#else
   assert( nodeNum.isValid() );
#endif

   if ( tildeMinorSearch ) return true;

   if ( pmp ) delete pmp;
   pmp = new PartialMinorProof;
   pmp->addAllReductions( graph );

   return true;
}

void Worker1::work()
{
   //cerr << " runOOFCheck()\n";
   if ( runOOFCheck() ) return;

#if 1
   //cerr << " runCanonicTest()\n";
   //if ( runCanonicCheck() ) return;

#else
cerr << " runFEStest()\n";
runFEStest(); return;
#endif

   if ( tildeMinorSearch )
   {
      cerr << " tildeMinorSearch()\n";
      if ( runTildeMinorSearch() ) return;
      else aassert(false);
   }

   if ( univDistSearch )
   {
      cerr << " universalDistinguisherSearch()\n";
      if ( runUniversalDistinguisherSearch() ) return;
      else aassert(false);
   }

   //cerr << " runCongruenceCheck()\n";
   if ( runCongruenceCheck() ) return;

   //cerr << " runRandomExtnSearch()\n";
   if ( runRandomExtnSearch() ) return;

   //if ( runRandomMinorSearch() ) return;

   //cerr << " runTestset()\n";
   if ( useTestset && Family::testsetAvailable() )
      runTestset();
   else
      assumeNonminimal();
}

//--------------------------------------------------------------------

bool Worker1::runCanonicCheck()
{
   // assume quick test has been done

/*
   if ( graph.vertices() > 12 )
   {
      log() << "canonic giveup" << nl;
      return false;
   }
*/

   // run longer pretests

   Dtime timer;
   timer.start();

   // run full check
   int to = canonicTimeout;
   bool rslt = RBBPG::isCanonic( graph, to );
   canonicTime = timer.stop();

   if ( ! rslt )
   {
      if ( timer.user() < MaxEasyCanonicTime ) 
         dispatch << Message::ENoncanonic << nodeNum;
      else
         dispatch << Message::HNoncanonic << nodeNum;

      return true;
   }
   else if (to != canonicTimeout)
   {
      log() << "canonic timout\n";
   }

   return false;
}

//--------------------------------------------------------------------

bool Worker1::runTestset()
{
   Dtime timer;
   timer.start();

   // gets loaded with minor numbers that get distinguished
   // and corresponding test numbers
   Array<MinorNumber> minors;
   Array<int> tests;

   // returns minor number of cong minor, -1 otherwise.
   // if otherwise, arrays are filled
   MinorNumber mNum = Family::applyTestset( graph, *pmp, minors, tests );

   // cerr << *pmp << nl;

   if ( mNum == -1 )
   {
      assert( pmp->proofIsComplete() );
      if ( !testCongruence )
      {
         dispatch << Message::MinimalByTestset << nodeNum;
         if ( sendPMP ) dispatch << *pmp;
      }
      setResult( Message::MinimalByTestset );
   }
   else
   {
      if ( !testCongruence )
      {
         const SearchMinor& minor = pmp->minors().get( mNum );
         dispatch << Message::MinorCongTestset << nodeNum << minor;
      }
      setResult( Message::MinorCongTestset );
   }

   testsetTime = timer.stop();   
   return true;
}

bool Worker1::assumeNonminimal()
{
   if ( !testCongruence )
      dispatch << Message::InconclusiveMinimality << nodeNum;
   setResult( Message::InconclusiveMinimality );
   return true;
}

bool Worker1::runRandomMinorSearch()
{
   return false;
}

bool Worker1::runOOFCheck()
{
  Dtime timer;
  timer.start();

  // no go if graph in family
  if ( (inFamily = Family::member( graph )) )
  {
    oofTime = timer.stop();
    return false;
  }

  bool minimal = outOfFamilyMinimalCheck( graph, *pmp );

  if ( minimal  )
  {
    dispatch << Message::MinimalByOOF << nodeNum;
  }
  else 
  {
    dispatch << Message::NonminimalByOOF << nodeNum;
  }

  oofTime = timer.stop();
  return true;
}

bool Worker1::runCongruenceCheck()
{
  // Check for non-minimal via congruence
  if ( ! Family::congruenceAvailable() ) return false;

  Dtime timer;
  timer.start();

  // Note: this does not modify pmp
  MinorNumber mNum = checkForCongruentMinor( graph, *pmp );
  congTime = timer.stop();

  bool ret = false;

  if ( mNum != -1 )
  {
    const SearchMinor& minor = pmp->minors().get( mNum );
    dispatch << Message::MinorCongDyProg << nodeNum << minor;
    ret = true;
  }
  // no cong minor - check if cong is tight
  else if ( ProbInfo::getBool( ProbInfo::CongIsTight ) )
  {
    dispatch << Message::MinimalByTightCong << nodeNum;
    ret = true;
  }

  if ( testCongruence )
  {
    ranCongruenceCheck = true;
    congMinimalResult = (mNum == -1);
    if ( congMinimalResult )
       // Check minimality results
       ret = false;
#ifdef TEST_CONG_CORRECTNESS
    // Check nonminimal results too
    ret = false;
#endif
  }

  return ret;
}

bool Worker1::runRandomExtnSearch()
{
   Dtime timer;
   timer.start();

   // search results
   Array< Array<MinorNumber> > minors( maxExtnRuns );
   Array<NBBPG> extensions( maxExtnRuns );

   for ( int run=0; run < maxExtnRuns; run++ )
   {
      randomExtensionSearch( graph, *pmp, info, run, 
         &extensions, &minors, 0 );

      if ( pmp->proofIsComplete() )
      {
         setResult( Message::MinimalByExtn, run + 1 );
         if ( !testCongruence )
         {
            dispatch <<  Message::MinimalByExtn << nodeNum;
            if ( sendPMP ) dispatch << *pmp;
         }
         randDistTime = timer.stop();
         return true;
      }
   }

   randDistTime = timer.stop();
   return false;
}

bool Worker1::runUniversalDistinguisherSearch()
{
  Dtime timer;
  timer.start();

  // search results
  bool result;
  int run;
  for ( run = 0; run < maxExtnRuns; run++ )
  {
    result = universalDistinguisherSearch( graph, *pmp, info, run );
    if ( result ) break;
  }

  if ( result )
  {
    log() << "univ dist runs required: " << run+1 << nl;
    dispatch << Message::MinimalByExtn << nodeNum;
  }
  else
  {
    log() << "univ dist not found" << nl;
    dispatch << Message::InconclusiveMinimality << nodeNum;
  }

  univDistTime = timer.stop();
  return true;
}

bool Worker1::runTildeMinorSearch()
{
  Dtime timer;
  timer.start();

  // Check for non-minimal via congruence
  if ( ! Family::congruenceAvailable() ) aassert(false);

  bool result = checkForTildeCongruentMinor( graph, maxExtnRuns, info, &log() );

  if ( result )
  {
    dispatch << Message::InconclusiveMinimality << nodeNum;
  }
  else
  {
    dispatch << Message::MinimalByTightCong << nodeNum;
  }

  tildeMinorTime = timer.stop();
  return true;
}

//-------------------------------------------------------------------


void mainWorker1( int argc, char** argv )
{

   switch (argc)
   {
     case 1: 	worker1.noClock(); break;

     case 2:    {
                  istrstream argIn( argv[1] );
                  int secs;
                  argIn >> secs;
                  aassert( ! argIn.fail() );
                  log() << "Starting with cpu time limit: " << secs << nl;
                  cerr << "Starting with cpu time limit: " << secs << nl;
                  worker1.startClock(secs); 
                }
                  break;

     default:   fatalError( "usage: Worker1 <driver> [CPU time limit] " );
   }

   signal(SIGTERM, Worker1::stopSignal);
   signal(SIGINT, Worker1::stopSignal);
   signal(SIGPIPE, SIG_IGN);
   
   worker1.mainLoop();
}

// --------------------------------------------------------------------

#include "graph/bgraph.h"
#include "general/bitvect.h"

void Worker1::runFEStest()
{
  SearchMinor minor;
  BGraph G(graph);
  
//cerr << "checking graph: " << graph << nl << G << nl;

  vertNum n = G.order();

  BitVector bndry(n); 
  bndry.clearAll();
  int i; for (i=0; i<n; i++) if (G.isBoundary(i)) bndry.setBit(i);

  Array<BitVector> buddies(n);
  //
  for (i=0; i<n; i++) 
  {
    buddies[i].resize(n);
    buddies[i] = G.neighbors(i);
    if (G.isBoundary(i)) { buddies[i] |= bndry; buddies[i].clearBit(i); }
//cerr << "buddies[" << i << "] = " << buddies[i] << nl;
  }

  BitVector empty(n); 
  empty.clearAll();

  for (i=0; i<n-1; i++)
  for (int j=i+1; j<n; j++)
  {
    if (G.isBoundary(i) && G.isBoundary(j)) continue;
    if ( ! G.isEdge(i,j) ) continue;

    //empty.setBit(i); empty.setBit(j);
    //cerr << i << ',' << j << " : " << (buddies[i] & buddies[j]) << " == " << empty << nl;
    //
    if ( (buddies[i] & buddies[j]) == empty )
    {
//cerr << "yes\n";
      dispatch << Message::MinorCongDyProg << nodeNum << minor;
      return;
    }
    //empty.clearBit(i); empty.clearBit(j);
  }

  cerr << " runCanonicTest()\n";
  if ( runCanonicCheck() ) return;
  dispatch << Message::MinimalByTightCong << nodeNum;
  return;
}

