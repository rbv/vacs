
// -------------------------------------------
// --------------- sm_dispatch.c -------------
// -------------------------------------------

/*tex
 
\file{sm_dispatch.c}
\path{src/searchmain}
\title{Search program dispatcher}
\classes{}
\makeheader
 
xet*/

#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <iostream>

#include "family/family.h"
#include "family/probinfo.h"
#include "search/searchmain.h"
#include "vacs/runinfo.h"
#include "vacs/filesystem.h"
#include "search/searchnode.h"
#include "search/search.h"
#include "search/nodeinfo.h"
#include "vacs/message.h"
#include "set/seq.h"
#include "general/stream.h"
#include "general/random.h"

//--------------------------------------------------------------

static class Dispatcher
{
 static bstream cmdFile;

 // ... workers list

public: 

 static void processLoop();

 static void initialize(int fd) { cmdFile.openOnFD(fd); }

 static bstream& sendToManager() { cerr << "sendToManager\n"; return cmdFile; }

};

bstream Dispatcher::cmdFile;

//--------------------------------------------------------------
//---------- Extension functions -------------------------------
//--------------------------------------------------------------

#define NO_EXTENSION_WORK               10
#define EXTEND_PLAN_SIZE                40

//
// Must be static for the callback
//
static struct ExtendItem { int count; NodeNumber n; };

bool operator>( const ExtendItem& e1, const ExtendItem& e2 )
{ return ( e1.count > e2.count ); }
//
bool operator<( const ExtendItem& e1, const ExtendItem& e2 )
{ return ( e1.count < e2.count ); }
//
typedef SortedSequence< ExtendItem > ExtendSS;
//
static ExtendSS* extendSS;
//
static int currentLevel;

//--------------------------------------------------------------
//--------------------------------------------------------------

static void synchronize()
{
   FileManager::lockRequest( LSysR );
   search.sync();
   FileManager::lockRequest( USysR );
}

// Temp function - move to search and fs_nodes
// Return false iff callback returns false.
//
static bool iterateOverNodes(
       SearchNodeIter::Status status, 
       bool (callback)( NodeNumber n ) )
{
   SearchNodeIter I( status );
   NodeNumber n;

   while( ( n = I() ).isValid() )
   {
      if ( ! callback( n ) ) return false;
   }
   return true;
}

// Called for each node in the system.
//
static bool buildUnknownListCallback( NodeNumber n )
{
   
   const SearchNode& sn = search.node( n );
   aassert( sn.status() == SearchNode::Unknown );

   if ( currentLevel == -1 ) // 1st call
   {
      currentLevel = sn.depth();
   }
   else
   {
      if ( sn.depth() > currentLevel ) return false; // cancel iterator
   }

   ExtendItem k;
   UnknownState st;
   search.unknownState( n, st );
   k.count = st.extensionSearches();
   k.n = n;

   if ( extendSS->size() >= EXTEND_PLAN_SIZE ) 
   {
      if ( k < extendSS->max() )
      {
         extendSS->delMax();
         extendSS->insert( k );
         //log() << " i" << n << nl;
      }
   }
   else
   {
      extendSS->insert( k );
      //log() << " a" << n << nl;
   }

   return true; // continue search
}

static void buildUnknownList()
{
   if ( !extendSS ) extendSS = new ExtendSS;

   extendSS->clear();

   currentLevel = -1;
   iterateOverNodes( SearchNodeIter::Unknown, buildUnknownListCallback );

   // randomize the array, if not empty
   //
   int s = extendSS->size();
   if ( s )
   {
      RandomInteger rand( s );
      for ( int i=0; i<s; i++ )
         extendSS->swap( i, rand() );
   }
      
}

static NodeNumber nextUnknownNode()
{
   //log() << "choosing extension...";

   if ( !extendSS || extendSS->empty() )
   {
        //log() << "queue empty...sync
	synchronize();

	buildUnknownList();

	if (extendSS->size()) 
	{
	  log() << "queue empty...refilling...";
	  for ( int i=0; i<extendSS->size(); i++ )
	     log() << extendSS->get(i).n << ' ';
	  log() << nl;
	}

	//log() << "Sync - ext search" << nl;
	sleep( NO_EXTENSION_WORK );
        // 
	//synchronize(); -- do before refilling
   }

   if ( extendSS->empty() ) return NodeNumber();

   const ExtendItem& s = extendSS->min();
   NodeNumber n = s.n;

   extendSS->delMin();
   return n;
}

//--------------------------------------------------------------
//--------------------------------------------------------------

static int extnTriesBeforeTestset;
static int extnTriesBeforeNonminimal;

static bool checkExtnExceeded( NodeNumber n )
{
   FileManager::lockRequest( LSysR );

   SearchNode s;
   search.readNoCache( n, s );

   //log() << "Too many Extns check: " << n << nl;

   if ( s.status() != SearchNode::Unknown )
   {
      log() << "Node " << n << " not unknown...no extn check try" << nl;
   
      FileManager::lockRequest( USysR );
      return true;
   }

   UnknownState st;
   search.unknownState( n, st );

   //cerr << "unknownState: " << n  << ' ' << st.extensionSearches() << nl;

   if ( extnTriesBeforeNonminimal != -1 )
      if ( st.extensionSearches() >= extnTriesBeforeNonminimal )
      {
         //SearchControl::nodeNonminimal( n );
	 assert(false);
         //DatabaseBase::unlockSystem();
         FileManager::lockRequest( USysR );
         return true;
      }

   if ( Family::testsetAvailable() )
      if ( extnTriesBeforeTestset != -1 )
         if ( st.extensionSearches() >= extnTriesBeforeTestset )
         {
            PartialMinorProof proof;
            search.unknownStatePMP( n, proof );
            RBBPG G;
            search.graph( n, G );
            //DatabaseBase::unlockSystem();
            FileManager::lockRequest( USysR );

            log() << "Testset: " << n << nl;

            // gets loaded with minor numbers that get distinguished
            // and corresponding test numbers
            Array<MinorNumber> minors;
            Array<int> tests;

            // returns minor number of cong minor, -1 otherwise.
            // if otherwise, arrays are filled
            MinorNumber congMinor = 
		Family::applyTestset( G, proof, minors /* , tests */);

            if ( congMinor == -1 )
            {
              //assert( minors.size() != 0 ); -- root has no minors
              //SearchControl::nodeMinorsDistTestset( n, minors, tests );
              Dispatcher::sendToManager() << (uchar) 
		Message::MinorsDistTestset << n << minors << tests;
            }
            else
            {
              //SearchControl::nodeMinorCongTestset( n, congMinor );
              Dispatcher::sendToManager() << (uchar)
		Message::MinorCongTestset << n << congMinor;
            }

            return true;
         }

   // node's not for us...
   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

   return false;
}

//--------------------------------------------------------------
//--------------------------------------------------------------

/*
 * Main loop checking for new worker clients and issues work until
 * manager closes connection.
 */ 
void Dispatcher::processLoop()
{
   NodeNumber n;

   while (true)
   {
     if ( ( n = nextUnknownNode() ).isInvalid() )
     {
        //log() << "no extension nodes" << nl;
	//sleep( NO_EXTENSION_WORK ); // sleep again...
        continue;
     }

     if ( checkExtnExceeded( n ) ) continue;

assert(false); // testing
   }
}

//--------------------------------------------------------------
// 			MAIN entry point
//--------------------------------------------------------------

void mainDispatch( int argc, char** argv )
{
   assert(argc==2);

   // tmp ?
   extnTriesBeforeTestset = 0;
   //  ProbInfo::getInt( ProbInfo::ExtnTriesBeforeTestset );
   extnTriesBeforeNonminimal = -1;
   //  ProbInfo::getInt( ProbInfo::ExtnTriesBeforeNonminimal );

cerr << "tries = " <<    extnTriesBeforeTestset << ' ' <<
	extnTriesBeforeNonminimal << nl;

cerr << "argv[1][0] = " << (int) argv[1][0] << nl;

   // First argument should be the manager's file discriptor channel.
   //
   Dispatcher::initialize( (int) (argv[1][0]) );

   Dispatcher::processLoop();
}

//------------------------------------------------------------------------
// *** * ***** *  ***  *  ******** * **** ***  ****** ***** *** ***** * **
//------------------------------------------------------------------------

#ifdef OLD_DISPATCHER_STUFF

please get from backup 

void mainDispatch( int argc, char** argv ) // sorry, half-way hacked (*mjd*)!!
...

#endif
