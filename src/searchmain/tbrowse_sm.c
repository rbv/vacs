
// -------------------------------------------
// --------------- sm_tbrowse.c --------------
// -------------------------------------------

/*tex
 
\file{sm_tbrowse.c}
\path{src/searchmain}
\title{Text-based browser}
\classes{}
\makeheader
 
xet*/

#include "search/searchmain.h"

#include "set/set.h"
#include "general/str.h"
#include "error.h"
#include "search/searchnode.h"
#include "search/nodeinfo.h"
#include "search/search.h"
#include "family/probinfo.h"

#include "bpg/bpgutil.h"

#include "vacs/runinfo.h"
#include "vacs/filesystem.h"

#include "array/sortarray.h"
#include "array/psortarray.h"

static void tbrowseMainLoop( const char* cmd = NULL );
static bool printNode(const Str&);

static bool TBTextM = false;

void mainTbrowse( int argc, char** argv )
{
   if ( argc > 1 && !strcmp( argv[1], "textm" ) )
   {
      TBTextM = true;
      argc--;
      argv[1] = argv[0];
      argv++;
   }

   if ( argc == 1 ) { tbrowseMainLoop(); return; }

   if ( argc == 2 ) { tbrowseMainLoop( argv[1] ); return; }

#if 0
   if ( argc == 2 ) 
    { 
      Str s(argv[1]); 
      
      if ( ! printNode(s) ) fatalError( "Optional 1st Arg not an integer" );
      else return;
     }
#endif

   fatalError( "Too many args to textdump" );
}

//-----------------------------------------------------------

static void minCheck1()
{
   cout << "Check for minimal nodes with no minimal children." << nl << nl;
   cout << "Iterating over minimal grown nodes..." << nl;
   
   //DatabaseBase::lockSystem();
   FileManager::lockRequest( LSysR );

   SearchNodeIter I( SearchNodeIter::Minimal, SearchNodeIter::Grown );

   NodeNumber n;
   Array<NodeNumber> children;

   while ( (n=I()).isValid() )
   {
      cout << "checking " << n << "...";
      bool ok = false;
      search.children( n, children );

      for ( int i=0; i<children.size(); i++ )
      {
         if ( search.node( children[i] ).status() == SearchNode::Minimal )
         { ok=true; break; }

         if ( search.node( children[i] ).status() != SearchNode::Irrelevant )
            continue;

         // check for isomorph
         IrrelevantInfo info;
         search.irrelevantInfo( children[i], info );

         if ( info.type() != IrrelevantInfo::Isomorphic ) 
            continue;

         SearchNode& iso =  search.node( info.isomorphic() );

         if ( iso.status() == SearchNode::Irrelevant )
         { cout << "misformed tree..."; continue; }

         if ( iso.status() == SearchNode::Minimal )
         {
            ok=true;
            break;
         }
      }

      if ( ok )
      {
         cout << "ok" << nl;
      }
      else
      {
         cout << "no minimal children" << nl;
      }
   }

   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

}

//-----------------------------------------------------------


static void help()
{
   cout << "Available commands for `tb [textm] {n}':" << nl;
   cout << "   ?              this help" << nl;
   cout << "   q              quit textdump" << nl;
   cout << "   M              min check" << nl;
   cout << "   {n}            dump node #n" << nl;
   cout << nl;
}


//-----------------------------------------------------------

static void tbrowseMainLoop( const char* cmd )
{
   while(1)
   {
      char in[81];

      if ( cmd )
      {
         strcpy( in, cmd );
         cmd = "q";
      }
      else
      {
         cout << nl;
         cout << "Enter command (? for help): ";
         cin.getline( in, 80 );
      }

      Str s(in);

      if ( s.words() == 0 ) s = "q";
      
      if ( s.words() != 1 )
      {
         cerr << "input error - one token only" << nl; continue;
      }

      s = s.word( 1 );
      s.lowerCase();

      if ( printNode(s) ) continue;

      if ( s == "q" || s == "quit" || s == "exit" ) break;
      if ( s == "m" ) { minCheck1(); continue; }
      if ( s == "?" ) { help(); continue; }

      cerr << "Unknown command" << nl;
   }

}

static bool printNode(const Str &s)
 {
      int nodenum;
      //
      if ( s.asInteger( nodenum ) )
       {
         NodeNumber n = nodenum;
         //DatabaseBase::lockSystem();
	 FileManager::lockRequest( LSysR );

         if ( search.isValid( n ) )
         {
            const SearchNode& s = search.node( n );
            cout << '\n';
            search.printNode( cout, n, 2 );

            //mjd debug
	    if ( ProbInfo::getBool( ProbInfo::DumpCongruence ) )
             {
                 RBBPG G;
                 search.graph( n, G);
		 if ( TBTextM )
                    PartialMinorProof::dumpCongruenceStates( cout, G ); 
             }
         }
         else
            cout << "Node not in system" << nl;

         //DatabaseBase::unlockSystem();
	 FileManager::lockRequest( USysR );
         return true;
       }
      else return false;
 }

