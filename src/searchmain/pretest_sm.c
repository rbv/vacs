 
// -------------------------------------------
// ---------------- sm_pretest.c -------------
// -------------------------------------------
 
/*tex
 
\file{sm_pretest.c}
\path{src/searchmain}
\title{Main program to run pretests on all nodes}
\classes{}
\makeheader
 
xet*/

#include "search/searchmain.h"
#include "search/searchnode.h"
#include "search/search.h"
#include "family/family.h"
#include "error.h"

#if 0

void mainPretest( int argc, char** )
{

   if ( argc != 1 ) fatalError( "pretest takes no arguments. " );

   SearchIter I( SearchIter::Unknown );
   SearchNode* s;

   int tNum; // test number

   aassert( false ); // not changed to new pretest system yet...
/*
   while ( s = search.node( I() ) )
   {
      if ( Family::nonminimalPretest( s->graph(), tNum ) )
      {
         // node is nonminimal
         //
         //SearchControl::nodeIsNonminimalByPretest( s->number(), tNum );
      }

      // remove from memory
      //
      search.swapOut( s->number() );
   }
*/
}

#endif
