 
// -------------------------------------------
// ---------------- sm_extend.c --------------
// -------------------------------------------
 
/*tex
 
\file{sm_extend.c}
\path{src/searchmain}
\title{*OBSOLETE* Main program to run extension searches on nodes}
\classes{}
\makeheader
 
xet*/

#if 0

#include "family/probinfo.h"
#include "search/searchmain.h"
#include "search/searchcontrol.h"
#include "search/searchnode.h"
#include "search/searchutil2.h"
#include "search/searchminor.h"
#include "search/nodeinfo.h"
#include "search/search.h"

#include "family/family.h"
#include "vacs/filesystem.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "error.h"

//static int ExtnEdgeWeight;
//static int ExtnRuns;
//static int ExtnTries;
//static int ExtnMaxLength;
//static int ExtnFlags;


//---------------------------------------------------------------

void mainExtend( int argc, char** argv )
{

   if ( argc != 2 ) fatalError( "extend takes two arguments. " );

   // parse 1st arg
   //
   if ( argc == 1 ) 
      fatalError( "missing node number" );

   int nn;
   if ( sscanf( argv[1], "#%d", &nn ) != 1 )
      fatalError( "parse error on node num" );

   NodeNumber nodeNum( nn );

   // shift args
   argc--;
   argv[1] = argv[0];
   argv++;
   if ( argc != 1 ) fatalError( "Extra param" );

   // get the driver params we need
   //
   int runs = ProbInfo::getInt( ProbInfo::ExtnRuns );
   randomExtensionSearchInfo info;
   info.ExtnEdgeWeight = ProbInfo::getInt( ProbInfo::ExtnEdgeWeight );
   info.ExtnTries = ProbInfo::getInt( ProbInfo::ExtnTries );
   info.ExtnMaxLength = ProbInfo::getInt( ProbInfo::ExtnMaxLength );

   if ( ProbInfo::getBool( ProbInfo::Connected ) )
      info.ExtnFlags |= ExtensionConnected;
   if ( ProbInfo::getBool( ProbInfo::KeepTree ) )
      info.ExtnFlags |= ExtensionTree;
   

   log() << "runs        " << runs << nl;
   log() << "edge weight " << info.ExtnEdgeWeight << nl;
   log() << "ext tries   " << info.ExtnMaxLength << nl;
   log() << "flags       " << info.ExtnFlags << nl;

   DatabaseBase::lockSystem();
   SearchNode s = search.node( nodeNum );
   PartialMinorProof proof;
   search.unknownStatePMP( nodeNum, proof );
   RBBPG G;
   search.graph( nodeNum, G );
   DatabaseBase::unlockSystem();

   log() << "Node: " << nl << s << nl;

   if ( s.status() != SearchNode::Unknown )
   {
      log() << "Node status not unknown...nothing to do!" << nl;
      return;
   }

   Array<NBBPG> extensions( runs );
   Array< Array<MinorNumber> > minors( runs );

   int badCount = 0;
   //
   for ( int run=0; run<runs; run++ )
   {
      log() << "--------- run " << run << "-----------"  << nl;
      randomExtensionSearch( G, proof, info, run, 
         &extensions, &minors, &(log()) );

      if ( proof.proofIsComplete() ) break;
   }

   DatabaseBase::lockSystem();
   for ( run=0; run<runs; run++ )
   {
      if ( minors[run].size() != 0 )
      {
         SearchControl::nodeIsDistFromMinors( nodeNum, 
            minors[run], extensions[run] );
      }
      else
      {
         badCount++;
      }
   }

   if ( badCount ) SearchControl::nodeDistSearchFailed( nodeNum, badCount );

   DatabaseBase::unlockSystem();

}


#endif
