
/*
"../inc/../src/searchmain/isoclean_sm.c", line 79.25: 1540-055: (S)
"Array<RBBPG*>" cannot be converted to "const Array<RBBPG>&".
"../inc/../src/searchmain/isoclean_sm.c", line 79.25: 1540-306: (I) The
previous message applies to argument 1 of function
"IsoDBItem__compute(const Array<RBBPG>&,const
Array<char>&,Array<IsoDBItem1>&,Array<IsoDBItem2>&)".
gmake: *** [_searchmain2.o] Error 1
*/

#include "family/family.h"
#include "general/str.h"
#include "bpg/bpgutil.h"
#include "search/search.h"
#include "search/proof_db.h"
#include "search/searchminor.h"


void mainIsoClean( int argc, char** argv )
{
  if ( argc != 3 ) { cerr << "2 args\n"; return; }

  bistream minIn( argv[1] );
  bistream nonminIn( argv[2] );
/*
  bostream minOut( argv[3] );
  bostream nonminOut( argv[4] );
*/

  if ( minIn.fail() || nonminIn.fail() )  // !minOut ||  || !nonminOut
  { cerr << "file problem!!\n"; return; }

  MinimalProof mProof;
  NonminimalProof nProof;
  RBBPG graph;
  PartialMinorProof pmp;

  Array< RBBPG > mGraphs;
  Array< RBBPG > nGraphs;

  cerr << "Loading minimals...";
  while(1)
  {
    minIn >> mProof;
    if ( mProof.type() == MinimalProof::None ) break;
    minIn >> graph;
    if ( mProof.hasPMP() ) minIn >> pmp;
    mGraphs.append( graph );
  }
  cerr << "found " << mGraphs.size() << nl;

  cerr << "Loading nonminimals...";
  while(1)
  {
    nonminIn >> nProof;
    if ( nProof.type() == NonminimalProof::None ) break;
    nonminIn >> graph;
    nGraphs.append( graph );
  }
  cerr << "found " << nGraphs.size() << nl;

  Array< bool > mKillList( mGraphs.size() );
  Array< bool > nKillList( nGraphs.size() );

  mKillList.fill(false);
  nKillList.fill(false);

  cerr << "Checking minimals internally" << nl; 
  {
    Array< bool > mask( mGraphs.size() );
    Array<int> isoResult( mGraphs.size() );
    Array<IsoDBItem1>  isoReps1( mGraphs.size() );
    Array<IsoDBItem2>  isoReps2( mGraphs.size() );

    mask.fill( true );
    IsoDBItem__compute( mGraphs, mask, isoReps1, isoReps2 );
      
    IsoDBItem__checkInternalIso( mGraphs, isoReps1, isoReps2, mask, isoResult, 0 );

    for ( int i=0; i<mGraphs.size(); i++ )
      if ( isoResult[i] != -1 )
        if ( mGraphs[i] < mGraphs[isoResult[i]] )
          mKillList[isoResult[i]] = true;
        else
          mKillList[i] = true;
  }


  cerr << "Checking minimals against nonminimals" << nl; 

#if 0
  cerr << "Checking prefixes" << nl;

  while(1)
  {
    PartialMinorProof pmp;

    int proofType;
    cin >> proofType;
    MinimalProof proof( (MinimalProof::ProofType) proofType );
    proof.notUsed();

    if ( !cin ) break;

    RBBPG graph;
    char str[ 500 ];
    cin.getline( str, 500 );
    RBBPG::fromString( str, graph );

    s << proof;
    s << graph;
    if (proof.hasPMP()) s << pmp;
  }

  MinimalProof proof( MinimalProof::None );
  s << proof;

#endif

  IsoStats::statistics( cerr );
}

