
// -------------------------------------------
// --------------- sm_stats.c ----------------
// -------------------------------------------

/*tex
 
\file{sm_stats.c}
\path{src/searchmain}
\title{Program to compile and print search statistics}
\classes{}
\makeheader
 
xet*/

#include "search/searchmain.h"

#include "set/set.h"
#include "error.h"
#include "search/searchminor.h"
#include "search/nodeinfo.h"
#include "search/searchnode.h"
#include "search/search.h"
#include "family/family.h"
#include "bpg/bpgutil.h"
#include "general/str.h"

#include "vacs/runinfo.h"
#include "vacs/filesystem.h"

#undef loop

static void mainLoop();

class LevelStats {

   bool showVertices;  // compute stats by graph order
   bool showNB;        // obstructions only: count number of \delta-obstructions graphs which are actual obstructions
   bool showIso;       // count number of unlabelled graphs found
   bool showGraphs;    // print one graph from each isomorphism class

   // What kind of nodes to mark as 'useful'
   enum {
       useNone,
       useObst,        // Obstructions
       useUnique       // First occurrence of each graph
   } useType;
   const char *useStr; // "Obst" or "Unique"

   //SearchNode::Status type;
   ExtType type;
   SearchNodeIter iter;
   const char *status;

   IsoTester *isoTest;

   struct rec
   {
      int total, status, nonbndy, noniso, used;

      rec() : total(0), status(0), nonbndy(0), noniso(0), used(0) {}
      void operator+=( const rec &rhs )
      {
         total += rhs.total;
         status += rhs.status;
         nonbndy += rhs.nonbndy;
         noniso += rhs.noniso;
         used += rhs.used;
      }
   };

   DyArray<rec> levelStats;  // stats by node depth
   DyArray<rec> vertexStats; // stats by number of vertices
   DyArray<int> copyCounts;     // number of times each item in isoDB is used

   BitVector usedNodes;    // useType != None only: mark ancestors of \delta-obstructions. Big, but not resizeable

   void usage();
   void mainloop();

   void markUsed( NodeNumber n );
   void computeObstUsage();
   void update( NodeNumber n );
   void updateUse( NodeNumber n, int level, int vertices );

   void print( int level, const char* label, rec r, bool printTotals );
   void printArray( const char *label, Array<rec> &statArray, bool printTotals );
   void printStats();

public:
   void main( int argc, char **argv );

   LevelStats() : isoTest(NULL), usedNodes(0), iter(SearchNodeIter::SAny), copyCounts(0, 50000) {}
   ~LevelStats();
};

//-------------------------------------------

void LevelStats::usage()
{
   cerr << "usage: levels <type> [vertices] [iso] [use[=obst|=uniq]]\n"
           "where <type> is one of:\n"
           " a -- all \n"
           " m -- minimal \n"
           " n -- nonminimal \n"
           " u -- unknown \n"
           " r -- relevant \n"
           " i -- irrelevant \n"
           " o -- \\delta-obstructions \n"
           "vertices: Tally by number of vertices \n"
           "iso: Find number of non-boundaried and non-isomorphic non-boundaried graphs \n"
           "use: Count number of ancestors of 'useful' nodes: first occurrence of each \n"
           "     unboundaried graph (default), or \\delta-obstructions \n";
   fatalError( "Bad arguments" );
}

//-------------------------------------------

void LevelStats::main( int argc, char** argv )
{
   showVertices = false;
   showNB = false;
   showIso = false;
   showGraphs = false;
   useType = useNone;

   if ( argc < 2 || strlen( argv[1] ) != 1 )
      usage();

   switch ( argv[1][0] )
   {
/*
      case 'm':
          type = SearchNode::Minimal;
          iter = SearchNodeIter( SearchNodeIter::SAny );
          break;
      case 'n': type = SearchNode::Nonminimal; break;
      case 'u': type = SearchNode::Unknown; break;
      case 'i': type = SearchNode::Irrelevant; break;
*/
      case 'a':
          type = AllNodes;
          iter = SearchNodeIter( SearchNodeIter::SAny );
          status = "All nodes";
          break;
      case 'r':
          type = Relevant;
          iter = SearchNodeIter( SearchNodeIter::Relevant );
          status = "Relevant";
          break;
      case 'm':
          type = Minimal;
          iter = SearchNodeIter( SearchNodeIter::Minimal );
          status = "Minimal";
          break;
      case 'n':
          type = Nonminimal;
          iter = SearchNodeIter( SearchNodeIter::Nonminimal );
          status = "Nonminimal";
          break;
      case 'i':
          type = Irrelevant;
          iter = SearchNodeIter( SearchNodeIter::Irrelevant );
          status = "Irrelevant";
          break;
      case 'u':
          type = Unknown;
          iter = SearchNodeIter( SearchNodeIter::Unknown );
          status = "Unknown";
          break;
      case 'o':
          type = Obst;
          iter = SearchNodeIter( SearchNodeIter::Minimal, SearchNodeIter::GAny, SearchNodeIter::OutOfFamily );
          showNB = true;
          status = "Obstructions";
          break;
      default: usage();
   }
   argc--;
   argv++;

   while ( argc > 1 )
   {
      if ( !strcmp( argv[1], "iso" ) )
      {
          showIso = true;
      }
      else if ( !strncmp( argv[1], "use", 3 ) )
      {
          if ( !strncmp( argv[1] + 3, "=obst", 5 ) )
          {
              useType = useObst;
              useStr = "Obst";
          }
          else
          {
              useType = useUnique;
              useStr = "Unique";
          }
          usedNodes.resize( search.numberOfNodes() );
      }
      else if ( !strcmp( argv[1], "vertices" ) )
      {
          showVertices = true;
      }
      else if ( !strcmp( argv[1], "graphs" ) )
      {
          showGraphs = true;
      }
      else
         usage();
      argc--;
      argv++;
   }

   if ( argc != 1 )
      usage();

   if ( useType != useNone && type == Obst )
      fatalError( "Can't show usage when tallying obstructions" );

   if ( showGraphs && !showIso )
      fatalError( "'graphs' requires 'iso'" );

   if ( showIso || useType == useUnique )
   {
       isoTest = new IsoTester;
   }

   //levelStats.resize( 50 );
   //vertexStats.resize( 50 );
   mainloop();
}

LevelStats::~LevelStats()
{
   if ( isoTest )
       delete isoTest;
}

void mainLevels( int argc, char** argv )
{
   LevelStats stats;
   stats.main( argc, argv );
}

//-------------------------------------------

void LevelStats::mainloop()
{
   SearchNodeIter iter2( SearchNodeIter::SAny );

   NodeNumber n;

   //DatabaseBase::lockSystem();
   FileManager::lockRequest( LSysR );

   while ( ( n = iter2() ).isValid() )
   {
       const SearchNode& s = search.node( n );
       int level = s.depth();
       levelStats.auto_ref( level ).total++;
   }

   if ( useType == useObst )
      computeObstUsage();

   // We iterate backwards so that we can mark ancestors of 'useful' nodes
   // in the same pass.
   iter.end();
   while ( ( n = iter.prev() ).isValid() )
   {
      if ( ! ( n.asInt() % 50000 ) ) cerr << n << nl;
      update( n );
   }
   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

   printStats();
}

//-------------------------------------------

void LevelStats::markUsed( NodeNumber n )
{
    for (;;) {
        SearchNode &sn = search.node( n );
        usedNodes.set_bit( n.asInt() );
        if ( sn.depth() == 0 ) break;
        assert( sn.parent().asInt() < n.asInt() );
        n = sn.parent();
    }
}

void LevelStats::computeObstUsage()
{
   MinimalProofDatabase& db = search.minimalProofDatabase();
   cerr << "Scanning db..." << nl;

   int sz = db.size();
   for ( int ref=0; ref < sz; ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      if ( ref % 50000 == 0 ) cerr << "db ref: " << ref << nl;

      MinimalProof proof;
      db.getInfo( ref, proof );
   
      if ( proof.type() != MinimalProof::OutOfFamily ) continue;

      NodeNumber n = proof.usedBy();
      if ( n == -1 )
      {
         cerr << "skipping unused proof " << ref << nl;
         continue;
      }

      markUsed( n );
   }
}

//-------------------------------------------

void LevelStats::updateUse( NodeNumber n, int level, int vertices )
{
    if ( usedNodes.get_bit( n.asInt() ) )
    {
        levelStats.auto_ref( level ).used++;
        vertexStats.auto_ref( vertices ).used++;
    }
}

void LevelStats::update( NodeNumber n )
{
   const SearchNode& s = search.node( n );
   int level = s.depth();
   int vertices = 0;
   RBBPG G;

   if ( showIso || useType == useUnique || showVertices )
   {
      search.graph( n, G );
      vertices = G.vertices();
   }

   levelStats.auto_ref( level ).status++;
   vertexStats.auto_ref( vertices ).status++;  // May be garbage if not in use; no matter

   int m = s.membership();

   if ( showIso || useType == useUnique )
   {
      int isoref = isoTest->find( G );
      if ( isoref == -1 )
      {
         // Not in DB
         levelStats.auto_ref( level ).noniso++;
         vertexStats.auto_ref( vertices ).noniso++;
         //if ( levelStats.auto_ref( level ).noniso == 54 )
         //    cout << G << nl;
         isoref = isoTest->add( G );
         if ( showGraphs )
             cout << vertices << ' ' << G << nl;
         if ( useType == useUnique )
             markUsed( n );
      }
      copyCounts.auto_ref( isoref )++;
   }

   if ( useType != useNone )
       updateUse( n, level, vertices );

/*
   switch ( s.status() )
   {
      case SearchNode::Minimal:
      {
         S.Minimal[m]++;
         MinimalProof proof;
         search.minimalProof( n, proof );
         switch( proof.type() )
         {
            case MinimalProof::External:    S.MExternal[m]++; break;
            case MinimalProof::DistinguisherProof: 
                                            S.DistinguisherProof[m]++; break;
            case MinimalProof::TightCong:   S.CongTight[m]++; break;
            case MinimalProof::Testset:     S.MTestset[m]++; break;
            case MinimalProof::OutOfFamily: S.MOutOfFamily[m]++; break;
            case MinimalProof::Root:        S.MRoot[m]++; break;
            default: aassert( false );
         }
         switch( s.growStatus() )
         {
            case SearchNode::Growable:    S.MGrowable[m]++; break;
            case SearchNode::NotGrowable: S.MNotGrowable[m]++; break;
            case SearchNode::Grown:       S.MGrown[m]++; break;
            default: aassert( false );
         }
         break;
      }

      case SearchNode::Unknown:
      {
         S.Unknown[m]++;
         switch( s.growStatus() )
         {
            case SearchNode::Growable:    S.UGrowable[m]++; break;
            case SearchNode::NotGrowable: S.UNotGrowable[m]++; break;
            case SearchNode::Grown:       S.UGrown[m]++; break;
            default: aassert( false );
         }
         break;
      }

      case SearchNode::Irrelevant:
      {
         S.Irrelevant[m]++;
         IrrelevantInfo info;
         search.irrelevantInfo( n, info );
         switch( info.type() )
         {
            case IrrelevantInfo::ENoncanonic:  S.ENoncanonic[m]++; break;
            case IrrelevantInfo::QNoncanonic:  S.QNoncanonic[m]++; break;
            case IrrelevantInfo::NonTree:      S.Nontree[m]++; break;
            case IrrelevantInfo::MultiGraph:   S.MultiGraph[m]++; break;
            case IrrelevantInfo::Isomorphic:   S.Isomorphic[m]++; break;
            case IrrelevantInfo::IsoSibling:   S.IsoSibling[m]++; break;
            case IrrelevantInfo::Disconnected: S.Disconnected[m]++; break;
            case IrrelevantInfo::Prefix:       S.Prefix[m]++; break;
            case IrrelevantInfo::OutOfFamily:  S.IOutOfFamily[m]++; break;
            case IrrelevantInfo::Pretest:      S.Pretest[m]++;
                   APretestStats[ info.pretest() ][m]++;
                   break;
            default: aassert( false );
         }
         break;
      }

      case SearchNode::Nonminimal:
      {
         S.Nonminimal[m]++;
         NonminimalProof proof;
         search.nonminimalProof( n, proof );
         switch( proof.type() )
         {
            case NonminimalProof::HNoncanonic:    S.HNoncanonic[m]++; break;
            case NonminimalProof::External:       S.NExternal[m]++; break;
            case NonminimalProof::CongruentMinor: S.CongruentMinor[m]++; break;
            case NonminimalProof::Testset: 	  S.NTestset[m]++; break;
            case NonminimalProof::Parent:         S.Parent[m]++; break;
            case NonminimalProof::OutOfFamily:    S.NOutOfFamily[m]++; break;
            case NonminimalProof::GiveUp:         S.NGiveUp[m]++; break;
            case NonminimalProof::NotSaved:       S.NNotSaved[m]++; break;
            default: aassert( false );
         }
         switch( s.growStatus() )
         {
            case SearchNode::Growable:    S.NGrowable[m]++; break;
            case SearchNode::NotGrowable: S.NNotGrowable[m]++; break;
            case SearchNode::Grown:       S.NGrown[m]++; break;
            default: aassert( false );
         }
         break;
      }

      default: aassert( false );

   } 

   if ( s.status() == SearchNode::Irrelevant ) return;
*/
}

//-------------------------------------------

/*
   PCollection<NBBPG> obst;

   MinimalProofDatabase& db = search.minimalProofDatabase();

   cerr << "Scanning db..." << nl;
   int count = 0;
   int sz = db.size();
   for ( int ref=0; ref < sz; ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      if ( ref % 2000 == 0 ) cerr << "db ref: " << ref 
        << ", count = " << count << nl;

      MinimalProof proof;
      db.getInfo( ref, proof );

      RBBPG G;
      search.graph( proof.usedBy(), G );
   
      if ( proof.type() != MinimalProof::OutOfFamily ) continue;

      count++;

      // do a type conversion from RBBPG to BPG
      //
      NBBPG* nbG = new NBBPG( G );

      //cerr << "Checking...";
      bool isObs = isObstruction(*nbG);
      //cerr << "done" << nl;

      if ( isObs )
      {
//         cerr << "Adding...";
         obst.add( nbG );
//         cerr << "done" << nl;
      }
   }

   //cerr << obst << nl;

   if (ostr) *ostr << "Boundaried obstructions:     " << count << nl;
   if (ostr) *ostr << "Nonboundaried obstructions(preiso): " << obst.size() << nl;

   cerr << "Removing iso copies..." << nl;

*/

//-------------------------------------------

void LevelStats::print( int level, const char* label, rec r, bool printTotals )
{
   Str s;

   if ( level > 1 ) s.append("   ");

   s.append( label );
   s.padTo( 12 );
   if ( printTotals )
      s.append( Str( r.total ).padLeft(10) );
   s.append( Str( r.status ).padLeft(15) );
   if ( showNB )
       s.append( Str( r.nonbndy ).padLeft(20) );
   if ( showIso )
       s.append( Str( r.noniso ).padLeft(15) );
   if ( useType != useNone )
       s.append( Str( r.used ).padLeft(15) );

   cout << s << nl;
}

void LevelStats::printArray( const char *label, Array<rec> &statArray, bool printTotals )
{
   cout << "            ";
   if ( printTotals )
   cout << "     Total";
   cout << Str( status ).padLeft(15);
   if ( showNB )
      cout << " Non-boundaried and...";
   if ( showIso )
      cout << "      Non-isom.";
   if ( useType != useNone )
      cout << "           Used (" << useStr << ")";
   cout << nl;

   rec totals = rec();
   for (int i = 0; i < statArray.size(); i++)
       totals += statArray[i];

   print( 1, "Total", totals, printTotals );
   for (int i = 0; i < statArray.size(); i++)
   {
       char buf[16];
       snprintf( buf, 16, "%s %d", label, i );
       print( 1, buf, statArray[i], printTotals );
   }
}              

void LevelStats::printStats()
{
    printArray( "Level", levelStats, true );
   cout << nl;
   if ( showVertices )
       printArray( "Order", vertexStats, false );

   if ( !showIso )
       return;

   cout << "\nHistogram, number of copies per unlabelled graph\n";

   DyArray<int> hist;

   for (int i = 0; i < copyCounts.size(); i++)
   {
       // Defaults to 0
       hist.auto_ref( copyCounts[i] )++;
   }
   for (int i = 0; i < hist.size(); i++)
   {
       cout << i << '\t' << hist[i] << nl;
   }

}
