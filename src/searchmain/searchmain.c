
// -------------------------------------------
// ------------- searchmain.c ----------------
// -------------------------------------------

/*tex
 
\file{searchmain.c}
\path{src/searchmain}
\title{Main search program}
\classes{}
\makeheader

\begin{desc}
This is the entry point for all of the VACS main programs,
except the browser.
It processes the first argument of the command line as the 
driver file name, and then switches on \$0.
\end{desc}
 
xet*/

#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include "search/searchmain.h"

#include "search/search.h"
#include "vacs/runinfo.h"
#include "vacs/filesystem.h"
#include "error.h"

// Table of program options vs. entry points.
//
//
typedef void (*ProgramEntryPoint)( int argc, char** argv );
//
struct ProgramTableEntry
{
   const char*       programName; 
   ProgramEntryPoint programEntryPoint;
   RunInfo::RunType  runType;
};

static ProgramTableEntry programTable[] =
{
   "Manager",  mainManager,  RunInfo::Manager,
   "Dispatch", mainDispatch, RunInfo::Dispatch,
   "Stats",    mainStats,    RunInfo::Browser,
   "Levels",   mainLevels,   RunInfo::Browser,
   "Text",     mainText,     RunInfo::Browser,
   "Tbrowse",  mainTbrowse,  RunInfo::Browser,
   "Extract",  mainExtract,  RunInfo::Browser,
   "Worker1",  mainWorker1,  RunInfo::Worker,
   "Human1",   mainHuman1,   RunInfo::Worker,
   "Testset",  mainTestset,  RunInfo::Worker,
   "IsoClean", mainIsoClean, RunInfo::Worker,
   "Play",     mainPlay,     RunInfo::Worker,
   0, 0, RunInfo::Worker
};

static void parseCommandLine(
   int& argc, 
   char **&argv, 
   char*& driverFilename,
   ProgramEntryPoint& entry,
   RunInfo::RunType& runType

);

int main( int argc, char* argv[] )
{

   // check for the required number of args on the command line
   // this is a static function in main.c
   // set the driver filename first arg
   //
   char* driverFilename;
   ProgramEntryPoint entryPoint;
   RunInfo::RunType runType;
   //
   parseCommandLine( argc, argv, driverFilename, entryPoint, runType );

   // Initialize runInfo by having it read the driver file.
   // Initializes stuff like list of operators for this pathwidth.
   // This call will not return if there is a problem.
   // Do this for all program types (even remote workers).
   //
   runInfo.initialize( driverFilename, runType );

   // Ok for all program types.
   //
   FileManager::initialize(argv[0]); // pass current program name.

   if ( runType == RunInfo::Manager )
   {
      search.createDatabases();
      FileManager::managerInitialization();
   }

   // Workers do not access the database 
   //
   if ( runType != RunInfo::Worker )
   {
      search.initialize();
   }

   // Call the entry point
   //
   entryPoint( argc, argv );

   /*
   if ( runType == RunInfo::Manager )
   {
      search.statistics( cout );
   }
   */

   // Close file system
   //
   FileManager::destruction();

   // de-allocate statics and quit
   //
   Exit(0);

   return 0;
#ifdef FST
   //allClassesStatus();
#endif

}

//-----------------------------------------------------------

static void parseCommandLine(
   int& argc, 
   char **&argv, 
   char*& driverFilename,
   ProgramEntryPoint& entry,
   RunInfo::RunType& runType
)
{
   // look for options

   // check for at least 2 more args - program and driver filename
   //
   if ( argc < 3 )
      fatalError( "At least two arguments required." );

   //cerr << "**" << argv[1] << "**" << nl;
   // process the entry point arg
   //
   entry = 0;
   for( int i=0; programTable[i].programName; i++ )
   {
      if ( !strcmp( programTable[i].programName, argv[1] ) )
      {
         entry = programTable[i].programEntryPoint;
         runType = programTable[i].runType;
      }
   }
   if ( entry == 0 )
      fatalError( "Bad program entry point in first arg" );

   argc--;
   argv[1] = argv[0];
   argv++;

   // Process the second arg.
   //
   driverFilename = argv[1];

   argc--;
   argv[1] = argv[0];
   argv++;

}


//-----------------------------------------------------------


int launchProgram( char* programName, 
   char* arg1, char* arg2, char* arg3, char* arg4 )
{

   // flush std out and std err
   //
   cout.flush();
   cerr.flush();

   int pid;

   if ( ( pid = fork() ) == 0 )
   {
      // close all other files, bypassing iostream
      //
      for ( int fd = maxFile() - 1; fd >= 3; fd-- ) close( fd );

      execl( 
         FileManager::executableFilename(), 
         FileManager::executableFilename(), 
         programName, 
         runInfo.driverFilename(), 
         arg1,
         arg2,
         arg3,
         arg4,
         NULL
      );

      aasserten( false ); // only get here on an bad bad error
   }
   if ( pid == -1 )
      aasserten( false );

   return pid;
}


