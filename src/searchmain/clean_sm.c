 
// -------------------------------------------
// --------------- clean_sm.c ----------------
// -------------------------------------------
 
/*tex
 
\file{clean_sm.c}
\path{src/searchmain}
\title{}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "error.h"
#include "general/stream.h"

#include "search/searchmain.h"
#include "search/searchminor.h"
#include "search/proof_db.h"
#include "search/search.h"

//------------------------------------------------------------------

void mainClean( int , char** )
{
#if 0
   cleanProcessArgs( argc, argv );

   search.minimalDatabase().cleanUp();

   search.nonminimalDatabase().cleanUp();
#endif
}

//------------------------------------------------------------------

static void cleanUsage()
{
   fatalError( "usage: Extract (type) [a], type=m,n,u" );
}

static void cleanProcessArgs( int, char** )
{
}

