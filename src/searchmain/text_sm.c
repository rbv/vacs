 
// -------------------------------------------
// --------------- sm_text.c -----------------
// -------------------------------------------
 
/*tex
 
\file{sm_text.c}
\path{src/searchmain}
\title{Program to output search nodes}
\classes{}
\makeheader

xet*/

#include <stdio.h>
#include "search/searchmain.h"

#include "set/set.h"
#include "error.h"
#include "search/searchnode.h"
#include "search/search.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "graph/ltree.h"
#include "search/searchminor.h"
#include "search/nodeinfo.h"
#include "search/proof_db.h"

#include "general/stream.h"
#include "graph/i-o/psspec.h"
#define LABEL_EDGES 0

#include "vacs/runinfo.h"
#include "vacs/filesystem.h"

enum TextFormats { TText, TTextm, TPs, TTex, TNumDGraph, 
                   TNumGraph, TGGraph, TPsTree };
static TextFormats fType;
static int TOrdinal = -1;

//enum Specials { None, NBConn };
//Specials sType;

static int targetnode = -1;
static SearchNodeIter::Status iterS;
static SearchNodeIter::GrowStatus iterG;
static SearchNodeIter::FamilyStatus iterF;

static void textProcessArgs( int argc, char** argv );
static void output();
static void outputTree();

static texstream* texs = 0;

//------------------------------------------------------------------

void mainText( int argc, char** argv )
{

   textProcessArgs( argc, argv );

   if ( fType == TTex )
   {
      texs = new texstream( 1 ); // attach to cout
      *texs << "\\documentstyle{article}" << nl
            << "\\special{header=" << RunInfo::getVACSpath() 
            << "/lib/ltree.ps}" << nl
            << "\\setlength{\\evensidemargin}{0in}" << nl
            << "\\setlength{\\oddsidemargin}{0in}" << nl
            << "\\setlength{\\topmargin}{-0.5in}" << nl
            << "\\setlength{\\textwidth}{6.5in}" << nl
            << "\\setlength{\\textheight}{9in}" << nl
            << "\\begin{document}" << nl;
   }

   if ( fType == TPsTree )
   {
      outputTree();
   }
   else
   {
      output();
   }

   if ( fType == TTex )
   {
      *texs << "\\end{document}" << nl;
      delete texs;
   }
}

//------------------------------------------------------------------

static void output()
{
   SearchNodeIter I( iterS, iterG, iterF );

   NodeNumber n;

   //DatabaseBase::lockSystem();
   FileManager::lockRequest( LSysR );

   while ( ( n = I() ).isValid() )
   {
      if ( targetnode != -1 && n.asInt() != targetnode )
           continue;

      const SearchNode& s = search.node( n );

      bool skip = false;
      if ( TOrdinal != -1 )
      {
         NonminimalProof np;
         MinimalProof mp;
         switch ( s.status() )
         {
            case SearchNode::Minimal:    search.minimalProof( n, mp );
                             if ( mp.type() != TOrdinal ) skip = true; break;
            case SearchNode::Nonminimal: search.nonminimalProof( n, np );
                             if ( np.type() != TOrdinal ) skip = true; break;
         }
      }
      if ( skip ) continue;

      RBBPG G;

      switch ( fType )
      {
         case TText:
            search.printNode( cout, n, 2 );
            cout << nl;
            break;
         case TTextm:
            search.graph( n, G );
            search.printNode( cout, n, 2 );
            PartialMinorProof::dumpCongruenceStates( cout, G );
            cout << nl;
            break;
         case TTex:
            search.texNode( *texs, n );
            break;
         case TNumGraph:
            search.graph( n, G );
            cout << n << ' ' << G << nl;
            break;
         case TNumDGraph:
            search.graph( n, G );
            cout << n << ' ' << s.depth() << ' ' << G << nl;
            break;
         case TGGraph:
            search.graph( n, G );
            cout << G << nl;
            break;
         case TPs:
            search.graph( n, G );
            bpg2ps( G );
            cout << "showpage\n\n";
            break;
         default: aassert(false);
      }
   }

   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

}

//------------------------------------------------------------------

// Tree drawing routines

#if LABEL_EDGES

static Str nodeDrawStr( vertNum node, const void *lt )
{
   const LTree *t = (const LTree*) lt;

   NodeNumber n = NodeNumber( (int) t->getLabel(node) );

   return asString( n );
}

static Str edgeDrawStr( vertNum parent, vertNum child, const void *lt )
{
   (void) parent; // stop warnings

   const LTree *t = (const LTree*) lt;

   NodeNumber n = NodeNumber( (int) t->getLabel(child) );
   const SearchNode& s = search.node( n );

   return asString( s.op() );
}

#else

static Str nodeDrawStr( vertNum node, const void *lt )
{
   const LTree *t = (const LTree*) lt;

   NodeNumber n = NodeNumber( (int) t->getLabel(node) );
   const SearchNode& s = search.node( n );

   return asString( n ) + Str(',') + asString( s.op() );
}

#endif

static PSSpec::PSNodeType nodeDrawType( vertNum node, const void *lt )
{
   const LTree *t = (const LTree*) lt;

   NodeNumber n = NodeNumber( (int) t->getLabel(node) );
   const SearchNode& s = search.node( n );

   SearchNode::Membership fam = s.membership();

   PSSpec::PSNodeType ret;

   switch ( fam )
   {
      case SearchNode::InF:
         switch( s.status() )
         {
            case SearchNode::Minimal:    ret = PSSpec::WhiteCircle; break;
            case SearchNode::Nonminimal: ret = PSSpec::WhiteSquare; break;
//            case SearchNode::Unknown:    ret = PSSpec::GrayCircle; break;
            default: aassert( false );
         }
         break;

      case SearchNode::NotInF:
         switch( s.status() )
         {
            case SearchNode::Minimal:    ret = PSSpec::BlackCircle; break;
            case SearchNode::Nonminimal: ret = PSSpec::BlackSquare; break;
            default: aassert( false );
         }
         break;

      case SearchNode::NotComputed:
        switch( s.status() )
         {
            case SearchNode::Minimal:    ret = PSSpec::GrayCircle; break;
            case SearchNode::Nonminimal: ret = PSSpec::GraySquare; break;
            default: aassert( false );
         }
         break;
         aassert( false );

      default:
         aassert( false );
   }

   return ret;
}

static void outputTree()
{
   SearchNodeIter I( iterS, iterG, iterF );

   LTree T( 0 );

   NodeNumber n;

   while ( ( n = I() ).isValid() )
   {
      // skip root
      if ( n.asInt() == 0 ) continue;

      const SearchNode& s = search.node( n );

      vertNum v;
      Label l = s.parent().asInt();

      //assert( T.labelIndex( l, v ) );
      // ???
      Assert ( T.labelIndex( l, v ) );

      T.addLeaf( v, n.asInt() );
   }

   //T.draw( cout, nodeDrawStr, nodeDrawType );
   
   PSSpec spec;
   //
   spec.setFormat(PSSpec::Poster);
   //
   spec.setNodeStrFnc(nodeDrawStr);
   spec.setNodeTypeFnc(nodeDrawType);
#if LABEL_EDGES
   spec.setEdgeStrFnc(edgeDrawStr);
#endif

   tree2ps( CAST(T, const Tree), &spec );
}

//------------------------------------------------------------------

static void textUsage()
{
   cerr << "Usage: text nodenum|type [format] [ordinal]" << nl;
   cerr << "  If nodenum is given, extracts a single graph" << nl;
   cerr << "  type: a=all, m=minimal, n=nonminimal, i=irrelevant, u=unknown, r=relevant, k=known" << nl;
   cerr << "     optional 2nd letter: f=in family, o=not in family" << nl;
   cerr << "     optional 3rd letter: g=grown" << nl;
   cerr << "  format:  text, textm, ps, tex, tree, gr, ndg, ng, ngg" << nl;
   cerr << "    graphical: " << nl;
   cerr << "     tree:  postscript search tree" << nl;
   cerr << "            (circles in family, squares out, white min, black nonmin)" << nl;
   cerr << "     ps:    graph" << nl;
   cerr << "     tex:   graph" << nl;
   cerr << "    textual: " << nl;
   cerr << "     text:  node description" << nl;
   cerr << "     textm: node and minors description" << nl;
   cerr << "     gr:    t-parse" << nl;
   cerr << "     ng:    id t-parse" << nl;
   cerr << "     ndg:   id depth t-parse" << nl;
   cerr << "Most combinations of type and format don't work :)" << nl;
   cerr << "Examples:" << nl;
   cerr << "   text rf tree = tree of relevant graphs, restricted to F" << nl;
   cerr << "   text mo ps = boundaried obstructions as postscript" << nl;
   cerr << "   text mfg = internal minimal nodes" << nl;
   cerr << "   text u tex = unknown nodes as tex" << nl;
   cerr << "   text n ng 5 = nonmin graphs and numbers - testset only" << nl;

   Exit(1);
}


static void textProcessArgs( int argc, char** argv )
{

   // set defaults
   //
   fType = TText;
   //sType = None;
   iterS = SearchNodeIter::SAny;
   iterG = SearchNodeIter::GAny;
   iterF = SearchNodeIter::FAny;
   
   // check for node type
   //
   if ( argc == 1 ) textUsage(); 

   if ( sscanf( argv[1], "%d", &targetnode ) == 1 )
       goto skipSearchArgs;

   if ( strlen( argv[1] ) < 1 ) textUsage();
   if ( strlen( argv[1] ) > 3 ) textUsage();

   switch( argv[1][0] )
   {
      case 'a': iterS = SearchNodeIter::SAny; break;
      case 'm': iterS = SearchNodeIter::Minimal; break;
      case 'n': iterS = SearchNodeIter::Nonminimal; break;
      case 'i': iterS = SearchNodeIter::Irrelevant; break;
      case 'u': iterS = SearchNodeIter::Unknown; break;
      case 'r': iterS = SearchNodeIter::Relevant; break;
      case 'k': iterS = SearchNodeIter::MinimalOrNonminimal; break;
      default:
          textUsage();
   }

   if ( strlen( argv[1] ) >= 2 )
      switch( argv[1][1] )
      {
         case 'f': iterF = SearchNodeIter::InFamily; break;
         case 'o': iterF = SearchNodeIter::OutOfFamily; break;
         default: textUsage();
      }

   if ( strlen( argv[1] ) >= 3 )
      switch( argv[1][2] )
      {
         case 'g': iterG = SearchNodeIter::Grown; break;
         default: textUsage();
      }

 skipSearchArgs:

   // shift
   argc-=1; argv[1]=argv[0]; argv+=1;

   // check for output format
   //
   if ( argc == 1 ) return; // use default

   bool ok = false;
   if      ( !strcmp( argv[1], "text" ) ) { fType = TText; ok=true; }
   else if ( !strcmp( argv[1], "textm" ) ){ fType = TTextm; ok=true; }
   else if ( !strcmp( argv[1], "tree" ) ) { fType = TPsTree; ok=true; }
   else if ( !strcmp( argv[1], "ps" ) )   { fType = TPs; ok=true; }
   else if ( !strcmp( argv[1], "tex" ) )  { fType = TTex; ok=true; }
   else if ( !strcmp( argv[1], "ng" ) )   { fType = TNumGraph; ok=true; }
   else if ( !strcmp( argv[1], "ndg" ) )  { fType = TNumDGraph; ok=true; }
   else if ( !strcmp( argv[1], "gr" ) )   { fType = TGGraph; ok=true; }
   else textUsage();

   if ( !ok ) textUsage();

   argc-=1; argv[1]=argv[0]; argv+=1;

   if ( argc == 1 ) return; // use default

   if ( argc != 1 )
   {
      bool ok = false;
      if ( sscanf( argv[1], "%d", &TOrdinal ) == 1 ) ok=true;
      if ( !ok ) textUsage();
      argc-=1; argv[1]=argv[0]; argv+=1;
   }

   // check cmd line
   //
   if ( argc != 1 ) textUsage();
}
