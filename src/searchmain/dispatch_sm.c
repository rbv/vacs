
// -------------------------------------------
// --------------- sm_dispatch.c -------------
// -------------------------------------------

/*tex
 
\file{sm_dispatch.c}
\path{src/searchmain}
\title{Search program dispatcher}
\classes{}
\makeheader
 
xet*/

#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>

#include <iostream>

//#include "family/family.h"
#include "family/probinfo.h"
//#include "search/searchmain.h"
//#include "vacs/runinfo.h"
#include "vacs/filesystem.h"
//#include "search/searchnode.h"
//#include "search/search.h"
#include "search/nodeinfo.h"
#include "vacs/message.h"
//#include "set/seq.h"
#include "general/stream.h"
//#include "general/random.h"
#include "vacs/fs_server.h"

//--------------------------------------------------------------

class Dispatcher
{
 static int cmdfd, listenfd;

 static ofstream cmdLog;

 static bstream cmdFile;

 // ... workers list

 static WorkList work;

 static bool sendPMP;
 static bool sendMembership;

public: 

 static void stopSignal(int);

 static void processLoop();

 static void initialize(int fd);
};

bstream Dispatcher::cmdFile;

WorkList Dispatcher::work;

int Dispatcher::cmdfd;
int Dispatcher::listenfd;
bool Dispatcher::sendPMP;
bool Dispatcher::sendMembership;

ofstream Dispatcher::cmdLog;

//--------------------------------------------------------------
//--------------------------------------------------------------

void Dispatcher::stopSignal(int)
{
  // Not safe to write to cerr in here; being lazy
  cerr << "Dispatch received quit signal\n";
  cerr << "Manager was the bottleneck for " << work.workersIdleTime() << " sec\n";

  //siglongjmp(sig_point,1);
  Message::closeServer();
  exit(0);
}

//--------------------------------------------------------------

#include <signal.h>

void Dispatcher::initialize(int fd)
{
    cmdfd = fd;
    cmdFile.openOnFD( fd, 1 << 16, true ); 

    signal( SIGINT, Dispatcher::stopSignal );
    signal( SIGTERM, Dispatcher::stopSignal );
    signal( SIGPIPE, SIG_IGN );

    work.readConfig();

    char buf[STRMAX];

    /* obtain fd to listen for client requests on */
    //    gethostname( buf, STRMAX );

    int portNum = 0;
    aasserten( (listenfd = serverListen( buf, STRMAX, portNum)) >= 0 );

cerr << "setting server " << buf << ',' << portNum << nl;

    if ( ! Message::setServerNames( buf, portNum, cmdLog ) )
    {
     cerr << "VACS server already open!\n";
     cmdFile << Message::ProcessFinished;
     exit(0);
    }
    cmdLog.close();

cerr << "listenfd = " << listenfd << nl;

   sendPMP = ProbInfo::getBool( ProbInfo::MessagesContainPMP );
   sendMembership = ProbInfo::getBool( ProbInfo::WorkersDoMembership );
}

static void see(int maxfd, fd_set &allset)
{
  for ( int i=0; i<=maxfd; i++)
  {
   if ( FD_ISSET(i,&allset) ) cerr << i << ' ';
  }
  cerr << nl;
}

#define UNKNOWN_LIMIT 2000

/*
 * Main loop checking for new worker clients and issues work until
 * manager closes connection.
 */ 
void Dispatcher::processLoop()
{
    int		clifd;
    bstream     worker;

    fd_set	rset, allset;
    
    FD_ZERO(&allset);
    FD_SET(listenfd, &allset);
    FD_SET(cmdfd, &allset);

    NodeNumber node;
    SearchMinor minor;
    Array<MinorNumber> minors;
    Array<int> tests;
    NBBPG extn;

    bool scoldedManager = false;

    while (true) 
    {
//        int dumpCount = 0;
        bool toDump = false;
    	rset = allset;		/* rset gets modified each time around */

        int maxfd = max(work.max_fd(),max(listenfd,cmdfd));
//see(maxfd, rset);
#ifdef FST
        if ( 0 && ! (dumpCount++ & 0x0f))
        {
           GraphDictItem::classStatus();
           SkipListNode< NodeNumber, GraphDictItem >::classStatus();
           SkipListNode< int, Client >::classStatus();
           RBBPG::classStatus();
           //PartialMinorProof::classStatus();
        }
#endif

//cerr << "dispatch select with maxfd = " << maxfd << nl;

#if 0
cerr << " ---------------- fd status --------------------" << nl;
for ( int fd = 50; fd < 55; fd++ ) {
  errno = 0;
  int fl = ::fcntl(fd, F_GETFL, 0);
  cerr << fd  << ' ' << errno << " Status: " << fl << nl;
}
cerr << " -----------------------------------------------" << nl;
#endif

        Gtimers::start( 1, "select" );

    	aasserten( select( maxfd + 1, &rset, NULL, NULL, NULL) > 0 );

        Gtimers::stop( 1 );

    	/* accept new client request */
        //
    	if (FD_ISSET(listenfd, &rset)) 
        {
    		aasserten( (clifd = serverAccept(listenfd)) >= 0 );

cerr << "serverAccept " << clifd << nl;

    		work.clientAdd(clifd);

    		FD_SET(clifd, &allset);
    		//if (clifd > work.maxfd() )
    	        //	maxfd = clifd;	/* max fd for select() */

    		cerr << "new connection: fd = " << clifd << nl;
                work.dump();
    		continue;
    	}

    	/* Check for unknown search nodes from manager */
        //
    	if (FD_ISSET(cmdfd, &rset)) 
        { 	

#if 0
  errno = 0;
  int fl = ::fcntl(cmdFile.fd(), F_GETFL, 0);
  cerr << "cmdFile: errno = "  << errno << " Status: " << fl << nl;
  cerr << "  readyForRead = " << 
		(cmdFile.readyForRead() ? "true" : "false") << nl;
#endif

                Gtimers::start( 2, "read unknowns" );
  
                RBBPG graph;
                NodeNumber node;

                // else process unknown nodes
                //
                cmdFile >> node;

//cerr << "from Manager: " << node;

                // Did manager close connection?
                //
		if ( cmdFile.eof() || cmdFile.fail() ) break;

                cmdFile >> graph;
//cerr << " -- " << graph << nl;

                Gtimers::stop( 2 );
                Gtimers::start( 3, "doUnknownNode" );

                assert( node.isValid() );
		work.doUnknownNode( node, graph );

                Gtimers::stop( 3 );

                if ( ! scoldedManager && work.nodesWaiting() > UNKNOWN_LIMIT ) 
                {
		   cmdFile << Message::Pause;
                   cmdFile.flush();
                   scoldedManager = true;
                }
	
    		continue;
    	}

        const ClientNode  *pClientNode = work.head();
        //
    	while ( pClientNode ) 
        {
          clifd = pClientNode->object().fd;

          //cerr << "checking action on " << clifd << nl;

          if (FD_ISSET(clifd, &rset)) 
          {

            Gtimers::start( 4, "worker.openOnFD" );

    	    /* 
             * read argument buffer from client 
             */
            worker.detach();
            worker.openOnFD(clifd);

	    /* 
             * Process client's request 
	     */
	    Message::WorkResultType resultType;
            //

            Gtimers::stop( 4 );
            Gtimers::start( 5, "read from worker" );

	    worker >> resultType;

            //cerr << "worker result: " << resultType << nl;

	    if (worker.fail())
    	    {
		if (worker.eof())
		{
		    /* client has closed connection */
		    cerr << "closing: fd " << clifd << nl;
		}
		else
		{
		    // To be safe, kill the client
		    cerr << "Unexpected worker error errno=" << errno << " reading client " << pClientNode->object() << nl;
		}
               
    		close(clifd);
    		FD_CLR(clifd, &allset);

    		work.clientDel(clifd);

		cerr << "closed: fd " << clifd << nl;

                break;  // our pClientNode is bad
    	    } 

	    if ( resultType == Message::BackDoor )
            {
                cerr << "Backdoor signal!\n";
                Gtimers::stop( 5 );

         	// print out current worker activity.
    		//
                worker << "\nBackdoor info follows:\n";
		work.backDoorDump(worker);

                // accept requests to terminate
                //
                int w_fd;

                while ( ! (worker.eof() || worker.fail()) )
                {
                  worker >> w_fd;
                  cerr << "received backdoor kill req for " << w_fd << nl;
                  if ( w_fd <= 0 || w_fd == clifd ) break;

                  if ( FD_ISSET(w_fd, &allset) )
                  {
                    cerr << "giving up on worker " << w_fd << nl;
                    close(w_fd);
                    FD_CLR(w_fd, &allset);

                    work.clientDel(w_fd);

                    worker << w_fd; // give acknowledgement
                  }
                  else worker << 0; // invalid worker

                  //worker.flush();
                }
 
                worker << -1;  // give human something to chew

	        break;
            }

            if ( resultType == Message::ProcessFinished )
            {
                cerr << "processFinished " << clifd << nl;
                aassert( false );
            }
            else  // hope we don't block...
            {
                char inFamily;
                static PartialMinorProof* pmp = 0;
                if ( sendPMP )
                {
                  if ( pmp ) delete pmp;
                  pmp = new PartialMinorProof;
                }

		//cmdFile << resultType;  // moved to each output line

		switch ( resultType )
		{
		    case Message::MinimalByTestset:
		    case Message::MinimalByExtn:
                      worker >> node;
                      if ( sendPMP )
                        worker >> *pmp;
                      if ( sendMembership )
                        worker >> inFamily;
                      Gtimers::stop( 5 );
                      Gtimers::start( 6, "forward to manager" );
                      if ( worker.fail() || worker.eof() )
                        cerr << "Unexpected worker err " << errno << nl;
                      else
                      {
                        cmdFile << resultType << node;
                        if ( sendPMP )
                          cmdFile << *pmp;
                        if ( sendMembership )
                          cmdFile << inFamily;
                      }
		      break;
		    
		    case Message::MinimalByTightCong:
		    case Message::InconclusiveMinimality:
		    case Message::MinimalByOOF:
		    case Message::NonminimalByOOF:
		    case Message::ENoncanonic:
		    case Message::HNoncanonic:
		      worker >> node;
                      if ( sendMembership )
                        worker >> inFamily;
                      Gtimers::stop( 5 );
                      Gtimers::start( 6, "forward to manager" );
                      if ( worker.fail() || worker.eof() )
                        cerr << "Unexpected worker err " << errno << nl;
                      else
                      {
		        cmdFile << resultType << node;
                        if ( sendMembership )
                          cmdFile << inFamily;
                      }
		      break;
		    
		    case Message::MinorCongTestset:
		    case Message::MinorCongDyProg:
		      worker >> node >> minor;
                      if ( sendMembership )
                        worker >> inFamily;
                      Gtimers::stop( 5 );
                      Gtimers::start( 6, "forward to manager" );
                      if ( worker.fail() || worker.eof() )
                        cerr << "Unexpected worker err " << errno << nl;
                      else
                      {
		        cmdFile << resultType << node << minor;
                        if ( sendMembership )
                          cmdFile << inFamily;
                      }
		      break;
		    
		    default: aassert(false);
		    
		} // switch result type
                cmdFile.flush();
                Gtimers::stop( 6 );
                //if ( worker.eof() ) cerr << "Unexpected worker eof" << nl;
                //if ( worker.fail() ) cerr << "Unexpected worker err " << errno << nl;

            } // sent processed work to manager

            Gtimers::start( 7, "clientFinishedJob" );
            work.clientFinishedJob(clifd);
            Gtimers::stop( 7 );

            if ( scoldedManager && work.nodesWaiting() < UNKNOWN_LIMIT/5*4 )
            {
               cmdFile << Message::Resume;
               cmdFile.flush();
               scoldedManager = false;
               toDump = true;
            }
            //toDump = true;

    	 } // is selected for action

        pClientNode = pClientNode->next();

       } // while another client

       if (toDump)
           work.dump();

   } // infinite loop

   usleep(5000);
   cerr << "Closing dispatch server (manager eof); cmdFile bits: " << cmdFile.bistream::eof() << " " << cmdFile.bistream::fail() << " " << cmdFile.bostream::fail() << "\n";

   cerr << "Manager was the bottleneck for " << work.workersIdleTime() << " sec\n";
   log() << "Manager was the bottleneck for " << work.workersIdleTime() << " sec\n";

   Dtime dtime;
   dtime();
   cerr << "Dispatcher times: " << dtime.user() << " user " << dtime.system() << " system\n";
   Gtimers::print( cerr );

   Message::closeServer();

}

//--------------------------------------------------------------
// 			MAIN entry point
//--------------------------------------------------------------

void mainDispatch( int argc, char** argv )
{
   aassert(argc==2);

cerr << "Dispatcher start\n";

   // First argument should be the manager's file discriptor channel.
   //
   Dispatcher::initialize( (int) (argv[1][0]) );

   Dispatcher::processLoop();

}

//------------------------------------------------------------------------
// *** * ***** *  ***  *  ******** * **** ***  ****** ***** *** ***** * **
//------------------------------------------------------------------------

#ifdef OLD_DISPATCHER_STUFF

please get from backup 

void mainDispatch( int argc, char** argv ) // sorry, half-way hacked (*mjd*)!!
...


#if 0
void processMessage() // tmp prototype for compile test
{
  bstream manager;
  bstream s;

  //Message::MessageType mType;
  //s >> mType;

  switch ( mType )
  {
    case Message::Worker1Request:
      // get some work and send it 
      // if queue is empty, read from manager, blocking until something 
      // comes through (which could be search-done message)
      break;

    case Message::WorkResult:
    {
      NodeNumber n;
      PartialMinorProof pmp;
      MinorNumber minor;
      Array<MinorNumber> minors;
      Array<int> tests;
      NBBPG extn;

      Message::WorkResultType resultType;
      s >> resultType;

      manager << Message::WorkResult << Message::WorkResult;

      switch ( resultType )
      {
        case Message::MinorsDistTestset:
        case Message::MinorsDistExtn:
          s >> n >> pmp;
          manager << n << pmp;
          break;

        case Message::InconclusiveMinimality:
          s >> n;
          manager << n;
          break;

        case Message::MinorCongTestset:
        case Message::MinorCongSearch:
          s >> n >> minor;
          manager << n << minor;
          break;

        default: aassert(false);

      } // switch result type

      break;

    }

    default: aassert(false); // bad message header

  } // switch message type
}
#endif


#endif
