
// -------------------------------------------
// --------------- sm_play.c -----------------
// -------------------------------------------

/*tex
 
\file{sm_play.c}
\path{src/searchmain}
\title{Startup code}
\classes{}
\makeheader
 
xet*/

#include <stdio.h>
#include "family/family.h"
#include "general/str.h"
#include "bpg/bpgutil.h"
#include "search/search.h"
#include "search/proof_db.h"
#include "search/searchminor.h"

//----------------------------------------------------------------

static void play1()
{
   //DatabaseBase::lockSystem();
   FileManager::lockRequest( LSysR );


   PartialMinorProof pmp;
   MinimalProofDatabase& db = search.minimalProofDatabase();
   MinimalProof proof;
   db.getInfo( 22, proof );
   db.getPMP( 22, pmp );

   cerr << pmp << nl;

   RBBPG G;
   search.graph( proof.usedBy(), G );
   cerr << "G: " << G << nl;

   const NBBPG& D = pmp.distinguishers().get( 0 );
   cerr << "D: " << D << nl;

   CBBPG glue( G, D );
   bool mem = Family::member( glue );
   cerr << glue << ' ' << ( mem ? "in" : "out" ) << nl;


   const SearchMinorDictionary& minors = pmp.minors();
   MinorNumber mNum;
   ForAllDictionaryItems( minors, mNum )
   {
      const SearchMinor& minor = minors.get( mNum );
//cerr << minor << nl;
      RBBPG mG = G;
      minor.calcMinor( mG );
      CBBPG glue( mG, D );
      bool mem = Family::member( glue );
      cerr << mNum << ' ' << glue << ' ' << ( mem ? "in" : "out" ) << nl;
   }

   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

}

//----------------------------------------------------------------

static void play2()
{
   RBBPG G1;
   {
   Str s( "0,1,2,3,4,01,02,0,12,1,01" );
   BPG::fromString( s, G1 );
   cerr << G1 << nl;

   bool mem = Family::member( G1 );
   cerr << ( mem ? "in" : "out" ) << nl;
   }

   RBBPG G2;
   {
   Str s( "0,1,2,3,4,01,02,0,03,12,1" );
   BPG::fromString( s, G2 );
   cerr << G2 << nl;

   bool mem = Family::member( G2 );
   cerr << ( mem ? "in" : "out" ) << nl;
   }

   if ( isomorphic( G1, G2, freeBoundary ) )
      cerr << "iso" << nl;
   else
      cerr << "not iso" << nl;
}

//----------------------------------------------------------------
 
static void play3()
{
   int n = 3;

   int i;
   Operator op;
   RBBPG G;
   G.resize(n);

   op.firstOp();
   for ( i=0; i<n; i++ )
      G.setOp( i, op );

   bool done = false;
   int cnt = 0;
   while(1)
   {
      cerr << cnt << ' ' << G << nl;

      i = n-1;
      while(1)
      {
         op = G[i];
         op++;
         G.setOp(i,op);
         if ( ! op.isNoOp() ) break;
         op.firstOp();
         G.setOp(i,op);
         i--;
         if ( i < 0 ) { done=true; break; }
      }
      if ( done ) break;
      cnt++;
   }

}


//----------------------------------------------------------------

static void play4()
{

   while (1)
   {
      char in[161];
      int err;

      RBBPG B;

      cin.getline( in, 160 );

      if ( !cin ) break;

      Str s(in);
      if ( (err = BPG::fromString( s, B )) >= 0 )
      { cerr << "input error " << err << nl; break; }

      cerr << "Input RBBPG:  " << B << nl;

#if 1
      RBBPG H;
      RBBPG::findCanonic( B, H );
      cerr << "Final result: " << H << nl;
#else
      bool ret = RBBPG::isCanonic( B );
      if ( ret ) cerr << "   canonic" << nl; 
      else cerr << "   non-canonic" << nl;
#endif
   }

}

#include "graph/i-o/psspec.h"

static void play5()
{
    char in[161];
    int err;

    RBBPG B;

    cin.getline( in, 1000 );

    if ( !cin ) return;

    Str s(in);
    if ( (err = BPG::fromString( s, B )) >= 0 )
    { cerr << "input error " << err << nl; return; }

    cerr << "Input RBBPG:  " << B << nl;

    bpg2ps(NBBPG(B));

}


//----------------------------------------------------------------


static void play6()
{
  cerr << "Loading binary db file from stdin\n";
  cerr << "Writing ascii db to stdout\n";
  bistream s;
  s.openOnFD( 0 );
   
  while(1)
  {
    MinimalProof proof;
    RBBPG graph;
    PartialMinorProof pmp;

    s >> proof;
    if ( proof.type() == MinimalProof::None ) break;
    s >> graph;
    if ( proof.hasPMP() ) { s >> pmp; }

    cout << int(proof.type()) << ' ' << graph << nl;
  }
}

static void play7()
{
  cerr << "Loading ascii file from stdin\n";
  bostream s;
  s.openOnFD( 1 );
   
  while(1)
  {
    PartialMinorProof pmp;

    int proofType;
    cin >> proofType;
    MinimalProof proof( (MinimalProof::ProofType) proofType );
    proof.notUsed();

    if ( !cin ) break;

    RBBPG graph;
    char str[ 500 ];
    cin.getline( str, 500 );
    RBBPG::fromString( str, graph );

    s << proof;
    s << graph;
    if (proof.hasPMP()) s << pmp;
  }

  MinimalProof proof( MinimalProof::None );
  s << proof;

}

// Isomorphism testing
static void play8()
{
    char str1[] = "[0,1,2,3,4,01,0,02,03]";
    RBBPG graph1;
    RBBPG::fromString( str1, graph1 );

    char str2[] = "[0,1,2,3,4,01,02,0,03,04]";
    RBBPG graph2;
    RBBPG::fromString( str2, graph2 );

    IsoDBItem1 i1_1, i1_2;
    IsoDBItem2 i2_1, i2_2;

    IsoStats::startInit(1);  // beat assert
    initIsoDBItems( graph1, i1_1, i2_1 );
    initIsoDBItems( graph2, i1_2, i2_2 );
    IsoStats::stopInit();

    cerr << i2_1 << i2_2;

    IsoStats::startCheck(1);
    cerr << (i2_1 == i2_2);
    IsoStats::stopCheck();
}

//----------------------------------------------------------------

void mainPlay( int argc, char** argv )
{
   int program;
   if ( argc != 2 || sscanf( argv[1], "%d", &program ) != 1) 
     { cerr << "Number required" << nl; return; }

   switch ( program )
   {
      case 1: play1(); break;
      case 2: play2(); break;
      case 3: play3(); break;
      case 4: play4(); break;
      case 5: play5(); break;
      case 6: play6(); break;
      case 7: play7(); break;
      case 8: play8(); break;
      default: cerr << "Bad number" << nl;
   }
}

