 
// -------------------------------------------
// --------------- sm_extract.c --------------
// -------------------------------------------
 
/*tex
 
\file{sm_extract.c}
\path{src/searchmain}
\title{Program to output search node info in various formats}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "error.h"
#include "general/stream.h"
#include "set/set.h"

#include "search/searchmain.h"
#include "search/searchminor.h"
#include "search/proof_db.h"
#include "search/search.h"
#include "graph/i-o/psspec.h"
#include "bpg/bpgutil.h"
#include "bpg/bpgutil2.h"
#include "family/family.h"
#include "family/probinfo.h"
#include "family/fam_oplane.h"
#include "graph/graph.h"
#include "bpg/i-o/bpg2graph.h"

static void extractProcessArgs( int argc, char** argv );

enum ExtType { AllNodes, Relevant, Minimal, Nonminimal, Unknown, Irrelevant, Obst };
enum ExtFormat { Text, Ascii, Ps, Grph, Bin, AdjMat };
enum ExtFlag { FlagAll, FlagUsed, FlagUnused };

static ExtType extType;
static ExtFormat extFormat;
static ExtFlag extFlag;
static int extLength;
static int extVertices;

static ostream* ostr;
static astream* astr;
static bostream* bstr;

//------------------------------------------------------------------

static void extMinimal()
{
   MinimalProofDatabase& db = search.minimalProofDatabase();

   for ( int ref=0; ref < db.size(); ref++ )
   {
      if ( ref > 0 && ref % 2000 == 0 ) cerr << "ref " << ref << nl;
      if ( ! db.exists(ref) ) continue;

      MinimalProof proof;
      db.getInfo( ref, proof );

      if ( extFlag == FlagUnused && proof.isUsed() ) continue;
      if ( extFlag == FlagUsed && !proof.isUsed() ) continue;

      RBBPG G;
      search.graph( proof.usedBy(), G );

      if ( extLength && G.length() != extLength ) continue;
      if ( extVertices && G.vertices() != extVertices ) continue;

      if ( extFormat == Grph ) { *ostr << G << nl; continue; }

      if ( extFormat == AdjMat ) 
      { Graph H; bpg2graph(G,H); *ostr << H << nl; continue; }

      switch ( extFormat )
      {
         case Text: *ostr << nl << "Proof #" << ref << nl;
                    *ostr << "Graph " << G << nl;
                    break;
         case Ascii: *astr << eol << "Proof #" << ref << eol; break;
         case Bin: *bstr << proof << G; break;
         default: aassert(false);
      }

      if ( proof.hasPMP() )
      {
         PartialMinorProof pmp;
         db.getPMP( ref, pmp );
         switch ( extFormat )
         {
            case Text: *ostr << proof << nl << pmp << nl; break;
            case Ascii: *astr << proof << eol << pmp << nl; break;
            case Bin: *bstr << pmp; break;
            default: aassert(false);
         }
      }
      else
      {
         switch ( extFormat )
         {
            case Text: *ostr << proof << "  created " << proof.created() << nl;
                       break;
            case Ascii: *astr << proof << eol;
                        break;
         }
      }

   }

   if ( extFormat == Bin )
   {
      MinimalProof proof( MinimalProof::None );
      *bstr << proof;
   }
}


//------------------------------------------------------------------

static void extNonminimal()
{
   NonminimalProofDatabase& db = search.nonminimalProofDatabase();

   for ( int ref=0; ref < db.size(); ref++ )
   {
      if ( ref > 0 && ref % 2000 == 0 ) cerr << "ref " << ref << nl;
      if ( ! db.exists(ref) ) continue;

      NonminimalProof proof;
      db.getInfo( ref, proof );

      if ( extFlag == FlagUnused && proof.isUsed() ) continue;
      if ( extFlag == FlagUsed && !proof.isUsed() ) continue;

      RBBPG G;
      search.graph( proof.usedBy(), G );

      if ( extLength && G.length() != extLength ) continue;
      if ( extVertices && G.vertices() != extVertices ) continue;

      if ( extFormat == Grph ) { *ostr << G << nl; continue; }

      if ( extFormat == AdjMat )
      { Graph H; bpg2graph(G,H); *ostr << H << nl; continue; }

      if (ostr) *ostr << nl << "Proof #" << ref << ' ' 
          << "size " << G.size()-G.boundarySize() << nl << G << nl;
      if (astr) *astr << nl;

      if (ostr) *ostr << proof << nl;
      if (astr) *ostr << proof << nl;
      if (bstr) *bstr << proof << G;

   }

   if ( extFormat == Bin )
   {
      NonminimalProof proof( NonminimalProof::None );
      *bstr << proof;
   }
}


//------------------------------------------------------------------

static void extUnknown()
{
   UnknownStateDatabase& db = search.unknownStateDatabase();

   for ( int ref=0; ref < db.size(); ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      UnknownState proof;
      db.getInfo( ref, proof );

      if (ostr) *ostr << nl << "Proof #" << ref << nl << proof << nl;
      if (astr) *astr << nl;

      PartialMinorProof pmp;
      //db.getPMP( ref, pmp );
      //if (ostr) *ostr << proof << nl << pmp << nl;
      //if (astr) *astr << proof << nl << pmp << nl;

   }
}

//------------------------------------------------------------------

static void extIrrelevant()
{
   IrrelevantInfoDatabase& db = search.irrelevantInfoDatabase();
   db.loadNumberOfKeys();
   *ostr << "next num " << db.nextNumber() << nl;

   for ( int ref=0; ref < db.nextNumber(); ref++ )
   {
      IrrelevantInfo proof;
      db.getInfo( ref, proof );

      if (ostr) *ostr << nl << "Proof #" << ref << nl << proof << nl;
      if (astr) *astr << nl;
   }
}

//------------------------------------------------------------------

// Obstruction extraction code

static bool isoTest( const NBBPG& g1, const NBBPG& g2 )
{
   return ::isomorphic( g1, g2, noBoundary );
}

static bool isoTestG( const Graph& g1, const Graph& g2 )
{
   return g1 == g2;
}

bool hasNonboundaryDeg2( const BPG& G );

static int skipped_disconnected = 0;
static int skipped_oneconnected = 0;

// Return only obstructions of interest
static bool obstPostCheck( const NBBPG &G)
{
  if ( ProbInfo::getBool( ProbInfo::Connected ) )
  {
     if ( isDisconnected( G ) )
     {
        skipped_disconnected++;
        cout << "skipping (disconnected): " << G << nl;
        return false;
     }
  }

  if ( ProbInfo::getBool( ProbInfo::Biconnected ) )
  {
     // Cut vertices
     if ( !isBiconnected( G ) )
     {
        skipped_oneconnected++;
        cout << "skipping (not biconnected): " << G << nl;
        return false;
     }
  }

  return true;

  // return ! hasNonboundaryDeg2( G );
}

#include "graph/algorithm/genus_ga.h"
//
bool (*familyTest)(const Graph&) = outerplanar1;

static void convert2Obstruction(Graph &G, bool removeIsoVertices = true)
{
  cerr << "outerplanar1\n";
  if (removeIsoVertices) G.rmIsoNodes();
  //aassert( familyTest(G) == false );

  // now just check for contractions (deletable edges have been removed)
  //
  int n=G.order();
  for (int i=0; i<n-1; i++)
    for (int j=i+1; j<n; j++)
      if ( G.isEdge(i,j) )
      {
        Graph H(G);
        H.contractEdge(i,j);
        if (familyTest(H) == false)
        {
           G.contractEdge(i,j);
           convert2Obstruction(G, false);
           return;
        }
      }
}


static void extObst()
{

// if matrix then do alternate test below
//
//if ( extFormat != AdjMat )
if ( 1 )
{

   // Non-boundaried copies of all delta obstructions
   PCollection<NBBPG> preobst;
   // Actual obstructions
   PCollection<NBBPG> obst;

   IsoTester isoTest;

   MinimalProofDatabase& db = search.minimalProofDatabase();

   cerr << "Scanning db..." << nl;
   int count = 0;
   int sz = db.size();
   for ( int ref=0; ref < sz; ref++ )
   {
      if ( ref % 500000 == 0 )
         cerr << "(processed " << ref << " records) Checking for delta-obstructions, found " << count << "; " << preobst.size() << " unique up to isomorphism" << nl;

      if ( ! db.exists(ref) ) continue;

      MinimalProof proof;
      db.getInfo( ref, proof );

      if ( !proof.isUsed() ) continue;
   
      if ( proof.type() != MinimalProof::OutOfFamily ) continue;

      RBBPG G;
      search.graph( proof.usedBy(), G );

      if ( extLength && G.length() != extLength ) continue;
      if ( extVertices && G.vertices() != extVertices ) continue;

      count++;

      if ( isoTest.find( G ) == -1 ) {
         // New isomorph
         // do a type conversion from RBBPG to BPG
         //
         NBBPG* nbG = new NBBPG( G );

         preobst.add( nbG );
         isoTest.add( G );
      }
   }

   //cerr << obst << nl;

   if (ostr) *ostr << "In total " << count << " delta-obstructions; " << preobst.size() << " unique up to isomorphism" << nl;

   NBBPG* p;
   count = 0;
   for ( int idx = 0; idx < preobst.size(); idx++ ) {
       if ( idx % 250 == 0 ) cerr << "Checking for obstructions, processed " << idx << " graphs, found " << count << " obstructions" << nl;

       NBBPG *graph = preobst.get( idx );

       if ( isObstruction( *graph ) ) {
           obst.add( graph );
           count++;
       } else {
           delete graph;
       }
   }

   // Give up ownership of pointers to prevent double freeing
   preobst.removeAll();

   if (ostr) *ostr << "Total obstructions: " << obst.size() << nl;
   if (ostr) *ostr << "After Post-Processing:" << nl;

   PCollectionIter<NBBPG> I2( obst );
   I2.start();
   count = 0;
   while ( p = I2() )
   {
      if ( obstPostCheck(*p) )  // Filter out uninteresting obstructions
      {
         count++;

         if ( extFormat == AdjMat ) 
         { Graph G; bpg2graph( *p, G ); cout << G; continue; }

         if (ostr) *ostr << *p << nl;
         if ( extFormat == Grph ) { cout << *p; }
         if ( extFormat == Ps ) { bpg2ps( *p ); cout << "showpage\n\n"; }
      }
   }

   if (ostr) *ostr << "Total obstructions:          " << count << nl;
   if ( ProbInfo::getBool( ProbInfo::Connected ) ) {
      if (ostr) *ostr << "Skipped " << skipped_disconnected << " disconnected obstructions" << nl;
   }
   if ( ProbInfo::getBool( ProbInfo::Biconnected ) ) {
      if (ostr) *ostr << "Skipped " << skipped_oneconnected << " 1-connected obstructions" << nl;
   }
}
else  
{
   PCollection<Graph> obstG;

   MinimalProofDatabase& db = search.minimalProofDatabase();

   int count = 0;
   for ( int ref=0; ref < db.size(); ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      if ( ref % 2000 == 0 ) cerr << "db ref: " << ref 
        << ", count = " << count << nl;

      MinimalProof proof;
      db.getInfo( ref, proof );

      RBBPG G;
      search.graph( proof.usedBy(), G );
   
      if ( proof.type() != MinimalProof::OutOfFamily ) continue;

      count++;

      // do a type conversion from RBBPG to Graph
      //
      Graph* adjmatG = new Graph;
      bpg2graph(G, *adjmatG);
  
      convert2Obstruction(*adjmatG); // warning builtin membership
  
      //cerr << *adjmatG;
      if (adjmatG->connected()) obstG.add( adjmatG );
   }

   cerr << "Removing iso copies..." << nl;

   // remove iso copies
   //
   PCollection<Graph> obstG2;
   obstG.removeEqualities( obstG2, isoTestG );

   cerr << obstG2.size() << nl; // count

   PCollectionIter<Graph> iterG( obstG2 );
   Graph* p;
   while ( p = iterG.next() )
   {
     if  (ostr) *ostr << *p;
   }
} 

}

//------------------------------------------------------------------
//------------------------------------------------------------------

void mainExtract( int argc, char** argv )
{
   extractProcessArgs( argc, argv );

   ostr = 0;
   astr = 0;

   switch ( extFormat )
   {
      case Grph:                      // use cout
      case AdjMat:                      // use cout
      case Text: ostr = &cout; break;
      case Ascii: astr = new astream( 1 ); assert(astr); break;
      case Bin: //bstr = new bostream( "extract.bin" ); break;
                bstr = new bostream;
                //bstr->openOnFD(1); break;
                bstr->openOnFD(1); break;
      case Ps:  ostr = &cout; break;
      default: aassert(false);
   }

   //DatabaseBase::lockSystem();
   FileManager::lockRequest( LSysR );

   switch ( extType )
   {
      case Minimal: extMinimal(); break;
      case Nonminimal: extNonminimal(); break;
      case Unknown: extUnknown(); break;
      case Irrelevant: extIrrelevant(); break;
      case Obst: extObst(); break;
      default: aassert(false);
   }
   //DatabaseBase::unlockSystem();
   FileManager::lockRequest( USysR );

   if (astr) delete astr;
   if (ostr) { *ostr << std::flush; }
   if (bstr) { bstr->detach(); delete bstr; }
}

//------------------------------------------------------------------

static void extractUsage()
{
   cerr << "usage: extract <type> [len=X] [vertices=X] [ text|a|ps|graph|adjmat|bin [used|unused] ]\n"
           "where <type> is one of:\n"
           " m -- minimal \n"
           " n -- nonminimal \n"
           " u -- unknown \n"
           " i -- irrelevant \n"
           " o -- obstruction \n"
           "Specify len=X and/or vertices=X to restrict to certain nodes \n";
   fatalError( "Bad arguments" );
}

static void extractProcessArgs( int argc, char** argv )
{
   extFormat = Text;
   extFlag = FlagAll;
   extLength = 0;
   extVertices = 0;

   if ( argc < 2 ) extractUsage();

   switch ( argv[1][0] )
   {
      case 'm': extType = Minimal; break;
      case 'n': extType = Nonminimal; break;
      case 'u': extType = Unknown; break;
      case 'i': extType = Irrelevant; break;
      case 'o': extType = Obst; break;
      default: extractUsage();
   }
   argc--;
   argv++;

   // Need iso db in order to print iso info
   if ( extFormat == Text && extType != Obst && extType != Irrelevant )
      search.loadIsoDatabase();

   if ( argc >= 2 && !strncmp( argv[1], "len=", 4 ) )
   {
      extLength = atoi( argv[1] + 4 );
      if ( !extLength ) extractUsage();
      if ( extType != Minimal && extType != Nonminimal && extType != Obst )
         fatalError( "len filtering only implemented for minimal, nonminimal & obstructions" );
      argc--;
      argv++;
   }

   if ( argc >= 2 && !strncmp( argv[1], "vertices=", 9 ) )
   {
      extVertices = atoi( argv[1] + 9 );
      if ( !extVertices ) extractUsage();
      if ( extType != Minimal && extType != Nonminimal && extType != Obst )
         fatalError( "vertices filtering only implemented for minimal, nonminimal & obstructions" );
      argc--;
      argv++;
   }

   if ( argc >= 2 )
   {
      if ( !strcmp( argv[1], "a" ) ) extFormat = Ascii;
      else if ( !strcmp( argv[1], "text" ) ) extFormat = Text;
      else if ( !strcmp( argv[1], "ps" ) ) extFormat = Ps;
      else if ( !strcmp( argv[1], "graph" ) ) extFormat = Grph;
      else if ( !strcmp( argv[1], "adjmat" ) ) extFormat = AdjMat;
      else if ( !strcmp( argv[1], "bin" ) ) extFormat = Bin;
      else extractUsage();
      argc--;
      argv++;
   }

   if ( argc >= 2 )
   {
      if ( !strcmp( argv[1], "unused" ) ) extFlag = FlagUnused;
      else if ( !strcmp( argv[1], "used" ) ) extFlag = FlagUsed;
      else extractUsage();
      if ( extType != Minimal && extType != Nonminimal )
         fatalError( "used/unused flag only meaningful for minimal & nonminimal" );
      argc--;
      argv++;
   }
}

