 
// -------------------------------------------
// --------------- import_sm.c ---------------
// -------------------------------------------
 
/*tex
 
\file{import_sm.c}
\path{src/searchmain}
\title{Program to output search node info in binary form}
\classes{}
\makeheader

xet*/

#include "stdtypes.h"
#include "error.h"
#include "general/stream.h"

#include "search/searchmain.h"
#include "search/searchminor.h"
#include "search/proof_db.h"
#include "search/search.h"

#if 0

static void importProcessArgs( int , char** );

enum ImpType { Minimal, Nonminimal };

static ImpType impType;

static astream* astr;

static bool eraseDB = false;

//------------------------------------------------------------------

static void impMinimal()
{
   MinimalProofDatabase db = search.minimalProofDatabase();

   for ( int ref=0; ref < db.nextNumber(); ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      MinimalProof proof;
      db.getInfo( ref, proof );

      if (ostr) *ostr << nl << "Proof #" << ref << nl;
      if (astr) *astr << nl;

      if ( proof.type() == MinimalProof::DistinguisherProof )
      {
         PartialMinorProof pmp;
         db.getPMP( ref, pmp );
         if (ostr) *ostr << proof << nl << pmp << nl;
         if (astr) *astr << proof << nl << pmp << nl;
      }
      else
      {
         if (ostr) *ostr << proof << nl;
         if (astr) *ostr << proof << nl;
      }

   }
}


//------------------------------------------------------------------

static void extNonminimal()
{
   NonminimalProofDatabase db = search.nonminimalProofDatabase();

   for ( int ref=0; ref < db.nextNumber(); ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      NonminimalProof proof;
      db.getInfo( ref, proof );

      if (ostr) *ostr << nl << "Proof #" << ref << nl;
      if (astr) *astr << nl;

      if (ostr) *ostr << proof << nl;
      if (astr) *ostr << proof << nl;

   }
}


//------------------------------------------------------------------

static void extUnknown()
{
   UnknownStateDatabase db = search.unknownStateDatabase();

   for ( int ref=0; ref < db.nextNumber(); ref++ )
   {
      if ( ! db.exists(ref) ) continue;

      UnknownState proof;
      db.getInfo( ref, proof );

      if (ostr) *ostr << nl << "Proof #" << ref << nl;
      if (astr) *astr << nl;

      PartialMinorProof pmp;
      db.getPMP( ref, pmp );
      if (ostr) *ostr << proof << nl << pmp << nl;
      if (astr) *astr << proof << nl << pmp << nl;

   }
}


//------------------------------------------------------------------
//------------------------------------------------------------------

void mainImport( int argc, char** argv )
{
   importProcessArgs( argc, argv );

   ostr = 0;
   astr = 0;

   switch ( extFormat )
   {
      case Text: ostr = &cout; break;
      case Ascii: astr = new astream( 1 ); // attach to cout
      default: aassert(false);
   }

   DatabaseBase::lockSystem();
   switch ( extType )
   {
      case Minimal: extMinimal(); break;
      case Nonminimal: extNonminimal(); break;
      case Unknown: extUnknown(); break;
      default: aassert(false);
   }
   DatabaseBase::unlockSystem();

}

//------------------------------------------------------------------

static void importUsage()
{
   fatalError( "usage: Import (type) [erase], type=m,n,u" );
}

static void importProcessArgs( int argc, char** argv )
{
   if ( argc == 3 )
   {
      if ( ! strcmp( argv[2], "erase" ) ) importUsage();
      extFormat = Ascii;
   }
   else
   {
      if ( argc != 2 ) importUsage();
   }

   switch ( argv[1][0] )
   {
      case 'm': extType = Minimal; break;
      case 'n': extType = Nonminimal; break;
      default: importUsage();
   }
}

#endif

void mainImport( int , char** )
{
}
