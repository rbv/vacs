
// ------------------------------------------
// --------------- fs_logs.c ----------------
// ------------------------------------------

/*tex
 
\file{fs_logs.c}
\path{src/filesystem}
\title{Log file support}
\classes{}
\makeheader
 
xet*/

#include "vacs/filesystem.h"

#include "vacs/runinfo.h"

const int FileManager::_logMax = 100000;

Filename FileManager::_logName;
ofstream FileManager::_log;
int      FileManager::_logExtensionNum;

// ----- logging ------

ostream& FileManager::log()
{
   //checkLog( _log, _logMax, _logName, _logExtensionNum );
   return _log;
}


// Log maintainence

void FileManager::checkLog( ofstream& log, int maxSize, 
                            const Filename& name, int& extensionNum )
{
   // What is the current size of the log?
   //
   assert( log );

   if ( log.tellp() > maxSize )
   {
      close( log );
      extensionNum++;
      openLog( log, name, extensionNum );
      logHeader( log );
      assert( log );
   }
}

void FileManager::openLog( ofstream& log, const Filename& name, 
                           int &extensionNum )
{
   // This is probably over the top; just trying to emulate the old code
   const char *fname = name.name( extensionNum );
   if ( fileExists( fname ) )
   {
      log.setstate( ios::badbit );
      return;
   }

   open( log, fname, logOpenMode );
}

void FileManager::logHeader( ostream& o )
{
   runInfo.printHeader( o );
   o << nl;
}

