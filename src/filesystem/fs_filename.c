
// ------------------------------------------
// --------------- fs_filename.c ------------
// ------------------------------------------

/*tex
 
\file{fs_filename.c}
\path{src/filesystem}
\title{Definitions for class Filename}
\classes{Filename}
\makeheader
 
xet*/

#include <stdlib.h>
#include <stdio.h>

#include "vacs/filesystem.h"


// create empty, and flag
//
Filename::Filename()
{
   extPos = -1;
   str = 0;
   alloc = 0;
}

// copy ctor
//
Filename::Filename( const Filename& f )
{
   //aassert( false ); // is it really needed?

   alloc = f.alloc;
   extPos = f.extPos;
   if (alloc)
   {
      str = new char[ alloc ];
      memcpy( str, f.str, alloc );
   }
   else
      str = 0;
}

// create and initialize str
//
Filename::Filename( const char* base, int extraSpace )
{
   int len = strlen( base );
   alloc = len + extraSpace + 1;
   str = new char[ alloc ];
   strcpy( str, base );
   str[ len ] = '\0';
   extPos = len;
}

Filename::Filename( const char* base1, const char* base2, int extraSpace )
{
   int len = strlen( base1 ) + strlen( base2 );
   alloc = len + extraSpace + 1;
   str = new char[ alloc ];
   strcpy( str, base1 );
   strcpy( str+strlen(base1), base2 );
   str[ len ] = '\0';
   extPos = len;
}

Filename::Filename( const Filename& fName, const char* base2, int extraSpace )
{
   int len = fName.extPos + strlen( base2 );
   alloc = len + extraSpace + 1;
   str = new char[ alloc ];
   strncpy( str, fName.str, fName.extPos );
   strcpy( str+fName.extPos, base2 );
   str[ len ] = '\0';
   extPos = len;
}

Filename::Filename( const Filename& fName, const char* base2, 
                    const char* base3, int extraSpace )
{
   int len = fName.extPos + strlen( base2 ) +strlen( base3 );
   alloc = len + extraSpace + 1;
   str = new char[ alloc ];
   strncpy( str, fName.str, fName.extPos );
   strcpy( str+fName.extPos, base2 );
   strcpy( str+fName.extPos+strlen(base2), base3 );
   str[ len ] = '\0';
   extPos = len;
}

Filename::~Filename()
{
   if ( extPos != -1 )
      delete [] str;
}

void Filename::operator=( const Filename& f )
{
   assert( f.alloc != 0 );

   if ( alloc ) delete [] str;

   alloc = f.alloc;
   extPos = f.extPos;
   str = new char[ alloc ];
   memcpy( str, f.str, alloc );
}

const char* Filename::name( int extension ) const
{
   assert( extPos != -1 );
   sprintf( str+extPos, "%d", extension );
   return str;
}

const char* Filename::name( const char* extension ) const
{
   assert( extPos != -1 );
   sprintf( str+extPos, "%s", extension );
   return str;
}

const char* Filename::name( const char extension ) const
{
   assert( extPos != -1 );
   str[ extPos ] = extension;
   str[ extPos+1 ] = '\0';
   return str;
}

Filename Filename::dirname() const
{
   const char *slash;
   slash = strrchr( str, '/' );
   if ( !slash )
       return Filename( "", 50 );

   Filename ret( str, 50 );
   ret.extPos = slash - str + 1;
   ret.str[ret.extPos] = '\0';
   return ret;
}
