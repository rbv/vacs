
// ------------------------------------------
// -------------- fs_command.c --------------
// ------------------------------------------

#if 0

/*tex
 
\file{fs_command.c}
\path{src/filesystem}
\title{Command file processing routines}
\classes{}
\makeheader
 
xet*/

#include <stdio.h>
#include "vacs/filesystem.h"
#include "vacs/runinfo.h"
#include "general/system.h"


void FileManager::writeLineToCommandFile( Str s )
{
   assert( runInfo.runType() != RunInfo::Manager );

   assert( s.length() < CommandLineMax - 1 );

   ofstream o;

   lockRequest( LSysC );

   // File must already exist
   open( o, commandFilename(), commandWriteMode );

   assert( o );

   o << s << " $ " << date << nl;

   assert( o );

   close( o );

   lockRequest( USysC );
}


Str FileManager::readLineFromCommandFile( int& pos )
{
   assert( runInfo.runType() == RunInfo::Manager );

   ifstream i;

   lockRequest( LSysC );

   open( i, commandFilename(), commandReadMode );

   // check for anything new
   //
   i.seekg( 0, ios::end );
   int filesize = (int) i.tellg();  

   if ( pos == filesize )
   {
      close( i );
      return Str( "" );
   }

   // seek to the specified position
   //
   i.seekg( pos );

   char s[ CommandLineMax ];

   i.get( s, CommandLineMax-1 );

   // record the new position 
   //
   pos = (int) i.tellg();

   close( i );

   lockRequest( USysC );

   return Str( s );

}

#endif
