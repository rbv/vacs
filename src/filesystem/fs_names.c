
// ------------------------------------------
// --------------- fs_names.c ---------------
// ------------------------------------------

/*tex
 
\file{fs_names.c}
\path{src/filesystem}
\title{File names}
\classes{}
\makeheader
 
xet*/

#include "vacs/filesystem.h"
#include "vacs/runinfo.h"


// constants appended to runDirectory to get subdirs and filenames
//
//const char* FileManager::_nodeBaseDirname = "nodes/";
//const char* FileManager::_snNodeFilename = "node";
//const char* FileManager::_snGraphFilename = "graph";
//const char* FileManager::_snChildFilename = "child";
//const char* FileManager::_snMinorFilename = "minor";

const char* FileManager::_commandFilename = "command";
//const char* FileManager::_minimalDBFilename = "minimal";
//const char* FileManager::_nonminimalDBFilename = "nonminimal";
const char* FileManager::_nodeStatusFilename = "searchstatus";
const char* FileManager::_managerStatusFilename = "runstatus";
const char* FileManager::_logDispatchBasename = "log/dispatch/log.";
const char* FileManager::_logManagerBasename = "log/manager/log.";
const char* FileManager::_logBrowserBasename = "log/browser/log.";
const char* FileManager::_logWorkerBasename = "log/worker/log.";

//const char* FileManager::_familyToolBin = "/project/cdf/vacs/fam/bin/";
//const char* FileManager::_executableFilename = "/project/cdf/vacs/exe/search";
Filename FileManager::_executableFilename;

const char* FileManager::_MDBBasename = "prob/mdb";
const char* FileManager::_NDBBasename = "prob/ndb";
const char* FileManager::_IsoDBBasename = "prob/isodb";
const char* FileManager::_SNBasename = "run/sn";
const char* FileManager::_SNCBasename = "run/snc";
const char* FileManager::_SNIBasename = "run/sni";
const char* FileManager::_SNUEBasename = "run/uedb";
const char* FileManager::_SNIIBasename = "run/iidb";


// ------ file names -------

const char* FileManager::mainFilename( const char* extension )
{
   return _mainDir.name( extension );
}

const char* FileManager::commandFilename()
{
   return mainFilename( _commandFilename );
}

const char* FileManager::nodeStatusFilename()
{
   return mainFilename( _nodeStatusFilename );
}

const char* FileManager::managerStatusFilename()
{
   return mainFilename( _managerStatusFilename );
}

// ----------------------------------------------------

const char* FileManager::MDBBasename()
{
   return mainFilename( _MDBBasename );
}

const char* FileManager::NDBBasename()
{
   return mainFilename( _NDBBasename );
}

const char* FileManager::SNBasename()
{
   return mainFilename( _SNBasename );
}

const char* FileManager::SNUEBasename()
{
   return mainFilename( _SNUEBasename );
}

const char* FileManager::SNIIBasename()
{
   return mainFilename( _SNIIBasename );
}

const char* FileManager::SNCBasename()
{
   return mainFilename( _SNCBasename );
}

const char* FileManager::SNIBasename()
{
   return mainFilename( _SNIBasename );
}

const char* FileManager::IsoDBBasename()
{
   return mainFilename( _IsoDBBasename );
}


DatabaseBase::AccessMode FileManager::standardAccessMode()
{
   int mode;

   switch( runInfo.runType() )
   {
      case RunInfo::Manager:
         mode =
            DatabaseBase::WriteAccess;
            //| DatabaseBase::WriteSync
         break;

      case RunInfo::Dispatch:
         mode = 0;
         break;

      case RunInfo::Worker:
         mode = 0;
         break;

      case RunInfo::Browser:
         mode = 0;
         break;

      default: aassert(false);
   }

   return DatabaseBase::AccessMode( mode );
}


DatabaseBase::AccessMode FileManager::standardCacheMode()
{
   int mode = int( standardAccessMode() );

   switch( runInfo.runType() )
   {
      case RunInfo::Manager:
         mode = mode | DatabaseBase::Cache;
         break;

      case RunInfo::Dispatch:
         //mode = mode | DatabaseBase::Cache;
         break;

      case RunInfo::Worker:
         break;

      case RunInfo::Browser:
         mode = mode | DatabaseBase::Cache;
         break;

      default: aassert(false);
   }

   return DatabaseBase::AccessMode( mode );
}

