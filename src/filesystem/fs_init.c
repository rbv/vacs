
// ------------------------------------------
// --------------- fs_init.c ----------------
// ------------------------------------------

/*tex
 
\file{fs_init.c}
\path{src/filesystem}
\title{File system initialization}
\classes{FileManager}
\makeheader
 
xet*/

#include "family/probinfo.h"
#include "vacs/filesystem.h"
#include "vacs/runinfo.h"
#include "error.h"
#include "general/system.h"

//#define DBG

// The global variable fileMngr is defined here
//
//FileManager fileMngr;

Filename FileManager::_mainDir;

FilesystemLock *FileManager::fs_locker;

//
// Initialization
//

void FileManager::initialize( const char* exeName )
{
   // Program for spawning tasks;
   //
   _executableFilename = Filename( exeName, 0 );

   // set up filenames
   //
   //_mainDir = Filename( ProbInfo::getPath(ProbInfo::Directory), 50 );
   _mainDir = Filename( "./", 50 );

   // init node file system
   //
   //nodeFileSystemInit();

   // logs - create filename
   //
   //System w;
   //Str nm( int( long(w) ) );

   char hostname[ MAXHOSTNAMELEN ];
   ::gethostname( hostname, MAXHOSTNAMELEN );

   Str nm( (int) getpid() );
   nm.append( '.' );
   nm.append( hostname );
   nm.append( '.' );

   switch ( runInfo.runType() )
   {
      case RunInfo::Manager: 
         _logName = Filename( _mainDir, _logManagerBasename, nm, 50 );
         break;

      case RunInfo::Dispatch: 
         _logName = Filename( _mainDir, _logDispatchBasename, nm, 50 );
         break;

      case RunInfo::Browser: 
         _logName = Filename( _mainDir, _logBrowserBasename, nm, 50 );
         break;

      case RunInfo::Worker: 
         _logName = Filename( _mainDir, _logWorkerBasename, nm, 50 );
         break;

      default: aassert( false );
   }

   _logExtensionNum = 1;

   //cerr << '.' << _logName.name() << '.' << nl;

   // open the log
   //
   openLog( _log, _logName, _logExtensionNum );

   //DBG( "log " << _logName.name("") << " " << _logExtensionNum << nl )

   assert( _log );

   logHeader( _log );

   // Set up this process as a client to the filesystem lock server.
   //
   if ( runInfo.runType() != RunInfo::Worker ) 
   {
     fs_locker = new FilesystemLock;
   }
}


void FileManager::destruction()
{
   // Tell lock server that we are done with the database.
   //
   if (fs_locker) delete fs_locker;

   // close the cached files
   //
   //nodeFileSystemDestruct();

   // close the logs
   //
   close( _log );
}


// manager initialization code
//
void FileManager::managerInitialization()
{
   assert( runInfo.runType() == RunInfo::Manager );

   // this is first run
   //
   log() << "Manager initializing" << nl;

   // initialize and write status
   //
   //RunInfo::initializeManagerStatus();
   //
   //RunInfo::saveManagerStatus();

#if 0
   // move old versions of writeable files to storage directory
   // create the directories, or check they exist
   // this is handled by a shell script
   //
   int ret;
   Str s( _familyToolBin );

   s.append( "searchRunFileInit " );
   s.append( _mainDir.name("") );
   s.append( " " );

   ret = system( s );
   if ( ret ) fatalError( "File system initialization error!" );

   // Create the command file (no locks needed here)
   //
   ofstream command;
   open( command, commandFilename(), ios::app );

   assert( command );

   close( command );

   // Save the run status.
   //
   RunInfo::saveManagerStatus();
#endif
}

//#undef DBG
