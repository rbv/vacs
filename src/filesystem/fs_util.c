 
// ------------------------------------------
// ---------------- fs_util.c ---------------
// ------------------------------------------
 
/*tex
 
\file{fs_util.c}
\path{src/filesystem}
\title{File utilities}
\classes{}
\makeheader
 
xet*/
 
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/resource.h>

#include "vacs/runinfo.h"
#include "vacs/filesystem.h"

#if 0
#define TEST1( c )						\
   if (f) cerr << RunInfo::runType() << ' ' << c 		\
               << ' ' << f.rdbuf()->fd() << nl;
#define TEST2( c, fn )						\
   if (f) cerr << RunInfo::runType() << ' ' << c 		\
               << ' ' << f.rdbuf()->fd() 			\
               << "\t\t" << fn << nl;
#else
#define TEST1( c )
#define TEST2( c, fn )
#endif


int maxFile()
{
   struct rlimit rlim;
   if ( getrlimit(RLIMIT_NOFILE, &rlim) )
      aasserten( false );
   return rlim.rlim_cur;
}

bool fileExists( const char* path )
{
   FILE *cfile;
   cfile = fopen( path, "r" );
   if (!cfile) return false;
   fclose( cfile );
   return true;
}

//-------------------------------------------------------

void FileManager::openRead( fstream& f, const char* fn, int mode )
{
   //lockRequest( LSysR );

   f.open( fn, (ios::openmode)mode );
   TEST2( "Or", fn );
   //if (f) lock( f, false );
}

void FileManager::openWrite( fstream& f, const char* fn, int mode )
{
   //lockRequest( LSysW );

   f.open( fn, (ios::openmode)mode );
   TEST2( "Ow", fn );
   //if (f) lock( f, true );
}

void FileManager::open( ifstream& f, const char* fn, int mode )
{
   //lockRequest( LSysR );

   f.open( fn, (ios::openmode)mode );
   TEST2( "Or", fn );
   //if ( f ) lock( f, false );
}

void FileManager::open( ofstream& f, const char* fn, int mode )
{
   //lockRequest( LSysW );

   f.open( fn, (ios::openmode)mode );
   TEST2( "Ow", fn );
   //if ( f ) lock( f, true );
}

//-------------------------------------------------------

void FileManager::closeWrite( fstream& f )
{
   f.flush();
   TEST2( "C ", ' ' );
   f.close();

   //lockRequest( USysW );
}

void FileManager::closeRead( fstream& f )
{
   TEST2( "C ", ' ' );
   f.close();

   //lockRequest( USysR );
}

void FileManager::closeNoUnlock( fstream& f )
{
   assert(false);
   f.flush();
   TEST2( "C ", ' ' );
   f.close();
}

void FileManager::close( ifstream& f )
{
   //cerr << "close 2 " << nl;
   //if (f) unlock( (fstreambase&) f );
   TEST2( "C ", ' ' );
   f.close();

   //lockRequest( USysR );
}

void FileManager::close( ofstream& f )
{
   //cerr << "close 3 " << nl;
   f.flush();
   //if (f) unlock( (fstreambase&) f );
   TEST2( "C ", ' ' );
   f.close();

   //lockRequest( USysW );
}

//-------------------------------------------------------
// * *** *** ** ***** ***** *** ****** * ******* *** ***
//-------------------------------------------------------

#ifdef OLD_LOCK_STUFF

inline void FileManager::lock( fstreambase& f, bool writeLock )
{
   //assert(false); // use lock server

   int fd = f.rdbuf()->fd();

   struct flock l;
   l.l_whence = SEEK_SET;
   l.l_start = 0;
   l.l_len = 0;

   int flOp;

   if ( writeLock )
   {
      l.l_type = F_WRLCK;
      TEST1( "Lw" )
      flOp = LOCK_EX;
   }
   else
   {
      l.l_type = F_RDLCK;
      TEST1( "Lr" )
      flOp = LOCK_SH;
   }

   //asserten( fcntl( fd, F_SETLKW, int(&l) ) != -1 );
   aasserten( flock( fd, flOp ) != -1 );

   //off_t oldPos = lseek( fd, 0, SEEK_CUR );
   //asserten( oldPos != -1 );
   //asserten( lseek( fd, 0, SEEK_SET ) != -1 );
   //asserten( lockf( fd, F_LOCK, 0 ) != -1 );
   //asserten( lseek( fd, oldPos, SEEK_SET ) != -1 );
}


// not inline because return is sorried
//
void FileManager::unlock( fstreambase& f )
{
   //assert(false); // use lock server

   int fd = f.rdbuf()->fd();

   if ( fd == -1 ) return;

   TEST1( "U " )
/*
   {
      struct flock l;
      l.l_type = F_WRLCK;
      l.l_whence = SEEK_SET;
      l.l_start = 0;
      l.l_len = 0;

      aasserten( fcntl( fd, F_GETLK, int(&l) ) != -1 );
      int t = ( l.l_type == F_UNLCK ) ? true : false;

      if (f) cerr << RunInfo::runType() << ' ' << "U " 
                  << ' ' << f.rdbuf()->fd() << ' ' << t << nl;
   }
*/

   struct flock l;

   l.l_type = F_UNLCK;
   l.l_whence = SEEK_SET;
   l.l_start = 0;
   l.l_len = 0;

   //cerr << "unlock " << runInfo.runType() << ' ' << fd << nl;

   //struct stat buf;
   //asserten( fstat( fd, &buf ) != -1 );

   //asserten( fcntl( fd, F_SETLKW, int(&l) ) != -1 );
   aasserten( flock( fd, LOCK_UN ) != -1 );

   //off_t oldPos = lseek( fd, 0, SEEK_CUR );
   //asserten( oldPos != -1 );
   //asserten( lseek( fd, 0, SEEK_SET ) != -1 );
   //asserten( lockf( fd, F_ULOCK, 0 ) != -1 );
   //asserten( lseek( fd, oldPos, SEEK_SET ) != -1 );
}

void FileManager::close( fstream& f )
{
   assert(false);
   f.flush();
   if (f) unlock( (fstreambase&) f );
   TEST2( "C ", ' ' );
   f.close();
}

void FileManager::unlock( ifstream& f )
{
   assert(false);
   if (f) unlock( (fstreambase&) f );
}

void FileManager::unlock( ofstream& f )
{
   assert(false);
   f.flush();
   if (f) unlock( (fstreambase&) f );
}

void FileManager::unlock( fstream& f )
{
   assert(false);
   f.flush();
   if (f) unlock( (fstreambase&) f );
}

//-------------------------------------------------------

void FileManager::lock( fstream& f, bool writeLock )
{
   assert(false);
   if (f) lock( (fstreambase&) f, writeLock );
}

void FileManager::lock( ofstream& f )
{
   assert(false);
   if (f) lock( (fstreambase&) f, true );
}

void FileManager::lock( ifstream& f )
{
   assert(false);
   if (f) lock( (fstreambase&) f, false );
}

#endif
