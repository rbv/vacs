
// ------------------------------------------
// -------------- fs_searchstatus.c ---------
// ------------------------------------------

/*tex
 
\file{fs_searchstatus.c}
\path{src/filesystem}
\title{Search status file support routines}
\classes{}
\makeheader
 
xet*/

#include "vacs/filesystem.h"
#include "vacs/runinfo.h"
#include "search/search.h"

#if 0

// Search status support

// save to disk
//
void FileManager::saveNodeStatus( Search& s )
{
   assert( runInfo.runType() == RunInfo::Manager );

   ofstream file;

   open( file, nodeStatusFilename(), ios::out );

   assert( file );

   s.saveToFile( file );

   assert( file );

   close( file );
}

// update from disk
// return false on error (caller handles error)
//
void FileManager::loadNodeStatus( Search& s )
{
   //assert( runInfo.runType() != RunInfo::Manager );

   ifstream file;

   open( file, nodeStatusFilename() );

   assert( file );

   s.loadFromFile( file );

   // check for error
   assert( file );

   close( file );
}


bool FileManager::assertNodeStatusFile()
{
   ifstream f;

   open( f, nodeStatusFilename() );
 
   bool ret = f ? true : false;
 
   close( f );
 
   return ret;
}

#endif
