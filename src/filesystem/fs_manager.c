
// ------------------------------------------
// ------------ fs_runstatus.c --------------
// ------------------------------------------

/*tex
 
\file{fs_runstatus.c}
\path{src/filesystem}
\title{File system run status rountines}
\classes{}
\makeheader
 
xet*/

#include "vacs/filesystem.h"


// class RunStatus support

void FileManager::openManagerStatusFileForWrite( ofstream& f )
{
   //FileManager::lockRequest( LSysW );
   open( f, managerStatusFilename(), ios::out );
}

void FileManager::openManagerStatusFileForRead( ifstream& f )
{
   //FileManager::lockRequest( LSysR );
   open( f, managerStatusFilename(), ios::in );
}

bool FileManager::assertManagerStatusFile()
{
   ifstream f;

   // no lock check here...

   open( f, managerStatusFilename() );

   bool ret = f ? true : false;

   close( f );

   return ret;
}

