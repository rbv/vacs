

#include "general/stream.h"
#include "search/nodeinfo.h"
#include "search/search.h"

//---------------------------------------------------------------

ostream& operator<<( ostream& s, const ProofUsage& usage )
{
   if ( usage.isUsed() ) s << "used by " << usage.usedBy();
   else s << "not used";

   IsoDBItem1 &iso1 = search.isoDatabase().getIso1( usage.isoRef() );
   s << " iso ref " << usage.isoRef() << " := " << iso1;
   return s;
}

//---------------------------------------------------------------

ostream& operator<<( ostream& s, const MinimalProof& p )
{ return s << p.typeAsString() 
           << ',' << (ProofUsage&) p;
           //<< " Crtd: " << p.created()
           //<< ", Updtd: " << p.updated(); 
}

ostream& operator<<( ostream& s, const NonminimalProof& p )
{ 
   s << p.typeAsString() << ',' << (ProofUsage&) p;
   if ( p.type() == NonminimalProof::CongruentMinor ||
        p.type() == NonminimalProof::Testset )
           s << ", minor: " << p.congruentMinor();
   return s;

           //<< " Crtd: " << p.created()
           //<< ", Updtd: " << p.updated();
}

ostream& operator<<( ostream& s, const UnknownState& p )
{ return s << p.typeAsString()
           << ' ' << (ProofUsage&) p;
           //<< ", searches: " << p.extensionSearches();  // not used
}

ostream& operator<<( ostream& s, const IrrelevantInfo& p )
{
   s << p.typeAsString();
   if ( p.type() == IrrelevantInfo::Isomorphic )
      s << ',' << p.isomorphic();
   if ( p.type() == IrrelevantInfo::IsoSibling )
      s << ',' << p.isomorphic();
   if ( p.type() == IrrelevantInfo::Pretest )
      s << ',' << p.pretest();
   return s;
}

//-------------------------------------------------------------------

texstream& operator<<( texstream& s, const MinimalProof& p )
{ s.ostr() << p; return s; }

texstream& operator<<( texstream& s, const NonminimalProof& p )
{ s.ostr() << p; return s; }

texstream& operator<<( texstream& s, const UnknownState& p )
{ s.ostr() << p; return s; }

texstream& operator<<( texstream& s, const IrrelevantInfo& p )
{ s.ostr() << p; return s; }

//-------------------------------------------------------------------

astream& operator<<( astream& s, const MinimalProof& p )
{
   return s << putLeft << p._type << putRight;
}

astream& operator>>( astream& s, MinimalProof& p )
{ 
   int i;
   s >> getLeft >> i >> getRight;
   p._type = MinimalProof::ProofType(i);
   return s;
}

astream& operator<<( astream& s, const NonminimalProof& p )
{
   s << putLeft << p._type;
   if ( p.type() == NonminimalProof::CongruentMinor || 
        p.type() == NonminimalProof::Testset ) s << p._minor;
   s << putRight;
   return s;
}

astream& operator>>( astream& s, NonminimalProof& p )
{
   int i;
   s >> getLeft >> i;
   if ( p.type() == NonminimalProof::CongruentMinor ||
        p.type() == NonminimalProof::Testset ) s >> p._minor;
   s >> getRight;
   p._type = NonminimalProof::ProofType(i);
   return s;
}

astream& operator<<( astream& s, const UnknownState& p )
{ 
   return s << putLeft << p._extensionSearches << putRight;
}

astream& operator>>( astream& s, UnknownState& p )
{ 
   return s >> getLeft >> p._extensionSearches >> getRight;
}

astream& operator<<( astream& s, const IrrelevantInfo& p )
{ 
   return s << putLeft << p._data._pretest << putRight;
}

astream& operator>>( astream& s, IrrelevantInfo& p )
{ 
   return s >> getLeft >> p._data._pretest >> getRight;
}
