
/*tex
 
\file{graph_io.c.c}
\path{src/i-o}
\title{Stream I/O for graph classes}
\classes{}
\makeheader
 
xet*/

#include <iostream>
//#include <stdlib.h>

#include "graph/graph.h"
#include "graph/bgraph.h"
#include "general/stream.h"

//
// Binary version
//

bostream& operator<<( bostream& s, const Graph &G )
{
  s << vertNum( G.order() );
  if ( G.order() ) 
    s.write( (char*) G._adj[0], sizeof(bool)*G.order()*G.order() );

  return s;
} 

bistream& operator>>( bistream& s, Graph &G )
{						
  vertNum n;
  s >> n;

  G.newGraph(n);

  if (n) s.read( (char*) G._adj[0], sizeof(bool)*n*n );

  return s;
}

bostream& operator<<( bostream& s, const BGraph &G )
{
  s << CAST( G, Graph );

  s << G._bndry << G._bFlag;

  return s;
} 

bistream& operator>>( bistream& s, BGraph &G )
{						
  s >> CAST( G, Graph );

  s >> G._bndry >> G._bFlag;

  return s;
}

