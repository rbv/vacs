
#include "general/stream.h"
#include "bpg/oparray.h"


bistream& operator>>( bistream& s, OpArray& A )
{ return s >> CAST( A, Array<Operator> ); }

bostream& operator<<( bostream& s, const OpArray& A )
{ return s << CAST( A, Array<Operator> ); }

ostream& operator<<( ostream& o, const OpArray& A )
{ return o << CAST( A, Array<Operator> ); }

astream& operator<<( astream& s, const OpArray& A )
{ return s << CAST( A, Array<Operator> ); }

astream& operator>>( astream& s, OpArray& A )
{ return s >> CAST( A, Array<Operator> ); }

