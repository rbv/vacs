
/*tex
 
\file{testset_io.c.c}
\path{src/i-o}
\title{Stream I/O for testset clas}
\classes{}
\makeheader
 
xet*/

#include <iostream>
//#include <stdlib.h>

#include "graph/bgraph.h"
#include "general/stream.h"
#include "family/testset/testset.h"

// Binary version
//
bistream& operator>>( bistream& s, TestSet &TS )
{						
  int fk;
  s >> fk;
  //cerr << "fk = " << fk << nl;
  aassert(fk = TS._fixedK);

  int numTests;
  s >> numTests;
  cerr << "numTests = " << numTests << " ... "; 

#if 0
  vertNum bs;
  s >> bs;
  cerr << "boundarySize = " << bs << nl;
#endif

  TS.resize(numTests);

  for (int i=0; i<numTests; i++)
  {
    BGraph *tmp = new BGraph;
    s >> *tmp;
    TS[i] = tmp;
    //cerr << "read test: " << *(TS.get(i));
  }

  cerr << "load completed.\n";
  return s;
}

bostream& operator<<( bostream& s, const TestSet &TS )
{
  s << int( TS._fixedK ) << int( TS.size() );
  cerr << "writing " << TS.size() << " tests\n";

#if 0
  s << vertNum( TS.get(0)->boundarySize() );
  cerr << " with bs = " << TS.get(0)->boundarySize() << nl;
#endif

  for (int i=0; i < TS.size(); i++)
  {
    s << *(TS.get(i));
//    cerr << "wrote test: " << *(TS.get(i));
  }

  return s;
} 

