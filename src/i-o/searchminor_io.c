
#include "search/searchminor.h"
#include "graph/i-o/psspec.h"
#include "general/stream.h"

#undef minor


ostream& operator<<( ostream& s, const SearchMinor& m )
{
   //s << "type: ";
   s << ' ';
   switch ( m._type )
   {
      case SearchMinor::DeleteVertex: s << "dv"; break;
      case SearchMinor::DeleteEdge:   s << "de"; break;
      case SearchMinor::ContractEdge: s << "ce"; break;
      default: aassert(false);
   }
   s << ", pos=" << m._parsedInfo1;
   return s;
}

astream& operator<<( astream& s, const SearchMinor& m )
{
   return s << putLeft << m._type << m._parsedInfo1 << m._parsedInfo2 << putRight;
}

astream& operator>>( astream& s, SearchMinor& m )
{
   return s >> getLeft >> m._type >> m._parsedInfo1 >> m._parsedInfo2 >> getRight;
}

//------------------------------------------------------------

bistream& operator>>( bistream& s, PartialMinorProof& p )
{ 
   p._distinguishers.in(s);
   p._minors.in(s);
   p._proofs.in(s);
   s >> p.nextFreeDistNumber >> p.nextFreeMinorNumber;
   return s;
}

bostream& operator<<( bostream& s, const PartialMinorProof& p )
{
   p._distinguishers.out(s);
   p._minors.out(s);
   p._proofs.out(s);
   s << p.nextFreeDistNumber << p.nextFreeMinorNumber;
   return s;
}

ostream& operator<<( ostream& s, const PartialMinorProof& p )
{
   s << "PMP: " << nl
     << "   distrs: "; p._distinguishers.out(s); s << nl
     << "   minors: "; p._minors.out(s); s << nl
     << "   proofs: "; p._proofs.out(s); s << nl
     << "   num distrs = " << p.nextFreeDistNumber 
     << "   num minors = " << p.nextFreeMinorNumber << nl;
   return s;
}


astream& operator<<( astream& s, const PartialMinorProof& p )
{
     p._distinguishers.out(s); s << eol;
     p._minors.out(s); s << eol;
     p._proofs.out(s); s << eol;
   return s;
}


static Array< MinorNumber > minorNums;
static const PartialMinorProof* pmpCB;

static PSSpec::PSEdgeType edgeCallback( vertNum index, vertNum, const void*  )
{
   bool del = false;
   bool cont = false;
   const SearchMinorDictionary& minors = pmpCB->minors();

   for ( int i=0; i<minorNums.size(); i++ )
   {
      SearchMinor minor = minors.get( minorNums[i] );
      if ( minor.parsedPosition() == index && minor.type() == SearchMinor::DeleteEdge )
         del = true;
      if ( minor.parsedPosition() == index && minor.type() == SearchMinor::ContractEdge )
         cont = true;
   }
   if ( del )
      if ( cont ) return PSSpec::BoldDashed;
      else return PSSpec::Dashed;
   else
      if ( cont ) return PSSpec::Bold;
      else return PSSpec::Normal;
}

texstream& PartialMinorProof::texOut( texstream& s, const RBBPG& G )
{
   PartialMinorProof& pmp = *this;

   //s << "\\# distinguishers = " << pmp.nextFreeDistNumber << "\\\\" << nl;
   //s << "\\# minors = " << pmp.nextFreeMinorNumber << "\\\\" << nl;

   const DistinguisherDictionary& dists = pmp.distinguishers();
   DistinguisherNumber distNum;

   int height = 20 * Operator::boundarySize();
   PSSpec spec( 0, height, PSSpec::DviSpecial );
   spec.setOut( s.ostr() );
   spec.setEdgeTypeFnc( edgeCallback );
   pmpCB = &pmp;

   ForAllDictionaryItems( dists, distNum )
   {
      pmp.minorsDistinguishedByDistinguisher( distNum, minorNums );
      //s.ostr() << dist << ':' << minorNums << nl;

      Distinguisher dist = dists.get( distNum );
      CBBPG glue( G, dist );
      s << " Proof: " << glue << "\\\\" << nl;
      bpg2ps( glue, &spec );
      s << "\\\\" << nl;
   }

   if ( ! pmp.proofIsComplete() )
   {
      pmp.minorsDistinguishedByDistinguisher( -1, minorNums );
      s << "Unproven: \\\\" << nl;
      bpg2ps( G, &spec );
      s << "\\\\" << nl;
      //s.ostr() << "-1" << ':' << minorNums << nl;
   }

   return s;
}

