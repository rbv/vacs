
/*tex
 
\file{matrix_io.c.c}
\path{src/i-o}
\title{Specialisations for matrix classes}
\classes{}
\makeheader
 
xet*/

#include <iostream>
//#include <stdlib.h>

#include "general/stream.h"
#include "array/matrix.h"

// Binary version
//
#define BitwiseMatrixIO( T )					\
   template<>											\
   bistream& operator>>( bistream& s, SquareMatrix<T>& A )	\
   {								\
      int z;							\
      s >> z;							\
      assert( z >= 0 );						\
      A.setDim( z );						\
      if ( z )							\
        s.read( (char*) *A._s, z * z * sizeof(T) );		\
      return s;							\
   }								\
   template<>											\
   bostream& operator<<( bostream& s, const SquareMatrix<T>& A )\
   { 								\
      s << A.dim();						\
      if (A.dim() )						\
       s.write( (char*) *A._s, A.dim() * A.dim() * sizeof(T) );	\
      return s;							\
   } 								\
//cerr << "matrix dump\n";

//--------------------------------------------------------------

#if 0
NOT DONE YET!
#define MemberwiseArrayIO( T )					\
   template<>											\
   bostream& operator<<( bostream& s, const Array<T>& A )	\
   {								\
      int z = A.size();						\
      s << z;							\
      for ( int i = 0; i < z; i++ ) s << A[i];			\
      return s;							\
   }								\
   template<>											\
   bistream& operator>>( bistream& s, Array<T>& A )		\
   {								\
      int z;							\
      s >> z;							\
      assert( z >= 0 );						\
      A.allocate( z );						\
      A.setSize( z );						\
      for ( int i = 0; i < z; i++ ) s >> A[i];			\
      return s;							\
   }
#endif

//-----------------------------------------------------------------

BitwiseMatrixIO( short )
BitwiseMatrixIO( char )
#if defined(__DECCXX) | defined(__GNUC__)
BitwiseMatrixIO( bool )
#endif

