
#include "general/stream.h"
#include "bpg/operator.h"

bistream& operator>>( bistream& s, Operator& op )
{ return s >> op.op; }

bostream& operator<<( bostream& s, const Operator& op )
{ return s << op.op; }

static const char *opChars = "0123456789abcdef";

ostream& operator<<( ostream& o, const Operator& op )
{
   if ( op.isVertexOp() )
      o << opChars[ int( op.lowNibble() ) ];
   else if ( op.isEdgeOp() )
      o << opChars[int( op.lowNibble() )] << opChars[int( op.highNibble() ) ];
   return o;
}

astream& operator<<( astream& s, const Operator& op )
{
   if ( op.isVertexOp() )
      s.put( opChars[ int( op.lowNibble() ) ] );
   else if ( op.isEdgeOp() )
      s.put( opChars[int( op.lowNibble() )] );
      s.put( opChars[int( op.highNibble() ) ] );
   return s;
}

/*
int hex2int( char ch )
{
	if (ch >= 'a' && ch <= 'f')
		return 10 + ch - 'a';
	else if (ch >= '0' && ch <= '9')
		return ch - '0';
	return -1;
}
*/

astream& operator>>( astream& s, Operator& op )
{
   ios::fmtflags oldflags = s.flags();
   s.setf( ios::hex );
   int i;
   s >> i;
   s.flags( oldflags );

   assert( i >= 0 && i < 256 );
   if ( i < 10 )
      op = Operator::vertexOperator( i );
   else
      op = Operator::edgeOperator( i/16, i%16 );
   return s;
}
