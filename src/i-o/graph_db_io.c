
#include "general/stream.h"
#include "search/graph_db.h"

namespace Schmidt_Druffel
{

ostream& operator<<( ostream& o, const Schmidt_Druffel::IsoDBItem1& item )
{
  o << static_cast< IsoDBItem1Base >( item )
    << " hash=" << item.hash
    << nl;
  return o;
}

} // end namespace

namespace NAUTY_Iso
{

ostream& operator<<( ostream& o, const NAUTY_Iso::IsoDBItem1& item )
{
  o << static_cast< IsoDBItem1Base >( item )
    << " hash=" << item.hash
    << nl;
  return o;
}

} // end namespace


ostream& operator<<( ostream& o, const IsoDBItem1Base& item )
{
   o << " parselen=" << item.parseLen() << " proofref=";
   if ( item.proofRef().isUsed() ) o << item.proofRef(); else o << "not used";
   return o;
}


#if 0

bistream& operator>>( bistream& i, IsoDBItem& item )
{ return i >> item._graph; }

bostream& operator<<( bostream& o, const IsoDBItem& item )
{ return o << item._graph; }

ostream& operator<<( ostream& o, const IsoDBItem& item )
{ return o << item._graph; }

#endif
