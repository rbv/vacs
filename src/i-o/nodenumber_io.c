

#include "general/stream.h"
#include "search/nodenum.h"

bistream& operator>>( bistream& s, NodeNumber& n )
{
   int num;
   s >> num;
   n.num = num;
   return s;
}

bostream& operator<<( bostream& s, const NodeNumber& n )
{ return s << n.num; }

ostream& operator<<( ostream& o, const NodeNumber& n )
{ return o << '#' << n.num; }

texstream& operator<<( texstream& o, const NodeNumber& n )
{ return o << "\\#" << n.num; }

astream& operator<<( astream& o, const NodeNumber& n )
{ return o << '#' << n.num; }

astream& operator>>( astream& o, NodeNumber& n )
{ 
   char ch;
   o >> ch;
   assert( ch == '#' );
   int num;
   o >> num;
   n.num = num;
   return o;
}

