

#include "general/stream.h"
#include "search/nodeinfo.h"
#include "search/searchnode.h"



#ifndef CRAY

#define BitwiseBstreamIO( T )						\
   bistream& operator>>( bistream& s, T &t )				\
   { s.read( (char*) &t, sizeof(T) ); return s; }			\
									\
   bostream& operator<<( bostream& s, const T &t )			\
   { s.write( (char*) &t, sizeof(T) ); return s; }

BitwiseBstreamIO( MinimalProof )
BitwiseBstreamIO( NonminimalProof )
BitwiseBstreamIO( IrrelevantInfo )
BitwiseBstreamIO( UnknownState )
BitwiseBstreamIO( SearchNode )
BitwiseBstreamIO( SearchMinor )

#else

#define BitwiseVoid( T )						\
   bistream& operator>>( bistream& s, T& )				\
   { aassert(false); return s; }					\
   bostream& operator<<( bostream& s, const T& )			\
   { aassert(false); return s; }


BitwiseVoid( MinimalProof )
BitwiseVoid( NonminimalProof )
BitwiseVoid( IrrelevantInfo )
BitwiseVoid( UnknownState )
BitwiseVoid( SearchNode )

bistream& operator>>( bistream& s, SearchMinor& t )
{ 
  s.read( (char*) &t + 4, 2 ); //short
  s.read( (char*) &t + 12, 2 ); //short
  s.read( (char*) &t + 16, 2 ); //2 chars
  return s; 
}
bostream& operator<<( bostream& s, const SearchMinor& t )
{ 
  s.write( (char*) &t + 4, 2 );
  s.write( (char*) &t + 12, 2 );
  s.write( (char*) &t + 16, 2 ); 
  return s;
}
#endif

