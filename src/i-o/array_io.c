
// This is not a template file!

/*tex
 
\file{io_array.c.c}
\path{src/i-o}
\title{Specializations for array classes}
\classes{}
\makeheader
 
xet*/

#include <iostream>
//#include <stdlib.h>

#include "general/stream.h"
#include "vacs/iosext.h"
#include "array/array.h"
#include "array/dyarray.h"

#include "bpg/operator.h"
#include "bpg/bpg.h"
#include "search/nodenum.h"
#include "search/searchminor.h"

ostream& operator<<( ostream &o, const Array<bool> &AB )
{
  o << '[';
  for (int i=0; i<AB.size(); i++)
   {
	o << (AB[i]?'T':'F');
   }
  return o << ']';
}

ostream& operator<<( ostream &o, const DyArray<bool> &AB )
{
  o << '[';
  for (int i=0; i<AB.size(); i++)
   {
	o << (AB[i]?'T':'F');
   }
  return o << ']';
}

//-------------------------------------------------------------

#if 1

template<>
void Array<uchar>::loadFromFile( istream& s )
{
   // load size and alloc self
   //
   int newSize;
   ::get( s, newSize );

   resizeNoCopy( newSize );

   if ( newSize > 0 )
      s.read( (char*)array, size() );
}

template<>
void Array<uchar>::saveToFile( ostream& s )
{
   ::put( s, ulong(_size) );

   s.write( (char*)array, size() );
}

//--------------------------------------------------------------

template<>
void Array<int>::loadFromFile( istream& s, int length )
{
   resizeNoCopy( length );

   if ( length > 0 )
      s.read( (char*) array, size()*sizeof(int) );
}

template<>
void Array<int>::saveToFileNoLength( ostream& s )
{
   s.write( (char*) array, size()*sizeof(int) );
}

#endif


//--------------------------------------------------------------

/*
 Specialization of ctor from Buffer, to get around cfront bug.
*/

// Binary version
//
#define BitwiseArrayIO( T )					\
   template<>											\
   bistream& operator>>( bistream& s, Array<T>& A )		\
   {								\
      int z;							\
      s >> z;							\
      if (s.eof() || s.fail()) { A.resize(0); return s;	}	\
      assert( z >= 0 );						\
      A.resizeNoCopy( z );					\
      if (z)							\
         s.read( (char*) A.baseAddr(), z * sizeof(T) );		\
      return s;							\
   }								\
   template<>											\
   bostream& operator<<( bostream& s, const Array<T>& A )	\
   { 								\
      s << A.size();						\
      if (A.size())						\
        s.write( (char*) A.baseAddr(), A.size() * sizeof(T) );	\
      return s;							\
   } 								\

//--------------------------------------------------------------

#define MemberwiseArrayIO( T )					\
   template<>											\
   bostream& operator<<( bostream& s, const Array< T >& A )	\
   {								\
      int z = A.size();						\
      s << z;							\
      for ( int i = 0; i < z; i++ ) s << A[i];			\
      return s;							\
   }								\
   template<>											\
   bistream& operator>>( bistream& s, Array< T >& A )		\
   {								\
      int z;							\
      s >> z;							\
      assert( z >= 0 );						\
      A.allocate( z );						\
      A.setSize( z );						\
      for ( int i = 0; i < z; i++ ) s >> A[i];			\
      return s;							\
   }

//-----------------------------------------------------------------

BitwiseArrayIO( char )

#ifdef CRAY
MemberwiseArrayIO( Operator )
MemberwiseArrayIO( NodeNumber )
MemberwiseArrayIO( short )
MemberwiseArrayIO( int )
MemberwiseArrayIO( unsigned int )
MemberwiseArrayIO( SearchMinor )
#else
BitwiseArrayIO( Operator )
BitwiseArrayIO( NodeNumber )
BitwiseArrayIO( short )
BitwiseArrayIO( int )
BitwiseArrayIO( unsigned int )
BitwiseArrayIO( SearchMinor )
#endif

MemberwiseArrayIO( NBBPG )
MemberwiseArrayIO( Array< int > )
#if defined(__DECCXX) || defined(__GNUC__)
MemberwiseArrayIO( bool )
#endif

