
#include "general/stream.h"
#include "search/search.h"
#include "search/searchnode.h"
#include "search/proof_db.h"
#include "graph/i-o/psspec.h"
#include "family/family.h"


void Search::texNode( texstream& s, NodeNumber n )
{
   const SearchNode& sn = node( n );

   RBBPG G;
   graph( n, G );

   int height = 20 * G.boundarySize();
   PSSpec spec( 0, height, PSSpec::DviSpecial );
   spec.setOut( s.ostr() );

   s << "\\noindent" << nl;
   s << "\\underline{Tree node " << n << "}\\\\" << nl;

   s << "Graph:" << G << "\\\\" << nl;
   s << "mbrship: " << sn.membershipAsString() << "\\\\" << nl;
   bpg2ps( G, &spec );
   s << "\\\\" << nl;

   //s << "\\noindent" << nl

   s << "parent: " << sn.parent()
     << ", depth: " << sn.depth() 
     << ", grow status: " << sn.growStatusAsString() << "\\\\" << nl;

   s << "status: " << sn.statusAsString()
     << ", reference: " << sn.statusAsChar() 
     << sn.proofReference() << "\\\\" << nl;

   if ( sn.growStatus() == SearchNode::Grown )
   {
      Array<NodeNumber> ch;
      children( n, ch );
      s << "children: " << ch << "\\\\" << nl;
   }

   MinimalProof mp;
   NonminimalProof np;
   UnknownState us;
   IrrelevantInfo ii;

   switch( sn.status() )
   {
      case SearchNode::Minimal:  minimalProof( n, mp ); s << mp; break;
      case SearchNode::Nonminimal: nonminimalProof( n, np ); s << np; break;
      case SearchNode::Unknown: unknownState( n, us ); s << us; break;
      case SearchNode::Irrelevant: irrelevantInfo( n, ii ); s << ii; break;
      default: aassert(false);
   }
   s << "\\\\" << nl;

   PartialMinorProof pmp;
   switch( sn.status() )
   {
      case SearchNode::Minimal:  
      {
         MinimalProof minProof;
         search.minimalProof( n, minProof );
         if ( minProof.type() == MinimalProof::DistinguisherProof )
         {
            minimalProofPMP( n, pmp ); 
            pmp.texOut( s, G ); 
         }
         break;
      }
      case SearchNode::Unknown: 
      {
         //RBBPG G;
         //search.unknownStateDatabase().graph( sn.unknownState(), G );
         //unknownStatePMP( n, pmp ); 
         //pmp.texOut( s, G );
         break;
      }
   }

}

//----------------------------------------------------------------------

void Search::printNode( ostream& s, NodeNumber n, int level )
{
   const SearchNode& sn = node( n );

   RBBPG G;
   graph( n, G );

   s << n << ' ' << G << nl
     << "depth:   " << sn.depth() << nl
     << "parent:  " << sn.parent() << nl
     << "status:  " << sn.statusAsString() << nl
     << "proof:   " << sn.statusAsChar() << sn.proofReference() << nl
     << "mbrship: " << sn.membershipAsString() << nl
     << "grow:    " << sn.growStatusAsString() << nl;

   if ( sn.growStatus() == SearchNode::Grown )
   {
      Array<NodeNumber> ch;
      children( n, ch );
      s << "children:" << ch << nl;
   }

   if ( level < 1 ) return;

   MinimalProof mp;
   NonminimalProof np;
   UnknownState us;
   IrrelevantInfo ii;

   s << "   ";
   switch( sn.status() )
   {
      case SearchNode::Minimal:  minimalProof( n, mp ); s << mp; break;
      case SearchNode::Nonminimal: nonminimalProof( n, np ); s << np; break;
      case SearchNode::Unknown: unknownState( n, us ); s << us; break;
      case SearchNode::Irrelevant: irrelevantInfo( n, ii ); s << ii; break;
      default: aassert(false);
   }
   s << nl;

   if ( level < 2 ) return;

   PartialMinorProof pmp;
   switch( sn.status() )
   {
      case SearchNode::Minimal:  
      {
         MinimalProof minProof;
         search.minimalProof( n, minProof );
         if ( minProof.type() == MinimalProof::DistinguisherProof )
         {
            minimalProofPMP( n, pmp ); s << pmp; break;
         }
         break;
      }
      case SearchNode::Unknown: 
      {
         //unknownStatePMP( n, pmp ); s << pmp;
         break;
      }
   }
   s << nl;

   if ( level < 3 ) return;


}

//----------------------------------------------------------------------

void PartialMinorProof::dumpCongruenceStates( ostream& o, RBBPG& G )
{

   if ( ! Family::congruenceAvailable() ) return;

   Congruence* C = Family::newCongruence();

   C->compute( G );

   o << "State for graph: ";
   C->out( o );
   o << nl;

   PartialMinorProof pmp;
   pmp.addAllReductions( G );

   const SearchMinorDictionary& sm = pmp.minors();
   MinorNumber i;

   ForAllDictionaryItems( sm, i )
   {
      RBBPG H = G;
      const SearchMinor& m = sm.get(i);
      m.calcMinor( H );

      o << "Minor: " << m << ' ' << H << nl << "state: ";
      C->compute( H );
      C->out( o );
      o << nl;
   }

}

