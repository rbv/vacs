
#include "general/stream.h"
#include "bpg/bpg.h"

bistream& operator>>( bistream& s, BPG& G )
{ 
   s >> CAST( G, OpArray );
   G.changed();	//mjd July 30, 1993
//   s >> G._numVertices >> G._numEdges;
   return s;
}

bostream& operator<<( bostream& s, const BPG& G )
{ 
   s << CAST( G, OpArray );
//   s << G._numVertices << G._numEdges;
   return s;
}

ostream& operator<<( ostream& o, const BPG& G )
{ return o << CAST( G, OpArray ); }

astream& operator<<( astream& s, const BPG& G )
{ return s << CAST( G, OpArray ); }

astream& operator>>( astream& s, BPG& G )
{ return s >> CAST( G, OpArray ); }

// -----------------------------------------------

bistream& operator>>( bistream& i, NBBPG& G )
{ return i >> CAST( G, BPG ); }

bostream& operator<<( bostream& o, const NBBPG& G )
{ return o << CAST( G, BPG ); }

ostream& operator<<( ostream& o, const NBBPG& G )
{ return o << CAST( G, BPG ); }

texstream& operator<<( texstream& o, const NBBPG& G )
{ o.ostr() << CAST( G, BPG ); return o; }

astream& operator<<( astream& o, const NBBPG& G )
{ o << CAST( G, BPG ); return o; }

// -----------------------------------------------

bistream& operator>>( bistream& i, RBBPG& G )
{ return i >> CAST( G, BPG ); }

bostream& operator<<( bostream& o, const RBBPG& G )
{ return o << CAST( G, BPG ); }

ostream& operator<<( ostream& o, const RBBPG& G )
{ return o << CAST( G, BPG ); }

texstream& operator<<( texstream& o, const RBBPG& G )
{ o.ostr() << CAST( G, BPG ); return o; }

// -----------------------------------------------

bistream& operator>>( bistream& i, CBBPG& G )
{ return i >> CAST( G, BPG ) >> G.bPos; }

bostream& operator<<( bostream& o, const CBBPG& G )
{ return o << CAST( G, BPG ) << G.bPos; }

ostream& operator<<( ostream& o, const CBBPG& G )
{ return o << CAST( G, BPG ) << " bPos: " << G.bPos; }

texstream& operator<<( texstream& o, const CBBPG& G )
{ 
   NBBPG G1( G, 0, G.center() );
   NBBPG G2( G, G.center(), G.length() );
   o << G1 << G2;
   return o;
}

