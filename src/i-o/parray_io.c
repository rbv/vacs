
// ------------------------------------------
// ----------------- parray.c ---------------
// ------------------------------------------

#if 0

#include "general/stream.h"
#include "array/parray.h"

#ifndef NOBuffer
//
// --- load from buffer ---
//
template< class T >
bistream& operator>>( bistream& s, PArray<T> A )
{
   int z;
   s >> z;
   A.resize( z );
   for ( int i = 0; i < z; i++ )
   {
      A[i] = new T;
      s >> *A[i];
   }
   return s;
}

// Non-members

template< class T >
bostream& operator<<( bostream& s, const PArray<T>& A )
{
   // save our size, for reloading
   //
   s << A.size();

   for ( int i = 0; i < size(); i++ )
      s << A[i];
}

template< class T >
ostream& operator<<( ostream& o, const PArray<T>& A )
{
   o << '[';
   if ( size() > 0 )
       o << A[0];

   for ( int i = 1; i < size(); i++ )
      o << ',' << A[i];
   o << ']';

   return o;
}

template< class T >
ostream& textOut( ostream& o, const PArray<T>& A, const char* separator )
{
   o << '[';
   if ( size() > 0 )
       o << A[0];

   for ( int i = 1; i < size(); i++ )
      o << separator << A[i];
   o << ']';

   return o;
}

//----------------------------------------------------------------

#endif
#endif

