

#include "general/stream.h"
#include "search/searchnode.h"

ostream& operator<<( ostream& o, const ProofRef& ref )
{ 
   o 
   << "st: " << ref.statusAsString()
   << ", pf: " << ref._state._minimalProof;  // get the data from the union...

   return o;
}

ostream& operator<<( ostream& o, const SearchNode& sn )
{ 
   o 
   << "op:" << sn.op()
   << ", de:" << sn.depth()
   << ", pn: " << sn.parent()
   << ", st: " << sn.statusAsString()
   << ", pf: " << sn._state._minimalProof  // get the data from the union...
   << ", gr: " << sn.growStatusAsString()
   << ", mbr: " << sn.membershipAsString();

   return o;
}

