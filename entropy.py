#!/usr/bin/env python

import numpy as np

def H_of_binary(p):
    return -np.log2(p) * p + -np.log2(1-p) * (1-p)


# Bits to encode adjacency matrix


def pr(vertices, tparse_len):
    edges = tparse_len - vertices

    bits = vertices * (vertices - 1) / 2.
    onbits = edges
    if onbits >= bits:
        return

    print "tparse length =", tparse_len, vertices, "vertices", edges, "edges", H_of_binary(onbits/bits) * bits / 8., "bytes", "adj M bits =", bits, "=", bits/8., "bytes"


def encoding_len(ones, zeroes, p):
    return -np.log2(p) * ones + -np.log2(1-p) * zeroes


def fixed_ratio2(vertices, edges, p):
    fixed_ratio(vertices, vertices + edges, p)

def fixed_ratio(vertices, tparse_len, p):
    edges = tparse_len - vertices

    bits = vertices * (vertices - 1) / 2.
    onbits = edges
    if onbits >= bits:
        return

    print "tparse length =", tparse_len, vertices, "vertices", edges, "edges", encoding_len(onbits, bits - onbits, p) / 8, "bytes", "adj M bits =", bits, "=", bits/8., "bytes"



print 256/8.

for i in range(8, 21):
    pr(i, 45)



for p in np.linspace(0.05, 0.95, 10):
    print "-------, p =", p
    for i in range(8, 21):
        fixed_ratio(i, 45, p)


for p in np.linspace(0.05, 0.95, 16):
    print "-------, p =", p
    # fixed_ratio2(16, 29, p)
    # fixed_ratio2(15, 30, p)
    fixed_ratio2(17, 32, p)
    fixed_ratio2(16, 33, p)

# Not including initial operators
t_length = 39
t_length += 4+1 # pathwidth
vertices = round(t_length / 3.5)
edges = t_length - vertices
adj_matrix_bits = vertices * (vertices - 1) / 2.
onbits = edges
p = onbits / adj_matrix_bits
print "p = ", p, "v", vertices, edges

# // Assume average of 2 edges per vertex
# // (seems to be true for graphs in the torus4 search tree)
# // For e.g. 15 vertices, 30 edges, 
