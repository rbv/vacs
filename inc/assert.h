
// -----------------------------------------
// --------------- assert_h ----------------
// -----------------------------------------
 
/*tex
 
\file{assert.h}
\path{inc}
\title{Header for assertion control}
\classes{}
\makeheader
 
xet*/

#ifndef assert_h
#define assert_h

// We'll do assertions our way to cut down on object size
// and compile time.

/*

 assert  - assert only if NDEBUG is not defined
          - to be renamed assert when code recompiles
 Assert   - eval expression, but don't check if NDEBUG is defined
 aassert  - assert, regardless of NDEBUG

 The same, but print errno if error occurs:
 asserten
 Asserten
 aasserten

 */

// --- Internal functions, to cut down link size ---

extern "C"
{
   int assertFail( const char*, const char*, int );
}

extern "C"
{
   int assertenFail( const char*, const char*, int );
}

// --------------------------------------------------

#ifdef NDEBUG

//#define assert(e) ((void)0)
//#define asserten(e) ((void)0)
#define assert(e)  ((void)0)
#define asserten(e) ((void)0)
#define Assert(e) (e)
#define Asserten(e) (e)

#else

#define assert(e) (void)( (e) || (assertFail(#e,__FILE__,__LINE__)) )
#define asserten(e) (void)( (e) || (assertenFail(#e,__FILE__,__LINE__)) )
#define Assert(e) (void)( (e) || (assertFail(#e,__FILE__,__LINE__)) )
#define Asserten(e) (void)( (e) || (assertenFail(#e,__FILE__,__LINE__)) )

#endif

#define aassert(e) (void)( (e) || (assertFail(#e,__FILE__,__LINE__)) )
#define aasserten(e) (void)( (e) || (assertenFail(#e,__FILE__,__LINE__)) )

void assertFailMesg( const char* );

#endif
