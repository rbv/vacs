
// -------------------------------------------
// -------------- testset.h -------------------
// -------------------------------------------

/*tex
 
\file{testset.h}
\path{inc/automata}
\title{Header for test set class}
\classes{TestSet, PassSet}
\makeheader
 
xet*/

#ifndef _testset_h
#define _testset_h

//-------------------------------

class PassSet : public BitVector
{
   iodeclarations( PassSet );

public:

};


//-------------------------------


class TestSet
{
   iodeclarations( TestSet );

public:

   void setMode();

   void computePassSet( const RBBPG&, PassSet& ) const;

   void addTest( const RBBPG&, bool permute );

   int numberOfTests() const;

   load( istream& );

};

#endif
