
// -------------------------------------------
// ------------ lautomata.h -------------------
// -------------------------------------------

/*tex
 
\file{lautomata.h}
\path{inc/automata}
\title{Header for linear automata class}
\classes{LAutomata}
\makeheader
 
xet*/

#ifndef _lautomata_h
#define _lautomata_h


class LAutomata
{

   iodeclarations( LAutomata )

public:

   typedef int State;

   enum GrowOrder { DepthFirst, BreadthFirst };

   enum GrowMethod { Full, CanonicOnly };

   enum RepMethod { LexLeast, FirstFound, LatestFound };

   static const State noState;  // entry representing transition not computed

private:

   // indexed by state and operator
   //
   Table< State > _stateTable;

   Family* _family;

   void constructState( State );

   void dfsConstruct();

   

public:

   // default ctor
   //
   LAutomata();

   //--------------------------------------

   // must call this before building
   //
   void setFamily( const Family& );

   // build using a complete test set
   //
   virtual void construct( const TestSet&, GrowOrder, GrowMethod, RepMethod );

   // build using a tight congruence
   //
   virtual void construct( const Congruence&, GrowOrder, GrowMethod, RepMethod);

   //--------------------------------------

   // return number of states
   //
   int states() const;

   bool isState( State ) const;

   State startState() const;

   State nextState( State s, Operator op )  // inline, for speed
   { return _stateTable(s,op.enum()); }

};

#endif
