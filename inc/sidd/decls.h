
#ifndef _SIDDS_GML
#define _SIDDS_GML

#include <stdlib.h>

#define S_MEMORY

/* 
 * this is the declaration file decls.h for testing for the gang of 4 and K4 
 */

struct adj_list {

    adj_list() : next(0) {}

#ifndef S_MEMORY
    ~adj_list() 
    {
      if (next) delete next;
    } 
#endif
                  int vertex;
                  int weight;
                  struct adj_list *next;
       };

struct vertex_record {
    
    vertex_record() : neighbours(0) {}

#ifndef S_MEMORY
    ~vertex_record()
    {
      if (neighbours) delete neighbours;
    }
#endif
                  int label;
                  int degree;
                  int weight;
                  struct adj_list* neighbours;
       };

struct graph {

    graph() : vertex_table(0), articulation(0) {}

#ifndef S_MEMORY
    ~graph() 
    {
      if (vertex_table) free((char*) vertex_table);
      if (articulation) delete articulation;
    }
#endif
               int size;
               struct vertex_record *vertex_table;
               struct adj_list *articulation;
       };

struct graph_list {

    graph_list() : component(0), next(0) {}

#ifndef S_MEMORY
    ~graph_list()
    {
      if (component) delete component;
      if (next) delete next;
    }
#endif
    
                   int label;
                   struct graph *component;
                   struct graph_list *next;
       };

struct edge_pair {

    edge_pair() : next(0) {}

#ifndef S_MEMORY
    ~edge_pair()
    {
      if (next) delete next;
    }
#endif

                   int v1;
                   int v2;
                   struct edge_pair *next;
       };

struct composite {

    composite() : list(0), stack(0) {}

#ifndef S_MEMORY
    ~composite() 
    {
      if (list) delete list;
      if (stack) delete stack;
    }
#endif

                   struct adj_list *list;
                   struct edge_pair *stack;
       };

#ifdef S_MEMORY
#include <memory.h>
#include "assert.h"

class someMemory
{
  static   int _size;
  static   int _used;
  static   char* _memory;

  public:

    static void create(int size) 
    {
      _size = size*1024;
      _used = 0;
      assert(size>0);
      //assert(_memory==0);
      _memory = new char[_size];
    }

   static void destroy()
   {
     _size=0;
     delete [] _memory;
   }

   static void* getMemory(int bytes)
   {
    aassert(bytes>0 && bytes+_used < _size);

    void *ptr = _memory+_used;
    _used += bytes;

    return ptr;
   }

   static void* getZeroMemory(int bytes)
   {
    return memset(getMemory(bytes), 0, bytes);
   }
};
#endif

/*
 * external functions
 */
graph *read_graph();
graph *copy_graph( graph *G );
graph_list *find_connected_components( graph *G );
graph *find_articulation(graph *G, graph_list *connected);
int shrink_graph( graph *H );
int find_621( graph *G, graph *component );
adj_list *get_edge_ptr( graph *G, int v1, int v2);
graph *create_graph(int size);
graph *check_vertex( graph *G, int v, int *found);
graph *add_edge(graph *G, int v1, int v2, int wt);
graph *delete_edge(graph *G, int v1, int v2);
graph *check_vertex_621( graph *G, int v );

#endif
