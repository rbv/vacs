
// ------------------------------------------
// ---------------- table.h -----------------
// ------------------------------------------

/*tex
 
\file{table.h}
\path{inc/array}
\title{Header for table class}
\classes{Table}
\makeheader
 
xet*/


#ifndef _table_h
#define _table_h

#include "vacs/object.h"
#include "stdtypes.h"


template< class T >
class Table 
{

   templated_iodeclarations( Table )

private:

	// table dimension
	//
	int _rows;
	int _cols;

	// pointer to array of row pointers
	//
	Array<T*> _s;

        void _allocate( int rows, int cols );
        //
        void _deallocate()
         {
           if (_dim) 
             {
	       delete [] _s[0];
	       delete [] _s;
             }
            _dim = 0;
         }

public:
	Table();

	Table( const Table<T>& );

	Table( int rows, int cols );

	~Table();

	T at( int r, int c ) const
	   { assert( r>=0 && c>=0 && r<_dim && c<_dim );
	     return _s[r][c];
	   }

	void put( int r, int c, T value )
	   { assert( r>=0 && c>=0 && r<_dim && c<_dim );
	     _s[r][c] = value;
	   }

	T& operator()( int r, int c ) const
	   { assert( r>=0 && c>=0 && r<_dim && c<_dim );
	     return _s[r][c];
	   }

	int rows() const { return _rows; }
	int cols() const { return _cols; }

        // currently, can only be called if dflt ctor has created empty
        //
        void redimension( int rows, int cols );

        void addRows( int numNewRows );

        void addRows( int numNewRows, int overAllocateNumRows );

	// fill the matrix with the passed value
	//
	void fill( const T& value );

	void fillRow( int row, const T& value );

	void fillCol( int col, const T& value );
	
	void operator=( const Table<T> & );

        bool operator==( const Table<T> & ) const;

};


#endif
