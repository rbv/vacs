
// ------------------------------------------
// ------------ psortarray.h ----------------
// ------------------------------------------

/*tex
 
\file{psortarray.h}
\path{inc/array}
\title{Header for sortable array class}
\classes{PSortableArray}
\makeheader
 
xet*/


#ifndef _psortarray_h
#define _psortarray_h

#include "array/sortarray.h"
#include "array/parray.h"

// for qsort()
//
#include <stdlib.h>

// Class PSortableArray - see SortableArray

template <class T> class PSortableArray;
template <class T> ostream& operator<<( ostream& o, const PSortableArray<T>& A );

template <class T>
 
class PSortableArray : public PArray< T >
{

public:

        using Array<T>::resize;
        using Array<T>::resizeNoCopy;
        using Array<T>::shrink;
        using Array<T>::size;
        using Array<T>::get;
        using Array<T>::put;
        using Array<T>::baseAddr;
        using Array<T>::offsetAddr;
 
        // constructor : create empty
        //
        PSortableArray() : PArray<T>() {}
 
        // constructor : create with given size
        //
        PSortableArray( int size ) : PArray<T>( size ) {}
 
        // copy constructor
        //
        PSortableArray( const PSortableArray<T>& A ) : PArray<T>(A) {}
 
        // copy constructor, with extra entries
        //
        PSortableArray( const PSortableArray<T>& A, int extra ) 
	   : PArray<T>(A,extra) {}
 
        // assignment
        //
        PSortableArray<T>& operator=( const PSortableArray<T>& A )
           { PArray<T>::operator=(A); return *this; }
 
        // resize
        //
        void resize( int newSize )
           { PArray<T>::resize( newSize ); }

        // resize with fill
        //
        void resize( int newSize, T* fill )
           { PArray<T>::resize( newSize, fill ); }
 
        // destructor
        //
        ~PSortableArray() {}
 
        // subscripting
        //
        T*& operator[]( int i ) const
           { return PArray<T>::operator[](i); }

        // ------- Sorting --------
        //
        void sortUp();

        void sortDown();

        void sort() { sortUp(); }

        // sort only part of the array
        //
        void sortUp( int fromPos, int toPos );

        void sortDown( int fromPos, int toPos );

        // ------- Comparing --------
        //
        bool operator<( const PSortableArray<T>& ) const;

        bool operator>=( const PSortableArray<T>& a ) const
           { return ! (*this < a); }

        bool operator>( const PSortableArray<T>& ) const;

        bool operator<=( const PSortableArray<T>& a ) const
           { return ! (*this > a); }


        friend ostream& operator<< <>( ostream& o, const PSortableArray<T>& A );
	   //{ return o << CAST( A, PArray<T> ); }

};

#ifdef INLINE_ARRAY
#include "psortarray.c"
#endif

#endif
