
// ------------------------------------------
// ----------------- array.h ----------------
// ------------------------------------------

/*tex

\file{growarray.h}
\path{inc/array}
\title{Header for array classes}
\classes{GrowArray}
\makeheader


xet*/

#ifndef _growarray_h
#define _growarray_h

#include "array/array.h"



template< class T >

class GrowArray : private Array<T>
{

public:

	// constructor : create empty
        //
	GrowArray() : Array<T>() {}

	// constructor : create with given size
        //
	GrowArray( int size ) : Array<T>( size ) {}

	// copy constructor
        //
	GrowArray( const GrowArray<T>& A ) : Array<T>(A) {}

	// subscripting
        //
	T& operator[]( int i ) const
	{ 
	   assert( i >= 0 );
	   if ( i >= size() )
	      ( (Array<T>*) this ) -> resize( i+1 ); // cast away the const
	   return Array<T>::operator[](i);
	}

	// Promotions from Array
	//
	Array<T>::size;
	Array<T>::length;

};

#ifdef INLINE_ARRAY
#include "growarray.c"
#endif

#endif
