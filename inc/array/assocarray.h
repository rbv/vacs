
// -----------------------------------------
// ------------ assocarray.h ---------------
// -----------------------------------------

/*tex
 
\file{assocarray.h}
\path{inc/array}
\title{Header for associative array class}
\classes{DoubleArray, PDoubleArray, AssocArray, AssocArrayIter}
\makeheader
 
xet*/

#ifndef _assocarray_h
#define _assocarray_h

#include "array/array.h"

template< class T1, class T2 >
 
class DoubleArray 
{

   // ??? Not defined anywhere
   //binaryio( DoubleArray<T1,T2> );

private:
 
   Array<T1> _array1;
   Array<T2> _array2;
 
   // Assignment - disabled
   //
   void operator=( DoubleArray<T1,T2>& )
      { aassert(false); }
 
   // copy ctor - disabled
   //
   DoubleArray( DoubleArray<T1,T2>& )
      { aassert(false); }
 
public:
 
   // Create empty
   //
   DoubleArray() 
      : _array1(), _array2() {}
 
   // Create with given size
   //
   DoubleArray( int size )
      : _array1( size ), _array2( size ) {}
 
   // resize
   //
   void resize( int newSize )
      { _array1.resize( newSize ); _array2.resize( newSize ); }

   // return size
   //
   int size() const
      { return _array1.size(); }
 
   T1& get1( int index )
      { return _array1[index]; }
 
   T2& get2( int index )
      { return _array2[index]; }
 
   void put1( int index, T1& t )
      { _array1[index] = t; }
 
   void put2( int index, T2& t )
      { _array2[index] = t; }
 
   friend ostream& operator<<( ostream&, DoubleArray<T1,T2> );
};
 
//--------------------------------------------------------------

template< class T1, class T2 >
 
class PDoubleArray 
{

private:
 
   Array<T1> _array1;
   PArray<T2> _array2;
 
   // Assignment - disabled
   //
   void operator=( PDoubleArray<T1,T2>& )
      { aassert(false); }
 
   // copy ctor - disabled
   //
   PDoubleArray( PDoubleArray<T1,T2>& )
      { aassert(false); }
 
public:
 
   // Create empty
   //
   PDoubleArray() 
      : _array1(), _array2() {}
 
   // Create with given size
   //
   PDoubleArray( int size )
      : _array1( size ), _array2( size ) {}

   // resize
   //
   void resize( int newSize )
      { _array1.resize( newSize ); _array2.resize( newSize ); }

   // return size
   //
   int size() const
      { return _array1.size(); }
 
   T1& get1( int index )
      { return _array1[index]; }
 
   T2& get2( int index )
      { return _array2[index]; }
 
   void put1( int index, T1& t )
      { _array1[index] = t; }
 
   void put2( int index, T2& t )
      { _array2[index] = t; }
 
 
   friend ostream& operator<<( ostream&, PDoubleArray<T1,T2> );

};

//--------------------------------------------------------------
 
template< class Key, class Type >
 
class AssocArray : public DoubleArray< Key, Type >
{
private:

   // friend class AssocArrayIter; // cfront bug

   // disable copy ctor
   //
   AssocArray( const AssocArray<Key,Type>& ) { aassert( false ); } 

   // disable assignment
   //
   void operator=( const AssocArray<Key,Type>& ) { aassert( false ); } 

public:  // public due to cfront friend bug

   // Return the size of the arrays.
   //
   int size() const
      { return this -> DoubleArray<Key,Type>::size(); }

public:
 
   // Create, using parameter as empty flag for type Key.
   // Parameter will be optional when cfront bug disappears.
   //
   AssocArray( Key emptyFlag );

   // Return the object at the specified index.
   // Error if key not found.
   //
   Type* operator[]( Key key );

   // Check if key is used.
   //
   bool hasKey( Key key );

   // Remove the element (do not delete the pointer)
   // Error if not found.
   //
   void remove( Key key );

   // Remove the element, and delete the pointer.
   // Error if not found.
   //
   void removeAndDelete( Key key );

};

//--------------------------------------------------------------

template< class Key, class Type >
 
class AssocArrayIter
{
private:

   int _pos;

   const AssocArray<Key,Type>& _coll;

   Type* step( int direction );

public:

   AssocArrayIter( const AssocArray<Key,Type>& coll )
      : _coll( coll )
      { _pos = -1; }

   Type* next() { return step( 1); }
   Type* prev() { return step(-1); }

   void start() { _pos = -1; }
   void end() { _pos = _coll.size(); }

};

#ifdef INLINE_ARRAY
#include "assocarray.c"
#endif

#endif
 
