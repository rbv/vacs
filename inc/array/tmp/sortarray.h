
// ------------------------------------------
// ------------- sortarray.h ----------------
// ------------------------------------------

/*tex
 
\file{sortarray.h}
\path{inc/array}
\title{Header for sortable array class}
\classes{SortableArray}
\makeheader
 
xet*/


#ifndef _sortarray_h
#define _sortarray_h

#include "array/array.h"

// -------------------------
// Classes:
//    SortableArray	- array that can be compared and sorted
// -------------------------

//--------------------------------------------------------------
//
// class SortableArray provides the ability to sort and compare arrays.
// The element class must provide:
//
//   bool operator< 				(sortUp)
//   bool operator>				(sortDown)
//
// and/or
//
//   int compare(const T *a, const T *b) 	(sort)
// 
// that returns {-1,0,1}

//---------------------------------------------------------------------

template< class T >
//
class SortableArray : public Array<T>
{

public:

	/* 
         * constructor : create with given size
         */
	SortableArray( int size ) : Array<T>( size ) {}

	// ctor - create empty
	//
	SortableArray() : Array<T>() {}

        // ~SortableArray() {}

        /*
         * from standard array/dyarray
         */
        SortableArray(const Array<T> &A) : Array<T>( A ) {}

	// Return largest element in array.
	//
	T max();

	// Return smallest element in array.
	//
	T min();


	// ------- Sorting --------
	//
	void sortUp();

	void sortDown();

	void sort();

	// sort only part of the array
	//
	void sortUp( int fromPos, int toPos );

	void sortDown( int fromPos, int toPos );

	// ------- Comparing --------
	//

        bool operator==( const SortableArray<T> &SA ) const
         {
	  return Array<T>::operator==( CAST( SA, Array<T> ) );
         }

	bool operator<( const SortableArray<T>& ) const;

	bool operator>=( const SortableArray<T>& a ) const
	   { return ! (*this < a); }

	bool operator>( const SortableArray<T>& ) const;

	bool operator<=( const SortableArray<T>& a ) const
	   { return ! (*this > a); }

};

//----------------------------------------------------------------------

//template< class T > inline int compare( const T*, const T* );
template< class T > inline int compareUp( const T* a, const T* b );
template< class T > inline int compareDown( const T* a, const T* b );

// Comparison fncts for built-in types
//
inline int compareUp( const int*    a, const int*    b ) { return *a - *b; }
inline int compareUp( const uint*   a, const uint*   b ) { return *a - *b; }
inline int compareUp( const char*   a, const char*   b ) { return *a - *b; }
#ifndef CRAY
inline int compareUp( const uchar*  a, const uchar*  b ) { return *a - *b; }
#endif
inline int compareUp( const short*  a, const short*  b ) { return *a - *b; }
inline int compareUp( const ushort* a, const ushort* b ) { return *a - *b; }
inline int compareUp( const long*   a, const long*   b ) { return *a - *b; }
inline int compareUp( const ulong*  a, const ulong*  b ) { return *a - *b; }
//
inline int compareDown( const int*    a, const int*    b ) { return *b - *a; }
inline int compareDown( const uint*   a, const uint*   b ) { return *b - *a; }
inline int compareDown( const char*   a, const char*   b ) { return *b - *a; }
#ifndef CRAY
inline int compareDown( const uchar*  a, const uchar*  b ) { return *b - *a; }
#endif
inline int compareDown( const short*  a, const short*  b ) { return *b - *a; }
inline int compareDown( const ushort* a, const ushort* b ) { return *b - *a; }
inline int compareDown( const long*   a, const long*   b ) { return *b - *a; }

template< class T >
inline int compare( const T*, const T* )
{
   aassert(false);
   return 1;
}

template< class T >
inline int compareUp( const T* a, const T* b )
{
   int ret;
   if ( *a > *b ) ret = 1;
   else if ( *a < *b ) ret = -1;
   else ret = 0;
   return ret;
}

template< class T >
inline int compareDown( const T* a, const T* b )
{
   return -compareUp( a, b );
}
#endif
