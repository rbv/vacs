
// ------------------------------------------
// ---------------- matrix.h ----------------
// ------------------------------------------

/*tex
 
\file{matrix.h}
\path{inc/array}
\title{Header for matrix class}
\classes{SquareMatrix,Matrix}
\makeheader
 
xet*/


#ifndef _matrix_h
#define _matrix_h

#include "stdtypes.h"

// Classes SquareMatrix and Matrix


template< class T >
class SquareMatrix 
{

   templated_iodeclarations( SquareMatrix )

private:

	// matrix dimension
	//
	int _dim;

	// pointer to array pf pointers
	//
	T** _s;

        void _allocate( int dimension );
        //
        void _deallocate( )
         {
           if (_dim) 
             {
	       delete [] _s[0];
	       delete [] _s;
             }
            _dim = 0;
         }

public:
	SquareMatrix()			// must then use setDim()
	   { _dim = 0; }

	SquareMatrix( const SquareMatrix<T>& );

	SquareMatrix( int dimension ) 
           {
             _allocate(dimension);
           }

	~SquareMatrix()
       	   {
            _deallocate();
	   }


	T at( int r, int c ) const
	   { assert( r>=0 && c>=0 && r<_dim && c<_dim );
	     return _s[r][c];
	   }

	void put( int r, int c, T value )
	   { assert( r>=0 && c>=0 && r<_dim && c<_dim );
	     _s[r][c] = value;
	   }

	T& operator()( int r, int c ) const
	   { assert( r>=0 && c>=0 && r<_dim && c<_dim );
	     return _s[r][c];
	   }

	int dim() const { return _dim; }

        void setDim(int dim) 		// currenly destroys old info
         { 
            _deallocate();
            _allocate(dim);
         }

	// fill the matrix with the passed value
	//
	void fill( T value );

	void fillRow( int row, T value );

	void fillCol( int col, T value );
	
	void fillDiag( T value );


	void operator=( const SquareMatrix<T> &m );

        bool operator==( const SquareMatrix<T> &m ) const;


	friend ostream& operator<<( ostream&, const SquareMatrix<T>& );

};

//----------------------------------------------------------------------

template< class T >
//
class Matrix 
{

   templated_iodeclarations( Matrix )

private:

	// matrix dimensions
	//
	int _dim1, _dim2;

	// pointer to array pf pointers
	//
	T** _s;

	Matrix( const Matrix<T>& )
	   { aassert( false ); }

	void operator=( const Matrix<T>& )
	   { aassert( false ); }

        void _allocate( int dim1, int dim2 );
        //
        void _deallocate( );

public:

	// disallow for now
	//
	Matrix();

        void setDim( int dim1, int dim2 );

	Matrix( int dim1, int dim2 );

	~Matrix();

	T at( int r, int c ) const
	   { assert( r>=0 && c>=0 && r<_dim1 && c<_dim2 );
	     return _s[r][c];
	   }

	void put( int r, int c, T value )
	   { assert( r>=0 && c>=0 && r<_dim1 && c<_dim2 );
	     _s[r][c] = value;
	   }

	T& operator()( int r, int c ) const
	   { assert( r>=0 && c>=0 && r<_dim1 && c<_dim2 );
	     return _s[r][c];
	   }

	int dim1() const { return _dim1; }
	//
	int dim2() const { return _dim2; }

	// fill the matrix with the passed value
	//
	void fill( T value );

	void fillRow( int row, T value );

	void fillCol( int col, T value );
	
	friend ostream& operator<<( ostream&, const Matrix<T>& );

};

#endif
