
// ------------------------------------------
// ----------------- array.h ----------------
// ------------------------------------------

/*tex

\file{array.h}
\path{inc/array}
\title{Header for array classes}
\classes{Array}
\makeheader

\begin{desc}
These classes provide support for arrays of objects, using
the template mechanism.
Class Array is for plain objects (usually small).
Class PArray is for an array of pointers to objects.
Class GrowArray provides an array that automatically grows when
an out-of-bounds subscript is supplied to operator[].
\end{desc}

// Array elements must have operator== defined.


xet*/

#ifndef _array_h
#define _array_h

// debugging
#include <iostream>

#include "vacs/object.h"
#include "stdtypes.h"
#include "general/op_eq.h"
#include "array/simple.h"

template< class T >
class Array 
{

   templated_iodeclarations( Array )

private:
	// current size 
        //
	int _size;

protected:

	// Copy length elements from <a> to self.
	// No error checking.
	//
	void copy( const T* a, int length );

	// the actual array
	// memory management in dyarray needs access
	//
	T* array;

	// For dyarray (and others who need to know about storage).
	//
	// Return the amount of storage allocated.
	//
	virtual int allocSize() const { return _size; }
	//
	virtual void allocate( int i );
	//
	// Reallocate to newSize, copying first keepNum elements to new.
	//
	virtual void reallocate( int newSize, int keepNum );
	//
	// Fiddle with the usable size of the array.
	// For dyarray.
	//
	void setSize( int size ) { _size = size; }

        // For marray, to set the internal pointer
        //
	void setBaseAddr( void* p ) { array = (T*) p; }

public:
	// constructor : create with 0 size
	//
	Array() { allocate( 0 ); }

	// constructor : create with given size
        //
	Array( int size );

	// copy constructor
        //
	Array( const Array<T>& );

	// copy constructor, with extra entries
        //
	Array( const Array<T>&, int size );

	// assignment
	// does not reallocate if size(rhs)=size(lhs)
        //
	Array<T>& operator=( const Array<T>& );

	// resize the array and copy old elements to new.
	// does nothing if newSize == size().
	//
	void resize( int newSize );

	// resize the array, and do not copy old elements to new.
	// does nothing if newSize == size().
	//
	void resizeNoCopy( int newSize );

	// resize the array and copy old elements to new
	// does nothing if newSize == size()
	// If the array grows, the new area is filled with fill.
	//
	void resize( int newSize, const T& fill );

        /*
         * extend the array by "addSize" elements.
         */
        void extend( int addSize ) { resize( size()+addSize ); }
        //
        void extend( int addSize, const T& fill )
		{ resize( size()+addSize, fill ); }
        //
        void append( const T& item ) { extend( 1, item ); }
	//
	void shrink( int addSize ) { resize( size()-addSize ); }
        
        /*
         * Add and subtract other subarrays/sets
         */
        void append( const Array<T> &A );
        //
        void minus( const Array<T> &A );


	// Load from file.  Resize according to the stored size,
	// then load.  Does no free store operation if new size 
	// == old size.
	// This is a binary operation, not ascii.
	// It is not implemented in general, but is specialised for uchar.
	//
	void loadFromFile( istream& );

	// Same as above, but the length is not loaded from the file.
	//
	void loadFromFile( istream&, int length );

	// Save to file.  This is a binary operation, not ascii.
	// Length is written out initially.
	// It is not implemented in general, but is specialised for uchar.
	//
	void saveToFile( ostream& );

	// Same as above, but the length is not saved to the file.
	//
	void saveToFileNoLength( ostream& );

	// destructor
        //
	virtual ~Array();

	// fill the array
	//
        void fill(const T &fill);

	// subscripting
        //
	T& operator[]( int i ) const
        {
	   assert( i>=0 && i<_size );
           return array[i];
        }

   // non-lvalue access
   //
   T get( int i) const
   {
      assert( i>=0 && i<_size );
      return array[i];
   }
   //
   void put( int i, const T val )
   {
      assert( i>=0 && i<_size );
      array[i] = (T) val;
   }

	int size() const { return _size; }

	int length() const { return _size; }

	// Array comparison functions.
	//
	bool operator==( const Array<T>& ) const;

	bool operator!=( const Array<T>& a ) const
	   { return ! (*this == a); }

	/*
	 * Return index of an prospective element of the array 
	 * (returns _size if the element isn't found.)
	 */
	int index( const T& ) const;

        /* 
         * for PermEnum class
         */ 
        void swap( int i, int j )
	   { 
             assert( i>=0 && i<_size && j>=0 && j<_size ); 
             T val = array[i];
             array[i] = array[j];
             array[j] = val;
           }
        //
        void reverse( int i, int j );
        void reverse( int j ) 		{ reverse( 0, j ); }
        void reverse( ) 		{ reverse( 0, _size-1 ); }
        //
        void rotate( int i, int j );
	//
        void rotateLeft( int num );
        void rotateRight( int num );
	//
        void shiftLeft( int index, int num );
        void shiftAndResizeLeft( int index, int num );
        void shiftAndResizeRight( int index, int num );

	// Direct internal access
	//
	void* baseAddr() const
	   { return &( array[ 0 ] ); }
	//
	void* offsetAddr( int pos ) const
	   { return &( array[ pos ] ); }
        //
        // copy the passed memory to the location array[index]
        //
        void binaryInsert( int index, char* buffer, int length );

        // Simple sorting (only implemented for char, short, int)
        //
        void simpleSortUp();

	// --- friends ---

	friend ostream& textOut( ostream&, const Array<T>&, 
				 const char* separator );

};

template< class T >
static ostream& operator<<( ostream& o, const Array<T>& A )
{
   //o << A.size() << ' ' << A.allocSize() << ' ';
   o << '[';
   if ( A.size() > 0 ) o << A[0];
   for ( int i = 1; i < A.size(); i++ ) o << ',' << A[i];
   o << ']';
   return o;
}


template< class T >
static astream& operator<<( astream& o, const Array<T>& A )
{
   o << putLeft << A.size();
   for ( int i = 0; i < A.size(); i++ )
      o << A[i] << ' ';
   return o << putRight;
}

#endif
