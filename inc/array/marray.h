
// ------------------------------------------
// ---------------- marray.h ----------------
// ------------------------------------------

/*tex

\file{marray.h}
\path{inc/array}
\title{Header for memory-array classes}
\classes{MArray}
\makeheader

\begin{desc}
This class provides support for Arrays on user-allocated memory.
\end{desc}

xet*/

#ifndef _marray_h
#define _marray_h

#include "array/array.h"

template< class T >
class MArray : public Array<T>
{

   templated_iodeclarations( MArray )

   using Array<T>::resize;
   using Array<T>::resizeNoCopy;
   using Array<T>::shrink;
   using Array<T>::size;
   using Array<T>::get;
   using Array<T>::put;
   using Array<T>::baseAddr;
   using Array<T>::offsetAddr;
   using Array<T>::setSize;
   using Array<T>::setBaseAddr;

protected:

   // These functions and resize(...) redefine the storage management.
   //
   virtual int allocSize() const { return Array<T>::allocSize(); }
   //
   virtual void allocate( int ) { aassert(false); }
   //
   virtual void reallocate( int , int ) { aassert(false); }

   MArray( const MArray<T>& ) { aassert(false); }
   MArray<T>& operator=( const MArray<T>& ) { aassert(false); return *this; }

public:

   MArray()
      : Array<T>()
      {}
   // ctor - assume mem points to objects in user memory
   //
   MArray( void* mem, int size )
      : Array<T>()
      { setSize(size); setBaseAddr(mem); }

   // ctor - assume mem points to size, then objects in user memory
   //
   MArray( void* mem )
      : Array<T>()
      { setSize(*((int*)mem)); setBaseAddr((char*)mem+sizeof(int)); }

   void set( void* mem, int size )
      { setSize(size); setBaseAddr(mem); }

   void set( void* mem )
      { setSize(*((int*)mem)); setBaseAddr((char*)mem+sizeof(int)); }

   ~MArray()
      { setBaseAddr(0); } // make sure Array doesn't deallocate

};

#ifdef INLINE_ARRAY
#include "marray.c"
#endif

#endif
