 
// ------------------------------------------
// ------------------ matrix.c --------------
// ------------------------------------------

/*tex
 
\file{matrix.c}
\path{src/template}
\title{Definitions for matrix classes}
\classes{SquareMatrix,Matrix}
\makeheader
 
xet*/

#ifndef matrix_c
#define matrix_c

#include "array/matrix.h"

// ------------------------------------------


// Class SquareMatrix definitions

template< class T >
void SquareMatrix<T>::_allocate( int dimension )
{
   
   _dim = dimension;

   if ( _dim == 0 ) { _s = 0; return; }

   assert( dimension > 0 );

   // allocate array of pointers
   //
   _s = new T*[ _dim ];

   // allocate the rest in one step
   //
   _s[0] = new T[ _dim*_dim ];

   for ( int i=1; i<_dim; i++ )
      _s[i] = _s[0] + i*_dim;

}

template< class T >
void SquareMatrix<T>::_deallocate()
{
  if (_dim) 
  {
    delete [] _s[0];
    delete [] _s;
  }
  _dim = 0;
}


template< class T >
SquareMatrix<T>::SquareMatrix( const SquareMatrix<T> &m ) 
{
   if (m._dim == 0) { _dim = 0; return; }
   else _allocate(m._dim);

   for ( int i=0; i<_dim; i++ ) 
    for ( int j=0; j<_dim; j++ ) 
      _s[i][j] = m._s[i][j];
}

template< class T >
void SquareMatrix<T>::fill( T value )
{
   for ( int i=0; i<_dim; i++ )
   {
      T* p = _s[i];
      for ( int j=0; j<_dim; j++ )
         p[j] = value;
   }
}

template< class T >
void SquareMatrix<T>::fillRow( int row, T value )
{
   for ( int i=0; i<_dim; i++ )
      put( row, i, value );
}

template< class T >
void SquareMatrix<T>::fillCol( int col, T value )
{
   for ( int i=0; i<_dim; i++ )
      put( i, col, value );
}

template< class T >
void SquareMatrix<T>::fillDiag( T value )
{
   for ( int i=0; i<_dim; i++ ) put( i, i, value );
}

template< class T >
void SquareMatrix<T>::operator=( const SquareMatrix<T> &m ) 
{
   if ( _dim != m._dim ) 
    {
     _deallocate();

     if ( m._dim == 0 ) return;
     else _allocate(m._dim);
    }

   for ( int i=0; i<_dim; i++ ) 
    for ( int j=0; j<_dim; j++ ) 
      _s[i][j] = m._s[i][j]; 
}

template< class T >
bool SquareMatrix<T>::operator==( const SquareMatrix<T> &m ) const
{
   if ( _dim != m._dim ) return false;

   for ( int i=0; i<_dim; i++ ) 
    for ( int j=0; j<_dim; j++ ) 
     if ( _s[i][j] != m._s[i][j] ) return false;

   return true;
}

/*
template< class T >
void SquareMatrix<T>::distanceMatrix()
{

   for (int k=0; k<_dim; k++)
      for (int i=0; i<_dim; i++)
         for (int j=0; j<_dim; j++)
         {
            T sum = at(i,k) + at(k,j);
            if (at(i,j) > sum) put( i , j, sum );
         }
}
*/


// --------- I/O ----------

template< class T >
ostream& operator<<( ostream& o, const SquareMatrix<T>& M )
{
   o << nl;

   for ( int i=0; i<M.dim(); i++ )
   {
      for ( int j=0; j<M.dim(); j++ )
      {
         o << M.at(i,j) << ' ';
      }
      o << nl;
   }

   return o;

}

// ------------------------------------------
// ------------------------------------------


// Class Matrix definitions

template< class T >
Matrix<T>::Matrix()
{
   _dim1 = 0;
}

template< class T >
Matrix<T>::Matrix( int dim1, int dim2 )
{
   _allocate( dim1, dim2 );
}

template< class T >
void Matrix<T>::_allocate( int dim1, int dim2 )
{
   assert( dim1 > 0 && dim2 > 0 );

   _dim1 = dim1;
   _dim2 = dim2;

   // allocate array of pointers
   //
   _s = new T*[ _dim1 ];

   // allocate the rest in one step
   //
   _s[0] = new T[ _dim1*_dim2 ];

   for ( int i=1; i<_dim1; i++ )
      _s[i] = _s[0] + i*_dim2;

}


template< class T >
void Matrix<T>::_deallocate()
{
   if ( _dim1 )
   {
      delete [] _s[0];
      delete [] _s;
   }
}

template< class T >
void Matrix<T>::setDim( int dim1, int dim2 )
{
   _deallocate();
   _allocate( dim1, dim2 );
}

template< class T >
Matrix<T>::~Matrix()
{
   _deallocate();
}


template< class T >
void Matrix<T>::fill( T value )
{
   for ( int i=0; i<_dim1; i++ )
   {
      T* p = _s[i];
      for ( int j=0; j<_dim2; j++ )
         p[j] = value;
   }
}

template< class T >
void Matrix<T>::fillRow( int row, T value )
{
   for ( int i=0; i<_dim2; i++ )
      put( row, i, value );
}

template< class T >
void Matrix<T>::fillCol( int col, T value )
{
   for ( int i=0; i<_dim1; i++ )
      put( i, col, value );
}

// --------- I/O ----------

template< class T >
ostream& operator<<( ostream& o, const Matrix<T>& M )
{
   o << nl;

   for ( int i=0; i<M.dim2(); i++ )
   {
      for ( int j=0; j<M.dim1(); j++ )
      {
         o << M.at(i,j) << ' ';
      }
      o << nl;
   }

   return o;

}

#endif

