
// ------------------------------------------
// ---------------- dyarray.c ---------------
// ------------------------------------------

/*tex
 
\file{dyarray.c}
\path{src/template}
\title{Definitions for dynamic array classes}
\classes{DyArray}
\makeheader
 
xet*/

#ifndef _dyarray_c
#define _dyarray_c

#include <iostream>
#include "array/dyarray.h"

#define Inline

/*
 * Maximum amount to over allocate by. (to do later)
 */
//#define _DyArrayGrowMax_ 16


// ------------------------------------------

// Allocate the array and set the allocated size.
// This is a virtual function.
//
template< class T >
Inline void DyArray<T>::allocate( int size )
{
   assert( size >= 0 );

   if ( size != 0 )
      array = new T[ size ]();
   else
      array = 0;

   //cerr << "da===" << size << ' ' << (void*) array << ' ' << (void*) this << nl;

   _allocSize = size;

   setSize( size );
}


// Allocate the array and set the allocated size.
// Copy n items from the old array to the new.
// Delete the old array.
// This is a virtual function.
// Setting allocSize means nothing for Array (always==_size).
//
template< class T >
Inline void DyArray<T>::reallocate( int newSize, int keepNum )
{
   //cerr << "dyarray reallocate " << array << nl;

   assert( newSize >= 0 );
   assert( keepNum <= size() ); // can't keep more than it has

   setSize( newSize );

   if ( allocSize() >= newSize ) return;
   //cerr << "   realloc needed" << nl;

   T* oldArray = array;

   // we know newSize > 0, because it's > allocSize
   //
   array = new T[ newSize ]();

   //cerr << "dr===" << newSize << ' ' <<(void*)array<< ' ' << (void*) this<<nl;

   int copyMax = min( keepNum, newSize );
   copy( oldArray, copyMax );
 
   _allocSize = newSize;

   if ( oldArray ) delete [] oldArray;
}


template< class T >
Inline void DyArray<T>::resizeAndOverAllocate( int newSize, int extraSize )
{
   assert( newSize >= 0 );
   assert( extraSize >= 0 );

   // check if realloc is needed
   //
   if ( allocSize() < newSize )
   {
      //reallocate( newSize + extraSize, min( size(), newSize ) );
      //
      // since newSize >= allocSize() >= size()
      //
      reallocate( newSize + extraSize, size() ); 
   }

   setSize( newSize );

}

template< class T >
Inline void DyArray<T>::resizeNoCopyAndOverAllocate( int newSize, int extraSize )
{
   assert( newSize >= 0 );
   assert( extraSize >= 0 );

   // check if realloc is needed
   //
   if ( allocSize() < newSize )
   {
      reallocate( newSize + extraSize, 0 );
   }

   setSize( newSize );

}


// Constructors, copying  -----------------------------------------

template< class T >
Inline DyArray<T>::DyArray( int size, int moreSpace )
{
   assert( size >= 0 );
   allocate( size + moreSpace );
   setSize( size );
}


template< class T >
Inline DyArray<T>::DyArray( const Array<T>& a )
{
   allocate( a.size() );
   copy( (T*) a.baseAddr(), a.size() );
}


template< class T >
Inline DyArray<T>::DyArray( const DyArray<T>& a )
{
   allocate( a.size() );
   copy( (T*) a.baseAddr(), a.size() );
}


template< class T >
Inline DyArray<T>::DyArray( const Array<T>& a, int extra )
{
   assert( a.size() + extra >= 0 );

   allocate( a.size() + extra );
   copy( (T*) a.baseAddr(), a.size() );
}


template< class T >
Inline DyArray<T>& DyArray<T>::operator=( const Array<T>& a )
   { Array<T>::operator=( a ); _allocSize = size(); return *this; }


template< class T >
Inline DyArray<T>& DyArray<T>::operator=( const DyArray<T>& a )
   { Array<T>::operator=( a ); _allocSize = size(); return *this; }

#endif

