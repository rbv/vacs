 
// ------------------------------------------
// ---------------- sortarray.c -------------
// ------------------------------------------

/*tex
 
\file{sortarray.c}
\path{src/template}
\title{Definitions for sortable array class}
\classes{SortableArray}
\makeheader
 
xet*/

#ifndef _sortarray_c
#define _sortarray_c

#include <utility>
using std::min;
using std::max;

#include <stdlib.h>

#include "array/sortarray.h"

// Class SortableArray definitions

//----------------------------------------------------------------


template< class T >
T SortableArray<T>::max()
{
   T m;
   m = get(0);
   for ( int i = 1; i < size(); i++ )
      m = ::max( m, get(i) );
   return m;
}

template< class T >
T SortableArray<T>::min()
{
   T m;
   m = get(0);
   for ( int i = 1; i < size(); i++ )
      m = ::min( m, get(i) );
   return m;
}

//----------------------------------------------------------------

#if 0
// Comparison fncts for built-in types
//
inline int compareUp( const int*    a, const int*    b ) { return *a - *b; }
inline int compareUp( const uint*   a, const uint*   b ) { return *a - *b; }
inline int compareUp( const char*   a, const char*   b ) { return *a - *b; }
inline int compareUp( const uchar*  a, const uchar*  b ) { return *a - *b; }
inline int compareUp( const short*  a, const short*  b ) { return *a - *b; }
inline int compareUp( const ushort* a, const ushort* b ) { return *a - *b; }
inline int compareUp( const long*   a, const long*   b ) { return *a - *b; }
inline int compareUp( const ulong*  a, const ulong*  b ) { return *a - *b; }
//
inline int compareDown( const int*    a, const int*    b ) { return *b - *a; }
inline int compareDown( const uint*   a, const uint*   b ) { return *b - *a; }
inline int compareDown( const char*   a, const char*   b ) { return *b - *a; }
inline int compareDown( const uchar*  a, const uchar*  b ) { return *b - *a; }
inline int compareDown( const short*  a, const short*  b ) { return *b - *a; }
inline int compareDown( const ushort* a, const ushort* b ) { return *b - *a; }
inline int compareDown( const long*   a, const long*   b ) { return *b - *a; }
inline int compareDown( const ulong*  a, const ulong*  b ) { return *b - *a; }

template< class T >
int compare( const T*, const T* )
{
   aassert(false);
   return 1;
}


// compare fncts for non-built-in types
//
template< class T >
//inline 
int compareUp( const T* a, const T* b )
{
//cerr << "compareUp of " << *a << " and " << *b << nl;
   int ret;
   if ( *a > *b ) ret = 1;
   else if ( *a < *b ) ret = -1;
   else ret = 0;
//cerr << "-- returns " << ret << nl;
   return ret;
}
//
//
template< class T >
//inline 
int compareDown( const T* a, const T* b )
{
   return -compareUp( a, b );
}
#endif

template< class T >
void SortableArray<T>::sortUp()
{
   int (*comp)( const T*, const T* ) = compareUp;
   qsort( baseAddr(), size(), sizeof(T), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void SortableArray<T>::sortDown()
{
   int (*comp)( const T*, const T* ) = compareDown;
   qsort( baseAddr(), size(), sizeof(T), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void SortableArray<T>::sort()
{
   //sortUp();
   int (*comp)( const T*, const T* ) = compare;
   qsort( baseAddr(), size(), sizeof(T), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void SortableArray<T>::sortUp( int fromPos, int toPos )
{
   assert( fromPos >= 0 && toPos < size() && fromPos <= toPos );

   int (*comp)( const T*, const T* ) = compareUp;
   qsort( offsetAddr(fromPos), 
          toPos-fromPos+1,
          sizeof(T), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void SortableArray<T>::sortDown( int fromPos, int toPos )
{
   assert( fromPos >= 0 && toPos < size() && fromPos <= toPos );

   int (*comp)( const T*, const T* ) = compareDown;
   qsort( offsetAddr(fromPos), 
          toPos-fromPos+1,
          sizeof(T), 
          (int (*)(const void*,const void*)) comp );
}

//---------------------------------------------------

template< class T >
bool SortableArray<T>::operator<( const SortableArray<T>& a ) const
{
   for ( int i = 0; i < ::min(size(),a.size()); i++ )
      if (  (*this)[i] < a[i] ) return true;
      else if (  (*this)[i] > a[i] ) return false;

   if ( size() < a.size() ) return true;
   else                     return false; // equal or >

/* old code
   for ( int i = 0; i < size(); i++ )
      if ( ! ( (*this)[i] < a[i] ) ) return false;
   return true;
*/
}

template< class T >
bool SortableArray<T>::operator>( const SortableArray<T>& a ) const
{
   for ( int i = 0; i < ::min(size(),a.size()); i++ )
      if (  (*this)[i] > a[i] ) return true;
      else if (  (*this)[i] < a[i] ) return false;

   if ( size() > a.size() ) return true;
   else                     return false; // equal or <

/* old code
   for ( int i = 0; i < size(); i++ )
      if ( ! ( (*this)[i] > a[i] ) ) return false;
   return true;
*/
}


#endif

