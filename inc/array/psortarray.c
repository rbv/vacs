 
// ------------------------------------------
// --------------- psortarray.c -------------
// ------------------------------------------

/*tex
 
\file{psortarray.c}
\path{src/template}
\title{Definitions for sortable array class}
\classes{SortableArray}
\makeheader
 
xet*/

#ifndef _psortarray_c
#define _psortarray_c

#include "array/psortarray.h"
#include <iostream>


// compare fncts for non-built-in types
//
template< class T >
inline int compareUPointer( const T** a, const T** b )
{
   bool ret;
   if ( **a > **b ) ret = 1;
   else if ( **a < **b ) ret = -1;
   else ret = 0;
   return ret;
}
//
//
template< class T >
inline int compareDPointer( const T** a, const T** b )
{
   return -compareUPointer( a, b );
}

template< class T >
void PSortableArray<T>::sortUp()
{
   int (*comp)( const T**, const T** ) = compareUPointer;
   qsort( baseAddr(), size(), sizeof(T*), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void PSortableArray<T>::sortDown()
{
   int (*comp)( const T**, const T** ) = compareDPointer;
   qsort( baseAddr(), size(), sizeof(T*), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void PSortableArray<T>::sortUp( int fromPos, int toPos )
{
   assert( fromPos >= 0 && toPos < size() && fromPos <= toPos );

   int (*comp)( const T**, const T** ) = compareUPointer;
   qsort( offsetAddr(fromPos), 
          toPos-fromPos+1,
          sizeof(T*), 
          (int (*)(const void*,const void*)) comp );
}

template< class T >
void PSortableArray<T>::sortDown( int fromPos, int toPos )
{
   assert( fromPos >= 0 && toPos < size() && fromPos <= toPos );

   int (*comp)( const T**, const T** ) = compareDPointer;
   qsort( offsetAddr(fromPos), 
          toPos-fromPos+1,
          sizeof(T*), 
          (int (*)(const void*,const void*)) comp );
}

//---------------------------------------------------

template< class T >
bool PSortableArray<T>::operator<( const PSortableArray<T>& a ) const
{
   for ( int i = 0; i < ::min(size(),*a.size()); i++ )
      if (  *(*this)[i] < *a[i] ) return true;
      else if (  *(*this)[i] > *a[i] ) return false;

   if ( size() > a.size() ) return true;
   else                     return false; // equal or >

/* old
   for ( int i = 0; i < size(); i++ )
      if ( ! ( *(*this)[i] < *a[i] ) ) return false;
   return true;
*/
}

template< class T >
bool PSortableArray<T>::operator>( const PSortableArray<T>& a ) const
{
   for ( int i = 0; i < ::min(size(),a.size()); i++ )
      if (  *(*this)[i] > *a[i] ) return true;
      else if (  *(*this)[i] > *a[i] ) return false;

   if ( size() > a.size() ) return true;
   else		     	    return false; // equal or <

/* old
   for ( int i = 0; i < size(); i++ )
      if ( ! ( *(*this)[i] > *a[i] ) ) return false;
   return true;
*/
}


#endif

