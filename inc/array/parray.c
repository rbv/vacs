
// ------------------------------------------
// ----------------- parray.c ---------------
// ------------------------------------------

/*tex
 
\file{parray.c}
\path{src/template}
\title{Definitions for array classes}
\classes{Array, PArray}
\makeheader
 
xet*/

#ifndef _parray_c
#define _parray_c

#include <iostream>
#include "array/parray.h"


#ifdef SUNPRO
#include "graph/indgrph.h"
#endif

template<class T>
void PArray<T>::deleteAll()
{
   for ( int i=0; i<size(); i++ )
   {
      if ( get(i) ) delete get(i);
      put( i, 0 );
   }
}

#endif

