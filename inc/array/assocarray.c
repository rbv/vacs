
// ------------------------------------------
// ---------------- assocarray.c ------------
// ------------------------------------------

/*tex
 
\file{assocarray.c}
\path{src/template}
\title{Definitions for associative array classes}
\classes{DoubleArray, AssocArray, AssocArrayIter}
\makeheader
 
xet*/

#ifndef _assocarray_c
#define _assocarray_c

#include "array/assocarray.h"

//----------------------------------------------------------------

template< class Key, class Type >
AssocArray<Key,Type>::AssocArray( Key emptyFlag )
{
}

template< class T1, class T2 >
bistream& operator>>( bistream& s, DoubleArray<T1,T2>& A  )
{ s >> A._array1 >> A._array2; }

template< class T1, class T2 >
bostream& operator<<( bostream& s, const DoubleArray<T1,T2>& A  )
{ s << A._array1 << A._array2; }

template< class T1, class T2 >
bistream& operator>>( bistream& s, PDoubleArray<T1,T2>& A  )
{ s >> A._array1 >> A._array2; }

template< class T1, class T2 >
bostream& operator<<( bostream& s, const PDoubleArray< T1, T2 >& A  )
{ s << A._array1 << A._array2; }

#endif

