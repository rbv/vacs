
// ------------------------------------------
// --------------- hugearray.h --------------
// ------------------------------------------

/*tex

\file{hugearray.h}
\path{inc/array}
\title{Header for huge array classes}
\classes{HugeArray}
\makeheader

xet*/

#include "array.h"

#ifndef _hugearray_h
#define _hugearray_h

class HugeArray 
{

   iodeclarations( HugeArray )

private:

   static const int ObjectsPerChunkLog2;

   int _size;
   int _objectSize;
   //int _chunkSize;
   int _lastChunkSize;

   Array<char*> _chunks;

   int chunk( int objNum ) const
      { return objNum >> ObjectsPerChunkLog2; }
   int pos( int objNum ) const
      { return objNum & ~( ulong(-1) << ObjectsPerChunkLog2 ); }

   int bytesPerChunk() const { return (1 << ObjectsPerChunkLog2) * _objectSize;}

   // copy constructor and assignment - not allowed
   //
   HugeArray( const HugeArray& );
   HugeArray& operator=( const HugeArray& );

public:

   // constructor : create uninitialised...must call setObjectSize
   //
   HugeArray();

   // destructor
   //
   virtual ~HugeArray();

   // specify the size of each object 
   //
   void setObjectSize( int sz );

   // Does nothing if newSize == size().
   // If extending, new data initialised to zero, if contracting, entire contents deleted!
   //
   void resize( int newSize );

   // clear all
   //
   void clearAll();

   // subscripting
   //
   //char& operator[]( int index );

   char* offset( int index );

   int size() const { return _size; }

   // low-level file io
   // assume file is pre-seeked.
   //
   void loadFromFD( int fd );
   //
   //void saveToFD( int fd );
};

#endif
