
// ------------------------------------------
// ------------------ array.c ---------------
// ------------------------------------------

/*tex
 
\file{array.c}
\path{src/template}
\title{Definitions for array classes}
\classes{Array}
\makeheader
 
xet*/

#ifndef _array_c
#define _array_c

#ifndef MODULE_OK
   //#define MODULE_OK
#endif

#include <utility>
using std::min;
using std::max;

#include <memory.h>
#include "array/array.h"
#include "general/murmurhash2.h"

#ifdef __xlC__
iodefinitions(class BGraph*)
iodefinitions(class NBBPG*)
iodefinitions(class InducedGraph*)
iodefinitions(struct NMPretestInternal)
iodefinitions(class _InternalNode)
iodefinitions(class fesEdge)
iodefinitions(class VertLevel)
template<class T> class DyArray;
iodefinitions2(class DyArray<char>*)
template<class A,class B> class SkipListNode;
typedef SkipListNode<int,class Client> skiper1;
typedef SkipListNode<class NodeNumber, class GraphDictItem> skiper2;
iodefinitions(skiper1*)
iodefinitions(skiper2*)
iodefinitions(class rec)
iodefinitions(class PWChar*)
iodefinitions(class ShortParse*)
iodefinitions2(class ShortParse)
iodefinitions2(class BitVector)
iodefinitions2(class VertSearchItem)
iodefinitions2(class Witness)
#endif

#ifdef __xlC__
opEqual(class SearchMinor)
opEqual(class NMPretestInternal)
opEqual(class _InternalNode)
opEqual(class fesEdge)
opEqual(class rec)
opEqual(class SearchNode)
#endif

// ------------------------------------------

//--- Array ---

// dtor
//
template< class T >
Array<T>::~Array()
{
   //cerr << "aad: " << (void*) array << ' ' << (void*) this << nl;
   if (array) delete [] array;
   array = 0;
}


// copy all ops from <a> into self
//
template< class T >
void Array<T>::copy( const T *a, int length )
{
   if ( length == 0 ) return;

   if ( simpleType( (*this)[0] ) )
   {
      memcpy( baseAddr(), a, length*sizeof(T) );
   }
   else
   {
      for ( int i=0; i<length; i++ )
         array[i] = (T) a[i];
   }
}


// Allocate the array and set the allocated size.
// This is a virtual function.
// Setting allocSize means nothing for Array (always==_size).
//
template< class T >
void Array<T>::allocate( int size )
{
   assert( size >= 0 );

   if ( size != 0 )
      array = new T[ size ];
   else
      array = 0;

   //cerr << "aa===" << size << ' ' << (void*) array << ' ' << (void*) this << nl;

   setSize( size );
}


// Allocate the array and set the allocated size.
// Copy n items from the old array to the new.
// Delete the old array.
// This is a virtual function.
// Setting allocSize means nothing for Array (always==_size).
//
template< class T >
void Array<T>::reallocate( int newSize, int keepNum )
{
//cerr << "array reallocate" << nl;

   assert( newSize >= 0 && keepNum >= 0 );
   assert( keepNum <= size() ); // can't keep more than it has

   if ( allocSize() == newSize ) return; // do nothing

   setSize( newSize );

   T* oldArray = array;

   if ( newSize != 0 )
      array = new T[ newSize ];
   else
      array = 0;

   //cerr << "ar===" << newSize << ' ' << array  << nl;

   int copyMax = min( keepNum, newSize );
   copy( oldArray, copyMax );

   if ( oldArray ) delete [] oldArray;
}


//----------------------------------------------------------------



// reallocate the array, copy old to new
//
template< class T >
void Array<T>::resize( int newSize )
{
   assert( newSize >= 0 );

   reallocate( newSize, min( size(), newSize ) );
}


// reallocate the array, do not copy old ops into new
//
template< class T >
void Array<T>::resizeNoCopy( int newSize )
{
   assert( newSize >= 0 );

   reallocate( newSize, 0 );
}


// resize, filling new area with fill
//
template< class T >
void Array<T>::resize( int newSize, const T& fill )
{
   int oldSize = _size;

   resize( newSize );

   for ( int i=oldSize; i<_size; i++ )
      array[i] = (T) fill;
}


// Constructors, copying  -----------------------------------------

template< class T >
Array<T>::Array( int size )
{
   assert( size>=0 );
   allocate( size );
   //cerr << "array int ctor" << nl;
}


template< class T >
Array<T>::Array( const Array<T>& a )
{
   allocate( a.size() );
   copy( (T*) a.baseAddr(), a.size() );
   //cerr << "array copy ctor" << nl;
}


// copy, with extra
//
template< class T >
Array<T>::Array( const Array<T>& a, int extra )
{
   assert( a.size() + extra >= 0 );

   allocate( a.size() + extra );
   copy( (T*) a.baseAddr(), a.size() );
}

// assignment
//
template< class T >
Array<T>& Array<T>::operator=( const Array<T>& a )
{
   if ( this != &a ) // beware of s=s
   {
      reallocate( a.size(), 0 );
      copy( (T*) a.baseAddr(), a.size() );
   }

   return *this;
}


template< class T >
bool Array<T>::operator==( const Array<T>& a ) const
{
   if ( size() != a.size() ) return false;
   for ( int i = 0; i < size(); i++ )
      if ( ! ( (*this)[i] == a[i] ) ) return false;
   return true;
}

template< class T >
void Array<T>::fill(const T &fill)
{
   for (int i=0; i<_size; i++) array[i] = (T) fill;
}


/*
 * Reverse a subrange of an array.
 */
template< class T >
void Array<T>::reverse( int i, int j )
 {
    assert( i>=0 && i<_size && j>=0 && j<_size && i <= j);

    if (j-i == 0) return;

    Array<T> rev(j-i+1);

//    cerr << "rev size " << rev.size() << nl;

    int k; for (k=0; k < rev.size(); k++) rev.array[k]=array[k+i];

    for (k=0; k < rev.size(); k++) array[j-k]=rev.array[k];
 }


/*
 * Rotate array Left a few times.
 */
template< class T >
void Array<T>::rotateLeft( int num )
 {
    assert( num>=0 && num<_size );

    if (num == 0) return;

    Array<T> rot(_size);

    int k; for (k=0; k < _size; k++) rot[k]=array[(k+num) % _size];
    //
    for (    k=0; k < _size; k++) array[k]=rot[k];
 }

/*
 * Rotate array Right a few times.
 */
template< class T >
void Array<T>::rotateRight( int num )
 {
    assert( num>=0 && num<_size );

    if (num == 0) return;

    Array<T> rot(_size);

    int k; for (k=0; k < _size; k++) rot[k]=array[(k+_size-num) % _size];
    //
    for (    k=0; k < _size; k++) array[k]=rot[k];
 }

/*
 * Try to find the index of an array element.
 */
template< class T >
int Array<T>::index( const T &elm ) const
 {
  int idx=0;
 
  // Some objects don't have operator()!= defined.
  //
  while ( idx < size() ) if (array[idx] == elm) return idx; else idx++;
  //while ( idx < size() && array[idx] != elm ) idx++;

  return idx;
 }


template< class T >
void Array<T>::shiftAndResizeLeft( int index, int num )
{
   assert( num >= 0 ); //assert( index <= num );
   
   assert( num <= size() );

   if (num == 0) return;

   int sz = size();
   for (int k=index; k < sz-num; k++) put( k, array[k+num] );
   resize( sz - num );
}

template< class T >
void Array<T>::shiftLeft( int index, int num )
{
   assert( num >= 0 ); //assert( index <= num );
   
   assert( num <= size() );

   if (num == 0) return;

   int sz = size();
   for (int k=index; k < sz-num; k++) put( k, array[k+num] );
   //resize( sz - num );
}

template< class T >
void Array<T>::shiftAndResizeRight( int index, int num )
{
   if (num == 0) return;

   resize( size() + num );

   for (int k=size()-num-1; k >= index; k--) put( k+num, array[k] );
}

template< class T >
unsigned int Array<T>::hash32( unsigned int seed ) const
{
    return MurmurHash2( array, _size * sizeof(T), seed );
}

template< class T >
unsigned long long Array<T>::hash64( unsigned long long seed ) const
{
    return MurmurHash64A( array, _size * sizeof(T), seed );
}

// copy memory into array
//
template< class T >
void Array<T>::binaryInsert( int index, char* buffer, int length )
{
   assert( (char*) offsetAddr( index ) + length <= (char*) offsetAddr(size()) );
   memcpy( offsetAddr( index ), buffer, length );
}

// append an array
//
template< class T >
void Array<T>::append( const Array<T> &a )
{
   int n = size();
   
   extend(a.size());

   for (int j=0; j<a.size(); j++) array[j+n]=a[j];
}

// subtrack items from an array
//
template< class T >
void Array<T>::minus( const Array<T> &a )
{
   for (int i=0; i<size(); i++)
    for (int j=0; j<a.size(); j++)
     {
       if (array[i]==a[j]) 
         {
           swap(i,size()-1);
           shrink(1);
           i--;
           break;
         }         
     } 
}

//------------------------------------------------------------------

template< class T >
texstream& operator<<( texstream& o, const Array<T>& A )
{
#ifndef __xlC__
   o << '[';
   if ( A.size() > 0 ) o << A[0];
 
   for ( int i = 1; i < A.size(); i++ ) o << ',' << A[i];
   o << ']';
#endif
 
   return o;
}

#if 1
template< class T >
istream& operator>>( istream& in, Array<T>& A )
{
   int n=A.length();
   int i=0;

   char c=0;

   while (c!='[') in >> c;
   in >> c;
   if (c==']') { A.resize(0); return in; }
   in.putback(c);

   while (i<n)
    {
#ifdef __xlC__
      aassert(false);
#else
      in >> A[i++];
#endif
      in >> c;

      if (c==']') { A.resize(i); return in; }
      assert(c==',');
    }

   T item;
   //
   while (c!=']')
    {
#ifdef __xlC__
      aassert(false);
#else
     in >> item;
#endif
//     cerr << "b) " << item << nl;

     A.append(item);

     in >> c;			// added by mjd 07/09/93
     assert(c==']' || c==',');	// dito
    }
 
   return in;
}
#endif

template< class T >
ostream& textOut( ostream& o, const Array<T>& A, const char* separator )
{
   o << '[';
   if ( A.size() > 0 ) o << A[0];
   for ( int i = 1; i < A.size(); i++ ) o << separator << A[i];
   return o << ']';
}

template< class T >
astream& operator>>( astream& s, Array<T>& A )
{
#ifdef __xlC__
   aassert(false);
   return s;
#else
   s >> getLeft;

   int sz;
   s >> sz;
   assert( sz >= 0 );
   A.resize(sz);

   for ( int i = 0; i < sz; i++ )
      s >> A[i];

   return s >> getRight;
#endif
}

template< class T >
ostream& operator<<( ostream& o, const Array<T>& A )
{
   //o << A.size() << ' ' << A.allocSize() << ' ';
   o << '[';
   if ( A.size() > 0 ) o << A[0];
   for ( int i = 1; i < A.size(); i++ ) o << ',' << A[i];
   o << ']';
   return o;
}

template< class T >
astream& operator<<( astream& o, const Array<T>& A )
{
#ifdef __xlC__
   aassert(false);
   return o;
#else
   o << putLeft << A.size();
   for ( int i = 0; i < A.size(); i++ )
      o << A[i] << ' ';
   return o << putRight;
#endif
}

#endif

