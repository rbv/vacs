
// ------------------------------------------
// ----------------- array.h ----------------
// ------------------------------------------

/*tex

\file{parray.h}
\path{inc/array}
\title{Header for array classes}
\classes{PArray}
\makeheader

xet*/

#ifndef _parray_h
#define _parray_h

#include "array/array.h"


// Wrapper class to make arrays of pointers type-safe.
// I/O routines are reimplemented to dereference.

template <class T>

class PArray : private Array< T* >
{

   templated_iodeclarations( PArray )

protected:

	// for sort (since Array is private)
	//
	void* baseAddr()
	   { return Array<T*>::baseAddr(); }
	//
	void* offsetAddr( int pos )
	   { return Array<T*>::offsetAddr( pos ); }


public:

	// constructor : create empty
        //
	PArray() : Array<T*>() {}

	// constructor : create with given size
        //
	PArray( int size ) : Array<T*>( size ) {}

	// copy constructor
        //
	PArray( const PArray<T>& A ) : Array<T*>(A) {}

	// copy constructor, with extra entries
        //
	PArray( const PArray<T>& A, int extra ) : Array<T*>(A,extra) {}


	// assignment
        //
	PArray<T>& operator=( const PArray<T>& A )
	   { Array<T*>::operator=(A); return *this; }

	// resize
	//
	void resize( int newSize )
	   { Array<T*>::resize( newSize ); }

	// resize with fill
	//
	void resize( int newSize, T* fill )
	   { Array<T*>::resize( newSize, fill ); }

	// destructor
        //
	~PArray() {}

	// subscripting
        //
	T*& operator[]( int i ) const
	   { return Array<T*>::operator[](i); }

	// Promotions
	//
	Array<T*>::size;
	Array<T*>::length;
	Array<T*>::get;
	Array<T*>::put;

	//friend ostream& textOut( ostream&, const PArray<T>&, const char* separator );

        // special stuff
        //
        void deleteAll();
};

#ifdef INLINE_ARRAY
#include "parray.c"
#endif

#endif
