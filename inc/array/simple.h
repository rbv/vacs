
// ------------------------------------------
// ----------------- simple -----------------
// ------------------------------------------

/*tex

\file{simple.h}
\path{inc/array}
\title{Simple type marking for array}
\classes{}
\makeheader

xet*/

#ifndef SIMPLE_H
#define SIMPLE_H

template< class T >
bool simpleType( const T& );

#define Inline inline

Inline bool simpleType( const int& )   { return true; }
Inline bool simpleType( const char& )  { return true; }
Inline bool simpleType( const short& ) { return true; }
Inline bool simpleType( const long& )  { return true; }
Inline bool simpleType( const uint& )   { return true; }
#ifndef CRAY
Inline bool simpleType( const uchar& )  { return true; }
#endif
Inline bool simpleType( const ushort& ) { return true; }
Inline bool simpleType( const ulong& )  { return true; }

Inline bool simpleType( const class Operator& )  { return true; }
Inline bool simpleType( const class NodeNumber& )  { return true; }
Inline bool simpleType( const class SearchNode& )  { return true; }

template <class T>
Inline bool simpleType( const T& )
{
   return false;
}

#undef Inline

#endif
