
// ------------------------------------------
// --------------- dyarray.h ----------------
// ------------------------------------------

/*tex

\file{dyarray.h}
\path{inc/array}
\title{Header for array classes}
\classes{DyArray}
\makeheader

\begin{desc}
This class provide support for dynamic arrays (via explicit 
length control member functions) of objects, using the template mechanism.
Class DyArray is derived from Array.

Also see classes Array, PArray, and GrowArray.

\end{desc}

xet*/

#ifndef _dyarray_h
#define _dyarray_h

#include "vacs/object.h"
#include "array/array.h"
#include "stdtypes.h"

/*
 * Commented out member functions are inherited from class Array.
 */

template< class T >

class DyArray : public Array<T>
{

private:
   // Actual allocated size of the array
   //
   int _allocSize;

protected:

   using Array<T>::array;
   using Array<T>::copy;
   using Array<T>::setSize;
   using Array<T>::setBaseAddr;

   // These functions and resize(...) redefine the storage management.
   //
   virtual int allocSize() const { return _allocSize; }
   //
   virtual void allocate( int size );
   //
   virtual void reallocate( int newSize, int keepNum );

public:

   using Array<T>::resize;
   using Array<T>::resizeNoCopy;
   using Array<T>::shrink;
   using Array<T>::size;
   using Array<T>::get;
   using Array<T>::put;
   using Array<T>::baseAddr;
   using Array<T>::offsetAddr;

   // constructor : create with 0 size
   //
   DyArray()
      { allocate(0); }

   /*
    * constructor : create with given size and extra space to grow.
    */
   DyArray( int size, int moreSpace=0 );
          
   // copy ctors
   //
   DyArray( const Array<T>& a );
   //
   DyArray( const DyArray<T>& a );

   /*
    * Copy constructor, with extra entries (size may be negative!)
    */
   DyArray( const Array<T>& a, int size );


   /*
    * Assignment:
    * does not reallocate if space(rhs) >= size(lhs)
    */
   DyArray<T>& operator=( const Array<T>& a );
   //
   DyArray<T>& operator=( const DyArray<T>& a );


   // Resize and over-allocate the array.
   // Array's usable size is newSize, and allocSize is newSize + extraSize
   //
   void resizeAndOverAllocate( int newSize, int extraSize );
   //
   void resizeNoCopyAndOverAllocate( int newSize, int extraSize );

   // Return a reference to an element, automatically extending array if needed
   T& auto_ref( int index )
   {
       if ( index >= size() )
           resizeAndOverAllocate( index + 1, 100 );
       return (*this)[index];
       //for ( int i = size(); i <= index; i++ )
   }

#if 1
   // comparison 
   //
   bool operator==( const DyArray<T> &DA ) const
    {
     // the virtual stuff should kick in.
     //
     return Array<T>::operator==( DA );
    }
#endif

};

//#define Inline inline
//#include "../src/template/array/dyarray.c"
//#undef Inline

#ifdef INLINE_ARRAY
#include "dyarray.c"
#endif

#endif
