
/*tex
 
\file{message.h}
\path{inc/vacs}
\title{Header for server/client messages.}
\classes{Message}
\makeheader
 
xet*/

#ifndef _message_h
#define _message_h
#undef minor

#include "stdtypes.h"
#include "general/stream.h"

// Message control (worker, dispatcher, manager)

class Message
{
  
public:

#if 0
  // first byte of any message - indicates routing
  enum MessageType
  {
    LockRequest,       // all to lock manager
    WorkResult,        // workers to dispatcher
    WorkData,          // dispatcher to workers
    Worker1Request,    // worker1 to dispatcher
    UnknownNodes,      // manager to dispatcher
    SearchComplete     // manager to dispatcher
  };
#endif

  enum WorkResultType {
    MinimalByTestset,		// node num, array of minor#s, array of test#
    MinimalByExtn,		// node num, array of minor#s, extn
    MinorCongTestset,		// node num, minor #
    MinorCongDyProg,		// node num, minor #
    MinimalByTightCong,		// node num
    InconclusiveMinimality,	// node num
    ENoncanonic,		// node num
    MinimalByOOF,		// node num
    NonminimalByOOF,		// node num
    HNoncanonic,			// node num
    ProcessFinished,		// worker or dispatcher quitting
    BackDoor,			// send to dispatcher by 'human' process
    Pause,				// sent to manager to throttle (searchnode queue full)
    Resume,				// sent to manager to resume
  };

  // set/find advertized server's socket address
  // (returns false if already opened)
  //
  static bool setServerNames( const char* server, int portNum,
                              ofstream& out ); // for dispatcher
  //
  static void closeServer(); // for dispatcher
  //
  static bool getServerNames( char* hostname, int &portNum ); // for worker
  //
  static int getDispatchFD(); // for worker (calls getServerNames, etc.)

};

#if 0
inline bstream& operator>>( bstream& s, Message::MessageType& msg )
{ s >> (int&) msg; return s; }
#endif

#if 0
// change to uchar ?  // mjd test
//
inline bstream& operator>>( bstream& s, Message::WorkResultType& msg )
{ uchar uc; s >> uc; msg = (Message::WorkResultType) uc; return s; }

inline bstream& operator<<( bstream& s, const Message::WorkResultType& msg )
{ s << (uchar) msg; return s; }

#else

inline bstream& operator>>( bstream& s, Message::WorkResultType& msg )
{ s >> (int&) msg; return s; }

inline bstream& operator<<( bstream& s, const Message::WorkResultType& msg )
{ s << (int&) msg; return s; }

#endif

// returns listen fd for server
// (uses current host and uses/modifies port number)
//
int serverListen( char *hostname, int hsize, int &portNum );

// returns client's connection fd
//
int clientConnect( const char *hostname, int portNum );

// new connection for server from a client
//
int serverAccept(int listenfd); // vanilla for now

#endif
