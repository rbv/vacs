
// -----------------------------------------
// --------------- fs_lock.h ---------------
// -----------------------------------------

/*tex
 
\file{fs_lock.h}
\path{inc/vacs}
\title{Header for server/clients locking of file system.}
\classes{FilesystemLock}
\makeheader
 
xet*/

#ifndef _fs_lock_h
#define _fs_lock_h

#include <unistd.h>
#include "stdtypes.h"
#include "assert.h"
#include "general/stream.h"

//------------------ low level code -----------------------

#ifdef UNIX_DOMAIN

// from Stevens' ANSI C library...
//
extern "C"
{
 int cli_conn( const char* name );
 int serv_accept( int listenfd, uid_t *uidptr );
 int serv_listen( const char* name );
}

inline int clientConnect( const char* name )
 { return cli_conn( name ); }

inline int serverListen( const char* name )
 { return serv_listen( name ); }

inline int serverAccept(int listenfd, uid_t *uidptr )
 { return serv_accept( listenfd, uidptr ); }

#else /* use the INET domain */

// returns listen fd for server 
// (uses current host and uses/modifies port number)
//
int serverListen( char *hostname, int hsize, int &portNum );

// returns client's connection fd
//
int clientConnect( const char *hostname, int portNum );	

// new connection for server from a client
//
int serverAccept(int listenfd); // vanilla for now 

#endif

//-------------- Client and Server stuff ----------------

// The kinds of requests clients can make to server.
//
// (Lock/Unlock) System (Read/Write/CommandFileOnly)
//
//enum FS_RequestType { LSysR=0, USysR,  LSysW, USysW,  LSysC, USysC,  NoAck };
enum FS_RequestType { LSysR=0, USysR,  LSysW, USysW,  NoAck };

// retreval functions for socket names
//
//void getServerPathName( char* path );	// advertized server's socket
//void getClientPathName( char* path );	// local temp file for clients socket


//-------------- FileManager's lock class ----------------

/*
 * Clients 'access class' for gaining rights to filesystem.
 */
class FilesystemLock
{
  int	fd;	// file descriptor for this end of server/client socket

public:
 
  // Open a connection with the lock server (external program). 
  //
  FilesystemLock();

  // The following close sends EOF to server (I think?).
  //
  ~FilesystemLock() { aassert( close(fd) == 0 ); }  

  // This method blocks until request is accepted by server (or error).
  //
  bool request( const FS_RequestType rType );

  // set/find advertized server's socket address 
  // (returns false if already opened)
  //
  static bool setServerNames( const char* server, int portNum,
			      ofstream& out );

  static void spawnServer();
  //
  static void closeServer();
  //
  static bool getServerNames( char* hostname, int &portNum ); 

};

#endif
