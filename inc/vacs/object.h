
// -----------------------------------------
// --------------- object.h ----------------
// -----------------------------------------

/*tex
 
\file{object.h}
\path{inc/vacs}
\title{Header for object class}
\classes{VacsObject}
\makeheader
 
xet*/

#ifndef _object_h
#define _object_h

//#define VAXOBJECT

/*
 * For checking other  #ifdef's
 */
#ifdef VAXOBJECT

// Empty root class for all classes

class VacsObject
{
};

#endif

#endif
