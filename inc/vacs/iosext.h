
// -----------------------------------------
// --------------- iosext.h ----------------
// -----------------------------------------

/*tex
 
\file{iosect.h}
\path{inc/vacs}
\title{Header for iostream extension functions}
\classes{}
\makeheader
 
xet*/


#ifndef _iosext_h
#define _iosext_h

// Extensions to stream classes

#include "stdtypes.h"


void get( istream&, int& );

void put( ostream&, int );

#endif
