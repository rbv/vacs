
// -----------------------------------------
// ------------- fs_server.h ---------------
// -----------------------------------------

/*tex
 
\file{fs_server.h}
\path{inc/vacs}
\title{Header for servers that lock filesystem and take care of work requests.}
\classes{Client,LockList,WorkList}
\makeheader
 
xet*/

/* ----------------------------------------------- */
/* This is a private header for filesystem server! */
/* ----------------------------------------------- */

#ifndef _fs_server_h
#define _fs_server_h

#include <sys/types.h>
#include <sys/socket.h>
#include "vacs/fs_lock.h"
#include "general/queue.h"
#include "general/bitvect.h"
#include "set/skiplist.h"

//#define MachineNameLen 20


#include <sys/select.h>
//#ifndef FD_SETSIZE
//#define FD_SETSIZE 128
//#endif

class Client
{
public:

   int		fd;

   sockaddr	peer;
   //int		peerSize;

   Client() { fd = -1; }

   bool operator==(const Client &C2) const { return fd == C2.fd; }
   bool operator< (const Client &C2) const { return fd < C2.fd; }
   bool operator> (const Client &C2) const { return fd > C2.fd; }

   bool operator==(int fd2) const { return fd == fd2; }
   bool operator< (int fd2) const { return fd < fd2; }
   bool operator> (int fd2) const { return fd > fd2; }

};

ostream &operator<< (ostream &os, const Client &cl);

typedef SkipListNode< int, Client > ClientNode;

class LockList : public SkipList< int, Client >
{
  int _maxfd,		// Maximum number of any of the file descriptors.
      _readLocks;	// How many read locks?
      
  Queue< int > 	_readReqQueue, 	// waiting lists
//	        _cmmdReqQueue,
		_writeReqQueue;

  //BitVector _readers(sizeof(fd_set)*8); // probably hard limit of 32
  BitVector _readers; // (FD_SETSIZE); // probably a hard limit of 256

  int _writeLock;	// Who has the system locked? (-1 if no one)
//      _cmmdLock;	// Who has the command file locked?

public:

  LockList() : _readers(FD_SETSIZE)
  { 
   _maxfd = -1; 
   _readers.clearAll();
   _readLocks = 0;
   
   _writeLock = -1;;
//   _cmmdLock = -1;
  }

  ~LockList() {}

  void clientAdd(int fd);

  void clientDel(int fd);

  void request(const char *buf, int nread, int fd);

  int max_fd() const { return _maxfd; } // returns -1 if none.

  void dump( ostream &out = cerr ) const;
};

//---------------------------------------------------------------------

#include "search/nodenum.h"
#include "bpg/bpg.h"

struct GraphDictItem
{

public:

  NodeNumber n;
  RBBPG G;

  bool operator==( const GraphDictItem& _n ) const
    { return _n.n.asInt() == n.asInt(); }

  bool operator<( const GraphDictItem& _n ) const
    { return _n.n.asInt() < n.asInt(); }

  bool operator>( const GraphDictItem& _n ) const
    { return _n.n.asInt() > n.asInt(); }

  bool operator==( const NodeNumber _n ) const
    { return _n.asInt() == n.asInt(); }

  bool operator<( const NodeNumber _n ) const
    { return _n.asInt() < n.asInt(); }

  bool operator>( const NodeNumber _n ) const
    { return _n.asInt() > n.asInt(); }
};


class WorkList : public SkipList< int, Client >
{

  int _maxfd;		// Maximum number of any of the file descriptors.
  int _numWorkers;
  int _workCount;	// How many workers are working?
  int _assignedCount;	// Number of nodes assigned to workers

  int _jobsPerWorker;	// Max number of nodes to assign to a single worker

  Queue< NodeNumber > 	_unknownQueue; // waiting lists of uknown nodes
  // List of workers which might be able to take more jobs.
  // A worker can appear multiple times, even if they can't take any work
  Queue< int > 		_workQueue;

  SkipList< NodeNumber, GraphDictItem >	_graphDict;

  //BitVector _workers; // (FD_SETSIZE); // probably a hard limit of 256
  Array< Array<NodeNumber> > _doing;	// nodes assigned to each worker

  mutable double _idleStartTime;
  mutable double _workersIdleTime;

  void send2worker(int w_fd, NodeNumber node);

  void distributeWork();

  void checkWhetherIdling() const;

public:

  WorkList();
  ~WorkList() {}

  void readConfig();

  void clientAdd(int fd);

  void clientDel(int fd);

  void doUnknownNode(NodeNumber node, const RBBPG& graph );

  // greater than or equal to largest fd
  int max_fd() const { return _maxfd; } // returns -1 if none.

  int numWorkers() const { return _numWorkers; }  // SkipList::size not implemented!

  int nodesWaiting() const { return _unknownQueue.size(); }

  // Workers with no nodes assigned
  int workersWaiting() const { return numWorkers() - _workCount; }

  void clientFinishedJob( int fd );

  // Amount of time where at least one worker was waiting for a job from the manager
  double workersIdleTime() const;

  void dump( ostream &out = cout ) const;

  void backDoorDump( bostream &out ) const;
};

#endif
