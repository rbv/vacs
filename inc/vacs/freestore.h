
// -----------------------------------------
// ------------- freestore.h ---------------
// -----------------------------------------

/*tex
 
\file{freestore.h}
\path{inc/vacs}
\title{Header for free store tracking macros}S
\classes{}
\makeheader

 
xet*/

#include <stddef.h>

// Macros for installing new/delete counting in a class.
//
//
// In class declaration, do something like:
//   class Foo : public Base
//   {
//      FREESTOREDECL
//      private:
//        ...
//      public:
//        ...
//   };
// Note that this macro leaves the class in private state.
//
// In a definition file, put:
// FREESTOREDEFN
//
// See object.h and object.c for an example.

// Util function to print the class status - this means that
// iostream.h is not neeeded for the inline declarations.
//
void classStatus( char* text, char* className, long long newCount, long long delCount );

#ifdef FST

#define FreeStoreTest( className )					\
   private:								\
      static long long newCount;						\
      static long long delCount;						\
   public:								\
      void* operator new( size_t s )					\
         { newCount++; return ::operator new(s); }			\
      void operator delete( void* p )					\
         { if (p) delCount++; ::operator delete(p); }			\
      static void classStatus()						\
         { ::classStatus( "", "className", newCount, delCount ); }	\
      static void classStatus( char* text )			\
         { ::classStatus( text, "className", newCount, delCount ); }	\
   private:

#define FreeStoreDefn( className )					\
   long long className::newCount = 0;					\
   long long className::delCount = 0;

#define FreeStoreDefnTemplate( className )				\
   template< class T > long long className<T>::newCount = 0;			\
   template< class T > long long className<T>::delCount = 0;

#else

#define FreeStoreTest( className )

#define FreeStoreDefn( className )

#define FreeStoreDefnTemplate( className )

#endif

/* OLD
#define FreeStoreDefn( className )				\
   inline void className::classInit()				\
      { newCount = delCount = 0; }				\
   inline void className::classDestruct()			\
      { newCount = delCount = 0; }				\
   inline void className::classStatus( ostream& o )		\
      { o << "className : new: " << newCount 			\
          << ", delete: " << delCount				\
          << ", difference: " << delCount - newCount; }

*/
