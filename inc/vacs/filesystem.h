
// ------------------------------------------
// ------------- filesystem.h ---------------
// ------------------------------------------

/*tex
 
\file{filesystem.h}
\path{inc/vacs}
\title{Header for filesystem classes}
\classes{Filename, FileManager}
\makeheader
 
xet*/


#ifndef _filesystem_h
#define _filesystem_h

#include "stdtypes.h"
#include "search/nodenum.h"
#include "array/array.h"
#include "general/str.h"
#include "general/stream.h"
#include "general/database_b.h"
#include "vacs/fs_lock.h"

#undef minor

class Search;

// Declaration of class FileManager.
//
// All methods are static, and no FileManager object exists.
// This class provides the interface to the OS for naming, opening, and 
// destroying files and directories.

//-----------------------------------------------------------------

// Class Filename supports fast construction of file names
//
class Filename 
{
private:

	// string where base is kept, and extension is placed
	//
	char* str;

	// the number of bytes allocated is here
	//
	int alloc;

	// position of end of base (extension is placed here)
	//
	int extPos;

public:
	// trivial ctor - set state to be invalid
	//
	Filename();

	// copy constructor
	//
	Filename( const Filename& );

	// The following constructors create a string containing
	// the "base" of the filename.
	// When "name" is called, the appropriate extension is
	// appended (in-place).
	//
	Filename( const char* base, int extraSpace );
	//
	Filename( const char* base1, const char* base2, int extraSpace );
	//
	Filename( const Filename&, const char* base2, int extraSpace );
	//
	Filename( const Filename&, const char* base2,
	          const char* base3, int extraSpace );

	// dtor
	//
	~Filename();

	// append the extension to the base, and return the
	// pointer to the (modified) string.
	// The no-arg form just returns the current string.
	//
	const char* name() const { return str; }
	//
	const char* name( int extension ) const;
	//
	const char* name( const char extension ) const;
	//
	const char* name( const char* extension ) const;

	// Strip filename, returning directory (with final /)
	Filename dirname() const;

	// assignment operator - copies the string, so don't overuse!
	//
	void operator=( const Filename& );

};

//-----------------------------------------------------------------

// FileManager assumes that the global runInfo is available.

class FileManager 
{

private:

   // Can't be created
   FileManager();

   // Numerical constants
   //
   enum { 
      defaultFileProt = 0660, 
      //logOpenMode = ios::out | ios::app | ios::noreplace,
      //commandWriteMode = ios::out | ios::app | ios::nocreate,
      logOpenMode = (int)ios::out | (int)ios::app,
      commandWriteMode = (int)ios::out | (int)ios::in | (int)ios::ate,  // don't create the file (ios::app must not be specified)
      commandReadMode = ios::in 
   };

   // Used to build filenames
   //
   static Filename _mainDir;

   // File name support.
   // These functions return strings that
   // can be used to open files.
   //
   static const char* mainFilename( const char* extension );
   static const char* commandFilename();
   static const char* nodeStatusFilename();
   static const char* managerStatusFilename();

   // Constants appended to runDirectory to get subdirs and filenames.
   //
   static const char* _backupExtension;

   static const char* _nodeBaseDirname;
   //static const char* _snNodeFilename;
   //static const char* _snGraphFilename;
   //static const char* _snChildFilename;
   //static const char* _snMinorFilename;

   static const char* _commandFilename;
   //static const char* _minimalDBFilename;
   //static const char* _nonminimalDBFilename;
   static const char* _nodeStatusFilename;
   static const char* _managerStatusFilename;
   static const char* _logDispatchBasename;
   static const char* _logManagerBasename;
   static const char* _logBrowserBasename;
   static const char* _logWorkerBasename;

   static const char* _MDBBasename;
   static const char* _NDBBasename;
   static const char* _SNBasename;
   static const char* _SNUEBasename;
   static const char* _SNIIBasename;
   static const char* _SNCBasename;
   static const char* _SNIBasename;
   static const char* _IsoDBBasename;


   // log support
   //
   static Filename _logName;
   static const int _logMax;
   static int _logExtensionNum;
   static ofstream _log;

   static void checkLog( ofstream&, int maxNum, const Filename&, 
			 int& extensionNum );
   static void openLog( ofstream&, const Filename&, int& extensionNum );
   static void logHeader( ostream& );

   // Databases

   //static void nodeFileSystemInit();
   //static void nodeFileSystemDestruct();
   //static class Database* _snNodeDB;
   //static class Database* _snMinorDB;
   //static class Database* _snChildDB;
   //static class Database* _snGraphDB;


   // Initialization support
   //
   // Name of file system initialization script (with path).
   //
   //static const char* _familyToolBin;

   // Run support
   //
   // Name of search executable (with path).
   //
   static Filename _executableFilename;

   // File utilities
   //
   // Low level
   //
#ifdef OLD_LOCK_STUFF
   static void lock( fstreambase&, bool writeLock );
   //
   static void unlock( fstreambase& );
#endif
   //
   // General access
   //
   static void openRead( fstream&, 
                         const char*, 
                         int mode );
   //
   static void openWrite( fstream&, 
                          const char*, 
                          int mode );
   //
   static void open( ifstream&, 
                     const char*, 
                     int mode = ios::in );
   //
   static void open( ofstream&, 
                     const char*, 
                     int mode );
   //
   static void close( ifstream& );
   static void close( ofstream& );
   static void closeRead( fstream& );
   static void closeWrite( fstream& );
   static void close( fstream& );
   static void closeNoUnlock( fstream& );
   //
   static void lock( fstream&, bool writeLock );
   static void lock( ifstream& );
   static void lock( ofstream& );
   //
   static void unlock( fstream& );
   static void unlock( ifstream& );
   static void unlock( ofstream& );

public:

   // make sure that the run directories either don't exist, or are empty.
   // this function is to be called once, after runInfo::readInfo().
   //
   static void initialize(const char* exeName);
   //
   // Manager file system initialization.
   //
   static void managerInitialization();
   //
   // Destruction...end of run.
   //
   static void destruction();


   // Manager status file support.
   //
   // Try to open the manager status file for reading.
   // No error checking (check stream after call).
   // For use by RunInfo.
   //
   static void openManagerStatusFileForRead( ifstream& );
   //
   // Try to open the manager status file for writing.
   // No error checking (check stream after call).
   // For use by RunInfo.
   //
   static void openManagerStatusFileForWrite( ofstream& );
   //
   // Check if the manager status file exists.
   // Return true if it can be opened for reading.
   //
   static bool assertManagerStatusFile();
   //
   // Close the manager status file
   //
   static void closeManagerStatusFile( ofstream& f ) { close(f); }
   static void closeManagerStatusFile( ifstream& f ) { close(f); }
	
   // Log support.
   // These functions return an ios for output.
   // 
   static ostream& log();


   // Command information file support.
   // These functions are only used by class SearchControl.
   //
   enum { CommandLineMax = 500 };
   //
   static void writeLineToCommandFile( Str );
   //
   // Read a line from the command file, if one is available.
   // If there isn't one, set str to the null string.
   // Changes \n to \0 at the end of the string.
   // The read starts at the passed position, and the position
   // is updated to reflect the read.
   //
   static Str readLineFromCommandFile( int& pos );


   // Return full path and name of the search main program.
   //
   static const char* executableFilename()
      { return _executableFilename.name(); }

   static Filename executableDirectory()
      { return _executableFilename.dirname(); }

   static const char* lockFilename() { return "_lock"; }

   static const char* shutdownFlagFilename() { return "_stop"; }

   // Database file names
   //
   static DatabaseBase::AccessMode standardAccessMode();
   static DatabaseBase::AccessMode standardCacheMode();
   static const char* MDBBasename();
   static const char* NDBBasename();
   static const char* SNBasename();
   static const char* SNUEBasename();
   static const char* SNIIBasename();
   static const char* SNCBasename();
   static const char* SNIBasename();
   static const char* IsoDBBasename();

   /*
    * Client/Server based filesystem locking.
    */
private:
   
   static FilesystemLock *fs_locker;

public:
   //
   // This method blocks until request is accepted by server (or error).
   //
   static bool lockRequest( const FS_RequestType rType )
   {
    aassert( fs_locker->request( rType ) ); // temp. crash on errors
    return true;
   }

};  // FileManager

// Replacement for NOFILE, which is obsolete and incorrect on Linux
int maxFile();

bool fileExists(const char* path);

// Global log function
//
inline ostream& log() { return FileManager::log(); }
//inline ostream& log() { FileManager::log().flush(); return FileManager::log(); }

#endif
