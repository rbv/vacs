
// ------------------------------------------
// --------------- runinfo.h ----------------
// ------------------------------------------

/*tex
 
\file{runinfo.h}
\path{inc/vacs}
\title{Header for run information class}
\classes{RunInfo}
\makeheader
 
xet*/


#ifndef _runinfo_h
#define _runinfo_h

#include <unistd.h>

//cxx #include <sysent.h>

#include <sys/param.h>
#include <stdlib.h>

#include "stdtypes.h"
//#include "family/probinfo.h"

#ifdef __xlC__
extern "C" int gethostname(char*, int);
#endif

// Declaration of class RunInfo.
// A single instance, runInfo, of this class is defined globally in runinfo.c.
// The declaration of this instance is in this header.
// This class stores all of the run-dependent parameters of the program,
// and make them available to the whole of the program.

//-----------------------------------------------------------------


class RunInfo 
{

public:
	// Run types
	//
	enum RunType
	{
	   Manager,		// Write access to nodes, USR1 signal
	   Dispatch,		// Read access, USR1 signal
	   Worker,		// Read access, no signals
	   Browser,		// Write access to browser files, no signals
	   Test			// No files.
	};

private:

	// always _pathWidth + 1
        //
	int _boundarySize;

	const char* _driverFilename;

	static RunType _runType;

	// Signals
	//
	// Flags is a signal has been received.
	//
	friend void signalTrap(int);
	//
	bool _signal;
	//
	// Signal has been received - set flag (signalTrap only)
	// 
	void setSignal()
	  { _signal = true; }

public:

	// Constructor/destructor

	// constructor just initializes all fields to 'invalid' state
        //
	RunInfo();

	// free any strings and such
        //
	~RunInfo();

	// read info from named file, and initialize all fields
        //
	void initialize( const char* filename, RunType runType );

	short bs() { return _boundarySize; }
	short boundarySize() { return _boundarySize; }

	static RunType runType(){ return _runType; }

	void printHeader( ostream& o );

#if 0
	// --- Signals ---
	//
	// Called during search startup to setup signal processing.
	//
	void installSignalTrap();
	// 
	// Send a signal to the manager (dispatch only)
	//
	bool sendSignalToManager();
	// 
	// Send a signal to the pid (manager only, sending to dispatch)
	//
	bool sendSignalToPID( int pid );
	// 
	// Test if a signal has been received (manager,dispatch)
	// 
	bool signal() const
	  { return _signal; }
	// 
	// Signal has been processed - clear flag (manager,dispatch)
	// 
	void clearSignal()
	  { _signal = false; }
#endif

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 64
#endif

private:

   // storage for hostname
   //
   static char _hostname[ MAXHOSTNAMELEN ];

public:

   static int getPid() { return ::getpid(); }

   const char* driverFilename() const { return _driverFilename; }

   // returns pointer to static storage 
   //
   //
   static const char* getHostname()
      { 
        aassert( gethostname( _hostname, MAXHOSTNAMELEN ) == 0 );
        return _hostname;
      }

   static const char* getVACSpath() 
    { 
      // no memory loss -- pointer to current enviornment
      //
      const char *env = getenv("VACS"); 
      aassert(env);
      return env;
    }

   // ------------------------------------------------------------------
   //
   // --------- Run Status ----------
   //

#if 0
private:

   // Manager Status file format:
   //
   // pid runningFlag commandFilePosition hostname 
   //

   // For Manager only
   //
   static int _managerCommandPosition;

public:

   static void managerCommandPosition( int i );

   static int managerCommandPosition()
      { assert( runType() == Manager );
        return _managerCommandPosition;
      }

   // Save the status to disk.
   // Fatal on file system error.
   // Manager only.
   //
   static void saveManagerStatus( bool shutdown = false );

   // Try to read the status from the filesystem.
   // If file not found, return false.
   // Fatal error if any other problems.
   // 
   //
   //static bool readManagerStatus();

   // Load the command position and last node num, for non-1st-run startup.
   //
   static void loadManagerStatus();

   // Stop the manager.
   // Write the status file (running=false), and exit.
   // Does not return.
   //
   void managerShutdown();

   // Initialize to values for the first run of the
   // program.  Called if status file does not exist.
   // For FileManager only.
   //
   static void initializeManagerStatus();

   // Methods for programs to get the pid and hostname
   // of the currently running manager.
   // Returns 0 if not found.
   //
   static int getManagerPID();
   //
   //static char* getManagerHostname();
   // 

   static bool isManagerRunning();
#endif

};


// ------------- Declarations of Globals ---------------

extern RunInfo runInfo;

#endif
