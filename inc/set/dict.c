
/*tex
 
\file{dict.c}
\path{src/template}
\title{Main file for dictionary classes}
\classes{}
\makeheader
 
xet*/

#ifndef _dict_c
#define _dict_c

#include "dict.h"
#include "general/stream.h"

template< class Key, class Dat >
void Dictionary<Key,Dat>::in( bistream& s )
{
   s >> keys >> dats;
}

template< class Key, class Dat >
void Dictionary<Key,Dat>::out( bostream& s ) const
{
   s << keys << dats;
}

template< class Key, class Dat >
void Dictionary<Key,Dat>::out( astream& s ) const
{
   s << putLeft << keys << dats << putRight;
}

#if 0
template< class Key, class Dat >
void Dictionary<Key,Dat>::in( astream& s ) const
{
   aassert(false);
}
#endif

template< class Key, class Dat >
void Dictionary<Key,Dat>::out( ostream& s ) const
{
   s << '<';
   for ( int i=0; i<size(); i++ )
   {
      if ( i ) s << ';';
      s << keys[i] << ',' << dats[i];
   }
   s << '>';
}

//-------------------------------------------------------

template< class Key, class Dat >
Dictionary<Key,Dat>::Dictionary()
{
}

template< class Key, class Dat >
void Dictionary<Key,Dat>::insert( const Key& k, const Dat& d )
{
   int p = pos(k);
   if ( p == -1 )
   {
      keys.append( k );
      dats.append( d );
   }
   else
      dats[p] = d;
}

template< class Key, class Dat >
int Dictionary<Key,Dat>::pos( const Key& k ) const
{
   int l = keys.size();
   for ( int i=0; i<l; i++ )
      if ( keys[i] == k ) return i;
   return -1;
}

template< class Key, class Dat >
const Dat& Dictionary<Key,Dat>::get( const Key& k ) const
{
   int p = pos( k );
   assert( p != -1 );
   return dats[p];
}

template< class Key, class Dat >
bool Dictionary<Key,Dat>::defined( const Key& k ) const
{
   return pos(k) != -1;
}


template< class Key, class Dat >
void Dictionary<Key,Dat>::del( const Key& k )
{
   int p = pos( k );
   assert( p != -1 );
   keys.shiftAndResizeLeft( p, 1 );
   dats.shiftAndResizeLeft( p, 1 );
}

template< class Key, class Dat >
void Dictionary<Key,Dat>::reverse()
{
   keys.reverse( 0, size() - 1 );
   dats.reverse( 0, size() - 1 );
}

template< class Key, class Dat >
int Dictionary<Key,Dat>::iterInit( Key& k ) const
{
   if ( keys.size() > 0 ) k = keys[0];
   return 0;
}

template< class Key, class Dat >
bool Dictionary<Key,Dat>::iterActive( int i ) const
{
   return i < size();
}

template< class Key, class Dat >
void Dictionary<Key,Dat>::iterNext( int& i, Key& k ) const
{
   i++;
   if ( i < size() ) k = keys[i];
}

#ifndef INLINE_SET

#include "bpg/bpg.h"
#include "search/searchminor.h"

template class Dictionary<short, short>;
template class Dictionary<short, NBBPG>;
template class Dictionary<short, SearchMinor>;


#endif

#endif
