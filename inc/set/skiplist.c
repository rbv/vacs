/*tex

\file{skiplist.c}
\path{src/template}
\title{Main file for skiplist class}
\classes{}
\makeheader

xet*/

#ifndef _skipList_c
#define _skipList_c

#include "set/skiplist.h"

// some statics
//
template< class Key, class Obj > int SkipList<Key,Obj>::MaxNumberOfLevels = 16;

template< class Key, class Obj > RandomInteger SkipList<Key,Obj>::RND(32768);

template< class Key, class Obj >
//
int SkipList<Key,Obj>::randomLevel()
{
  int level=1;

  // p = 1/4;
  //
  while ( (RND() & 0x3)==0 && ++level < MaxNumberOfLevels-1 );

  return level;
}

template< class Key, class Obj > 
//
SkipList<Key,Obj>::SkipList()
{
  _level = 0;  // max active level (actually one less than used).

  _header = newNodeOfLevel(MaxNumberOfLevels);

//cerr << "MaxNumberOfLevels=" << MaxNumberOfLevels << ' ' << _header <<nl;

  for(int i=0; i<MaxNumberOfLevels; i++) 
   {
     _header->put(i,0);		// null pointers
   }
}

template< class Key, class Obj >
//
void SkipListNode<Key,Obj>::_internalDelete( SkipListNode<Key,Obj> *p )
{
 if (p->get(0)) _internalDelete(p->get(0));

 delete p;
}

template< class Key, class Obj > 
//
SkipList<Key,Obj>::~SkipList()
{
  if (_header->get(0)) SkipListNode<Key,Obj>::_internalDelete(_header->get(0));

  delete _header;
}

template< class Key, class Obj > 
//
void SkipList<Key,Obj>::insert(const Obj &object) 
{
    int k = _level;
    SkipListNode<Key,Obj> *p = _header, *q;

//cerr << "inserting " << object;

    Array< SkipListNode<Key,Obj>* > update(MaxNumberOfLevels); 

//cerr << "searching p=" << p << nl;
    do
    {
	while ( q = p->get(k) )
         {
//cerr << "q=" << q << nl;
           if (q==0 || q->object() > object) break;

           p = q;
         }
	update[k] = p;

    } while(--k>=0);

    k = randomLevel();

    if (k>_level) 
     {
	k = ++_level;
	update[k] = _header;
     }

//cerr << " with level " << k << nl;

    q = newNodeOfLevel(k+1,object);

    do 
    {
	p = update[k];
	q->put(k,p->get(k));
	p->put(k,q);

    } while(--k>=0);

//cerr << "end of insert\n";
}

// almost identical to above (note: two return statements)
//
template< class Key, class Obj > 
//
bool SkipList<Key,Obj>::uniqueInsert(const Obj &object) 
{
    int k = _level;
    SkipListNode<Key,Obj> *p = _header, *q;

//cerr << "Unique inserting " << object;

    Array< SkipListNode<Key,Obj>* > update(MaxNumberOfLevels); 

//cerr << "searching p=" << p << nl;
    do
    {
	while ( q = p->get(k) )
         {
//cerr << "q=" << q << nl;

           if (q==0 || q->object() > object) break;

	   if (q->object() == object) return false;

           p = q;
         }
	update[k] = p;

    } while(--k>=0);

    k = randomLevel();

    if (k>_level) 
     {
	k = ++_level;
	update[k] = _header;
     };

//cerr << " with level " << k << nl;

    q = newNodeOfLevel(k+1,object);

    do 
    {
	p = update[k];
	q->put(k,p->get(k));
	p->put(k,q);

    } while(--k>=0);

//cerr << "end of insert\n";

   return true;
}

template< class Key, class Obj >
//
bool SkipList<Key,Obj>::explicitRemove(const Obj &object)
{
  Array< SkipListNode<Key,Obj>* > update(MaxNumberOfLevels); 

  SkipListNode<Key,Obj> *p = _header, *q;
  int k = _level;

  do 
   {
     while ( q = p->get(k), q!=0 && object > q->object() ) p = q;
     update[k] = p;
 
   } while(--k>=0);

  if (q==0) return false;

  if (object == q->object())
   {
	for(k=0; k<=_level && (p=update[k])->get(k) == q; k++) 
         {
	  p->put(k,q->get(k));
         }
 
	delete q;

        while( _header->get(_level) == 0 && _level > 0 ) _level--;

	return true;
   }
  else return false;
}

template< class Key, class Obj > 
//
bool SkipList<Key,Obj>::remove(const Key &key)
{
//cerr << "removing " << key << nl;

  Array< SkipListNode<Key,Obj>* > update(MaxNumberOfLevels); 

  SkipListNode<Key,Obj> *p = _header, *q;
  int k = _level;

  do 
   {
     while ( q = p->get(k), q!=0 && q->object() < key ) p = q;
     update[k] = p;
 
   } while(--k>=0);

  if (q==0) return false;

  if ( q->object() == key )
   {
	for(k=0; k<=_level && (p=update[k], p->get(k) == q); k++) 
         {
	  p->put(k,q->get(k));
         }
 
	delete q;

        while( _header->get(_level) == 0 && _level > 0 ) _level--;

	return true;
   }
  else return false;
}

template< class Key, class Obj > 
//
bool SkipList<Key,Obj>::explicitSearch(const Obj &object) const
{
  SkipListNode<Key,Obj> *p = _header, *q;

  int k = _level;

  do 
  {
    while ( q = p->get(k), q!=0 && q->object() < object ) p = q;

  } while (--k>=0);

  if (q==0) return false;

  if (q->object() == object) return true;
  else 			     return false;

}

template< class Key, class Obj > 
//
bool SkipList<Key,Obj>::search(const Key &key, Obj &object) const
{
  SkipListNode<Key,Obj> *p = _header, *q;
  int k = _level;

  do 
  {
    while ( q = p->get(k), q!=0 && q->object() < key ) p = q;

  } while (--k>=0);

  if (q==0) return false;

  if (q->object() == key)
   { 
     object = q->object();
     return true;
   }
  else return false;

}

//-------------------------------------------------------------------------

#ifdef __xlC__
inline ostream& operator<<(ostream &o, const class Client &S) _iodefbody_
inline ostream& operator<<(ostream &o, const class GraphDictItem &S) _iodefbody_
#endif

template< class Key, class Obj >
//
ostream& operator<<(ostream &o, const SkipList<Key,Obj> &S)
{
 const SkipListNode<Key,Obj> *p = S._header;

 o << "{ ";

 p = p->next();	// header doesn't contain anything!

 while (p)
  {
   o << p->object();

   if (p->next()) o << ", "; p = p->next();
  }

 return o << " }";
}

#ifndef INLINE_SET

#include "search/nodenum.h"
#include "vacs/fs_server.h"

template class SkipList<int, Client>;
template class SkipList<NodeNumber, GraphDictItem>;

#endif

#endif
