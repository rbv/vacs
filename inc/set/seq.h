
// ------------------------------------------
// ------------------- seq.h ----------------
// ------------------------------------------

/*tex
 
\file{seq.h}
\path{inc/set}
\title{Header for seq classes}
\classes{}
\makeheader
 
xet*/

#ifndef _seq_h
#define _seq_h

#include "array/array.h"

template< class T >
class SortedSequence : public Array< T >
{

protected:

   using Array< T >::setSize;
   using Array< T >::setBaseAddr;

public:

   SortedSequence() {}

   ~SortedSequence() {}

   using Array< T >::resize;
   using Array< T >::resizeNoCopy;
   using Array< T >::shrink;
   using Array< T >::size;
   using Array< T >::get;
   using Array< T >::put;
   using Array< T >::index;
   using Array< T >::baseAddr;
   using Array< T >::shiftAndResizeLeft;
   using Array< T >::shiftAndResizeRight;

   void insert( const T& );

   void remove( const T& );

   bool empty() const
      { return size() == 0; }

   const T& min() const
      { return (*this)[ 0 ]; }

   const T& max() const
      { return (*this)[ size()-1 ]; }

   void delMax();

   void delMin();

   void clear()
      { resize(0); }

   // intersection
   //
   void join( const SortedSequence<T> &S1, const SortedSequence<T> &S2 );

   // union
   //
   void meet( const SortedSequence<T> &S1, const SortedSequence<T> &S2 );

   // comparision
   //
   bool operator==( const SortedSequence<T> &S ) const
   {
    return Array<T>::operator==( CAST( S, Array<T> ) );
   }
};

#ifdef INLINE_SET
#include "set/seq.c"
#endif

#endif
