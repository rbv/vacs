
/***********************************************************************
 * Header for sets with built in iterator.
 **********************************************************************/

/*tex

\file{iterset.h}
\path{inc/set}
\title{Header for sets (collections) with built in iterator.}
\classes{IterSet, PIterSet}
\makeheader

xet*/

#ifndef __iterset_h
#define __iterset_h

#include "set/set.h"

/*
 * Class controlled collection (with built-in iterator).
 */
template< class T >
//
class IterSet : public Collection<T>
 {

private:

   CollectionIter<T> *access;

public:

   /*
    * Constructors. 
    */ 
   //IterSet(const T &empty) : Collection<T>(empty)
   IterSet(const T &empty ) : Collection<T>(empty)
    {
      access = 0;
    }
   //
   IterSet(const T &empty, const T &x) : Collection<T>(empty)
    {
      add(x);
      access = 0;
    }

   IterSet() : Collection<T>(T(-1))
    {
      access = 0;
    }

   /*
    * Destructor.
    */
   ~IterSet() { if (access) delete access; }


   /*
    * Add a bit to assignment operator.
    */
   void operator=( const IterSet<T> &F )
    {
      Collection<T>::operator=( CAST( F, Collection<T> ) );
      if (access) 
       {
        delete access;
        access = 0;
       }
      //access = new CollectionIter<T>(F);
    }


   /*
    * Iterator member functions.
    */
   T operator()() const
      { return access->operator()(); }

   T prev() const
      { return access->prev(); }

   T next() const
      { return access->next(); }

   /*
    * Must start iterator before use.
    */
   void start() const
      { 
       if (access) access->start();
       else ( (IterSet<T>*) this ) -> access = new CollectionIter<T>(*this);
      }
   //
   void end() const
      { 
       if (!access) 
          ( (IterSet<T>*) this ) -> access = new CollectionIter<T>(*this);

       access->end();
      }

   void stop() const 
      { 
        if (access) delete ( (IterSet<T>*) this ) -> access; 
        ( (IterSet<T>*) this ) -> access = 0;
      }

   bool operator==(const IterSet<T> &F) const
   {
     return Collection<T>::operator==( CAST( F, Collection<T> ) );
   } 

#if 1
   void deleteIterItem()
   {
     assert(access);
     Collection<T>::deleteIterItem( *access );
   }

   void changeIterItem( const T &item )
   {
     assert(access);
     Collection<T>::changeIterItem( *access, item ); 
   }   
#endif

 };

/*
 * Class controlled pointer collection (with built-in iterator).
 */
template< class T >
//
class PIterSet : public PCollection<T>
 {
private:

   PCollectionIter<T> *access;

public:

   /*
    * Constructors. 
    */ 
   PIterSet()
   {
      access = 0;
   }
   //
   PIterSet(T *x)
   {
      this->add(x);
      access = 0;
   }

   /*
    * Destructor.
    */
   ~PIterSet() { if (access) delete access; }


   /*
    * Place unique items in PCollection C.
    */
   //void removeEqualities( PIterSet<T>& C )
   // {
   //   PCollection<T>::removeEqualities( C, eqTest );
   // }

   /*
    * Add a bit to assignment operator.
    */
   void operator=( const PIterSet<T> &F )
   {
      PCollection<T>::operator=( CAST( F, PCollection<T> ) );

      if (access) 
       {
        delete access;
        access = 0;
       }
      //access = new PCollectionIter<T>(F);
   }

   /*
    * Iterator member functions.
    */
   T* operator()() const
      { return access->operator()(); }

   T* prev() const
      { return access->prev(); }

   T* next() const
      { return access->next(); }

   /*
    * Must start iterator before use.
    */
   void start() const
   { 
       if (access) access->start();
       else ( (PIterSet<T>*) this ) -> access = new PCollectionIter<T>(*this);
   }
   //
   void end() const	// Warning -- see stop below.
   { 
       if (!access) 
          ( (PIterSet<T>*) this ) -> access = new PCollectionIter<T>(*this);

       access->end();
   }

   void stop() const 
   { 
        if (access) delete ( (PIterSet<T>*) this ) -> access;
        ( (PIterSet<T>*) this ) -> access = 0;
   }

   bool operator==(const PIterSet<T> &F) const
   {
     return PCollection<T>::operator==( CAST( F, PCollection<T> ) );
   } 

#if 1
   void deleteIterItem()
   {
     assert(access);
     PCollection<T>::deleteIterItem( *access );
   }

   void removeIterItem()
   {
     assert(access);
     PCollection<T>::removeIterItem( *access );
   }

   void changeIterItem( T *item )
   {
     assert(access);
     PCollection<T>::changeIterItem( *access, item ); 
   }
#endif

 };

//-------------------------------------------------------------------

template< class T >
inline ostream& operator<<( ostream &o, const IterSet<T> &C )
{
   return o << CAST( C, const Collection<T> );
}

template< class T >
inline ostream& operator<<( ostream &o, const PIterSet<T> &C )
{
   return o << CAST( C, const PCollection<T> );
}

template< class T >
inline istream& operator>>( istream &o, IterSet<T> &C )
{
   return o >> CAST( C, Collection<T> );
}

template< class T >
inline istream& operator>>( istream &o, PIterSet<T> &C )
{
   return o >> CAST( C, PCollection<T> );
}

#endif
