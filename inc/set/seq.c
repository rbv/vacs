
/*tex
 
\file{set.c}
\path{src/template}
\title{Main file for set classes}
\classes{}
\makeheader
 
xet*/

#ifndef _seq_c
#define _seq_c

#include "seq.h"

template< class T >
void SortedSequence<T>::delMax()
{
   resize( size() - 1 );
}

template< class T >
void SortedSequence<T>::delMin()
{
   shiftAndResizeLeft( 0, 1 );
}

template< class T >
void SortedSequence<T>::insert( const T& item )
{
   // find insert pos
   int l = size();
   int i; for (i=0; i<l; i++ )
      if ( (*this)[i] > item ) break;
   shiftAndResizeRight( i, 1 );
   put( i, item );
}

template< class T >
void SortedSequence<T>::remove( const T& item )
{
   // find delete position
   //
   int i = index(item);

   assert(i < size());

   shiftAndResizeLeft( i, 1 );
}

// intersection of two sequences
//
template< class T >
void SortedSequence<T>::join( const SortedSequence<T> &S1,
			      const SortedSequence<T> &S2 )
{
   int sz1 = S1.size(),
       sz2 = S2.size();

   resize( ::max(sz1,sz2) );  // just in case

   int i1 = 0, i2 = 0, sz = 0;
   //
   T item1, item2;
   //
   while ( i1<sz1 && i2<sz2 )
    {
      item1 = S1.get(i1);
      item2 = S2.get(i2);

      if (item1==item2) { put( sz++, item1 ); i1++; i2++; }
      else
      if (item1<item2)  i1++;
      else		i2++;
    }
  
   resize(sz);
}

// union of two sequences
//
template< class T >
void SortedSequence<T>::meet( const SortedSequence<T> &S1,
			      const SortedSequence<T> &S2 )
{
   int sz1 = S1.size(),
       sz2 = S2.size();

   resize(sz1+sz2);  // just in case

   int i1 = 0, i2 = 0, sz = 0;
   //
   T item1, item2;
   //
   while ( i1<sz1 && i2<sz2 )
    {
      item1 = S1.get(i1);	
      item2 = S2.get(i2);

      if (item1==item2) { put( sz++, item1 ); i1++; i2++; }
      else
      if (item1<item2)  { put( sz++, item1 ); i1++; }
      else		{ put( sz++, item2 ); i2++; }
    }
  
   // at most one of the following may be true.
   //
   if (i1<sz1) while ( i1<sz1 ) put( sz++, S1.get(i1++) );
   //
   if (i2<sz2) while ( i2<sz2 ) put( sz++, S1.get(i2++) );

   resize(sz);
}

#ifndef INLINE_SET

template class SortedSequence<short>;

#endif

#endif
