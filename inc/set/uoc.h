
// ------------------------------------------
// ------------------- uoc.h ----------------
// ------------------------------------------

/*tex
 
\file{uoc.h}
\path{inc/set}
\title{Header for set classes}
\classes{Collection}
\makeheader
 
xet*/


#ifndef _uoc_h
#define _uoc_h

#include <iostream>
#include "array/array.h"

//--------------------------------------------------------------

// cfront bug! Does not work yet.
/*
// Overloaded function that provides the default empty flag for the type.
// Define a similar function in each header, as required.
//
inline char  emptyFlag( char  ) { return -1; }
inline short emptyFlag( short ) { return -1; }
inline int   emptyFlag( int   ) { return -1; }
*/

template< class T > class CollectionIter;

template< class T >

class Collection : public Array<T>
{

	templated_iodeclarations( Collection )
private:

	static const int growSize = 16;

	// grow the collection by growSize elements
	//
	void grow();

protected:

	// number of used entries
	//
	int _used;

	// next free position (== ArrayVoidPtr::size() if none free)
	//
	int _nextFree;

	// empty flag
	//
	T _empty;

	// keep track of how many iterators are active on self
	//
	int _activeIters;

	// let PCollection access copy ctor

	// copy ctor
	//
	Collection( const Collection<T>& C )
	   : Array<T>( C )
	   { 
             _used = C._used;
             _nextFree = C._nextFree;
             _empty = C._empty;
             _activeIters = 0; 
           }

public:

   using Array<T>::resize;
   using Array<T>::resizeNoCopy;
   using Array<T>::shrink;
   using Array<T>::size;
   using Array<T>::get;
   using Array<T>::put;
   using Array<T>::baseAddr;
   using Array<T>::offsetAddr;
   using Array<T>::setSize;
   using Array<T>::setBaseAddr;
   using Array<T>::swap;
   using Array<T>::index;

/*
	// Constructor - use default empty flag
	//
	Collection();
*/

	// Constructor - use passed value as empty flag
	//
	Collection( const T& emptyFlag );


	// make sure that no iterators are active on self
	//
	~Collection()
	   { aassert( _activeIters == 0 ); }

	// Returns the element that is used to mark entries as empty
	//
	T emptyFlag() const
	   { return _empty; }

        // check if passed thing is in the set (use operator ==)
        //
        bool contains( const T& ) const;

	// adds object to the collection
	//
	void add( const T& );

	// adds unique object to the collection (returns true if successful)
	//
        bool addUnique( const T& );

        /*
         * Add a collection to current collection. (duplicates allowed)
         */
        void addSet( const Collection<T>& C );

        void addUniqueSet( const Collection<T>& C );

	// removes object from the collection
	// does not delete the object itself
	//
	void remove( const T& );
        //
        void purge( const T &a ) { remove(a); }

        /*
         * Remove/purge all items.  
         */
        void removeAll()
         {
          // not allowed to kill if any activists.
          //
          assert( _activeIters == 0 );

          _used = 0;
          _nextFree = 0;

          Array<T>::fill( emptyFlag() );
         }
        // 
        void purgeAll() { removeAll(); }

	// optimize storage
	//
	void compact();

	void operator=( const Collection<T>& C )
	{
	   Array<T>::operator=( C );
	   _used = C._used;
	   _empty = C._empty;
	   _nextFree = C._nextFree;

           _activeIters = 0; 
	}
        //
	void copy( const Collection<T>& C ) { operator=(C); }

        /* 
         * Unordered set isomorphism.
         */
	bool operator==( const Collection<T>& C ) const;

	// return the number of used elements.
	//
	int size() const { return _used; }

	// check if the collection is empty
	//
	bool isEmpty() const { return size() == 0; }

	// Convert to an array
	//
	Array<T> asArray() const;

#if 1
        /*
         * Methods that operate with an active iterator.
         */ 
        void deleteIterItem( CollectionIter<T> &I );

        void changeIterItem( CollectionIter<T> &I, const T &item );
#endif

	// These methods are for iterators only.
	// Should probably be private, with iterators friends, but
	// there's yet another cfront bug.
	//
	void newIter() const
	   { ( (Collection<T>*) this ) -> _activeIters++; }
	void delIter() const
	   { ( (Collection<T>*) this ) -> _activeIters--; }
	int arraySize() const
	   { return Array<T>::size(); }
        //
        int numIterators() const { return _activeIters; }


};

#ifdef __xlC__
template<class T>
inline istream& operator>>( istream& o, Collection<T>& ) _iodefbody_ 
#if 0
template<class T>
inline bostream& operator<<( bostream& o, Collection<T>& ) _iodefbody_ 
template<class T>
inline bistream& operator>>( bistream& o, Collection<T>& ) _iodefbody_ 
#endif
//template<class T>
//inline texstream& operator<<( texstream& o, Collection<T>& ) _iodefbody_ 
//template<class T>
//inline astream& operator<<( astream& o, Collection<T>& ) _iodefbody_ 
//template<class T>
//inline astream& operator>>( astream& o, Collection<T>& ) _iodefbody_
#endif

#ifdef INLINE_SET
#include "set/uoc.c"
#endif

#endif
