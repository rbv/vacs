
// ------------------------------------------
// ------------------- set.h ----------------
// ------------------------------------------

/*tex
 
\file{set.h}
\path{inc/set}
\title{Header for set classes}
\classes{Collection, PCollection, CollectionIter, PCollectionIter}
\makeheader
 
xet*/


#include "set/uoc.h"
#include "set/uocp.h"
#include "set/uociter.h"

