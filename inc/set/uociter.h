
// ------------------------------------------
// --------------- uociter.h ----------------
// ------------------------------------------

/*tex
 
\file{set.h}
\path{inc/set}
\title{Header for set iterorators} 
\classes{CollectionIter, PCollectionIter}
\makeheader
 
xet*/


#ifndef _uociter_h
#define _uociter_h

// #include "array/array.h"

#include "stdtypes.h"
#include "vacs/object.h"
#include "set/uoc.h"
#include "set/uocp.h"

//template< class T > class Collection;

//template< class T > class PCollection;


//--------------------------------------------------------------
//              Iterators
//--------------------------------------------------------------

// Collection iterators
//

template < class T >
class CollectionIter
{

private:

	// current position (last element returned) (-1 at start)
	//
	int pos;

	// current collection
	//
	const Collection<T>* coll;

public:

	CollectionIter( const Collection<T>& C );
	//   { coll = &C; pos = -1; C.newIter(); }

	~CollectionIter();
	//   { if (coll) coll->delIter(); }

	// Detatch the iterator from the collection.
	//
	void unlink();
        //   {  assert(coll); coll->delIter(); coll=0; }

	// returns next object, 0 if no more
	//
	T operator()();

	T prev();

	T next()
	   { return (*this)(); }

	void start() { pos = -1; }

	void end();

};

//--------------------------------------------------------------

// type-safe wrapper template for Collection iterators
// Only difference is op(), which returns a pointer to a T,
// rather than a ptr to a ptr to a T.

// public - should be private (cfront bug)


template<class T>
class PCollectionIter : public CollectionIter<T*>
{
public:

	PCollectionIter( const PCollection<T>& C );
	   //: CollectionIter<T*>( C ) {}

#if 1
	T* operator()()
	   { return CollectionIter<T*>::operator()(); }

	T* prev()
	   { return CollectionIter<T*>::prev(); }

	T* next()
	   { return CollectionIter<T*>::next(); }

	void start()
	   { CollectionIter<T*>::start(); }

	void end()
	   { CollectionIter<T*>::end(); }
#endif
};

//------------------------------------------------------------

#ifdef INLINE_SET
#include "set/uociter.c"
#endif

#endif
