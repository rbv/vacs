/*tex
\file{skiplist.h}
\path{inc/set}
\title{Header file for skiplist class}
\classes{}

Example of Skip List source code for C: (C++ version by mjd on 20-Oct-93) \\

Skip Lists are a probabilistic alternative to balanced trees, as
described in the June 1990 issue of CACM and were invented by 
William Pugh in 1987. 

\makeheader
xet*/

#ifndef SKIPLIST_H
#define SKIPLIST_H

#include <iostream>

#include "stdtypes.h"
#include "array/array.h"
//#include "array/parray.h"
#include "general/random.h"

template< class Key, class Obj > class SkipList;	// forward decl.

template< class Key, class Obj >
//
class SkipListNode // This class should be hidden from user?
{
private:
        friend class SkipList<Key,Obj>;

        Obj _object;

        Array<SkipListNode<Key,Obj>*> _forward;

        // Constructors, etc.
        //
	SkipListNode() : _forward(0) { aassert(false); } 

        SkipListNode(int n) : _forward(n) { } // For head of list only!

        SkipListNode(int n, const Obj &object) : _forward(n) 
        { 
          _object = object; 
        }

        ~SkipListNode() {}

        // Access methods
        //
	SkipListNode<Key,Obj>*& operator[](int i) { return _forward[i]; }

        SkipListNode<Key,Obj>* get( int i ) const { return _forward.get(i); }
    
        void put( int i, SkipListNode<Key,Obj> *p ) { _forward.put(i,p); }

        // for SkipList destructor
        //
        static void _internalDelete( SkipListNode<Key,Obj> *p );

public:
        const Obj& object() const { return _object; }
 
        const SkipListNode<Key,Obj>* next() const { return get(0); }
};

#if 0
template< class Key, class Obj >
//
ostream& operator<<(ostream &o, const SkipListNode<Key,Obj> &node)
{
  return o << node.object();
}
#endif

#if 1
/*
 * The parameter class Obj in SkipList<Key,Obj> must support the following:
 * 
 * bool operator==(const Key&) const { return false; }
 * bool operator<(const Key&) const { return false; }
 *
 * Observe the beauty of this when class Key == class Obj.
 *
 * Requirements:
 *
 * 1) Obj1 == Obj2 implies Key1 == Key2
 * 2) Obj1 <  Obj2 implies Key1 <= Key2
 * 3) Obj1 == Key2 implies Key1 == Key2
 * 4) Obj1 <  Key2 implies Key1 <  Key2
 *
 * The simpliest usage would be to have Obj's op== and op< use its internal key.
 * (Key's are not stored in our SkipList so duplicate memory wouldn't be used.)
 *
 */
#else
/*
 * If user wants to access SkipList via "keys" then the following class should
 * be a base class of something.
 */
template< class T >
//
class SkipListKey
{
public:

  SkipListKey() {}
  ~SkipListKey() {}

  virtual bool operator==(const T&) const { return false; }
  virtual bool operator>(const T&) const { return false; }
};
#endif

template< class Key, class Obj >
ostream& operator<<(ostream &o, const SkipList<Key,Obj> &S);

template< class Key, class Obj >
//
class SkipList
{
private:
	static int MaxNumberOfLevels;

        static RandomInteger RND;

        int randomLevel();

	SkipListNode<Key,Obj>* newNodeOfLevel(int l) 
        {
          return new SkipListNode<Key,Obj>(l);
        }

	SkipListNode<Key,Obj>* newNodeOfLevel(int l, const Obj &object) 
        {
          return new SkipListNode<Key,Obj>(l, object);
        }

	int _level;				// Maximum level used.

        SkipListNode<Key,Obj> *_header;		// Start of skip list.

public:
        SkipList();
  
        ~SkipList();

        // allow duplicates -- insert method
        //
        void insert(const Obj &object);

        // insert method ( returns false if already exists )
        //
        bool uniqueInsert(const Obj &object);

        // remove first item satisfying operator== 
        //
        bool explicitRemove( const Obj &object );

        // Function that returns -1 for <, 0 for ==, and 1 for >
        //
        //typedef int (*SearchPredFNC)(const Obj &object); 

        // remove first item satisfying predicate function
        //
        bool remove( const Key &key );

        // Check for an object
        //
        bool explicitSearch( const Obj &object ) const;

        // Find matching object (returns match in object)
        //
        bool search( const Key &key, Obj &object ) const;
        //
        bool grab( const Key &key, Obj &object )
            // deletes from list too.
         {
           if (search(key,object)) return explicitRemove(object);  // lazy!
           else 		   return false;
         }

        // access to sorted list -- use this then SkipListNode::next() 
        //
        const SkipListNode<Key,Obj>* head() const { return _header->get(0); }

        // query if empty
        //
        bool empty() const { return _header->get(0)==0; }

        // Not implemented
        //int size() const;

        // output
        //
        friend ostream& operator<< <>(ostream &o, const SkipList<Key,Obj> &S);
};

#ifdef INLINE_SET
#include "set/skiplist.c"
#endif

#endif

