 
// ------------------------------------------
// ----------------- uocp.c -----------------
// ------------------------------------------

/*tex
 
\file{uocp.c}
\path{src/template}
\title{Definitions for collection of pointers class}
\classes{PCollection}
\makeheader
 
xet*/

#ifndef _uocp_c
#define _uocp_c

#include <iostream>
#include "set/uocp.h"
#include "set/uociter.h"
#include "array/parray.h"

// -------------------------------------------------------------
//
//  Class PCollection

template< class T >
PArray<T> PCollection<T>::asPArray() const
{
   int sz = size();

   PArray<T> a( sz );

   int idx = 0;

   for (int i=0; i<arraySize(); i++)
   {
      if (get(i) != emptyFlag()) a[idx++] = get(i);
   }

   assert(idx==sz);

   return a;
}


#ifndef SUNPRO
// general instantiation causes an error because of abstract classes 
//
template< class T >
PCollection<T>::PCollection( const PCollection<T>& C ) : Collection<T*>( C )
 {
     for (int i=0; i<Array<T*>::size(); i++)
          if ( (*this)[i] ) (*this)[i] = new T( *(*this)[i] );
 }

template< class T >
void PCollection<T>::copy( const PCollection<T>& C ) 
{

#ifdef AUTODEL
   purgeAll();
#endif

   Collection<T*>::operator=( C );

   for (int i=0; i<Array<T*>::size(); i++)
    if ( (*this)[i] ) (*this)[i] = new T( *(*this)[i] );
}

template< class T > 
void PCollection<T>::addSetDeep( const PCollection<T> &C ) 
{ 
  for (int i=0; i< C.Array<T*>::size(); i++ ) 
   {
     if ( C[i] != _empty ) add( new T( *C[i] ) );
   }
}

template< class T > 
void PCollection<T>::addDeep( const T *a ) 
{ 
     add( new T( *a ) );
}

#endif /* non - SUNPRO */

template< class T >
void PCollection<T>::applyUnary( void (*unary)( T& ) )
{
   PCollectionIter<T> I( *this );

   T* p1;

   // go through the collection, applying unary
   //
   while ( p1 = I() )
   {
//cerr << "before \n"<< *p1 << nl;
      unary( *p1 );
//cerr << "after \n"<< *p1 << nl;
   }

}

template< class T >
bool PCollection<T>::addUnique( T *a )
{
 { // scope iterator.
   //
   PCollectionIter<T> I( *this );

   T* p1;

   // go through the collection checking for match
   //
   while ( p1 = I() )
   {
      if (*p1 == *a) return false;
   }
 }

   add(a);
   return true;
}

template< class T >
bool PCollection<T>::addUnique( 
  T *a,
  bool (*equalityTest)( const T&, const T& )
)
{
 { // scope iterator.
   //
   PCollectionIter<T> I( *this );

   T* p1;

   // go through the collection checking for match
   //
   while ( p1 = I() )
   {
      if (equalityTest(*p1,*a)) return false;
   }
 }

   add(a);
   return true;
}


template< class T >
bool PCollection<T>::contains( const T *a ) const
{
 { // scope iterator.
   //
   PCollectionIter<T> I( *this );

   T* p1;

   // go through the collection checking for match
   //
   while ( p1 = I() )
   {
      if (*p1 == *a) return true;
   }
 }

   return false;
}


template< class T >
void PCollection<T>::removeEqualities(
   PCollection<T>& C,
   bool (*equalityTest)( const T&, const T& )
)
{
   assert( C.isEmpty() );

   PCollectionIter<T> I1( *this );

   T* p1;

   // go through the collection
   //
   while ( p1 = I1() )
   {
      bool found = false;

      { // scope the iterator for C...can't add new element while it exists

         // check *p1 against contents of C
         //
         PCollectionIter<T> I2( C );

         T* p2;

         while ( p2 = I2() )
         {
            if ( equalityTest( *p1, *p2 ) )
            {
               found = true;
               break;
            }
         }

      } // iterator scope

      if ( ! found ) C.addDeep( p1 );

   }

   return;
}

#ifdef SUNPRO
// lazy for now -- definition for the delete 
//#include "graph/ltree.h"
#endif

template< class T >
void PCollection<T>::removeEqualities(
   bool (*equalityTest)( const T&, const T& )
)
{
   assert(_activeIters == 0);

   int sz = arraySize();
   int isoNum = 0;

   for (int i=0; i<sz-1; i++)
    {
     if ( get(i) == emptyFlag() ) 
      {
        swap(i,sz-1);
        sz--;
        i--;
        continue;
      }

     for (int j=i+1; j<sz; j++)
      {
       if ( get(j) == emptyFlag() ) 
        {
          swap(j,sz-1);
          sz--;
          j--;
          continue;
        }

       if ( equalityTest( *(get(i)),  *(get(j)) ) )
        {
          swap(j,sz-1);
          sz--;
          j--;
          isoNum++;
        }
      }

    } // i

   _used -= isoNum;
   _nextFree = sz;

   //mjd fix ??
   //
   for (int i=sz; i<arraySize(); i++)
    {
      if ( get(i) != emptyFlag() ) 
       {
         delete get(i);
         put(i,emptyFlag());
       }
    }

   return;
}

// operator== version of the above
//
template< class T >
void PCollection<T>::removeEqualities( )
{
//cerr << "in removeEqualities()\n";
   assert(_activeIters == 0);

   int sz = arraySize();
   int isoNum = 0;

   for (int i=0; i<sz-1; i++)
    {
     if ( get(i) == emptyFlag() ) 
      {
        swap(i,sz-1);
        sz--;
        i--;
        continue;
      }

     for (int j=i+1; j<sz; j++)
      {
       if ( get(j) == emptyFlag() ) 
        {
          swap(j,sz-1);
          sz--;
          j--;
          continue;
        }

//cerr << "equalityTest" << *get(i) << " and " << *get(j) <<nl;

       //if ( equalityTest( *(get(i)),  *(get(j)) ) )
       if ( *get(i) == *get(j) )
        {
//cerr << "is true\n";
          swap(j,sz-1);
          sz--;
          j--;
          isoNum++;
        }
      }

    } // i

   _used -= isoNum;
   _nextFree = sz;

   //mjd fix ??
   //
   for (int i=sz; i<arraySize(); i++)
    {
      if ( get(i) != emptyFlag() ) 
       {
         delete get(i);
         put(i,emptyFlag());
       }
    }

   return;
}


template< class T >
void PCollection<T>::purge( T* a )
 {
   Collection<T*>::remove( a );
   delete a;
 }


template< class T >
void PCollection<T>::deleteAll()
 {
   PCollectionIter<T> I( *this );
 
   T* p1;
 
   // go through the collection, applying unary
   //
   while ( p1 = I() ) delete p1;
}


/*
 * Isomorphism member functions.
 */
template< class T >
bool PCollection<T>::operator==( const PCollection<T> &C ) const
 {
   if ( size() != C.size() ) return false;

   int a2sz = C.Array<T*>::size();

   Array< int > mapped( a2sz );
   mapped.fill(-1);

   for (int i=0; i < Array<T*>::size(); i++)
    {
     if ( get(i) == emptyFlag() ) continue;

     bool found = false;
     //
     for (int j=0; j < a2sz; j++)
      {
        if ( mapped[j] != -1 || C.get(j) == emptyFlag() ) continue;

//cerr << "template ==:\n" << *(get(i)) << nl << *(C.get(j)) << nl;

        if ( *(get(i)) == *(C.get(j)) )		// Hope this is defined!
         {
           mapped[j] = i;
           found = true;
           break; 				// j loop
         }
      }

     if ( found == false ) return false;

    }

  return true;
 }

template< class T >
bool PCollection<T>::isomorphic( const PCollection<T> &C,
			bool (*equalityTest)( const T&, const T& ) ) const
 {
   if ( size() != C.size() ) return false;

   int a2sz = C.Array<T*>::size();

   Array< int > mapped( a2sz );
   mapped.fill(-1);

   for (int i=0; i < Array<T*>::size(); i++)
    {
     if ( get(i) == emptyFlag() ) continue;

     bool found = false;
     //
     for (int j=0; j < a2sz; j++)
      {
        if ( mapped[j] != -1 || C.get(j) == emptyFlag() ) continue;

        if ( equalityTest(*(get(i)), *(C.get(j))) )
         {
           mapped[j] = i;
           found = true;
           break;
         }
      }

     if ( found == false ) return false;

    }

  return true;
 }

template< class T >
void PCollection<T>::deleteIterItem( PCollectionIter<T> &I )
{
  int pos = CAST(I,int);
  assert(pos >= 0 && pos < arraySize());

  delete (*this)[pos];
  (*this)[pos] = 0;

  _nextFree = min( _nextFree, pos );
  _used--;
}

template< class T >
void PCollection<T>::removeIterItem( PCollectionIter<T> &I )
{
  int pos = CAST(I,int);
  assert(pos >= 0 && pos < arraySize());

  //delete (*this)[pos];
  (*this)[pos] = 0;

  _nextFree = min( _nextFree, pos );
  _used--;
}

template< class T >
void PCollection<T>::changeIterItem( PCollectionIter<T> &I, T *item)
{
  int pos = CAST(I,int);
  assert(pos >= 0 && pos < arraySize());

  assert(item != 0);

  delete (*this)[pos];
  (*this)[pos] = item;
}

// -------------------------------------------------------------------------

// Template IO
//
template< class T >
ostream& operator<<( ostream& o, const PCollection<T>& C )
{
   PCollectionIter<T> I( C );
 
   o << '{';
 
   if ( C.size() > 0 )
      o << *I();
 
   T* p;
   while ( (p = I()) != C.emptyFlag() )
      o << ',' << *p;
 
   o << '}';
 
   return o;
}
#if 0
template< class T >
bostream& operator<<( bostream& o, const PCollection<T>& C )
{
   o << C.size();
   PCollectionIter<T> I( C );
   T* p;
   while ( (p = I()) != C.emptyFlag() )
      o << *p;
   return o;
}
template< class T >
bistream& operator>>( bistream& o, PCollection<T>& C )
{
   PCollectionIter<T> I( C );
   int size;
   o >> size;
   T* p;
   for (int i=0; i<size; i++ )
   {
     p = new T;
     o >> *p;
   }
   return o;
}
#endif

#if 0
template< class T >
ostream& textOut( ostream& o, const PCollection<T>& C, const char* separator )
{
   PCollectionIter<T> I( C );
 
   if ( C.size() > 0 )
      o << *I();
 
   T* p;
   while ( (p = I()) != C.emptyFlag() )
      o << separator << *p;
 
   return o;
}
#endif

#ifndef INLINE_SET

#include "graph/tree.h"
#include "graph/ltree.h"
#include "graph/indgrph.h"
#include "bpg/bpg.h"
#include "bpg/algorithm/pathwidth_b.h"

// Instantiations

template class PCollection<Tree>;
template class PCollection<LTree>;
template class PCollection<PIterSet<LTree> >;
template class PCollection<InducedGraph>;
template class PCollection<PWChar>;
template class PCollection<NBBPG>;
template class PCollectionIter<NBBPG>;

// operator overloads have to be instantiated separately, because they're not part of the class!

template ostream& operator<< <PIterSet<LTree> >(ostream&, const PCollection<PIterSet<LTree> >&);

#endif

#endif
