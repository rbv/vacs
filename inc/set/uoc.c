 
// ------------------------------------------
// ------------------ uoc.c -----------------
// ------------------------------------------

/*tex
 
\file{uoc.c}
\path{src/template}
\title{Definitions for collection class}
\classes{Collection}
\makeheader
 
xet*/

#ifndef _uoc_c
#define _uoc_c


#include <iostream>
#include "set/uoc.h"
#include "set/uociter.h"

// Class Collection definitions

/*
// constructor
//
template< class T >
Collection<T>::Collection()
: Array<T>()
{
   T t; // create a default T to pass to the emptyFlag function
   _empty = emptyFlag( t );

   _used = 0;
   _nextFree = 0;
   _activeIters = 0;
}
*/


// constructor
//
template< class T >
Collection<T>::Collection( const T& emptyFlag )
: Array<T>()
{
   _empty = (T) emptyFlag;
   _used = 0;
   _nextFree = 0;
   _activeIters = 0;
}


template< class T >
void Collection<T>::add( const T& a )
{
   assert( a != _empty );

   // not allowed to add while an iterator is active
   //
   assert( _activeIters == 0 );

   if ( _nextFree == Array<T>::size() )
   {
      // no room!
      grow();
   }

   (*this)[ _nextFree ] = (T) a;
   _used++;

   // update _nextFree
   while ( _nextFree < Array<T>::size() && (*this)[_nextFree] != _empty )
      _nextFree++;

} 

template< class T >
bool Collection<T>::addUnique( const T& a )
{
   if (index(a) < arraySize()) return false;
   else
    {
     add(a);
     return true;
    }
} 


template< class T > 
void Collection<T>::addSet( const Collection<T> &C ) 
{ 
  for (int i=0; i< C.Array<T>::size(); i++ ) 
   {
     if ( C[i] != _empty ) add(C[i]);
   }
}

template< class T > 
void Collection<T>::addUniqueSet( const Collection<T> &C ) 
{ 
  for (int i=0; i< C.Array<T>::size(); i++ ) 
   {
     if ( C[i] != _empty ) addUnique(C[i]);
   }
}

template< class T >
void Collection<T>::remove( const T& a )
{
   assert( a != _empty );

   for (int i=0; i<Array<T>::size(); i++ )
   {
      if ( (*this)[i] == a )
      {
         (*this)[i] = _empty;
         _nextFree = min( _nextFree, i ); 
         _used--;
      }
   }

   // not found - do nothing
}


template< class T >
void Collection<T>::grow()
{
   int oldSize = Array<T>::size();

   // make it bigger
   //
   resize( oldSize + growSize );

   // fill new part with nulls
   //
   for (int i=oldSize; i<Array<T>::size(); i++ )
      (*this)[i] = _empty;
}


template< class T >
void Collection<T>::compact()
{
   int i = 0;
   int j = 0;

   while ( j < Array<T>::size() )
   {
      if ( (*this)[j] != _empty )
         (*this)[i++] = (*this)[j];

      j++;
   }

   // resize to have room for 0..i-1
   resize( i );

}


template< class T >
Array<T> Collection<T>::asArray() const
{
   int sz = size();

   Array<T> a( sz );

   CollectionIter<T> I( *this );
   int i = 0;

   while( i < sz )
   {
      a[i++] = I();
   }

   assert( i == sz );

   return a;
}

/*
 * Unordered set isomorphism.
 */
template< class T >
bool Collection<T>::operator==( const Collection<T>& C ) const
 {
   if ( size() != C.size() ) return false;

   int a2sz = C.Array<T>::size();

   Array< int > mapped( a2sz ); 
   mapped.fill(-1);

   for (int i=0; i < Array<T>::size(); i++)
    {
     if ( get(i) == _empty ) continue;

     bool found = false;
     //
     for (int j=0; j < a2sz; j++)
      {
        if ( mapped[j] != -1 || C.get(j) == _empty ) continue;

        if ( get(i) == C.get(j) )
         { 
           mapped[j] = i;
           found = true;
           break;
         }
      }

     if ( found == false ) return false;

    }

  return true;
 }

template< class T >
bool Collection<T>::contains( const T &x ) const
{
   CollectionIter<T> I( *this );

   for (int i=0; i<size(); i++)
   {
      if (I() == x) return true;
   }

   return false;
}

#if 1
template< class T >
void Collection<T>::deleteIterItem( CollectionIter<T> &I ) 
{
  int pos = CAST(I,int);
  assert(pos >= 0 && pos < arraySize());

  (*this)[pos] = _empty;
  _nextFree = min( _nextFree, pos ); 
  _used--;
}

template< class T >
void Collection<T>::changeIterItem( CollectionIter<T> &I, const T &item ) 
{
  int pos = CAST(I,int);
  assert(pos >= 0 && pos < arraySize());

  assert(item != _empty);
  (*this)[pos] = item;
}
#endif

//--------------------------------------------------------

#if 0
template< class T >
ostream& textOut( ostream& o, const Collection<T>& C, const char* separator )
{
   CollectionIter<T> I( C );

   if ( C.size() > 0 )
      o << I();

   T p;
   //
   while ( (p = I()) != C.emptyFlag() ) o << separator << p;

   return o;
}
#endif

template< class T >
ostream& operator<<( ostream& o, const Collection<T>& C )
{
   CollectionIter<T> I( C );
 
   o << '{';

   if ( C.size() > 0 )
      o << I();

   T p;
   //
   while ( (p = I()) != C.emptyFlag() ) o << ',' << p;

   o << '}';

   return o;
}

#ifndef INLINE_SET

#include "bpg/bpg.h"

template class Collection<short>;
template class Collection<NBBPG*>;
template class CollectionIter<NBBPG*>;

#endif

#endif
