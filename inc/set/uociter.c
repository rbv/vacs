 
// ------------------------------------------
// ---------------- uociter.c ---------------
// ------------------------------------------

/*tex
 
\file{uociter.c}
\path{src/template}
\title{Definitions for collection iterator classes}
\classes{CollectionIterator, PCollectionIterator}
\makeheader
 
xet*/

#ifndef _uociter_c
#define _uociter_c

#include "array/array.h"
#include "set/uociter.h"

// ------------------------------------------

// --- Iterator for Collection

template< class T >
CollectionIter<T>::CollectionIter( const Collection<T> &C )
{ 
   coll = &C; pos = -1; C.newIter(); 
}

template< class T >
CollectionIter<T>::~CollectionIter()
{ 
   if (coll) coll->delIter(); 
}

template< class T >
void CollectionIter<T>::unlink()
{ 
   assert(coll); coll->delIter(); coll=0; 
}

template< class T >
T CollectionIter<T>::operator()()
{
   assert( coll );

   T e = coll -> emptyFlag();

   int oldPos = pos;

   // find next element
   //
   pos++;
   //
   for ( ; pos < coll->Array<T>::size(); pos++ )
   {
      if ( (*coll)[pos] != e ) return (*coll)[pos];
   }

   if ( pos >= coll->Array<T>::size() ) // ran off end?
      pos = oldPos;  // restore pos

      //pos = coll->Array<T>::size(); // old code
   
   return e;
}


template< class T >
void CollectionIter<T>::end()
{
   assert( coll );
   pos = coll->Array<T>::size();
}


template< class T >
T CollectionIter<T>::prev()
{
   assert( coll );

   T e = coll -> emptyFlag();

   // find next element
   //
   pos--;
   for ( ; pos >= 0; pos-- )
   {
      if ( (*coll)[pos] != e ) return (*coll)[pos];
   }

   if ( pos < 0 ) pos = -1;
   
   return e;
}

template< class T >
PCollectionIter<T>::PCollectionIter( const PCollection<T>& C )
  : CollectionIter<T*>( C ) {}

#endif
