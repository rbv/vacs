
// ------------------------------------------
// ------------------ dict.h ----------------
// ------------------------------------------

/*tex
 
\file{dict.h}
\path{inc/set}
\title{Header for dictionary classes}
\classes{}
\makeheader
 
xet*/

#ifndef _dict_h
#define _dict_h

#include "array/array.h"

template< class Key, class Dat >
class Dictionary
{
#if 1 
   // cfront bug!
   //friend class ostream& operator<<( ostream&, const Dictionary<Key,Dat>& );	
   //friend class bostream& operator<<( bostream&, const Dictionary<Key,Dat>& );
   //friend class bistream& operator>>( bistream&, Dictionary<Key,Dat>& );

public:
   void out( ostream& ) const;
   void out( bostream& ) const;
   void in( bistream& );
   void out( astream& ) const;
   void in( astream& );

#endif

private:

   Array< Key > keys;
   Array< Dat > dats;

   int pos( const Key& ) const;

public:

   Dictionary();

   void insert( const Key&, const Dat& );

   const Dat& get( const Key& ) const;

   void del( const Key& );

   bool defined( const Key& ) const;

   int size() const
      { return keys.size(); }

   void reverse();

   // for iterator
   //
   int iterInit( Key& ) const;
   void iterNext( int&, Key& ) const;
   bool iterActive(int) const;

};

#define ForAllDictionaryItems( dict, key )	\
   for( int _di=dict.iterInit(key); dict.iterActive(_di); dict.iterNext(_di,key) )


#ifdef INLINE_SET
#include "set/dict.c"
#endif

#endif
