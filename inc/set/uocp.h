
// ------------------------------------------
// ------------------- uocp.h ----------------
// ------------------------------------------

/*tex
 
\file{uocp.h}
\path{inc/set}
\title{Header for set classes}
\classes{PCollection}
\makeheader
 
xet*/


#ifndef _uocp_h
#define _uocp_h

#include "stdtypes.h"

// template <class T> class Collection;
// template <class T> class Array;
template <class T> class PArray;

#include "set/uoc.h"



#define AUTODEL

// Type-safe wrapper for PCollection.
// The inline function here just perform typecasting,
// except for I/O.

// Derivation should be private, but cfront crashes.


//templated_pre_iodeclarations( PCollection )

template< class T > class PCollectionIter;

template< class T >

class PCollection : public Collection<T*>
{
private:

   templated_iodeclarations( PCollection )

protected: //  kev

	// copy ctor
	//
	PCollection( const PCollection<T>& C );

    using Collection<T*>::_used;
    using Collection<T*>::_nextFree;
    using Collection<T*>::_empty;
    using Collection<T*>::_activeIters;

public:

	PCollection() : Collection<T*>( (T*) 0 ) {}


#ifdef AUTODEL
	/*
	 * Destructor with recursive delete option.
         */
	~PCollection( )
          {
		  deleteAll();
	  };
#endif

	using Collection<T*>::arraySize;
	using Collection<T*>::get;
	using Collection<T*>::put;
	using Collection<T*>::swap;

	T* emptyFlag() const
	   { return 0; }

	int size() const
	   { return Collection<T*>::size(); }

	bool isEmpty() const
	   { return size() == 0; }

        // Convert to a PArray
        //
        PArray<T> asPArray() const;

        /*
	 * Add item(s).  Collection now owns and will delete by destructor.
	 */
	void add( T* a )
	   { Collection<T*>::add( a ); }

        /*
	 * Add item(s).  Collection now owns it's own copy.
	 */
	void addDeep( const T *a );
	   // move to .c if needed { Collection<T*>::add( new T(*a) ); }

        // check if passed thing is in the set (use operator ==)
        //
        bool contains( const T *a ) const;

        // add unique item (returns true if successful)
        //
        bool addUnique( T *a );
        bool addUnique( T *a, bool (*equalityTest)( const T&, const T& ) );


        void addSet( const Collection<T*>& C )
	   { Collection<T*>::addSet( C ); }

        // deep copy a set of pointers
        //
        void addSetDeep( const PCollection<T>& );


	void remove( T* a )			// give up ownership.
	   { Collection<T*>::remove( a ); }
        //
        void purge( T* a );			// deletes match.

	void compact()
	   { Collection<T*>::compact(); }

#ifdef AUTODEL
protected:
/*
 * restricted for auto delete mode.
 */
#endif

	// Delete all pointers.  Warning! Does not remove from collection.
	//
	void deleteAll();		// applyUnary( delete );

public:

        /*
         * Remove all items.  Warning! Does not return them to free store.
         */
// base class is satisfactory ?
//#if 0
        void removeAll()
         {
           Collection<T*>::removeAll();

           Array<T*>::fill( emptyFlag() );
         }
	//#endif

        /*
         * Delete all pointers and reset to empty collection.
         */
        void purgeAll()
         {
	  deleteAll(); 	// deletes

          removeAll();	// sets pointers to emptyFlag
         }

#ifdef AUTODEL

/*
 *  operator=() and copy() are both equivalently deep.
 */
	void operator=( const PCollection<T>& C ) 
         { 
           copy( C ); 
           _activeIters = 0;
         }

#else
	// Not a deep copy - pointers only
	//
	void operator=( const PCollection<T>& C )
	   { Collection<T*>::operator=(C); }

#endif
        
        /*
         * Semi-deep copy.  ( Does not delete previous collection items. )
	 * [ deletes if AUTODEL is defined ]
         */
	void copy( const PCollection<T>& C );
     

	// Passed an empty PUOC C, this method deep-copies *objects* from this
	// to C, first checking for presence in C using equalityTest.
	// In essence, make C a set from this.
	// Typically, the test is something like isomorphism.
	// 
	void removeEqualities( PCollection<T>& C, 
		               bool (*equalityTest)( const T&, const T& ) 
	);

        // inplace version of the above.
        //
	void removeEqualities( bool (*equalityTest)( const T&, const T& ));

        // operator==(const T&, const T&) version of the above;
        //
	void removeEqualities( );

        /*
         * Isomorphism should be explicit with element "equalityTest" if
	 * default element operator== shouldn't be used.
         */
        bool operator==( const PCollection<T> &C ) const;
        //
        bool isomorphic( const PCollection<T>& C,
	        	  bool (*equalityTest)( const T&, const T& ) ) const;

	// Apply the function to each element of the collection.
	//
	void applyUnary( void (*unary)( T& ) );

#if 1
        /*
         * Methods that operate with an active iterator.
         */
        void deleteIterItem( PCollectionIter<T>& );
        void removeIterItem( PCollectionIter<T>& );

        void changeIterItem( PCollectionIter<T>&, T *a );
#endif

};

#ifdef __xlC__
template<class T>
inline istream& operator>>( istream& o, PCollection<T>& ) _iodefbody_
#if 0
template<class T>
inline texstream& operator<<( texstream& o, PCollection<T>& ) _iodefbody_
template<class T>
inline bostream& operator<<( bostream& o, PCollection<T>& ) _iodefbody_
template<class T>
inline bistream& operator>>( bistream& o, PCollection<T>& ) _iodefbody_
template<class T>
inline astream& operator<<( astream& o, PCollection<T>& ) _iodefbody_
template<class T>
inline astream& operator>>( astream& o, PCollection<T>& ) _iodefbody_
#endif
#endif

#ifdef INLINE_SET
#include "set/uocp.c"
#endif

#endif
