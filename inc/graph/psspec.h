
/***********************************************************************
 * Header for PostScript output customization class.
 **********************************************************************/

/*tex

\file{psspec.h}
\path{inc}
\title{Header for general purpose postscript drawers}
\classes{ PSSpec }
\makeheader

xet*/

#ifndef __psspec_h
#define __psspec_h

#include <iostream>

#include "graph/stdgraph.h"
#include "general/str.h"

/* 
 * -----------------------------------------------------------------
 * Optional specifications/options passed to graph object drawers.
 * -----------------------------------------------------------------
 *
 * Note: Some drawers may not use all of these options.
 *
 */

//template < class GObject >	// class PSSpec<GObject> ...

class PSSpec
 { 
 
public: 
 
  // PostScript options
  //
  enum PSNodeType { WhiteCircle=0, BlackCircle=1, GrayCircle=2, 
		    WhiteSquare=3, BlackSquare=4, GraySquare=5 }; 
   
  enum PSEdgeType { Normal=0, Dashed=1, Bold=2, Squiggly=3,  
                    RightArrow=4, LeftArrow=5, BothArrow=6,
                    Dotted=7, BoldDotted=8, BoldDashed=9 }; 
 
  enum PSFormat { BoundingBox=0, DviSpecial=1, Poster=2 }; 
 
  // Callback functions
  //
  typedef Str (*NodeStrFnc)(vertNum, const void *Obj = 0); 

  typedef PSNodeType (*NodeTypeFnc)(vertNum, const void *Obj = 0); 

  typedef Str (*EdgeStrFnc)(vertNum, vertNum, const void *Obj = 0); 

  typedef PSEdgeType (*EdgeTypeFnc)(vertNum, vertNum, const void *Obj = 0); 

private: 
 
  /* 
   * If the following are not set (assigned) then defaults are used. 
   */ 
  // 
  NodeStrFnc _nodeStr; 
  // 
  NodeTypeFnc _nodeType; 
  // 
  EdgeStrFnc _edgeStr; 
  // 
  EdgeTypeFnc _edgeType; 
 
  /* 
   * Some optional headings/trailers.  
   * (hack until object is designed to know these things) 
   */ 
  const Str *_title;		/* Something on top of figure. */ 
   
  const Str *_caption;		/* Something under figure. */ 
 
  ushort _width, _height;	/* For bounding box. */ 
 
  /** 
   ** Default output format 
   **/ 
  PSFormat _format; 
 
  ostream *_out;
 
  int _objectOptions;		// Can be used for a graph object that
			        // has its own unique options (usually
				// by setting various bits)

public: 
 
  /* 
   * Constructors 
   */ 
  PSSpec( ushort width = 400, ushort height = 400,  
          PSFormat format = BoundingBox ) 
   { 
    _nodeStr = 0; 
    _nodeType = 0; 
    _edgeStr = 0; 
    _edgeType = 0; 
    _title = 0; 
    _caption = 0; 
 
    _width = width; 
    _height = height; 
 
    _format = format; 

    _out = 0;

    _objectOptions = 0;
   } 
 
  /* 
   * Destructor 
   */ 
  ~PSSpec() {} 
 
  /* 
   * Set methods. 
   */ 
  void setNodeStrFnc( NodeStrFnc f ) 
   { 
    _nodeStr = f; 
   } 
 
  void setNodeTypeFnc( NodeTypeFnc f ) 
   { 
    _nodeType = f;   
   }    
   
  void setEdgeStrFnc( EdgeStrFnc f ) 
   { 
    _edgeStr = f; 
   } 
 
  void setEdgeTypeFnc( EdgeTypeFnc f ) 
   { 
    _edgeType = f; 
   } 
 
  void setTitle( const Str &title ) { _title = &title; } 
   
  void setCaption( const Str &caption ) { _caption = &caption; } 
 
  void setWidth( ushort width ) { _width = width; } 
 
  void setHeight( ushort height ) { _height = height; } 
 
  void setFormat( PSFormat format ) { _format = format; } 
 
  void setOut(ostream &out) { _out = &out; } 

  void setSetOptions( int options ) { _objectOptions = options; } 
 
  /* 
   * Access methods. 
   */  
  NodeStrFnc getNodeStrFnc() const
   { 
    return _nodeStr; 
   } 
 
  NodeTypeFnc getNodeTypeFnc() const
   { 
    return _nodeType;   
   }    
 
  EdgeStrFnc getEdgeStrFnc() const
   { 
    return _edgeStr; 
   } 
 
  EdgeTypeFnc getEdgeTypeFnc() const
   { 
    return _edgeType; 
   } 
 
  const Str* getTitle() const { return _title; } 
  
  const Str* getCaption() const { return _caption; } 
 
  ushort getWidth() const { return _width; } 
 
  ushort getHeight() const { return _height; } 
 
  PSFormat getFormat() const { return _format; } 
 
  ostream* getOut() const { return _out; } 
 
  int getOptions() const { return _objectOptions; } 
 
  /* 
   * Access pass-thru methods. 
   */  
  Str drawNodeStr(vertNum v, const void *Obj = 0) const
   { 
    if (_nodeStr) return getNodeStrFnc()(v,Obj); 
    else 	  return Str(); 
   } 
 
  PSNodeType drawNodeType(vertNum v, const void *Obj = 0) const
   { 
    if (_nodeType) return getNodeTypeFnc()(v,Obj);   
    else	   return WhiteCircle; 
   }    
 
  Str drawEdgeStr(vertNum u, vertNum v, const void *Obj = 0) const
   { 
    if (_edgeStr) return getEdgeStrFnc()(u,v,Obj); 
    else	  return Str();
   } 
 
  PSEdgeType drawEdgeType(vertNum u, vertNum v, const void *Obj = 0) const
   { 
    if (_edgeType) return getEdgeTypeFnc()(u,v,Obj); 
    else	   return Normal; 
   } 

  static void copyTexLib( ostream& );

  static void copyPsLib( ostream& );

 }; 
 
/*
 * Drawing functions that can use PSSpec.
 */
class Tree;
void tree2ps( const Tree &T, const PSSpec *spec = 0);

class Graph;
void graph2ps( const Graph &G, const PSSpec *spec = 0);

class BPG;
void bpg2ps( const BPG &B, const PSSpec *spec = 0, const void *callObj = 0 );

#endif 
