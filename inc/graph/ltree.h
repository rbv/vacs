
/***********************************************************************
 * Header for Labeled Trees.
 **********************************************************************/

/*tex

\file{ltree.h}
\path{inc/graph}
\title{Header for Labeled Trees}
\classes{LTree}
\makeheader

xet*/

#ifndef __ltree_h
#define __ltree_h

#include "graph/tree.h"
#include "array/sortarray.h"
#include "general/str.h"

typedef SortableArray< vertNum > LTreeRank;

//-------------------------------------------------------------- 
// Template version for adding generic labels (not implemented)
//-------------------------------------------------------------- 

//template<class Label>
//
typedef vertNum Label;
//
class LTree : private Tree
{

private:

  DyArray<Label> labels;	

public:

  static const Label NoLabel; // = 0xFF;

#if 0
  LTree(int space=0) : Tree(space), labels(1,space)  // Constructor: K1 #1
  { }
#endif
  
  LTree(const LTree &T) : Tree((const Tree&) T), labels(T.labels) {}

  LTree(const Label &rootLabel, int space=0) : 	// Constructor: labeled K1
    Tree(space), labels(1,space)
     { labels[0]=rootLabel; }

  /*
   * Binary type constructor with attached root.
   */
  LTree(const LTree &T1, const LTree &T2, const Label &rootLabel);
  //
  LTree(const LTree &T1, const LTree &T2, vertNum u, vertNum v); // Via edge.


  /*
   * Promote a unlabeled tree with default label.
   */
  LTree(const Tree &T, const Label &dflt) : Tree(T), labels(T.order())
   { 
    setAllLabels(dflt); 
   }

  /*
   * Same as above but with individual labels.
   */
  LTree(const Tree &T, const DyArray< Label > &L) : Tree(T) , labels(L) {}


  /*
   * Spanning tree for component containing vertex v.
   *
   * Note: There exists an LGraph( const class BPG& ) constructor.
   */
  LTree(const class LGraph &G, vertNum v=0);

  ~LTree() {}


  /*
   * Operations on vertices (nodes)
   */
  vertNum addLeaf(vertNum v, const Label &l); // Add leaf to tree.
  //
  void rmLeaf(vertNum v);                // Remove a vertex.
  //
  LForest* rmNode(vertNum v) const;       // Breakup tree into a forest.

  /*
   * Operations on edges.
   */  
  
  // Contract edge in tree.
  //
  void contract(vertNum &u, vertNum v, const Label &l);    

  // Split and edge into two.
  //
  vertNum subdivide(vertNum, vertNum, const Label &l);  

  /*
   * Basic query functions promoted from base class Tree.
   */
  //
  Tree::isEdge;				// SEE CLASS TREE FOR INFO.
  //
  Tree::isNode;
  //
  Tree::isLeaf;
  //
  Tree::leafParent;
  //
  Tree::neighbor;
  //
  Tree::getPath;
  //
  Tree::degree;
  //
  Tree::nodes;
  //
  Tree::order;
  //
  Tree::size;
  //
  Tree::edges;

  /*
   * Binary operators.
   */
  bool operator==(const LTree &T) const;  // Isomorphism check.
  // 
  LTree& operator=(const LTree &T);       // Bitwise copy.

  /*
   * Multi-query operations.
   */
  LTreeRank rank(vertNum root=0) const;

  Tree::degVec;
  //
  Tree::neighborList;
  //
  Tree::center;
  //
  Tree::BFS;
  //
  Tree::reverseBFS;
  //
  Tree::DFS;
  //
  Tree::preorderS;
  //
  //Tree::postOrderS;

  Tree::getParents;

  /*
   * Other Label operations.
   */
  Label getLabel(vertNum i) const
   {
    assert(i>=0 && i<_order);

    return labels[i];
   }

  bool labelIndex( const Label &l, vertNum &v ) const
   {
     for (int i=0; i<_order; i++)
      if (labels[i]==l) { v=i; return true; }

     return false;
   }

  void setLabel(vertNum i, const Label &l) /* const */
   {
    assert(i>=0 && i<_order);

    labels[i]=l;
   }

  void setAllLabels(const Label &l) /* const */
   {
    // for (int i=0; i<_order; i++) labels[i]=l;
    //
    labels.fill(l);
   }

  Label& operator[](vertNum i)
   {
    assert(i>=0 && i<_order);

    return labels[i];
   }

  /*
   * Stream i/o stuff.
   */
  friend ostream& operator<<( ostream &o, const LTree &T );

// Cancel the following: (Should use tree2ps!)
//
#if 0
  typedef Str (*LNodeDrawStr)( const LTree &l, vertNum node );
  typedef int (*LNodeDrawType)( const LTree &l, vertNum node );
  //
  void draw( ostream&, LNodeDrawStr, LNodeDrawType ) const;
#endif

 };

#if 0
// should be private to ltree.c
//
class L_InternalNode    // Should be local in the constructor LTree(LGraph)!
 {                             // (cfront sorry)
  public:

    L_InternalNode() {}

    vertNum graph, tree;
 };
#endif

#if 0
bool isMinor(const LTree &super, const LTree &sub);
bool isMinor(const class LForest &super, const LTree &sub);
bool isMinor(const class LForest &super, const LTree &sub);
bool isMinor(const class LForest &super, const LForest &sub);
#endif

#endif
