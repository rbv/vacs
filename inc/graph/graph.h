
/***********************************************************************
 * Header for Adjacency matrix graph.
 **********************************************************************/

/*tex

\file{graph.h}
\path{inc}
\title{Header for standard graph class}
\classes{}
\makeheader

xet*/

#ifndef __graph_h
#define __graph_h

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "set/iterset.h"

//#include "general/bitvect.h"
class BitVector;

#include <iostream>

class InducedGraph;
//
typedef PIterSet<InducedGraph> Components;

//typedef PIterSet<Graph> Blocks;
typedef PIterSet<InducedGraph> Blocks;

//class BPG;

/*
 * Current implementaion uses adjacency matrix form of a graph.
 */
class Graph
 {

  iodeclarations( Graph )

protected:

  vertNum _space;			// Current space allocated.

  vertNum _order;			// Number of vertices.

  bool **_adj;				// Adjacency matrix of graph.

//  const vertNum _graphExtend = 16;	// For extending allocating space.

  static bool **_allocate(vertNum n);	// Private free store routines.
  //
  static void _deallocate(bool **a);

public:

  Graph( vertNum order, 		// Constructor: adjacency matrix.
        const bool **M);		// (order, matrix)

  Graph( const AdjMatrix &M ); 		// Constructor: adjacency matrix.

  Graph( const AdjLists &L ); 		// Constructor: adjacency list.

  Graph(vertNum order = 0);		// Constructor: Empty graph with order. 

  Graph(const Graph &G, vertNum add = 0);// Constructor: graph copy with
					// added isolated vertices.

  Graph(const InducedGraph &G);		// Constructor: induced graph
					// (in indgrph.c)

//Graph( const BPG &B );		// Construct from a bounded
  // See graphutil.h			// pathwidth graph.  
					// (Boundary gets lost.)

  Graph(const class Tree &T);		// see tree.c

  void fromUser(vertNum order = 0);	// Interactive graph builder (cin).
//void fromXUser(vertNum = 0);		// Visual Interactive graph builder.


  /*
   * Reuse graph -- Graph( order ).
   */
  void newGraph( vertNum order = 0 );	// Doesn't reallocate if possible.

  ~Graph();				// Destructor.

  /*
   * Operations on vertices (nodes)
   */
  void addNode(vertNum v);		// Add isolated vertices to graph.
  //
  vertNum addNode() { addNode(1); return order()-1; }		
  //
  void rmNode(vertNum v);		// Remove vertex v.
  //
  void rmIsoNodes();			// Remove all isolated vertices.

  /*
   * Operations on edges.
   */
  void addEdge(vertNum, vertNum);	// Add edge.
  //
  void rmEdge(vertNum, vertNum);	// Remove edge from graph.
  //
  void rmEdges(vertNum);
  //
  void contractEdge(vertNum, vertNum);	// Contract edge in graph.

  void complement();			// Take complement of graph.

  /* 
   * Basic query functions.
   */
  bool isEdge(vertNum i, vertNum j) const	// Check for edges.
  {
     assert(i>=0 && i<_order && j>=0 && j<_order && i!=j);
     return _adj[i][j];
  }
  //
  bool is_node(vertNum i) const                 // Is this vertex present.
       { return (i>=0 && i<_order); }

  vertNum nodes() const { return _order; } 	// Number of vertices.
  //
  vertNum order() const	{ return _order; }

  edgeNum size() const;				// Number of edges.
  //
  edgeNum edges() const { return size(); }

  vertNum degree(vertNum i) const;		// edges adjacent to a node.

  bool connected() const;			// Is the graph connected?

  vertNum components() const;			// return number of components.
  //
  vertNum components( Components &C ) const;

  vertNum getBlocks( Blocks &B ) const;

  /*
   * Binary operators.
   */
  bool operator==(const Graph &G) const;	// Isomorphism check.

  void operator=(Graph &G);			// Bitwise copy .

  /*
   * Multi-query operations.
   */
  void degVec(DegreeSequence &deg, int flag=0) const; // (flag=1 for sort)
  //
  // Note: deg passed in/out as a parameter since it maybe pre-allocated.

  BitVector neighbors(vertNum v) const;        	// Open neighborhood.
  //
  vertNum neighborList(vertNum v, VertSet &A) const;

  /*
   * Graph data structures. (for read/only membership/invarient functions.)
   */
  void getAdjLists( AdjLists &L, DegreeSequence &deg ) const;
  //
  void getAdjLists( AdjLists &L ) const;

  void getAdjMatrix( AdjMatrix &M, DegreeSequence &deg ) const;

  /*
   * Yo, I/O dudes!
   */
  //friend istream& operator>>( istream &i, Graph &G );
  //
  friend istream& operator>>( istream &i, class LGraph &G );

 };

/*
 * Other stream i/o stuff.
 */
ostream& operator<<( ostream &o, const Graph &G );
//
istream& operator>>( istream &i, Graph &G );

//---------------------------------------------------------------------

/*
 * Add edge to graph.
 */
inline void Graph::addEdge(vertNum i, vertNum j)
 {
  assert(i>=0 && i<_order && j>=0 && j<_order && i!=j);
  _adj[i][j] = _adj[j][i] = 1;
 }

/*
 * Remove edge from graph.
 */
inline void Graph::rmEdge(vertNum i, vertNum j)
 {
  assert(i>=0 && i<_order && j>=0 && j<_order && i!=j);
  _adj[i][j] = _adj[j][i] = 0;
 }

#if 0
/*
 * Remove edges attached to a vertex from graph.
 */
inline void Graph::rmEdges(vertNum v)
 {
  assert(v>=0 && v<_order);
  for (int j=0; j<_order; j++) _adj[v][j] = _adj[j][v] = 0;
 }
#endif

#endif
