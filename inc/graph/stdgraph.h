/*
 * Standard types for graph classes.  
 * (Included by appropriate graph class header.)
 */

/*tex
 
\file{stdgraph.h}
\path{inc/graph}
\title{Header for standard graph definitions (part 1)}
\classes{}
\makeheader
 
xet*/


#ifndef _stdgraph_h
#define _stdgraph_h

#include "stdtypes.h"

typedef	short vertNum;
typedef	ushort edgeNum;
typedef	ushort arcNum;

#undef major

#if 0
// Optional methods that class GGraph will first check the template 
// representation GRep for (overrides default GGraph method).  Otherwise,
// a default method named df_"methodname" will be used.
//
// Warning: Only put things here if a major efficiency improvement is 
//          available with one of the known GRep representations.
//
enum GRepCanDoOptions // do"MethodName" = 1<<i
 { 
  doSize = 1,
  doDegree = 2,
  doNeighbor = 4,
  doContract = 8 
 };
//
typedef ushort GRepCanDoList;
#endif

//class Graph;

#endif
