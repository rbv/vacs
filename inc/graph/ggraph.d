
/***************************************************************************
 * General macros for default methods  (should only be used in source files)
 ***************************************************************************/

/*tex

\file{ggraph.d}
\path{src/graph}
\title{Default GRep methods}
\classes{ template<class GRep> }
\makeheader

xet*/

#ifndef _GGraph_macros
#define _GGraph_macros

//
// Default methods for a graph representation (GRep)
//

/*
 * Graph size.
 */
#define GRepSize(GRep) \
 \
edgeNum GRep::size() const \
 {  \
  vertNum numVertices = order();  \
  vertNum sz = 0; \
 \
  for (int i=0; i<numVertices; i++) \
   for (int j= i+1; j<numVertices; j++) if (isEdge(i,j)) sz++; \
  return sz; \
 }
 
/*
 * Node degree. 
 */ 
#define GRepDegree(GRep) \
 \
vertNum GRep::degree( vertNum u) const \
 { \
  vertNum numVertices = order(); \
 \
  vertNum sz = 0; \
 \
  for (int i= 0; i<numVertices; i++)  \
   { \
    if (i==u) \
    if (isEdge(i,u)) sz++; \
   } \
 \
  return sz; \
 }
 
/*
 * n-th neighbor of a node.  (starting at 0) 
 */ 
#define GRepNeighbor(GRep) \
 \
vertNum GRep::neighbor( vertNum u, vertNum n ) const \
 { \
  vertNum numVertices = order(); \
 \
  vertNum sz = 0; \
 \
  for (int i= 0; i<numVertices; i++)  \
   { \
    if (i==u) \
    if (isEdge(i,u)) sz++; \
    if (sz>n) return i; \
   } \
 \
  aassert(false); \
  return u; \
 }
 
/* 
 * Contract an edge in a graph.  (not digraphs) 
 */ 
#define GRepContract(GRep) \
 \
void GRep::contract(vertNum v1, vertNum v2) \
 { \
  assert( isEdge(v1,v2) ); \
 \
  vertNum numVertices = order(); \
 \
  for (int i=0; i<numVertices; i++) \
   { \
     if (i==v1) continue; \
     if (isEdge(i,v2)) addEdge(i,v1); \
   } \
 \
  rmNode(v2); \
  return; \
 }

#endif	
