
/***********************************************************************
 * Header for boundary graph class.
 **********************************************************************/

 
/*tex
 
\file{bgraph.h}
\path{inc/graph}
\title{Header for boundary graph class}
\classes{BGraph}
\makeheader
 
xet*/

#ifndef __bgraph
#define __bgraph

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "graph/graph.h"

class BPG;
class LGraph;

/*
 * Graph with boundary.
 */
class BGraph : private Graph
 {
private:

  iodeclarations( BGraph )

  VertArray _bndry;		// List of boundary vertices.
  DyArray<bool> _bFlag;		// Constant time boundary check.

public:
  /*
   * Constructor: Graph and boundary.
   */
#if 0
  BGraph(const Graph G, VertArray b) : Graph(G), _bndry(b), _bFlag(G.order())
   {
    assert(order() >= _bndry.size());

    _bFlag.fill(false);

    for (int i=0; i<_bndry.size(); i++) _bFlag[_bndry[i]] = true;
aassert(false); // make noninline
   }
#endif

#if 0
  /*
   * Constructor: Isolated vertices with no boundary.
   */
  BGraph(vertNum n = 0) : Graph(n), _bndry(0), _bFlag(n)
   { 
    _bFlag.fill(false);
   }
#endif

  /*
   * Constructor: Isolated vertices with initial boundary.
   */
  BGraph(vertNum n = 0, vertNum b = 0);

  // Constructors
  //
  BGraph(const BPG &B, vertNum extra = 0); // + new nodes

  BGraph(const BGraph &B, vertNum extra = 0); // + new nodes

  // from a labeled graph with unique labels 0,1,...,t-1
  //
  BGraph(const LGraph &LG, vertNum bndySz); // + boundary size

  // Destructor.
  // 
  ~BGraph() {};				

  // CirclePlus
  //
  void concat(const BGraph &B);

  // Same as circle plus but but boundary is lost in output graph G
  //
  static void getGluedGraph(const BGraph &B1, const BGraph &B2, Graph &G);

  /*
   * Promotions from Graph
   */
  Graph::order;
  //
  Graph::size;
  //
  Graph::isEdge;
  //
  Graph::degVec;
  //
  Graph::connected;
  //
  Graph::rmEdges;
  //
  Graph::rmEdge;
  //
  Graph::addEdge;
  //
  Graph::degree;

  Graph::neighbors;

  // biconnected to boundary vertices
  bool isBiconnected( const BGraph& Gin );
  
  /*
   * Operations on vertices (nodes)
   */
  void addNode(vertNum v)           // Add isolated vertices to graph.
   {
        //VertArray _bndryT(_bndry);	// List of boundary vertices.
        //DyArray<bool> _bFlagT(_bFlag);	// Constant time boundary check.

	Graph::addNode(v);

        //_bndry = _bndryT;
        //_bFlag = _bFlagT;
        _bFlag.extend(v,false);
   }
  //
  vertNum addNode() { addNode(1); return order()-1; }

  void getAdjLists( AdjLists &L, DegreeSequence &deg ) const
   {
    Graph::getAdjLists( L, deg );
   }

  /*
   * Set/Query about boundary size.
   */
  void vertOp( vertNum bdy ); 		// Isolated vertex pulloff.
  //
  void vertEdgeOp( vertNum bdy ); 	// Vertex pulloff with edge.
  //
  void edgeOP( vertNum b1, vertNum b2 );

  vertNum boundarySize() const { return _bndry.size(); }
  //
  vertNum boundary(vertNum i) const { return _bndry.get(i); }
  //
  bool isBoundary(vertNum i) const { return _bFlag.get(i); }

  bool operator==(const BGraph &B) const;		// Isomorphism check.

 };

inline ostream& operator<<( ostream &o, const BGraph &G )
 {
  o << "Boundary: ";
  int i; for (i=0; i<G.order(); i++) if (G.isBoundary(i)) o << i << ' ';
  o << " / ";
  for (i=0; i<G.boundarySize(); i++) o << G.boundary(i) << ' ';
  return o << nl << ((Graph&) G);
 }

#endif
