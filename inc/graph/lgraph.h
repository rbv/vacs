
/***********************************************************************
 * Header for labeled graph.
 **********************************************************************/

/*tex

\file{lgraph.h}
\path{inc/graph}
\title{Header for vertex labeled graph class.}
\classes{LGraph}
\makeheader

xet*/

#ifndef __lgraph_h
#define __lgraph_h

#include "graph/graph.h"
#include "array/dyarray.h"
#include "graph/forest.h"

/*
 * Current implementaion uses adjacency matrix form of a graph.
 */

//--------------------------------------------------------------
// Template version for adding generic labels (not implemented)
//--------------------------------------------------------------

//template<class Label>
//
typedef vertNum Label;

const Label NOLABEL=255;
//
class LGraph : public Graph
 {
private:

  DyArray< Label > labels;

public:

  LGraph(vertNum order, 		// Constructor: adjacency matrix.
        const bool **M) :		// (order, matrix)
    Graph( order, M), labels(order)	// Labels to be set by user.
    { }

  LGraph(vertNum order, 		// Constructor: adjacency matrix.
        const bool **M, 		// (order, matrix, label)
        const Label &l) ;
//    Graph( order, M), labels(order)
//    {
//      labels.fill(l);
//    }

  LGraph(vertNum order = 0) :		// Constructor: Empty graph with order. 
    Graph( order ), labels(order) 	// (undefined labels)
    { }

  LGraph(vertNum order, 		// Constructor: Empty graph with order. 
         const Label &l) ;
//    Graph( order ), labels(order) 
//    {
//      labels.fill(l);
//    }

  /*
   * Boundary vertices will be labeled 1 to boundarySize. 
   */
  LGraph( const class BPG& );

  /*
   * Vertices will be labeled same as trees.
   */
  LGraph( const LForest &F );

  // Constructor: graph copy with added isolated vertices.
  //
  LGraph(const LGraph &G, vertNum add = 0);
  
  ~LGraph() {}				// Destructor.


  void fromUser(vertNum order = 0)	// Interactive graph builder (cin).
   { (void) order; aassert(false); }


  /*
   * Operations on vertices (nodes)
   */ 
  void addNode(vertNum v, const Label &l);
  //
  vertNum addNode(const Label &l) { addNode(1,l); return order()-1; }
  //
  void rmNode(vertNum v);
  //
  void rmIsoNodes();

  /*
   * Operations on edges altered from class Graph.
   */
  void contractEdge(vertNum, vertNum, const Label&);

  void addLEdge( const Label &l1, const Label &l2 )
   {
     vertNum v1 = index(l1);
     vertNum v2 = index(l2);

     addEdge( v1, v2 );
   }

  bool isLEdge( const Label &l1, const Label &l2 )
   {
     vertNum v1 = index(l1);
     vertNum v2 = index(l2);

     return isEdge( v1, v2 );
   }

  /*
   * Binary operators.
   */
  bool operator==(const LGraph &G);		// Isomorphism check.

  void operator=(LGraph &G);			// Bitwise copy.

  /*
   * Other Label operations.
   */
  Label getLabel(vertNum i) const
   {
    assert(i>=0 && i<_order);

    return labels[i];
   }

  void setLabel(vertNum i, const Label &l) const
   {
    assert(i>=0 && i<_order);

    labels[i]=l;
   }

  Label& operator[](vertNum i)
   {
    assert(i>=0 && i<_order);

    return labels[i];
   }

  vertNum index( const Label &l )
   { 
     for (int i=0; i<order(); i++) if (labels[i] == l) return i;

     aassert (false);		// assumes label known to exist.

     return 0;
   }

  bool isLVertex( const Label &l, vertNum &v )
   { 
     for (int i=0; i<order(); i++) 
      if (labels[i] == l) { v = i; return true; }

     return false;
   }


  /*
   * Yo, I/O dude!
   */
  friend istream& operator>>( istream &i, LGraph &G );

 };

/*
 * Other Stream i/o stuff.
 */
ostream& operator<<( ostream &o, const LGraph &G );

#endif
