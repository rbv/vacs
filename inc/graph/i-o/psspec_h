
/***********************************************************************
 * Header for PostScript output customization class.
 **********************************************************************/

/*tex

\file{psspec.h}
\path{inc}
\title{Header for general purpose postscript drawers}
\classes{ PSSpec<Tree> }
\makeheader

xet*/

#ifndef __psspec_h
#define __psspec_h

#include <iostream>

#include "graph/stdgraph.h"
#include "general/str.h"

/* 
 * -----------------------------------------------------------------
 * Optional specifications/options passed to graph object drawers.
 * -----------------------------------------------------------------
 *
 * Note: Some drawers may not use all of these options.
 *
 */

//template < class GObject >	// class PSSpec<GObject> ...

class PSSpecTree 
 { 
 
public: 
 
  // PostScript options
  //
  enum PSNodeType { WhiteCircle=0, BlackCircle=1, GrayCircle=2, 
		    WhiteSquare=3, BlackSquare=4, GraySquare=5 }; 
   
  enum PSEdgeType { Normal=0, Dashed=1, Bold=2, Squiggly=3,  
                    RightArrow=4, LeftArrow=5, BothArrow=6 }; 
 
  enum PSFormat { BoundingBox=0, DviSpecial=1, Poster=2 }; 
 
  // Callback functions
  //
  typedef Str (*NodeStrFnc)(const Tree&, vertNum); 

  typedef PSNodeType (*NodeTypeFnc)(const Tree&, vertNum); 

  typedef Str (*EdgeStrFnc)(const Tree&, vertNum, vertNum); 

  typedef PSEdgeType (*EdgeTypeFnc)(const Tree&, vertNum, vertNum); 

private: 
 
  /* 
   * If the following are not set (assigned) then defaults are used. 
   */ 
  // 
  NodeStrFnc _nodeStr; 
  // 
  NodeTypeFnc _nodeType; 
  // 
  EdgeStrFnc _edgeStr; 
  // 
  EdgeTypeFnc _edgeType; 
 
  /* 
   * Some optional headings/trailers.  
   * (hack until object is designed to know these things) 
   */ 
  const Str *_title;		/* Something on top of figure. */ 
   
  const Str *_caption;		/* Something under figure. */ 
 
  ushort _width, _height;	/* For bounding box. */ 
 
  /** 
   ** Default output format 
   **/ 
  PSFormat _format; 
 
  ostream *_out;
 
public: 
 
  /* 
   * Constructors 
   */ 
  PSSpecTree( ushort width = 400, ushort height = 400,  
          PSFormat format = BoundingBox ) 
   { 
    _nodeStr = 0; 
    _nodeType = 0; 
    _edgeStr = 0; 
    _edgeType = 0; 
    _title = 0; 
    _caption = 0; 
 
    _width = width; 
    _height = height; 
 
    _format = format; 

    _out = 0;
   } 
 
  /* 
   * Destructor 
   */ 
  ~PSSpecTree() {} 
 
  /* 
   * Set methods. 
   */ 
  void setNodeStrFnc( NodeStrFnc f ) 
   { 
    _nodeStr = f; 
   } 
 
  void setNodeTypeFnc( NodeTypeFnc f ) 
   { 
    _nodeType = f;   
   }    
   
  void setEdgeStrFnc( EdgeStrFnc f ) 
   { 
    _edgeStr = f; 
   } 
 
  void setEdgeTypeFnc( EdgeTypeFnc f ) 
   { 
    _edgeType = f; 
   } 
 
  void setTitle( const Str &title ) { _title = &title; } 
   
  void setCaption( const Str &caption ) { _caption = &caption; } 
 
  void setWidth( ushort width ) { _width = width; } 
 
  void setHeight( ushort height ) { _height = height; } 
 
  void setFormat( PSFormat format ) { _format = format; } 
 
  void setOut(ostream &out) { _out = &out; } 
 
  /* 
   * Access methods. 
   */  
  NodeStrFnc getNodeStrFnc() const
   { 
    return _nodeStr; 
   } 
 
  NodeTypeFnc getNodeTypeFnc() const
   { 
    return _nodeType;   
   }    
 
  EdgeStrFnc getEdgeStrFnc() const
   { 
    return _edgeStr; 
   } 
 
  EdgeTypeFnc getEdgeTypeFnc() const
   { 
    return _edgeType; 
   } 
 
  const Str* getTitle() const { return _title; } 
  
  const Str* getCaption() const { return _caption; } 
 
  ushort getWidth() const { return _width; } 
 
  ushort getHeight() const { return _height; } 
 
  PSFormat getFormat() const { return _format; } 
 
  ostream* getOut() const { return _out; } 
 
  /* 
   * Access pass-thru methods. 
   */  
  Str drawNodeStr(const Tree &G, vertNum v) const
   { 
    if (_nodeStr) return getNodeStrFnc()(G,v); 
    else 	  return Str(); 
   } 
 
  PSNodeType drawNodeType(const Tree &G, vertNum v)  const
   { 
    if (_nodeType) return getNodeTypeFnc()(G,v);   
    else	   return WhiteCircle; 
   }    
 
  Str drawEdgeStr(const Tree &G, vertNum u, vertNum v)  const
   { 
    if (_edgeStr) return getEdgeStrFnc()(G,u,v); 
    else	  return Str();
   } 
 
  PSEdgeType drawEdgeType(const Tree &G, vertNum u, vertNum v) const
   { 
    if (_edgeType) return getEdgeTypeFnc()(G,u,v); 
    else	   return Normal; 
   } 

 }; 
 
#endif 
