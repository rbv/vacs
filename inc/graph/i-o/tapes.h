
/***********************************************************************
 * Header for reading Reed's isomorphic graph tapes.
 **********************************************************************/

#ifndef __tapes
#define __tapes

#include <stdio.h>

#include "stdtypes.h"
#include "graph/graph.h"

static const int fact10 = 3628800;

enum read_mode 
 {
  CONNECTED,
  NO_ISOLATED,
  ALL
 };

/*
 * Tape graph internal representation.
 */
class Tape_graph
 {
    FILE *in_graphs;
    vertNum size;
    read_mode mode;
    bool taken_complement;

    ushort f_graph[3];
    uint f_aut, f_count, f_record;

    struct 
     {
      ushort m[10],           /* Two byte words for adjacency matrix.  */
             span;            /* Maximum cover of connected vertices.  */
     } s_graph;

    Graph *orig_G;
    Graph *G;

    void build_graph();
    uint read_graph();

public:
  Tape_graph(FILE*, vertNum, read_mode = ALL);
	// Constructor: filename, graph size, graph mode.
  Tape_graph(read_mode = ALL);
        // Constructor: Ask user for filename and graph size.

  ~Tape_graph();		// Destructor.

  bool nextgraph();		// Load next file if possible;
				// 0 - EOF, otherwise # graphs read.

  Graph* graph() const { return G; }	// read only graph.
 };

#endif
