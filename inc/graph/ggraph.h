
/***********************************************************************
 * Header for general purpose graph (templated representation).
 **********************************************************************/

/*tex

\file{ggraph.h}
\path{inc}
\title{Header for general purpose graph class}
\classes{ GGraph<GRep> }
\makeheader

xet*/

#ifndef __ggraph_h
#define __ggraph_h

#include "graph/stdgraph.h"

//
class bistream;
class bostream;


/* 
 * -----------------------------------------------------------------
 * Create a graph with data structure represtented by type GRep.
 * -----------------------------------------------------------------
 *
 * ** The following methods should be available (and safe) to GRep:
 *
 * ==>>  Can steal some default methods from /inc/graph/ggraph.d  <<==
 *
 *   GRep();
 *   GRep(const GRep &G);
 *   void operator=(const GRep &G);
 *   //
 *   vertNum order() const; 
 *   edgeNum size() const; 
 *   bool isEdge(vertNum u, vertNum v) const;
 *   //
 *   vertNum degree( vertNum u ) const;
 *   vertNum neighbor(vertNum v, vertNum n=0) const;
 *
 * ** The following methods should be defined: 
 * ** (use "assert(false)" if not intended for use)
 *
 *   void addNode();
 *   void rmNode(vertNum);
 *   void addEdge(vertNum u, vertNum v);
 *   void rmEdge(vertNum u, vertNum v);
 *   void contract(vertNum u, vertNum v);
 *
 * ** Related classes:	// Constructors take either a GRep or GGraph
 *
 *   class EdgeIter;		
 *   class NeighborIter;
 *
 */
template < class GRep >
//
class GGraph
 {

private:

  GRep _graph;				// Hidden graph representation.

  
public:

  // Constructors
  //
  GGraph() { }				// empty graph

  GGraph(const GGraph &G) _graph(G.getRep()) {}

  // Destructor
  //
  ~GGraph() {}

//
// ---------------------------------------------------------------
//  GRep pass thru methods (until template ": public GRep" works)
// ---------------------------------------------------------------
// 

  /*
   * Operations on vertices (nodes)
   */
  //
  vertNum order() const	{ return _graph.order(); }
  //
  void addNode() { _graph.addNode(); }		
  //
  //vertNum addNode() { return _graph.addNode(); } // if availabe!
  // 
  void rmNode(vertNum v) { _graph.rmNode(v); }	
  //
  vertNum degree(vertNum v) const;
 
  /*
   * Operations on edges.  
   */
  //
  bool isEdge(vertNum i, vertNum j) const { return _graph.isEdge(i,j); }
  //
  edgeNum size() const { return _graph.size(); } 
  //
  void addEdge(vertNum u, vertNum v) { _graph.addEdge(u,v); }
  // 
  void rmEdge(vertNum u, vertNum v) { _graph.rmEdge(u,v); } 
  // 
  void contract(vertNum u, vertNum v) { _graph.contract(u,v); }

  /*
   * Access n-th neighbor. [ n = 0,1,...,deg(v)-1 ]
   */
  vertNum neighbor(vertNum v, vertNum n=0) const 
   {
    return _graph.neighbor(v,n); 
   }

  /*
   * Binary operators.
   */
  bool operator==(const GGraph<Grep> &G) const	// Isomorphism check.
   {
    return _graph == G.getRep();
   }
  //
  void operator=(const GGraph<Grep} &G) { _graph = G.getRep(); }

//
// -----------------------------------
//  Common non-GRep specific methods
// -----------------------------------
// 

  /*
   * Operations on vertices (nodes)
   */
  bool isNode(vertNum i) const { return ( i>=0 && i<_graph.order() ); }

  /*
   * Access internal representation (until template ": public GRep" works)
   */
  const GRep& getRep() { return _graph; }
  //
  GRep& operator() { return _graph; }

  //--------------------------------- 
  // Human readable and binary input.
  //---------------------------------
  //
  friend istream& operator>>( istream &i, GGraph &G );
  //
  friend bstream& operator>>( bistream &b, GGraph &G );

  /*
   * OTHER STUFF -- maybe should not be in this class!
   */

  // Note: deg passed in/out as a parameter since it maybe pre-allocated.
  //
  void degVec(DegreeSequence &deg, int flag=0) const; // (flag=1 for sort)

  bool connected() const;  // implies components() == 1
  //
  vertNum components() const;

 };

/*
 * Other stream i/o stuff.
 */
template <class Rep> ostream& operator<<( ostream &o, const GGraph<Rep> &G );

template <class Rep> bostream& operator<<( istream &b, const GGraph<Rep> &G );

#endif
