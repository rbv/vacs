
/***********************************************************************
 * Header for Trees.
 **********************************************************************/

/*tex

\file{tree.h}
\path{inc/graph}
\title{Header for Trees}
\classes{Tree}
\makeheader

xet*/

#ifndef __tree_h
#define __tree_h

#include "stdtypes.h"
//#include "array/array.h"
#include "array/dyarray.h"
#include "array/sortarray.h"
//#include "set/set.h"
//#include "general/bitvect.h"
//#include "general/str.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"

#include "graph/forest.h"

//#include <iostream>
//

typedef SortableArray< vertNum > TreeRank;


/*
 * Tree with default vertex 0 as root.
 */
class Tree
 {
protected:

  vertNum _order;			// Number of vertices.

  AdjLists adjLists;			// Adjacency lists.

public:

  Tree(int space=0) : adjLists(1,space) { _order=1; }	// Constructor: K1
  							// with room to grow.
  /*
   * Binary type constructors.
   */
  Tree(const Tree &T1, const Tree &T2);	// Via new root.
  //
  Tree(const Tree &T1, const Tree &T2, vertNum u, vertNum v); // Via edge.

  Tree(const Graph &G, vertNum v=0);	// Spanning tree for component
					// containing v. (BFS)

  Tree( const VertArray &LS );		// Construct tree from level sequence.

  Tree( const AdjLists &L );		// Construct tree from adjacency list.

  Tree( const Tree &T ) : adjLists(T.adjLists) { _order = T.order(); }

  ~Tree() {}				// Destructor.

  /*
   * Operations on vertices (nodes)
   */
  vertNum addLeaf(vertNum v);		// Add leaf to tree.
  //
  void rmLeaf(vertNum v);		// Remove a vertex.
  //
  Forest* rmNode(vertNum v) const;// Breakup tree into a forest.

  /*
   * Operations on edges.
   */
  void contract(vertNum&, vertNum);	// Contract edge in tree.
  //
  vertNum subdivide(vertNum, vertNum);  // Split and edge into two.

  /* 
   * Basic query functions.
   */
  bool isEdge(vertNum i, vertNum j) const	// Check for edges.
   {
    assert( isNode(i) && isNode(j) );
    return ( adjLists[i].index(j) < adjLists[i].size() );
   }
  //
  //
  bool isNode(vertNum i) const                  // Is this vertex present.
       { return (i>=0 && i<_order); }
  //
  //
  bool isLeaf(vertNum i) const                  // Is this a leaf.
       { return (i>=0 && i<_order && adjLists[i].size()==1); }
  //
  //
  vertNum leafParent(vertNum i) const	        // Return leaf's neighbor.
   {
    assert( isLeaf(i) );
    return adjLists[i][0];
   }

  void getParents(VertArray &P, vertNum root = 0) const;

  vertNum neighbor(vertNum v, vertNum i=0) const // Return i-th neighbor of v.
   {
    assert( isNode(v) && i<adjLists[v].size() );
    return adjLists[v][i];
   }

  /*
   * Find path between two vertices.
   */
  void getPath( vertNum u, vertNum v, Path &p) const;

  vertNum degree(vertNum i) const		// Number of neighbors.
   {
    assert( isNode(i) );
    return adjLists[i].size();
   }

  vertNum nodes() const { return _order; } 	// Number of vertices.
  //
  vertNum order() const	{ return _order; }

  edgeNum size() const  { return _order-1; }	// Number of edges.
  //
  edgeNum edges() const { return _order-1; }

  /*
   * Binary operators.
   */
  bool operator==(const Tree &T) const;		// Isomorphism check.
  //
  Tree& operator=(const Tree &T);		// Bitwise copy.

  /*
   * Multi-query operations.
   */
  TreeRank rank(vertNum root=0) const;
  
  void degVec(DegreeSequence &deg, int flag=0) const; // (flag=1 for sort)
  //
  // Note: deg passed in/out as a parameter since it maybe pre-allocated.

  // Open neighborhood.
  //
  vertNum neighborList(vertNum v, VertSet &A) const
   {
    assert( isNode(v) );
    A = adjLists[v];
    return adjLists[v].size();
   }

  /*
   * Determine center/bicenters of the tree.
   * (Returns true if unique center otherwise false for bicenters.)
   */
  bool center(vertNum &c1, vertNum &c2) const;

  /*
   * Search techniques.
   */
  void BFS(VertArray &seq, vertNum root=0) const;

  void reverseBFS(VertArray &seq, vertNum root=0) const 
   {
     BFS(seq,root);
     seq.reverse();
   }
  
  void DFS(VertArray &seq, vertNum root=0) const; // AKA preorder

  void preorderS(VertArray &seq, vertNum root=0) const
   {
     DFS(seq,root);	// note: DFS does right to left branches
   }

#if 0
  void postOrderS(VertArray &seq, vertNum root=0) const
   {
     reverseBFS(seq,root); // warning! -- may not be what is expected
   }
#endif


  /*
   * Statics
   */
  typedef void (*Callback_FNC)(Tree &T);
  //
  static void freeTrees(int n, Callback_FNC TreeFnc=0);

  /*
   * Stream i/o stuff.
   */
  friend ostream& operator<<( ostream &o, const Tree &T );

  //typedef Str (*NodeDrawStr)( const Tree &T, vertNum node );
  //typedef int (*NodeDrawType)( const Tree &T, vertNum node );
  //
  //void draw( ostream&, NodeDrawStr, NodeDrawType ) const;

 };

#if 0
bool isMinor(const Tree &super, const Tree &sub);
bool isMinor(const class Forest &super, const Tree &sub);
bool isMinor(const class Forest &super, const Tree &sub);
bool isMinor(const class Forest &super, const Forest &sub);
#endif

class _InternalNode             // Should be local in src files cnstr.
{                              // (cfront sorry)
  public:

    _InternalNode() {}

    vertNum graph, tree;
};


inline bool operator==(const _InternalNode&, const _InternalNode&)
{ aassert(false); return false; }

#endif
