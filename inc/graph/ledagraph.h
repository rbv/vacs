
/***********************************************************************
 * Header for LEDA inherited graph.
 **********************************************************************/

/*tex

\file{ledagraph.h}
\path{inc/graph}
\title{Header for LEDA graph class}
\classes{LedaGraph}
\makeheader

xet*/

#ifndef __ledagraph_h
#define __ledagraph_h

#include <iostream>
#include "graph/graph.h"
//
class InducedGraph;

//#define DOLEDA

#ifdef DOLEDA
#include "LEDA/graph.h"			// Directed graph!
#else
typedef Graph graph;	
#endif


/*
 * Current implementaion uses adjacency matrix form of a graph.
 */
class LedaGraph : public graph
 {

public:

  LedaGraph() {};			// Empty constructor.

  LedaGraph(const Graph &G);		// From local class Graph constructor.

  LedaGraph(const InducedGraph &G);	// Same ...

  ~LedaGraph() {};			// Destructor.

#ifndef DOLEDA
  void write(ostream &o) const { o << ((Graph&) *this) << nl; }
#endif
 };

/*
 * Other stream i/o stuff.
 */
inline ostream& operator<<( ostream &o, const LedaGraph &G )
 {
   G.write(o); 
   return o;
 }

#endif
