
/***********************************************************************
 * Algorithms for partial orders on graph structures.
 **********************************************************************/

/*tex

\file{orders_ga.h}
\path{inc/graph/algorithm}
\title{Header for graph partial orders}
\classes{Tree,Graph}
\makeheader

xet*/

#ifndef __orders_ga_h
#define __orders_ga_h

#include "graph/stdgraph.h"
class Tree;
class LTree;
#include "graph/forest.h"
//class Graph;

bool isMinor(const Tree &super, const Tree &sub);
bool isMinor(const Forest &super, const Forest &sub);

bool isMinor(const LTree &super, const LTree &sub);
bool isMinor(const LForest &super, const LForest &sub);

bool isHomeo(const Tree &super, const Tree &sub);
bool isHomeo(const Forest &super, const Forest &sub);

bool isHomeo(const LTree &super, const LTree &sub);
bool isHomeo(const LForest &super, const LForest &sub);

bool isRootedHomeo(const Tree &super, vertNum r1, 
	           const Tree &pattern, vertNum r2);
bool isRootedHomeo(const LTree &super, vertNum r1, 
		   const LTree &pattern, vertNum r2);

#endif
