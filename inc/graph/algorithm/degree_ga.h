
/*tex
 
\file{degree_ga.h}
\path{inc/graph/algorithm}
\title{Header simple degree sequence checks}
\classes{}
\makeheader
 
xet*/


#ifndef __degree_ga_h
#define __degree_ga_h

#include "stdtypes.h"
//
class Graph;

/*
 * Some standard degree tests.
 */
bool NoDegreeLessEqualKVertices( const Graph &G, int k );
//
bool NoDegreeZeroVertices( const Graph &G );
//
bool NoDegreeZeroOrOneVertices( const Graph &G );
//
bool NoDegreeZeroToTwoVertices( const Graph &G );

#endif
