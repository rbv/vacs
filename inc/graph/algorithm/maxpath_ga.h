
/*tex
 
\file{maxpath_ga.h}
\path{inc/graph/algorithm}
\title{Header for maxpath membership test}
\classes{}
\makeheader
 
xet*/


#ifndef __maxpath_ga.h
#define __maxpath_ga.h

#include "graph/stdgraph.h"

// Are all paths less than length k?
//
bool maxPath( const Graph &G, vertNum k );

#endif
