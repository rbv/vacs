
/*tex
 
\file{distance_ga.h}
\path{inc}
\title{Header for graph distance matrix}
\classes{}
\makeheader
 
xet*/


#ifndef __distMat_h
#define __distMat_h

#include "graph/graph.h"
#include "array/matrix.h"

typedef SquareMatrix<vertNum> DistanceMatrix;


/* 
 * Get distance matrix.
 */
void dist_mat(const Graph &G, DistanceMatrix &d);

#endif
