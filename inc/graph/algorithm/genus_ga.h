
/*tex
 
\file{genus_ga.h}
\path{inc/graph/algorithm}
\title{Graph embedable checkers}
\classes{}
\makeheader
 
xet*/


#ifndef __genus_ga_h
#define __genus_ga_h

#include "graph/stdgraph.h"

class Graph;
class InducedGraph;

// PLANARITY ?
//
bool planarBlock( const Graph &B );
bool planarBlock( const InducedGraph &B );
//
bool planar( const Graph &G );
bool planar( const InducedGraph &G );
//
bool outerplanar( const Graph &G );
bool outerplanar1( const Graph &G );

// Fixed param (within k-vertices of)
//
bool planar( const Graph &G, vertNum k );
bool planarEdge( const Graph &G, vertNum k );

// TORODIAL ?
//
bool torodialBlock( const Graph &B );
bool torodialBlock( const InducedGraph &B );
//
bool torodial( const Graph &G );
bool torodial( const InducedGraph &G );

/*
 * Main functions
 */
bool genusBrute( const Graph &G, int k );
//
int  genusBrute( const Graph &G );

#endif
