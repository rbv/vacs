
/*tex
 
\file{pathwidth_ga.h}
\path{inc}
\title{Header for finding pathwidth and path-decompositions}
\classes{}
\makeheader
 
xet*/


#ifndef __pathwidth_ga_h
#define __pathwidth_ga_h

#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"

bool pathwidthC( const Graph &G, vertNum k); // connected graph.
bool pathwidth( const Graph &G, vertNum k);
//
vertNum pathwidthC( const Graph &G ); // find minimum "k" 
vertNum pathwidth( const Graph &G ); 

typedef Array<VertArray> PathDeco;
//
bool path_decomposition( const Graph &G, int k, PathDeco &D );

/*
 * John Ellis and company's vertex separation algorithm for trees.
 */
//class RootedTree;
//
#include "graph/rtree.h"
//
vertNum pathwidth( const RootedTree &RT );
//
inline vertNum pathwidth( const Tree &T )
 {
  return pathwidth( RootedTree(T,0) );
 }

bool separationC( const Graph &G, vertNum k);

#endif
