
/*tex
 
\file{fvs_ga.h}
\path{inc/graph/algorithm}
\title{Various feedback vertex set algorithms}
\classes{}
\makeheader
 
xet*/


#ifndef __fvs_ga_h
#define __fvs_ga_h

#include "graph/graph.h"
#include "graph/stdgraph.h"

bool fvsBrute( const Graph &G, int k);
//
vertNum fvsBrute( const Graph &G);

bool fvsBrute2( const Graph &G);

bool fvsBrute2I( const Graph &G, int i);

// fix-parameter version
//
bool fvsFixed(const Graph &G, int k);
//
vertNum fvsFixed(const Graph &G);

// feedback edge sets
//
bool fesBrute(const Graph &G, int k);
int fesBrute(const Graph &G);
//
bool fesBrute2(const Graph &G_in);
bool fesBrute3(const Graph &G_in);
bool fesBrute4(const Graph &G_in);

// feedback vertex=1 & edge=1 set
//
bool vej11Brute(const Graph &G);
bool vej12Brute(const Graph &G);

// all k-subsets dominate all cycles
//
bool allFVS_1(const Graph &G);
bool allFVS_2(const Graph &G);
#endif
