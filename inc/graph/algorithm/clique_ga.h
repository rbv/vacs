
/*tex
 
\file{clique_ga.h}
\path{inc/graph/algorithm}
\title{Clique algorithm}
\classes{}
\makeheader
 
xet*/


#ifndef __clique_h
#define __clique_h

/*
 * Clique number of a graph.
 */
vertNum clique(const Graph &G);

#endif
