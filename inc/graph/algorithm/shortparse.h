
/*
 * ShortParse class used by pathwidth algorithm.
 */

/*tex

\file{shortparse.c}
\path{src/graph/algorithm}
\title{Header for pathwidth algorithm pathiwidth_ga}
\classes{ShortParse,ShortParses}
\makeheader

xet*/

#ifndef  _ShortPARSE_H
#define  _ShortPARSE_H

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "general/sets.h"
#include "array/sortarray.h"

//#define DEBUG
//#define DEBUG2

#include <iostream>

// Membership test classes.
//
//static 
class ShortParse
 {
private:

   VertSet _boundary, _interior;

public:

   ShortParse()  // hack for sortarray
    { 
    }

   // Creates a new parse with boundary and no interior.
   //
   ShortParse( const VertSet &B ) : _boundary(B)
    {
    }

   // Creates a new parse with interior.
   //
   ShortParse( const VertSet &I, const VertSet &B )
    : _interior(I), _boundary(B)
    {
    }

   // Creates a new parse from old.
   //
   ShortParse( const ShortParse &P ) 
    : _interior(P.interior()), _boundary(P.boundary())
    {
    }

   // assignment
   //
   void operator=( const ShortParse &P )
    {
     _interior = P.interior();	// should be a copy not a reference
     _boundary = P.boundary();
    }

   ~ShortParse()
    {
    }

   bool contains( const VertSet &S ) const;

   const VertSet& boundary() const { return _boundary; }
   //
   const VertSet& interior() const { return _interior; }

   // Number of vertices in the interior.
   //
   vertNum interiorLen() const { return _interior.size(); }

   // Number of vertices in the boundary.
   //
   vertNum boundarySize() const { return _boundary.size(); }

   // equality test
   //
   bool operator==(const ShortParse &S) const;

   // comparisions
   //
   bool operator<(const ShortParse &S) const;
   //
   bool operator>(const ShortParse &S) const
    {
      return S.operator<(*this);
    }

   /*
    * Sort for direct comparisons/searching
    */
    void sort() const  // only const in theory.
     {
        CAST(_boundary,VertSet).simpleSortUp();
        CAST(_interior,VertSet).simpleSortUp();
     };
 };

inline ostream& operator<<(ostream &o, const ShortParse &S)
 {
  return o << "{I: " << S.interior() << " B: " << S.boundary() << "}";
 }

//typedef Array<ShortParse> PParse;
typedef SortableArray<ShortParse> PParse;

// Database of the above.
//
//static 
class ShortParses
 {
private:

   // change!
   Array< PParse > *_sList;

   // Lookup indices.
   //
   int _at1, _at2;

   vertNum _bs;		// boundary size of ALL "ShortParse"s in PParse

public:

   ShortParses(vertNum len, vertNum bs) 
    {
      assert(len>0);
      assert(bs>0);

      _at1 = _at2 = -1;	// disable lookup's nextParse method.

      _sList = new Array< PParse >(len); 

//      cerr << "ShortParses() size " <<  _sList->get(0).size() << nl;

      _bs = bs; 	// fixed boundary size.
    }
 
   ~ShortParses() { delete _sList; _sList = 0; }

   // Number of vertices in the boundary.
   //
   vertNum boundarySize() const { return _bs; }

   // Check for any parses of a length.
   //
   bool something(vertNum l); 

   //
   // Add partial parses to database.
   //
   void addParse( const ShortParse &P ) 
    { 
#ifdef DEBUG
//cerr << "addParse( " << P << " )\n";
//cerr << "addParse() of length " << P.interiorLen() << nl;
//cerr << "Init parse list: \n" << (*_sList)[P.interiorLen()] << nl;
#endif 
     assert( P.boundarySize() == boundarySize() );
     assert(_at1 != P.interiorLen());  // can't add while iterator is active.

     P.sort(); 
     (*_sList)[P.interiorLen()].append(P); 
//cerr << "Modified parse list: \n" << (*_sList)[P.interiorLen()] << nl; 
    }

#ifdef DEBUG 
void dump(vertNum i)
    { 
cerr << _sList->get(i).size() << " parses of interior length " << i << nl; 
cerr << (*_sList)[i] << nl;
    }
#endif

   // Start iterator for partial parses of length
   //
   void lookup(vertNum k); 
   //
   // return next parse of setup length.
   //
   const ShortParse* nextParse();  // zero if no more.

   // Check for a complete parse (l = (n-k+1)/2);
   //
   bool findEven(vertNum l);
   //
   bool findOdd(vertNum l);

 }; 

#endif
