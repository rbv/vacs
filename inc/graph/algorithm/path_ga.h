
/*tex
 
\file{path_ga.h}
\path{inc/graph/algorithm}
\title{Header simple path routines}
\classes{}
\makeheader
 
xet*/


#ifndef __path_ga_h
#define __path_ga_h

#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"


// Should use distance matrix if possible.
//
vertNum minPath( const AdjLists &L,
                  vertNum u, vertNum v, vertNum maxLen =0 );
vertNum minPath( const Graph &G, 
                  vertNum u, vertNum v, vertNum maxLen =0 );

// Same as above but also returns "a" minimum path.
//
// (Note: returns number of edges while Path P has one more vertex.)
//
vertNum minPath( Path &P, const AdjLists &L,
                  vertNum u, vertNum v, vertNum maxLen =0 );
//
vertNum minPath( Path &P, const Graph &G, 
                  vertNum u, vertNum v, vertNum maxLen =0 );

#endif
