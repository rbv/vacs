
/*tex
 
\file{matching_ga.h}
\path{inc/graph/algorithm}
\title{Matching algorithms}
\classes{}
\makeheader
 
xet*/

#ifndef __matching_h
#define __matching_h

#include "graph/graph.h"
#include "graph/stdgraph2.h"

//  Edges of the matching are (vert[i], m_match[i]) for 0 <= i < vert.size()
//
struct Matching
{
  VertArray vert, match;
};

/*
 * Maximum matching of a graph.  (fixedK = 0 implies no bound)
 */
//vertNum matching(const Graph &G );
//
vertNum matching(const Graph &G, Matching &M, vertNum fixedK = 0 );

/*
 * Fixed paramater version of above.
 */
//vertNum matching(const Graph &G, vertNum k);

#endif
