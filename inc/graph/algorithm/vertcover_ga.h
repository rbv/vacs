
/*tex
 
\file{vertcover_ga.h}
\path{inc}
\title{Header vertex cover membership}
\classes{}
\makeheader
 
xet*/


#ifndef __vertcover_ga_h
#define __vertcover_ga_h

#include "graph/stdgraph.h"


bool vcLazy( const Graph &G, vertNum k);
//
vertNum vcLazy( const Graph &G ); // finds minimum "k"

bool vcBrute( const Graph &G, int k);
//
vertNum vcBrute( const Graph &G );

#endif
