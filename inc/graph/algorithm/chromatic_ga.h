
/*tex
 
\file{chromatic_ga.h}
\path{inc/graph/algorithm}
\title{Header for graph utility functions}
\classes{}
\makeheader
 
xet*/

#ifndef __chromatic_h
#define __chromatic_h

#include "graph/graph.h"
#include "graph/indgrph.h"

#if 0
vertNum chromatic(const Graph &G, vertNum lbound = 0);

#else
vertNum chromatic( const InducedGraph &G );

inline vertNum chromatic( const Graph &G )
 {
  InducedGraph IG( G );

  return chromatic( IG );
 }
#endif

#endif
