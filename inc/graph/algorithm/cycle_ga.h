
/*tex
 
\file{cycle_ga.h}
\path{inc/graph/algorithm}
\title{Header for simple "cycle" graph functions}
\classes{}
\makeheader
 
xet*/


#ifndef __cycle_ga_H
#define __cycle_ga_H

#include "graph/stdgraph2.h"

/*
 * minimum cycle and girth routines.
 */
vertNum minCycle( const AdjLists &L,
                  vertNum v, vertNum maxLen =0 );
//
vertNum minCycle( const Graph &G, vertNum v, vertNum maxSize=0 );
//
vertNum girth( const AdjLists &L, vertNum maxLen=0 );
//
vertNum girth( const Graph &G, vertNum maxSize=0 );
//
// Same as above but also returns cycles.
//
vertNum minCycle( Cycle &C,
                  const AdjLists &L, 
                  vertNum v, vertNum maxLen=0 );
//
vertNum minCycle( Cycle &C,
                  const Graph &G, vertNum v, vertNum maxSize=0 );
//
vertNum girth( Cycle &C,
               const AdjLists &L, 
               vertNum maxLen=0 );
//
vertNum girth( Cycle &C,
               const Graph &G, vertNum maxSize=0 );

/*
 * Does the graph contain NO cycles?
 */
bool acyclic( const Graph &G );
//
class InducedGraph;
//
bool acyclic( const InducedGraph &G );

#endif

