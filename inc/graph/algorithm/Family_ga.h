
/*tex
 
\file{family_ga.h}
\path{inc/graph/algorithm}
\title{Single include file for graph families}
\classes{}
\makeheader
 
xet*/


#ifndef __family_ga_h
#define __family_ga_h

#include "graph/algorithm/chromatic_ga.h"
#include "graph/algorithm/clique_ga.h"
#include "graph/algorithm/genus_ga.h"		// planar & torodial
#include "graph/algorithm/maxpath_ga.h"
#include "graph/algorithm/vertcover_ga.h"
#include "graph/algorithm/pathwidth_ga.h"

#endif
