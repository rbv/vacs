
/***********************************************************************
 * Header for Labeled Forests. (See "iterset.h" and "forest.h")
 **********************************************************************/

/*tex

\file{lforest.h}
\path{inc/graph}
\title{Header for Labeles Forests (PCollection of labeled trees)}
\classes{LForest}
\makeheader

xet*/

#error	-- E R R O R --

#ifndef __lforest_h
#define __lforest_h

#include "graph/ltree.h"
#include "set/set.h"

/*
 * Class controlled collection of Trees (with built-in iterator).
 */
class LForest : public PCollection<LTree>
 {
private:

   PCollectionIter<LTree> *access;

public:

   /*
    * Constructors. 
    */ 
   LForest()
    {
      access = 0;
    }
   //
   LForest(LTree *T)
    {
      add(T);
      access = 0;
    }

   /*
    * Destructor.
    */
   ~LForest() { deleteAll(); delete access; }


   /*
    * Remove nonunique trees
    */
   void removeEqualities( )
    {
      PCollection<LTree>::removeEqualities( eqTest );
    }

   /*
    * Add a bit to assignment operator.
    */
   void operator=( const LForest &F )
    {
      PCollection<LTree>::operator=(F);
      if (access) 
       {
        delete access;
        access = 0;
       }
      //access = new PCollectionIter<LTree>(F);
    }


   /*
    * Add/Remove operations without pointers.
    */
   //void add(const LTree &T) { add(&T); }
   //
   //void remove(const LTree &T) { remove(&T); }

   /*
    * Iterator member functions.
    */
   LTree* operator()()
      { return access->operator()(); }

   LTree* prev()
      { return access->prev(); }

   LTree* next()
      { return access->next(); }

   /*
    * Must start iterator before use.
    */
   void start()
      { 
       if (access) access->start();
       else access = new PCollectionIter<LTree>(*this);
      }
   //
   void end()
      { 
       if (!access) access = new PCollectionIter<LTree>(*this);
       access->end();
      }

 };

#endif
