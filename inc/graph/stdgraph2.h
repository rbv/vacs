/*
 * Standard [template] types for graph classes.  
 * (Included by appropriate graph class header.)
 */

/*tex
 
\file{stdgraph2.h}
\path{inc/graph}
\title{Header for standard graph definitions (part 2)}
\classes{}
\makeheader
 
xet*/


#ifndef _stdgraph2_h
#define _stdgraph2_h

#include "stdtypes.h"
#include "array/sortarray.h"
#include "array/dyarray.h"
#include "array/matrix.h"

class Graph;

typedef Array<vertNum> VertArray;

typedef DyArray<vertNum> VertSet;

typedef DyArray<vertNum> Path;

typedef DyArray<vertNum> Cycle;


typedef SquareMatrix<vertNum> VertMatrix;

typedef SortableArray<vertNum> DegreeSequence;

typedef SquareMatrix<vertNum> DistanceMatrix;

typedef SquareMatrix<bool> AdjMatrix;

typedef DyArray<VertSet> AdjLists;

// typedef PIterSet<InducedGraph> Blocks;

//
istream& operator>>( istream& in, AdjLists& L );

#endif

