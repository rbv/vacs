
/***********************************************************************
 * Header for Adjacency Lists graph representation.
 **********************************************************************/

/*tex

\file{listgrep.h}
\path{inc/graph}
\title{Header for adjacency lists graph representation}
\classes{ AdjListsGRep, ROAdjListsGRep }
\makeheader

xet*/

#ifndef __listgrep_h
#define __listgrep_h

#include "graph/stdgraph.h"
#include "array/dyarray.h"

class bistream;
class bostream;

typedef DyArray< DyArray< vertNum > > GRepAdjLists;

/*
 * Current implementaion uses adjacency lists form of a graph.
 */
class AdjListsGRep
 {

protected:

  GRepAdjLists _adj;			// Adjacency lists of graph.

public:

  // Constructor: AdjListsGRep
  //
  AdjListsGRep( const AdjListsGRep &L, vertNum extra = 0 );

//  AdjListsGRep( class MatrixGRep &L ); // Constructor: MatrixGRep

  AdjListsGRep( vertNum order = 0 )	// Constructor: Empty graph with order. 
   {
    assert(order>=0);

    _adj.resize(order);
   }

  ~AdjListsGRep() {}			// Destructor.

  /*
   * Operations on vertices (nodes)
   */
  //
  vertNum order() const	{ return _adj.size(); }
  //
  void addNode(vertNum n)		// Add isolated vertices to graph.
   {
    assert( n>0 );
    _adj.extend(n);
   }
  //
  vertNum addNode() { addNode(1); return order()-1; }		
  //
  //
  void rmNode(vertNum v);		// Remove vertex v.
  //
  vertNum degree( vertNum u ) const
   { 
     assert(u>=0 && u<order());
     return _adj.get(u).size();
   }   
  //
  vertNum neighbor( vertNum u, vertNum n=0 ) const
   {
     assert(u>=0 && u<order());
     assert(n<degree(u));
     return _adj.get(u).get(n);
   }


  /*
   * Operations on edges.
   */
  edgeNum size() const;
  //
  void addEdge(vertNum, vertNum);	// Add edge.
  //
  void rmEdge(vertNum, vertNum);	// Remove edge from graph.
  //
  bool isEdge(vertNum, vertNum) const;	// Check for edges.
  //
  void contract(vertNum, vertNum);	// Contract edge from graph.

  /*
   * Binary operators.
   */
  bool operator==(const AdjListsGRep &G) const;		// Isomorphism check.

  void operator=( const AdjListsGRep &G );		// Bitwise copy .

 };

//---------------------------------------------------------------------

/*
 * READ-ONLY AdjListsGRep  (created from streams or copy constructor)
 */
class ROAdjListsGRep
 {

protected:

  vertNum _order;			// Order of graph.
  edgeNum *_sdeg;			// Sum degree sequence.
  vertNum *_adj;			// Packed adjacency lists.

public:

  ROAdjListsGRep( ) { _order = 0; }	// Constructor: empty

  ROAdjListsGRep( const ROAdjListsGRep &M );// Constructor: adjacency matrix.

  ROAdjListsGRep( const AdjListsGRep &M );// Constructor: adjacency matrix.

  ~ROAdjListsGRep()			// Destructor.
   {
    if (_order>0) { delete [] _sdeg; delete [] _adj; }
   }

  /*
   * Operations on vertices (nodes)
   */
  //
  vertNum order() const	{ return _order; }
  //
  void addNode(vertNum v) { (void) v; aassert(false); }
  //
  void rmNode(vertNum v) { (void) v; aassert(false); }

  // Degree of a vertex
  //
  vertNum degree( vertNum u ) const
   { 
     assert(u>=0 && u<order());
     
     if (u==0) return _sdeg[0];
     else      return _sdeg[u]-_sdeg[u-1];
   }   
  //
  vertNum neighbor( vertNum u, vertNum n=0 ) const
   {
     assert(u>=0 && u<order());
     assert(n<degree(u));

     if (u==0) return _adj[n];
     else      return _adj[_sdeg[u-1]+n];
   }


  /*
   * Operations on edges.
   */
  //
  edgeNum size() const		// Something sweet and easy!
   {
     if (_order==0) return 0;
     else 	    return _sdeg[_order-1]/2;
   }
  //
  void addEdge(vertNum u, vertNum v) { (void) u; (void) v; aassert(false); }
  //
  void rmEdge(vertNum u, vertNum v) { (void) u; (void) v; aassert(false); }
  //
  void contract(vertNum u, vertNum v) { (void) u; (void) v; aassert(false); }
  //
  bool isEdge(vertNum i, vertNum j) const;

  /*
   * Binary operators.
   */
  bool operator==(const ROAdjListsGRep &G) const;	// Isomorphism check.

  void operator=( const ROAdjListsGRep &G);		// Bitwise copy .

  friend bistream& operator>>( bistream &i, ROAdjListsGRep &G );

 };

//---------------------------------------------------------------------

/*
 * Other stream i/o stuff.
 */
bostream& operator<<( bostream &o, const AdjListsGRep &G );
bostream& operator<<( bostream &o, const ROAdjListsGRep &G );

//---------------------------------------------------------------------

#endif
