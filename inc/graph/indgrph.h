
/***********************************************************************
 * Header for induced subgraph.
 **********************************************************************/

/*tex

\file{indgrph.h}
\path{inc/graph}
\title{Header for vertex induced subgraph class}
\classes{InducedGraph}
\makeheader

\begin{desc}
An efficient way of taking a vertex induced subgraph for read-only
purposes.  However, some of the functionality of the subgraph is 
limited.   Repeat vertices may be included for an augmented subgraph.
\end{desc}

\begin{bugs}
If original graph changes so does the induced subgraph.
\end{bugs}

xet*/

#ifndef __igraph_h
#define __igraph_h

#include "graph/graph.h"
#include "array/dyarray.h"
#include "general/stream.h"

class BitVector;

/*
 * A graph with only a subset of the vertices (relabeled 1...k)
 */
class InducedGraph 
 {
private:
  vertNum _order;			// Number of vertices.

  VertSet index;			// Adjacency matrix of graph.

  const Graph *G;			// Referenced graph.

public:

  InducedGraph(const Graph &G_param);

  InducedGraph(const Graph &G_param, const VertSet &vertList);

  InducedGraph(const Graph &S_param, const VertArray &vertList, vertNum count);

  InducedGraph(const InducedGraph &S_param, const VertSet &vertList);
   
  InducedGraph(const InducedGraph &S_param, const VertArray &vertList, 
		vertNum count);
			
  ~InducedGraph() {}			// Destructor.

  const Graph* baseGraph() const { return G; }

  /*
   * Try not to ABUSE the following!
   */
  void setIndex( const VertSet &vertList );

  void setIndex( const VertArray &vertList, vertNum count );

  void setIndex( const BitVector &B );

  /*
   * Operations.
   */
  void rmNode(vertNum i)
   {
     assert(isNode(i));

     _order--;

     index.swap(i,_order);
     index.shrink(1);
   }

  vertNum reference(vertNum i) const
   {
     assert(isNode(i));

     return index.get(i);
   }

  void addNode(vertNum ref)	// usually ref = G.reference(i)
   {
     assert( ref >= 0 && ref < G->order());

     _order++;

     index.append(ref);
   }

  void minus(const VertSet &vertList) 
   {
    vertNum count = vertList.size();
    //
    int i; for (i=0; i<count; i++) index[vertList[i]]=_order;
    
    vertNum toSwap=_order-1;
    //
    for (i=_order-1; i>=0; --i)
     if ( index[i]==_order ) index.swap( i, toSwap--);
   
    index.shrink(count); 

    _order -= count;			// Warning! Assumes nodes are distinct.
   }

  void minus(const VertArray &vertList, vertNum count)
   {
    int i; for (i=0; i<count; i++) index[vertList[i]]=_order;
    
    vertNum toSwap=_order-1;
    //
    for (i=_order-1; i>=0; --i)
     if ( index[i]==_order ) index.swap( i, toSwap--);
   
    index.shrink(count); 

    _order -= count;
   }


  void nodeSwap(vertNum i, vertNum j)
   {
     assert(isNode(i) && isNode(j));

     index.swap(i,j);
   }

  /* 
   * Basic query functions.
   */
  bool isEdge(vertNum i, vertNum j) const	// Check for edges.
      { return G->isEdge(index[i], index[j]); }
  //
  bool isNode(vertNum i) const                 // Is this vertex present.
       { return (i>=0 && i<_order); }

  vertNum nodes() const { return _order; } 	// Number of vertices.
  //
  vertNum order() const	{ return _order; }

  edgeNum size() const;				// Number of edges.
  //
  edgeNum edges() const { return size(); }


  vertNum components() const;                   // return number of components.
  //
  vertNum components( Components &C ) const;

  bool connected() const;			// Is the subgraph connected?
//   { return ( components()==1 ); }


  bool isCutVertex( vertNum ) const;
  //
  vertNum getBlocks( Blocks &B ) const;

  void degVec(DegreeSequence &deg, int flag=0) const; // (flag=1 for sort)
  //
  // Note: deg passed in/out as a parameter since it maybe pre-allocated.

  // Comparison
  //
  bool operator==(const InducedGraph &I) const
   {

     cerr << "operator==(const InducedGraph &I) const\n";

     if ( _order == I._order && G == I.G && index == I.index ) return true;
     else return false;
   }

 };

/*
 * Stream i/o stuff.
 */
ostream& operator<<( ostream &o, const InducedGraph &G );

#endif
