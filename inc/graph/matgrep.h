
/***********************************************************************
 * Header for Adjacency matrix graph representation.
 **********************************************************************/

/*tex

\file{matgrep.h}
\path{inc/graph}
\title{Header for adjacency matrix graph representation}
\classes{ MatrixGRep, ROMatrixGRep }
\makeheader

xet*/

#ifndef __matgrep_h
#define __matgrep_h

#include "graph/stdgraph.h"

//
class bistream;
class bostream;

/*
 * Current implementaion uses adjacency matrix form of a graph.
 */
class MatrixGRep
 {

protected:

  vertNum _space;			// Current space allocated.

  vertNum _order;			// Number of vertices.

  bool **_adj;				// Adjacency matrix of graph.

  static bool **_allocate(vertNum n);	// Private free store routines.
  //
  static void _deallocate(bool **a);

public:

  // Constructor: MatrixGRep
  //
  MatrixGRep( const MatrixGRep &M, vertNum extra = 0 );

//  MatrixGRep( class AdjListsGRep &L ); 	// Constructor: AdjListsGRep

  MatrixGRep( vertNum order = 0 );	// Constructor: Empty graph with order. 

  ~MatrixGRep();			// Destructor.

  /*
   * Operations on vertices (nodes)
   */
  //
  vertNum order() const	{ return _order; }
  //
  vertNum degree(vertNum u) const;
  //
  vertNum neighbor(vertNum v, vertNum n=0) const;
  //
  void addNode(vertNum v);		// Add isolated vertices to graph.
  //
  vertNum addNode() { addNode(1); return order()-1; }
  //
  void rmNode(vertNum v);		// Remove vertex v.

  /*
   * Operations on edges.
   */
  //
  edgeNum size() const;
  //
  void addEdge(vertNum, vertNum);	// Add edge.
  //
  void rmEdge(vertNum, vertNum);	// Remove edge from graph.
  //
  void contract(vertNum, vertNum);	// Contract an edge.
  //
  bool isEdge(vertNum i, vertNum j) const	// Check for edges.
       { return _adj[i][j]; }


  /*
   * Binary operators.
   */
  bool operator==(const MatrixGRep &G) const;		// Isomorphism check.

  void operator=( const MatrixGRep &G );		// Bitwise copy .

  friend istream& operator>>( istream &i, MatrixGRep &G );

 };

//---------------------------------------------------------------------

/*
 * READ-ONLY MatrixGRep  (created from streams or copy constructor)
 */
class ROMatrixGRep
 {

protected:

  vertNum _order;			// Number of vertices.

  bool *_adj;				// Upper triangular adjacency matrix.

public:

  ROMatrixGRep( ) { _order = 0; }	// Constructor: empty

  ROMatrixGRep( const ROMatrixGRep &M );// Constructor: adjacency matrix.

  ~ROMatrixGRep();			// Destructor.

  /*
   * Operations on vertices (nodes)
   */
  //
  vertNum order() const	{ return _order; }
  //
  void addNode(vertNum v) { (void) v; aassert(false); }
  //
  void rmNode(vertNum v) { (void) v; aassert(false); }
  //
  vertNum degree(vertNum u) const;
  //
  vertNum neighbor(vertNum v, vertNum n=0) const;

  /*
   * Operations on edges.
   */
  //
  edgeNum size() const;
  //
  void addEdge(vertNum u, vertNum v) { (void) u; (void) v; aassert(false); }
  //
  void rmEdge(vertNum u, vertNum v) { (void) u; (void) v; aassert(false); }
  //
  void contract(vertNum u, vertNum v) { (void) u; (void) v; aassert(false); }
  //
  bool isEdge(vertNum i, vertNum j) const;

  /*
   * Binary operators.
   */
  bool operator==(const ROMatrixGRep &G) const;	// Isomorphism check.

  void operator=( const ROMatrixGRep &G);		// Bitwise copy .

  friend istream& operator>>( istream &i, ROMatrixGRep &G );

  friend bistream& operator>>( bistream &i, ROMatrixGRep &G );

 };

//---------------------------------------------------------------------

/*
 * Other stream i/o stuff.
 */
//
// -- use generic GGraph output --
//
//ostream& operator<<( ostream &o, const MatrixGRep &G );
//
istream& operator>>( istream &i, MatrixGRep &G );

//ostream& operator<<( ostream &o, const ROMatrixGRep &G );
//
bostream& operator<<( bostream &o, const ROMatrixGRep &G );
//
istream& operator>>( istream &i, ROMatrixGRep &G );

//---------------------------------------------------------------------

/*
 * Add edge to graph.
 */
inline void MatrixGRep::addEdge(vertNum i, vertNum j)
 {
  assert(i>=0 && i<_order && j>=0 && j<_order && i!=j);

  _adj[i][j] = _adj[j][i] = 1;
 }

/*
 * Remove edge from graph.
 */
inline void MatrixGRep::rmEdge(vertNum i, vertNum j)
 {
  assert(i>=0 && i<_order && j>=0 && j<_order && i!=j);

  _adj[i][j] = _adj[j][i] = 0;
 }

#endif
