
/***********************************************************************
 * Header for rooted trees.
 **********************************************************************/

/*tex

\file{rtree.h}
\path{inc/graph}
\title{Header for rooted trees}
\classes{RootedTree}
\makeheader

xet*/

#ifndef __roottree_h
#define __roottree_h

#include "tree.h"

//#include <iostream>
//

typedef SortableArray< vertNum > TreeRank;


/*
 * Rooted tree with chosen root.  (read-only object)
 */
class RootedTree : private Tree
 {
protected:

  vertNum _root;			// Root and parents
  //
  VertArray _parents;			// indices in adj list.

public:

  RootedTree( const Tree &T, vertNum root = 0 ); // : _parents(T.order()) 

  ~RootedTree() { } // Destructor.

  /*
   * Basic query functions promoted from base class Tree.
   */
  //
  Tree::isEdge;                         // SEE CLASS TREE FOR INFO.
  //
  Tree::isNode;
  //
  Tree::isLeaf;
  //
  Tree::leafParent;
  //
  Tree::neighbor;
  //
  Tree::getPath;
  //
  Tree::degree;
  //
  Tree::nodes;
  //
  Tree::order;
  //
  Tree::size;
  //
  Tree::edges;

  /*
   * Binary operators.
   */
  bool operator==(const RootedTree &T) const	// Isomorphism check.
   {
    if (rank() == T.rank()) return true;
    else 		    return false;
   }
  //
  RootedTree& operator=(const RootedTree &T);	// Bitwise copy.

  /*
   * Multi-query operations.
   */
  TreeRank rank() const
   {
     return Tree::rank(_root);
   }
  // 
  Tree::degVec;
  //
  Tree::neighborList;
  //
  Tree::center;

  /*
   * Search techniques.
   */
  void BFS(VertArray &seq) const
   {
     Tree::BFS(seq,_root);
   }
  //
  void DFS(VertArray &seq) const
   {
     Tree::DFS(seq,_root);
   }

  /*
   * UNIQUE rooted tree stuff.
   */
  vertNum parent(vertNum v) const
   {
    assert( isNode(v) );
    assert( v != _root );

    return neighbor(v, _parents.get(v));
   }
  //
  vertNum root() const
   {
    return _root;
   }
  //
  vertNum child(vertNum v, vertNum i) const
   {
     return ( v == root() || i < _parents.get(v)) ? neighbor(v,i) 
                                                  : neighbor(v,i+1);
   }
  //
  vertNum children(vertNum v) const
   {
     return (v==_root) ? degree(v) : degree(v)-1;
   }

  /*
   * Stream i/o stuff.
   */
  friend ostream& operator<<( ostream &o, const RootedTree &T );

 };


#endif
