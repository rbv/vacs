
/***********************************************************************
 * Header for Forests and Labeled Forests. 
 **********************************************************************/

/*tex

\file{forest.h}
\path{inc/graph}
\title{Header for forests and labeled forests (PCollection of trees)}
\classes{Forest,LForest}
\makeheader

xet*/

#ifndef __forest_h
#define __forest_h

//#include "graph/ltree.h"
#include "set/iterset.h"

class Tree;
class LTree;

typedef PIterSet< Tree > Forest;

typedef PIterSet< LTree > LForest;

#endif
