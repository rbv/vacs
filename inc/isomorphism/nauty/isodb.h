
/***********************************************************************
 * NAUTY-specific isomorphism database base classes
 * This header should never be included explicitly!
 **********************************************************************/


#ifndef __nauty_isodb_h
#define __nauty_isodb_h

// If defined, all results are double-checked using SD isomorphism
//#define CHECK_ISO

#include "general/bitvect.h"

#include "graph/bgraph.h"
#include "isomorphism/schmidt_druffel/isodb.h"

namespace NAUTY_Iso
{

extern "C"
{
#define MAXN 32
// I would assume it's beneficial not to use 64 bit ints if we stick to small graphs
#define WORDSIZE 32
#include "nauty.h"
#include "nautinv.h"
// MAXM now defined to 1
}

class IsoDBItem2;

class IsoDBItem1 : public IsoDBItem1Base
{
   iodeclarations( NAUTY_Iso::IsoDBItem1 )

private:
   unsigned long long hash;

public:

#ifndef NDEBUG
   // Keep valgrind happy
   IsoDBItem1() { memset( this, 0, sizeof *this ); }
#endif

   bool operator==( const IsoDBItem1 &I ) const;

   size_t getHash() const { return (size_t)hash; }

   friend void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary );
};

class IsoDBItem2
{
   iodeclarations( IsoDBItem2 )

private:
   Array< setword > canonform;

#ifdef CHECK_ISO
   // For testing
   Schmidt_Druffel::IsoDBItem2 sd_check;
   RBBPG graph;
#endif

public:
   //bool isOK() const { return canonform.size() >= 0 && canonform.size() <= 500; }

   bool operator==( const IsoDBItem2 &I ) const;

   friend void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary );
};

void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary = false );

void printIsoAlgStats( ostream &os );

}  // end namespace

#endif
