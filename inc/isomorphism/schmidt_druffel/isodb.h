
/***********************************************************************
 * Schmidt-Druffel-specific isomorphism database base classes
 * This header should never be included explicitly!
 **********************************************************************/


#ifndef __sd_isodb_h
#define __sd_isodb_h

#include "isomorphism/schmidt_druffel/vertpart.h"

namespace Schmidt_Druffel
{

#define DegSeqSize 20

class IsoDBItem1 : public IsoDBItem1Base
{
   iodeclarations( Schmidt_Druffel::IsoDBItem1 )

private:

   //vertNum vertices;
   //vertNum edges;
   char degSeq[ DegSeqSize ];
   unsigned long long hash;

public:

#ifndef NDEBUG
   // Keep valgrind happy
   IsoDBItem1() { memset( this, 0, sizeof *this ); }
#endif

   void init( const RBBPG& G, IsomorphismBoundaryType );

   bool operator==( const IsoDBItem1 &I ) const;

   size_t getHash() const { return (size_t)hash; }
};

class IsoDBItem2
{
   iodeclarations( IsoDBItem2 )

private:

   VertPartition vertPart;
   DistanceMatrix distMat;
   IsomorphismBoundaryType bType;

public:
   //bool isOK() const { return distMat.dim() >= 0 && distMat.dim() <= 20; }

   IsoDBItem2() {}

   void init( const RBBPG& G, IsomorphismBoundaryType );

   bool operator==( const IsoDBItem2 &I ) const;
};

void initIsoDBItems( const RBBPG &G, IsoDBItem1 &I1, IsoDBItem2 &I2, bool ignoreBoundary = false );

void printIsoAlgStats( ostream &os );

}  // end namespace

#endif
