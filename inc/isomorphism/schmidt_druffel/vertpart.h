
/***********************************************************************
 * Header for Schmidt-Druffel isomorphism support data types
 **********************************************************************/

/*tex
 
\file{isomorphism.h}
\path{inc/isomorphism/isomorphim}
\title{Header for graph/bpg isomorphism classes/functions}
\classes{VertPartition}
\makeheader
 
xet*/


#ifndef __vertpart_H
#define __vertpart_H

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include <iostream>

/*
 * Data structure for partitioning vertices into characterizing classes.
 */
class VertPartition
 {
   iodeclarations( VertPartition )

public:
  vertNum    partitions;	// need to protect this!
  VertArray  class_num;
  VertMatrix class_rep;
  VertArray  class_cnt;

  /*
   * Construct (allocate) class vectors using distance matrix.
   */
  VertPartition( vertNum, const DistanceMatrix& );	

  VertPartition();	

  void init( vertNum, const DistanceMatrix& );	

  ~VertPartition() {}

#ifdef WATCH2
   void print()  
    {
     cerr << "VertPartition" << nl;
     cerr << (int) partitions << nl;
     cerr << class_num << nl;
     cerr << class_rep << nl;
     cerr << class_cnt << nl;
    }
#endif
 };

bistream& operator>>( bistream& s, VertPartition& vp );
bostream& operator<<( bostream& s, const VertPartition& vp );

/*
 * Data passed between Isomorphism levels (recursive infomation).
 */
struct VertSearchItem
 {
  public:
  vertNum  class_cnt, 		// also need to protect!
           class_num,
           vertex,
           map_to;

#ifdef WATCH2
  void print(int i=0)
   {
    cerr << i << " : " << (int) class_cnt << ' ' << (int) class_num 
                     << ' ' << (int) vertex << ' ' << (int) map_to << nl;
   }
#endif

   VertSearchItem(){}

   // for array class (temporarily)
   bool operator==( const VertSearchItem& ) const
    { aassert(false); return false; } // kill compile warning

   // Mike!  Must define these for SortableArray
   //
   bool operator>( const VertSearchItem& ) const { aassert(0); return false; }
   bool operator<( const VertSearchItem& ) const { aassert(0); return false; }

   friend ostream& operator<<(ostream &i, const VertSearchItem &v);
   friend int compare( const VertSearchItem*, const VertSearchItem* );
 };

template<>
int compare( const VertSearchItem*, const VertSearchItem* );

inline  ostream& operator<<(ostream &i, const VertSearchItem &v)
{
    return i << (int) v.class_cnt << ' ' << (int) v.class_num 
                     << ' ' << (int) v.vertex << ' ' << (int) v.map_to << nl;
}

//inline istream& operator>>(istream &o, VertSearchItem&) _iodefbody_

typedef SortableArray<VertSearchItem> VertSearch;

//
// Should execute the following in order listed.
//

/*
 * Initialize (unify) start up vectors if possible.
 */
bool find_class_map( const VertPartition &class1,
                     const VertPartition &class2,
                     VertArray &class_map );


/*
 * Build initial isomorphism search structure.
 */
void build_search( const VertPartition &class1,
                   const VertPartition &class2,
                   const VertArray &class_map,
                   VertSearch &S1, VertSearch &S2);

// Utility function in iso_bpg.c
//
bool isomorphic( 
   const DistanceMatrix& D1,
   const DistanceMatrix& D2,
   const VertPartition& class1,
   const VertPartition& class2,
   IsomorphismBoundaryType bType
);

/*
 * ISOMORPHISM function based on distance matrices.
 *
 * Currently, if boundary > 0 a "free boundary" isomorphism is done.
 * If boundary < 0 a "fixed boundary" isomorphism is done.
 *
 */
bool G_iso( const DistanceMatrix &D1,         // Distance Matrices. 
            const DistanceMatrix &D2,
            VertSearch &S1,                   // Unified search structures.
            VertSearch &S2,  
//          vertNum order,                    // Order of graphs.
            vertNum level,                    // Count of mapped vertices.
            int boundary = 0 );               // Common boundary size.
					      // (negative for fixed boundary)


#endif
