
/*tex
 
\file{iso_[l]tree.h}
\path{inc/isomorphism}
\title{Header auxilary tree iso routines.}
\classes{}
\makeheader
 
xet*/


#ifndef __iso_l_tree_H
#define __iso_l_tree_H

#include "stdtypes.h"

class Tree;
//
bool eqTest( const Tree &T1, const Tree &T2 );

class LTree;
//
bool eqTest( const LTree &T1, const LTree &T2 );

#endif
