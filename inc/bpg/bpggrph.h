 
// -------------------------------------------
// ---------------- bpggrph.h ----------------
// -------------------------------------------
 
/*tex
\file{bpggrph.h}
\path{inc/bpg}
\title{Definitions of BPG ``graph'' structures}
\classes{}
\makeheader
xet*/

#ifndef _bpggrph_h
#define _bpggrph_h
 
#include "stdtypes.h"
 
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "isomorphism/iso_graph.h"
 
#include "bpg/bpg.h"
 
// Return the degree sequence of the graph.
// type = noBoundary    ==> sort whole sequence
// type = freeBoundary  ==> sort bndry part; sort non-bndry part
// type = fixedBoundary ==> sort non-bndry part
//
// Except for noBoundary, the boundary vertices occupy the initial
// part of the sequence.
//
// The passed DegreeSequence is resized on entry.
//
void degreeSequence( const BPG&, DegreeSequence&, IsomorphismBoundaryType );
 

// Calculate the adjacency matrix of the graph.  
//
// The first boundarySize() entries are the boundary.
// The passed AdjMatrix must have dimension #vertices on entry.
//
void adjacencyMatrix( const BPG&, AdjMatrix& );

// Calculate the distance matrix of the graph.
//
// The passed DistanceMatrix must have dimension #vertices on entry.
//
void distanceMatrix( const BPG&, DistanceMatrix& );

#endif
