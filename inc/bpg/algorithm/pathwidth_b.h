
// -------------------------------------------
// ------------ pathwidth_b.h ----------------
// -------------------------------------------

/*tex
 
\file{pathwidth_b.h}
\path{inc/bpg/algorithm}
\title{Header for fixed-k pathwidth algorithm}
\classes{}
\makeheader

xet*/


#ifndef _pathwidth_b_h
#define _pathwidth_b_h

#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "array/array.h"
//#include "array/sortarray.h"
#include "set/uoc.h"
#include "set/seq.h"
#include "set/iterset.h"

typedef SortedSequence<vertNum> SortedNums;
//
typedef Array<SortedNums> IntervalModel;

class TypicalSeq : public VertSet
 {

public:

  TypicalSeq() { }

  // from arbitrary nonnegative integer sequence
  //
  TypicalSeq( const VertSet &S );

  // from arbitrary integer -- length 1 sequence
  //
  TypicalSeq( const vertNum &w ) : VertSet(1) 
   {
    (*this)[0] = w;
   }

  // concatenation of two typical sequences
  //
  TypicalSeq( const TypicalSeq &S1, const TypicalSeq &S2 );

  ~TypicalSeq() {}

  vertNum max() const;

  const TypicalSeq& addScalar( vertNum scalar );

  void split1( int index, TypicalSeq &S1, TypicalSeq &S2 ) const;

  void split2( int index, TypicalSeq &S1, TypicalSeq &S2 ) const;

  // comparision
  //
  bool operator==( const TypicalSeq &S ) const
  {
   return VertSet::operator==( CAST( S, VertSet ) );
  }


 };

#if 0
inline ostream& operator<<( ostream &o, const TypicalSeq &T )
{
  return o << CAST(T, VertSet) << nl;
}
#endif

typedef Array<TypicalSeq> TypicalList;

class PWChar
 {
  //iodeclarations( PWChar )

  friend class PWChar_introduceIter;

  IntervalModel _Z;
  //
  TypicalList _L;

public:

  PWChar() {}

  static PWChar* nullChar() 
   { 
    PWChar *pC = new PWChar;

#if 0
    SortedNums Z0;		// empty set of ..
    TypicalSeq T0;		
    T0.append(0); 		// .. length one

    pC->_Z.append(Z0); 
    pC->_L.append(T0); 
#endif

    return pC;
   }

  static PWChar* K1Char( vertNum v ) 
   { 
    PWChar *pC = new PWChar;

    SortedNums Z0;		
    Z0.insert(v);		// single vertex in a decomposition of ..
    TypicalSeq T0(1);		// .. length one	

    pC->_Z.append(Z0); 
    pC->_L.append(T0); 

    return pC;
   }

  PWChar( const IntervalModel &Z, const TypicalList &L )
   {
    _Z = Z;
    _L = L;
   }

  ~PWChar() {}

  bool operator==( const PWChar &C ) const
   {
     return (_Z == C._Z && _L == C._L);
   }

  void operator=( const PWChar &C )
   {
    _Z = C._Z;
    _L = C._L;
   }

  // pathwidth of this characteristic
  //
  vertNum width() const;

  void forget( vertNum i );

  bool hasVertex( vertNum i ) const;

  bool supportsEdge( vertNum i, vertNum j ) const;

  int minIndex( vertNum u ) const;

  int maxIndex( vertNum u ) const;

  /*
   * Access methods
   */
  const IntervalModel& Z() const { return _Z; }

  const TypicalList& L() const { return _L; }

 };

inline ostream& operator<<( ostream &o, const PWChar& C )
 {
  o << "Interval:\n" << C.Z() << nl;
  o << "TypicalList:\n" <<  C.L() << nl;
  return o;
 }


class PWChar_introduceIter
 {
   // Constructor arguments consist of splice characteristic, introduce vertex,
   // maximum width of new characteristics, and optional overlap restrictions.
   //
   const PWChar *pC;
   vertNum v;
   vertNum k;
   const VertSet *pO;

   int Zlen;			// Length of characteristic for easy reference.

   int startZ, endZ;		// 0 to |Z|-1
   int maxStartZ, minEndZ;	// Interval cover restrictions
				// -1 or |Z| indicates no restriction.

   int startL,    		// if -1 then we don't split left Z_i;
       endL;			// if |L_j| then we don't split right Z_j;

   void startInterval()
    {
     startZ = endZ = 0;
     startL = endL = -1;

     if (minEndZ > -1)
      {
       endZ = minEndZ;
       endL = 0;
      }
    }

   // Worker methods
   //
   void nextInterval();	
   //
   bool maxRangeConflicts();
   //
   vertNum gapWidth(int Zidx, int Lidx) const;
   //
   vertNum startWidth() const	// including new vertex v
    {
     return gapWidth(startZ, startL);
    }
   //
   vertNum endWidth() const	// including new vertex v
    {
     return gapWidth(endZ, endL);
    }

   void mergeDuplicates( IntervalModel &newZ, TypicalList &newL );


public:

   PWChar_introduceIter
   ( 
     	const PWChar &C, 
	vertNum vert, 
        vertNum maxWidth,
        const VertSet *Overlap = 0
   );

   ~PWChar_introduceIter() {}

   bool next( PWChar &NextC );
 };

// Exporting
//
typedef PIterSet<PWChar> PWState;
//
bool pathwidth( const class BPG& B, int k, PWState* PW_state = 0 );

#if 0
inline ostream& operator<<(ostream &o, const PWState &S )
 {
   //return o << CAST( S, PIterSet<PWChar> );
   //return o << CAST( S, PCollection<PWChar> );
   //return o << CAST( S, PCollection<IntervalModel> );
   return o << S.size();
 }
#endif
#endif
