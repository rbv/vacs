
// -------------------------------------------
// -------------- maxpath_b.h ----------------
// -------------------------------------------

/*tex
 
\file{maxpath_b.h}
\path{inc/bpg/algorithm}
\title{Header for maximum path functions}
\classes{MaxPathState}
\makeheader

xet*/


#ifndef _maxpath_h
#define _maxpath_h

#include <iostream>

#include "array/array.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"


// Right now can only do boundary size TWO!
//
class MaxPathState 
 {
private:

  bool _bndryEj;		// Is there an edge between the two
				// boundary vertices?

  int _maxPath[2];		// Longest path starting at boundary vertex.

  int _maxFound;		// Maximum known path so far.

public:

   MaxPathState() 
    {
      _bndryEj = false; 
      _maxPath[0] = _maxPath[1] = _maxFound = 0;
    }
   //
   ~MaxPathState() {}
 
   // Allow one to reuse class for another BPG string.
   //
   MaxPathState* init()
    {
      _bndryEj = false; 
      _maxPath[0] = _maxPath[1] = _maxFound = 0;

      return this;
    }

   /*
    * Update methods.
    */
   void newBoundaryVert(vertNum v);
   //
   void addBoundaryEdge(vertNum u, vertNum v);

   // inquire status
   //
   int maxPath() const { return _maxFound; }


   void operator=( const MaxPathState &s ) // Assignment
    {
     _bndryEj = s._bndryEj;

     _maxPath[0] = s._maxPath[0];
     _maxPath[1] = s._maxPath[1];

     _maxFound = s._maxFound;
    }

   bool operator==( const MaxPathState &s ) const // Equivalence class test
    {
     return (
	     _bndryEj == s._bndryEj &&
	     _maxPath[0] == s._maxPath[0] &&
	     _maxPath[1] == s._maxPath[1] &&
	     _maxFound == s._maxFound
            );
    }

   friend ostream& operator<<( ostream &o, const MaxPathState &s )
    {
      return o << (int) s.maxPath(); 
    }
 };

//----------------------------------------------------------------------

/*
 * Main contribution.
 */
bool maxPath( const BPG& B, int k, MaxPathState* MP_state = 0 );

#endif
