// -------------------------------------------
// ------------ embedding_b.h ----------------
// -------------------------------------------

#ifndef _embedding_b_h
#define _embedding_b_h

class EmbedState;

// This is just a subset of the full class definition in embedding_b.c!
class EmbedStateWrapper
{
   EmbedState *state;

public:
   // Create state for the disconnected bs-boundaried graph w/ no internal vertices
   EmbedStateWrapper( int bs, int handles, int crosscaps, int apices, int covering_faces );
   EmbedStateWrapper( const EmbedStateWrapper &rhs );
   ~EmbedStateWrapper();

   void compute( const BPG &G );

   // Number of embeddings
   int size() const;

   // Cost of building this state (totalsize)
   int cost() const;

   bool isEmpty() const;

   bool leq( const EmbedStateWrapper &rhs ) const;
   bool identical( const EmbedStateWrapper &rhs ) const;
};

#endif
