
/*tex
 
\file{acyclic_b.h}
\path{inc/bpg/algorithm}
\title{Header for no cycle test}
\classes{}
\makeheader

xet*/


#ifndef _acyclic_b_h
#define _acyclic_b_h

class BPG;

bool acyclic(const BPG &B);

#endif
