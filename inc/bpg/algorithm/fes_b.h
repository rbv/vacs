
// -------------------------------------------
// -------------- fes_b.h --------------------
// -------------------------------------------

/*tex
 
\file{fes_b.h}
\path{inc/bpg/algorithm}
\title{Header for feedback edge set functions}
\classes{}
\makeheader

xet*/


#ifndef _fes_b_h
#define _fes_b_h

#include "bpg/bpg.h"

bool fes1Member( const BPG& G );

bool fes2Member( const BPG& G );

bool fes3Member( const BPG& G );

#endif

