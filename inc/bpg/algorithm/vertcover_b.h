
// -------------------------------------------
// ------------ vertcover_b.h ----------------
// -------------------------------------------

/*tex
 
\file{vertcover_b.h}
\path{inc/bpg/algorithm}
\title{Header for vertex cover functions}
\classes{}
\makeheader

xet*/


#ifndef _vertcover_h
#define _vertcover_h

#include "array/array.h"


// Exporting
//
typedef Array<vertNum> vertexCoverState;
//
bool vertCover( const class BPG& B, int k, vertexCoverState* VC_state = 0 );

#endif
