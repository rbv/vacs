
// -------------------------------------------
// -------------- fvs_b.h --------------------
// -------------------------------------------

/*tex
 
\file{fvs_b.h}
\path{inc/bpg/algorithm}
\title{Header for feedback vertex set functions}
\classes{VertUnary,Parks,Witness}
\makeheader

xet*/


#ifndef _fvs_b_h
#define _fvs_b_h

#include "array/array.h"
#include "graph/stdgraph.h"
#include "graph/ltree.h"
#include "graph/forest.h"	// included in ltree.h (tree.h)
#include "bpg/bpg.h"
#include "set/iterset.h"
#include "vacs/freestore.h"
#include "family/probinfo.h"

// debug/option control
//
#define DFVS 
#define ONLYUP2K
#define DOTIGHTEN

#ifdef DFVS
#include <iostream>
#endif

typedef LForest Park;
//
bool sameTree( const Park &P, Label v1, Label v2 );
//
Park* connectTrees( const Park &P, Label v1, Label v2 );
//
class Parks* splitTree( const Park &P, Label v1, Label v2 );

//-----------------------------------------------------------------

/*
 * Functional classes for PCollection's applyUnary member.
 */
class VertUnary
 {
private:
  static vertNum _v;

public:
  void setLabel(vertNum v) { _v=v; }

  static void addBoundaryTree( Park &park )
   {
      LTree* lboundary = new LTree( (Label) _v );

      park.add( lboundary );
   }

  static void pullOffBoundary( Park &park );

 };

extern VertUnary vertUnary;	// in src/bpg/fvs.c

//-----------------------------------------------------------------

class Parks : public PIterSet<Park>
 {

   FreeStoreTest( Parks )

public:

  Parks() {}

  // Parks( const Parks& ) { aassert(false); } // kev

  ~Parks() {}
 
  void addParks( const Parks &parks );

  void doVertOpCase1( vertNum v )
   {
    vertUnary.setLabel(v); 

    //applyUnary( (void (*)(Park&)) VertUnary::pullOffBoundary );
    //
    applyUnary( VertUnary::pullOffBoundary );

//cerr << "finished applyUnary - vertUnary::pullOffBoundary\n";

#ifndef DOTIGHTEN
    //cerr << "removeEqualities();\n";
    removeEqualities();
#endif

    //
    //applyUnary( (void (*)(Park&)) VertUnary::addBoundaryTree );
    //
    applyUnary( VertUnary::addBoundaryTree );

//cerr << "finished applyUnary - vertUnary::addBoundaryTree\n";

   }   
  //
  void doVertOpCase1Union( vertNum v )
   {
    vertUnary.setLabel(v); 

    //applyUnary( (void (*)(Park&)) VertUnary::addBoundaryTree );
    //
    applyUnary( VertUnary::addBoundaryTree );

//cerr << "finished applyUnary - vertUnary::addBoundaryTree\n";

   }   

  void doVertOpCase2( vertNum v ) { (void) v; }
  //
  void doVertOpCase2Minus( vertNum v )
   {
    vertUnary.setLabel(v); 

    //applyUnary( (void (*)(Park&)) VertUnary::pullOffBoundary );
    //
    applyUnary( VertUnary::pullOffBoundary );

//cerr << "finished applyUnary - vertUnary::pullOffBoundary\n";

#ifndef DOTIGHTEN
    //cerr << "removeEqualities();\n";
    removeEqualities();
#endif

   }   

  void doEdgeOpCase4Eq( Parks &p, vertNum v1, vertNum v2 );
  //
  void doEdgeOpCase4Plus1( Parks &p, vertNum v1, vertNum v2 );

  void getIsolated( Parks &p, vertNum v1, vertNum v2 );

  bool operator==( const Parks &p ) const
   {
    return PIterSet<Park>::operator==( CAST(p,PIterSet<Park>) );
   }
 };

class Witness : public Parks 
 {
    FreeStoreTest( Witness )

private:

    vertNum _killed;

public:
    Witness() {}

    ~Witness() {}

    void setPark( Park &park ) 
     { 
       //purgeAll(); 	//It is assumed that this is done at the start.
       //
       assert( size() == 0 );

       Park *p = new Park(park);

       add( p );
     }

#if 0
    void setParks( const Witness &w ) 
     { 
       purgeAll(); 
   
       _killed = w.killed();

       Parks::copy( w );  
     }

#else

    void setParks( const Parks &parks ) 
     { 
       purgeAll(); 
   
       Parks::copy( parks );  
     }
#endif

    vertNum killed() const { return _killed; }

    void setKilled( vertNum kill ) { assert( kill>=0 ); _killed = kill; }
    

    Witness& operator=( const Witness &w )		// Assignment
     {
      Parks::operator=( w );

      _killed = w._killed;

      return *this;
     }

    bool operator==( const Witness &w ) const	// Equivalence class test
     {
      return ( _killed == w.killed() && Parks::operator==((const Parks&) w) );
     }

 };


//----------------------------------------------------------------------
//----------------------------------------------------------------------

#ifdef DOTIGHTEN
/*
 * An array of integers (# of kill vertices including each boundary subset)
 * plus the combined set of witnes forests.
 */
class fvsState : public VertArray 
{
private:
 
  vertNum _maxKill;

  Array<Parks> _witnesses;  // Indexed by number of kill vertices used.

public:

#ifdef DOTIGHTEN
   friend ostream& operator<<( ostream &o, const fvsState &s )
   {
      return o << CAST( s, VertArray ) << nl
               << s.getParks() << nl;
   }
#endif

  //fvsState( const Array< Witness > &dpState );

  fvsState() 
   { 
     _maxKill = ProbInfo::getInt(ProbInfo::ProblemNumber);
     _witnesses.resize(_maxKill+1);
   }

  // reset the state to a freash clean one.
  //
  void reset(vertNum two2bSize);

  ~fvsState() {}

  fvsState operator=( const fvsState & )
   {
    assert(false);
    return *this;
   }  

  bool operator==( const fvsState &S ) const;

  const Array<Parks>& getParks() const { return _witnesses; }

  void addParks(int index, const Parks &P)
   {
    assert(_maxKill>=0);
    if (index <= _maxKill) _witnesses[index].addParks(P);
   }
};

void tighten( Array<Witness> &C, fvsState& S );	// in family/fix_fvs.c

#else

typedef Array<Witness> fvsState;

// If state[i] >= state[k] where i \subseteq k then forget state[i]'s
// witnesses and update decrease state[i].
//
void tighten( fvsState& C );	// in family/fix_fvs.c

#endif

//----------------------------------------------------------------------

inline ostream& operator<<( ostream &o, const Witness &w )
 {
   return o << "Killed: " << w.killed() << nl << 
		CAST( w, PIterSet<Park> ) << nl;
 }
inline ostream& operator<<( ostream &o, const Parks &w )
 {
   return o << CAST( w, PIterSet<Park> ) << nl;
 }

//----------------------------------------------------------------------

/*
 * Main contributions.
 */
bool fvs( const BPG& B, int k, fvsState* FVS_state = 0 );
//
bool acyclic( const class InducedGraph &G );

#endif
