
/*tex
 
\file{family_b.h}
\path{inc/bpg/algorithm}
\title{Single include file for bpg families}
\classes{}
\makeheader
 
xet*/


#ifndef __family_b_h
#define __family_b_h

#include "bpg/algorithm/embedding_b.h"
#include "bpg/algorithm/fvs_b.h"
#include "bpg/algorithm/maxpath_b.h"
#include "bpg/algorithm/vertcover_b.h"

#endif
