
// ------------------------------------------
// --------------- oparray.h ----------------
// ------------------------------------------

/*tex
 
\file{oparray.h}
\path{inc/bpg}
\title{Header for operator array class}
\classes{OpArray}
\makeheader
 
xet*/


#ifndef _oparray_h
#define _oparray_h

//   oparray.h
//   Header for class OpArray

#include "bpg/operator.h"
#include "array/array.h"

#define MAX_TPARSE_LEN 64

class OpArray : private Array<Operator>
{

   iodeclarations( OpArray )

// Most of the functions here are meant for class BPG
//
protected:

	// Constructors, copying -------------------------

	// create empty (no new is performed)
	//
	OpArray() : Array<Operator>() {}

	// create with given size
	//
	OpArray( int size ) : Array<Operator>(size) {}

	// copy constructor
	//
	OpArray( const OpArray& a ) : Array<Operator>(a) {}

	// copy constructor, with extra space
	//
	OpArray( const OpArray& a, int extra ) : Array<Operator>(a, extra) {}

	// assignment operator
	//
	void operator=( const OpArray& a )
	   { Array<Operator>::operator=( a ); }

	// destructor
	//
	~OpArray() {}


	// Info, Access, Modifying -----------------------------

	// Return the position of the ith vertex operator.
	//
	int ithVertex( int ) const;

	// Return the position of the ith edge operator.
	//
	int ithEdge( int ) const;

	// Return number of vertex operators
	//
	int numVertexOps() const;

	// Return number of edge operators
	//
	int numEdgeOps() const;


public:
	// Searching -------------------------------------

	// The vertex search routine has three forms: next vertex,
	// next vertex equal (or incident) to parameter, 
	// and next vertex equal to one of the parameters.
	// They return the position of the target, or -1 if they run
	// off either end.
	//
	int nextVertexOp( int ) const;
	int nextVertexOp( int, Operator anOp ) const;
	int nextVertexOp( int, Operator vOp1, Operator vOp2 ) const;

	int prevVertexOp( int ) const;
	int prevVertexOp( int, Operator anOp ) const;
	int prevVertexOp( int, Operator vOp1, Operator vOp2 ) const;

	// The edge search routine has three forms:  next edge,
	// next edge equal to parameter, and next edge that
	// has a vertex in common with parameter (vertex or edge).
	//
	int nextEdgeOp( int ) const;
	int nextEdgeOp( int, Operator eOp ) const;
	int nextEdgeOpIncident( int, Operator eOp ) const;

	int prevEdgeOp( int ) const;
	int prevEdgeOp( int, Operator eOp ) const;
	int prevEdgeOpIncident( int, Operator eOp ) const;

	// --- promotions ---
	//
	Array<Operator>::size;
	Array<Operator>::put;
	Array<Operator>::get;
	Array<Operator>::baseAddr;
	Array<Operator>::hash32;
	Array<Operator>::hash64;

	// cfront sorry...
	//
	void resize( int newSize ) 
         { 
           //Array<Operator>::resize(newSize); 
           Array<Operator>::resize(newSize, Operator::noOp()); 
         }

        // Trim some number of non-noop operators from the end, and resize
        // Afterwards the last op is not a noop (but size may be 0)
        void trim( int amount = 0 );
};

#endif
