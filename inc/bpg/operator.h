
// -------------------------------------------
// --------------- operator.h ----------------
// -------------------------------------------

/*tex
 
\file{operator.h}
\path{inc/bpg}
\title{Header for operator class}
\classes{Operator}
\makeheader
 
xet*/


#ifndef _operator_h
#define _operator_h

#include "general/stream.h"
#include "general/random.h"
#include "stdtypes.h"
#include "graph/stdgraph.h"

// Translation unit testing
#ifdef MODULE_OK
#define OpChk(e)
#else
#define OpChk(e) e
#endif

class PermEnum;

// Operator does not subclass from VacsObject (doing so requires one extra
//   byte, essentially doubling the storage requirements of the program).

class PACKED Operator
{

private:

   iodeclarations( Operator )

	// The actual operator is stored here.
	// It is a char, composed of two nibbles:
	//         high nibble low nibble
	// edge        v2          v1   (v1 < v2; no self edges)
	// vertex      15          v
	// nop         15          15
        //
	uchar op;

	// some constants that are needed for nibble manipulation.
	// they are dependent on the "packing" into a char that we are using.
        //
	enum { NONE=15, SHIFT=4, LOWMASK=0x0f };

	// no-operator's value (for efficiency)
        //
	enum { noOpValue = 255 };

	// return the low nibble of the operator
        //
	char lowNibble() const { return op & LOWMASK; }

	// return the high nibble of the operator
        //
	char highNibble() const { return op >> SHIFT; }

	// boundary size ( obtained from runInfo; stored here for efficiency)
        //
	static int bs;

	// first operator, for iteration
	//
	static uchar _firstOpValue;
	static uchar _firstVertexOpValue;

	// private constructors:
	// these two methods have no error checking, they just plop
	// the passed values into op.

	// create a vertex operator
        //
	Operator( uchar vNum ){ op = NONE<<SHIFT | vNum; }

	// create an edge operator (assume vNum1 < vNum2)
        //
	Operator( uchar vNum1, uchar vNum2 ){ op = vNum2<<SHIFT | vNum1; }

	//--------

	// Conversion of ops to and from int (for iteration)
	// tables for fast enum <--> edge conversion
        //
	static Operator* enumToOpTable;
	static int* opToEnumTable;
	static Operator enumToOp( int enumOp )
	   { return enumToOpTable[ enumOp ]; }
	int opToEnum() const
	   { return opToEnumTable[ op ]; }


public:
	// Class methods --------------------------------------

	// must be called before the any instances are created!
        //
	static void initTables( int boundarySize );

	// must be called at program termination
        //
	static void destroyTables();

	// return the total number of ops
	//
	static int numberOfOperators()
	   { return (bs*bs + bs) / 2; }

	// return the boundary size
	//
	static int boundarySize()
	   { return bs; }


	// Constructors --------------------------------------

	// default constructor - set to noop
        //
	Operator() { op = noOpValue; }

	// Creating operators --------------------------------

	static Operator noOp()
	   { Operator o; o.op = noOpValue; return o; }

	// return vertex operator formed from first vertex of edge
	// *this must be an edge operator
        //
	Operator vertexOperator1() const
	   { assert( isEdgeOp() ); return Operator( lowNibble() ); }

	// return vertex operator formed from second vertex of edge
	// *this must be an edge operator
        //
	Operator vertexOperator2() const
	   { assert( isEdgeOp() ); return Operator( highNibble() ); }

	// return the edge operator of the vertex operators
	// *this must be a vertex operator
        //
	Operator edgeOperator( Operator vOp2 ) const
	   { assert( isVertexOp() && vOp2.isVertexOp() );
	     uchar v1 = lowNibble(); 
	     uchar v2 = vOp2.lowNibble();
	     return (v1<v2) ? Operator(v1,v2) : Operator(v2,v1);
	   }


	// return the vertex operator of a vertex
	// performs an error check
        //
	static Operator vertexOperator( vertNum v )
	   { assert( v>=0 && v<bs ); return Operator(v); }


	// return the vertex operator of a vertex
	// return noop on failure
        //
	static Operator vertexOperatorReturnFailure( vertNum v )
	   { if ( v<0 || v>=bs ) return noOp(); else return Operator(v); }


	// return the edge operator of the vertices
	// performs an error check
        //
	static Operator edgeOperator( vertNum v1, vertNum v2 )
	   { assert( v1>=0 && v2>=0 && v1<bs && v2<bs && v1!=v2 );
	     return v1<v2 ? Operator(v1,v2) : Operator(v2,v1);
	   }


	// return the edge operator of the vertices
	// return noop on failure
        //
	static Operator edgeOperatorReturnFailure( vertNum v1, vertNum v2 )
	   { if ( v1<0 || v2<0 || v1>=bs || v2>=bs || v1==v2 )
	     return noOp();
	     else return v1<v2 ? Operator(v1,v2) : Operator(v2,v1);
	   }

	// Return a random vertex operator
	//
	static Operator randomVertexOp( RandomInteger &rand );

	// Return a random edge operator
	//
	static Operator randomEdgeOp( RandomInteger &rand );


	// Testing --------------------------------------

	// check if operator is a vertex operator
        //
	bool isVertexOp() const
	   { return highNibble()==NONE && lowNibble()!=NONE; }

	// check if operator is a edge operator
        //
	bool isEdgeOp() const
	   { return highNibble()!=NONE && lowNibble()!=NONE; }

	bool isNoOp() const
	   { return op == noOpValue; }

	// tests: if two vertices ops are equal, if a vertex is an end
	// of an edge, or if two edges are incident
        //
	bool incident( const Operator anOp );

	bool operator==( const Operator anOp ) const
	   { return op==anOp.op; }

	bool operator!=( const Operator anOp ) const
	   { return op!=anOp.op; }

	bool operator<( const Operator anOp ) const;

	bool operator>( const Operator anOp ) const
           { return anOp.operator<( *this ); }

	// Iteration -----------------------------------------

	// Example:
	//    Operator op;
	//    for( op.firstOp; !op.isNoOp(); op++ ){ ... }

	// Increments the operator, sets to nop if op is currently last
        //
	Operator operator++( );
	Operator operator++( int );

	// sets the operator to be the first op in the internal order
        //
	void firstOp() { op = _firstOpValue; }

	void firstVertexOp() { op = _firstVertexOpValue; }

        bool isLastVertexOp() const;

	// Information ---------------------------------------

	// return the vertex of a vertex operator
        //
	vertNum vertex() const
	   { assert( isVertexOp() ); return lowNibble(); }

	// return the vertices of an edge operator
	// returns lower numbered vertex in v1
        //
	void vertices( vertNum& v1, vertNum& v2 ) const
	   { assert( isEdgeOp() ); v1=lowNibble(); v2=highNibble(); }

	// return first vertex of an edge (lower numbered)
        //
	vertNum vertex1() const
	   { assert( isEdgeOp() ); return lowNibble(); }

	// return second vertex of an edge (higher numbered)
        //
	vertNum vertex2() const
	   { assert( isEdgeOp() ); return highNibble(); }


	// Misc edge operator operations ----------------

	// test if passed vertex op is an end of this, an edge op.
	//
	bool edgeHasVertex( Operator vOp )
	   { assert( isEdgeOp() && vOp.isVertexOp() );
	     return lowNibble() == vOp.lowNibble()
	         || highNibble() == vOp.lowNibble();
	   }

	// test if this (a vertex op) is an end of the passed edge op
	bool vertexInEdge( Operator eOp )
	   { assert( isVertexOp() && eOp.isEdgeOp() );
	     return lowNibble() == eOp.lowNibble()
	         || lowNibble() == eOp.highNibble();
	   }

	// if this (an edge op ) 'has vOp', then return the 
	// vertex op of the other end of this.
	//
	Operator otherVertex( Operator vOp )
	   { assert( isEdgeOp() && vOp.isVertexOp() );
	     if   (vOp.lowNibble()==lowNibble()) return vertexOperator2();
	     else if (vOp.lowNibble()==highNibble()) return vertexOperator1();
	     else { aassert(false); return vOp; }
	   }

	// change any occurence in this of v1 to v2.
	// no error if v1 is not present in this.
	//
	Operator changeVertex( Operator v1, Operator v2 );

	// change any occurence in this of v1 to v2, and vice-versa.
	// no error if v1 is not present in this.
	//
	Operator exchangeVertices( Operator v1, Operator v2 );

	// Permute the operator according to p
	//
	Operator permute( const PermEnum& p );

}; // class Operator

#endif
