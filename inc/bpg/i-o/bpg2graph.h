 
/*tex
 
\file{bpg2graph.h}
\path{inc/bpg/i-o}
\title{Header for BPG conversion to Graph}
\classes{}
\makeheader

xet*/


#ifndef _bpg2graph_h
#define _bpg2graph_h
 
class Graph;
class BPG;

void bpg2graph(const BPG &bpg, Graph &g );

#endif
