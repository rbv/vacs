
// -------------------------------------------
// ---------------- bpgutil.h ----------------
// -------------------------------------------

/*tex
 
\file{bpgutil.h}
\path{inc/bpg}
\title{Header for BPG utility functions}
\classes{}
\makeheader
 
\begin{desc}
This header declares the more `standard' bpg utility functions.
\end{desc}
 
xet*/


#ifndef _bpgutil_h
#define _bpgutil_h

#include "stdtypes.h"
#include "isomorphism/iso_graph.h"

#include "bpg/bpg.h"

#undef minor


// This file declares "non-primitive" functions for
// bounded pathwidth graphs.

bool hasIsoNonboundaryVertex( const BPG& G );

// Check if the graph has an edge uv, with u degree 1 and
// both u and v non-boundary.
//
bool hasNonBoundaryDangle( const BPG& G );

// Find all edges uv with deg(u)==1, u non-boundary.
// The optional arguments are filled with the positions in G
// of all u, v, and uv operators satisfying the conditions.
// Return true iff graph has a dangle.
//
bool findAllDangles( 
   const BPG& G, 
   Array<int> *uArray = 0, 
   Array<int> *vArray = 0,
   Array<int> *uvArray = 0
);

// Check for an edge uv, with
//   u degree 1
//   u non-boundary
//   v has degree > 1
// Written as vertex cover pretest.
//
bool hasDangleWithOtherVertexDegreeGT1( const BPG& G );

bool hasIsolatedBoundaryVertex( const BPG& G );

// Test if a graph is disconnected.  
// For RB, this means there exists a component disconnected 
// from the boundary.
//
bool isDisconnected( const BPG& G );

// Test if a graph is biconnected to its boundary
// That is, no non-boundary cut vertices
template<class _BPG>
bool isBiconnected( const _BPG& G );

// Version used when every prefix of G is known to be boundary-biconnected
bool isBiconnectedTest( const RBBPG& G );


// Test if G has a cut edge
//
bool hasBridge( const BPG& G );

bool hasNonboundaryDeg2( const BPG& G );

void updateConnectivityArray( const BPG& G, Array<int>& connArray, 
  int start, int end );

bool isAcyclic( const BPG& G );

bool hasMultipleEdge( const BPG& G );

bool hasAnEdge( const BPG& G );

bool hasAVertex( const BPG& G );

bool isomorphic( const BPG& G1, const BPG& G2, 
                 IsomorphismBoundaryType = freeBoundary );

inline bool isomorphicNoBoundary( const BPG& G1, const BPG& G2 )
   { return isomorphic( G1, G2, noBoundary ); }

inline bool isomorphicFreeBoundary( const BPG& G1, const BPG& G2 )
   { return isomorphic( G1, G2, freeBoundary ); }

inline bool isomorphicFixedBoundary( const BPG& G1, const BPG& G2 )
   { return isomorphic( G1, G2, fixedBoundary ); }

// ladder layout parameters.
//
int steps( const BPG &G );
//
int steps( const BPG &G, int v, int &left, int &right ); 
//
inline int steps( const BPG &G, int v )
 { 
   int l,r; 
   return steps( G, v, l, r ); 
 }

#endif
