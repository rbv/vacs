 
// -------------------------------------------
// ---------------- bpgutil2.h ---------------
// -------------------------------------------
 
/*tex
 
\file{bpgutil2.h}
\path{inc/bpg}
\title{Header for BPG utility functions}
\classes{}
\makeheader

\begin{desc}
This header declares the more `non-standard' bpg utility functions.
\end{desc}
xet*/


 
#ifndef _bpgutil2_h
#define _bpgutil2_h
 
#include "stdtypes.h"
 
//#include "graph/stdgraph.h"
//#include "graph/stdgraph2.h"
 
#include "bpg/bpg.h"
#include "search/searchminor.h"

#undef minor

typedef SquareMatrix<bool> BndryIncidenceMatrix;


// Passed a boundary incindence matrix and start and end positions,
//    update the matrix.
// Range processed is start <= i < end (i.e. pass end=G.length() for whole of G)
// If end==-1, substitute G.length() for end.
// Return true if multiedge encountered (in which case M is not correct).
// If end is -1, go to end of G.
//
bool updateBndryIncidenceMatrix( const BPG& G, BndryIncidenceMatrix& M,
        int start = 0, int end = -1 );

// Reverse the graph and strip the boundary.  Result goes into G2.
//
void reverseAndStrip( const BPG& G1, BPG& G2 );


// Create a copy of g (virtual), and apply minor operations until
// either (1) the minor is empty (return false), or (2) the minor
// is congruent to g (return true).
//
bool randomMinorSearch( const BPG& g );
 
// Calculate the next minor of the graph g (virtual).
// The return graph h has caller defined type,
// and the minor operations respect whatever boundary may be present.
// Used to iterate through all of g's one-step minors.
// Returns false when no more minors.
// Typical usage:
//    type=BPG::DeleteVertex; pos=-1;
//    while( 1 )
//       BPG g(h); // or RBBPG g(h);, etc
//       if ( ! nextMinor( g, type, pos ) ) break;
//       do something with g;
//
// If compute is false, just the positions of the minors are calculated,
// but the graph is unaffected.
//
bool nextMinor( BPG& g, SearchMinor::ReductionType& type, int& pos, bool compute = true );


// Check if g and h are congruent.
//
bool checkCongruent( const BPG& g, const BPG& h );
 
// In-place obstruction minimisation
//
void obstMinimize( BPG& g );
 
// Find the depth of each vertex in the "ladder" representation.
// These depths are passed back in the second arg, which is an array
// indexed by vertex number.
//
void computeVertexLadder( const BPG& g, Array<int> ladder );


// Extend the graph g with random operators until it is not in the family.
// The input g must be right boundaried, and the extended graph h
//    is center boundaried.
// EdgeWeight is an integer between 0 and 100, specifying the percent
//    probabilty of each random op being an edge op.
// Hence 100 ==> extend with edges only.
// ExtendMax is the longest the extension is allowed to be.
// Tries is the number of searches allowed.
// The extension is returned in k (as usual, no boundary).
// Return true if successful, false is tries exceded.
// Defn is in bpgext.c.
//
//
enum ExtensionFlags {
   ExtensionConnected = 1,
   ExtensionTree = 2,
   ExtensionPretest = 4 };
//
struct randomExtensionSearchInfo
{
   int ExtnMaxLength;
   int ExtnTries;
   int ExtnEdgeWeight;
   int ExtnFlags;
   int ExtnSkip;

   randomExtensionSearchInfo()
   {
      ExtnMaxLength = 50;
      ExtnTries = 10;
      ExtnEdgeWeight = 80;
      ExtnFlags = 0;
      ExtnSkip = 0;
   }
};
//
bool extendOutOfFamily( const RBBPG& g, CBBPG& k, 
                        const randomExtensionSearchInfo&, int run = 0 );


// Apply minor operations to the right hand side of g until all such
// operations would move g into F.
// On entry, g must be out of F.
// g is modified.
//
void minimizeRHS( CBBPG& g );


// Check for obstruction.  G may be NB, CB, RB.
//
bool isObstruction( BPG& G );
 

/*
 * Make a random RBBPG of length n + boundary size.
 * edgeWeight=100 ==> all edges, =0 ==> all vertices
 * percentEdges = -1 (default) ==> even distibrution of Ops
 */
void randomBPG( int n, BPG &g, int percentEdges = -1 );

// Check if graph is canonic
//
//bool isCanonic( const RBBPG&, int timeout=0 );
 
// Check if graph is canonic
//
bool QCanonicCheck( const RBBPG& );
bool QCanonicCheck( Operator z1, Operator z2 );
bool QCanonicCheck( Operator z1, const RBBPG& );
 
#endif
