

// --------------------------------------
// ----------- gluedbpg.h ---------------
// --------------------------------------

/*tex
 
\file{gluedbpg.h}
\path{inc/bpg}
\title{Header for glued BPG class}
\classes{GluedBPG}
\makeheader
 
xet*/


#ifndef _gluedbpg_h
#define _gluedbpg_h

#include "bpg/bpg.h"
#include "general/permutes.h"

// header file for class GluedBPG (glued bounded pathwidth graph)
//
// This class is for high speed gluing and testing.
// See class CBBPG.


class GluedBPG
#ifdef VACSOBJECT
		 : public VacsObject
#endif
{
private:

	// The graphs
	//
	NBBPG g1, g2, glue;

	// position in g1, g2 of glue graph (== length(g1))
	//
	int glueStart;

public:

	// ctor - set the glue graph to 'right', copy 'left' into 1st graph's
	// left.
	// 
	GluedBPG( const BPG& g1, const BPG& glueGraph );

	// set the 2nd graph's left side
	//
	void setG2( const BPG& );

	// Apply all possible permutations of glue to g1 and g2,
	// looking for g1+perm(glue) not in F, g2+perm(glue) in F.
	// If found, return true and the perm that succeeded.
	// If not found, return false.
	//
	bool performTest( PermEnum* perm = 0 );

	// --- friends ---
	//
	friend ostream& operator<<( ostream&, const GluedBPG& );

};

ostream& operator<<( ostream& o, const GluedBPG& G );

#endif
