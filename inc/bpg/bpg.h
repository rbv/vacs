
// --------------------------------------
// --------------- bpg.h ----------------
// --------------------------------------


#ifndef _bpg_h
#define _bpg_h

/*tex
\file{bpg.h}
\path{inc/bpg}
\title{Header file for class BPG (bounded pathwidth graph)}
\classes{BPG, RBBPG, CBBPG, LBBPG}
\makeheader

\begin{desc}
\end{desc}

\begin{caveats}
The access control for the search methods in OpArray is `sorried'
in cfront.  They should be uncommented when cfront gets fixed.
In the meantime, defined inline pass-through functions
as needed.
\end{caveats}

\begin{bugs}
Figure out caching when: copied, deleted,... (who owns free store?)
The behavior of the cache has not been completely worked out.
Questions are: should cached free-store structures be copied
by the copy constructor, stored/loaded from buffers, and/or deleted by
the destructor?
\end{bugs}
xet*/


//  Read the header for BPG's base class, OpArray.
//
#include "stdtypes.h"
#include "bpg/oparray.h"
#include "graph/stdgraph2.h"

class Str;
class VertPartition;

//class BPG : private OpArray
class BPG : public OpArray
{
   iodeclarations( BPG )

public:

   enum BoundaryType { noBoundary, rightBoundary, centerBoundary, leftBoundary};

private:

   // Flags non-pointer cache entries as invalid.
   //
   enum { Invalid = -1 };

   // The cached values.
   //
   //short _numVertices;
   //short _numEdges;
   //VertPartition*  _vertPartition;
   //DistanceMatrix* _distanceMatrix;

   // Initialize the cache entries to the invalid state.
   // Invoked by all constructors.
   //
   void init()
   {
      //_numVertices = _numEdges = Invalid;
      //_vertPartition = 0;
      //_distanceMatrix = 0;
   }

   // Invalidate the cache. Must be invoked by any method
   // that modifies the graph.
   //
   void changed() {; }


   // Private methods to compute the cached structures.
   //
   //void computeVertPartition() const;
   //
   //void computeDistanceMatrix() const;


   // Used for contraction
   //
   void leftOfBoundaryContract( int pos );
   //
   void rightOfBoundaryContract( int pos );

public:

   //  All constructor calls are handled by OpArray.
   //  Initialize the cache.

   // Create empty. No free store is allocated.
   //
   BPG() : OpArray() { init(); }

   //  Create with the specified size.
   //  All entries are initialized to noOp.
   //
   BPG( int size ) : OpArray( size ) { init(); }

   // Copy constructor.  Make an exact duplicate of the argument.
   // Use to covert a boundaried graph to a non-boundaried graph.
   // Copy non-free store cache objects. (LATER!!)
   //
   BPG( const BPG& a ) : OpArray( a ) { init(); }

	
   //  Copy constructor.  Copy the argument, and allocate extra space
   //  (initialized with noOp).
   //
   BPG( const BPG& a, int extra ) : OpArray( a, extra ) { init(); }

   //  Assignment operator.  Internal storage is not deleted/reallocated
   //  if the argument has the same size as this.
   //
   void operator=( const BPG& a )
      { OpArray::operator=( a ); }

	
   //   Redefine subscript operator to be read-only,
   //   so that the cache can be maintained.
   //
   Operator operator[]( int index ) const
      { return OpArray::get( index ); }

   //  Same as subscript operator, but easier to use
   //   in member functions.
   //
   Operator getOp( int index ) const
      { return OpArray::get( index ); }

   // Use this method to change an operator.
   // Cache will be invalidated.
   //
   void setOp( int index, Operator op )
      { OpArray::put( index, op ); changed(); }

   // resize
   //
   OpArray::resize;

   // remove all nops from the graph (cache is not cleared)
   //
   void compress();

   // Clear the cache (to save memory).
   // Only free-store cached info is removed.
   //
   void clearCache() {}

   // static methods for constructing special graphs
   //
   // Create an empty graph -- this is just BPG(), but 
   // has a more informative name here.
   //
   //static BPG emptyGraph();

   // Create from null-terminated string.
   // Str format: comma seperated list of operators, arbitrary white space.
   // Operators use hex char system (0-9,a-f).
   // Str format example: 0,1,2,12,13
   // Return -1 if parse ok, position of error otherwise.
   // Typical call: BPG g; if ( BPG::fromString( aStr, g ) ) ...
   // Passed graph can be a subclass (RBBPG, CBBPG).
   //
   static int fromString( Str, BPG& );

   // Virtual ctor. Return a copy of this.
   //
   virtual BPG* copy() const = 0;

   // Virtual assignment operator.  Check that param is correct type,
   //   and assign to this.
   //
   virtual void update( const BPG& ) = 0;

   // Virtual dtor
   //
   virtual ~BPG();

   // check if equal up to min of lengths
   //
   static bool equalUpToSmaller( const BPG&, const BPG& );

   // -----------------------------------------------------------
   // --- Information ---

   // Return the boundary size (get from class Operator)
   //
   int boundarySize() const
      { return Operator::boundarySize(); }


   // Return number of operators
   //
   int size() const
      { return OpArray::size(); }

   int length() const
      { return OpArray::size(); }


   // Return position of ith vertex operator
   //
   int ithVertex(int i) const
      { return OpArray::ithVertex(i); }


   // Return position of ith edge operator
   //
   int ithEdge(int i) const
      { return OpArray::ithEdge(i); }


   // Return number of vertices (cached)
   //
   int vertices() const
   { 
      return OpArray::numVertexOps(); 
   }
   //
   int order() const { return vertices(); }


   // Return number of edges (cached)
   //
   int edges() const
   { 
      return OpArray::numEdgeOps(); 
   }


   void distanceMatrix( DistanceMatrix& ) const;

   void vertPartition( VertPartition& ) const;

   // Check if the operator at index is an isolated vertex operator.
   //
   bool isIsolatedVertex( int index ) const;

   void getEdgeOpEndpoints( int index, int &v1, int &v2 ) const;

   // -----------------------------------------------------------
   // --- utility ---

   // Remove all multiple edges from the graph.
   // Return true if any edges were removed.
   //
   bool removeMultipleEdges();

   // Remove all isolated non-boundary vertices from the graph.
   // Return true if any vertices were removed.
   //
   bool removeIsolatedVertices();

   // -----------------------------------------------------------
   // --- boundary ---

   // Return the boundary type.
   //
   virtual BoundaryType boundaryType() const = 0;

   // Set the graph to empty boundary
   //
   virtual void setEmpty() = 0;

   // Check that the graph has the required boundary vertices.
   //
   virtual bool isValid() const = 0;

   // Return true if the graph is empty.
   //
   virtual bool isEmpty() const = 0;

   // Return true iff (is vertex) && (is boundary)
   //
   virtual bool isBoundaryVertex( int index ) const = 0;

   // Return true iff (is vertex) && (is isolated) && (is not boundary)
   //
   virtual bool isIsoNonboundaryVertex( int index ) const = 0;

   // Return true iff (is edge) && (is boundary)
   //
   virtual bool isBoundaryEdge( int index ) const = 0;

   // Return true iff (is edge) && (is not boundary)
   // This is not the same as !isBoundaryEdge()
   //
   virtual bool isNonboundaryEdge( int index ) const = 0;

   // Return true position i is left of the boundary.
   // Used by contraction alg.
   // NB returns true.
   //
   virtual bool leftOfBoundary( int index ) const = 0;

   // -----------------------------------------------------------
   // --- Minor order ----
   //
   // Be careful! These functions do not check boundary conditions.
   //
   void deleteVertex( int position );

   void deleteIsoVertex( int position );

   void deleteEdge( int position );

   void contractEdge( int position );


   // -----------------------------------------------------------
   // --- Searching ---
   //
   // See oparray.h for documentation
   // Removed (cfront bug)
   //
   //OpArray::nextVertexOp;
   //OpArray::prevVertexOp;
   //OpArray::nextEdgeOp;
   //OpArray::nextEdgeOpIncident;
   //OpArray::prevEdgeOp;
   //OpArray::prevEdgeOpIncident;
   int prevVertexOp( int i, Operator anOp ) const
     { return OpArray::prevVertexOp( i, anOp ); }

   // Lexicographical order on t-parses, NOT skipping over noops
   //
   bool operator<( const BPG& ) const;

   // Less-than ordering suitable for sorted containers: DOES skip over noops
   //
   bool compare( const BPG& ) const;

   // Less-than canonic ordering <_c used to define canonical t-parses
   //
   bool canonicLess( const BPG& ) const;

   // copies operators to memory in binary form
   //
   void copyToMem( char* mem ) const;

   // resizes to length and does a raw insert (no error checking :) )
   //
   void loadFromMem( char* mem, int length );
};

// --------------------------------------------------------------------

class RBBPG;

class NBBPG : public BPG
{
   iodeclarations( NBBPG )

public:

   // ctors - see BPG

   // Note that ctors can create 'invalid' graphs, that do not have 
   // the required boundary vertices.
   //
   NBBPG()                            : BPG() {}
   NBBPG( int size )                  : BPG( size ) {}
   NBBPG( const NBBPG& a )            : BPG( a ){}
   NBBPG( const NBBPG& a, int extra ) : BPG( a, extra ) {}
   NBBPG( const BPG& a, int start, int end );

   // ctor from RBBPG - drop boundary
   //
   NBBPG( const RBBPG& );


   BPG* copy() const { return new NBBPG(*this); }

   void update( const BPG& b )
      { assert( b.boundaryType() == BPG::noBoundary ); *this = (NBBPG&) b; }

   // Create a graph consisting of (boundary size) vertex operators.
   //
   static NBBPG emptyGraph();

   void operator=( const NBBPG& a )
      { BPG::operator=( a ); }

   BoundaryType boundaryType() const { return noBoundary; }

   bool isValid() const;

   bool isEmpty() const;

   bool isBoundaryVertex( int index ) const;

   void setEmpty();

   bool isIsoNonboundaryVertex( int index ) const;

   bool isNonboundaryEdge( int index ) const;

   bool isBoundaryEdge( int index ) const;

   bool leftOfBoundary( int index ) const;

};


// --------------------------------------------------------------------

class RBBPG : public BPG
{

   iodeclarations( RBBPG )

public:

   // ctors - see BPG

   // Note that ctors can creat 'invalid' graphs, that do not have 
   // the required boundary vertices.
   //
   RBBPG()                            : BPG() {}
   RBBPG( int size )                  : BPG( size ) {}
   RBBPG( const BPG& a )              : BPG( a ){}
   RBBPG( const RBBPG& a, int extra ) : BPG( a, extra ) {}

   // Special ctor
   //
   RBBPG( const RBBPG&, const NBBPG& );

   BPG* copy() const
      { return new RBBPG(*this); }

   void update( const BPG& b )
      { assert( b.boundaryType() == BPG::rightBoundary ); *this = (RBBPG&) b; }

   // Create a graph consisting of (boundary size) vertex operators.
   //
   static RBBPG emptyGraph();

   void operator=( const RBBPG& a )
      { BPG::operator=( a ); }

   // Create from null-terminated string.
   // Str format: comma seperated list of operators, arbitrary white space.
   // Operators use hex char system (0-9,a-f).
   // Str format example: 12,13,01,1,01	     (Initial boundary not given.)
   // Return -1 if parse ok, position of error otherwise.
   // Typical call: BPG g; if ( RBBPG::fromString( aStr, g ) ) ...
   // Passed graph can be a subclass (RBBPG, CBBPG).
   //
   static int fromString( Str, BPG& );

   BoundaryType boundaryType() const { return rightBoundary; }

   bool isValid() const;

   bool isEmpty() const;

   void setEmpty();

   bool isBoundaryVertex( int index ) const;

   bool isIsoNonboundaryVertex( int index ) const;

   bool isNonboundaryEdge( int index ) const;

   bool isBoundaryEdge( int index ) const;

   bool leftOfBoundary( int index ) const;

   // These hashes do NOT take the boundary into account!
   // Therefore they are suitable only for RBBPG
   OpArray::hash32;
   OpArray::hash64;

   static bool isCanonic( const RBBPG& G, int &timeout );

   static bool findCanonic( const RBBPG& G, RBBPG& canonic ); 

   static bool findCanonic( const class BGraph& G, RBBPG& canonic ); 
};

// --------------------------------------------------------------------

class CBBPG : public BPG
{

   iodeclarations( CBBPG )

private:

   // Position of the boundary.  Points to the first char of the
   // right graph, so that the boundary vertex operators lie strictly
   // left of the position.
   //
   int bPos;

public:

   // ctors - see BPG

   // Note that ctors can creat 'invalid' graphs, that do not have 
   // the required boundary vertices.
   //
   CBBPG()                            : BPG()           { bPos = -1; }
   CBBPG( int size )                  : BPG( size )     { bPos = -1; }
   CBBPG( const CBBPG& a )            : BPG( a )        { bPos = a.bPos; }
   CBBPG( const CBBPG& a, int extra ) : BPG( a, extra ) { bPos = a.bPos; }

   BPG* copy() const
      { return new CBBPG(*this); }

   void update( const BPG& b )
      { assert( b.boundaryType() == BPG::centerBoundary ); *this = (CBBPG&) b; }

   void operator=( const CBBPG& a )
      { BPG::operator=( a ); bPos = a.bPos; }

   // Special ctors
   //
   CBBPG( const RBBPG&, int rightSize );
   CBBPG( const RBBPG&, const NBBPG& );

   // Setting
   //
   void loadLhs( const RBBPG&, int rightSize );

   // Extract left and right
   //
   void getRhsAsExtension( NBBPG& );
   //void getRhsAsRBBPG( RBBPG& );
   //void getLhsAsRBBPG( RBBPG& );

   // Info
   //
   int center() const
      { return bPos; }

   // Not implemented.
   //
   static CBBPG emptyGraph();

   BoundaryType boundaryType() const { return centerBoundary; }

   bool isValid() const;

   bool isEmpty() const;

   void setEmpty();

   bool isBoundaryVertex( int index ) const;

   bool isIsoNonboundaryVertex( int index ) const;

   bool isNonboundaryEdge( int index ) const;

   bool isBoundaryEdge( int index ) const;

   bool leftOfBoundary( int index ) const;

};

// --------------------------------------------------------------------

class LBBPG : public BPG
{
   iodeclarations( LBBPG )

public:

   LBBPG() : BPG() {}

   void appendToAxiom( const NBBPG& );

    // The pure virtuals...
   virtual BPG* copy() const;
   virtual void update( const BPG& );
   virtual BoundaryType boundaryType() const;
   virtual void setEmpty();
   virtual bool isValid() const;
   virtual bool isEmpty() const;
   virtual bool isBoundaryVertex( int index ) const;
   virtual bool isIsoNonboundaryVertex( int index ) const;
   virtual bool isBoundaryEdge( int index ) const;
   virtual bool isNonboundaryEdge( int index ) const;
   virtual bool leftOfBoundary( int index ) const;
};

#endif
