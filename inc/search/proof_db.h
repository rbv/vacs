

#ifndef _proofs_h
#define _proofs_h

#undef minor

#include "general/database_b.h"
#include "general/queue.h"
#include "search/nodeinfo.h"
#include "search/graph_db.h"

class VRSDatabase;
class FRSDatabase;
class PartialMinorProof;
class RBBPG;

//-----------------------------------------------------------------------

// Utility class to handle number of database entries
//
class DBNumberManager
{
private:
   int _nextNumber;
public:
   int allocate() { return _nextNumber++; }
   void initNextFreeNumber( int i ) { _nextNumber = i; }
   DBNumberManager()  { _nextNumber = 0; }
   int nextNumber() const { return _nextNumber; }
   int lastUsed() const { return _nextNumber-1; }

   void reset() { _nextNumber = 0; }
};

//----------------------------------------------------------------------

class ProofDatabaseBase
{
protected:
   int isoHits;

   DBNumberManager proofNumber;

   ProofDatabaseBase();

   void statistics( ostream& );

public:
   void addHit() { isoHits++; }

   int size() const { return proofNumber.nextNumber(); }
};

//----------------------------------------------------------------------

class MinimalProofDatabase : 
   public ProofDatabaseBase
{
private:

   FRSDatabase* _infoDB;
   VRSDatabase* _PMPDB;		// partial minor proof database

   static const char* ExtInfo;
   static const char* ExtPMP;
   static void makeNames( const char* , char*&, char*& );

public:

   MinimalProofDatabase(
      const char* basename,
      DatabaseBase::AccessMode mode,
      DatabaseBase::AccessMode cacheMode
   );

   ~MinimalProofDatabase();

   //void loadFromStream( class astream& );
   //void loadFromStream( class bistream&, int level, const NBBPG& prefix );

   void getInfo( int key, MinimalProof& ) const;

   void getPMP( int key, PartialMinorProof& ) const;

   // Adds a proof and returns its reference number.
   //
   int add( const MinimalProof& );
   //
   int add( const MinimalProof&, const PartialMinorProof& );

   void update( int key, const MinimalProof& );
   void update( int key, const PartialMinorProof& );

   bool exists( int key );

   void sync();
   void flush();
   void statistics( ostream& );

   void clearIsoDatabase();

   static void dbCreate( const char* basename );
   static bool dbExists( const char* basename );
};


//----------------------------------------------------------------------

class NonminimalProofDatabase : 
   public ProofDatabaseBase
{

   FRSDatabase* _infoDB;

   static const char* ExtInfo;
   static void makeNames( const char* , char*& );

public:

   NonminimalProofDatabase(
      const char* basename,
      DatabaseBase::AccessMode mode,
      DatabaseBase::AccessMode cacheMode
   );

   ~NonminimalProofDatabase();

   //void loadFromStream( class astream& );
   //void loadFromStream( class bistream&, int level, const NBBPG& prefix, bool loadAll );

   void getInfo( int key, NonminimalProof& proof ) const;

   void update( int key, const NonminimalProof& );

   // Adds a proof and returns its reference number.
   //
   int add( const NonminimalProof& );

   bool exists( int key );

   void sync();
   void flush();
   void statistics( ostream& );

   static void dbCreate( const char* basename );
   static bool dbExists( const char* basename );
};

//----------------------------------------------------------------------

class UnknownStateDatabase :
   public ProofDatabaseBase
{

   FRSDatabase* _infoDB;
   //VRSDatabase* _PMPDB;
   int _numUnknown;

   // Unused database numbers
   Queue< int > *freeNumbers;

   static const char* ExtInfo;
   static const char* ExtPMP;
   static void makeNames( const char* , char*&, char*& );

   // Get an unused DB key
   int allocate();

public:

   UnknownStateDatabase(
      const char* basename,
      DatabaseBase::AccessMode mode,
      DatabaseBase::AccessMode cacheMode
   );

   ~UnknownStateDatabase();

   void getInfo( int key, UnknownState& ) const;

   //void getPMP( int key, PartialMinorProof& ) const;

   // This function may only be called immediately after adding the graph to the isodb
   int add( const UnknownState& );

   void update( int n, const UnknownState& );
   //void update( int n, const PartialMinorProof& );

   bool exists( int key );

   void del( int n );

   int numUnknown() { return _numUnknown; }

   void sync();
   void flush();
   void statistics( ostream& );

   void clearIsoDatabase();

   static bool dbExists( const char* basename );
   static void dbCreate( const char* basename );
};


//----------------------------------------------------------------------

// Unlike the other databases IrrelevantInfoDatabase always starts with 0 items;
// it doesn't reload the actual count from disk.
// Which might be a bug, since it doesn't delete the files.

class IrrelevantInfoDatabase :
   public DBNumberManager
{

   FRSDatabase* _infoDB;

   static const char* ExtInfo;
   static void makeNames( const char* , char*& );

public:

   IrrelevantInfoDatabase(
      const char* basename,
      DatabaseBase::AccessMode mode,
      DatabaseBase::AccessMode cacheMode
   );

   ~IrrelevantInfoDatabase();

   // This was added just for Extract
   void loadNumberOfKeys();

   void getInfo( int key, IrrelevantInfo& ) const;

   int add( const IrrelevantInfo& );

   void sync();
   void flush();
   void statistics( ostream& );

   static void dbCreate( const char* basename );
   static bool dbExists( const char* basename );
};


#endif
