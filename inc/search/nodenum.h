
// -------------------------------------------
// ---------------- nodenum.h ----------------
// -------------------------------------------

/*tex

\file{nodenum.h}
\path{inc/search}
\title{Header file for class NodeNumber}
\classes{NodeNumber}
\makeheader

\begin{desc}
A NodeNumber acts as the handle for a SearchNode in the search system,
though its use could be generalised.
Each SearchNode is assigned a unique NodeNumber, and this NodeNumber
is used to refer to the SearchNode by the rest of the system.
The reason for this approach is that SearchNodes cannot be guaranteed
to be in memory, or to not move in memory, and so a pointer-independent
reference mechanism is used.
For example, a SearchNode records the NodeNumber of its parent, rather
than the pointer.
NodeNumbers are converted into real pointers by class SearchTree.
NodeNumbers can be passed to the file system to load SearchNodes
from disk.
\end{desc}

\begin{bugs}
The ctor from int is a design problem.
This class is meant to be tightly controlled, in that all of the 
NodeNumbers in the system are meant to be valid.
This is the case if the only way to create a NodeNumber is
to ask for a new (distinct) one, and if SearchNode's ctor
is the only place such a request occurs.
Unfortunately, the test programs need to create NodeNumbers from
user input.
\end{bugs}

\begin{caveats}
This class should actually probably be called SearchNodeNumber,
since that seems to be its use.
\end{caveats}

xet*/

#ifndef _nodenum_h
#define _nodenum_h

#include "general/stream.h"
#include "stdtypes.h"


// NodeNumber is small class for number assigning
// unique numbers to the nodes in the search.


class PACKED NodeNumber 
{
public:
	/*
	 * Current internal node number type. (for fast changes when playing)
	 */
	typedef int nodeKey;

private:

        iodeclarations( NodeNumber )

	friend class SearchNodeStatus;

	// the actual number if stored here
	//
	nodeKey num;

	// the next available node number is stored here
	//
	static nodeKey nextNum;

	// return a new nodeKey for a nodenumber, distinct from all others
	//
	static nodeKey newGNumber() { return nextNum++; }

	// static const nodeKey Invalid = -1;
	// static const nodeKey FirstNumber = 0;

	enum { Invalid = -1, FirstNumber = 0 };

	void setKey( nodeKey n ) { num = n; }

public:

	// ctors
	//
	NodeNumber( nodeKey i ) { num = i; }

	// Create an empty NodeNumber, flagged as invalid.
	//
	NodeNumber() { num = Invalid; }

	// tell if number is invalid
	//
	bool isInvalid() const
	   { return num == Invalid; }

	// tell if number is valid
	//
	bool isValid() const
	   { return num != Invalid; }

	// static method for creating new nodenumbers
	//
	static NodeNumber newNumber()
	   { return NodeNumber( newGNumber() ); }

	static NodeNumber firstNumber()
	   { return NodeNumber( FirstNumber ); }

	// return the node number as a nodeKey
	//
        nodeKey getKey() const { return num; }
	nodeKey asInt() const { return num; }

	bool operator==( const NodeNumber n ) const
	   { return num == n.num; }

	bool operator!=( const NodeNumber n ) const
	   { return num != n.num; }

	// For RunInfo start/stop procedures.
	//
        //static void lastNumber( int n )
        //   { nextNum = n; }
	//
        static int nextNumber()
	   { return nextNum; }

};

#endif
