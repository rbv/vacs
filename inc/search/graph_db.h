
#ifndef _graph_db_h
#define _graph_db_h

//#include <ext/hash_set>
#include <sparsehash/sparse_hash_set>

#include "array/marray.h"
#include "general/database_v.h"
#include "general/database_f.h"
#include "array/hugearray.h"
#include "bpg/bpg.h"
#include "graph/stdgraph.h"
#include "graph/stdgraph2.h"
#include "isomorphism/iso_graph.h"
#include "search/searchnode.h"
//include "bpg/bpgutil.h"
//include "bpg/bpgutil2.h"

//-----------------------------------------------------------------------

// Adds an in-memory cache of reference->RBBPG pairs (with reverse lookup) to a VRSDatabase for graphs
class GraphDatabase 
{
private:

   // First index: number of vertices
   // Second index: number of edges
   // Each DyArray* NULL for none, or is chunk of tightly packed serialised
   // graphs (RBBPG::CopyToMem), each prefixed with ref int
   // (ends with -1 ref sentinel)
   Array< Array< DyArray<char>* > > _cache;
   VRSDatabase* _graphDB;

   static const char* ExtG; // = ".Grf"
   static void makeNames( const char* basename , char*& result );

   bool cached;
   void cacheAdd( const RBBPG& G, int ref );

protected:

   // Adds the graph at the specified location.
   // Error if it exists already.
   //
   void add( int key, const RBBPG& );

   int nextFreeNumber();

public:

   GraphDatabase( 
     const char* basename, 
     DatabaseBase::AccessMode mode,
     DatabaseBase::AccessMode cacheMode );

   ~GraphDatabase();

   void graph( int key, RBBPG&  ); 

   // loads result array with db references of equal graphs
   //
   void checkForEqualEntry(
      const Array<RBBPG>&,              // graphs to check
      const Array<bool>& mask,          // only check true entries
      Array<int>& result );             // iso results (-1 ==> not found)

   // Search cache for equal graph, in the sense of bytesize identical serialisation
   // return -1 if not found
   int checkForEqualEntry( const RBBPG& );

   // clear graphs with lenght = v + e
   //
   void clearGraphCacheForLength( int length );

   void clearGraphCache();

   void sync();
   void flush();
   void statistics( ostream& );

   static void dbCreate( const char* basename );
   static bool dbExists( const char* basename );
};

//-----------------------------------------------------------------------

class IsoDBItem1Base
{
   iodeclarations( IsoDBItem1Base )

private:

   ProofRef _usedBy;
   char _parseLen;

public:

   IsoDBItem1Base() {}

   ~IsoDBItem1Base() {}

   ProofRef &proofRef() { return _usedBy; }
   ProofRef proofRef() const { return _usedBy; }
   int  parseLen() const  { return _parseLen; }
   int  parseLen( int len ) { assert(len!=0); _parseLen = len; }
};

#ifdef USE_SD_ISOMORPHISM

#include "isomorphism/schmidt_druffel/isodb.h"

typedef Schmidt_Druffel::IsoDBItem1 IsoDBItem1;
typedef Schmidt_Druffel::IsoDBItem2 IsoDBItem2;
using Schmidt_Druffel::initIsoDBItems;
using Schmidt_Druffel::printIsoAlgStats;

#elif defined(USE_BLISS_ISOMORPHISM)

#include "isomorphism/bliss/isodb.h"

typedef Bliss_Iso::IsoDBItem1 IsoDBItem1;
typedef Bliss_Iso::IsoDBItem2 IsoDBItem2;
using Bliss_Iso::initIsoDBItems;
using Bliss_Iso::printIsoAlgStats;

#elif defined(USE_NAUTY_ISOMORPHISM)

#include "isomorphism/nauty/isodb.h"

typedef NAUTY_Iso::IsoDBItem1 IsoDBItem1;
typedef NAUTY_Iso::IsoDBItem2 IsoDBItem2;
using NAUTY_Iso::initIsoDBItems;
using NAUTY_Iso::printIsoAlgStats;

#else

#error Specify an isomorphism algorithm

#endif

void IsoDBItem__compute( 
        const Array<RBBPG>& graphs, 
        const Array<bool>&  mask, 
        Array<IsoDBItem1>&  isoReps1,
        Array<IsoDBItem2>&  isoReps2
     ); 



void IsoDBItem__checkInternalIso( 
        const Array<RBBPG>& graphs, 
        const Array<IsoDBItem1>& isoReps1, 
        const Array<IsoDBItem2>& isoReps2, 
        const Array<bool>&  mask, 
        Array<int>&   result,
        int parseLen
     ); 

//-----------------------------------------------------------------------
class IsoDatabase 
{
private:

   struct IsoDBItem
   {
       IsoDBItem1 *iso1;
       IsoDBItem2 *iso2;
       int isoref;

       // Deleted item marker
       IsoDBItem()
           : isoref(0), iso1(NULL), iso2(NULL)
       {}

       IsoDBItem( int key, IsoDBItem1 *_iso1, IsoDBItem2 *_iso2)
           : isoref(key), iso1(_iso1), iso2(_iso2)
       {}

       bool operator==( const IsoDBItem &i2 ) const
       {
           // First check whether either is sentinel
           if ( iso1 && i2.iso1 )
               return *this->iso1 == *i2.iso1 && *this->iso2 == *i2.iso2;
           return iso1 == i2.iso1;
       }
   };

   struct _hashGetter
   {
       size_t operator()( const IsoDBItem &i ) const { return i.iso1->getHash(); }
   };
/*
   static bool _itemComp( const IsoDBItem *i1, const IsoDBItem *i2 )
   {
       return *i1->iso1 == *i2->iso1 && *i1->iso2 == *i2->iso2;
   }
*/

   //typedef __gnu_cxx::hash_set< IsoDBItem, _hashGetter > isohashset_t;
   typedef google::sparse_hash_set< IsoDBItem, _hashGetter > isohashset_t;

   FRSDatabase* _iso1DB;  // caching
   VRSDatabase* _iso2DB;  // non-caching, instead IsoDatabase does manual caching:

   // IsoDBItem2's are stored here in memory
   // Used to implement getIsoUser, though making isoResult a IsoDBItem* instead
   // would work just as well.
   HugeArray _cache2;

   // Indicates which records in _cache2 contain constructed IsoDBItem2s, and which are garbage
   HugeBitVector _cache2_used;

   // Used to lookup an item (NAUTY and BLISS key comparison done by comparing
   // serialisations of canonical forms; S-D Iso does isomorphism testing IIRC)
   isohashset_t _hashtable;

  
   // All graphs with length less than this have been purged
   // (but they still take up memory in _cache2 (only sizeof(IsoDBItem2)) and _iso1DB's cache)
   int minCachedLen;

   static const char* Ext1;
   static const char* Ext2;
   static void makeNames( const char* , char*&, char*& );

   void addHit( const IsoDBItem &item );

   // Called to remove records if databases weren't written cleanly
   void truncate_invalid( DatabaseBase::AccessMode mode );

public:
   // Should not be public?

   IsoDBItem1 &getIso1( int key );
   IsoDBItem2 &getIso2( int key );

protected:

   void eraseIso2( int key );

   // Adds the graph at the specified location.
   // Error if it exists already.
   //
   void add( int key, const RBBPG& );
   void add( int key, const IsoDBItem1&, const IsoDBItem2& );

   bool remove( int key );

public:

   // Load isodb records for all t-parses parse length (not depth!) of
   // minlen or greater.
   // Pass resumingRun = true if reusing run/ databases
   // (note actually implemented in the rest of VACS)
   //
   IsoDatabase( const char* basename, 
                DatabaseBase::AccessMode mode,
                DatabaseBase::AccessMode cacheMode,
                int minlen = 0,
                bool resumingRun = false );

   ~IsoDatabase();

   void checkForIsomorphicEntry(
      const Array<IsoDBItem1>&,		// graphs to check
      const Array<IsoDBItem2>&,		// graphs to check
      const Array<bool>& mask,		// only check true entries
      Array<int>& result,		// iso results (-1 ==> not found)
      int parseLen );

   // Return ref if the graph is in the DB, -1 otherwise.
   //
   int checkForIsomorphicEntry( IsoDBItem1&, IsoDBItem2& );
   int checkForIsomorphicEntry( const RBBPG& G );

   // Add a new graph to the database, returning an isoref number,
   // which is the record number in the iso1/2 DBs.
   int add( const RBBPG& );
   int add( const IsoDBItem1&, const IsoDBItem2& );

   int size() const { return _iso1DB->numberOfKeys(); }

   // Get ProofRef for an entry
   ProofRef proofRef( int isoRef )
   {
       assert( isoRef < size() );
       return getIso1( isoRef ).proofRef();
   }

   // Update ProofRef for an entry
   void proofRef( int isoRef, ProofRef ref );

   void clearIsoCacheUpToLength( int len );

   void sync();
   void flush();
   void statistics( ostream& );
   void clearIsoDatabase();

   static void dbCreate( const char* basename );
   static void dbDelete( const char* basename );
   static bool dbExists( const char* basename );
};


// Find the equivalence classes of a set of BPGs.
// Much more efficient than the alternative (PCollection<T>::removeEqualities)
// and simpler than using IsoDBItem__checkInternalIso
class IsoTester {
    IsoDatabase *isoDB;
    const char *isodb_path; // location of the temp iso database

  public:
    IsoTester() {
        isodb_path = tempnam( NULL, "VACSL" );
        //cerr << "Writing temp database to " << isodb_path << nl;
        IsoDatabase::dbCreate( isodb_path );
        isoDB = new IsoDatabase( isodb_path, DatabaseBase::WriteAccess, DatabaseBase::AccessMode(DatabaseBase::WriteAccess | DatabaseBase::Cache) );
    }

    ~IsoTester() {
        delete isoDB;
        IsoDatabase::dbDelete( isodb_path );
    }

    // Returns index, -1 if not found
    int find( BPG &G, bool ignoreBoundary = true ) {
        IsoDBItem1 iso1;
        IsoDBItem2 iso2;
        initIsoDBItems( G, iso1, iso2, ignoreBoundary );
        return isoDB->checkForIsomorphicEntry( iso1, iso2 );
    }

    // Returns index (unique graph number, counting from 0)
    int add( BPG &G, bool ignoreBoundary = true ) {
        IsoDBItem1 iso1;
        IsoDBItem2 iso2;
        initIsoDBItems( G, iso1, iso2, ignoreBoundary );
        return isoDB->add( iso1, iso2 );
    }
};


class IsoStats
{
   static long long iso1Checks;
   static int isoHits;

   static int isoInits[MAX_TPARSE_LEN];
   static long long iso2Checks[MAX_TPARSE_LEN];
   static double isoInitTimes[MAX_TPARSE_LEN];
   static double isoCheckTimes[MAX_TPARSE_LEN];
   static double isoAddTime;
   static double isoClearTime;

   // Set while inside a check or init block, 0 outside
   static int last_len;

public:
   static void statistics( ostream& );

   static void addInit() { /*assert(last_len);*/ isoInits[last_len]++; }
   static void addHit() { isoHits++; }
   static void addIso1Check() { iso1Checks++; }
   static void addIso2Check() { /*assert(last_len);*/ iso2Checks[last_len]++; }

   static void startInit(int len);
   static void stopInit();
   static void startCheck(int len);
   static void stopCheck();
   static void startAdd(int len);
   static void stopAdd();
   static void startClear();
   static void stopClear();
};

#endif
