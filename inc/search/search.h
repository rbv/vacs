
// ------------------------------------------
// --------------- search.h -----------------
// ------------------------------------------

/*tex
 
\file{search.h}
\path{inc/search}
\title{Header for search class}
\classes{Search, SearchNodeIter}
\makeheader
 
xet*/


#ifndef _search_h
#define _search_h


#include "stdtypes.h"
#include "search/nodenum.h"
#include "general/database_f.h"
#include "general/database_v.h"
#include "search/proof_db.h"
#include "array/array.h"

// Only one instance of this class should be defined.
// It is called search, and is declared in this file.

class SearchNode;
class UnknownState;
class IrrelevantInfo;
class MinimalProof;
class NonminimalProof;
class PartialMinorProof;

class Search
{

private:

   // grow the search array by growSize
   //
   void grow();
   //
   static const int growSize;

   // Statistics
   uint64_t  _unknownSwaps;  // Number of times a node is swapped for a lower one in <_c
   uint64_t  _minimalSwaps;
   int _numGrowable;

   void updateGrowableCount( const SearchNode& sn );

   SearchNode* _cacheNode; // Used as return value for node(NodeNumber)

   // Run specific databases
   //
   FRSDatabase*   _snNodeDB;
   VRSDatabase*   _snChildDB;
   UnknownStateDatabase*    _uState;
   IrrelevantInfoDatabase*  _iInfo;

   // Persistent databases
   //
   MinimalProofDatabase*    _mProofs;
   NonminimalProofDatabase* _nProofs;

   IsoDatabase *_isoDB;

public:

   // ctor - allocate cache
   //
   Search();

   // dtor - close databases
   //
   ~Search();

   // Open databases
   //
   void initialize();

   // Also load iso database if required, if not already
   // (For browser utilities: can be very slow)
   //
   void loadIsoDatabase();

   // Clear the usage on all min and non-min proofs
   //
   void clearProofUsage();

   // Create databases 
   //
   void createDatabases();

   // Check if the node number is ok and in range.
   //
   bool isValid( NodeNumber ) const;

   // Return the node with the given nodenumber.
   // Unless the DB is cached, the reference returned is to 
   // internal storage, so only one can be active at a time.
   //
   SearchNode& node( NodeNumber n );

   // Get a node directly from the database (bypass cache)
   //
   void readNoCache( NodeNumber n, SearchNode& sn );

   // Return the minimal proof of a node.  Error if not minimal.
   //
   void minimalProof( NodeNumber, MinimalProof& );
   void minimalProofPMP( NodeNumber, PartialMinorProof& );

   // Return the nonminimal proof of a node.  Error if not nonminimal.
   //
   void nonminimalProof( NodeNumber, NonminimalProof& );

   // Return the state of an unknown node.  Error if not unknown.
   //
   void unknownState( NodeNumber, UnknownState& );
   //void unknownStatePMP( NodeNumber, PartialMinorProof& );
   //
   // 
   void updateUnknownState( NodeNumber, const UnknownState& );
   void updateUnknownStatePMP( NodeNumber, const PartialMinorProof& );
   void delUnknownState( NodeNumber );

   void updateNodeAndIso( NodeNumber n, SearchNode& sn, SearchNode::Status stat, int pfNum );

   // Return the isoref of the proof used by a node
   int isoRef( SearchNode sn );

   // Return the info of an irrelevant node.  Error if not irrelevant.
   //
   void irrelevantInfo( NodeNumber, IrrelevantInfo& );


   // Return the next avaliable node number.
   //
   //NodeNumber nextNodeNumber() const
   //   { return NodeNumber::nextNumber(); }

   // Return the total number of nodes in the system
   //
   int numberOfNodes() const
      { return _snNodeDB->numberOfKeys(); }

   // Number of growable minimal nodes in the tree (not number in databases)
   int growableMinimal() const { return _numGrowable; }

   // UGH, horrible abstraction leak!
   // If sn is a growable minimal node, decrement the count
   void maybeDecrementGrowableCount( SearchNode &sn );

   void incrementUnknownSwaps() { _unknownSwaps++; }
   void incrementMinimalSwaps() { _minimalSwaps++; }

   // Reload all cached information for a node.
   // 
   //void sync( NodeNumber n );

   // Reload all cached information for all nodes.
   //
   void sync();

   // Flush write buffers in db's
   //
   void flush();

   void statistics( ostream& );

   // ----------------------------

   // Complex information
   //
   //int checkTreeIsomorphism();

   // ----------------------------

   // Access to databases
   //
   MinimalProofDatabase&    minimalProofDatabase()    { return *_mProofs; }
   NonminimalProofDatabase& nonminimalProofDatabase() { return *_nProofs; }
   UnknownStateDatabase&    unknownStateDatabase()    { return *_uState; }
   IrrelevantInfoDatabase&  irrelevantInfoDatabase()  { return *_iInfo; }
   IsoDatabase&  isoDatabase() { assert(_isoDB); return *_isoDB; }

   template<class ProofClass>
   int addToDB( ProofClass &proof );
   int addToDB( MinimalProof &proof, PartialMinorProof& pmp );

   //  --- Manager Access Only ---

   // Add a new node to the tree.
   // Return its number.
   //
   NodeNumber addNode( const SearchNode& );
   //
   void changeNode( NodeNumber n, const SearchNode& );
   
   void addChildren( NodeNumber parent, const Array<NodeNumber>& children );

   void predictNodeNumbers( Array<NodeNumber>& numbers );

   void createRootNode();

   // Special procedures
   //
   void graph( NodeNumber n, RBBPG& G );

   // Get children
   //
   void children( NodeNumber n, Array<NodeNumber>& );

   // Prints a node on the stream
   //
   //
   // Level:
   //   0 = just the node
   //   1 = node + proof
   //   2 = node + proof + pmp (if applicable)
   //
   void printNode( ostream& o, NodeNumber n, int level );

   void texNode( texstream& o, NodeNumber n );

   Str statusAsString( NodeNumber n );

   // Set to true when program should terminate cleanly ASAP.
   volatile bool stopReceived;

};

//-------------------------------------------------------------------

// Declaration of global

extern Search search;

//-------------------------------------------------------------------

// Iterator

class SearchNodeIter 
{

public:

   static long long iterations;

   // Types of iterators.
   //
   enum Status
   { 
      Minimal =     0x01, 
      Nonminimal =  0x02, 
      Unknown =     0x04, 
      Irrelevant =  0x08, 

      MinimalOrNonminimal = 0x03,
      UnknownOrMinimal = 0x05,
      Relevant = 0x07,
      SAny = 0x0f,
      SNegate = 0x10
   };
      
   enum GrowStatus
   {
      Growable    = 0x01,
      NotGrowable = 0x02,
      Grown       = 0x04,
      GAny =         0x0f,
      GNegate =      0x10
   };

   enum FamilyStatus
   {
      InFamily    = 0x01,
      OutOfFamily = 0x02,
      NotComputed = 0x04,
      FAny =         0x0f,
      FNegate =      0x10
   };


private:

	// current position
	//
	int _pos;

	// iterator type
	//
	Status _status;
	GrowStatus _grow;
	FamilyStatus _family;

	// step the iterator in the specified direction (1 or -1)
	//
	NodeNumber step( int dir );

public:

	SearchNodeIter( Status s, GrowStatus g = GAny, FamilyStatus f = FAny )
	   { _status = s; _grow = g; _family = f; _pos = -1; }

	~SearchNodeIter()
	   {}

	// Set the type, mid-iteration
	//
	void type( Status s, GrowStatus g = GAny, FamilyStatus f = FAny )
	   { _status = s; _grow = g; _family = f; }

	// Return the iterator type
	//
	//IteratorType type()
	//   { return _type; }

	// return next node's number, NodeNumber::Invalid if no more
	//
	NodeNumber operator()()
	   { return step(1); }

	NodeNumber next()
	   { return step(1); }

	// return prev node's number, NodeNumber::Invalid if no more
	//
	NodeNumber prev()
	   { return step(-1); }

	// restart the iterator at the beginning of the collection
	//
	void restart() { _pos = -1; }

	// restart the iterator at the end of the collection
	//
	void end() { _pos = search.numberOfNodes(); }

	// move the iterator to the specified position
	//
	void moveToNode( NodeNumber n );

	// move the iterator by the specified amount 
	//
	void forceMove( int k );

};

//----------------------------------------------------------------------

#endif
