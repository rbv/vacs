
// ------------------------------------------
// ------------- searchcontrol.h ------------
// ------------------------------------------

/*tex
 
\file{searchcontrol.h}
\path{inc/search}
\title{Header for search control class}
\classes{SearchControl}
\makeheader
 
xet*/


#ifndef _searchcontrol_h
#define _searchcontrol_h


#include "search/nodenum.h"
#include "search/searchminor.h"
#include "vacs/runinfo.h"
#include "general/system.h"
#include "general/str.h"

#undef minor

class Search;


class SearchTimer 
#ifdef VACSOBJECT
	 		: private VacsObject
#endif
{
private:

   int _endTime;

public:

   SearchTimer( int minutes )
      { _endTime = System::timeInSeconds() + minutes*60; }

   operator const void*() const
      { if ( System::timeInSeconds() <= _endTime ) 
           return 0; else return this; }

   operator void*()
      { if ( System::timeInSeconds() <= _endTime ) 
           return 0; else return this; }

   bool operator!() const
      { if ( System::timeInSeconds() <= _endTime )
           return false; else return true; }

};


// This class aids the interaction of the programs with
// the external file.

class SearchControl
#ifdef VACSOBJECT
		 : public VacsObject
#endif
{
private:

	// Print and scan strings for the external file.
	// The last %s is for scanning, to make sure there's no extra stuff.
	//
	static const char* ExternalNMFormat1;
	static const char* ExternalNMFormat2;

public:

	enum Command
	{
	   Stop,				// no args
	   GrowFromNode,			// node

	   NodeIsMinimal,			// node
//	   NodeMinorDistTestset,		// node, minor#, test#
	   NodeMinorsDistTestset,		// node, array of minor#s,test#
	   NodeMinorDistExtn,			// node, minor#, extn
	   NodeMinorsDistExtn,			// node, array of minor#s, extn
	   NodeDistSearchFailed,		// node, # tries

	   NodeNonminimal,			// node
//	   NodeNonminimalViaPretest,		// node, pretest #
	   NodeMinorCongTestset,		// node, minor #
	   NodeMinorCongSearch,			// node, minor #
	   NodeMinorSearchFailed,		// node, # of tries

	   Invalid				// Private flag, never returned
	};

   // Structure filled by processNextLine (i.e. returned to Manager)
   //
   struct CommandStruct
   {
      friend class SearchControl;

      Command command;

   private:

      void clearFlags();

      NodeNumber _nodeNumber;
      bool       _nodeNumberValid;
      
      MinorNumber _minorNumber;
      bool        _minorNumberValid;
      
      int  _tries;
      bool _triesValid;
      
      NBBPG _extension;
      bool  _extensionValid;

      Array<int>_testNumbers;
      bool      _testNumbersValid;

      Array<MinorNumber> _minorNumbers;
      bool               _minorNumbersValid;

   public:

      CommandStruct() { clearFlags(); }

      NodeNumber nodeNumber() const
         { assert( _nodeNumberValid ); return _nodeNumber; }
      
      MinorNumber minorNumber() const
         { assert( _minorNumberValid ); return _minorNumber; }
         
      Array<MinorNumber> minorNumbers() const
         { assert( _minorNumbersValid ); return _minorNumbers; }
         
      int tries() const
         { assert( _triesValid ); return _tries; }
         
      const NBBPG& extension() const
         { assert( _extensionValid ); return _extension; }

      const Array<int>& testNumbers() const
         { assert( _testNumbersValid ); return _testNumbers; }

   };

private:

   static void write( Command );
   static void write( Command, NodeNumber, MinorNumber );
   static void write( Command, NodeNumber, MinorNumber, const NBBPG& );
   static void write( Command, NodeNumber, const Array<MinorNumber>& );
   static void write( Command, NodeNumber, const Array<MinorNumber>&,
                      const NBBPG& );
   static void write( Command, NodeNumber );
   static void write( Command, NodeNumber, int );
   static void write( Command, NodeNumber, const Array<MinorNumber>&, 
                      const Array<int>& );
   //static void write( Command, int );

   static void sendSignalToManager();

   // parsing utilities
   //
   static bool parseCommand( const Str&, Command& );
   static bool parseNodeNumber( const Str&, NodeNumber& );
   static bool parseInteger( const Str&, int& );
   static bool parseExtension( const Str&, NBBPG& );
   static bool parseMinorNumber( const Str&, MinorNumber& );
   static bool parseMinorNumbers( const Str&, Array<MinorNumber>& );
   static bool parseIntArray( const Str&, Array<int>& );

public:

	//--------------------------------------------------------------

	// Functions for manager to process commands.

	// For Manager only.

	// Check if a new line is available, and process it if true.
	// CommandStruct is filled with the info appropriate
	// to the command.  The command field is Invalid if
	// there is no line available.
	//
	static void processNextLine( CommandStruct& );

	// Command processing is complete.
	// Update the command position.
	//
	static void updateCommandPosition();

   //--------------------------------------------------------------

   // Functions for programs to send commands to manager.

   static void stop();
   static void growFromNode( NodeNumber );

   static void nodeIsMinimal( NodeNumber );
   static void nodeMinorDistTestset( NodeNumber, MinorNumber, int testNumber );
   static void nodeMinorsDistTestset( NodeNumber, const Array<MinorNumber>&,
                                   const Array<int>& testNumbers );
   static void nodeMinorDistExtn( NodeNumber, MinorNumber, const NBBPG& );
   static void nodeMinorsDistExtn( NodeNumber, const Array<MinorNumber>&,
                                   const NBBPG& );
   static void nodeDistSearchFailed( NodeNumber, int numTries );


   static void nodeNonminimal( NodeNumber );
   static void nodeNonminimalViaPretest( NodeNumber, int pretestNumber );
   static void nodeMinorCongSearch( NodeNumber, MinorNumber minorNumber );
   static void nodeMinorCongTestset( NodeNumber, MinorNumber minorNumber );
   static void nodeMinorSearchFailed( NodeNumber, int numTries );

};

#endif
