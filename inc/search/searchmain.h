 
// -------------------------------------------
// -------------- searchmain.h ---------------
// -------------------------------------------

/*tex
 
\file{searchmain.h}
\path{inc/search}
\title{Header for main entry point of search}
\classes{}
\makeheader
 
xet*/

#ifndef _searchmain_h
#define _searchmain_h

#include "stdtypes.h"

// These are the entry points for the main programs.
// These are defined in the directory searchmain, in the files
// sm_dispatch.c, sm_obstdump.c, etc.
//
void mainSearch(   int argc, char **argv );
void mainDispatch( int argc, char **argv );
void mainTbrowse(  int argc, char **argv );
void mainText(     int argc, char **argv );
void mainStats(    int argc, char **argv );
void mainLevels(   int argc, char **argv );
void mainClean(    int argc, char **argv );
void mainWorker1(  int argc, char **argv );
void mainHuman1(  int argc, char **argv );
void mainTestset(  int argc, char **argv );
void mainManager(  int argc, char **argv );
void mainExtract(  int argc, char **argv );
void mainImport(   int argc, char **argv );
void mainPlay(     int argc, char **argv );
void mainIsoClean( int argc, char **argv );
void mainDBTool(   int argc, char **argv );

// Utility functions for search.
//
// Launch the named program.
// Returns the pid of the launched program (-1 if error)
//
int launchProgram(
   char* programName,
   char* arg1 = 0,
   char* arg2 = 0,
   char* arg3 = 0,
   char* arg4 = 0
   );

// Memory testing function
//
#ifdef FST
   void allClassesStatus();
#endif

#endif
