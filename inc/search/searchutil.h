
 
// -------------------------------------------
// -------------- searchutil.h ---------------
// -------------------------------------------

/*tex
 
\file{searchutil.h}
\path{inc/search}
\title{Header for search utility functions}
\classes{}
\makeheader
 
xet*/



#ifndef _searchutil_h
#define _searchutil_h

#include "array/array.h" // replace with forward decl later (sorried)
#include "search/nodenum.h"
#include "search/searchnode.h"  // kill later
#include "bpg/bpg.h"            // kill later

class SearchNode;
class RBBPG;


//class Array<SearchNode>;
//class Array<RBBPG>;


void searchStart();

void processExternalCommand();

// Instruct the search to grow from the specified node.
// Optionally return unknown nodes.
// Manager only.
//
void growNode( NodeNumber, Array< NodeNumber >* unknownNodes= 0,
 bool assumeNoncanonic = false, 
 const NBBPG* prefix = 0 );

void nodeCreation(
  const Array<SearchNode>& C,
  const Array<RBBPG>& G,
  const Array<NodeNumber>& newNumbers,
  Array<NodeNumber>* unknowns = 0,
  bool assumeNoncanonic = false,
  const NBBPG* prefix = 0
);

#endif
