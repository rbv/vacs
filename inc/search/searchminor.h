
// ---------------------------------------------------
// ------------------ searchminor.h ------------------
// ---------------------------------------------------

/*tex
 
\file{searchminor.h}
\path{inc/search}
\title{Header for search minor class}
\classes{SearchMinor}
\makeheader
 
xet*/


#ifndef _searchminor_h
#define _searchminor_h

#include "search/nodenum.h"
#include "bpg/bpg.h"
#include "general/str.h"
#include "set/dict.h"


#undef minor

typedef short MinorNumber;
typedef short DistinguisherNumber;

inline int compare( MinorNumber x, MinorNumber y ) { return x-y; }


// Header for class SearchMinor

// SearchMinor is a misnamed class that contains a description of a reduction
// operation in some graph order
// "or the current status of the search for information about the minor"?

class SearchMinor
{

   iodeclarations( SearchMinor )

public:

   enum ReductionType { DeleteEdge, ContractEdge, DeleteVertex };

private:

   // FIXME: replace shorts with uchars to save space
   // (Keeping shorts for now so as to not break existing DBs)

   // The position of the first edge/vertex op in the BPG operated on
   //
   short _parsedInfo1;

   // The position of the second edge/vertex op in the BPG operated on if any
   //
   short _parsedInfo2;

   // the type of mangling that was done to it
   // enum ReductionType
   //
   uchar _type;

public:

   SearchMinor();

   SearchMinor(  ReductionType type, int parsedPos1, int parsedPos2 = 0 );

   ~SearchMinor();

   static void zeroFill( SearchMinor& );

   // --- Information ---

   int parsedPosition() const { return _parsedInfo1; }

   ReductionType type() const { return ReductionType( _type ); }

   bool isPrimitive( const BPG &B, const BGraph &G ) const;

   // Calculate the minor described by this of the passed graph.
   // If param is derived from BPG, the virtual mechanism should work fine.
   //
   void calcMinor( BPG& ) const;

   int compare( const SearchMinor& ) const;

};

//--------------------------------------------------

typedef NBBPG Distinguisher;
typedef Dictionary< DistinguisherNumber, Distinguisher >DistinguisherDictionary;
typedef Dictionary< MinorNumber, SearchMinor > SearchMinorDictionary;
typedef Dictionary< MinorNumber, DistinguisherNumber > DistinguisherInfo;

//--------------------------------------------------

class PartialMinorProof 
{
   iodeclarations( PartialMinorProof )

public:
   enum Status
   {
      Distinguished,
      NotDistinguished
   };

private:

   DistinguisherDictionary _distinguishers;
   SearchMinorDictionary _minors;
   DistinguisherInfo _proofs;
   //RBBPG _bpg;

   int nextFreeDistNumber; 
   int nextFreeMinorNumber; 

   void operator=( const PartialMinorProof& ); // { aassert(false); }

   void addAllEdgeDeletions( const RBBPG& G );
   void addAllSubgraphs( const RBBPG& );
   void addAllMinors( const RBBPG& );

public:

   PartialMinorProof();

   ~PartialMinorProof();

   // General information
   //
   int numberOfMinors() const;
   //
   int numberOfDistinguishers() const;


   // Proof status information
   //
   int numberOfMinorsDistinguished() const;
   //
   bool proofIsComplete() const;
   //
   bool proofIsOptimal() const;
   //
   Status minorStatus( MinorNumber ) const;
   //
   DistinguisherNumber minorDistinguisher( MinorNumber ) const;
   //
   DistinguisherNumber checkForIsoDistinguisher( const NBBPG& ) const;
   //
   void minorsDistinguishedByDistinguisher( 
      DistinguisherNumber, Array<MinorNumber>& ) const;


   // Access to internal structures
   //
   const DistinguisherDictionary& distinguishers() const;
   //
   const SearchMinorDictionary& minors() const;


   // Manipulation
   //
   MinorNumber addMinor( const SearchMinor& );
   //
   void addAllReductions( const RBBPG& );
   //
   //void clearAll();
   //
   void removeMinor( MinorNumber );
   //
   DistinguisherNumber addDistinguisher( const Distinguisher& );
   //
   void addProof( MinorNumber, DistinguisherNumber );
   //
   //void changeProof( MinorNumber, DistinguisherNumber );
   //
   //void optimizeProof();
   
   texstream& texOut( texstream&, const RBBPG& );

   // form the minors of the graph and dump the cong states on the 
   // output stream
   //
   static void dumpCongruenceStates( ostream&, RBBPG& );
};


#endif
