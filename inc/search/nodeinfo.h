 
// -------------------------------------------
// --------------- nodeinfo.h ----------------
// -------------------------------------------
 
/*tex
 
\file{nodeinfo.h}
\path{inc/search}
\title{Header for search node information classes}
\classes{MinimalProof,NonminimalProof,UnknownState,IrrelevantInfo}
\makeheader
 
xet*/
 
 
 
#ifndef _nodeinfo_h
#define _nodeinfo_h

#include "search/searchminor.h"
#include "search/searchnode.h"
//#include "search/nodenum.h"

#undef minor

class Graph;
class PartialMinorProof;

//---------------------------------------------------------------------

class TimeStamp
{
   iodeclarations( TimeStamp )

   int _creation;
   int _update;

public:

   void setCreation() const;

   void setUpdate() const;

   char* created() const;

   char* updated() const;

};

//-------------------------------------------------------------

// FIXME: add a bit to proofs warning that they might not be associated with the correct t-parse
// (as a result of isomorphism matching, either to choice a canonical representative, or when
// rebuilding the search tree on a re-run)

class ProofUsage
{
   iodeclarations( ProofUsage )

   NodeNumber::nodeKey _user;

   // I doubt this should be mutable
   mutable int _isoRef;

   enum { NotUsed = -1 };

public:

   ProofUsage();

   // flag as not used by anybody
   //
   void notUsed();

   // set who were used by
   //
   void usedBy( NodeNumber n );

   // check if node is in use
   //
   bool isUsed() const;

   // return the node number using us
   // error if not in use
   //
   NodeNumber usedBy() const;

   // isoRef
   //
   void isoRef( int ref ) const { _isoRef = ref; }
   int isoRef() const { return _isoRef; }

};

//---------------------------------------------------------------------

class MinimalProof : public TimeStamp, public ProofUsage
{
   iodeclarations( MinimalProof )

public:

   enum ProofType { Root, DistinguisherProof, OutOfFamily, External, TightCong,
                    Testset, None, GiveUp };

private:

   ProofType _type;

public:

   MinimalProof() {}

   MinimalProof( ProofType type )
      { _type = type; }

   bool hasPMP() const
      { return _type == DistinguisherProof || _type == Testset; } 

   ProofType type() const  { return ProofType(_type); }

   const char* typeAsString() const;

   static const SearchNode::Status status_enum = SearchNode::Minimal;
};

//------------------------------------------------------------------------

class NonminimalProof : public TimeStamp, public ProofUsage
{
   iodeclarations( NonminimalProof )

public:

   enum ProofType { CongruentMinor, Parent, External, OutOfFamily, NotSaved,
                    Testset, GiveUp, None, HNoncanonic };

private:

   ProofType _type;
   SearchMinor _minor;

public:

   NonminimalProof() {}

   NonminimalProof( ProofType type );

   NonminimalProof( const SearchMinor& minor, ProofType type = CongruentMinor );

   ProofType type() const { return ProofType(_type); }

   const char* typeAsString() const;

   // Only valid if proof type is CongruentMinor or OutOfFamily.
   //
   const SearchMinor& congruentMinor() const;

   static const SearchNode::Status status_enum = SearchNode::Nonminimal;
};

//------------------------------------------------------------------------


class UnknownState : public ProofUsage
{
 
   iodeclarations( UnknownState )

private:
 
   int _extensionSearches;
 
public:
 
   UnknownState() { _extensionSearches = 0; }

   int extensionSearches() const
      { return _extensionSearches; }

   void incExtensionSearches() { _extensionSearches++; }
   void incExtensionSearches( int i ) { _extensionSearches += i; }

   const char* typeAsString() const;

   static const SearchNode::Status status_enum = SearchNode::Unknown;
};


//------------------------------------------------------------------------

class IrrelevantInfo
{

   iodeclarations( IrrelevantInfo )
 
public:
 
   enum InfoType { Isomorphic, MultiGraph, IsoSibling, Disconnected,
                   ENoncanonic, QNoncanonic, Pretest, NonTree, Prefix, OutOfFamily, Oneconnected };
 
private:
 
   InfoType _type;
 
   union
   {
      int _isomorphicNode;
      int _pretest;
   } _data;

public:

   IrrelevantInfo();

   // Set type
   //
   IrrelevantInfo( InfoType type );

   // general ctor
   //
   IrrelevantInfo( InfoType type, int data );

   // Isomorphism 
   //
   IrrelevantInfo( InfoType type, NodeNumber n );

   InfoType type() const { return _type; }
   
   int pretest() const
      { assert( type() == Pretest ); return _data._pretest; }

   NodeNumber isomorphic() const
      { assert( type() == Isomorphic || type() == IsoSibling ); 
        return NodeNumber(_data._isomorphicNode);
      }

   NodeNumber isoSibling() const
      { assert( type() == IsoSibling ); 
        return NodeNumber(_data._isomorphicNode);
      }

   const char* typeAsString() const;

   static const SearchNode::Status status_enum = SearchNode::Irrelevant;
};


#endif
