 
// -------------------------------------------
// -------------- searchnode.h ---------------
// -------------------------------------------

/*tex
 
\file{searchnode.h}
\path{inc/search}
\title{Header for search node class}
\classes{SearchNode, SearchNodeEvidence, SearchNodeStatus}
\makeheader
 
xet*/



#ifndef _searchnode_h
#define _searchnode_h

#include "stdtypes.h"
#include "search/nodenum.h"
#include "bpg/operator.h"
#include "general/str.h"

//#include "bpg/bpg.h"
//#include "search/searchminor.h"

//typedef Array<NodeNumber> NodeNumberCollection;
//typedef Collection<BPG> BPGCollection;

#undef minor
#undef major

//--------------------------------------------

class PACKED ProofRef
{
   iodeclarations( ProofRef )

public:
   enum Status { Minimal=0, Nonminimal=1, Unknown=2, Irrelevant=3 };

protected:
   union // of proof database keys
   {
      int _minimalProof;
      int _nonminimalProof;
      int _unknownState;
      int _irrelevantInfo;
   } _state;

   // Status and membership info
   //
   uchar _flags;

   enum
   {
      StatusMask = 0x03, StatusShift = 0,
   };

   // bitfield access
   //
   void setFlag( int mask, int shift, int value )
      { _flags &= ~(mask<<shift); _flags |= ( value << shift ); }
   //
   int getFlag( int mask, int shift ) const
      { return ( _flags >> shift ) & mask; }

public:

   ProofRef() { setUnused(); }

   bool isUsed() { return _state._minimalProof != -1; }

   void setUnused() { _state._minimalProof = -1; }

   Status status() const { return (Status)getFlag( StatusMask, StatusShift ); }

   void status( Status b ) { setFlag( StatusMask, StatusShift, b ); }

   void setProof( Status b, int proofId ) { status(b); _state._minimalProof = proofId; }

   int minimalProof() const
      { assert( status() == Minimal ); return _state._minimalProof; }

   int nonminimalProof() const
      { assert( status() == Nonminimal ); return _state._nonminimalProof; }

   int unknownState() const
      { assert( status() == Unknown ); return _state._unknownState; }

   int irrelevantInfo() const
      { assert( status() == Irrelevant ); return _state._irrelevantInfo; }

   void minimalProof( int ref )
      { assert( status() == Minimal ); _state._minimalProof = ref; }

   void nonminimalProof( int ref )
      { assert( status() == Nonminimal ); _state._nonminimalProof = ref; }

   void unknownState( int ref )
      { assert( status() == Unknown ); _state._unknownState = ref; }

   void irrelevantInfo( int ref )
      { assert( status() == Irrelevant ); _state._irrelevantInfo = ref; }

   const char* statusAsString() const;

   char statusAsChar() const;
};

class PACKED SearchNode : public ProofRef
{
   iodeclarations( SearchNode )

public:

   enum Membership { InF=0, NotInF=1, NotComputed=2 };

   enum GrowStatus { Growable=0, NotGrowable=1, Grown=2 };

private:

   // Bitmasks and shifts for ProofRef _flags
   //
   enum
   {
      MbrMask    = 0x03, MbrShift    = 2,
      GrowMask   = 0x03, GrowShift   = 4
   };
   //
   Operator _op;

   // graph information: operator and tree depth (root is 0)
   //
   short _depth;

   // tree information
   //
   NodeNumber _parent;

public:

   SearchNode();

   // constructor - create and initialize
   //
   SearchNode( NodeNumber parentNumber, Operator extensionOp, short depth );

   // copy ctor - disallowed
   //
   SearchNode( const SearchNode& );

   // Assignment (bitwise)
   //
   void operator=( const SearchNode& );

   // load from file system
   //
   // SearchNode( NodeNumber );

   // Create the root node.
   //
   static SearchNode createRootNode();

   // dtor
   //
   ~SearchNode();

   // --- Information ---

   NodeNumber parent() const
     { return _parent; }

   int depth() const { return _depth; }

   Operator op() const { return _op; }

   // --- Status information ---

   Membership membership() const
      { return (Membership) getFlag( MbrMask, MbrShift );}

   GrowStatus growStatus() const
      { return (GrowStatus) getFlag(GrowMask,GrowShift);}

   bool hasChildren() const
      { return growStatus() == Grown; }

   // 
   //

   // Return the top-level status as a string (Minimal, Nonminimal, ... )
   //
   static Str statusAsString( NodeNumber n );

   // Return the top-level status as a char (M,N,U,I)
   //
   static char statusAsChar( NodeNumber n );


   // --- Setting status ---

   void membership( Membership b ) { setFlag( MbrMask, MbrShift, b ); }

   void growStatus( GrowStatus b ) { setFlag( GrowMask, GrowShift, b ); }


   // Status as text 
   //
   const char* statusAsString() const { return ProofRef::statusAsString(); }
   //
   const char* growStatusAsString() const;
   //
   const char* membershipAsString() const;
   //
   char statusAsChar() const { return ProofRef::statusAsChar(); }
   //
   int proofReference() const;

};

#endif
