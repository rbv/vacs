
// -------------------------------------------
// -------------- searchutil2.h --------------
// -------------------------------------------

/*tex
 
\file{searchutil2.h}
\path{inc/search}
\title{Header for search utility functions}
\classes{}
\makeheader
 
xet*/



#ifndef _searchutil2_h
#define _searchutil2_h

#include "array/array.h" // replace with forward decl later (sorried)
#include "search/nodenum.h"
#include "search/searchnode.h"  // kill later
#include "bpg/bpg.h"            // kill later


MinorNumber checkForCongruentMinor(
   const RBBPG& G,
   PartialMinorProof& pmp
);


// returns true iff node is minimal
// DO NOT call if graph is not out of family
bool outOfFamilyMinimalCheck(
   RBBPG& graph,
   PartialMinorProof& pmp
);


void randomExtensionSearch(
   const RBBPG& G,
   PartialMinorProof& proof,
   const class randomExtensionSearchInfo&,
   // rest of params are optional (only used by extend_sm)
   int run = 0,
   Array< NBBPG >* foundExtensions = 0,
   Array< Array< MinorNumber > >* foundMinors = 0,
   ostream* log = 0
);

// return true iff G has a tilde cong minor
//
bool checkForTildeCongruentMinor(
   const RBBPG& G,
   int extnRuns,
   const class randomExtensionSearchInfo&,
   ostream* log = 0
);

bool universalDistinguisherSearch(
   const RBBPG& G,
   PartialMinorProof& proof,
   const randomExtensionSearchInfo& info,
   // rest of params are optional
   int run = 0,
   ostream* log = 0
);



#endif
