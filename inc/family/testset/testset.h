
/*
 * Generic base class testset header 
 */

/*tex

\file{testset.h}
\path{inc/family/testset}
\title{Header for test set basic methods}
\classes{}
\makeheader

xet*/

#ifndef _testset_h
#define _testset_h

#include "array/dyarray.h"
#include "array/parray.h"
#include "graph/bgraph.h"
#include "bpg/bpg.h"
#include "search/searchminor.h"

typedef bool (*familyMembership)(const class Graph&, int);

class TestSet : public DyArray<BGraph*>
{

  iodeclarations( TestSet )

  //Array< Array< int > >* impliesOOF;
  //Array< Array< int > >* impliesInF;
  SquareMatrix< bool >* impliesOOF;
  SquareMatrix< bool >* impliesInF;

protected:

  int _fixedK;

  familyMembership _membership;

  // worker for initTests method
  //
  bool _add( const BGraph *G )
   {
     if ( _membership( CAST(*G,Graph), _fixedK ) )
      { 
	append(new BGraph(*G)); 
        return true; 
      }
     else { return false; }
   }

public:

  // constructor and distructor
  //
  TestSet() { impliesOOF = 0; impliesInF = 0; }
  //
  ~TestSet(); // destroys all tests
  
  /*
   * Have each family generates their own tests.
   */
  void initTests( familyMembership member, int fixedK, int pathwidth ) 
   {
     _membership = member;
     _fixedK = fixedK; 

     (void) pathwidth;	// defaults to no tests generated.
   }

  void getState( const BPG &G, Array<bool> &_state );

  int applyTestset( const class RBBPG& graph, class PartialMinorProof& pmp,
	Array<MinorNumber> &distMinors, Array<int>& testNumbers );

  void buildImplications();
  void dumpImplications( bostream& );
  void loadImplications( bistream& );
};

#endif
