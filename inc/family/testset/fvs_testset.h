
/*
 * FVS testset header routines
 */

/*tex

\file{fvs_testset.h}
\path{inc/family/testset}
\title{Header for feedback vertex set test set routines}
\classes{}
\makeheader

xet*/

#ifndef fvs_testset_h
#define fvs_testset_h

#include "family/testset/testset.h"
#include "graph/algorithm/fvs_ga.h"
#include "graph/forest.h"

//extern FVS_TestSet FVS_Tests; -- now in family class.

class FVS_TestSet : public TestSet
{
public:

  // overrides
  //
  void initTests( familyMembership member, int fixedK, int pathwidth );

};


typedef PIterSet< LForest > LForests;
//
extern void generateForestTests(int t, LForests &FTests);

#endif
