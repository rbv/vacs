
// -------------------------------------------
// ---------------- fam_mp.h -----------------
// -------------------------------------------

/*tex
 
\file{fam_mp.h}
\path{inc/family}
\title{Family header for maximum path functions}
\classes{MaximumPathCongruence, MaximumPathFamily}
\makeheader

\begin{caveats}
This is a private header.
Only fam_mp.c and problem.c should include this file.
\end{caveats}
 
xet*/

#ifndef _fam_maxpath_h
#define _fam_maxpath_h

#include "family/family.h"
#include "bpg/algorithm/maxpath_b.h"


class MaximumPathCongruence : public Congruence
{

private:

   MaxPathState _state;

   void compute( const class BPG& );

   bool operator==( const Congruence& c ) const;

};


class MaximumPathFamily : public Family
{

   bool _congruenceAvailable() const { return true; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const
      { return new MaximumPathCongruence; }

//   int _applyTestset( const RBBPG&, PartialMinorProof&,
//	Array<MinorNumber> &distMinors ) const { return -1; }

   void _setTestset() {}


public:

   MaximumPathFamily();

};


#endif
