
// -------------------------------------------
// ---------------- fam_vc.h ------------------
// -------------------------------------------

/*tex
 
\file{fam_vc.h}
\path{inc/family}
\title{Family header for vertex cover functions}
\classes{VertexCoverCongruence, VertexCoverFamily}
\makeheader

\begin{caveats}
This is a private header.
Only vertcover.c and problem.c should include this file.
\end{caveats}
 
xet*/

#ifndef _fam_vertcover_h
#define _fam_vertcover_h

#include "family/family.h"
#include "bpg/algorithm/vertcover_b.h"


class VertexCoverCongruence : public Congruence
{

private:

   Array<vertNum> _state;

   void compute( const class BPG& );

   bool operator==( const Congruence& c ) const;

};


class VertexCoverFamily : public Family
{

private:

   bool _congruenceAvailable() const { return true; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const
    { 
     return new VertexCoverCongruence; 
    }

//   int _applyTestset( const RBBPG&, PartialMinorProof&,
//	Array<MinorNumber> &distMinors ) const { return -1; }

   void _setTestset() {}

public:

   VertexCoverFamily();

};


#endif
