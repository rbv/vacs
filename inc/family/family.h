

// -------------------------------------------
// -------------- family.h --------------------
// -------------------------------------------

/*tex
 
\file{family.h}
\path{inc/family}
\title{Header for family class}
\classes{Family, Congruence, NonminimalPretests}
\makeheader
 
This file does not need to be edited when adding new problems.
See {\tt probinfo.h} for list of current families.
xet*/

#ifndef _family_h
#define _family_h

#include "stdtypes.h"
#include "array/array.h"
#include "bpg/bpg.h"
#include "family/testset/testset.h"

class BGraph;
class NonminimalPretests;
class PartialMinorProof;
class SearchNode;
#include "search/searchminor.h"

class Congruence 
{

   friend class Family;

public:

   virtual bool operator==( const Congruence& ) const = 0;

   virtual void compute( const BPG& ) = 0;

   virtual void out( ostream& );

   virtual ~Congruence() {};

};


class Family 
{
private:

   // Keep a representative, to send virtual messages to.
   //
   static Family* rep;

   static TestSet *ts_rep;

   //static int _fixedK;  // -1 if not valid

   virtual bool _member( const BPG& g ) const = 0;

   virtual bool _congruenceAvailable() const = 0;

   virtual bool _testsetAvailable() const = 0;

   virtual const NonminimalPretests& _nonminimalPretests() const = 0;

   virtual Congruence* _newCongruence() const = 0;

   virtual void _setTestset() = 0; /* { setTestset( new Testset ); } */

   virtual void _printstats( ostream &os ) {}

protected:
   /*
    * Following should be used only by the derived _setTestset methods.
    */
   void setTestset( TestSet *t ) { ts_rep = t; }

   static bool noTestSet() { return ts_rep == 0; }

public:

   // This method is called by ProbInfo.
   //
   static void setFamily( Family& f ) 
      { rep = &f; }

   static bool member( const BPG& g )
      { return rep->_member( g ); }

   static Congruence* newCongruence()
      { return rep->_newCongruence(); }

   static bool congruenceAvailable()
      { return rep->_congruenceAvailable(); }

   static bool testsetAvailable()
      { return rep->_testsetAvailable(); }

   static const NonminimalPretests& nonminimalPretests()
      { return rep->_nonminimalPretests(); }

   static void printstats( ostream &os )
      { return rep->_printstats( os ); }

   // creates external database of tests
   //
   static void buildTestset();  	

   static int applyTestset( const RBBPG& graph, PartialMinorProof& pmp,
	Array<MinorNumber> &distMinors, Array<int>& testNumbers ) 
      { 
        if ( noTestSet() ) rep->_setTestset();
        return ts_rep->applyTestset( graph, pmp, distMinors, testNumbers ); 
      }

   Family() {}

   ~Family() { if ( ! noTestSet() ) delete ts_rep; }

};

//---------------------------------------------
// Should be inside NonminimalPretests...sorried
//
enum NMPretestInternalType { _BPG, _BGRAPH };
//
struct NMPretestInternal
{
   void* fnct;
   const char* name;
   NMPretestInternalType type;
};

class NonminimalPretests
{

private:

   /*const*/ RBBPG* _bpg;
   /*const*/ BGraph* _bgraph;

   Array<NMPretestInternal> tests;

public:

   NonminimalPretests();

   ~NonminimalPretests();

   typedef bool (*NMPretestGraph)( const BGraph& );
   typedef bool (*NMPretestBPG)( const RBBPG& );

   // Name should point to static storage (i.e. string is not copied)
   //
   void addTest( const char* name, NMPretestGraph );
   void addTest( const char* name, NMPretestBPG );

   void setGraph( const RBBPG& ) const;
   //void setBGraph( const BGraph& ) const;

   void releaseGraph() const;
   //void releaseBGraph() const;

   int numberOfTests() const { return tests.size(); }

   const char* name( int i ) const;

   // returns the test number that first returns true.
   // Returns -1 if all return false.
   //
   int applyAllTests() const;
   //int applyBGraphTests() const;
   int applyBGraphTests(const BGraph&) const;

   bool applyTest( int i ) const;

};

#endif
