
// -------------------------------------------
// ------------- fam_fvs.h -------------------
// -------------------------------------------

/*tex
 
\file{fam_fvs.h}
\path{inc/family}
\title{Family header for feedback vertex set functions}
\classes{FvsCongruence,FvsFamily}
\makeheader

\begin{caveats}
This is a private header.
Only fam_fvs.c and problem.c should include this file.
\end{caveats}
 
xet*/


#ifndef _fam_fvs_h
#define _fam_fvs_h

#include "family/family.h"
#include "bpg/algorithm/fvs_b.h"

// do we want to use the testset?
//
#define FVS_TESTSET

//----------------------------------------------------------------------

// for the test set (in testset/fvs_testset.h)
//
//typedef PIterSet< LForest > LForests;
//void generateForestTests( int, LForests& );
//
#ifdef FVS_TESTSET
#include "family/testset/fvs_testset.h"
#endif

//----------------------------------------------------------------------

class FvsCongruence : public Congruence
{

#ifndef FVS_TESTSET_old
   // use d.p.
   fvsState _state;
#else
   // use pass set
   Array<bool> _state;
#endif

   void compute( const BPG& );

   bool operator==( const Congruence& c ) const;

   void out( ostream &o ) 
    {
     o << _state;
    }
};

class FvsFamily : public Family
{
   bool _congruenceAvailable() const { return true; }

#ifdef FVS_TESTSET
   bool _testsetAvailable() const { return true; }
#else
   bool _testsetAvailable() const { return false; }
#endif

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const { return new FvsCongruence; }

   //int _applyTestset( const RBBPG&, PartialMinorProof&,
   // Array<MinorNumber> &distMinors ) const;

   void _setTestset();

public:

   FvsFamily();

   ~FvsFamily() { /* FVS_Tests.deleteTests(); -- destructor does this */  }

};

//----------------------------------------------------------------------

#endif
