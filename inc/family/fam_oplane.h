
// -------------------------------------------
// -------------- fam_oplane.h ---------------
// -------------------------------------------

/*tex
 
\file{fam_oplane.h}
\path{inc/family}
\title{}
\classes{}
\makeheader

\begin{caveats}
This is a private header.
Only fam_oplane.c and problem.c should include this file.
\end{caveats}
 
xet*/

#ifndef _fam_oplane_h
#define _fam_oplane_h

#include "family/family.h"


class OuterPlanarCongruence : public Congruence
{

private:

   void compute( const class BPG& );

   bool operator==( const Congruence& c ) const;

};


class OuterPlanarFamily : public Family
{

   bool _congruenceAvailable() const { return false; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const
      { return new OuterPlanarCongruence; }

//   int _applyTestset( const RBBPG&, PartialMinorProof&,
//	Array<MinorNumber> &distMinors ) const { return -1; }

   void _setTestset() {}

public:

   OuterPlanarFamily();

};


#endif
