
// -------------------------------------------
// ------------- fam_fes.h -------------------
// -------------------------------------------

/*tex
 
\file{fam_fes.h}
\path{inc/family}
\title{Family header for feedback edge set functions}
\classes{FesCongruence,FesFamily}
\makeheader

\begin{caveats}
This is a private header.
Only fam_fes.c and problem.c should include this file.
\end{caveats}
 
xet*/


#ifndef _fam_fes_h
#define _fam_fes_h

#include "family/family.h"
#include "bpg/algorithm/fes_b.h"

//----------------------------------------------------------------------

class FesCongruence : public Congruence
{

   void compute( const BPG& );

   bool operator==( const Congruence& c ) const;

   void out( ostream &o );
};

class FesFamily : public Family
{
   bool _congruenceAvailable() const { return false; }

   bool _testsetAvailable() const { return true; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const { return new FesCongruence; }

   void _setTestset();

public:

   FesFamily();

   ~FesFamily() {}

};

//----------------------------------------------------------------------

#endif
