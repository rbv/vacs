
// -------------------------------------------
// -------------- fam_plane.h ----------------
// -------------------------------------------

/*tex
 
\file{fam_plane.h}
\path{inc/family}
\title{Family header for maximum path functions}
\classes{PlanarCongruence, PlanarFamily}
\makeheader

\begin{caveats}
This is a private header.
Only fam_planar.c and problem.c should include this file.
\end{caveats}
 
xet*/

#ifndef _fam_planar_h
#define _fam_planar_h

#include "family/family.h"

// These pretests are shared with fam_embedding.c
bool planenmpIsoV( const RBBPG& g );
bool planenmpDangle( const RBBPG& g );
bool planenmpContractDeg2( const BGraph &G );


class PlanarCongruence : public Congruence
{

private:

   // Not implemented
   void compute( const class BPG& );
   bool operator==( const Congruence& c ) const;

};


class PlanarFamily : public Family
{

   bool _congruenceAvailable() const { return false; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const
      { return new PlanarCongruence; }

//   int _applyTestset( const RBBPG&, PartialMinorProof&,
//	Array<MinorNumber> &distMinors ) const { return -1; }

   void _setTestset() {}

public:

   PlanarFamily();

};


#endif
