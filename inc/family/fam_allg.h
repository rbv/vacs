
// -------------------------------------------
// ---------------- fam_allg.h ---------------
// -------------------------------------------

/*tex
 
\file{fam_allg.h}
\path{inc/family}
\title{}
\classes{}
\makeheader

xet*/

#ifndef _fam_allg_h
#define _fam_allg_h

#include "family/family.h"


class AllGraphsCongruence : public Congruence
{

private:

   void compute( const class BPG& );

   bool operator==( const Congruence& c ) const;

};


class AllGraphsFamily : public Family
{

private:

   bool _congruenceAvailable() const { return true; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const
   { 
     return new AllGraphsCongruence; 
   }

   void _setTestset() { }

public:

   AllGraphsFamily();

};


#endif
