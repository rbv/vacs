
// ------------------------------------------
// --------------- probinfo.h ---------------
// ------------------------------------------

/*tex
 
\file{probinfo.h}
\path{inc/family}
\title{Header for listing family problems.}
\classes{ProbInfo}
\makeheader
 
xet*/


#ifndef _probinfo_h
#define _probinfo_h

#include "stdtypes.h"

/* 
 * Enum type for linking to problems.
 *
 * Each of these problems should have a corresponding family/fam_*.h 
 * header, and congruences in family/fam_*.c, and (maybe) related 
 * membership functions in src/bpg or src/graph directories.
 *
 */
class ProbInfo
{
public:

   enum VacsProblem 
   { 
      VertexCover,		// fam_vc, bpg/vertcover, graph/vclazy.
      FeedbackVertexSet,	// fam_fvs., bpg/fvs, graph/fvsbrute,
				// graph/fvsfixed.
      FeedbackEdgeSet,		// fam_fes
      MaximumPath, 		// fam_mp, bpg/maxpath.
      EmbeddableTest,           // fam_embedding
      PlanarTest, 		// fam_plane ...
      ApexOuterPlanarTest,      // fam_oplane ...
      Pathwidth, 		// 
      AllGraphs, 		// fam_allg
      NoProblem
   };

   enum GraphOrderType
   {
      InvalidOrder,
      EdgeDeletionOrder,
      SubgraphOrder,
      MinorOrder,
   };

   enum ParamType { Integer, String, Subpath, Boolean };
   enum ParamReq { Required, Optional };

   // This table must align with the parse table in probinfo.c
   //
   enum Param
   {
      ProblemName,
      ProblemNumber,
      Directory,
      Keyword,
      PathWidth,
      GraphOrder,
      LexicographicCanonicity,
      MaxVertices,
      MaxNodes,
      ApexVertices,
      MembershipFlag,
      EmbeddingFaces,
      EmbeddingGenus,
      EmbeddingCrosscaps,
      EmbeddingApices,
      DumpCongruence,
      CongruenceFlag,
      Connected,
      Biconnected,
      KeepTree,
      ConservativeGrow,
      WorkerQueue,
      WorkersDoMembership,
      PretestsComplete,
      ExtnEdgeWeight,
      ExtnRuns,
      ExtnTries,
      ExtnMaxLength,
      ExtnSkip,
      ExtnDeterministic,
      //SaveNMProofs,
      CongIsTight,
      TestCongTightness,
      TestCongPermill,
      UseTestset,
      TildeMinorSearch,
      MaxEqualityIsoLevel,
      LoadAllNonminimal,
      LimitToPrefix,
      MessagesContainPMP,
      UnivDistSearch,
      PretestExtns,
      NonMinOOFRelevant,
      CanonicTimeout,
      DefaultToMinimal,
      VerboseNodeLogging,
      ParamTableSize		// Internal use
   };

   // Used to associate problem keywords with init functions.
   //
   struct ProblemLinkage
   {
      const char* keyword;
      VacsProblem problem;
   };

   struct ParseTableEntry
   {
      Param num;
      const char* name;
      ParamReq req;
      ParamType type;
      const char* dflt;
   };

private:

   // Parameter storage
   //
   static bool  _flags[ ParamTableSize ];
   static void* _params[ ParamTableSize ];
   static GraphOrderType _graphOrder;

   // The linkage table is defined in problems.c
   //
   static const ProblemLinkage problemLinkageTable[];

   // The actual parsetable is defined in parse.c
   //
   static const ParseTableEntry parseTable[];

   // Misc parsing stuff
   //
   static int lineNo;
   static void paramError( const char* s );
   static void parseError( const char* s );
   static bool parseInputLine( char*, char*, char* );
   static void parseBoolean( int, const char* );
   static void parseInteger( int, const char* );
   static void parseString( int, const char* );
   static void parseSubpath( int, const char* );

public:

   // Read and parse parameter file.
   // Called by initialize().
   //
   static void readProbInfo( const char* filename );

   // Called by runinfo initialization
   //
   static void initializeFamily();

   static void parseGraphOrder();

   static int   getInt( Param param );
   static bool  getBool( Param param );
   static const char* getString( Param param );
   static const char* getPath( Param param );

   static GraphOrderType graphOrder() { return _graphOrder; }
};

#endif
