
// -------------------------------------------
// -------------- fam_embedding.h ----------------
// -------------------------------------------

/*tex
 
\file{fam_embedding.h}
\path{inc/family}
\title{Family header for surface embedding}
\classes{EmbeddingCongruence, EmbeddableFamily}
\makeheader

\begin{caveats}
This is a private header.
Only fam_embedding.c and problem.c should include this file.
\end{caveats}
 
xet*/

#ifndef _fam_embedding_h
#define _fam_embedding_h

#include "family/family.h"
#include "bpg/algorithm/embedding_b.h"

#include <boost/smart_ptr/shared_ptr.hpp>

using boost::shared_ptr;


class EmbeddingCongruence : public Congruence
{
   friend class EmbeddableFamily;

   shared_ptr<EmbedStateWrapper> _state;

   EmbeddingCongruence( EmbedStateWrapper *state )
      : _state( state )
   {}

   EmbedStateWrapper *get() { return _state.get(); }

public:
   EmbeddingCongruence()
   {}      

   void compute( const class BPG &G );

   bool operator==( const Congruence& c ) const;

   EmbedStateWrapper *operator->() { return _state.get(); }
};

class EmbeddableFamily : public Family
{
   static int genus;
   static int crosscaps;
   static int covering_faces;
   static int apices;

   NonminimalPretests pretests;

   static int alternativeMembership;
   static int actualMembership;
   static int totalMembershipTests;
   static int biggestState;
   static int costliestState;
   static long long totalCost;
   static long long totalSize;
   static int totalCompute;

   static int cacheLookups;
   static int cacheHits;
   static int cacheBackHits;

   bool _congruenceAvailable() const { return true; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   bool tryAlternativeMembership( const BPG& G, bool &result ) const;

   const NonminimalPretests& _nonminimalPretests() const { return pretests; }

   Congruence* _newCongruence() const;

   void _setTestset() {}

   void _printstats( ostream &os );

public:

   EmbeddableFamily();

   static EmbeddingCongruence _compute( const BPG &G );
};


#endif
