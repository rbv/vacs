
// -------------------------------------------
// ------------- fam_pw.h --------------------
// -------------------------------------------

/*tex
 
\file{fam_pw.h}
\path{inc/family}
\title{Family header for pathwidth family}
\classes{PathwidthFamily}
\makeheader

\begin{caveats}
This is a private header.
Only fam_pw.c and probinfo.c should include this file.
\end{caveats}
 
xet*/


#ifndef _fam_pw_h
#define _fam_pw_h

#include "family/family.h"
#include "bpg/algorithm/pathwidth_b.h"

//----------------------------------------------------------------------

class PathwidthCongruence : public Congruence
{
   PWState _state;

   void compute( const BPG &g );

   bool operator==( const Congruence& ) const;
};


class PathwidthFamily : public Family
{

   bool _congruenceAvailable() const { return false; }

   bool _testsetAvailable() const { return false; }

   bool _member( const BPG& g ) const;

   const NonminimalPretests& _nonminimalPretests() const;

   Congruence* _newCongruence() const
   {
      return new PathwidthCongruence; 
   }

//   int _applyTestset( const RBBPG&, PartialMinorProof&,
//	Array<MinorNumber> &distMinors ) const { return -1; }

   void _setTestset() {}


public:

   PathwidthFamily();
};

//----------------------------------------------------------------------

#endif
