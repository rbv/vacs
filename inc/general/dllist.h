/*
 * rewrite of GNU DDList (template version) //mjd 
 */

#ifndef _DLList_H_
#define _DLList_H_ 

#include <iostream>
#include <stdlib.h>
#include <libc.h>

#include "stdtypes.h"

#include "general/dlnode.h"

//#include "error.h"
inline void error(const char *m) { cerr << m << nl; }

template <class T>
//
class DLList
{

private:

  // Error handler called by public routines.  The error handler can be
  // changed here for debugging.
  //
  void  error( const char *msg) const { error(msg); }

protected:  

  // points to the first element of the list
  //
  DLListNode<T>*           h;

  // points to iterator position.
  //
  DLListNode<T>*           iter;

public:
                        DLList() { h = iter = 0; }

                        ~DLList();	// deletes ListNodes

  bool                  isEmpty() const { return h == 0; }
			// returns true if a contains no elements

  bool                  isNotEmpty() const { return h != 0; }
			// returns true if not empty

  int                   length() const;
			//  returns the number of elements in list.

  void		        prepend(const T &item);
			// places argument at the front of the list.

  void		        append(const T &item);
			// places argument at the end of the list.

  void                  join(DLList<T> &);
			// a.join(b) places all nodes from b to the end 
			// of a, simultaneously destroying b.

  void		        delAfter(DLListNode<T> *p);
                        // a.delAfter(p) deletes the element following
                        // p. If p is 0, the first item is deleted.

  T		        front() const
{
  if (h == 0) error("front: empty list");
  return h->getItem();
}
			// returns a reference to the first item in list

  void		        removeFront();
			// removes first item in list 

  T		        rear() const
{
  if (h == 0) error("rear: empty list");
  return (h->bk)->getItem();
}
			// returns the last item in the of the list 

  void		        removeRear();
			// removes the last item in the list 

  // start/stop and next are commonly used to step thru list
  //
  void		        start() const { ((DLList<T>*)this)->iter = h; }
  //
  void		        stop() const { ((DLList<T>*)this)->iter = 0; }
  //
  bool 		        next(T &item) const;
  //
  bool 		        next(T &item, DLListNode<T>* &p) const
  {
    p = iter; return next(item);
  }           
  //
  void 		        nextWrap(T &item) const;
			// same as above but wraps, hence always 'true'
  //
  void 		        nextWrap(T &item, DLListNode<T>* &p) const
  {
    p = iter; nextWrap(item);
  }           

  // allows looping through list in reverse order
  //
  void		        last() const 
{ 
  ((DLList<T>*)this)->iter = (h == 0)? 0 : h->bk;
}
  //
  bool		        prev(T &item) const;

  bool                  owns(const T &item) const;
  			// returns true if item is in list
			// also sets iterator to item if true.

#if 0
  // use next(), prev()
  void			get(T &item) const
			{
			 assert(iter);
                         item = iter->getItem();
                        }
#endif

  DLListNode<T>*	index() const { return iter; }
			// returns current iterator/pointer in list.
 
  DLListNode<T>*        insAfter(DLListNode<T> *p, const T &item);
                        // a.insAfter(p, item) inserts item after
                        // position p.

  DLListNode<T>*        insBefore(DLListNode<T> *p, const T &item);
                        // a.insBefore(p, item) inserts item before
                        // position p.

  DLListNode<T>*        delAndNext(DLListNode<T> *p, int dir = 1);
                        // deletes the item at the position defined by p
                        // and returns the next item on the list via dir

  void                  replace(DLListNode<T> *oldp, const T &newItem)
{
   oldp->setItem(newItem);
}
			// removes item at position oldp and replaces
			// with item newp.  Equivalent to combination of
			// insAfter and del.

  int                   ok() const;
			// checks if list is ok

};

template <class T> 
//
ostream& operator<<(ostream &o, const DLList<T> &l);

#include "inc/dllist.c"

#endif	_DLList_H_


