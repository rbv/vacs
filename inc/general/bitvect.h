
/***********************************************************************
 * Header for bit vector class.
 **********************************************************************/

/*tex
 
\file{bitvect.h}
\path{inc/general}
\title{Header for bit-vector classes}
\classes{BitVector, HugeBitVector}
\makeheader
 
xet*/

#ifndef _bitvect_h
#define _bitvect_h

#include <stdint.h>
#include "stdtypes.h"
#include "general/stream.h"
#include "array/hugearray.h"
#include "array/dyarray.h"

class BitVectorBase
{
public:

    // Use native word size (there's no proper type for that)
    typedef intptr_t bitword;

    static const int bits_per_word = sizeof(bitword)*8;
    //const bitword full_bitword = 0xFFFFFFFF /* ULONG_MAX */;

protected:

    // number of bits -- passed to constructor
    int _len;

    // number of words required for storage
    int _numWords;

    // returns word number of a bit
    int index( int pos ) const
    {
        return( pos / bits_per_word );
    }

    // returns bit position in word
    int offset( int pos ) const
    {
        return( pos % bits_per_word );
    }

public:

    int length() const { return _len; }	// Number of data bits.

    int size() const { return _len; }	// Number of data bits.

};


class BitVector : public BitVectorBase
{
        iodeclarations( BitVector );

protected:

        /*
         * Comparison operators.
         */
	friend bool operator==( const BitVector&, const BitVector& );

	friend bool operator<( const BitVector&, const BitVector& );

	friend bool operator>( const BitVector&, const BitVector& );

	friend BitVector operator-( const BitVector&, const BitVector& );

	friend BitVector operator&( const BitVector&, const BitVector& );

	friend BitVector operator|( const BitVector&, const BitVector& );

	// and(&) with last word to allow direct comparison
	//
	bitword _mask;

	// the array
	//
	bitword* bv;

public:
	/*
	 * Default constructor to be sized later.
	 */
	BitVector()        // zero lengthed -- must use resize()!
	 {
	     _len = _numWords = 0;
             _mask = 0;
             bv = 0;
	 }

        // Contents are garbage after a resize!
        void resize( int length );

	BitVector( int length );

        /*
         * Small bit vector from an integer.
         */
	BitVector( int bits, int length )
         {
	     _len = length;
             _numWords = 1;

	    if ( offset( _len ) == 0 )
	      _mask = bitword( -1 );
 	    else
	      _mask = bitword( -1 ) >> ( bits_per_word - offset( _len ) );

            bv = new bitword[ _numWords ];
            bv[0] = bits;
         }

	BitVector( const BitVector& );

	BitVector& operator=( const BitVector& );

        // like vector<bool>::swap
        void swap( BitVector& );

        bool get_bit( int pos ) const
	{
	   assert( pos >= 0 && pos < _len ); 

	   return ( ( bv[ index(pos) ] & ( bitword(1) << offset(pos) ) ) != 0 );
	}
        //
	bool getBit( int i ) const { return get_bit(i); }
        //
	bool isBit( int i ) const { return get_bit(i); }
	
	void clr_bit( int pos )
	{
	   assert( pos >= 0 && pos < _len ); 

	   bv[ index(pos) ] &= ( ~( bitword(1) << offset(pos) ) );
	}
        //
	void clearBit( int i ) { clr_bit(i); }

	void set_bit( int pos )
	{
	   assert( pos >= 0 && pos < _len ); 

	   bv[ index(pos) ] |= ( bitword(1) << offset(pos) );
	}
        //
	void setBit( int i ) { set_bit(i); }

	void set_all( void )
	{
	   for (int i = 0; i < _numWords; i++ ) bv[ i ] = bitword(-1);
	   bv[ _numWords - 1] &= _mask;
	}
        //
	void setAll( void ) { set_all(); }

	void clr_all( void )
	{
	   for (int i = 0; i < _numWords; i++ ) bv[ i ] = 0;
	}
        //
	void clearAll( void ) { clr_all(); }

        unsigned long long hash();

	void complement( )
	{
	   for (int i = 0; i < _numWords; i++ ) bv[ i ] = ~bv[ i ];
	   bv[ _numWords - 1] &= _mask;
	}

	~BitVector() { delete [] bv; }	// Destructor.


        //inline bool operator!();		// All bits zero?
		bool operator!()
		 {
		  for (int i=0; i<_numWords; i++) if (bv[i]) return false;
		  return true;
		 }

        //inline bool full();			// All bits one?
		bool full()
		 {
		  for (int i=0; i<_numWords-1; i++) if (~bv[i]) return false;
		  if (bv[_numWords-1]!=_mask) return false;

		  return true;
		 }

        /* 
         * Compute and/or of two arguments and put result in this BitVector (must all be same length)
         */
        /*inline*/ void andOf( const BitVector&, const BitVector&);
        //
        /*inline*/ void orOf( const BitVector&, const BitVector&);


	// Bit vector intersection (and)
	//
	BitVector& operator&=( const BitVector& );

	// Bit vector Union (or)
	//
	BitVector& operator|=( const BitVector& );

	// Bit vector complement 
	//
	BitVector operator~();

}; 

	// tests equality of whole vectors
	//
	bool operator==( const BitVector&, const BitVector& );

	// tests inclusion of whole vectors
	//
	bool operator<( const BitVector&, const BitVector& );

	// tests inclusion of whole vectors
	//
	bool operator>( const BitVector& B1, const BitVector& B2 );

	// Bit vector difference.
	//
	BitVector operator-( const BitVector&, const BitVector& );

	// Bit vector intersection (and)
	//
	BitVector operator&( const BitVector&, const BitVector& );

	// Bit vector Union (or)
	//
	BitVector operator|( const BitVector&, const BitVector& );

//-------------------------------------------------------------------

#if 0
void BitVector::and( const BitVector& B1, const BitVector& B2 )
 {
  assert( length() == B1.length() && length() == B2.length() );

  for (int i=0; i<_numWords; i++) bv[i] = B1.bv[i] & B2.bv[i];
 }

void BitVector::or( const BitVector& B1, const BitVector& B2 )
 {
  assert( length() == B1.length() && length() == B2.length() );

  for (int i=0; i<_numWords; i++) bv[i] = B1.bv[i] | B2.bv[i];
 }
#endif

//-------------------------------------------------------------------

// HugeBitVector is intended for efficient extending and searching, not much else.
// Semantics are different: zero filled by default, extendable, not serialisable,
// methods for searching for set bits.

class HugeBitVector : public BitVectorBase
{
    HugeArray _blocks;

    static const int sumsize = 8096;

    // For each chunk of 'sumsize' bits, store number of set bits
    DyArray<short> _counts;

    bitword &getBitword( int bitno )
    {
        int ind = index( bitno );
        //return _blocks[ind / blocksize][ind % blocksize];
        return *(bitword *)_blocks.offset( ind );
    }

public:
    HugeBitVector( int size );
    ~HugeBitVector() {}

    // resize. Only extending allowed
    void extendTo( int size );

    // resize, destroying contents
    void recreate( int size );

    bool get_bit( int i )
    {
        assert( i < _len );
        return getBitword( i ) & (bitword( 1 ) << offset( i ));
    }

    void set_bit( int i, bool val = true )
    {
        assert( i < _len );
        bitword mask = bitword( 1 ) << offset( i );
        bitword &word = getBitword( i );
        if ( val )
        {
            if ( ! (word & mask) )
            {
                _counts[i / sumsize]++;
                word |= mask;
            }
        }
        else
        {
            if ( word & mask )
            {
                _counts[i / sumsize]--;
                word &= ~mask;
            }
        }
    }

    void clearAll( void );

    // Scan for set bits. To use, initialise bitno to -1, and call until result is false
    bool next_set_bit( int &bitno );

};

#endif
