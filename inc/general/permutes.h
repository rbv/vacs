
/*
 * Header file for enumeration of permutations.
 */

/*tex

\file{permutes.h}
\path{inc/general}
\title{Header for permutation class}
\classes{PermEnum}
\makeheader
 
\begin{desc}
Using the basic ideas of Fisher and Krause, 1812, this class
enumerates permutations in reverse lexicographic order.

This current implementation works satisfactory for $n \leq 8$ on a
repeated basis.  (Dinneen's opinion based on crude C++ benchmarks).
Using the Johnson-Trotter or another newer approach, I suspect that
$n=9$ (and possible $n=10$) would be feasible.
\end{desc}

\begin{usage}

PermEnum Sn(n);		// derived from Array $<$ uchar $>$

do {
// access Sn as a read-only array.
} while (Sn.next());

\end{usage}

xet*/

#ifndef __permutes
#define __permutes

#include "stdtypes.h"
#include "graph/stdgraph.h"
#include "array/array.h"

typedef int permNum;
typedef ulong permHash;
typedef Array<permNum> PermArray;

class PermEnum : public PermArray 
{
private:

#ifdef SN_RANKING

   permHash _hash;	// Use interger's linear order. 
   permHash _maxHash; 

   /*
    * Internal routines to convert between hash value and array.
    */
   rank();
   unrank();

#endif

   /*
    * Private access to array indexer. 
    */
   permNum& _A_(int i) { return this->PermArray::operator[](i); }

public:

   /*
    * Constructors.  (Not guaranteed to start at identity element.)
    */
   PermEnum(int n);

   /*
    * Copy constructor.
    */
   PermEnum( const PermEnum& P );

#ifndef SN_RANKING
   /*
    * If a lexicographic enumerator is installed then...
    */
   PermEnum(PermArray P) : PermArray(P) {} 	// Starting permuation.

   void setPerm(const PermArray &P) { (void) PermArray::operator=(P); }
#endif

   /*
    * Constuctor for subgroup specification.
    */
   //PermEnum(int n, ???? ) : PermArray(n); 
   
#ifdef SN_RANKING

   /*
    * Find next/prev permutation in linear order.
    */
   bool next()		// Returns 'false' if already last permutation.
    {
     if ( _hash == _maxHash ) return false;
     else
      {
       hash++;
       unrank();
       return true;
      }
    }

   bool prev();		// Returns 'false' if already first permutation.
    {
     if ( _hash == 0 ) return false;
     else
      {
       hash--;
       unrank();
       return true;
      }
    }

   /*
    * Take advatage of some efficiency features.
    */
   bool operator==( const PermEnum &P) 
    { 
      assert(_size == P._size);
      return (_hash == P._hash); 
    }
   //
   bool operator!=( const PermEnum &P) 
    { 
      assert(_size == P._size);
      return (_hash != P._hash); 
    }

#else

   /*
    * Find next permutation in default linear ordering.
    */
   bool next();		// Returns 'false' if already last permutation.

   bool next(int posK); // Next permutation by decrementing position k.

   permHash rank();	// A unique number for the permutation.
   
#endif

   /*
    * Make some of the Array functions read-only (nonaccessable).
    */
   permNum operator[](int i) const { return get(i); }
   //
   void put(int, permNum) { aassert(0); }  // YES, I need to think more.
   void resize(int) { aassert(0); }
   void swap(int, int) { aassert(0); }
   void rotate(int, int) { aassert(0); }
   // etc...

   // -------------- I/O ----------------

   friend inline ostream& operator<<( ostream& o, PermEnum p )
      { return o << CAST( p, PermArray ); }

 };

#endif
