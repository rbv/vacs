
/***********************************************************************
 * Header for dynamic bit vector class.
 **********************************************************************/

/*tex
 
\file{dybitvect.h}
\path{inc/general}
\title{Header for dynamic bit-vector class}
\classes{DyBitVector}
\makeheader

\begin{desc}
This class provide support for dynamic bit vectors (via explicit
length control member functions) of objects.
\end{desc}

 
xet*/

#ifndef _dybitvect_h
#define _dybitvect_h

#include "stdtypes.h"
#include "general/bitvect.h"

#define _Default_Space 4 

class DyBitVector : public BitVector
 {
private:
        int _space;           // How many words currently allocated.

public:
	/*
	 * Default constructor to be sized later.
	 */
	DyBitVector()
	 {
	     _len = _numWords = 0;
//             _mask = 0;
             
             bv = new bitword[_Default_Space];
             _space = _Default_Space;
	 }
        //
        void resize( int newBitLen );

	DyBitVector( int length ) : BitVector(length)
         {
          _space = _numWords;
         }

        // copy constructors
        //
	DyBitVector( const BitVector &b) : BitVector(b)
         {
          _space = _numWords;
         }
        //
	DyBitVector( const DyBitVector &d ) : BitVector( (BitVector) d ) 
         {
          _space = _numWords;
         }

        /*
         * Assignment:
         * does not reallocate if space(rhs) >= size(lhs)
         */
        DyBitVector& operator=( const DyBitVector &d)
         {
          BitVector::operator=( (BitVector) d );

          _space = _numWords;

          return *this;
         }

        /*
         * Destructor. (~BitVector() should be sufficient.)
         */
	~DyBitVector() {} 

}; 

        /*
         * Stream output.
         */
inline ostream& operator<< ( ostream& os, const DyBitVector &D )
 {
  return os << (BitVector) D;
 }
  


#endif
