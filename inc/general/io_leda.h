

// IO declaration for Leda container classes.

#ifndef _leda_io_h
#define _leda_io_h

#include "stdtypes.h"
//include "LEDA/dictionary.h"

class bistream;
class bostream;

// --- Dictionaries ---
 
template< class T1, class T2 >
bistream& operator>>( bistream& s, dictionary< T1, T2 >& D );

template< class T1, class T2 >
bostream& operator<<( bostream& s, const dictionary< T1, T2 >& D );

template< class T1, class T2 >
ostream& operator<<( ostream& s, const dictionary< T1, T2 >& D );

#endif
