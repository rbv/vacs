
/*
 * Header file for set partitions (Bell number)
 */

/*tex
 
\file{partitions.h}
\path{inc/general}
\title{Header for set partitions}
\classes{Partition}
\makeheader
 
xet*/

#ifndef partition_h
#define partition_h

#include <iostream>

#include "stdtypes.h"
#include "array/dyarray.h"
//
typedef ushort partType;

/*
 * Builds partitions of the set [0,1,...,n-1]
 */
class Partition
 {
   int _n;	// Universe set size.
   
   Array<partType> Value, Maximum;

public:

   Partition(int n) : Value(n), Maximum(n) 
    { 
     assert(n>0); 
     _n = n;

     getFirstPart();	// set up generator to first partition.
    }

   ~Partition() { }

   int size() const { return _n; }

   // Sets up first partition (only call to reset generator).
   //
   void getFirstPart() { Value.fill(0); Maximum.fill(1); }

   // Builds next partition.
   //
   bool getNextPart();

   /*
    * Which partition is number 0 <= i < n in?
    */
   partType operator[](int i) const { return Value.get(i); } 

   /*
    * Get partitions in an array of lists format 
    * (input contents are ignored)
    */
   void getPartition(DyArray< DyArray< partType > > &P) const;

 };

inline ostream& operator<<( ostream &o, const Partition &P )
{

  DyArray< DyArray< partType > > Plist;
  //
  P.getPartition(Plist); 

#if 0
  return o << Plist;
#else

  o << "{ ";

  for (int i=0; i<Plist.size(); i++)
   {
    for (int j=0; j<Plist[i].size(); j++)
     {
      o << Plist[i][j] << ' ';
     }

    if (i<Plist.size()-1) o << "# ";
   }

  return o << "}";
   
#endif
}

#endif
