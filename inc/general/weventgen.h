 
// -------------------------------------------
// --------------- weventgen.h ---------------
// -------------------------------------------
 
/*tex
 
\file{weventgen.h}
\path{inc/general}
\title{Declaration of weighted event generator class}
\classes{WeightedEventGenerator}
\makeheader
 
xet*/

#include "general/random.h"
#include "array/array.h"

typedef Array< int > WeightArray;

class WeightedEventGenerator
{

private:

   WeightArray _w;

   RandomInteger _r;

   int _totalWeight;

   // Totals the weights and sets the modulus of the generator
   //
   void recompute();

public:

   // Create, and initialize with the passed weight values
   //
   WeightedEventGenerator( const Array< int >& );

   // Create with size, and initialize all entrys to 100 (all equally likely)
   //
   WeightedEventGenerator( int size );

   // Return the next event
   //
   int operator()();
   //
   int event()
      { return operator()(); }

   // return current weight of an event
   //
   int weight( int eventNum )
      { return _w[ eventNum ]; }

   // set the weight for an event
   //
   void weight( int eventNum, int weight )
   { _w[ eventNum ] = weight; recompute(); }

};
