
// ------------------------------------------
// ----------------- strutil.h --------------
// ------------------------------------------

/*tex

\file{strutil.h}
\path{inc/general}
\title{Template for string-related functions}
\classes{}
\makeheader

\begin{usage}
This file should only be included by str.h.
To use these functions, include str.h.
\end{usage}

xet*/

#ifndef _strutil_h
#define _strutil_h

#include <sstream>
#include "general/str.h"

template< class T >
Str asString( const T& a )
{
	std::stringstream s;
	s << a;
	return Str(s.str().c_str());
}

#endif
