
// ------------------------------------------
// ----------------- queue.c ----------------
// ------------------------------------------

/*tex
 
\file{queue.h}
\path{src/template/general}
\title{Ostream implementation for queue classes }
\classes{Queue}
\makeheader
 
xet*/

#include "general/queue.h"

#ifdef __xlC__
class _InternalNode;
inline bool operator==(_InternalNode&, const _InternalNode&) 
{ aassert(false); return false; }
#endif

//---------------------------------------------------------------------

template<class T>
bool ArrayQueue<T>::exists( const T &a ) const
{
 if (isEmpty()) return false;

 const DyArray<T> &dyA = queue;

 if (head <= tail)
 {
   for (int i=head; i<=tail; i++)  
   {
     if ( dyA[i] == a ) return true;
   }
 }
 else
 {
   for (int i=head; i< space; i++)  
   {
     if ( dyA[i] == a ) return true;
   }
   //
   for (int i=0; i<=tail; i++)  
   {
     if ( dyA[i] == a ) return true;
   }
 }

 return false;
}

template<class T>
bool ArrayQueue<T>::remove( const T &a )
{
 if (isEmpty()) return false;

 DyArray<T> &dyA = queue;

 if ( head == tail )
 if ( dyA[head] == a )
 {
   empty();
   return true;
 }
 else return false;

 if (head < tail)
 {
   for (int i=head; i<=tail; i++)  
   {
     if ( dyA[i] == a ) 
     {
        dyA.shiftLeft( i, 1 );
        tail--;
        return true;
     }
   }
 }
 else
 {
   //aassert(false);  // have to run away...

   int i; for (i=head; i< space; i++)   // just move tail up for now
   {
     if ( dyA[i] == a ) 
     {
      dyA[i]=dyA[tail];
      tail--;
      if (tail<0) tail = space-1;
      return true;
     }
   }
   //
   for (i=0; i<=tail; i++)  
   {
     if ( dyA[i] == a ) 
     {
      dyA[i]=dyA[tail];
      tail--;
      if (tail<0) tail = space-1;
      return true;
     }
   }
 }

 return false;
}
template<class T>
ostream& operator<<(ostream &out, const Queue<T> &que)
{
 if (que.isEmpty()) return out << "<- <-";

 const DyArray<T> &dyA = CAST(que, DyArray<T>);
 char *pHead = (char*) &que;
 pHead += sizeof(DyArray<T>);
 int head = *((int*) pHead);
 pHead += sizeof(int);
 int tail = *((int*) pHead);
 pHead += sizeof(int);
 int size = *((int*) pHead);
 
 out << "<- ";
 //
 if (head <= tail)
 {
   for (int i=head; i<=tail; i++)  
   {
     out << dyA[i] << ' ';
   }
 }
 else
 {
   int i; for (i=head; i<size; i++)  
   {
     out << dyA[i] << ' ';
   }
   //
   for (i=0; i<=tail; i++)  
   {
     out << dyA[i] << ' ';
   }
 }
 //
 return out << "<-";
}
