
/*tex

\file{gtimers.h}
\path{inc/general}
\title{Header for globally visible array of timers}
\classes{Gtimers}
\makeheader

xet*/

#ifndef _gtimers_h
#define _gtimers_h

#include "general/dtime.h"
#include "general/system.h"
#include "general/stream.h"

class Gtimers {
    static const int NTIMES = 40;

    static double cpuTimes[NTIMES];
    static double wallTimes[NTIMES];
    static Dtime dtimers[NTIMES];
    static double stimers[NTIMES];
    static const char *names[NTIMES];
    static long long overhead;

    Gtimers() {}

public:
    // It's not necessary to call stop() after every start().
    // Only time betwen pairs are counted
    static void start( int i, const char *name = NULL );
    static void stop( int i );

    static void addTime( int i, float cputime, float walltime )
    {
        assert( i < NTIMES );
        cpuTimes[i] += cputime;
        wallTimes[i] += walltime;
    }

    static void print( ostream &os, int num_to_total = NTIMES );

};

#endif
