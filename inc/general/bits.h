
/*
 * A generic function to count bits of any type.
 */

/*tex

\file{queue.h}
\path{inc/general}
\title{Header (and implementation) for bits function.}
\classes{}
\makeheader

xet*/

#ifndef _BITS_H
#define _BITS_H

// FIXME: This may be too obfuscated for the compiler to automatically replace with POPCNT builtin

template<class Type>
inline int bits( Type a )
 {
  int cnt=0;
  char *cptr = (char *) &a;

  for (int i=0; i<sizeof(Type); cptr++, i++)
   for (int j=0; j<8; j++)
    if (*cptr & (1<<j)) cnt++;

  return cnt;
 }

#endif
