
// -------------------------------------------
// --------------- database_f.h --------------
// -------------------------------------------
 
/*tex
 
\file{database_f.h}
\path{inc/general}
\title{Header for fixed record size database class}
\classes{FRSDatabase}
\makeheader
 
xet*/

#ifndef _database_f_h
#define _database_f_h

#include "database_b.h"

// DatabaseBase is not used for storing the number of items, apparently.
class FRSDatabase : public DatabaseBase
{
private:

   void readControlFile();
   void writeControlFile();
   void positionDataFile( int key );

   // Control file format
   //
   struct ControlInfo
   {
      int format;
      int numKeys;
      int recordSize;
   };

   ControlInfo ctl;
   int dataFileSeek;  // The record which df is currently at

   // for creation
   //
   FRSDatabase();

   // sync util
   //
   void loadCache();

public:

   // Open a database.
   //
   FRSDatabase( const char* filename, AccessMode accessMode );

   // Close the database
   //
   ~FRSDatabase();

   // Create the database.
   //
   static void dbCreate( const char* filename, int recordSize );

   static bool dbExists( const char* filename )
      { return DatabaseBase::dbExists( filename, false ); }
 
   virtual void clearDatabase();

   // Check the integrity of the database.
   // Fix any problems if rebuild == true.
   // Check op! or lastError to see if db is ok.
   //
   //void checkIntegrity( bool rebuild = false );

   // Check if the record exists.
   //
   bool exists( int key );

   // Get a record from the database into the buffer.
   //
   void get( int key, char* buffer, bool bypassCache = 0 );

   // Return a pointer to a cached record.  Invalid if not caching.
   //
   char* getCache( int key );

   // Returns a record as a bistream.
   // Caller must delete the bistream when done.
   // Error if the default bistream buffer is too small.
   //
   class bistream* get( int key );

   // Put a record in the database, overwriting a record of the same key.
   //
   void put( int key, char* buffer );

   // Put a record in the database, overwriting a record of the same key.
   // The record is in the bostream, and o.pcount() must be == recordSize.
   //
   void put( int key, class bostream& );

   // Shorten the database to this many keys
   void truncate( int size );

   // Return the last used key in the DB
   //
   //int lastUsedKey();
   //
   int numberOfKeys();

   // Reload cache from disk (cache read-only access only)
   //
   void sync();

   // Reload a cahe item from disk (cache read-only access only)
   //
   //void sync( int key );

   void flush();
};

#endif
