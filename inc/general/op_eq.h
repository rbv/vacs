
// ------------------------------------------
// ----------------- op==.h -----------------
// ------------------------------------------

/*tex

\file{op==.h}
\path{inc/general}
\title{Template for generic operator== plug}
\classes{}
\makeheader

xet*/

#ifndef OP_EQ_H
#define OP_EQ_H

#include "stdtypes.h"

#ifdef __xlC__
#define opEqual(T) inline bool operator==(T&, T&) \
{ aassert(false); return false; } \
inline bool operator==(const T&, const T&) { aassert(false); return false; } 
#else
/*
#ifndef DOLEDA
template< class T >
inline bool operator==(const T&, const T&)
{ aassert(false); return false; }
#endif
*/
#endif

#endif
