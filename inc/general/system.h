
/*
 * System resource class (times and resource info).
 */

/*tex
 
\file{system.h}
\path{inc/general}
\title{Header for system interface class}
\classes{System,Timer}
\makeheader
 
xet*/



#ifndef _system_h
#define _system_h

#include <iostream>
#include <time.h>
#include <string.h>
#include <sys/time.h>
//#include <sys/resource.h>

#include "stdtypes.h"

#define _cbufsize 40

class System 
{
private:
   /*
    * Static so that latest wall time/date is kept for all instances.
    */
   static struct timeval _tv; 	// members: tv_sec and tv_usec
   static struct timezone _tz;

   static struct tm *_tm;	// members: tm_sec, tm_min, tm_hour,
				// tm_mday, tm_mon, tm_year, tm_wday, 
				// tm_yday, tm_isdst, *tm_zone, tm_gmtoff

   char _cbuf[_cbufsize];	// Reusable buffer containing system info.
				// User must copy returned strings!

public:
   System() { update();  _cbuf[0]=0; }

   void update() 
    { 
     gettimeofday(&_tv, &_tz); 
#ifdef __xlC__
     time_t tmp = _tv.tv_sec;
     _tm = localtime(&tmp);
#else
     _tm = localtime(&_tv.tv_sec);
#endif
    }    
 
   /*
    * Returns the number of seconds since Jan 1, 1970, 00:00.
    */
   operator int() { return _tv.tv_sec; }

   static double seconds()
   {
      gettimeofday(&_tv, &_tz); 
      return _tv.tv_sec + 0.000001 * _tv.tv_usec;
   }

   /*
    * Return pointers to time/date character strings.
    */
   char *getTime() 
    { 
      strftime( _cbuf, _cbufsize, " %r ", _tm ); // std C
      return _cbuf;
    }    
   //
   char *getExactTime() { update(); return getTime(); }
   //
   char *getDate()
    { 
      strftime( _cbuf, _cbufsize, " %h %d %Y ", _tm );  // std C
      return _cbuf;
    }    
   //
   // Kill silly \n than system puts in
   //
   char *getDateAndTime() { update(); char* s=asctime( _tm ); 
                            s[strlen(s)-1]=0; return s; }
 
   /*
    * Dump time/date strings to ostream.
    */
   void time(ostream &o) { o << getTime(); }
   void exactTime(ostream &o) { o << getExactTime(); }
   void dateAndTime(ostream &o) { o << getDateAndTime(); }
   void date(ostream &o) { o << getDate(); }

   static int timeInSeconds()
      { System t; return int(t); }

   static char* secondsToAscii( int seconds )
   { System t; t._tv.tv_sec = seconds; t._tv.tv_usec=0;
#ifdef __xlC__
     time_t tmp = _tv.tv_sec;
     _tm = localtime(&tmp);
#else
     _tm = localtime(&_tv.tv_sec);
#endif
     char* s=asctime(_tm); s[strlen(s)-5]=0; return s;
   }
};

#undef _cbufsize

#if 0
main() // test
 {
   System wall;

   cerr << "1" << (char*) wall.getTime() << "1\n";
   cerr << "2" << wall.getExactTime() << "2\n";
   cerr << "3" << (char*) wall.getDateAndTime() << "3\n";
   cerr << "4" << (char*) wall.getDate() << "4\n";
}
#endif

// Global stream functions (manipulators)

inline ostream& date( ostream& o )
{
   System wall;
   wall.dateAndTime(o);
   return o;
}



class Timer
{
   int _endTime;

public:

   Timer() {}

   Timer( int minutes, int seconds = 0 )
      { set( minutes, seconds); }

   void set( int minutes, int seconds = 0 )
      { _endTime = System::timeInSeconds() + minutes*60 + seconds; }

   operator const void*() const
      { if ( System::timeInSeconds() < _endTime ) return 0; else return this; }

   operator void*()
      { if ( System::timeInSeconds() < _endTime ) return 0; else return this; }

   bool operator!() const
      { return System::timeInSeconds() < _endTime; }


};

#endif

