
// -------------------------------------------
// --------------- database_v.h --------------
// -------------------------------------------
 
/*tex
 
\file{database_v.h}
\path{inc/general}
\title{Header for variable record size database class}
\classes{VRSDatabase}
\makeheader
 
xet*/

#ifndef _database_v_h
#define _database_v_h

#include "general/database_b.h"

class VRSDatabase : public DatabaseBase
{

private:

   void readControlFile();
   void writeControlFile();
   void positionKeyFile( int key, int whence = SEEK_SET );
   off64_t positionDataFile( off64_t pos, int whence = SEEK_SET );

   // Internal key format
   //
   struct PACKED Key
   {
      off64_t pos;    // position of record
      ushort len;  // length of record
   };

   // Control file format
   //
   struct ControlInfo
   {
      int format;
      int numKeys;
   };

   ControlInfo ctl;

   // Current position of df
   off64_t dataFileSeek;

   // for creation
   //
   VRSDatabase();
 
public:

   // Open a database.
   //
   VRSDatabase( const char* filename, AccessMode accessMode );

   // Close the database
   //
   ~VRSDatabase();

   // Create the database.
   //
   static void dbCreate( const char* filename );

   static bool dbExists( const char* filename )
      { return DatabaseBase::dbExists( filename, true ); }

   virtual void clearDatabase();

   // Check the integrity of the database.
   // Fix any problems if rebuild == true.
   // Check op! or lastError to see if db is ok.
   // Unimplemented.
   //
   //void checkIntegrity( bool rebuild = false );

   // Check if the record exists.
   // Unimplemented.
   //
   //bool exists( int key );

   // Get a record from the database into the len byte array buffer.
   // Error if the buffer is too small.
   // On success, returns the size of the record.
   //
   int get( int key, char* buffer, int len );

   // Returns a record as a bistream.
   // Caller must delete the bistream when done.
   // Error if the default bistream buffer is too small.
   //
   class bistream* get( int key );

   // Put a record in the database, overwriting a record of the same key.
   // The record is len bytes in buffer.
   //
   void put( int key, char* buffer, int len );

   // Put a record in the database, overwriting a record of the same key.
   // The record is in the bostream, and o.pcount() bytes are written.
   //
   void put( int key, class bostream& o );

   // Shorten the database to this many keys
   void truncate( int size );

   // Delete the record.
   // Space is not reclamed until compress().
   //
   void del( int key );

   // Return the last used key in the database.
   //
   int lastUsedKey();

   // reload cache (read only)
   //
   void sync();

   void flush();

   // Compress the records in the database.
   // Unimplemented.
   //
   //void compress();

};

#endif
