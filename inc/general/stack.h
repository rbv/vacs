

// ------------------------------------------
// ----------------- stack.h ----------------
// ------------------------------------------

/*tex
 
\file{stack.h}
\path{inc/general}
\title{Header (and implementation) for stack class }
\classes{Stack}
\makeheader
 
xet*/

#ifndef __stack_h
#define __stack_h

#include "stdtypes.h"
#include "array/dyarray.h"


//-------------------- Definition/Implementations ------------------

#define STACK_GROW 16
//
template<class T>
//
class Stack
 {
   DyArray<T> stack;

   int topIndex;
   int space;

   bool emptyFlag;
 
public:
   Stack( int guessSize=32 ) : stack( guessSize ? guessSize : 1 )
    { 
      space = stack.size();

      emptyFlag = true;
    }	

   bool isEmpty() { return emptyFlag; }

   void push(T s)			// Add item to stack.
     {
        if (emptyFlag)
         {
           topIndex = 0;
           emptyFlag = false;
         }
        else
         {
           /*
            * See if we have wrapped around and need to enlarge array.
            */
           if ( ++topIndex == space )
            {
              stack.extend( STACK_GROW );

              space += STACK_GROW;
            }

         } // else nonempty

        stack[topIndex]=s;
    }

   bool pop(T& s)			// Remove next item from stack.
    {
	if (emptyFlag) return false;
        else
         {
           s = stack[topIndex];

           if ( topIndex-- == 0 )  emptyFlag=true;

           return true;
	 }
    }

   T top()				// Peek at top of stack.
    {
     assert(!emptyFlag);
     
     return stack[topIndex];
    }

 };

//------------------------------------------------------------------

#endif
