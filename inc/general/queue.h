
// ------------------------------------------
// ----------------- queue.h ----------------
// ------------------------------------------

/*tex
 
\file{queue.h}
\path{inc/general}
\title{Header (and implementation) for queue classes }
\classes{Queue}
\makeheader
 
xet*/

#ifndef __queue_h
#define __queue_h

#include "stdtypes.h"
#include "array/dyarray.h"


//-------------------- Definition/Implementations ------------------

// ?# implementation #1

template<class T>
//
struct QueueList
 {
     T q_item;
     QueueList  *next;
     QueueList  *prev;
 }; 
 

template<class T>
//
class PtrQueue
 {
   QueueList<T> *head, *tail;
 
public:
   PtrQueue() { head = tail = NULL; }	// Construct empty queue.

   void enque(T s)			// Add item to queue.
     {
	if (head == NULL)
          {
            head = tail = new QueueList<T>;
            tail -> prev = NULL;
            head -> next = NULL;
          }
	else
          {
            tail -> next = new QueueList<T>;
            tail -> next -> prev = tail;
            tail = tail -> next;
	    tail -> next = NULL;
          }
        tail -> q_item = s;
     }

   bool deque(T& s)			// Remove next item from queue.
   {
	if (head == NULL) return false;
        else
         {
          s =  head -> q_item;
	  if (head -> next == NULL)
	  {
	     delete head; head = tail = NULL;
	  }
	  else
	  {
             head = head -> next; delete head -> prev;
	  }
          return true;
         }
     }

   void empty()			// Delete all items from queue.
    {
      while (head != NULL)
       {
	  if (head -> next == NULL)
	  {
	     delete head; head = tail = NULL;
	  }
	  else
	  {
             head = head -> next; delete head -> prev;
	  }
       }     
    }

   bool isEmpty() const { return (head == NULL); }
 };

// ?# implementation #2

#define QUEUE_GROW 16
//
template<class T>
//
class ArrayQueue
 {
   DyArray<T> queue;

   int head, tail;
   int space;

   bool emptyFlag;
 
public:

   ArrayQueue( int guessSize=32 ) : queue( guessSize ? guessSize : 1 )
    { 
      space = queue.size();

      emptyFlag = true;
    }	

   void enque(T s)			// Add item to queue.
     {
        if (emptyFlag)
         {
           head = tail = 0;
           emptyFlag = false;
         }
        else
         {
           tail = (tail+1) % space;

           /*
            * See if we have wrapped around and need to enlarge array.
            */
           if ( head == tail )
            {
	      queue.rotateLeft( head );
              queue.extend( QUEUE_GROW );

              head = 0;
              tail = space;

              space += QUEUE_GROW;
            }

         } // else nonempty

        queue[tail]=s;
    }

   bool deque(T& s)			// Remove next item from queue.
    {
	if (emptyFlag) return false;
        else
         {
           s = queue[head];

           if ( head == tail )  emptyFlag=true;

           head = (head+1) % space;

           return true;
	 }
    }

   void empty() { emptyFlag=true; }

   bool isEmpty() const { return emptyFlag; }

   int size() const 
   {
    if (emptyFlag) return 0;
    else return (head <= tail ? tail - head : space - head + tail) + 1;
   }

   bool exists( const T &a ) const;  // check for presence

   bool remove( const T &a );  // remove first match
 };

//------------------------------------------------------------------

/*
 * Pick default implementation.
 */
template<class T>
//
//class Queue : public PtrQueue<T> {};
//
  class Queue : public ArrayQueue<T> 
  {
   public:
    Queue() : ArrayQueue<T>() {}
    Queue(int sz) : ArrayQueue<T>(sz) {}
  };

template<class T>
ostream& operator<<(ostream &out, const Queue<T> &que);

#include "general/queue.c"

#endif
