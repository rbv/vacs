/*
 * rewrite of GNU DDList (template version) //mjd 
 */

/*tex

\file{dlnode.h}
\path{inc/general}
\title{Header for double linked lists nodes.}
\classes{ DLListNode<TNode> }
\makeheader

xet*/

#ifndef _DLListNode_H_
#define _DLListNode_H_ 

#include <iostream>

template <class T> class DLList;
template <class T> class SQList;

template <class TNode>
//
class DLListNode
{
  friend class DLList<TNode>;
  friend class SQList<TNode>;

  private:
     TNode			_item;

  protected:
     DLListNode<TNode>*         bk;
     DLListNode<TNode>*         fd;

  public:

     // mark node as not belonging to any list
     //
     inline DLListNode() { bk = fd = 0; }

     inline DLListNode(const TNode &item) { _item=item; bk = fd = 0; }

     inline DLListNode(const DLListNode<TNode> &n) 
                                    { _item=n.getItem(); bk = fd = 0; }

     inline ~DLListNode() {};

     TNode getItem() const { return _item; } 

     void setItem(const TNode &m) { _item = m; }

     TNode operator=(const TNode &m) { _item = m; return _item; }

     TNode operator=(const DLListNode<TNode> &n) 
                               { _item = n.getItem(); return _item; }

};   

template <class TNode>
//
inline ostream& operator<<(ostream &o, const DLListNode<TNode> &i )
 { 
   return o << i.getItem(); 
 }

#endif  // _DLListNode_H_
