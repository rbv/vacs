

// -----------------------------------------
// ----------------- str.h -----------------
// -----------------------------------------

/*tex
 
\file{str.h}
\path{inc/general}
\title{Header for string class}
\classes{Str}
\makeheader
 
xet*/


#ifndef _str_h
#define _str_h

#include <string.h>
#include "general/stream.h"

class Str
{

private:

   struct srep
   {
      char* s;			// ptr to data
      int n;			// reference count
      srep() { n = 1; }		// ctor
   };

   srep* p;

   void unhook( int newAllocSize = 0 );

public:

   Str();			// Str x

   Str( const char* );		// Str x = "abc"

   Str( const char );		// Str x = "a"

   Str( const Str& );		// Str x = Str ...

   Str( int );			// Str x( 23 ) // String "23"

   Str& operator=( const Str& );

   Str& operator=( const char* );

   ~Str();

   static Str withLength( int length ); // allocs length+1

   // append arg to this, and return a reference to this
   //
   Str& append( const Str& );

   Str& append( const char* );

   Str& append( const char );

   int words( char sep = ' ' ) const;

   Str word( int i, char sep = ' ' ) const;

   Str& lowerCase();

   // Return the position in the string
   // of the target character.
   // Return -1 of not found.
   //
   int index( char target ) const;

   Str& padTo( int length, char pad = ' ' );

   Str& padLeft( int length, char pad = ' ' );


   // Count the number of occurences of char in this.
   //
   int countChar( char c ) const;

   // Parse as integer
   // Return true on success, false on failure.
   //
   bool asInteger( int& ) const;

   // return ptr to the char array
   //
   operator char*() const
      { return p->s; }

   // return lvalue of a char
   //
   char& operator[]( int i )
      { assert( i >= 0 && i <= length() ); // allow access to terminator
        return p->s[i];
      }

   int length() const
      { return strlen( p->s ); }

   friend ostream& operator<<( ostream&, const Str& );

   // concat 2nd arg to 1st, return reference to 1st
   //
   friend Str& operator+=( Str&, const Str& );
   //
   friend Str& operator+=( Str&, const char* );
   //
   // concat args, retuning a new string
   //
   friend Str operator+( const Str&, const Str& );
   //
   friend Str operator+( const Str&, const char* );
   //
   friend Str operator+( const Str&, const char );


   friend int operator==( const Str& x, const char* s )
      { return strcmp( x.p->s, s ) == 0; }
   //
   friend int operator==( const Str& x, const Str& y )
      { return strcmp( x.p->s, y.p->s ) == 0; }

   friend int operator!=( const Str& x, const char* s )
      { return strcmp( x.p->s, s ) != 0; }
   //
   friend int operator!=( const Str& x, const Str& y )
      { return strcmp( x.p->s, y.p->s ) != 0; }

   // open an istrstream on self
   //
   istrstream* stream() const;

};

#include "general/strutil.h"

// Misc utilities

inline const char* boolToString( bool b )
{ const char* t = b ? "true" : "false"; return t; }

#endif
