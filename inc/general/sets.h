
/*
 * Header file for subsets and bipartitions.
 */

/*tex
 
\file{sets.h}
\path{inc/general}
\title{Header for subset generator classes}
\classes{Subsets, Pairs}
\makeheader
 
xet*/

#ifndef sets_h
#define sets_h

#include "stdtypes.h"
//#include "error.h"

/*
 * Local subset generator.
 */
class Subsets
 {
   int n;	// Universe set size.
   int k;	// Subset size.
   int *a;	// Elements of subset.

public:
   Subsets(int sub_n, int sub_k);

   bool nextset();

   int item(int i) { return a[i]; }

   unsigned int intBitVec(); // if n <= 32

   ~Subsets() { delete [] a; }
 };

/*
 * Local pairing generator.
 */
class Pairs
 {
   int n;	// Universe set size.
   int n2;	// Number of pairs desired.
   int *v1;	// Elements of component 1.
   int *v2;	// Elements of component 2.
   int *used;	// List of paired up elements.

public:
   Pairs(int);

   bool nextpair();

   int item1(int i) { return v1[i]; }
   int item2(int i) { return v2[i]; }

   ~Pairs() { delete [] v1; delete [] v2; delete [] used;}
 };

#endif
