 
// -------------------------------------------
// --------------- bfstream.h ----------------
// -------------------------------------------
 
/*tex
\file{bfstream.h}
\path{inc/general}
\title{Header for binary file stream classes}
\classes{bistream, bostream}
\makeheader
xet*/

#ifndef _bstream_h
#define _bstream_h

#include <fstream>
#include <strstream>
#include "stdtypes.h"
#include <iostream>
//#include <unistd.h>

using std::ios;
using std::cin;
using std::cout;
using std::cerr;
using std::istrstream;
using std::ostrstream;
using std::istream;
using std::ostream;
using std::iostream;
using std::fstream;
using std::ifstream;
using std::ofstream;

extern "C" int close(int);

#ifdef CRI
#define schar signed char
#else
#define schar char
#endif


#ifdef __GNUC__
#include <ext/stdio_filebuf.h>
typedef __gnu_cxx::stdio_filebuf<char> gnu_filebuf;

// ifstream/ofstream class with file descriptor interface, as legacy
// STL implementations had
template<class base_stream, int _openmode>
class _lowlvl_fstream : public base_stream {
private:
   gnu_filebuf buf;
public:
   _lowlvl_fstream(int fd, int _dummy = 0, int bufsize = BUFSIZ);
   _lowlvl_fstream(const char *name, ios::openmode mode);
   gnu_filebuf* rdbuf() { return &buf; }
};

typedef _lowlvl_fstream<istream, ios::in> lowlvl_ifstream;
typedef _lowlvl_fstream<ostream, ios::out> lowlvl_ofstream;
typedef _lowlvl_fstream<iostream, (int)ios::in | (int)ios::out> lowlvl_fstream;

#else

typedef ifstream lowlvl_ifstream;
typedef ofstream lowlvl_ofstream;
typedef fstream lowlvl_fstream;

#endif


//-------------------------------------------------------------------

class astream : public lowlvl_fstream
{
public:
   //astream() {}

   astream( int fd )
   : lowlvl_fstream( fd )
   { setf(skipws); }

   astream(const char* name, ios_base::openmode mode=ios::in )
   : lowlvl_fstream( name, mode )
   { setf(skipws); }

   ostream& ostr() { return *this; }
   istream& istr() { return *this; }

   // put a char as a char (as opposed to as an int)
   //
   astream& put( const char a );
   astream& get( char& a );

   astream& operator<<( astream& (*f)(astream&) ) { return (*f)(*this); }
   astream& operator>>( astream& (*f)(astream&) ) { return (*f)(*this); }

   astream& operator<<( int a );
   astream& operator<<( schar a );
   astream& operator<<( long a );
   astream& operator<<( short a );
   astream& operator<<( uint a );
#ifndef CRAY
   astream& operator<<( uchar a );
#endif
   astream& operator<<( ulong a );
   astream& operator<<( ushort a );
   astream& operator<<( const char* a );
   astream& operator<<( const float a );

   astream& operator>>( int& a );
   astream& operator>>( schar& a );
   astream& operator>>( long& a );
   astream& operator>>( short& a );
   astream& operator>>( uint& a );
#ifndef CRAY
   astream& operator>>( uchar& a );
#endif
   astream& operator>>( ulong& a );
   astream& operator>>( ushort& a );
   astream& operator>>( char*& a );
   astream& operator>>( float &a );
};

// manipulators...
astream& putLeft( astream& );
astream& getLeft( astream& );
astream& putRight( astream& );
astream& getRight( astream& );
astream& eol( astream& );

//-------------------------------------------------------------------

class texstream : public lowlvl_ofstream
{


public:
	//texstream() {}

   /*
   texstream( fstream &stream )
   : lowlvl_ofstream( stream )
   {}
   */

   texstream( int fd )
   : lowlvl_ofstream( fd )
   {}

   texstream(const char* name, ios_base::openmode mode=ios::in)
   : lowlvl_ofstream( name, mode )
   {}

   ostream& ostr() { return *this; }

   texstream& operator<<( int a )    { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( schar a )  { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( long a )   { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( short a )  { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( uint a )   { lowlvl_ofstream::operator<<( a ); return *this; }
#ifndef CRAY
   texstream& operator<<( uchar a )  { lowlvl_ofstream::operator<<( a ); return *this; }
#endif
   texstream& operator<<( ulong a )  { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( ushort a ) { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( const char* a )  { lowlvl_ofstream::operator<<( a ); return *this; }
   texstream& operator<<( const float a )  { lowlvl_ofstream::operator<<( a ); return *this; }
};

//-------------------------------------------------------------------

class bistream 
{

public:
   enum BIBufferType { Memory, File };

private:

   istrstream* s; // memory buffer
   lowlvl_ifstream* f;   // file stream

   bool nonblocking;  // used by file streams only

   static char* _buffer;        // static input buffer
   static bool  _bufferInUse; 
   static const int _BufferSize; // buffer size

public:

   struct MemFlag { };

   // Open and attach to file.
   //
   bistream( const char* name, ios::openmode mode = ios::in );

   // open on nothing
   //
   bistream();

   // Open and attach to memory buffer.
   // Tell the istrstream that size is len.
   // Buffer must be allocated and filled before this call
   // by a call to preAllocBuffer.
   //
   bistream( int len, MemFlag );

   ~bistream();

   void openOnFD( int fd, int bufsize = BUFSIZ, bool nonblocking = false );

   int fd();
   int fail() { return f ? f->fail() : s->fail(); }
   int eof() { return f ? f->eof() : s->eof(); }
   void close();
#if defined(__DECCXX)
   int detach() { return -1; }
#elif defined(__GNUC__)
   int detach() { if (f) close(); return -1; }
#else
   int detach() { if (f) return f->rdbuf()->detach(); else return -1; }
#endif

   // Grab the internal static buffer in order to fill it for a 
   // memory-buffer ctor call.
   // Must be called before the memory-buffer ctor.
   //
   static char* preAllocBuffer();

   // Return the type
   //
   BIBufferType type() const { return (f ? File : Memory); }

   static int bufferSize()
      { return _BufferSize; }

   static void releaseBuffer()
      { _bufferInUse = false; }

   // Return the memory buffer. Error if type != Memory.
   //
   char* buffer();

   bool ok() const;
   //operator void*();
   //operator const void*() const;

   int tellg() const;	// The current position of ios.rdbuf()'s get pointer?
   void seekg( int pos );

   int gcount() const;  // Returns number of characters extracted by the
			// last unformated input function?

#if defined(__DECCXX) || defined(__GNUC__)
   bistream& operator>>( bool& );
#endif
   bistream& operator>>( int& );
   bistream& operator>>( schar& );
   bistream& operator>>( long& );
   bistream& operator>>( short& );
   bistream& operator>>( uint& );
#ifndef CRAY
   bistream& operator>>( uchar& );
#endif
   bistream& operator>>( ulong& );
   bistream& operator>>( ushort& );
   bistream& operator>>( float& );

   bistream& read( char*, int n );

   operator istream* () { if (f) return dynamic_cast<istream*>(f); return dynamic_cast<istream*>(s); }

};

class bostream // : private ofstream
{
public:

   enum BOBufferType { Memory, File };

private:

   ostrstream* s;  // memory buffer output
   lowlvl_ofstream*   f;  // file output
   //BOBufferType _type;

   static char* _buffer;        // static output buffer
   static bool  _bufferInUse;   // static output buffer
   static const int _BufferSize; // buffer size

public:

//#ifndef __xlC__
   struct MemFlag { MemFlag(){} };
//#endif

   // open and attach to file
   //
   bostream(
      const char* name, 
      ios::openmode mode=ios::out
   );

   // open and attach to memory buffer
   //
   bostream( MemFlag );

   // open with no type
   //
   bostream();

   ~bostream();

   //bostream( int fd );
   void openOnFD( int fd, int bufsize = BUFSIZ );

   int fd();
   int fail() { return f ? f->fail() : s->fail(); }
#if defined(__DECCXX)
   int detach() { return -1; }
#elif defined(__GNUC__)
   int detach() { if (f) close(); return -1; }
#else
   int detach() { if (f) return f->rdbuf()->detach(); else return -1; }
#endif
   void close();

   void flush();

   // Return the type
   //
   BOBufferType type() const { return (f ? File : Memory); }

   // Return the memory buffer. Error if type != Memory.
   //
   char* buffer();

   bool operator!() const;
   bool ok() const;
   //operator void*() const;

   int tellp() const; 	// The current position of outs.rdbuf()'s put pointer? 

   // memory buffer only
   //
   int pcount() const;

#if defined(__DECCXX) || defined(__GNUC__)
   bostream& operator<<( bool );
#endif
   bostream& operator<<( int );
   bostream& operator<<( schar );
   bostream& operator<<( long );
   bostream& operator<<( short );
   bostream& operator<<( uint );
#ifndef CRAY
   bostream& operator<<( uchar );
#endif
   bostream& operator<<( ulong );
   bostream& operator<<( ushort );
   bostream& operator<<( const char* );
   bostream& operator<<( const float );

   bostream& write( const char*, int n );

   ostream* ostreamCast() { if (f) return dynamic_cast<ostream*>(f); return dynamic_cast<ostream*>(s); }

};

class bstream : public bistream, public bostream
{
public:
  struct UnbufferedInput { };

  bstream() {}

  ~bstream() { bistream::detach(); bostream::detach(); }

  void openOnFD( int fd, int bufsize = BUFSIZ, bool nonblocking = false )
  { bistream::openOnFD( fd, 0 /*bufsize, nonblocking*/ ); bostream::openOnFD( fd, bufsize ); }
/*
  void openOnFD( int fd, UnbufferedInput )
  {
     bistream::openOnFD( fd, 0 );
     bostream::openOnFD( fd );
  }
*/
  bool readyForRead(int msec_timeout=0); 

  bool readyForWrite(int msec_timeout=0);

  void close() { bostream::detach(); bistream::close(); }

  int eof() { return bistream::eof(); }

  int fail() { return (bistream::fail() || bostream::fail()); }

  int fd() { return bostream::fd(); }

  int detach() { bistream::detach(); return bostream::detach(); }

  int flush() { bostream::flush(); }

};

#endif
