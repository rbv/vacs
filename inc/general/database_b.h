
// -------------------------------------------
// --------------- database_b.h --------------
// -------------------------------------------
 
/*tex
 
\file{database_b.h}
\path{inc/general}
\title{Header for abstract base class for databases}
\classes{DatabaseBase}
\makeheader
 
xet*/

#ifndef _database_b_h
#define _database_b_h

// For off64_t in glibc
#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif

#include <unistd.h>
#include "stdtypes.h"
#include "general/str.h"
#include "general/bitvect.h"

class HugeArray;
template< class T > class DyArray;

// File format version number for all databases
#define DB_FORMAT 3

class DatabaseBase
{
public:

   // The locking support is all disabled/commented out.
   // It appears that it tried to implement locking for
   // different machines accessing a DB on a network share.
   // Certainly don't want to support that anymore.

   // Database open modes
   //
   enum AccessMode
   {
      WriteAccess = 1,
      WriteSync   = 2,    // fsync after writes
      //ReadLock    = 4,  // No longer used
      //WriteLock   = 8,  // No longer used
      Cache       = 16    // Keep in-memory copy of files
   };

   enum Error
   {
      NoError = 0,
      ErrNoDF = 1,        // check errno for file error on data file
      ErrNoKF = 2,        // check errno for file error on key file
      ErrNoCF = 3,        // check errno for file error on control file
      FileError = 4,      // check errno for file error on unknown file
      BufferTooSmall = 5, // passed buffer too small for get
      NoWrite = 6,        // put attempted without write access
      BadPutLen = 7,      // len passed to put is not greater than 0
      //NoEntry = 8,        // get or delete attempted for non-existent record
      //AlreadyWrite = 9,   // database is already open with write access
      BadStreamType = 10, // bostream, bistream must be memory buffered
      //FixTooHard = 11,    // database could not be fixed...too damaged
      //FixAccessError = 12,// database could not be fixed...no access
      //CtlFileMissing = 13,// may be set by checkIntegrity
      //AlreadyLocked = 14, // lock() called twice
      //NotLocked = 15,     // unlock() without lock()
      //CantLock = 16,      // retry count exceeded (warning to cerr)
      BadKey = 17         // key is out of range
   };

private:

   //char* lfName;  // lock file name
   //int lf;  // lock file
   static bool _persistentLock; 

   static const int LockRetryDelay;
   static const int LockRetryCount;
   static const int CreateFileProt;

   DatabaseBase( const DatabaseBase& );
   void operator=( const DatabaseBase& );

protected:

   // for debugging
   Str basename;

   Error error;      // last error

   // access type flags
   bool writeSync;
   //bool readLock;   // no longer used
   //bool writeLock;  // no longer used
   bool allowWrite;
   bool cache;

   // Caching 
   //
   HugeArray* _cache;
   //
   void recordDirty( int recordNumber );
   //
   HugeBitVector _dirtyRecords;

   off64_t positionFile( int fd, off64_t pos, int whence = SEEK_SET ); // seek, sync 

   bool fileIsEmpty( int fd ); // check is file is empty

   //static const int KeyFileGrowIncrement;

   void setError( Error );
   void _lock();
   void _unlock();
   static void _lockSystem();
   static void _unlockSystem();

   int cf;  // control file
   int kf;  // key file
   int df;  // data file

   static bool createFlag; // flag set by static create methods

   // default ctor for creation by static create() methods.
   //
   DatabaseBase();
   //
   void initialize( const char* filename, AccessMode accessMode, bool keyed );

   // Check if the database exists.
   //
   static bool dbExists( const char*, bool keyed );

   // Open a database.
   // Creates if doesn't exist if mode is not ReadOnly.
   //
   DatabaseBase(const char* filename, AccessMode accessMode, bool keyed = true);

   // flush buffers
   //
   void flush();

public:

   // Close the database
   //
   ~DatabaseBase();

   // Delete all files
   static void dbDelete( const char *basename );

   // Check the integrity of the database.
   // Fix any problems if rebuild == true.
   // Check op! or lastError to see if db is ok.
   //
   //virtual void checkIntegrity( bool rebuild = false ) = 0;

   // Check if the record exists.
   //
   //virtual bool exists( int key ) = 0;

   virtual void clearDatabase() = 0;

   // Check if DB is caching
   //
   bool isCaching() const
      { return cache; }

   // Persistent locks
   //
   // Lock the file, for multiple accesses.
   //
   static void lockSystem();
   //
   // Release the lock.
   //
   static void unlockSystem();
   //
   // Check if locked.
   //
   static bool locked();

};

#include "vacs/filesystem.h"

#endif
