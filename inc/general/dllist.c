/*
 * rewrite of GNU DDList (template version) //mjd
 *
 *   copied from Doug Lea (dl@rocky.oswego.edu)
 */

/*tex

\file{dllist.c}
\path{src/template/lists}
\title{Source for double linked lists.}
\classes{ DLList<T>, DLListNode<TNode> }
\makeheader

xet*/

#include <values.h>
#include <iostream>

#include "general/dllist.h"

template <class T>
//
DLList<T>::~DLList() 
{
  if (isEmpty()) return;

  assert( ok() );

  DLListNode<T>* t;

  iter = h->fd;
  //
  while (iter!=h) 
   { 
    t = iter; iter=iter->fd; delete t; 
   }
  //
  delete iter;
}

template <class T>
//
int DLList<T>::length() const
{
  int l = 0;
  DLListNode<T>* t = h;
  if (t != 0) do { ++l; t = t->fd; } while (t != h);
  return l;
}

template <class T>
//
void DLList<T>::prepend(const T &item)
{
  DLListNode<T> *t = new DLListNode<T>(item);

  if (h == 0)
    t->fd = t->bk = h = t;
  else
  {
    t->fd = h;
    t->bk = h->bk;
    h->bk->fd = t;
    h->bk = t;
    h = t;
  }
}

template <class T>
//
void DLList<T>::append(const T &item)
{
  DLListNode<T>* t = new DLListNode<T>(item);
  if (h == 0)
    t->fd = t->bk = h = t;
  else
  {
    t->bk = h->bk;
    t->bk->fd = t;
    t->fd = h;
    h->bk = t;
  }
}

template <class T>
//
DLListNode<T>* DLList<T>::insAfter(DLListNode<T> *p, const T &item)
{
  if (p == 0) error("null index");

  DLListNode<T>* t = new DLListNode<T>(item);
  t->bk = p;
  t->fd = p->fd;
  p->fd->bk = t;
  p->fd = t;
  return t;
}

template <class T>
//
DLListNode<T>* DLList<T>::insBefore(DLListNode<T> *p, const T &item)
{
  if (p == 0) error("null index");

  DLListNode<T>* t = new DLListNode<T>(item);
  t->bk = p->bk;
  t->fd = p;
  p->bk->fd = t;
  p->bk = t;
  if (p == h) h = t;
  return t;
}

template <class T>
//
void DLList<T>::delAfter(DLListNode<T> *p)
{
  DLListNode<T>* u = p;
  if (h == 0) error("empty list");
  if (u == h->bk) error("cannot del_after last");
  if (u == 0) u = h;  // del_after 0 means delete first

  DLListNode<T>* t = u->fd;
  t->bk->fd = t->fd;
  t->fd->bk = t->bk;
  if (t == h) h = t->fd;
  t->fd = t->bk = NULL;

  iter = 0;		// don't screw up iterator.
  delete t;
}

template <class T>
//
void DLList<T>::join(DLList<T>& b)
{
  DLListNode<T>* t = b.h;

  b.h = b.iter = 0;	// empty second list

  if (h == 0)
    h = t;
  else if (t != 0)
  {
    DLListNode<T>* l = t->bk;
    h->bk->fd = t;
    t->bk = h->bk;
    h->bk = l;
    l->fd = h;
  }
}

template <class T>
//
bool DLList<T>::owns(const T &item) const
{
  DLListNode<T>* t = h;
  if (t != 0)
  {
    do
    {
      if (t->getItem() == item) { ((DLList<T>*)this)->iter = t; return true; }
      t = t->fd;
    } while (t != h);
  }
  return false;
}

template <class T>
//
DLListNode<T>* DLList<T>::delAndNext(DLListNode<T> *p, int dir)
{
  if (p == 0) error("null index");
  DLListNode<T>* q;
  if (p->fd == p)
  {
    delete p;
    h = 0;
    q = 0;
  }
  else
  {
    if (dir < 0)
    {
      if (p == h)
        q = 0;
      else
        q = p->bk;
    }
    else
    {
      if (p == h->bk)
        q = 0;
      else
        q = p->fd;
    }
    p->bk->fd = p->fd;
    p->fd->bk = p->bk;
    if (p == h) h = p->fd;
  }

  delete p;
//  p->fd = p->bk = NULL;

  iter = 0;
  return q;
}

template <class T>
//
void DLList<T>::removeFront()
{
  if (h == 0)
    error("removeFront of empty list");
  DLListNode<T>* t = h;
  if (h->fd == h)
    h = 0;
  else
  {
    h->fd->bk = h->bk;
    h->bk->fd = h->fd;
    h = h->fd;
  }

  delete t;
  //t->fd = t->bk = NULL;

  iter = 0;
}


template <class T>
//
void DLList<T>::removeRear()
{
  if (h == 0)
    error("removeRear of empty list");
  DLListNode<T>* t = h->bk;
  if (h->fd == h)
    h = 0;
  else
  {
    t->fd->bk = t->bk;
    t->bk->fd = t->fd;
  }

  delete t;
  //t->fd = t->bk = NULL;

  iter = 0;
}

template <class T>
//
bool DLList<T>::next(T &item) const
{
  if (iter == 0) return false;

  item = iter->getItem();

  ((DLList<T>*)this)->iter = iter->fd;
  //
  if (iter == h) ((DLList<T>*)this)->iter=0;

  return true;
}

template <class T>
//
void DLList<T>::nextWrap(T &item) const
{
  assert(iter);

  item = iter->getItem();

  ((DLList<T>*)this)->iter = iter->fd;

  return;
}

template <class T>
//
bool DLList<T>::prev(T &item) const
{
  if (iter == 0) { return false; }

  item = iter->getItem();

  if (iter == h) ((DLList<T>*)this)->iter = 0;
  else           ((DLList<T>*)this)->iter = iter->bk;

  return true;
}


template <class T>
//
int DLList<T>::ok() const
{
  int v = 1;
  if (h != 0)
  {
    DLListNode<T>* t = h;
    int count = 99999999;      // Lots of chances to find h!
    do
    {
      count--;
      v &= t->bk->fd == t;
      v &= t->fd->bk == t;
      t = t->fd;
    } while (v && count > 0 && t != h);
    v &= count > 0;
  }
  if (!v) error("invariant failure");
  return v;
}

/*
 * i/o
 */
template <class T>
//
ostream& operator<<(ostream &o, const DLList<T> &l)
 {
   T item;

   o << "<< ";

   l.start();
   //
   while (l.next(item))
    {
      o << item << ' '; 
    }
   //
   l.stop();

   o << ">>";
   return o;
 }

