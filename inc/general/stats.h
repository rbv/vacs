/* This class is unfinished
 */

#include <vector>

class Stats {

    // If true, the histogram is extended to the right
    // as new data arrives, keeping bin width the same, but never to the left
    bool autogrow;

    std::vector<int> histogram;
    double histMin, histMax;
    double binWidth;
    // For mean and variance
    double total_x;
    double total_x2;  // Sum of squares
    int counts;

  public:
    Stats(int bins, double lbound, double rbound, bool _autogrow = false);
    Stats(int bins, double binsize = 1.);  // autogrows

    void printHist();

    void addDatum(double val);

};
