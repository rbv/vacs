
/*
 * Floating point user and system CPU timers.
 */

/*tex

\file{dtime.h}
\path{inc/general}
\title{Header for user and system CPU timer class}
\classes{DTime}
\makeheader

xet*/

#ifndef _Dtimer_h
#define _Dtimer_h

//#define NOSTIME

// C version stolen from:
//
// char *= "@(#)dtime_ - v1.0  01/04/90  Don Faatz  Rensselaer CS Lab"; 
// char *= "@(#)dtime_ - Emulate Sun FORTRAN dtime() function in 'C'";

#include <time.h>
#include	<sys/types.h>
#include	<sys/times.h>
#include	<sys/param.h>

#if defined(CRAY) || defined(__APPLE__)
#include <limits.h>
#define HZ CLK_TCK 
#endif
#ifdef __DECCXX
#define HZ 400
#endif

#include	"assert.h"

class Dtime
 {
    float	l_user;
    float	l_sys;
    float	tarray[2];
 
public:
    Dtime() { tarray[0] = tarray[1] = 0.0; }
 
    /*
     * Set up current CPU mark.
     */
    void start() 
     {
	struct	tms	l_time;

	Assert ((times(&l_time) != -1));  	
         {
		l_user = l_time.tms_utime;
		l_sys = l_time.tms_stime;
         }
     }

    /*
     * Returns elapsed CPU time since last call ( or start() ).
     */
    float operator()()
     {
	struct	tms	l_time;
	float	l_utime;
	float	l_stime;

	Assert (times(&l_time) != -1);  	// We can update times.

         {
		l_utime = l_time.tms_utime;
		l_stime = l_time.tms_stime;
		tarray[0] = (l_utime - l_user)/HZ;
		tarray[1] = (l_stime - l_sys)/HZ;
		l_user = l_utime;
		l_sys  = l_stime;

                // return user+system time
      	        //
#ifdef NOSTIME
		return(tarray[0]);	
#else
		return(tarray[0] + tarray[1]);	
#endif
         }
     }
    //
    float stop() { return (*this)(); }

    /*
     * Return a breakdown of last operator() call.
     */
    float user() { return tarray[0]; }
    //
    float system() { return tarray[1]; }

    // clock() is no more precise than times() on Linux
    static double CPUtime() { return (double)clock() / CLOCKS_PER_SEC; }
};

#endif
