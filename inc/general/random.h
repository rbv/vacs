
// -------------------------------------------
// ----------------- random.h ----------------
// -------------------------------------------

/*tex
 
\file{random.h}
\path{inc/general}
\title{Header for random number generator class}
\classes{RandomInteger}
\makeheader
 
xet*/

 
#ifndef _random_h
#define _random_h

#include "vacs/object.h"
#include "stdtypes.h"

class RandomInteger 
#ifdef VACSOBJECT
			: public VacsObject
#endif
{

private:

	// a 32-bit primitive CA
	//
	static const ulong _mask;

	// current value of the generator
	//
	ulong _seed;

	// the range of numbers to return
	//
	int _min, _max;

	// initialize _seed from system time
	//
	void initSeed();

public:

	// create a generator that returns values 0 .. modulus-1
	//
	RandomInteger( int modulus );

	// create a generator that returns values min .. max
	//
	RandomInteger( int min, int max );

	// Get the modulus
	//
	int modulus()
           { return _max+1; }

	// Change the modulus
	//
	void modulus( int m )
           { _max = m-1; }

	// set the seed manually
	// use this to get thet same sequence every time
	//
	void setSeed( ulong value );

	// return the next number
	//
	int operator()();

};

#endif
