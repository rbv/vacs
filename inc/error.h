
// -------------------------------------------
// -------------- error.h --------------------
// -------------------------------------------

/*tex
 
\file{error.h}
\path{inc}
\title{Header for error-handling functions}
\classes{}
\makeheader
 
xet*/


#ifndef _error_h
#define _error_h

// Header file for all error-related functions

#include <stdlib.h>

#include "general/stream.h"
#include "stdtypes.h"

// Program termination.
// Call Exit() to quit the program.
// Pass a function to callOnExit for final cleanup.
// 
void Exit( int level=0 );
// 
// just can't figure out how to do w/o typedef!!
typedef void (*ExitFunction)(void);
void callOnExit( ExitFunction );


inline void fatalError( const char* s )
{
   cerr << "Fatal error: " << s << '\n';
   Exit(-1);
}

/*
 * Allow old error handling to work.
 */
// template <class X>
inline void error(const char *X) 
 { 
#ifdef _NDEBUG
  ;
#else
  cerr << "error(" << X << ")" << '\n'; assert(false); 
  //{ printf("Declining feature 'error(\"%s\");' called!\n", X); assert(0); }
#endif
 }

#endif
