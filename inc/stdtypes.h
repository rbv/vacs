
// -------------------------------------------
// ------------ stdtypes.h --------------------
// -------------------------------------------

/*tex
 
\file{stdtypes.h}
\path{inc}
\title{Header for standard types}
\classes{}
\makeheader
 
xet*/

#ifndef _stdtypes_h
#define _stdtypes_h

// This should be the only include of assert_h in the system.
//
#include "assert.h"

#include "vacs/freestore.h"

/*
 * TYPE CONVENTIONS:
 *
 * Class names: (1) Begin with a upper case letter
 *              (2) No underscore characters (unless private subclass)
 *		(3)
 *
 * Typedefs: (1) Begin with a lower case letter for syntactic sugar types.
 *               (ushort, bool, vertNum, etc.)
 *           (2) Class or application specific types should start with 
 *		 an uppercase and/or read as a type. (IntArray, etc.)
 *
 * Preprocessor stuff:  (1) Heavy on the caps (e.g., STRMAX, XFUNCPROTO)
 *                      (2) Begin with double underscore (e.g., __graph)
 *			    [ _graph_h is also acceptable. ]
 *
 * Function names:  
 *  (a) static:
 *  (b) extern:  
 *  (c) member:
 *  (d)
 *
 * Variable names:
 *  (a) local:		Author's choice?
 *  (b) global:		Try to avoid.  Name to signify globalness.
 *  (c) private:        Begin with underscore.
 *  (d) public:    	Shouldn't ever be any!  see (1)
 *  (e) protected: 	No underscore.
 *  (f)
 *
 */
// (1) a little more work, but it's better to do
//   private: 
//      int _foo;
//   public:
//      void foo( int i ) { _foo = i; }
//      int foo() { return _foo; }
//  Much easier in the long run, for inserting assertions or whatever.

typedef unsigned int	uint;
typedef unsigned char	uchar;
typedef unsigned long	ulong;
typedef unsigned short	ushort;

/*
 * Standard constants.
 */
//#ifndef DOLEDA
//#define __BUILTIN_BOOL__
#ifndef __DECCXX
#ifndef __GNUC__
typedef char    bool;
//const bool false = 0;
//const bool true = 1;
#ifndef BOOLENUM
#define BOOLENUM
enum {false = 0, true = 1};
#endif
#endif
#endif
//#endif

#ifdef __GNUC__
#define PACKED __attribute__((packed))
#else
#define PACKED
#endif

#ifdef __APPLE__
#include <unistd.h>
typedef off_t off64_t;
#define lseek64 lseek
#endif

// maximum length for boring strings (problem name, etc)
//
#define STRMAX 256

//template <class T> inline T max(T i, T j) { return (i > j) ? i : j; }
//template <class T> inline T min(T i, T j) { return (i < j) ? i : j; }
//template <class T> inline void swap(T& i,T& j) { T t=i; i=j; j=t; }

// iostream defines
//
const char nl = '\n';
const char tab = '\t';
const char bell = '\7';

// binary io 
//
// #ifdef __xlC__
#if defined(__xlC__) || defined(__DECCXX)
#include <iostream>
#include "general/stream.h"
#endif

#define iodeclarations( aClass )					\
   friend ostream& operator<<( ostream&, const aClass& );		\
   friend istream& operator>>( istream&, aClass& );		\
   friend texstream& operator<<( texstream&, const aClass& );	\
   friend bostream& operator<<( bostream&, const aClass& );	\
   friend bistream& operator>>( bistream&, aClass& );		\
   friend astream& operator<<( astream&, const aClass& );		\
   friend astream& operator>>( astream&, aClass& );

// This should be placed before the definition of a templated class with
// operator<</>> overloads.
// aClass should take a single template argument
#define templated_pre_iodeclarations( aClass )					\
   template<class U> class aClass;									\
   template<class U> ostream& operator<<( ostream&, const aClass<U>& );		\
   template<class U> istream& operator>>( istream&, aClass<U>& );		\
   template<class U> texstream& operator<<( texstream&, const aClass<U>& );	\
   template<class U> bostream& operator<<( bostream&, const aClass<U>& );	\
   template<class U> bistream& operator>>( bistream&, aClass<U>& );		\
   template<class U> astream& operator<<( astream&, const aClass<U>& );		\
   template<class U> astream& operator>>( astream&, aClass<U>& );

#define templated_iodeclarations( aClass )					\
   template<class U> friend ostream& operator<<( ostream&, const aClass<U>& );		\
   template<class U> friend istream& operator>>( istream&, aClass<U>& );		\
   template<class U> friend texstream& operator<<( texstream&, const aClass<U>& );	\
   template<class U> friend bostream& operator<<( bostream&, const aClass<U>& );	\
   template<class U> friend bistream& operator>>( bistream&, aClass<U>& );		\
   template<class U> friend astream& operator<<( astream&, const aClass<U>& );		\
   template<class U> friend astream& operator>>( astream&, aClass<U>& );

//#define _iodefbody_ { aassert(false); return o; }

#define iodefinitions( aClass )					\
inline ostream& operator<<( ostream& o, aClass& ) _iodefbody_ \
inline istream& operator>>( istream& o, aClass& ) _iodefbody_ \
inline texstream& operator<<( texstream& o, aClass& ) _iodefbody_ \
inline bostream& operator<<( bostream& o, aClass& ) _iodefbody_ \
inline bistream& operator>>( bistream& o, aClass& ) _iodefbody_ \
inline astream& operator<<( astream& o, aClass& ) _iodefbody_ \
inline astream& operator>>( astream& o, aClass& ) _iodefbody_

#define iodefinitions2( aClass )				\
inline texstream& operator<<( texstream& o, aClass& ) _iodefbody_ \
inline bostream& operator<<( bostream& o, aClass& ) _iodefbody_ \
inline bistream& operator>>( bistream& o, aClass& ) _iodefbody_ \
inline astream& operator<<( astream& o, aClass& ) _iodefbody_ \
inline astream& operator>>( astream& o, aClass& ) _iodefbody_

// cast an object to another type
// Warning! Only use to cast a reference to a reference to a base class.
//
//#define CAST( anObject, aClass ) *( (aClass*) (&(anObject)) )
//
// Even better!  Cast as a reference.
//
#define CAST( anObject, aClass ) ( (aClass&) anObject )

// set an object to zero
//
#define setZero { ::memset( (char*) this, 0, sizeof(*this) ); }

#endif

