
/*
 * Browser Class contains current state of searchnode position.
 */

/*tex
 
\file{brstate.h}
\path{inc/browser}
\title{Header for browser state}
\classes{Browser}
\makeheader

 
xet*/


#ifndef _browseState_h
#define _browseState_h

#include <iostream>	// for error checking

#include "stdtypes.h"
#include "error.h"

#include "vacs/runinfo.h"
#include "vacs/filesystem.h"
#include "search/search.h"
#include "search/searchnode.h"
#include "search/searchminor.h"


class BrowserState
 {
	/**
         ** Search node stuff in this global browser state class.
         **/

	 NodeNumber parent;

	 SearchNode *sNode;
         NodeNumber sNodeNum;
         SearchNodeIter nodeIter;          

	 SearchMinor *sMinor;
	 SearchMinorIterator* sMinorIter;

	 NodeNumberCollectionIter* siblings;

	 Array<NodeNumber> _siblings;
	 Array<NodeNumber> _children;

	// Called whenever sNode should change.  Updates all members.
	//
	void update( NodeNumber );

	// Init all pointer members to 0
	//
	void initMembers();
 
  public:
     
     /*
      * Constructor loads root search node.
      */
     BrowserState(SearchNodeIter::IteratorType 
	  	   iter = SearchNodeIter::All) : nodeIter(iter) 
      {
       initMembers();
       sMinorIter = 0;
       siblings = 0;

       update( NodeNumber::firstNumber() );
      };


     ~BrowserState()
      {
       delete sMinorIter;
       delete siblings;
      }

     /*
      * Access functions.
      */
     bool nodeAvailable() { return (sNode !=0); }
     //
     NodeNumber nodeNumber() const
        { assert(sNode); return sNodeNum; }
     //
     SearchNode& node() const
        { assert(sNode); return *sNode; }
     //
     bool minorAvailable() { return (sMinor !=0); }
     //
     SearchMinor& minor() const
        { assert(sMinor); return *sMinor; }
     //
     NodeNumber nodeParent() const
        { assert(sNode); return sNode->parent(); }
     //
     const Array<NodeNumber> nodeChildren() const
        { assert(sNode); return _children; }
     //
     const Array<NodeNumber> nodeSiblings() const
        { assert(sNode); return _siblings; }
    
     /*
      * Traverse filesystem member functions.
      * (If false is returned then state should be unchanged!)
      */
     bool toViaKey(NodeNumber::nodeKey nNum);
     //
     bool toPrev();
     //
     bool toNext();
     //
     bool toParent();
     //
     bool toFirstChild();
     //
     bool toLastChild();
     //
     bool toPrevSib();
     //
     bool toNextSib();

     /*
      * Change current minor.
      */
     bool toViaLabel(MinorNumber mNum) { (void) mNum; return false; }
     //
     bool toFirstMinor();
     //
     bool toLastMinor();
     //
     bool toPrevMinor();
     //
     bool toNextMinor();

     /*
      * Change Next/Prev traverse methods.
      */
     void setIter(SearchIter::IteratorType iter)
      {
         (void) iter;
//       nodeIter.nowIteratorOver(iter);
      }
 };

#endif
