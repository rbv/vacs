
/*
 * Browser Class contains current state of all widgets.
 */

/*tex
 
\file{browser.h}
\path{inc/browser}
\title{Header for browser}
\classes{Browser}
\makeheader

 
xet*/


#ifndef _browser_h
#define _browser_h

#include <iostream>	// for error checking

#include "search/nodenum.h"
//#include "search/search.h"
#include "search/searchnode.h"
#include "search/searchminor.h"
#include "stdtypes.h"

// X11 includes
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>

// Xlib includes
#include <X11/Xlib.h>
#include <X11/Xutil.h>
// #include <math.h>

// Athena widget includes
#include <X11/Xaw3d/AsciiText.h>
#include <X11/Xaw3d/Box.h>
#include <X11/Xaw3d/Cardinals.h>
//#include <X11/Xaw3d/Clock.h>
#include <X11/Xaw3d/Command.h>
#include <X11/Xaw3d/Dialog.h>
#include <X11/Xaw3d/Form.h>
#include <X11/Xaw3d/Label.h>
//#include <X11/Xaw3d/List.h>
//#include <X11/Xaw3d/Logo.h>
#include <X11/Xaw3d/MenuButton.h>
//#include <X11/Xaw3d/Scrollbar.h>
#include <X11/Xaw3d/SimpleMenu.h>
#include <X11/Xaw3d/SmeBSB.h>
#include <X11/Xaw3d/SmeLine.h>
//#include <X11/Xaw3d/StripChart.h>
#include <X11/Xaw3d/Paned.h>
#include <X11/Xaw3d/Toggle.h>
//#include <X11/Xaw3d/Viewport.h>

/*
 * Structure containing external data.
 *  (searchnodes, bpgs, etc.)
 */
extern class BrowserState *bState;
extern class Browser *xBrowser;

/*
 * Our friendly window interface.
 */
class Browser 
 {
private:
	//
	// Warning!  All of this is still in the experimental stage. 
	// (i.e., widget structure currently set up for playing)
	//

	/*
         * Widgets variable are indentated to denote widget tree structure.
	 * 	(excludes popup widgets)
         */
	Widget	topLevel,
		    panedLayout,
			titleLabel,
			topBox,
			    quitCommand,
			    infoCommand,
			    proofButton,
				proofMenu,
				    saveProof,
				    rereadProof, // ______
				    deleteProof,
			    keyLabel,
			proofText,
			graphNumLine,
			minorLine,
			glueDialog,
			boxOfButtons,
			    minorButton,
                                minorMenu,
				    viaMinorLabel,
					// a popup widget,
				    firstMinor,
				    lastMinor,
				    nextMinor,
				    prevMinor,
			    glueButton,
			    viewToggle,
                        bottomBox,
			    viaGraphNum,
				// a popup widget,
			    prevGraphNum,
			    nextGraphNum, //_______ 
			    toParent,
			    firstChild,
			    lastChild,
			    nextSibling,
			    prevSibling,
                        viewTree,
                        viewGraph;

        /*
         * Hopefully, just one common "application context" for all browsers.
         */
	XtAppContext appContext;

        /*
         *  Data for drawing graphs.
         */
        struct {
           Display*   display;    // Display for the window.
           Window     window;     // Window for the drawable.
           GC         gc;         // Graphics context.
        } viewWindow;

        /*
         *  Data for browsing graphs.
         */
        struct {
           Window window;
           int    viewMargin;
           int    selMargin;
        } treeWindow;

        /*
         *  Toggle button flag for pictorial.
         */
         bool toggleFlag;
	 bool ShowPictorial;
         

        /*
         * Friendly callback procedure.
         */
        friend void quit( Widget, XtPointer, XtPointer );
        //
        bool quitFlag;

        //----------------------------------------------------------------

        /*
	 * Internal widget design functions.
	 */
	void createTopBoxButtons( );
        //
        void createNodeButtons( );
        //
	void createProofMenu( );
        //
	void createGraphNumLine( );
        //
	void createMinorLine( );
        // 
        void createBoxOfButtons( );
        //
	void createNodeMenu( );
        //
	void createMinorMenu( );

        //----------------------------------------------------------------

 	/*
	 * Specific widget update routines.  ( Uses *bState )
 	 */
	void _updateKeys();
	//
	void _updateProof() { ; }
	//
	void _updateGraph();
	//
	void _updateMinor();
	//
	void _updateView() { ; }

public:
	/*
         * Construct (build) browser layout given root Widget.
         */
        Browser(XtAppContext appCon, Widget top);

        /*
         * Destructor destroys screen and this class.
         */
        ~Browser() {}

         void activate();

         Widget top() const { return topLevel; }

	/*
	 * Public update routines intended for callback functions.
	 */
	void updateScreen()
         {
           _updateProof();
           _updateGraph();
            updateMinor();
         }
	//
	void updateMinor() 
         { 
           _updateKeys();
           _updateMinor();

           // if (ShowPictorial) _updateView();
           if (toggleFlag) DrawTree();
           if (toggleFlag) DrawGraph();
         }
         //
         void DrawGraph();
         void ToggleFlag()
         {
            toggleFlag = !toggleFlag;
         }

         // tree graph support functions
         void DrawTree();
         void GetNewNodeFromTree(int, int);
         NodeNumber::nodeKey SelectNodeFromTree(int, int);
         void DrawLineInTree(int, int, int, int);
         void DrawNodeInTree(int, int, bool);
         void DrawMarkInTree(int, int);
         void DrawStringInTree(int, int, char*);
         
         

 };

    /*
     * X-window callback functions. 
     *
     * (Don't have time to mess with the security leak.)
     */
    void textdump( Widget, XtPointer, XtPointer );

    void popupAskKey( Widget, XtPointer, XtPointer );

    void popupSelectIter( Widget, XtPointer, XtPointer );

    void popupAskLabel( Widget, XtPointer, XtPointer );

    void proofSave( Widget, XtPointer, XtPointer );
    //
    void proofReread( Widget, XtPointer, XtPointer );
    //
    void proofDelete( Widget, XtPointer, XtPointer );

    void dumpProof( Widget, XtPointer, XtPointer );

    void toggleView( Widget, XtPointer, XtPointer );

    void dumpGlue( Widget, XtPointer, XtPointer );

    void pGraphNum( Widget, XtPointer, XtPointer );
    //
    void nGraphNum( Widget, XtPointer, XtPointer );
    //
    void toCreator( Widget, XtPointer, XtPointer );
    //
    void fChild( Widget, XtPointer, XtPointer );
    //
    void lChild( Widget, XtPointer, XtPointer );
    //
    void pSib( Widget, XtPointer, XtPointer );
    //
    void nSib( Widget, XtPointer, XtPointer );

    void fMinor( Widget, XtPointer, XtPointer );
    //
    void lMinor( Widget, XtPointer, XtPointer );
    //
    void pMinor( Widget, XtPointer, XtPointer );
    //
    void nMinor( Widget, XtPointer, XtPointer );
    //
    void okayKeyInput( Widget, XtPointer, XtPointer );

    /*
     * Translator action routines.
     */
    void endNodeInput( Widget, XEvent*, String*, Cardinal* );
    //
    void endMinorInput( Widget, XEvent*, String*, Cardinal* );
    //
    void setGlue( Widget, XEvent*, String*, Cardinal* );
    //
    void treeGraphEvent( Widget, XEvent*, String*, Cardinal* );

    void quit( Widget, XtPointer, XtPointer );

#endif
